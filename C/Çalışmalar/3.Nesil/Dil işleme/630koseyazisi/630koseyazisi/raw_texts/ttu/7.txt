Bu b�rokrasiyle turizm hedefi zor tutar 
BA�BAKAN Erdo�an, turizm i�in haz�rlad�klar� projeksiyonu sundu. 

Gere�inden fazla iddial� hedefler beni hep �rk�tm��t�r. Ancak bu kez �yle de�il. Hayalci olmadan, ula��labilir hedefler koymu�lar. Ancak T�rkiye'deki b�rokrasiyle en ula��labilir hedeflerin bile nas�l ula��lamaz hale geldi�ini ben biliyorum.

�zellikle de turizmde.

�rnek mi?

Hemen anlatay�m.

Hat�rlayacaks�n�z, bu k��ede Antalya'da turizme a��lan ��Aksu-Lara�� sahil band�nda dev oteller yap�ld�, yap�l�yor ve yap�lacak. 

Binlerce yatakl� enfes bir sahil �eridi olu�uyor.

Ancak burada �ok ciddi bir sorun vard�. Bu milyar dolarl�k tesislere turisti ula�t�rabilecek bir yol yoktu.

Bu durum, bu k��ede defalarca dile getirildi ve sonunda konu bizzat Ba�bakan'�n m�dahalesiyle ��z�ld�. 

Ba�bakan kendi �abalar�yla bu yol i�in ��ekstra�� bir kaynak yaratt� ve yol yap�m� belirli bir a�amaya geldi. Yaza kadar bitecek.

Ayn� b�lgede yine bu k��ede dile getirilmi� bir ba�ka sorun vard�. 

Turizm a��s�ndan m�thi� potansiyel bar�nd�ran bu sahil band�n�n en g�zel kesimi, kamuya ait kamplar ve kiral�k ��obalarla�� i�gal edilmi�ti. 

Yaza yaza bunlar�n da ��y�k�lmas�n��� sa�lad�k. 

Burada aynen Konyaalt�'nda oldu�u gibi bir turistik tesisler b�t�n� olu�turulacakt�.

Projenin ad� ��Lara Beach Park��t� ve �ok modern bir projeydi. 

Ancak bu i� nedense y�r�m�yor. 

B�rak�n buraya yeni tesislerin yap�m�na ba�lanmas�n�, y�k�lan kamp ve obalar�n molozlar� bile kald�r�lm�yor.

Antalya'n�n en yeni ve en g�zel turizm band� bir mezbelelik.

Nedeni ise bir vali yard�mc�s�. 

Bir y�lda kendini amorti edebilecek ve b�y�k turizm geliri sa�layacak bu proje, bir vali yard�mc�s� y�z�nden y�r�m�yor. 

Bu durumu vali biliyor, ilgili belediyeler biliyor ancak kimse bir �ey yapam�yor.

�ddialara g�re s�z konusu vali yard�mc�s�, buran�n Konyaalt� gibi tek par�a halinde yap�l�p ihalelerle kiralanmas�n� istemiyor ve bu y�zden projeyi engelliyor.

Bunu yaparken de arkas�nda AKP'nin oldu�u havas�n� yay�yor.

Antalya'n�n en k�sa s�rede hayata ge�ecek, en g�zel projesi duruyor.

Ba�bakan ise turizmde b�y�k hedeflerden bahsediyor.

Ben de g�l�yorum.

Ses k�t�, yan�t g�zel

AY�E Hatun �nal, alb�m�ne y�nelik ele�tirime son derece ����k�� bir yan�t vermi�.

��Fatih Altayl� benim yapt���m m�zik t�r� i�in biraz ya�l�.��

Ay�e Hatun �nal'�n yan�t verme d�zeyi, umar�m T�rk bas�n�ndaki kimi yazarlar i�in de �rnek te�kil eder.

Hem zek� dolu, hem i�neleyici, hem de �ok fazla k�r�c� de�il.

Popstar sadece bir ses midir?

POPSTAR yar��mas�ndan Elena elendi. Herkes �okta. Ben ise hi� �a��rmad�m. 

Bence o yar��maya kat�lan �ocuklar i�inde en g�zel sesi olan, en iyi �ark� yorumlayan ve b�y�k ihtimalle m�zik bilgisi en fazla olan Elena idi.

Ama T�rkiye'nin popstar� olmas� m�mk�n de�ildi. Bunu o da, o yar��may� d�zenleyenler de, o yar��maya oylar� ile kat�lanlar da biliyordu.

Elena'n�n oradaki varl���, g�zel bir ��fantezi�� idi ve bence buraya kadar gelmesi bile b�y�k ba�ar�yd�. 

Allah a�k�na do�ru s�yleyin; Rus aksan�yla T�rk�e konu�an, �irin ve sempatik olmas� d���nda hi�bir ��s�r�kleyici�� �zelli�i olmayan birinin T�rkiye'nin ��popstar��� olmas� sizce m�mk�n m�yd�?

Bence de�ildi. 

Elena'n�n elenmesi asl�nda �ok �nemli bir ger�e�i ortaya ��kard�. Sadece bu yar��ma �rne�inden hareket etsek bile ��popstar�� demek, sadece ��en iyi ses�� demek de�il. ��Popstar�� demek, en yak���kl� ya da en g�zel de demek de�il (Sel�uk da elenmi�ti). ��Popstar�� demek, en iyi m�zik bilgisi olan anlam�na da gelmiyor.

Popstarl�k ba�ka bir �ey. 

Bir ���k, bir etki... Moda tabiriyle bir ��karizma��.

Kalan yar��mac�larda bu ne kadar var bilmiyorum ama Elena'da hi� yoktu, �st�ne �stl�k bir de yabanc� k�kenliydi. 

O y�zden elendi.

J�ri halk de�il de m�zik otoriteleri olsayd�, Elena yar��man�n ku�kusuz birincisi olurdu.

Ama j�ri halk. 

Halk dedi�inin b�y�k b�l�m� ise mevcut popstarlar i�in �l�p biten ve yenileri i�in �l�p bitmek isteyen, daha da �tesi yenisinin se�iminde pay sahibi olmak isteyen ��varo� k�zlar���.

�stelik onlar i�in popstar�n anlam� bizimkinden farkl�. 

�brahim Tatl�ses'in aya��ndan ��kar�p f�rlatt��� �orab� kap��anlar onlar...

Emrah'� g�r�nce d���p bay�lanlar onlar...

Elena'y� eleyenler de onlar...

Ama yine de ben en �ok Elena'y� dinlemeyi �zleyece�im.

NE ZAMAN ADAM OLURUZ?

G�venin korkuyla tesis edilemeyece�ini anlad���m�z zaman. 
