Don Hooton looked at Alex Rodriguez squirming through a news conference, and he thought about his own son, dead since 2003. 

Both knuckleheads. Both willing to try something they did not understand. One, playing with a $250 million contract, currently undergoing some public unpleasantness over steroids. The other, once a high school junior, trying to put on some bulk, now a name on a foundation. 

Don Hooton was present under the big tent Tuesday as Rodriguez became the latest Yankee, the latest major league superstar, to be linked to performance-enhancing drugs.

Rodriguez gave his updated version of the truth, how he and a cousin, whose name he would not reveal, had tried a substance available in the Dominican Republic. He called it boli, but the original article on SI.com said he had tested positive, in 2003, for Primobolan, an expensive form of steroid not available in the United States.

Rodriguez said he used it maybe twice a month from 2001 through 2003, but he claimed he had no idea whether it helped him or what it might have done for his body.

Young, Rodriguez referred to himself at 24 and 25.

Stupid. He called himself stupid many times, blaming his lack of a college education, as if that was an excuse for overlooking the warning that steroids have been banned by Major League Baseball since 1991. He started using the stuff, or so he claimed, three years after the first public suspicions about Mark McGwire and other sluggers of that generation. But he just went ahead and tried the stuff, because the era was so �loosey goosey.�


His answers were vague, aimed toward moving along, so he could join the large complement of teammates sitting off to his right, giving him support. What else could they do? Not show up? 

But at least Alex Rodriguez was alive. And maybe his venture into the wonderful world of performance-enhancing drugs could help save a few young people who have a bit of knucklehead in them, too.

That was why Don Hooton was under the Yankees� big tent. Hooton runs a foundation aimed at educating young people not to put substances into their bodies in the name of bigger muscles and tighter abs.

Hooton reached out to Rodriguez a day after the Yankees star made his bland appearance on ESPN on Feb. 9, after the article on SI.com on Feb. 7.

Rodriguez did not seem to know much about Taylor Hooton, whose story has been told quite often in print and on television in recent years. Once Hooton reached out to Rodriguez, the player did call Hooton and volunteer to make appearances for the foundation to talk about the dumb things he did. It is a start.

�Sixteen-year-olds have no idea,� Hooton said Tuesday after listening to Rodriguez describe his own path through steroids.

Hooton encountered the wrath of steroids in January 2003. His son was 16 at the time, was 6 feet 2 inches and 180 pounds, not exactly a stick figure, but slender enough that the high-school baseball coach told him he needed some more bulk for the level of baseball in that part of Dallas.

�What the coach didn�t know was that half the team was already on anabolic steroids,� Hooton said.

The son gained 30 pounds in a few months before the family noticed his mood swings, known as roid rage. A psychiatrist urged him to stop taking the drug, but it can take a year for the body to produce testosterone again. Going through withdrawal, Taylor Hooton began experiencing depression.

�June 10 was his birthday,� Don Hooton said. �We rented a suite at the Rangers game.� The father did not remember how Alex Rodriguez, then in his third year of using a steroid, performed that night. He does know that his son committed suicide on July 15, 2003.

These are details that perhaps Alex Rodriguez will learn if he follows through on his promise to make appearances for the Hooton Foundation, which has been recommended to him as a reputable institution, with a $700,000 annual budget. The subject of money was not raised Tuesday, and perhaps it would have been crass, but suffice it to say that Rodriguez could subsidize a lot of seminars for young people with the loot the Steinbrenners are paying him.

His own presence could also be valuable, particularly because a significant percentage of professional athletes who test positive are from Latin American countries.

If A-Rod follows through, if he remembers, if he actually means what he says, he could have some redeeming social value.

�People have no idea,� Don Hooton said. �I met a cheerleader who wanted to have abs, so she took a drug called Winstrol, and within five weeks, she tried to commit suicide. She�s all right. But these kids don�t know what they are taking.�


What Hooton has learned since his son died is that major leaguers have access to the best stuff, the safest stuff, because of their money.

�The kids are getting crap sold in gyms,� Hooton said. �Stuff from China comes in, gets mixed with cooking oil, peanut oil, over 20 percent of the stuff is contaminated. These kids are injecting themselves with poison. Just because it�s called a certain name, that doesn�t mean that�s what they�re getting.�

Hooton has learned a lot since his son died. He spent this day watching a wealthy major leaguer who may or may not be learning anything, but at least could be valuable to scare the next generation of steroid knuckleheads.

