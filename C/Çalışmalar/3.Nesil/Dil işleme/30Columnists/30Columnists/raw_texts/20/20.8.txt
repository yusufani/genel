Last week, certain members of the financial press were up before the parliamentary Treasury committee to answer questions about their role, if any, in contributing to the current crisis.

We members of the press are all too aware of the accusations that we did not "see it coming", or assisted its coming, or, now it has come, are overdoing the gloom. Such accusations tend to be levelled not only by non-members of the press but also by the merry band of former editors who now write "media" columns.

One former editor of the Financial Times, Richard Lambert, now director-general of the CBI, introduced his guest speaker, Lord Mandelson, at a lunch in Davos by proclaiming "the official end of 'Who can be gloomiest in Davos?' week".

Lambert, an old friend of mine, is someone my Irish mother would have described as possessing "a sunny disposition". Just as well, because not least among the sources of gloomy news and statistics has been the CBI itself, whose surveys and economic assessments have a remarkably good record.

Now, the scene shifted from Davos to snowbound London, where the airwaves were full of news of a study concluding that we should all spend more time with our families, and reports that the economy had lost billions in output because - well, we were building snowmen and tobogganing with our families.

Personally, I take the concern about lost output at a time like this with a pinch of the salt that is reportedly in such short supply. Lost output can soon be made up - if there is any demand for it - although one naturally sympathises with anyone whose life was seriously disrupted by the events of last week.

Yes, for economists, and indeed for everyone else, what matters at a time of recession is ensuring that there is enough demand in the economy. The traditional justification for economic policies deliberately aimed at reducing the level of demand is inflation, or fear of inflation, or the need to prop up a collapsing currency.

Politicians and central bankers seldom admit that their actions cause recession. Inflation is not the problem at the moment. That is why policymakers are throwing everything, including the kitchen sink, at trying to prevent the recession turning into outright depression. They have not deliberately caused this recession, but their actions, and the insufficient attention they paid to economic history and the causes of business cycles ("boom and bust"), certainly contributed to the present crisis.

One thing that worries not just the government and the financial markets but also the layperson is the impact on government revenue of the recession and the associated jump in government borrowing. I bumped into a senior Inland Revenue officer the other day who asked my opinion, and I replied that I was becoming "cautiously more pessimistic". The words slipped out, just like that, as Tommy Cooper would have said. But there you are.

These worries about government borrowing are understandable, but the message cannot be repeated often enough that, if the private sector is not spending, it is up to the public sector to fill the gap. This was the great insight Keynes brought to the world. For the public sector to cut back when the private sector is in such a panic only aggravates the crisis.

In order not to contrive an even bigger economic crisis, we have to grin and bear figures for public sector borrowing that were previously unimaginable - but, one hopes, with an eye on bringing the finances into better shape in the medium term.

Frankly, I am less worried about the level of government borrowing than I am about the exchange rate. Having conducted a campaign over many years in this column about the dangers to our competitiveness of an overvalued pound - Ed Balls once joked that, whatever the level of sterling, I always wanted it lower - I cannot but welcome the devaluation of the past couple of years, even though the damage to our manufacturing sector means there is less industry than there should have been to take advantage of it. 

Indeed, an earlier adjustment would have been far preferable to what actually took place. But what concerns me is the government's apparent insouciance towards further falls at a time when there is so much concern about protectionism.

Competitive devaluation was one of the "beggar my neighbour" policies that caused such damage in the 1930s. The further reduction in the Bank rate from 1.25 to 1% last week seems to me to have been unnecessary, given what has gone before and the time lags involved in monetary policy, and has given yet another signal that the UK does not care how far the pound falls.

This is dangerous territory, especially after the huge devaluation that has already taken place (30% within two years, at one point). It would be ironic if confidence collapsed completely and the Bank of England found itself having to introduce emergency increases in interest rates at a time of recession in order to ward off a complete rout of sterling.
