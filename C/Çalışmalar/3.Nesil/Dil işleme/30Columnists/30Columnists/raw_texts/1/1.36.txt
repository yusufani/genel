DO our MSPs crave to return to their childhood? Are they so nostalgic and dewy-eyed that they live in a romantic world where steam trains puffed through Border hills and the fat controller welcomed them into his subsidised station office?

What is it about politicians, and MSPs in particular that they have to have train sets, and not just a wooden one from Galt Toys, not just a Tri-ang Hornby or better still, a Marklin H0 gauge train set. No, they need to have the real thing, only it'ADVERTISEMENTs not their parents who have to stump up for it � it's us the taxpayer, and then some.

Next week the long-awaited re-laid Stirling to Dunfermline line will open � to great fanfares no doubt � with MSPs patting themselves in the back at what a fine job they've done.

And before anyone thinks of checking up, let me say, at the outset I supported the idea of the line. But my support was back in 2002 when we were being told it was going to cost �14m to reinstate the line. By the time it opens next week the bill will have climbed to a staggering �85m � making the original business case completely fanciful.

Be it building a parliament or a branch line, it would seem that MSPs just don't have a grip of public works or public spending.

And before someone thinks I've got it in for railways I need to remind readers that I, like many nerdy teenagers, was once a trainspotter. I love railways. I swoon at the smell of diesel engines or the faint memory of soot from steam engines. My grandfather was a bridge inspector in British Railways and I lived in a railway community beside St Margaret's loco sheds. Indeed, when I get the chance I will always choose the train over flying � it makes for a more comfortable, stress-free journey.

But I don't let this appeal for railways cloud my judgement. They are expensive to run and in many situations there are better alternatives to spend money on.

Before we rush to invest in the Waverley line down to Galashiels we should stop to look at how the costs of the Stirling to Dunfermline line were six times over the estimate and ask what will the real price of this Borders fantasy be?

The estimate has already gone from �110m to �150m and just recently went up to �235m. There is no possible business case for investing such public funds in a 35-mile line that will not be any quicker than driving � and once the Dalkeith by-pass is open will, for many, be slower!

It would be significantly cheaper to lay tarmac over the old tracks and allow buses and lorries exclusive use, with them able to service ALL the towns and villages they pass, than require significant engineering works for heavier trains that are going to run on a single track and have only one stop at Stow!

It just doesn't make sense, it doesn't add up and it will cost a small fortune. 

Politicians are shunting their responsibilities up a siding. They should get off the footplate and call a halt to this railway madness before we end up like the original builders of the Waverley line � bust!

Slippery slope
WHAT a fankle Wendy Alexander got herself into last week. A perfectly simple idea � and a good one � that Labour should agree in principle to a referendum on Scotland's continued participation in the success story that is the United Kingdom of Great Britain and Northern Ireland ended up as a summersault after a back flip following a triple salchow. And it wasn't a pretty sight.

Wendy, here's some advice for free. Keep it simple. Yes, the people should decide on Scotland's future, just as we should decide about the European constitution, but here's no need to complicate matters by talking about who's Bill will set the question. 

Having two brains is no excuse to show off, you only end up looking too clever by half, legs splayed and outfit torn on the political ice rink that is Holyrood.

Budget bribery
SO Alistair Darling has had to deliver a mini-budget, bribing his unruly backbenchers and the electorate before a crucial by-election at Crewe and Nantwich. 

All we need now is the International Monetary Fund to jet in and tell us our public finances are in a mess (and they are) and we'll be right back to the dear old Seventies when Great Britain was no longer considered great and our country was going to the dogs.

The �600 increase in the tax allowance is actually a sensible move � but it should have been announced when abolishing the 10p tax rate � not as a panic measure.

Nor should the Chancellor be borrowing to do it � making the public debt higher. But then Labour is in freefall just now and is desperate to find a parachute that works. 

Some people are saying Wendy couldn't lead a pack of Brownies. Well I wouldn't let Darling or Brown near the Cub Scouts either.
