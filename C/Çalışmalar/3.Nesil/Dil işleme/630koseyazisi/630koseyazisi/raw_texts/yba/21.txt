Karayolu kavgas� b�y�yor  
     
    
KKTC'de bug�nk� de�eriyle 23 trilyonluk D�rtyol-Ge�itkkale-Girne karayolu ihalesinin iptal karar�n�n uygulanmamas� �zerine �styap� �n�aat firmas� sahibi H�seyin G�ndo�du ile Karayollar� Genel M�d�r� Din�er Yi�it aras�ndaki kavga s�r�yor.

Bize y���nla mahkeme karar� ile ilgili bir s�r� belge g�nderen G�ndo�du, Yi�it'in bize yapt��� a��klamalar�n ger�ekd��� oldu�unu, baz� mahkeme kararlar�n� kendine yontarak yorumlamak istedi�ini s�yledi.

�haleden �nce bu i�in Kolin �n�aat'a %15-20 tenzilatla verilece�ini Yi�it'e bildirdiklerini, nitekim iddialar�n�n do�ru ��kt���n�, daha sonra onaylamamas� i�in kendisini uyard�klar�n� ancak yine ger�ekleri g�rmedi�ini anlatan G�ndo�du �unlar� s�yledi:

���haleyi iptal ederse makam�n�n tehlikeye girebilece�ini d���n�yordu. Bunun �zerine Ankara 8. �dare Mahkemesi'ne ihalenin iptali davas� a�t�k. Mahkeme bu konuda verdi�i kararda ��...�te yandan 22.12.2000 tarihinde yap�lan ihale saatinden �nce, ihalenin sonucuna ili�kin yaz�l� olarak yap�lan ihbarlardaki iddialar�n aynen ger�ekle�mesi yap�lan ihalede ku�kular olu�mas�na yol a�t���ndan bu durum ihale i�leminde g�d�lmesi gereken 'a��kl�k' ilkesini ihlal eder niteliktedir�� dedi. Gerek�ede ayr�ca gerekli rekabetin ve kamu yarar�n�n sa�lanmad��� ve ku�kular yaratt��� ifadesi yer ald�. Bu durumda iddiam�z�n nas�l as�ls�z olabildi�ini s�yleyebiliyor?��

REKABET ENGELLEND�

- Yi�it'in bize sundu�u Dan��tay 2. Dairesi ve Ankara C. Savc�l���'n�n kararlar� bir beraat midir?

- Hay�r tam tersine; 'yarg�lanmama' karar�d�r (bulundu�u makamdan �t�r�). Yani aklanm�� de�ildir.

- Peki bu durumda ne oldu?

- Daha ne olsun; kendini aklam�� gibi g�sterdi�i kararlardan sonra Ankara 8. �dare Mahkemesi'nin karar� ile ihaledeki ku�kular ortaya konulmu�, yap�lan uyar�lar�n dikkate al�nmad��� ortaya ��km��t�r. Ve ihale iptal edilmi�tir. Bir yolsuzluk olmasayd� ihale iptal edilir miydi? '�halede rekabetin engellendi�i, kamu yarar�n�n sa�lanmad���, yap�lan �ik�yetin dikkate al�nmad���' yarg� karar� ile tescil edilmesine ve devleti asgari 6.5 trilyon ger�ek zarara u�ratmas�na ra�men bu ki�inin halen nas�l bu g�revde kalabildi�idir. Burada 'Vurgun Operasyonu'ndan tutuklu bulunan Sedat Aban'�n 'Karayollar� ve �ller Bankas� Genel M�d�rlerinin koruyucu melekleri var' s�z�n� unutmamak gerekiyor. �ller Bankas� ve Yap� ��leri M�d�rleri gitti; Yi�it ise h�l� g�revde... 

- Bu durumda Bay�nd�rl�k Bakan� Akcan ne yapacak?

AKCAN �Z�N VERMED�

- �lk �nce Din�er'in sizi uyuttu�unu bilin. A��klamas�nda Dan��tay 10. Dairesi'nin y�r�tmeyi durdurma talebinin ret karar�n�n 8.4.2002'de Karayollar�'na tebli� edildi�ini beyan etti. Gecikmi� oldu�u 30 g�nl�k s�reyi kamufle etmek istedi. Ancak bu karar 2.3.2002'de tebli� olunmu�tu. Bir genel m�d�re bu yak���r m�? Say�n Akcan'a gelince... Adalet Bakanl���'na 7.5.2002 tarihinde g�nderdi�i yaz�da; Yi�it'i koruyarak savc�l��a yapt���m�z �ik�yetlere ra�men Kolin'in K�br�s'taki i�i s�rd�rmedi�ini belirterek, yarg�lama iznini vermedi. K�br�s noterli�inin tespitlerini g�rmezlikten geldi.

Din�er ve G�ndo�du aras�ndaki hukuk sava�� bitmeyecek gibi g�r�n�yor.



�� konu �� yan�t


�STYAPI firmas�n�n sahibi H�seyin G�ndo�du, Karayollar� Genel M�d�r�'n�n a��klamas�nda yer alan �� konu hakk�nda da ��yle konu�tu:

- Yi�it, firmam�z hakk�nda Ankara-Polatl� yolunu k�t� yapt���m�za dair su�lamalarda bulunup 'kapasitesi yetersiz' diyor. �rnek bir yol oldu�u konusunda iddial�y�m. Bize di�er kamu kurulu�lar�nca i� verilmedi�ini �ne s�r�yor. Biz ise ihalelere kat�ld���m�z� ya da idare ile devir s�zle�mesi imzalad���m�z� yarg�da tespit ettirince iddialar� itibars�z kald�. Bu yoldaki a��klamas�yla da sizi yan�ltmak istedi.

- DLH'n�n Ercan Havaalan� onar�m i�i ihalesine davet edilmedi�imizi iddia ederek, firmam�z� k���k d���rmeye �al���yor. Kendi hatam�zdan do�an basit bir belge eksikli�i y�z�nden yeterlik alamad���m�z� DLH'ya sorabilir. 1.2 trilyonluk gibi k���k bir i�e �styap� firmas�n�n yeterlilik alamamas� inand�r�c� m�d�r Din�er Bey... Kendisi bize kar�� husumet i�indedir.

- Ayd�n-Denizli otoyol ihalesinde, kredi niyet mektubunun CSFB bankas�n�n bilgisi dahilinde haz�rlanmad��� su�lamas�na gelince... Bu karalama kampanyas�n�n bir aya��d�r; b�yle bir mektup var m�d�r? Varsa niye ortaya ��kart�p gere�ini yapm�yorlar? Yi�it hakk�m�zda Ayd�n-Denizli otoyolu ile ilgili ithamlarda -yarg�da g�r��ece�iz- bulunurken, 4.1.2001 tarihinde 234 milyon dolarl�k bu ihalenin Bay�nd�r �n�aat'tan devir almam�z� bizi yeterli g�rerek izin veren ve s�zle�meyi imzalayan kendisi de�il midir?

Say�n Bayer biliniz ki, Din�er baz� konularda sizi yan�lt�yor. Fatih Altayl�'ya otoyol ve k�pr� zamlar�n� yarg� iptal ederken, bu nihai karar de�ildir diyor. Nihai karar� bekliyorum 


Anzaklar kadar duyarl� olal�m


T�RK ulusuna bir vatan kazand�ran ve b�y�k bir komutan� tarihe mal eden �anakkale Sava�lar�'n�n ge�ti�i b�lgeye kar�� ulus�a duyars�z kal�nmas� �ok �z�c�. Her 18 Mart'ta devlet taraf�ndan g�rkemli t�renler yap�l�yor, �anakkale Sava�lar�'nda can veren kahramanlar sayg�yla an�l�yor ama Gelibolu Yar�madas�'ndaki bu t�renlerde �lke insan�n� pek fazla g�rmek m�mk�n de�il...

T�RSAB Ba�kan� Ba�aran Ulusoy, 81 ilin gen�lerinin her 18 Mart'ta �anakkale'de toplanmalar�n� ve Gelibolu Yar�madas�'nda d�zenlenen anma t�renlerine kat�lmalar�n� �neriyor. Finansman�n da b�y�k firmalar taraf�ndan kar��lanaca��n� s�yl�yor.

Yaln�zca 18 Mart'ta de�il her zaman ve her yerde �anakkale Sava�lar�'nda can veren isimsiz kahramanlar� sayg�yla anmakta kusur etmemeliyiz. 
