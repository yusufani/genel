Siyasalla�an UEFA 

Avrupa'daki bir�ok �lkenin d��i�leri bakanl�klar�n�n, T�rkiye'ye yap�lacak seyahatlerin ertelenmesi tavsiyesi �zerine UEFA karar al�yor!.. 
-Al�nan mesnetsiz ve varsay�mlara dayal� kararla; G.Saray ve Be�ikta� kul�plerimiz ma�dur ediliyor. 
- Saha ve seyirci avantaj dengesi kayboluyor. 
-UEFA'ya olan, g�ven duygusu zedeleniyor. 
-�lkemizin prestiji sars�l�yor. �lkemizde s�k rastlanmayan ve m�nferit olay olan, bir-iki bomban�n ba��m�za a�t�klar�na bak�n�z. T�rkiye d���nda, y�llarca s�regelen ter�r eylemlerine ra�men, sportif faaliyetlerine ara vermeyen Avrupa, �lkemizde m�nferiden geli�en bir-iki olay i�in, evsahibi oldu�umuz ma�lar� ba�ka sahalara al�yor. UEFA, G.Saray ve Be�ikta� kul�plerimiz nezdinde, �lkemizi cezaland�r�yor. "G�n, bug�n; devir, bu devir" diyerek kenetlenmesi ve ivedi tedbirleri almas� gereken ki�iler ise, h�l� �ahsi m�lahazalarla, televizyon programlar�nda birbirlerini k�rmakla me�guller!.. Bu da yetmiyormu�cas�na, bir televizyon program�nda Be�ikta� Ba�kan Vekili Sn. H.G�reli, sahan�n de�i�mesinin, ciddi bir k�lfet getirmedi�ini �srarla savunuyor. Hayret ettim..! 
-UEFA'n�n bu karar�, Be�ikta� Kul�b�'n� deplasman gideri, seyahat, konaklama, saha kiras�, g�venlik �demesi gibi maddi ve manevi zahmetlerle kar�� kar��ya getirmiyor mu? 
-Bu, kurumun zarar� de�il mi? 
-�stelik, �lkemizin prestij kayb� hi� mi hesap edilmiyor? 
-UEFA, bu koca Cumhuriyet'i, bedevi devleti mi san�yor? Bunun b�yle olmad���n�, y�ce milletimiz bir g�n mutlaka ispat edecektir. 
