#include <stdio.h>
#include <stdlib.h>
int f(int*);
int g(int);
int main () {
	int a=10;
	int*p;
	p=&a;
	printf("p'nin gosterdigi yeri basar: %d\n",*p);
	printf("p'nin degerini yani,p'nin gosterdigi  yerin adresini basar %d\n",p);
	printf("a'nin degerini basar %d \n",a);
	printf("a'nin degerini basar : %d\n",&a);
	printf("p'nin adresini basar %d\n",&p);
	//Her dizi bir pointer ve her pointer bir dizidir
	//intd[80]*p1;
	//p1=d;
	/*Burada p1,d dizisinin ilk elemaninin adresiniin degerini al�r.Yani
	dizi adi,aslinda o dizinini hafizas�ndaki ba�lang�� adresini tutmaktad�r.
	d dizisinin 5. elemanina eeri�mek ise d[4] veya *(p1+4)
	*/ 
	/* mesela ben �grencilerin not ortalamasini hesaplayan bir program yazmak
	istiyorum fakat ayn� zamanda ka� �grenci oldugunu da ben girmek istiyorum yani
	en ba�ta tan�mlarken int ort[50] felan yapmak yerine oradaki 50 yi ben girmek istiyorum.
	Pointerlar burada devreye giriyor*/
	//int *p=(int*)malloc(sizeof(int)*a);//size of verilen bilginin kaplad��� yeri
	//a yi istedi�imizi yazabiliriz:D
	//malloc kullanmak i�in #include <stdlib.h>
	p= &a;
	printf("%d",*p);
	*p=20;
	printf("\n %d",a);
	int *q= (int*)malloc(sizeof(int)*a);
	q[3]=70;
	printf("\n %d",q[3]);
	f(&a);	
	printf("\n %d",a);
	g(a);	
	printf("\n %d",a);
}
int f (int * a){// call by reference
	*a=80;
}
int g(int a){// call by value
	a=90;
}
