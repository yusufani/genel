IT'S been a dreadful week for hearing the details of court cases involving multiple murderers. First there was Steve Wright being convicted of murdering five women in Ipswich last year. It is thought he may have been responsible for others going back as far as the early nineties. 

Then there was Levi Bellfield, found guilty of two murders and an attempted murder, and now suspected of 20 other similar attacks, including the murder of schoolgirl Milly Dowler.

With DNA now being so important in bringing criminals to justice itADVERTISEMENTis no surprise to hear � after such gory evidence � that some people wish to see a national DNA database established, keeping on file records of every individual living in Britain.

This would go far further than is the current practice, where in England and Wales anyone charged with a crime has their DNA kept irrespective of the outcome, while in Scotland we destroy it if an accused is acquitted.

In the same week, coincidentally, two British men took their case to the European Court of Human Rights in Strasbourg, claiming that having had their cases dropped they were now being discriminated against by the English authorities retaining their DNA profile.

But why not help the police this way? The issue comes right back to the presumption of innocence. We are all innocent until, beyond reasonable doubt, we are proven to be guilty. This assumption is because we in Britain shaped our law from the first principle that we are free people. 

Where limitations are placed upon us they are to say what we cannot do (such as infringe other people's liberties) rather than, as in many other countries, where the law expressly lists what you are allowed to do. 

Without that permission you have to presume you can't do it!

It's this presumption of innocence, of being free, that has caused us never to have a national database of fingerprints. Computers made the massive task possible long ago, but it is the unacceptable infringement of our liberties that stops politicians from taking such a step.

The people that think we should have our DNA taken and kept on file for the very small chance we might commit a serious crime where it might come in useful, are the same type of people who think it's not your money the Government spends, that everybody under the sun should have right of entry to your home and that the Bully State can tell you what to eat and drink and where to smoke.

Of course, having DNA records can help the police, and those of convicted criminals should be held � but the day we make it compulsory is the day we give up our presumption of innocence.

Mugabe watch out
"We shall send our armies in to free the people of Zimbabwe, and we shall call on Mugabe to resign. If he does not, we shall attack!"

Brave words, spoken with great passion and winning the overwhelming support of her audience. Hillary Clinton, maybe? 

No, a 16-year-old schoolgirl arguing to liberate the people of Zimbabwe through the use of military force. I was at the Botswana Schools Debating Championships on Saturday � and this girl won the award as best debater and her girls' team trounced all-comers to take the trophy � using English, her second language.

I tell you, she could teach MSPs a thing or two about how to put over an argument, clearly and rationally. Robert Mugabe should be glad she's still only a youngster � or his neighbour could be preparing for war.

Your card is marked
If ever anyone had any doubts that the control freaks who seek to govern our lives will not rest until they completely dictate what we can do, then the idea floated recently by Julian Le Grand of Health England that to buy tobacco you should need a permit, must surely settle the argument.

Not content with stopping people from smoking socially together in their local pub or club; not content with telling people that they can't smoke in their stationary car even if they are alone � if it's used as a part time mini-cab, no, now they want to hound smokers further.

Let's face it, they want to eat away at smokers' liberties until they have none. Next it will be the permit to buy alcohol and eventually the permit to buy meat. These people know no limit.

That's also how the identity card will be used, at first as a security measure, then as a control measure, combining it with your DNA, health files, bank records, car transponder � until even your simplest choices are controlled or restricted.

It may take 50 years, but it's coming unless we speak out. Thank goodness I won't be around to see it if it happens.
