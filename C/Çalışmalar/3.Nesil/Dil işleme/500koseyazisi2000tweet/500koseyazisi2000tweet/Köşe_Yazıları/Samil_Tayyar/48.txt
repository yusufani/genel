﻿Devlette neler oluyor?


Bir süredir tuhaf işler oluyor. Nasıl görmek veya okumak gerekir, gerçekten çok zorlanıyorum. Samimi olmak gerekirse, midem bulanıyor artık.

Çukurca’daki mayın faciasından sorumlu tutulan Hakkari Tugay Komutanı Tuğgeneral Zeki Es tutuklandı. İddialara ve delillere bakıldığında böyle bir kararın verilmesi, hiç kimse için sürpriz olmadı.

Garip olan, iddiaları örtbas etmekle suçlanan ve son YAŞ toplantısında terfi ettirilmeyen Hakkari Tümen Komutanı Tümgeneral Gürbüz Kaya’nın Askeri Yüksek İdare Mahkemesi’nde açtığı yürütmeyi durdurma davasını kazanmasıdır. Karara göre, ay sonuna kadar Korgeneral yapılması gerekiyor.

Biri Mamak Cezaevi’ne diğeri Korgeneral rütbesiyle karargaha...

Cumhuriyet tarihinin en önemli darbe davalarından Balyoz’da tutuklu sanık kalmadı, ifadeye çağrılan paşalar orduevlerinde saklandı, savcı talimatlarına uyulmadı, hiç kimsenin de gıkı çıkmadı, Silivri’de ise Ergenekon sanıkları gün sayıyor.

Oysa hem iddialar hem deliller, Balyoz’un Ergenekon’dan daha büyük bir dava olduğunu gösteriyordu.

Beyazlar dışarıya zenciler içeriye...

3 yıldır Silivri’de ömür tüketen Ergenekon sanıklarının yakarışları mahkeme duvarından öteye ulaşmazken, bir gün dahi içeri girmeyip hastanede sanık olmanın dayanılmaz ağırlığını yaşayan Mehmet Haberal yüzünden 9 hakime tazminat cezası verildi. Yargıdaki köklü teamülleri alt üst etme ve 58 bin tutuklu için emsal oluşturma pahasına olsa bile Haberal’ın hatırına hakimlere “tutukluluk tazminatı” yağdırıldı.

Haberal’ın Ergenekon sanığı olması nedeniyle Yargıtay’ın sevgisine mazhar olduğunu düşünüyorsanız, yanılırsınız. Şikayetçi Mustafa Balbay olsaydı sözgelimi, böyle bir karar çıkmazdı.

Üstatlara tazminat ödülü, alttakilere elma şekeri...

Yargıtay, hiçbir hakim veya savcıya göstermediği ilgiyi Erzincan Cumhuriyet Başsavcısı İlhan Cihaner’den esirgemedi. Kıran kırana savaştı, ses kayıtlarını tınlamadı, dosya olmadığı halde fotokopi üzerinden karar verip Cihaner’i serbest bıraktı. Cihaner, Haberal gibi bir ilkin kahramanıydı.

Yeni HSYK, Cihaner’i Adana’ya düz savcı

olarak atayarak sürece tepkisini gösterdi belki, gariptir tartışmaların merkezinde ve HSYK seçiminde YARSAV listesinin ilk sırasındaki Sincan Hakimi Osman Kaçmaz’a dokunan olmadı.
Direnenlere yeni adres, uzlaşanlara selam...

İstanbul Cumhuriyet Başsavcılığı, Özden Örnek’e ait olduğu iddia edilen darbe günlükleriyle ilgili dosyayı Ergenekon’la bağlantısı olmadığı gerekçesiyle Ankara’ya havale etti. Tüm fatura, 2009 Aralık başında açılan dosyaya yaklaşık 4 ay önce görevlendirilen Savcı Mehmet Ergül’e kesildi.

Komutanları sorgulayıp saatlerce ifadesini alan Başsavcıvekili Turan Çolakkadı ve Ergenekon savcılarının sorgulama sonrası şüphelileri mahkemeye sevk ederken tutuklanmalarına gerek görmemeleri çabucak unutuldu. İddianamelerin omurgasını oluşturan günlüklerle ilgili yaklaşım, daha ilk gün kötürümdü. Perşembenin gelişi çarşambadan belliydi.

Darbe günlüklerine özgürlük, Narnia günlüklerine pranga...

Bu ve benzeri o kadar çok örnek var ki, yazmaya kalksak satırlar yetmez. Ama ortak noktaları tek: cari olan üstünlerin hukuku...

Umarım, yeni dönem hukuk ihlallerinin asgari düzeye ineceği, herkesin hukuk önünde eşit olacağı bir dönem olur. Son hadiseler, mevzuatta yapılacak değişikliğin tek başına yeterli olmayacağını, köhnemiş zihniyet yargıdan tasfiye oluncaya kadar sancılı sürecin devam edeceğini gösteriyor.

Yaşananların basit analizini hukuki perspektiften bu şekilde birkaç cümleyle özetlemek mümkün, ancak Ergenekon, Balyoz ve darbe günlükleriyle ilgili sürecin gidişatına dair bazı ipuçlarını da son olayların perde aralıklarında görebiliriz.

Kürt meselesinde olduğu gibi burada da (sivil/asker) eylemsizlik sürecine bağlı olarak bir mutabakat ihtimalinin doğduğunu söylemek için erken midir, bilmiyorum. Önümüzdeki dönemin çok iyi takip edilmesi gerekir.

Devlet içinde bu yönde bir mutabakat oluşursa, darbe davaları minimize olur, zencilerden ibaret küçük hücrelere dönüştürülür, yeni tahliye kararları çıkar ve faturadaki rakam azalır.

Mutabakat oluşmazsa? Çatışmanın seyri, süreci tayin eder...