The Cox-2 anti-inflammatory Vioxx is like the Canadian Olympic team: every time it makes the news, it�s for the wrong reasons. Thus, a recent study found that people who take low-dose ASA along with Vioxx do not get the stomach protection against ulcer that Vioxx is supposed to offer. 
Why does that matter? 
Because a great many Vioxx users do exactly that, that is, they take a low-dose ASA in order to protect their hearts, and a Vioxx to protect their stomach, yet according to this and other studies, these people are wiping out at least one of the benefits of these drugs and perhaps the other as well. 
That�s because several studies have found that Vioxx may raise the risk of heart attack. 
The latest study to come to that conclusion is a large one from the Kaiser Permanente Group in the US that compared Vioxx users with Celebrex users (Celebrex is Vioxx�s main rival) and concluded that Vioxx users had a 50 % higher risk of heart attack than Celebrex users. >br>
Not only that, but this study also concluded that those who took the highest dose of Vioxx had 3 times the risk of heart attack of people taking standard anti-inflammatories. 
Bottom line: until more data comes forward to dispute these claims, if you�re taking Vioxx, you should ask your doctor - soon - why you�re on it instead of on some other anti-inflammatory. 

