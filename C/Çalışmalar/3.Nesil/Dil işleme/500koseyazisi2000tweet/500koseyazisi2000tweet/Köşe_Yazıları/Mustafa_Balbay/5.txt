﻿Yeni bir dönem...
1 Kasım seçimlerinin sonuçlarına genel bir çerçeveden baktığımızda bütün partiler açısından yeni bir dönemin başladığını söyleyebiliriz. Buna, şu anda iktidarda olan parti de dahildir. Dahildir, çünkü 7 Haziran’dan 1 Kasım’a giden süreçte yaşananlar sadece bu zaman diliminde olup bitmiş, bir daha konu edilmeyecek şeyler değildir. 
AKP, 7 Haziran seçimlerinin sonuçlarını beğenmedi, halka bu kararını düzelt dedi. Halk da iki HDP’den, üç MHP’den bulup buluşturup, bu kadar eklesek olur mu, karşılığını verdi. Normal demokrasilerde iktidar gücünü elinde bulunduranlar, halka kararını düzelt demezler. Derlerse halk onlara, sen kendini düzelt, der. 
Türkiye’de böyle olmadı. Şu oldu: 
7 Haziran seçimlerinin ardından bir terör belası Türkiye’nin üzerine çöktü. Silahlar konuşunca, bombalar patlayınca gerçekler susar. Gerçekleri haykıranların sesi bu gürültünün gölgesinde kalır. 
Terör, başkentin göbeğine kadar geldi, tarihin en büyük kıyımı yaşandı. 
Bu, AKP’ye yaradı! 
Medya üzerinde olağanüstü dönemlerde bile yaşanmayacak büyük bir baskı uygulandı, iktidar dünyanın en büyük medya patronu haline geldi. Türkiye’nin basın özgürlüğü notu dünyanın dört bir ucundan gazetecilerin bir dehşet mektubu kaleme almasını gerektirecek kadar dibe vurdu. 
Bu, AKP’ye yaradı! 
Suriye politikasının iflası yine 7 Haziran’dan sonra ayyuka çıktı. Rusya’nın devreye girmesiyle küresel anlamda politikamız çöktü. Sığınmacıların Ege’yi mezarlık haline getirmesiyle insanlık anlamında politikamız çöktü. Harcadığımız 7 milyar dolar anlamsız hale geldi. 
Bu, AKP’nin işine yaradı!

***

Bunlar AKP’nin işine yaradıktan sonra başka işine yarayanları söylemeye gerek var mı? Tabloyu AKP açısından irdelemeyi elbette sürdüreceğiz. 
Muhalefete geçelim... 
MHP’de Devlet Bahçeli’nin izlediği politikayı öteki partiler bir yana bu partinin mensupları da anlamakta güçlük çekiyordu. Sonuçlar gösteriyor ki MHP’ye oy verenler de anlamakta güçlük çekmiş. İktidarın korku ve dehşet senaryolarına oyla karşılık vermiş. 
HDP, AKP’nin devlet gücünü de elinde tutup kendisini terörize etmesine iyi bir karşılık veremedi. Diyarbakır, Suruç, Ankara hattındaki terör dalgası HDP’nin Türkiye partisi olma çabasını da dinamitledi. HDP bu dalgayı tam bir demokrasi ve parlamento söylemiyle aşabilirdi. Bunu sadece barajın biraz üzerine çıkabilecek kadar aşmış görünüyor. 
MHP ve HDP’de, 7 Haziran sürecini yönetme konusu beraberinde bir iç tartışma getirecektir.

***

CHP’nin konumunda 7 Haziran’a oranla çok büyük bir değişiklik yok. Beklenti, yüzde 30’a doğru ivmenin yükseleceği yönündeydi. 
Yukarıda, “AKP’nin işine yaradı” diye vurguladığımız acı gerçeklerin normal bir ülkede ana muhalefet partisini güçlendirmesi ve onu güçlü bir iktidar seçeneği haline getirmesi gerekirdi. 
Olmadı. 
Niçin? 
İşte CHP açısından bütün düğüm burada. Siyasete de uyarlanabilecek şöyle bir söz var: 
Ne söylersen söyle, söylediğin karşındakinin anladığı kadardır. 
Bu anlamda dünyanın en büyük doğrularını söyleseniz bile halk bunu inandırıcı bulmuyorsa, halkın yerine başka bir halk ithal edemeyeceğimize göre yöntemi ve söylemi değiştirmek gerekir. 
CHP, kırıp dökmeden konuşabilmeyi bir ölçüde öğrendi. 
Ortada kahredecek, pes edecek, moral bozacak bir durum yok. Her türlü faşizan yöntemle iktidar gücünü elinde tutan bir yapıya karşı halkın 4’te 1’inin oyunu elinde tutan bir partinin kendisini yenileyip, azımsanmayacak mevcut gücü katlamanın yollarını bulması gerekir. 
Olağan kongre süreci CHP için büyük bir fırsattır.