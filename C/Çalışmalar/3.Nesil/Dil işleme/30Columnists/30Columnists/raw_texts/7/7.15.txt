Now that Jacqui Smith's chums have told the world that the home secretary's husband will be "sleeping on the sofa for a while" I think it's probably time to draw a veil over the Smiths' love life for few days.

Not that a total news blackout from here to polling day would save her now from the electorate's gathering wrath.

But heaven knows how many adult videos Richard Timney may feel the need to rent when he's camping on the sofa in Redditch, unable to get a good night's sleep upstairs and the boss away most nights, sleeping in her sister's �116,000 spare room in south London. Mrs Timney spends most nights there, it now transpires, as she fights to justify her expense claims to the parliamentary watchdog.

Long gone are the days when senior ministers lived in grand style, usually on their own money, earned or inherited, though the tabloids treat all MPs as if they were rich, which most aren't. In government? Apart from Lords Drayson and Myners I can't think of any.

In fact the Smiths sound like a lot of people nowadays, who have to work away from home or get divorced and have to sleep in spare rooms and on sofas.

One big difference is that theirs is a separation caused by Smith's election to parliament in the Blair landslide of 1997 and her subsequent string of promotions, leading to what is now definitely looking like overpromotion by Gordon Brown in 2007.

It may have been difficult for Timney to find himself at home acting the househusband, though it happens to many men nowadays. It may provide an explanation for why he may have rented those videos (why do people call them "adult" when they're actually more likely to be "adolescent"?), but it doesn't provide an excuse for charging them to the public purse.

I've tried to think what might have happened. Perhaps it was the kids who rented those dirty movies while Dad was doing Mum's constituency email ... but even then Timney shouldn't have put them on expenses; he's a Blair babe's helpmate, a Blair boy. He should know better.

Careless? Silly? Stupid? Greedy? Grossly insensitive? Illegal? You take you pick of available adjectives. That's the other big difference. Much of the Smiths' lifestyle, including Timney's �40,000 salary as Smith's parliamentary assistant (sic), is paid for by the taxpayer. Surely, he could have afford to rent his own dirty videos, the taxpayer is entitled to ask. There is only one apposite word for this behaviour. I fear it rhymes with banker.

Anyway, condemnation is the easy bit. We can all do that and even Richard Littlejohn managed to thump the barn door a few times in today's Mail. And I assume that the good people of Redditch, where her majority was 2,716 in 2005, will pass an unfavourable judgment when the time comes.

It's their decision. Brown could act first in any reshuffle he cares to make later this year, though somehow my instinct is that he won't admit his error. He phoned her at the weekend to confirm his support, admirably loyal, not necessarily appropriate in the circumstances.

Under New Labour tradition Alistair Darling is normally sent into departments to calm down the mess made by others. So I suppose he could be called upon again if Brown wants a new home secretary and another chancellor who will be more compliant. I have no scrap of evidence or even hearsay to support this outlandish theory.

The larger question is: what do we do about it? MPs' pay has been a mess for as long as I can remember and these wretched allowances were intended 20 years ago to provide a soft alternative to higher salaries.

Though many MPs claim modestly � some little or nothing � from the additional costs allowance for housing, others have behaved badly. The same is true in banking: indeed in most walks of life. And if you want to be reminded just how foolishly distinguished people can be, listen to Clive James's astonishing Radio 4 tale of the fall of Marcus Einfeld, a distinguished Australian judge now in jail over a speeding ticket.

The theoretical answer to the exes row lies in the promised review of MPs' allowances by the committee on standards in public life, which will report after the election � the best time for a new administration to act. It would propose higher salaries and fewer allowances. Ideally, there might also be fewer MPs, which would help fund the deal.

None of it is likely to happen. Why? Because it would be difficult. There will be a recession on, public debt is high, voters in employment will be paying higher taxes, those jobless will be enduring hard times. It is never a good time to address MPs' pay in an adult (in the proper sense) way � which is why we are in this mess.

We are in a similar mess over the funding of political parties: voters dislike all available options. The motormouth who suggested in Saturday's Times that MPs should not be paid at all was describing a recipe for electing rich men � Sir Fred Goodwin for chancellor perhaps? � and hair-shirted fanatics, in some cases both at once. Best not to go there, so we will stagger on.

The other larger question is: what are revelations like this, tawdry and depressing in a small way, doing to the public realm? The theory behind greater openness and transparency is that it will sweep away the secrets and the cobwebs and restore trust and confidence.

The practice is just the opposite: it corrodes trust even further. "They're all as bad as each other" is the wrong conclusion. They're not.

That is our fault. One further illuminating aspect of the affair, which is unlikely to be illuminated much, is how the famous video invoice from Virgin TV and subsequent Commons invoice came to be published in the Mail on Sunday, leading exponents of this form of expose-cum-chequebook journalism.

The gossip at Westminster is that someone in the fees office (which handles such claims) is leaking this sort of stuff. It is claimed that the Daily Express had the story first, not something we hear every day.

Whatever the truth, the same parliamentary fees office has been nodding through incautious expense claims from MPs, ones that might as sensibly have been challenged as Goodwin's pension. Laxity and leaks are as an unattractive a combination as dirty movies and middle-aged parliamentary assistants.
