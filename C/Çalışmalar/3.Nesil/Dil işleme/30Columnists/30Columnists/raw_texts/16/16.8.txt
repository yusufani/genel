Of all the advice parents give to children heading off to college, warnings about alcohol � and especially about abusing alcohol � may be the most important. At most colleges, whether and how much students drink can make an enormous difference, not just in how well they do in school, but even whether they live or die.


Every state has a minimum drinking age of 21, and the vast majority of college students are younger than that. Yet drinking, and in particular drinking to get drunk, remains a major health and social problem on campuses. Car crashes and other accidental injuries, sexual assaults, fights, community violence, academic failure and deaths from an overdose of alcohol are among the consequences. 

College students spend about $5.5 billion a year on alcohol, more than they spend on books, soft drinks and other beverages combined. Alcohol is a factor in the deaths of about 1,700 college students each year. 

The consequences can be particularly severe when people binge drink, a drinking pattern adopted by 44 percent of college students, national surveys have shown. Binge drinking is defined as consuming five or more drinks for men or four or more for women in a row, usually within two hours.

�Most alcohol-related harms experienced by college students occur among drinkers captured by the five/four measure of consumption,� Henry Wechsler of the Harvard School of Public Health and Toben F. Nelson of the University of Minnesota wrote in July in The Journal of Studies on Alcohol and Drugs. 

A petition circulating among college presidents seeks to lower the drinking age to 18 on the theory that it would reduce the number of students who binge drink beyond the boundaries of college campuses. But opponents say there is no hard evidence for this belief and a better plan would be to change the drinking culture on campus.

About half of college binge drinkers arrive on campus having engaged in similar behavior in high school; an equal number acquire this behavior in college, Elissa R. Weitzman of Harvard and colleagues reported.

Every year, tens of thousands of college students wind up in emergency rooms suffering from the life-threatening effects of alcohol intoxication. And every year, about a dozen students, including some of the best and brightest and most athletically talented, die from acute alcohol poisoning. In one study of students who suffered alcohol-related injuries, 21 percent reported consuming eight or more drinks in a row.

Although Greek houses, which have the highest rates of binge drinking, are infamous for a free-flowing alcohol culture, studies have found that student athletes and sports fans are also among the heaviest drinkers, often gathering to drink to oblivion after an athletic event.

A Community Approach

A concerted effort has been made in the last decade to define the factors that prompt binge drinking on campuses and devise effective methods to combat it. What has become most obvious to researchers is that colleges cannot achieve this on their own.

�Basically, having programs to reduce binge drinking on college campuses in the absence of broad-based community interventions to do likewise may be a bit like rearranging deck chairs on the Titanic,� said Dr. Timothy S. Naimi of the Centers for Disease Control and Prevention.

The Harvard School of Public Health College Alcohol Study, which began in 1993, has identified several environmental and community factors that encourage binge drinking. Dr. Wechsler, who directed the study, said in an interview that high-volume alcohol sales, for example, and promotions in bars around campuses encourage drinking to excess.

�Some sell alcohol in large containers, fishbowls and pitchers,� he said. �There are special promotions: women�s nights where the women can drink free; 25-cent beers; two drinks for the price of one; and gut-busters, where people can drink all they want for one price until they have to go to the bathroom. Sites with these kinds of promotions have more binge drinking.

�Price is an issue,� he added. �It can be cheaper to get drunk on the weekend than to go to a movie.� 

Although it is a college�s duty to educate students about the effects of alcohol and the risks of drinking too much, �education by itself doesn�t work,� Dr. Wechsler said. �You must attack the supply side as well as the demand side.�

More than half the alcohol outlets surrounding colleges that participated in the Harvard study offered promotions with price discounts, and nearly three-fourths that served alcohol on the premises had price discounts on weekends. 

The study found that the sites of heaviest drinking by college students were off-campus bars and parties held off-campus and at fraternity and sorority houses. 

