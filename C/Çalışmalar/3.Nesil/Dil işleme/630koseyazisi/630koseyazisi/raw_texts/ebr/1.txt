�ocuktan al haberi

Sabahtan beri gazeteleri m�nc�k m�nc�k okudum.

Atat�rk��n hangi tak�m� tuttu�u, �ebnem Schaefer��n bek�ret raporu, Fener-PSV ma��, kad�nlara �zel cami a��ls�n m� a��lmas�n m� tart��mas�, Abdullah G�l��n t�rban� kar�s�n�n hatr�na savundu�unu s�ylemesi, CIA�in �imdilerde t�rmalayan hurmalar�...

Her hadise, ayr� gazetelerin, ayr� yazarlar�n�n perspektifinden, ayr� ayr�...

Haber, salt haber de�il art�k ��nk�. Haberi nereden ald���n�z da ayn� oranda ehemmiyet ta��yor. �yle ya azizim, herkesin bir duru�u var.

Politika, spor, seks... Hayat denen oyunda, herkes kendine g�re bir pozisyon al�yor. �stelik, hayat, te�bihte hata olmaz, bir ma�sa e�er, kimileri a��r faull�, sakatlamaya niyetli faull� oynuyor. 

Allah�tan hayat�n her �eye ra�men ola�an�st�, m�ltefit bir y�z� de var. En beklemedi�i anda huzurda bir mucize �eklinde belirebiliyor.

En iyi arkada��m�n annesi -ki hem ikinci annem addederim kendisini, hem de en iyi arkada�lar�mdan biri- bir g�nl���ne �stanbul�a gelmi�.

G�zlerimin i�ine, mavi-gri g�zleriyle, �ifa verir gibi, en i�inden g�lerek bakt� ve esasta neyin �nemli oldu�unu hat�rlatt�:

�nsan�n ufkunu, ge�en y�llarla geni�letece�ine daraltt���n�... Oysa buna hi� de gerek olmad���n�...

Bunun omurgaya zarar verdi�ini... Oysa omurga denen �eyin, dimdik tutmay� becerdi�inde, nefes al�r gibi nefes al�p verebilmene imk�n tan�d���n�...

Dandik bir poz�r olmaktansa post�r� dik tutman�n sa�l��a fayda sa�lad���n�...

�ocuklu�umu, ne bekledi�imi bilmeden bekleyip durdu�um uzuuun bir can s�k�nt�s� olarak hat�rlar�m. G�len bir foto�raf�m neredeyse yok denecek azd�r �ocuklu�umdan kalma...

�imdi anl�yorum ki oysa, can s�k�nt�s�yla ba� etmeyi en iyi bildi�im d�nemmi� o.

A�a�lara ��karak, �eteler kurarak, okuyarak, resim yaparak, oyunlar icat ederek...

Bir yandan da ne kadar zamand�r i�ten bir kahkaha atmad���m�...

Oysa ��kretmek l�z�m ve hakikaten i�kembeden de�il, omurgan dik oldu�u i�in ci�erden kahkaha atabilmek...

Olmaya devlet cihanda bir nefes s�hhat gibi derler ya. Ve bir kahkaha, bir pirzola... B�nyeye g�lmek gerek...

Yogaya ba�lamal�... Ahmet G�ntan��n can�m �iiri Ormanlar�n G�mb�rt�s��ndeki gibi �zerinde d�nya haritas� olan bir uyku tulumuyla uyumal�. M�zi�i daha iyi bir kulakla dinleyebilmek ve sporu daha iyi bir g�zle izleyebilmek i�in matematik �al��mal�. Ve �espri� diye dayat�lan andavall�klara, evet, kesinlikle, tepeden ve yine de g�lerek bakmal�. Kendi esprini �reterek yani... �ocuk�a bir safiyet ve gaddarl�kla, gerekirse alay ederek. Dalgan� ge�ersin, o olur... (�Dalga dedi�in denizde olur� diye giden bir espri noksan� deyi� de vard�r ya hani. �ocukken b�y�y�nce ne olmak istedi�imi sorduklar�nda, yunus bal��� diye cevap verirdim. Reenkarnasyon diye bir �ey varsa, benim bir sonraki hayat�m i�in dile�im budur.)

O zaman, insan�n gen� de �lse ihtiyar da, mutlu �lmesi m�mk�nd�r.

Oyundan zevk almaya bakmal� belki. Tek i�i denizde z�play�p hoplay�p tiz kahkahalar atan yunuslar gibi...

Ha, bir de tabii, bu arada, k�pekbal�klar�n� da fena d�ver yunuslar. Bir de o var...

Birka� g�nd�r, �ebnem Schaefer��n bek�ret raporunu bilemeyece�im, benim kafam, �kafa kar���kl��� raporu� gibi �al���yor. Kusura bakmay�n yani daldan dala atlayan bir muhabbete sard�ysam.

Demem odur ki: Oyun oynarken e�lenmekte fayda var. Sa�lam bilgidir. �ocukluktan kalma bir bilgi. Hem omurgaya da iyi gelir.

B�t�n �ocuklara hararetle tavsiye edilir... �ocuklar ki tarih kadar ihtiyard�rlar...
