#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <math.h>
float fonksiyon(float [],int,float);
float turev(float[],int,float);
int isaret_degisimi(float,float);
int main(){
	system("title Sayisal Analiz Araci");
	int secim,t,x,i,j,z,satir,sutun,n;
	int derece,flag;
	float katsayi[10];
	float a,fa=0.0,b,c,fb=0.0,p=1.0,epsilon,fcold,fc,delta,d,h;
	printf("***Sayisal Analiz Araci'na hosgeldiniz***\n");
	printf("<1>Grafik Yontemi\n<2>Yariya Bolme Yontemi \n<3>Regula Falsi Yontemi\n<4>Newton-Raphson Yontemi\n<5>Matrisin inversini(tersinin) alma\n<6>Gauss Eliminasyon veya Gauss Jordan Yontemi\n<7>Trapez Yontemi\n<8>Simpson Yontemi\n");
	printf("Lutfen istediginiz yontemin numarasini tuslayiniz:");
	secim=getch();
	int menu=1;
	while(menu=1){
	switch (secim){
		case 77:{	
			system("CLS");
			printf("***Sayisal Analiz Araci'na hosgeldiniz***\n");
			printf("<1>Grafik Yontemi\n<2>Yariya Bolme Yontemi \n<3>Regula Falsi Yontemi\n<4>Newton-Raphson Yontemi\n<5>Matrisin inversini(tersinin) alma\n<6>Gauss Eliminasyon veya Gauss Jordan Yontemi\n<7>Trapez Yontemi\n<8>Simpson Yontemi\n");
			printf("Lutfen istediginiz yontemin numarasini tuslayiniz:");
			secim=getch();
			break;
		}
		case 49:{
			system("CLS");
			printf("-Grafik Yontemi-\n");
			printf("Lutfen fonksiyonun derecesini giriniz:");
			scanf("%d",&derece);
			for(i=0;i<=derece;i++){
				printf("Lutfen x^(%d)'li terimin katsayisini giriniz:",derece-i,i+1);
				scanf("%f",&katsayi[i]);
			}
			printf("Lutfen bir baslangic degeri giriniz:");
			scanf("%f",&a);
			printf("Lutfen bir delta(2 kok arasindaki artis miktari) degeri giriniz:");
			scanf("%f",&delta);
			printf("Lutfen bir epsilon degeri giriniz:");
			scanf("%f",&epsilon);
			printf("");
			c=a+delta;
			fa=fonksiyon(katsayi,derece,a);
			fc=fonksiyon(katsayi,derece,c);
			while(fabs(fa-fc)>epsilon){
				printf("x=%f,x+delta=%f,f(x)=%f,f(x+delta)=%f\n",a,c,fa,fc);
				if(isaret_degisimi(fa,fc)==1){
					delta/=2;
					c=a+delta;
				}
				else {
					a=c;
					c=c+delta;
				}
				fa=fonksiyon(katsayi,derece,a);
				fc=fonksiyon(katsayi,derece,c);
			}
			printf("Kokumuz:%f",a);
			printf("\nMenuye donmek icin 'M' veya 'm' tuslayiniz .Yariya bolme yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:");
			secim=getch();
			if((secim==109) || (secim==77)) secim=77;
			else secim=49;		
			break;
		}
		case 50:{
			system("CLS");
			printf("-Yariya Bolme Yontemi-\n");
			printf("Lutfen fonksiyonun derecesini giriniz:");
			scanf("%d",&derece);
			for(i=0;i<=derece;i++){
				printf("Lutfen x^(%d)'li terimin katsayisini giriniz:",derece-i,i+1);
				scanf("%f",&katsayi[i]);
			}
			printf("Lutfen bir a(ilk deger) degeri giriniz:");
			scanf("%f",&a);
			printf("Lutfen bir b(son deger) degeri giriniz:");
			scanf("%f",&b);
			printf("Lutfen bir epsilon degeri giriniz:");
			scanf("%f",&epsilon);
			fa=fonksiyon(katsayi,derece,a);
			fb=fonksiyon(katsayi,derece,b);
			printf("Ilk degerler icin\nF(a)=%f F(b)=%f",fa,fb);
			if(fa*fb<0.0000){
				c=(a+b)/2;
				fc=fonksiyon(katsayi,derece,c);
				if((fc==0) || (fabs(fc)<=epsilon)) {	
				printf("Kokumuz:%f\n",c);
				}
				else{
					fcold=0;
					flag=0;
					while((fabs(fc))>epsilon){//Konu anlat�m kitab�ndaki de�ere g�re degil!!!!			
						printf("\n f(c)=%f f(b)=%f f(a)=%f",fc,fb,fa);
						if((fa*fc)<0){
							b=c;
							fb=fonksiyon(katsayi,derece,b);
						}
						if((fb*fc)<0){
							a=c;
							fa=fonksiyon(katsayi,derece,a);
						}
						c=(a+b)/2;	
						fcold=fc;
						fc=fonksiyon(katsayi,derece,c);
					}
					printf("Kokumuz:%f",c);
					printf("\nMenuye donmek icin 'M' veya 'm' tuslayiniz .Yariya bolme yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:");
					secim=getch();
					if((secim==109) || (secim==77)) secim=77;
		        	else secim=50;
					break;
				}
			}
			else{
				if(fa*fb>0.00000) printf("Kok yoktur");
				if((fa==0.00000) && (fb==0.00000)){
					printf("\na ve b cakisik kokdur ve degeri %f'dir",a);
					getch();
					break;
				}
				else{				
					if(fa==0.00000) printf("\na koktur ve degeri:%f'dir",a);
					if(fb==0.00000) printf("\nb koktur ve degeri:%f'dir",b);
				}
			}
				printf("\nMenuye donmek icin 'M' veya 'm' tuslayiniz .Yariya bolme yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:");
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
		    	else secim=50;
				break;
			}
		case 51:{
				system("CLS");
				printf("-Regula Falsi Yontemi-\n");
				printf("Lutfen fonksiyonun derecesini giriniz:");
				scanf("%d",&derece);
				for(i=0;i<=derece;i++){
					printf("Lutfen x^(%d)'li terimin katsayisini giriniz:",derece-i,i+1);
					scanf("%f",&katsayi[i]);
				}
				printf("Lutfen bir a degeri giriniz:");
				scanf("%f",&a);
				printf("Lutfen bir b degeri giriniz:");
				scanf("%f",&b);
				printf("Lutfen onemsenmeyecek bir epsilon degeri giriniz:");
				scanf("%f",&epsilon);
				fa=fonksiyon(katsayi,derece,a);
				fb=fonksiyon(katsayi,derece,b);
				printf("fa icin %f   fbicin %f",fa,fb);
				if(fa*fb<0.0000){
					float c,fc;
					c=((b*fa)-(a*fb))/(fa-fb);
					fc=fonksiyon(katsayi,derece,c);
					if((fc==0) || (fabs(fc)<=epsilon)) {	
						printf("Kokumuz %f",c);
						printf("FCDEGER� %f",fc);
					}
					else{
						fcold=0;
						while(fabs(fc)>epsilon){//Konu anlat�m kitab�ndaki de�ere g�re degil!!!!				
							printf("\nfc=%f,fb=%f,fa=%f\n",fc,fb,fa);
							if((fa*fc)<0){
								b=c;
								fb=fonksiyon(katsayi,derece,b);
							}
							if((fb*fc)<0){
								a=c;
								fa=fonksiyon(katsayi,derece,a);
							}
							c=((b*fa)-(a*fb))/(fa-fb);
							printf("\na=%f b=%f\n",a,b);
							printf("c=%f",c);	
							fc=fonksiyon(katsayi,derece,c);
						}
					printf("Kokumuz %f",c);
					}
				}
				else{
					if(fa*fb>0.00000) printf("Kok yoktur");
					if((fa==0.00000) && (fb==0.00000)){
						printf("a ve b cakisik kokdur ve degeri %f'dir",a);
						getch();
						break;
					}
					else{				
						if(fa==0.00000) printf("a koktur ve degeri:%f'dir",a);
						if(fb==0.00000) printf("b koktur ve degeri:%f'dir",b);
					}
					}
				printf("\nMenuye donmek icin M/m.Yariya bolme yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:");
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
			    else secim=51;
				break;
			}
		case 52:{
				system("CLS");
				printf("-Newton-Raphson Yontemi-\n");
				printf("Lutfen fonksiyonun derecesini giriniz:");
				scanf("%d",&derece);
				for(i=0;i<=derece;i++){
					printf("Lutfen x^(%d)'li terimin katsayisini giriniz:",derece-i,i+1);
					scanf("%f",&katsayi[i]);
				}
				printf("Lutfen bir baslangic x degeri giriniz:");
				scanf("%f",&a);// x i a da tuttum ki degisken harcamay�m diyer!!!!!!!!!!!!!!!!! b de x1 olsun
				printf("Lutfen onemsenmeyecek bir epsilon degeri giriniz:");
				scanf("%f",&epsilon);
				b=a;
				do{
					printf("a=%f,b=%f,f(a)=%f,f(b)=%f\n",a,b,fa,fb);	
					a=b;
					fa=fonksiyon(katsayi,derece,a);
					fb=turev(katsayi,derece,a);
					b=a-(fa/fb);
				}while(fabs(fa)>epsilon);
				printf("Kokumuz:%f",a);
				printf("\nMenuye donmek icin M/m.Yariya bolme yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:");
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
		     	else secim=52;
				break;
			}
		case 53:{
				system("CLS");
				int flag,k;
				float kat,tmp,det;
				printf("Bu program matrisin Inversini alir \nLutfen Inversi alinacak matrisin kenar sayisini giriniz: ");
				scanf("%d",&satir);
				float A[satir][satir],b[satir][satir],deta[satir][satir];
				for (i=0;i<satir;i++){
       				for(j=0;j<satir;j++){
            			printf("\nLutfen matrisin %d.satirdaki %d.sutundaki terimi giriniz\n A[%d][%d]=",i+1,j+1,i+1,j+1);
            			scanf("%f",&A[i][j]);
            			deta[i][j]=A[i][j];// Bu determinant al�rken matrisi bozmamak i�in
        			}
    			}
    			i=0;
    			flag=1;
    			while(i<satir && flag==1){
        			if(deta[i][i]==0){
            			k=i;
            			while(deta[k][i]==0 && k<satir){
                		k++;
            			}
            			if (k==satir){
                		flag=0;
            			}
						else{
                			for (j=0;j<satir;j++){
                    		tmp=deta[i][j];
                    		deta[i][j]=deta[k][j];
                    		deta[k][j]=tmp;
                			}
            			}
            			i++;
        			}
        			else i=satir+1;
        		}
        		for(i=0;i<satir;i++){
        			d=deta[i][i];
        			for(x=i+1;x<satir;x++){
        				j=i;
        				kat=deta[x][j]/d;
        				for(j=0;j<3;j++){
        					deta[x][j]=deta[x][j]-(kat*deta[i][j]);
						}
					}
				}// determinant matirisini buraya kadar olu�turduk.
				for(i=0;i<satir;i++){
					for(j=0;j<satir;j++){
						if(i==j) b[i][j]=1;
						else b[i][j]=0;
					}
				}
				for(i=0;i<satir;i++){
					d=A[i][i];
					for(j=0;j<satir;j++){
						A[i][j]=A[i][j]/d;
						b[i][j]=b[i][j]/d;
					}
					for(int x=0;x<satir;x++){
						if(x!=i){
							k=A[x][i];
							for(j=0;j<satir;j++){
								A[x][j]=A[x][j]-(A[i][j]*k);
								b[x][j]=b[x][j]-(b[i][j]*k);				
							}
						}
					}
				}// buraya kadartersini ald�k
				det=1;
				for(i=0;i<satir;i++){
					det=deta[i][i]*det;
				}
				if(det==0) printf("Matrisin determinanti 0 dir.Bu yuzden tersi yoktur.");
				else printf("Girdiginiz matrisin determinanti =%f\n",det);
				if (det!=0){
					for (i=0;i<satir;i++){
        				for(j=0;j<satir;j++){
            				printf("b[%d][%d] : %f  ",i,j,b[i][j]);
        				}
        				printf("\n");
    				}
    			}
				printf("\nMenuye donmek icin M/m.Matrisin Inversini tekrar almak icin herhangi bir tusu tuslayiniz:");
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
			    else secim=53;
				break;
			}
		case 54 :{
			system("CLS");
			printf("Keyfiniz hangisini secmek ister?\n<1> Gauss Eliminasyon\n<2>Gauss Jordan\n");
			scanf("%d",&i);
			if(i==1){
				system("CLS");
				printf("-Gauss Eliminasyon-\n");
				printf("Lutfen matrisin satir sayisini giriniz: ");
				scanf("%d",&satir);
				printf("Lutfen matrisin sutun sayisini giriniz: ");
				scanf("%d",&sutun);
				float matris[satir][sutun],sonuc[satir];
				for (i=0;i<satir;i++){
       				for(j=0;j<sutun;j++){
            			printf("\nLutfen matrisin %d.satirdaki %d.sutundaki terimi giriniz\n A[%d][%d]=",i+1,j+1,i+1,j+1);
            			scanf("%f",&matris[i][j]);
        			}
    			}
    			for(i=0;i<satir;i++){
    				printf("Lutfen sonuclar matrisinin %d. elemanini giriniz:",i+1);
    				scanf("%f",&sonuc[i]);
				}
				for (i=0;i<satir;i++){
					d=matris[i][i];
					sonuc[i]/=d;
       				for(j=0;j<sutun;j++){
            			matris[i][j]/=d;
        			}
        			if(i!=satir-1){
        				for(x=i+1;x<sutun;x++){
        					a=matris[x][i];
        					for(j=0;j<satir;j++){
        						matris[x][j]=matris[x][j]/a;
        						matris[x][j]=matris[x][j]-matris[i][j];
        						matris[x][j]=matris[x][j]*a;
							}
							sonuc[x]/=a;
							sonuc[x]-=sonuc[i];
							sonuc[x]*=a;
						}
    				}
			}
		
			for(i=satir-1;i>=0;i--){
				for(j=sutun-1;j>i;j--){
					sonuc[i]=sonuc[i]-matris[i][j]*sonuc[j];
				}
			}
			for(i=0;i<satir;i++){
				printf("%d.kokumuz=%f\n",i+1,sonuc[i]);
			}
			printf("\nMenuye donmek icin M/m.Gauss eliminasyon veya Gauss Jordan yontemini tekrar denemek icin herhangi bir tusu tuslayiniz:");
			secim=getch();
			if((secim==109) || (secim==77)) secim=77;
			else secim=54;
			break;	
		}
		if(i==2){
				system("CLS");
				printf("-Gauss Jordan-\n");
				printf("Lutfen matrisin satir sayisini giriniz: ");
				scanf("%d",&satir);
				printf("Lutfen matrisin sutun sayisini giriniz: ");
				scanf("%d",&sutun);
				float matris[satir][sutun],sonuc[satir];
				for (i=0;i<satir;i++){
       				for(j=0;j<sutun;j++){
            			printf("\nLutfen matrisin %d.satirdaki %d.sutundaki terimi giriniz\n A[%d][%d]=",i+1,j+1,i+1,j+1);
            			scanf("%f",&matris[i][j]);
        			}
    			}
    			for(i=0;i<satir;i++){
    				printf("Lutfen sonuclar matrisinin %d. elemanini giriniz:",i+1);
    				scanf("%f",&sonuc[i]);
				}
				for (i=0;i<satir;i++){
					d=matris[i][i];
					sonuc[i]/=d;
       				for(j=0;j<sutun;j++){
            			matris[i][j]/=d;
        			}
        			if(i!=satir-1){
        				for(x=i+1;x<sutun;x++){
        					a=matris[x][i];
        					for(j=0;j<satir;j++){
        						matris[x][j]=matris[x][j]/a;
        						matris[x][j]=matris[x][j]-matris[i][j];
        						matris[x][j]=matris[x][j]*a;
							}
							sonuc[x]/=a;
							sonuc[x]-=sonuc[i];
							sonuc[x]*=a;
						}
    				}
			}
			for(z=satir-1;z>=1;z--){//z bir sonraki sola gecmek i�in mesela en sa� sutunu s�f�rlad�m s�ra bir solundaki gibi
			for(i=satir-2+z-(satir-1);i>=0;i--){
				x=1;
				for(j=sutun-1+z-(satir-1);j>=0;j--){
					if(x==1) d=matris[i][j];
					matris[i][j]=matris[i][j]-(d*matris[z][j]);
					x=0;
				}
				sonuc[i]=sonuc[i]-(d*sonuc[z]);
			}
		}
		printf("Koklerimiz sirayla:\n");
		for(i=0;i<satir;i++) printf("x%d=%f\n",i+1,sonuc[i]);
		printf("\nMenuye donmek icin M/m.Gauss eliminasyon veya Gauss Jordan yontemini tekrar denemek icin herhangi bir tusu tuslayiniz:");
		secim=getch();
		if((secim==109) || (secim==77)) secim=77;
		else secim=54;		
	break;	
	}
	case 55:{
		system("CLS");
		printf("-Trapez Yontemi-\n");
		printf("Lutfen fonksiyonun derecesini giriniz:");
		scanf("%d",&derece);
		for(i=0;i<=derece;i++){
			printf("Lutfen x^(%d)'li terimin katsayisini giriniz:",derece-i,i+1);
			scanf("%f",&katsayi[i]);
		}
		printf("Lutfen bir hassasl�k(n) degeri giriniz:");
		scanf("%d",&z);
		printf("Lutfen fonksiyon icin ilk degeri giriniz:(a)");
		scanf("%f",&a);
		printf("Lutfen fonksiyon icin uc degeri giriniz:(b)");
		scanf("%f",&b);
		h=(b-a)/z;
		d=0;
		for(i=1;i<=z-1;i++){
			d=d+fonksiyon(katsayi,derece,a+i*h);
		}
		d*=2;
		d=h/2*((fonksiyon(katsayi,derece,a)+fonksiyon(katsayi,derece,b))+d);
		printf("Integral sonucu:%f",d);
		printf("\nMenuye donmek icin M/m.Trapez Yontemini tekrar denemek icin herhangi bir tusu tuslayiniz:");
		secim=getch();
		if((secim==109) || (secim==77)) secim=77;
		else secim=55;	
		break;
	}
	case 56:{
		int dogruluk;
		system("CLS");
		printf("-Simpson Yontemi-\n");
		printf("Lutfen fonksiyonun derecesini giriniz:");
		scanf("%d",&derece);
		for(i=0;i<=derece;i++){
			printf("Lutfen x^(%d)'li terimin katsayisini giriniz:",derece-i,i+1);
			scanf("%f",&katsayi[i]);
		}
		printf("Lutfen bir hassaslik(n) degeri giriniz:");
		scanf("%d",&z);
		dogruluk=0;
		if(z%2==0) dogruluk=1;
		if(dogruluk==0) printf("\n N cift sayi olmak zorundadir.\n");
		while(dogruluk==1){
		printf("Lutfen fonksiyon icin ilk degeri giriniz:(a)");
		scanf("%f",&a);
		printf("Lutfen fonksiyon icin uc degeri giriniz:(b)");
		scanf("%f",&b);
		h=(b-a)/z;
		d=0;//tek sayilarin toplam�
		c=0;//�ift sayili koklerin toplam�
		for(i=1;i<z;i++){
			if(i%2==1) d=d+fonksiyon(katsayi,derece,a+i*h);
			else c=c+fonksiyon(katsayi,derece,a+i*h);
		}
		d*=4;
		c*=2;
		d=(h/3)*(d+c+fonksiyon(katsayi,derece,a)+fonksiyon(katsayi,derece,b));
		printf("Integral Sonucu:%f",d);
		dogruluk=0;
		}
		printf("\nMenuye donmek icin <M>/<m> .Simpson Yontemini tekrar denemek icin herhangi bir tusu tuslayiniz:");
		secim=getch();
		if((secim==109) || (secim==77)) secim=77;
		else secim=56;	
		break;
	}
	}
}
}
}
float fonksiyon(float katsayi[],int derece,float x){
	float p=1.0;
	float fx=0.0;
	for(int i=0;i<=derece;i++){
	fx=fx+p*katsayi[derece-i];			
	p=p*x;
	}
	return fx;
}
float turev(float katsayi[],int derece,float x){
	int tek=1;
	float p=0.0;
	float fx=0.0;
	for(int i=0;i<=derece;i++){
	fx=fx+p*i*katsayi[derece-i];		
	if(tek==1){
		tek=0;
		p=1.0;
		p/=x;
	}
	p=p*x;
	
	}
	return fx;
}
int isaret_degisimi(float a,float b){
	if((a<0) & (b>=0)) return 1;
	if((a>=0) & (b<0)) return 1;
	else return 0;
}
