It might seem like an unnecessary act of gallantry to leap to the defence of a player who in the last two years has been crowned Players' Player of the Year (twice), Fans' Player of the Year (twice), Football Writers' Player of the Year (twice), Barclays Player of the Season (twice), Uefa Club Footballer of the Year , FIFPro World Player of the Year and, as of this morning, the Ballon d'Or winner for 2008.

No doubt Cristiano Ronaldo's self-esteem is more or less intact this morning after his record tally of 446 points in the France Football poll. Not that he's one to get carried away. "Tomorrow morning in training, I will still be the same person as always," he said after being informed he'd won the award. Still the same old raging egomaniac, then.

Yesterday Paul Doyle expressed what many people feel about both Ronaldo and his golden ball. World's biggest show-off, individualist and poseur wins centralised and celebrity-centric gong. Why should we be surprised, curious, or - in particular - happy about this?

Ronaldo is an easy target here. He has an astonishingly single-minded capacity to infuriate. He pays no lip-service to the traditional tenets of just doing my bit for the team, wanting to stay at this club for the rest of my life and being astonished to be honoured by my fellow professionals.

On the bench, on the pitch, talking at length about himself in the press: at all times he talks, acts and plays as though he is the best footballer in the world. By miles. This is understandably annoying. And all the more so because it's true.

So why should we celebrate this most inevitable of shoo-ins? This terribly pious and self-regarding act of celebrity football amour proper? What's there to smile about here?

Firstly, Ronaldo deserved to win. And he deserves it precisely because of the way he plays, because of all that indulgent and decorous trickery, the insistence not just on winning but on demonstrating his technical and physical superiority.

Nobody else does this; partly because very few can. Lionel Messi can play like this. Kaka is also highly effective but without gilding the lily in the same way. Take Ronaldo away from the Premier League and what are we left with? Plenty of exceptional players. Plenty of players with great technical skills and the capacity to be explosive, but nobody else who can marry individual indulgence with team play at such a consistently high level.

The point is, Ronaldo is not a portent of some grim sponsored show-pony corporate future. He's a throw-back, a high-profile example of a dying breed, and a player who only survived in his current flamboyant and infuriating form because he's so very good at it.

Currently European football does not encourage this type of player to flourish. It's a centralised affair and a business of minimising risk. You used to find a player who at least played with some sense of the Ronaldo style at most top clubs. Joe Cole could have been a case in point. At Chelsea his frills have been sacrificed for an admirable ferreting and adept sense of team play.

If Cole had stayed at West Ham the exuberant tricksiness of his youth could have been allowed to flourish. He might have been West Ham's best player, the team luxury. Perhaps even a less effective player. But the game itself would be more varied and more decorous.

This kind of thing doesn't happen much now. A player like Cole was too valuable to be allowed to operate outside of the Champions League machine. He was co-opted. What has emerged is a muted virtuoso.

And perhaps the same thing would have happened to Ronaldo, his excesses might have been sanded off at Manchester United, if he wasn't so jaw-droppingly good. Only the very best are allowed to play like this.

Looking to the wider field it's a process that was applied to Steve McManaman at Real Madrid. McManaman left Liverpool an accomplished, if occasionally frustrating dribbler, but essentially a creative midfielder. This wasn't what Madrid required of him. With great intelligence he adapted the way he played, becoming a diligent link-man, a top-drawer water-carrier with a shimmy.

You get a lot of these kind of players these days. But you don't get many like the old McManaman. Which is a bit of a shame.

The death of the individual: this is an old theme. As long ago as 1974 Pele gave a TV interview in which he joined in the general lament over the absence of blue-riband mavericks knocking about the world game (although Pele, to be fair, would probably agree that the moon was made of Utterly Butterly if you put a camera in front of him).

But at the top level this is now a hard-nosed reality of a game that � with its 30-strong squads and endless European league competitions - is decidedly risk-averse.

The Ballon d'Or is just another award, and awards are annoying because they usually go to the bloke who kicks the ball into the goal, or, at best, the bloke who passes it to him before he kicks it into the goal. There are other ways to play football: a really good goalkeeper or a great centre-half also deserve to be rewarded. But equally we're lucky to still have at least one player of Ronaldo's style � the free-spirit and the entertainer - who also meets the standard.

Sometimes even the obvious deserves to be celebrated. Ronaldo and the Ballon d'Or are a perfect fit this time around. We may have reservations about both. But right now they bring each other credit.
