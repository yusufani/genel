Türkiye terörist (!)?şovmen Beyaz’ı konuşurken canlı bomba 

İktidarın devleti çökme noktasına getirdiği ülkemizde AKP önde gelenlerinin verdiği işaretle, yargı, yandaş medya ve sosyal medya trolleri, televizyon şovunda bugüne kadar hiç suya sabuna dokunmayan Beyazıt Öztürk’ü teröristlikle suçlayıp linç etmekle uğraşırken, terör, korkunç yüzünü İstanbul’un göbeğinde, Sultanahmet Meydanı’nda gösterdi.
Bu satırların yazıldığı saatlerde Valilik, 10 turistin hayatını kaybettiğini ve 15’inin de 
yaralandığını açıklamıştı.
Ayrıca her terör saldırısından ve katliamdan sonra yapıldığı gibi, yayın yasağı da getirilmişti!
Belli ki, dış basında IŞİD tarafından yapıldığı öne sürülen saldırı tüm dünyada birinci haber olup lanetlenirken, ülkemizdeki bir avuç bağımsız medya kuruluşunun, bu korkunç olayın ardındaki gerçekleri ve ihmalleri sorgulaması istenmiyordu!..
O nedenle terörün her türlüsünü telin ederek, toplumumuzu hedef alan bir başka teröre geçelim ve yasa dışı kumar terörüyle ilgili çarpıcı dosyamızı açıklayalım:

* * *

CHP İzmir milletvekili, benim basın özgürlüğü için verdiği mücadele nedeniyle “Günümüzün Hasan Tahsin”i dediğim Atila Sertel, Meclis’teki görevine de hızlı bir başlangıç yaptı.
Kamu İktisadi Teşebbüsleri (KİT) Komisyonu üyesi olan Sertel, ayağının tozuyla, yasak olmasına rağmen, iktidarın internet üzerinden yasa dışı kumar oynatan sitelere göz yumduğunu ortaya çıkardı.

* * *

Sevgili okurlarım,
Bilindiği gibi ülkemizde Milli Piyango’nun denetimi ve izni dışında her türlü bahis ve talih oyunlarının oynanması yasak. Aksi takdirde yasalarımız hem oynatanlar, hem de oynayanlar hakkında hapis cezaları öngörüyor. Yasalar, suç işlendiğinin tespiti halinde bu yayınlara erişimin engellenmesi yetkisini de mahkeme kararına gerek kalmaksızın, TİB’e (Telekomünikasyon İletişim Başkanlığı) veriyor.
Ancak Sayıştay raporlarında TİB’in, Milli Piyango İdaresi’nce kendisine bildirilen sitelerden bir çoğunu kapatmadığı görülüyor. Bu da zihinlere “İktidarın yönetimindeki kurumlardan biri olan TİB, ülkemizdeki yasa dışı kumara göz mü yumuyor” sorusunu getiriyor. Ayrıca kara para aklama başta olmak üzere ülke ekonomisine ciddi zararlar veren bu sitelerin kapatılmaması “İktidar eliyle yasa dışı kumar oynanmasına ön ayak olmak ve göz yummak” anlamına da geliyor. O zaman da şu soruları sormak gerekiyor: “Yasa dışı oluşan büyük pastadan kimler pay alıyor? Yasa dışı kumar yoluyla kara paradan nemalananlar arasında AKP’den birileri ve onlara bağlı bazı bürokratlar bulunuyor mu?..”

* * *

Atila Sertel, çarpıcı iddialarını Sayıştay raporlarında yer alan tablo ve rakamlara dayandırıyor. Örneğin Milli Piyango İdaresi 2009 yılından itibaren 2015 yılına kadar toplam 2 bin 163 yasa dışı bahis veya kumar oynatan site tespit ederek TİB’e bildirmiş. Ama gelin görün ki TİB, bunlardan sadece 469’unu kapatmış veya erişimini engellemiş. Geri kalan büyük çoğunluk ise, yasa dışı kumar oynatmaya rahatlıkla devam etmiş!.. Bunu gören kara paracılar da kumar oynatan site sayısını çığ gibi büyütmüşler!..

* * *

Atila Sertel, yasa dışı bahis veya kumar oynatan sitelerin ülke ekonomisine verdiği zararın en az 1 milyar lira olduğunu öne sürüyor. Bu iddiasını da Spor Toto Teşkilat Başkanı Muharrem Kasapoğlu’nun, Ocak 2014 tarihinde yaptığı, “Türkiye’de illegal bahis sektörünün yıllık hacminin 1 milyar lira olduğu” yönündeki açıklamasına dayandırıyor. Milli Piyango İdaresi’nin 2014 yılında devlet bütçesine, dolaylı ve direkt olarak 1 milyar lira katkı sağladığı düşünüldüğünde, yasal olmayan sitelerde dönen kayıt dışı paranın bunun çok üzerinde olduğu ortaya çıkıyor. Böylece birilerinin devletin kasasından dolaylı yollardan para çaldığı ve buna karşın devlet kurumlarının bu hırsızlığa göz yumduğu anlaşılıyor.

* * *

CHP İzmir Milletvekili Sertel’e göre; yasa dışı kumar veya bahis sektörü sadece ülke ekonomisine zarar vermekle kalmıyor, manen büyük tahribata da yol açıyor. Zira bahis oynatan sitelere girişte yaş denetimi olmadığı için, 18 yaşından küçük çocuklar da rahatlıkla girip bahis oynayabiliyorlar.

* * *

Sevgili okurlarım,
Geçmişteki uygulamalara dayanarak CHP’nin mücadeleci ve cesur Milletvekili Atila Sertel’in bu çarpıcı tespitlerinin AKP iktidarını harekete geçirmesini ve sorumlulara yasal işlem yapılmasını beklemiyorum.
Ama yasa dışı kumar teröründen söz ettiğimiz için Sertel ve benim hakkımda “terörist” suçlamasıyla soruşturma açılırsa, şaşırmayacağımızı belirtiyorum!..
Çünkü şovmenleri teröristlikle yaftalayarak, katliamlara yayın yasağı getirerek, terörle mücadele edileceğini düşünebilen bir zihniyet yönetiyor ülkeyi!..