Han�er meselesi mi dediniz 
MERAK ediyorum. Aralar�nda �mer �elik, Egemen Ba��� gibi makul isimlerin bile bulundu�u 300 AKP�li, izinsiz Kuran kurslar�n� yeniden canland�racak bu madde i�in el kald�r�rken Devlet Bakan� Cemil �i�ek acaba ne d���nm��t�r? 

Mesela o malum �han�er� meselesi yine akl�na gelmi� midir?

Bir ba�ka elin, T�rkiye�nin s�rt�na ba�ka bir b��a�� saplad���n� d���nm�� m�d�r?

Acaba bir ba�ka han�eri de, be� on bin satan cemaat gazeteleri istedi diye durup dururken Ceza Kanunu�na bu maddeyi ekleyen ellerde g�rm�� m�d�r?

Bu son dakika gol�n�n ad�n� koyabilmi� midir?

Mesela, �Durup dururken maraza ��karmak� gibi bir c�mle... 

* * *

Merak ediyorum. Bu nedir?

�Her �ey iyi gidiyor� diye d���n�rken, birden �asl�na r�cu etme� duygusu mu?

Allah a�k�na bu �lkede, isteyen istedi�i evde Kuran kursu a�s�n diyen makul bir �o�unluk mu var?

Sorun, istedi�iniz kadar sorun.

Egoist cemaatler, yaygarac� az�nl�klar, kulaklara, vicdanlara h�kim oldu mu, ne dinleyen kal�r, ne de i�iten.

Makul insanlar sokaktan kovulur, ortal�k i�te b�yle eli de�ilse bile dili han�erlilere kal�r.

* * *

�u manzaraya bir bak�n.

Bu �lke Avrupa demokrasisinin hudutlar�ndan ge�mek istiyor.

O Meclis�in �o�unlu�unun akl� ise evlerde, apartman dairelerinde, sokak aralar�nda, cami altlar�nda izinsiz Kuran kursu a�makta.

Medeni olmak isteyen bir toplum izinden, kuraldan korkar m�?

�nanc�nda samimi olan bir insan, Kuran ��retiminin bir d�zen alt�nda olmas�ndan �ekinir mi?

Bu �lkede Kuran ��renmek isteyen insana, d�zenli b�t�n yollar a��k de�il mi?

Birtak�m fanatik, cahil insanlar�n a�t��� Kuran kurslar�ndan ge�mi�te neler �ekti�imizi bu kadar �abuk mu unuttuk?

Yoksa o k���c�k cemaatlerin tahakk�m� bu kadar b�y�k m�?

Ne yaz�k ki bir �soku�turma� k�lt�r� giderek ki�ili�imizi kapl�yor.

T�rkiye�yi d���nen yok.

Varsa yoksa yak�n �evremiz.

* * *

En b�y�k korkumuz, �rg�tl� k���k cemaatler.

Halk�n y�zde 70�inin b�yle dertleri varm�� yokmu� kimseyi ilgilendirmiyor.

��lk� Ocaklar��ndaki arkada�lar ne der?�

�Devrimci yolda�lar�n y�z�ne bakamay�z.�

�Milli G�r���� karde�lerimiz bizi peri�an eder.�

Hi� merak etmeyin, bu makul insanlar, maraza istemeyen bu sessiz �o�unluk da bir g�n sizi peri�an edecek.

Ekonomi iyi gidiyor.

M�zakereler ba�layacak.

Ortal�k sakin.

Anketlerden ilk defa T�rk halk�n�n iyimserlik sinyalleri geliyor.

Ama ne �are.

Kan�n�za maraza girmi�se, cemaat�ilik ruhunuza i�lemi�se, rahat size bat�yorsa, elinizde ��p oray� buray� e�elemeye ba�lars�n�z.

Nas�l olsa i�iniz rahat.

��nk� �ankaya�da bir Cumhurba�kan� var. G�lmeyen, merhabala�mayan, sessiz sedas�z bir insan.

Nas�l olsa bunu geri �evirir.

B�ylece sen sa� ben selamet.

Yeni demokrasinin muhte�em i�birli�i.

Biliniz ki, at�lan bu her manas�z ve tehlikeli ad�m, biz en a��r ele�tirenlerinin g�z�nde bile Cumhurba�kan� Sezer�i sempatik olmasa da gerekli k�l�yor.

�ankaya�y� g�z�m�zde, makul�n �o�unlukta oldu�u bir senatoya �eviriyor.

* * *

Bir kere daha anlad�m ki, egoist cemaatler konu�maya ba�lad� m�, ak�l korkup ka��yor.

O toz duman i�inde kimin han�eri kimin s�rt�na saplan�yor, anlamak m�mk�n olmuyor. 


