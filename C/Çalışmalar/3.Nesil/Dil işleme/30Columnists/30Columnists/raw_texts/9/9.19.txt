Playing outdoors dramatically cuts a child's risk of becoming short-sighted, research shows. 

Spending two or three hours outside each day halves the chance of developing the condition. 

It challenges the belief that short-sightedness is caused by computer use, TV watching or reading in dim light. 

Myopia usually develops during childhood but many adults' eyesight worsens after they start jobs in an office. 

The Australian government researchers believe that sunlight triggers the release of chemicals that prevent the distortion of the eyeball - which causes the condition.
They compared the vision and habits of six and seven-year-olds in Singapore and Australia. Thirty per cent of the Singaporean children were short-sighted - ten times the Australian rate. 

A similar pattern emerged when the analysis was limited to children of Chinese descent, meaning the difference could not be explained by genetics. 

Both groups spent a similar amount of time reading, watching television and playing computer games. But the Australian children spent an average of two hours a day outdoors - 90 minutes more than their Asian counterparts. 

Professor Ian Morgan, of the Australian Research Council's Vision Centre, said: 'We're seeing large increases in myopia among children in urban societies all around the world - and the outstanding common factor may be less and less time spent outdoors. 

'The idea that reading makes you short-sighted has been popular for a couple of hundred years. But recent data shows that the time spent indoors is a more important factor.'

Professor Morgan believes natural light - which can be hundreds of times brighter than indoor light - triggers the release of dopamine. 

 

The chemical stops the eyeball from growing out of shape and causing myopia. 

Analyses in India and the US have come to a similar conclusion.
A study of Sydney children carried out last year found those who spend the most time outdoors had the best eyesight. 

It concluded that three hours of natural light a day could halve the risk of short-sightedness.
Professor Morgan said humans were naturally long-sighted but there was a dramatic rise in myopia once people began intensive schooling and spent little or no time outdoors.
'In some East Asian cities 80-90 per cent of children are affected - and governments and the World Health Organisation are very worried about it,' he said.
Researcher Dr Kathryn Rose said: 'Our evidence suggests that the key factor is being outdoors and it does not matter if that time is spent having a picnic or playing sport.

'Both will protect a child's eyes from growing excessively, which is a major cause of myopia.'
