Sales are booming at online fashion retailer Asos, boosted by strong demand for Christmas party dresses.

Asos, which stands for As Seen On Screen, sells cut-price versions of outfits worn by celebrities, targeting 16- to 34-year olds. 

Reporting a surge in half-year profits �2.4m, up from �300,000, chief executive Nick Robertson said today that sales have doubled over the past nine weeks.

"Christmas really is going to be strong for us," he said. There's a massive fair wind blowing behind the online market, and we've got a much wider range of products this year."

Among the bestsellers so far are sequined and gem-embellished partywear, prom-style dresses and traditional little black numbers. Patent accessories are popular and anything in purple and red is also going well.

Robinson said the most popular celebrities to copy this Christmas are TV presenters Fearne Cotton and Alexa Chung, along with the Olsen twins, Peaches Geldof and Sienna Miller.

He has no doubt it will be an online Christmas this year. "All the high street retailer's websites will be doing well, but their overall businesses will not," he said, highlighting recent research predicting a 42% surge in online shopping over the festive season.

Aim-listed Asos has 1.6 million registered users and, according to Hitwise, is the second most visited online fashion store in the UK after Next.

Retail analyst Jose Marco-Tobares at Numis said the Asos results show that whilst the high street retail environment is very tough, home shopping "continues to power ahead". 

He noted that �24bn of �26bn in increased retail expenditure over the last five years has come from online. "We believe this trend is likely to continue and we expect Asos will continue to benefit from this."

Asos shares were up more than 5% by mid-afternoon at 192.5p.
