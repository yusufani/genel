Japonya, Milli Tak�m'� bekliyor 
Kyoto

D�NYA Kupas� bu y�l Japonya ve Kore'de yap�lacak. T�rkiye'nin de aras�nda bulundu�u milli tak�mlar�n, ma�lar�n� bu iki �lkeden hangisinde yapaca�� �n�m�zdeki g�nlerde �ekilecek kurayla belirlenecek. Finale kalan baz� �lkeler, kamp yapacaklar� yeri her iki �lkede de belirlemeye ba�lam��. 

�rne�in Almanlar, Kore ve Japonya'da iki�er kenti kamp yeri olarak bug�nden se�mi�. Kurada hangi �lke ��karsa �nceden belirledikleri o kampa gidecekler. T�rkiye ise kamp yerini belirlemekte olduk�a zorlan�yor.

Nedeni; Japonya'da T�rk Milli Tak�m�'na g�sterilen yo�un ilgi... �u ana kadar 14 il, T�rk Milli Tak�m�'n� a��rlamak i�in ba�vuruda bulunmu�.

�ANS Y�ZDE 50

Her ne kadar T�rk Milli Tak�-m�'n�n Japonya'ya gitme �ans� y�zde 50 de olsa, Japonlar bug�nden Milli Tak�m'� bekliyor. Aktar�ld���na g�re ba�vuranlar�n hepsi, ��Tak�m�n�z bizim ilimize gelmezse al�n�r�z�� yakla��m�nda.

Yakaland��� tayfuna yenik d��en Ertu�rul Z�rhl�s�'n�n 1890'da a��klar�nda batt��� ��el'in karde� kenti Wakayama'n�n Kushimoto �l�esi de bunlardan biri. Milli Tak�m'�n kamp yapaca�� yerleri dahi belirlemi�ler. 

Balina �eklinde dev bir stadyum, T�rk Milli Tak�m�'n� bekliyor. 

Sadece Wakayama de�il, Saithama, �iba, Narita da bunlar aras�nda. B�y�kel�i Yaman Ba�kut, kar��la�t��� bask�lar kar��s�nda �aresiz. Japonya'n�n futbol tutkunlu�unun tarihi o kadar geri gitmiyor.

Profesyonel lig uygulamas� 10 y�l kadar �nce ba�lam�� olmas�na ra�men, Japonlar futbola b�y�k ilgi duyuyor. T�rk futbolcular� da bu ilginin �n s�ras�nda yer al�yor.

Marmara �niversitesi'nde 10 y�l �nce master yap�p Japonya'ya d�nm�� olan Nabumitsu Hayamizu ve Yasushi �mamatsu, sohbetimizde T�rk futbolcular�n neredeyse soya�ac�n� ��kard�. Sadece isimlerini de�il, ge�mi�te hangi tak�mlarda oynad�klar�n�, att�klar� gol say�lar�n�, soyisimlerine kadar bir�ok futbolcunun neredeyse �eceresini s�ralad�lar.

T�rk televizyonlar�n�n izlenme olana�� olmayan Japonya'da bu kadar �ok bilgi nas�l elde ediliyor? Anlatt�klar�na g�re, her hafta ba�� internet �zerinden T�rkiye'deki ma�lar� takip ediyorlarm��.

Hatta �mamatsu, koyu bir Fenerbah�eli.

Mahalle tak�m�nda top ko�turuyor. Ma�lara da Fenerbah�e formas�n� giyip ��k�yormu�. Baz� arkada�lar�n�n da Galatasaray formas� giyip geldiklerini s�yl�yor.

Hepsi de T�rk Milli Tak�m�'na rehberlik yapmak i�in can at�yorlar. B�y�kel�ilik verilerine g�re de �u an i�in 1500 ki�i seferber olmaya haz�r. Japonya'da T�rkiye'ye kar�� ilgi bu kadar yo�un.

Ancak, Afganistan harek�t�ndan sonra Japon h�k�meti, T�rkiye'yi riskli �lkeler s�n�f�na koyunca bu �lkeden T�rkiye'ye gidecek turist say�s�nda inan�lmaz d��me olmu�. Ge�en y�l T�rkiye'ye giden turist 90 bine yakla��rken, son d�nemde THY baz� seferlerini iptal etmek zorunda kalm��.

Japon h�k�metinin nezdinde yap�lan giri�imler sonucunda, T�rkiye tehlike s�ralamas�ndan ��kar�lm��. Buna ra�men b�rak�lan imaj�n yaratt��� g�lge, hen�z yok olmam��.

T�RK�YE YOLU

Japonya, 2003'� T�rkiye y�l� olarak kutlayacak.

Kalan g�lgenin silinmesi i�in iyi bir f�rsat.

Bunun i�in de ba�ta turizm sekt�r� olmak �zere T�rkiye'nin Japonya'da yo�un bir tan�t�m�na ihtiya� var.

Milli Tak�m'a g�sterilen yo�un ilgiden yola ��k�ld���nda k���k bir �aba dahi Japonya'dan T�rkiye'ye gidecekleri birka� kat�na ��karmaya yeter. 


