Benim nerem �i�man? 
Bu art�k bir end�stri. Sa�l�kl� beslenme, anti-aging, detoks. Bu alanda �ne ��kan isimlerden biri Taylan K�meli. 

Son zamanlarda mesle�inde �yle y�kseldi ki, onu g�rmezden gelmek art�k m�mk�n de�il. Her g�n verdi�i sa�l�k ���tleriyle onun takibindeyken, bir de sindirim yo�urdu reklamlar�yla kar��m�za ��k�nca, kap�s�n� �almak ve hakk�ndaki dedikodular�n cevab�n� almak farz oldu. Neydi o dedikodu? Efendim, �ok kiloluymu� da, kocaman poposu varm��, bu halde nas�l insanlar� zay�flatacakm��, zay�flatacaksa �nce kendini zay�flats�nm��. Tahmin edersiniz ki, b�yle bir soruyu bir insana sormak zor bir i�ti: �Neden popon b�y�k? Neden basenlerin geni�?� denmez. Ama Taylan K�meli komplekssiz bir insan, �Onlar�n kalpleri k�t�� dedi sorunun cevab�n� k�t diye verdi. Asl�na bakarsan�z benim ��plak g�z�mle g�rd���m, bir fazlal���n�n olmad���yd�. �yle ya da b�yle, o meseleyi konu�tuk, bir de araya bo�anmas� s�ras�nda ��kan dedikodular� s�k��t�rd�k..

Ka� kilosunuz?

- 56 kilo 400 gr. �stiyorsan�z tart�lay�m...

Delirdiniz mi? Ben tart�ya ��kmay� sevmem, bir ba�kas�n� da ��kartmak istemem! Ne kadar fazlan�z var?

- Hi� yok vallahi. Sadece 11 kilo 800 gr. ya��m var, ki bu da son derece normal...

Diyetisyen oldu�unuz i�in mi size kilolar�n�zdan y�klendiler?

- Yok, yok... Mesle�inizde ba�ar�l�ysan�z ve kad�nsan�z bitmi�tir. Bu, �im�ekleri �zerinize �ekmeniz i�in yeterli. Yani bazen, �Ke�ke erkek olsayd�m� diyorum, ���yle mesle�inde ba�ar�s�z, aptal ve �i�man bir erkek...� O zaman kimse benimle u�ra�mazd�...

�Kendisi kilo veremiyor, bize verdirtmeye �al���yor� ele�tirisine verecek cevab�n�z nedir?

- Ne olacak? Onlar k�t� kalpli! 42 ya��nda bir kad�n�m. 2 do�um yapt�m. Boyum 1.70, kilom da 56. Beni kilolu olmakla itham edenler, g�l�n� olmas�nlar l�tfen, literat�re baks�nlar, kilo b�l� boy karenin 18.5 ile 25 aras�nda olmas�, normal kiloda olunmas� demektir. Kilom gayet iyi yani. Ama kamera, insan� oldu�undan daha �i�man g�sterebiliyor. Bunlar�n hepsini bir tarafa b�rak�yorum, �unu soruyorum: �nsan, g�z doktoru diye g�zl�k takamayacak m�? Kalp doktoru diye kalp krizi ge�irmeyecek mi? G�ld�rmesinler beni...

Yani �i�man diyetisyen olabilir...

- Olabilir tabii! Fevkalade do�ru yiyordur ama hipotiroidi vard�r, ne bileyim inselin resistansl� ya��yordur. Ya da genetik formasyonlar nedeniyle kiloludur. Yery�z�ndeki b�t�n diyetisyenler zay�f olacak diye bir �ey yok ki. Bir insan, sa�l�kl� beslendi�i halde kilolu olabilir ya da kilolu g�z�kebilir. V�cudun elma ya da armut tipi olmas�n�n da �nemi vard�r...

Sizinki ne tipi?

- Ay�pt�r s�ylemesi armut! Basenli bir kad�n oldu�umu kabul ediyorum ama kilolu oldu�umu asla!

Problemin esas� nedir: Sizi �ekemeyenler mi var?

- Elbette. �nsanlar zannediyor ki, 2-3 tane �nl�y� zay�flatt�m ve Taylan K�meli oldum. Hay�r efendim. �unu bilmelerini istiyorum, ben aya��mda �ocu�umu sallarken, fizyoloji �al���yordum ve o dersten 90 al�yordum. Ben do�umuma 2 saat kala bitirme s�nav�na giriyordum. �ocu�uma s�t verirken okula gidiyordum... Ben bug�nlere 18 saat �al��arak geldim. On binlerce insan�n te�ekk�r duas�n� alarak... Ama onlar meseleye �u s��l�kta bakmay� tercih ediyorlar: �Reklam filminde oynad� kim bilir ne paralar ald�...� �yle ah�m �ah�m bir para da almad�m, i�leri rahat olsun...

Bu size ilk ��akmalar�� m�?

- De�il. Bu �lkede hi�bir diyetisyenin �zel hayat� 1. sayfadan haber olarak verilmez. Benimki verildi. Oysa, ben sansasyonlar�n �ok uza��nday�m. Benim kocam birisiyle olur, olmaz. Kime ne? �zel hayat�m�zdan kime ne? Ondan �nce de Hacettepe mezunu olmad���m s�ylendi. �Diyet uzman� de�il� dendi. ��la�la zay�flat�yor� dendi. Kulaklar�m� t�k�yorum bu t�r iftiralara ama reklam filmi her �eyin �zerine t�y dikti...

Reklam filminde oynamadan, �Ben sa�l�k sekt�r�nde �al���yorum. Tek bir �r�nle �zde�le�mem do�ru mu?� diye kendi kendinize sormad�n�z m�?

- Sordum. Mesle�imle ilgili bir ad�m atmadan �nce hem kendime hem de konunun uzmanlar�na soruyorum mutlaka. Amerikan ve Avrupa Diyetisyenler Derne�i �yesiyim. Onlar, sa�l�k��lar�n bu t�r faaliyetlerde �spokesperson� (uzman g�r��) olarak yer almas�n� yad�rgam�yorlar. Kurumsal kimli�imi olu�turan �pr� �irketi de bu reklamlarda oynamamda bir sak�nca g�rmedi. Onlar beni y�nlendiriyor, �unu yap, bunu yapma, �uraya ��k, buraya ��kma diyor...

Tamamen bir �star� gibi davran�yorsunuz yani?

- Mecburum. Bu toplum beni b�yle bir yere getirdi. Dikkatli davranmaya �al���yorum. Yanl�� yapmaktan korkuyorum. Bir de �u var tabii: O reklama ben ��kmasayd�m kim ��kacakt�? Benim ��yle bir misyonum var. Ben do�rular� s�ylemeye �al��an bir kad�n�m. Hangi ko�ulda olursa olsun... 

�yi ama art�k her yerde g�r�� bildiriyorsunuz. Medya maydanozuna d�n��mekten korkmuyor musunuz?

- Tabii ki korkuyorum. �nan�r m�s�n�z, g�nde belki 20 tane teklif geliyor: ��u �u konuda g�r�� bildirir misiniz, r�portaj verir misiniz?� Se�ici olmaya �al���yorum. 

BEN YALNIZIM BEN�M B�R A�KIM YOK S�YLER M�S�N�Z N�YE ZAYIFLAYAYIM

Ge�enlerde hayran� oldu�um bir sinema sanat��s�yla kar��la�t�m: ��ocuklu�umdan beri size tapar�m. Siz muhte�emsiniz sultan�m. �zin verin. sizi zay�flatay�m.� Bana ��yle dedi: �Taylan Han�m, ben yaln�z�m. Hayat�mda bir a�k�m yok, s�yler misiniz neden zay�flayay�m?� G�rd�n�z m�, ��hret, tam da b�yle bir �ey. Yaln�zl�k getiriyor, a�k� elinizden al�yor. Bir de her ili�kinin bir menfaat �er�evesine d�n��t���n� hissediyorsunuz. Hi� aramayan ilkokul arkada�lar�n�z, akrabalar�n�z aramaya ba�l�yor. Sizinle hi� konu�mayan insanlar, sanki �ok samimiymi�iz gibi davranmaya ba�l�yor. ��hret olduktan sonra benim ger�ek dostum k�pe�im oldu. Ad� Masum. Niye? ��nk� ger�ek masumiyetin sadece onda oldu�una inan�yorum. �nsanlar sizin de�i�iminize g�re de�i�iyorlar. De�i�meyen biri iki ki�i varsa, onlar da ger�ek dostlar�m...

HAYATTA �K� �EY�N�Z OLSUN:TA�INIRKEN K�TAPLARINIZ �L�RKEN DE SEYAHAT ANILARINIZ

Anneannemin laf�d�r, benim de hayat felsefemdir: �Hayatta iki �eyiniz olsun. Ta��n�rken kitaplar�n�z, �l�rken de seyahat an�lar�n�z...� Kitaba ve seyahate tahmin edemeyece�iniz kadar yat�r�m yapar�m! 
