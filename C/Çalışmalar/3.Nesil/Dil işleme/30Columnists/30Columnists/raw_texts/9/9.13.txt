It seems those of us who covered our school books with scribbles may not have had an entirely wasted education. 
For doodling may actually be good for the brain, scientists claim. 
Far from being a sign of inattention, it is thought to focus the mind and stop daydreaming, allowing people to persevere with dull tasks.
Researcher Jackie Andrade, who conducted a study into the habit, said: 'This study suggests that in everyday life doodling may be something we do because it helps keep us on track with a boring tasks, rather than being an unnecessary distraction that we should try to resist.' 
Professor Andrade, of Plymouth University, asked 40 men and women to listen to a tedious telephone message about plans for a party. 
It included names of guests, as well as names of people who would not attend, place names and other irrelevant material. 
None of the participants was told they were to be tested to see how much they remembered afterwards. 

The doodlers  -  around half of those who took part  -  had written down more names while listening to the message, the journal Applied Cognitive Psychology reports. 
They also had better recall for both the party-goers' names and place names mentioned in the recording  -  remembering nearly a third more than the others. 
Professor Andrade said that doodling may help because it is simple enough not to drain brainpower, therefore allowing the mind to carry out a task without affecting how well it is done. 
And by preventing concentration from wandering, it may help us focus better. 
