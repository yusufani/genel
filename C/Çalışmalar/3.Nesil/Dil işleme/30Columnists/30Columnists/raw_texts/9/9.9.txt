Being lonely is as bad for your health as smoking or obesity, experts have warned. 

Being cut off from friends and family can raise blood pressure and weaken the immune system, the American Association for the Advancement of Science's annual conference heard. 

It can also make it harder to sleep and even speed the progression of dementia, according to psychologist John Cacioppo. 

He found loneliness raises levels of the hormone cortisol and can push blood pressure up into the danger zone for heart attacks and strokes. 

Research showed the difference in health between the lonely and the most socially active could be as great as that between smokers and non-smokers and the obese and those of normal weight. 

Professor Cacioppo said: 'When time takes its toll on the body, loneliness steepens that slope of descent.'
In his study, the loneliest people had blood pressure readings up to 30 points higher than those with the most active social lives, making them three times more likely to develop heart disease and stroke and twice as likely to die from them as people with normal blood pressure.
High levels of cortisol can also suppress the immune system, raising a person's vulnerability to disease.
The lonely also sleep more fitfully, feel lethargic during the day and are more likely to rely on sleeping tablets.
Loneliness also has numerous other effects on health, including accelerating the pace of dementia.
It is not clear why this is but it may be their brains lack the suppleness of those who socialise regularly.
When researchers compared the health of people who shut themselves away with from the world with gregarious types they found the difference as great as that between smokers and non-smokers, the obese and the normal weight or those who exercised and those who didn't.
Professor Cacioppo said the phenomenon was hugely relevant in today's fragmented society, where many people communicate through the internet rather than face-to-face.
The Chicago University researcher said: 'We are increasingly living in isolation.  Partly because we are ageing, also because we are marrying later and having fewer children there are fewer confidantes and levels of loneliness are going up.'
The professor advises the lonely to try making friends through charity work and says it is better to have a few strong friendships than lots of acquaintances.
'Lonely people feel a hunger,' he said.  'The key is to realise that the solution lies not in being fed but in cooking for and enjoying a meal with others.'
The professor believes the trait has its roots deep in evolution.
Pangs of loneliness likely reminded those who had become isolated with a reminder to rejoin the security of the pack.
