#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct ogrenci {
	char ogrenci_isim[50];
	int ogrenci_vize1, ogrenci_vize2, ogrenci_final;
	union {
		int but;
		int ortalama;
	}sonuc;
	struct ogrenci *next;
};
typedef struct ogrenci ELEMENT;
ELEMENT * yerac(ELEMENT *p);
ELEMENT* listeye_ekle(ELEMENT *p);
void araya_ekle(ELEMENT *e, ELEMENT *p);
void ara(char * name);
void sil(ELEMENT * gidici);
static ELEMENT *root = NULL;
int main() {
	int secim = 1;
	while (secim != 0) {
		printf("\n1->Ogrenci ekle\n2->Ogrenci goruntule\n3->Ogrenci araya ekle\n4->ogrenci sil\n5->Ogrenci ara\n6-> Ogrencileri DOsyaya yazdır\n0->CIKIS");
		scanf("%d", &secim);
		system("CLS");
		switch (secim) {
		case 1: {
			ELEMENT *p = NULL;
			p = listeye_ekle(yerac(p));
			printf("İsim:\n"); scanf("%s", p->ogrenci_isim);
			printf("Vize1 vize2 final \n"); 
			scanf("%d%d%d", &p->ogrenci_vize1, &p->ogrenci_vize2, &p->ogrenci_final);
			int ortalama = (p->ogrenci_vize2 + p->ogrenci_vize1 + p->ogrenci_final) / 3;
			if (ortalama < 50) {

				printf("bıt:\n"); scanf("%d", &p->sonuc.but);
			}
			else p->sonuc.ortalama = ortalama;
			break;
		}
		case 2: {
			if (root != NULL) {
				ELEMENT *p=NULL;
				/* for(p=root;p->next!=NULL;p=p->next){
					 printf("Isım: %s Vizeler:%d%d final :%d",p->ogrenci_isim,p->ogrenci_vize1,p->ogrenci_vize2,p->ogrenci_final);
					 int ortalama=(p->ogrenci_vize2+p->ogrenci_vize1+p->ogrenci_final)/3;
					 if (ortalama == p->sonuc.ortalama) printf(" Ortalama: %d",p->sonuc.ortalama);
					 else printf(" But %d",p->sonuc.but);
				 }*/
				p = root;
				while (p != NULL) {
					printf("Isım: %s Vizeler:%d%d final :%d\n", p->ogrenci_isim, p->ogrenci_vize1, p->ogrenci_vize2, p->ogrenci_final);
					int ortalama = (p->ogrenci_vize2 + p->ogrenci_vize1 + p->ogrenci_final) / 3;
					if (ortalama == p->sonuc.ortalama) printf(" Ortalama: %d", p->sonuc.ortalama);
					else printf(" But %d", p->sonuc.but);
					p = p->next;
				}

			}
			break;
		}
		case 3: {
			ELEMENT *p = NULL;
			p=yerac(p);
			printf("İsim:\n"); scanf("%s", p->ogrenci_isim);
			printf("Vize1 vize2 final \n"); scanf("%d%d%d", &p->ogrenci_vize1, &p->ogrenci_vize2, &p->ogrenci_final);
			int ortalama = (p->ogrenci_vize2 + p->ogrenci_vize1 + p->ogrenci_final) / 3;
			if (ortalama < 50) {
				printf("bıt:\n"); scanf("%d", &p->sonuc.but);
			}
			else p->sonuc.ortalama = ortalama;
			int n, i = 1;
			printf("Kacinci siraya eklemek istersiniz:"); scanf("%d", &n);
			ELEMENT *e = root;
			while (i < n) {
				e = e->next;
				i++;
			}
			araya_ekle(e, p);
			break;
		}
		case 4: {
			int n, i = 0;
			printf("Kacinci sirayi silmek  istersiniz: 0 dan basliyor"); scanf("%d", &n);
			ELEMENT *e = root;
			while (i < n) {
				e = e->next;
				i++;
			}
			sil(e);
			break;

		}
		case 5: {
			char name[20];
			printf("aranacak ogrencinin adı ");
			scanf("%s", name);
			ara(name);
			break;
		}

		}
	}
}
ELEMENT * yerac(ELEMENT *p) {
	p = (ELEMENT *)malloc(sizeof(ELEMENT));
	p->next = NULL;
	return p;
}
ELEMENT* listeye_ekle(ELEMENT *p) {
	if (root == NULL) root = p;
	else {
		ELEMENT *e = root;
		while (e->next != NULL) e = e->next;// for(i=0;i<n;i++)
		e->next = p;
	}
	return p;
}
void araya_ekle(ELEMENT *e, ELEMENT *p) {//p araya alınacak olan 
	ELEMENT *c = root;
	while (c->next != e) {
		c = c->next;
	}
	c->next = p;
	p->next = e;

}
void sil(ELEMENT * gidici) {
	ELEMENT *p;
	p = root;
	while (p->next != gidici) p = p->next;
	p->next = gidici->next;
	free(gidici);
}
void ara(char * name) {
	ELEMENT *p;
	p = root;
	while (strcmp(p->ogrenci_isim,name) && p != NULL ) p = p->next;
	if (p == NULL) printf("Aranan isimde biri bulunamadi");
	else {
		printf("Ogrenci:%s Ortalaması veya butu=%d", p->ogrenci_isim, p->sonuc);
	}

}