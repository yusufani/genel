It found favour with Queen Victoria, whose 64-year reign was the longest in British history.
Flowing from a spring on the edge of the Balmoral estate, it is also ideally placed to have played a role in the youthful looks of the current Queen.
Scientists certainly seem to think that the pure waters of the Pannanich Wells in north-east Scotland could help hold back the hands of time.
A duo of experiments show that the spring water, bottled and sold as Deeside Water, mops-up dangerous molecules that have been linked to ageing as well as a host of diseases.
The water was also found to beat other brands in hydrating the skin, making it appear more youthful and smoothing away wrinkles.
If that were not enough, previous studies have credited the water with easing the pain of arthritis and thwarting the growth of cancerous cells.
Scientist Dr Mary Warnock said: 'We didn't think something as simple as water would necessarily have benefits other than boosting rehydration.
'We are all looking for simple ways to stay healthy and look our best.  Deeside mineral water could be one of those solutions and therefore much more research is required in this area.'
Dr Warnock, of Queen Margaret University in Edinburgh, carried out experiments designed to gauge the ability of various waters to mop-up free radicals.
Produced when food is turned into energy, these dangerous molecules attack the body's cells and have been linked to ageing, as well as diseases such as cancer, Alzheimer's and Parkinson's.
The lab tests showed the Scottish spring water to be better than other bottled waters and than tap water at warding off attack.
Dr Warnock said: 'There are a number of foods and drinks known for their free radical scavenging properties.
'But I don't know of any other water that does it to the level of Deeside water.'
The second series of experiments, carried out at Leeds University, focused on the water's ability to hydrate the skin.
Twenty two young women drank a litre of Deeside water or of tap water a day for several months and the health of their skin was charted.
 
The analysis showed the spring water was better at hydrating the skin and that it seemed to have smoothed away some of the women's wrinkles.
It is thought the effect may be, at least in part, due to the combination of minerals the water gathers during the 50 years it is filtered through granite and peatland before being bottled.
The healing powers of Deeside water were first noted 250 years ago when a woman was cured of scrofula - an infection of glands in the neck - after bathing in and drinking the waters.
As word spread, a spa grew up at nearby Ballater to accommodate those who wished to sample the springs.
Notable visitors included Sir Walter Scott, Lord Byron and Queen Victoria, who recorded sampling the water in her Highland Journals of 1856.
The Queen stays at Balmoral every summer while the Queen Mother also loved visiting Scotland and spent long periods on the Deeside estate.
Martin Simpson, managing director of the Deeside Water Company, which funded the research, said: 'All waters are not the same - Deeside really is different.'
