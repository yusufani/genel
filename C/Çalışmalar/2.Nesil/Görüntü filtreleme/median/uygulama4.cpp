#include <stdio.h>
#include <string.h>
#include <math.h>
#include "image_processing.cpp"
#define filterWidth 11
#define filterHeight 11
void combsort(int * data, int amount);

int main(void){
	//Verilen isimdeki resim okunuyor.
	int M, N, Q;
    bool type;
    char resimadi[] = "lenasp.pgm";
	readImageHeader(resimadi, N, M, Q, type);
	int** resim = resimOku(resimadi); 
	//Okuma bitti
	int** resim_median=resim;
 	int count=0;
	int x,y; //�evrim de�i�kenleri
	int filterX,filterY;
	int pixel_values[filterWidth * filterHeight];
	int w=N-(filterWidth/2);
	int h=M-(filterHeight/2);
	int x0=filterWidth/2;
	int y0=filterHeight/2;
	for(x = x0; x < w; x++)
	{
		for(y = y0; y < h; y++)
		{
		int n = 0;
        //set the color values in the arrays
        for(filterX = 0; filterX < filterWidth; filterX++)
        	{	
				for(filterY = 0; filterY < filterHeight; filterY++)
	        		{
	            	int imageX = (x - filterWidth / 2 + filterX + w) % w;
		            int imageY = (y - filterHeight / 2 + filterY + h) % h;
		            pixel_values[n] = resim[imageX][imageY];
		            n++;
		            count++;
	        		}        
			}
        combsort(pixel_values, filterWidth * filterWidth);
        resim_median[x][y] = pixel_values[filterWidth * filterHeight/2];
		}
	}
	char yeniresimadi2[] = "mymedian_11";
	strcat(yeniresimadi2, resimadi);
	resimYaz(yeniresimadi2, resim_median, N, M, Q);
    printf("Median Filtre basariyla tamamlandi :) %d satir %d sutun %d ad�mda islendi \n",N,M,count);

	system("PAUSE");
	return 0;
}
void combsort(int * data, int amount)
{
    int gap = amount;
    bool swapped = false;
    while(gap > 1 || swapped)
    {
        //shrink factor 1.3
        gap = (gap * 10) / 13;
        if(gap == 9 || gap == 10) gap = 11;
        if (gap < 1) gap = 1;
        swapped = false;
        for (int i = 0; i < amount - gap; i++)
        {
            int j = i + gap;
            if (data[i] > data[j])
            {
                data[i] += data[j]; 
                data[j] = data[i] - data[j]; 
                data[i] -= data[j]; 
                swapped = true;
            }
        }
    }
} 
