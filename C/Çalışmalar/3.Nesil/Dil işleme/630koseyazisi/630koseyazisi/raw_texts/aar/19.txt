K�t� m�d�r kad�n d��k�n� olmak? 
Benden size tavsiye: E�er kitab�n�z� yazacaksan�z, kendiniz yaz�n. �lmeden. Dedikodularla, onun bunun anlatt�klar�yla ak�lda kalmak istemiyorsan�z, i� ba�a d��ecek. Ger�ek neyse, siz onu yazacaks�n�z. �arp�tacaksan�z da, siz �arp�tm�� olacaks�n�z. 

En iyisi bu.

Hi� kimse sizi sizin kendinizi alg�lad���n�z gibi alg�layamaz.

�stelik Ercan Ar�kl� kadar �ansl� olmayabilir, Arda Uskan kadar usta bir kalemin eline d��emeyebilirsiniz.

Arda Uskan, y�llarca Ercan Ar�kl��yla birlikte �al��m�� isimlerden biri, a��k ve net bir bi�imde �Ben bu kitapta taraf�m� diyor, �Onun hakk�nda k�t� bir �ey yazabilmem m�mk�n de�il.�

*

Siz hen�z kitab� okumad�n�z.

Okudu�unuz, gazetelerin kitaptan yapt��� al�nt�lar.

Ne yalan s�yleyeyim, ben de �st �ste gelen Ercan Ar�kl� yaz�lar�n� okuyunca, �Yok art�k daha neler� dedim. Ama nihayet elime Arda�n�n kitab� ge�ti: �G�le G�le Bebe�im...�

A aaaa...

�a��rd�m.

Gazetelerde okudu�umuz, 400 sayfal�k kitab�n sadece 15 sayfas�.

Bu, ba�ka bir �ey.

Kitab� elimden b�rakamad�m.

Bir kere �malzeme� iyi.

Ercan Bey bir roman kahraman� olarak ola�an�st�.

Herkesin kabul edece�i gibi, adam�n hayat� film zaten.

G�zya�� var, dram var, felaketler var, h�nz�rl�klar, muz�rl�klar var, ba�ar� var, a�k var, seks var, para var, ��hret var, yaln�zl�k var, h�z�n var...

Otob�s �arpmas� gibi tuhaf bir final var.

Yani insan�n etkilenmemesi m�mk�n de�il.

Herkesin de vurgulad��� gibi, o hayatta olsayd�, bu �malzeme� kar��s�nda ellerini ovu�tururdu.

Arda da i�in hakk�n� vermi� do�rusu, sinemac� ya, senarist gibi kare kare kurmu� kitab�.

Araya tan�kl�klar sokmu�.

Kolay okunuyor.

B�l�mler zekice ayr�lm��.

�Ad� Aylin� t�r� bir kitap olmu�, s�rad��� ve farkl� bir karakterle kar�� kar��ya kal�yorsunuz, ama arka planda T�rk dergicilik tarihi i�in ne kadar de�erli biri oldu�unu da anl�yorsunuz.

Arda her �eyi dozunda vermi� s�kmadan, s�k��t�rmadan.

Tatl� bir kitap.

Ama bu kitapla Ercan Ar�kl��y� ��z�mleyebilece�inizi zannetmeyin, zaten Arda bunu s�yl�yor, �B�yle bir niyetim yoktu� diyor, �Ben, benim Ercan Ar�kl��m� anlatt�m.�

Evet.

Bu kitapta okudu�unuz Arda�n�n Ercan Ar�kl��s�.

Onunla zaman ge�irmi�, birlikte olmu�, birlikte �al��m��, omuz omuza durmu� herkes i�in ayr� bir Ercan Ar�kl� olabilir.

Arda�n�nki, �ok zeki, �ekici, esprili, hafif oyunbaz, h�z�nl� ve yaln�z bir adam.

Evet, kad�n d��k�n�.

Evet, �eytan t�y� olan bir kad�n d��k�n�.

Ama kad�nlar�n sevgilisi olan bir kad�n d��k�n�...



ʑG�le G�le Bebe�im�, tam olarak neyin kitab�? Bir d�neme damgas�n� vuran Avrupai dergi yay�nc�l���n�n m� yoksa Ercan Ar�kl� dedikodular�n�n m�?

- Bu kitapta Nokta Dergisi�nin �yk�s� var. Kad�nca�n�n nas�l ��kt��� var. H�ncal Ulu�un a�z�ndan Erkek�e�nin nas�l ��kt��� var. Beyaz Dizi fenomeni var. Bir Geli�im �mparatorlu�u nas�l ��k�yor ortaya ve bat�yor, var...

Ercan Ar�kl�, sizin kitab�n�zda anlat�ld��� kadar kad�n d��k�n� m�?

- Ben bir �ey soraca��m: K�t� m�d�r kad�n d��k�n� olmak? Evet, hayat�n� kad�nlarla payla�may� severdi. Ama Allah a�k�na, neden erkeklerle ya�amay� sevsin ki? Evet, kad�n d��k�n�yd�... Abisi, Ercan Bey�in bu �zelli�inin sonradan edinilmi� bir �ey olmad���n� s�yl�yor. Diyor ki: ���inden gelen bir �eydi, yani kad�nlar� tavlayay�m diye de�il.� Bunu hakikaten hissediyor, kar��s�ndakini krali�e gibi g�r�yor, ona da kendini �yle hissettiriyor. S�k�l�nca da ili�kisini bitiriyor...

�yi de sevi�mek istedi�i kad�nlar�n listesini ��karan, sonuca ula��nca da �entik atan bir adam imaj� ��k�yor kar��m�za...

- Bu 1970�ten 82�ye kadar olan bir hikaye. �entik atmak de�il. Ho�land��� kad�nlar� bir kenara not alm��. Onlar� elde etmek i�in u�ra� vermi�. Ama gizlememi�, o d�nem birlikte oldu�u sevgilisine listeyi g�stermi�. Samimi ve muzip bir durum var ortada. �Yatt��� kad�nlar�n listesini tutan adam� vaziyetleri de�il yani, bu kadar ucuz de�il.

Liste �imdi kimde?

- Bende. Bir art niyetim olsayd�, listedeki o isimleri pekala yay�nlayabilirdim. Akl�m�n ucundan bile ge�medi...

�Ercan Ar�kl�, e�i �nci Ar�kl�, o�ullar� Giray ve Ali �u anda bir arada ve mutlular� gibi �eyler d���n�yor musunuz?

- Hay�r. Benim hi� �yle romantik inan�lar�m yok. �z�r dilerim. Ben diyorum ki... Ercan Bey, �teki tarafta da listesini yap�yordur!.. 
