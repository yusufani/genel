The menswear group Moss Bros became the latest victim of declining consumer confidence yesterday as it warned that it would miss profit targets this year.

Its shares fell by 3.95% to 36.5p as the group, which includes the Cecil Gee and Hugo Boss brands, said customer numbers had fallen sharply over the past seven weeks. Its traditional hire business has also been hit by competition from Tesco, Asda and Matalan. 

Philip Mountford, chief executive of Moss Bros, declined to give precise figures but analysts estimated that group sales were down by 3% since mid-October, having previously been ahead by 1.5%.

"It's certainly a big negative swing," said Mountford, "and we are not alone in this - it's hitting the whole market."

Menswear was always the first to suffer in a spending slowdown, he said. "It's the first thing to go in any household when budgets come under pressure. Womenswear and spending on children come later."

He added his voice to the growing calls for a cut in interest rates. "If you take the average Joe in the street, earning between �25,000 and �50,000 a year, when his mortgage goes up by �150 a month, it makes a huge dent in disposable income."

Sales had fallen across the board, Mountford said, from premium brands such as Ted Baker and Calvin Klein to its Moss range. The hire business had also been hit by growing competition from the supermarkets and the discount retailer Matalan. "They're all advertising along the lines of 'why hire when you can buy for �35?' and it has hit us," he said.

Thanks to a strong second and third quarter, like-for-like sales for the year are still ahead by 1.1% despite the recent slump. Mountford said that, even with the key Christmas trading period still ahead, the recent poor performance made it "unlikely" that profits would meet City forecasts. Analysts expecting the group profits to rise, from �3.4m to about �3.8m, are now cutting those forecasts.

Mountford stressed that the group remained financially strong, with no debt on its balance sheet. "The business is well resourced to both limit any further downside from a weaker consumer environment, and capitalise on the subsequent improvement," he said.

Yesterday's profits warning from Moss Bros follows a growing toll of gloomy announcements from retail and consumer businesses in recent days, including the pub chain Regent Inns, the sofa retailer SCS, the car dealer Pendragon and the restaurant group Clapham House, owner of Gourmet Burger Kitchen and Tootsies.

The B&Q group Kingfisher and DSG, which owns Dixons and Currys, have also warned of tough times ahead, while last week the jewellery group Signet, which owns H Samuel and Ernest Jones, became the first retailer to issue a pre-Christmas profits warning.

The British Retail Consortium also reported a weak rise in sales last month despite big discounts. UK retail sales edged up 1.2% on a like-for-like basis against last November. There were good figures yesterday from Tesco, which said it was looking forward to a strong Christmas.
