Karadut Mektupları

BEDRİ RAHMİ EYÜBOĞLU-MARİ GEREKMEZYAN AŞKININ YAZIŞMALARI İLK KEZ SERGİLENECEK
Karadut Mektupları
 
·       Usta ressam-şair Bedri Rahmi’nin, 1940’larda heykeltıraş sevgilisiyle yaşadığı tutkulu aşkın mektupları ortaya çıktı
 
·       “Hani nar çiçekleri? Hani benim Bedir’im? Her gölgeyi sana benzettim, her sesi senin sandım. Gözlerim istasyon yollarında dondu kaldı. Özledim!”
 
Bir telefon geldi; İş Sanat’tan…
“Bir sergi hazırlıyoruz. Adı:
‘Biz Mektup Yazardık’- Bedri Rahmi Eyüboğlu ve Çağdaşlarından Mektuplar, 1930-1974)”
Harika haber!
Bedri Rahmi'nin el yazısı mektupları sergilenecek. Hem de kimlerle:
Nâzım Hikmet, Ahmet Hamdi Tanpınar, Fikret Muallâ, Âşık Veysel, Adalet Cimcoz, Orhan Veli, Necip Fazıl, İbrahim Çallı, Andra Lhoté, Fahrünisa Zeid, Abidin Dino, Reşat Nuri Güntekin, Cemal Tollu, Nurullah Berk, Arif Kaptan ve diğerleri…
 


Tanıdığımız, sevdiğimiz şairlerin, ressamların, romancıların el yazısı mektupları, ilk kez gün ışığına çıkacak, bizlerle buluşacak. Bedri Rahmi’nin gelini Hughette Eyüboğlu’nun aile arşivini açmasıyla 3 yılda hazırlanan sergi, İş Sanat Kibele Galerisi’nde 5 Mayıs’ta açılacak. 500 sayfalık kitabı da hazır…
 

 
Aşk mektupları
Doğrusu, mektupların hepsini merak ettim; ama yukarıdaki listede olmayan birini sordum ilkin:
“Karadut’un yazdıkları var mı?”
“Var” dediler; verdiler.
10 yıl önce, “Yüzyılın Aşkları” belgesel serisi içinde (Can Yayınevi) Bedri Rahmi Eyüboğlu’nun eşi Eren Eyüboğlu ile aşkını işlemiştim. O aşk, mektuplara kazılıydı. Ancak gün gelmiş, usta ressam ve şair, gönlünü bir başka kadına kaptırmış, evliliği sarsılmıştı.
O kadının adı, Mari Gerekmezyan’dı.
 
 
“Karadutum, çatalkaram, çingenem”
Bedri Rahmi, Eren’den bir çocuk sahibi olduktan hemen sonra tanışmıştı bu genç Ermeni sanatçıyla… Güzel Sanatlar Akademisi’nin heykel bölümüne misafir öğrenci olarak gelmişti. Bedri Rahmi de orada asistandı.
1940’lar başlamıştı. Savaş yıllarıydı.
Bedri Rahmi, Mari’yle gizliden gizliye buluşmaya başlamıştı. Sırılsıklam aşıktı ona…
“Sigara paketlerine resmini çiziyor, körpe fidanlara adını yazıyordu”.
“Karadutum, çatalkaram, çingenem,
Daha nem olacaktın bir tanem,
Gülen ayvam, ağlayan narımsın.
Kadınım, kısrağım, karımsın” şiirini –karısına değil- ona yazmıştı.
 
 
Mavi attaki sevdalılar
Pek çok tablo vardı bu ilişkiden artakalan; pek çok şiir…
Bedri Rahmi onun portrelerini çizmişti; Mari, Bedri Rahmi’nin büstünü yapmıştı.
Ve usta ressam, düşsel bir tabloda sevdalısıyla kendisini, gökyüzünde kanat açan iki atlı olarak resmetmişti.
Ancak ikilinin tutkusunu kanıtlayan, o ünlü şiirdeki aşkı belgeleyen bir mektup ortaya çıkmamıştı.
Şimdi çıkıyor.
20 Haziran’a kadar İş Sanat’a giderseniz Mari’nin mektuplarını görebilir ya da Bennu Yıldırım’ın sesinden dinleyebilirsiniz.
Ama ondan önce, Mari’nin hasret satırları ilk kez bu sayfada yayımlanıyor.
Bedri Rahmi’nin belgeselini yapmış biri sıfatıyla, “Karadut mektupları”na erken ulaşma ayrıcalığına kavuşuyor ve bu tutkulu aşkın satırlarını sizlerle paylaşıyorum:
 

 
 

 
Canım Çebişim
 
“Karadut Mektupları”, “Canım Çebişim” diye başlıyor.
Orta Anadolu’da keçi yavrularına “çebiş” derler…
Mektuplaştıkları yıllarda Bedri Rahmi, dönemin hükümeti tarafından, Anadolu’yu resmetsin diye Çorum’a gönderilmişti. Bedri Rahmi, en verimli çağını o dönem yaşamış, en güzel eserlerini o yıllarda vermişti.
“Çebiş”, Şair’in diline oradan yerleşmiş, ondan da sevgilisine sirayet etmiş olmalı…
İşte o tarihsiz mektuplardan biri:
 
 
Deli gibi koştum arkandan
“Canım Çebişim,
Belki eski günleri hatırlarsın da sokağın başında beni beklersin diye deli gibi arkandan yokuştan aşağı koştum, fakat kayıp olmuştun. Saatlerce siluetini aradım, nafile. Her gölge beni aldatıyordu. Benim canım Çebişim! Canım Çebişim! Canım Çebişim! Canım Çebişim!
Çebiş, hemen seni özlemeye başladım bile…”
 
 
Başımda müphem hisler
Bir tarihsiz mektup daha… Mari’nin okuduğu Esayan Lisesi’nden yazılmış olması muhtemel:
“Çebişim!
Dün sabah Esayan’ın kapıcısına beni arayan olursa hemen çağırmasını, müdüre çıkıp bir telefon beklediğimi, derhal beni haberdar etmesini söyledikten sonra sınıfa girdim.
Beni muhakkak arayacağına inanıyordum.
Bugüne kadar bütün perşembelerinde saat 1’de buluştuğumuz, hayde Çebiş oraya yürü... yürü yürü!..
Her şeye rağmen kendimi çok hafif hissediyordum, aptal aptal yürüdüm, aptal aptal etrafıma bakındım, aptal aptal anneme çattım, daha doğrusu hiçbir şey duymuyordu, evvelsi günkü hadisenin ağırlığını, beni, ezici tarafını anlayamıyor. Yalnız başımın üzerinde müphem, bana ait olmayan birçok hislerin dolaştığını seziyor; fakat bir türlü onları benimseyemiyordum.
İçinde bulunduğum anı iyi veya kötü yaşayabilmem için seni görmem lazımdı.
‘Bir at gelir dost mudur
Düşman mı bilmem.’
 
 
Bana ıstırap verdin
Gördüm ve duydum ki dün seni aramam hiç doğru değilmiş, fakat bunu da yapamazdım.
Hayret başımın üstündeki o müphem sandığım hisler birer şekil ve mana aldı. Melek veya şeytan olduklarını tefrik edemezken hepsi birer şeytan kılığına girdiler.
Bir gün evvelki bütün kelimeler manasını değiştirip Perikles Kılıcı gibi sallanmaya başladılar. Ne tuhaf! İnsan kelimelere kendi içinin istediği manayı, rengi veriyor.
Çarşambanın azabını dün duydum, canım Çebişim bana tasavvur edemeyeceğin kadar ızdırap verdin.
 
 
Fenayım, beni kurtar
Bugün yüzümü görsen, dünkü müphem olmasına rağmen iyi bir şeyler ümit eden içimin şavkından hiçbir eser kalmadı.
Nedir bu beni ezen, boğan, nefes aldırmayan hisler? Ben sana inanmak istiyorum, yalnız sana.
Canım Çebişim, beni bunların elinden kurtar... Çok fenayım.
Dün akşam sana uğradığımı söyledim, başka şeyler de konuştuk ama seni gördüğümde anlatırım.
Bugün için Adalet’e (Cimcoz) gideceğimizi söyledi. Benim gitmek istemediğimi kendisinin yalnız gitmesini ileri sürdüm.
–Yoo! Olmaz.
Bilmem ne olacak? Ben bugün muhakkak gelmeye çalışacağım, şayet gelemezsem yarın cumartesi sabah saat 10’da gel canım Çebişim, ne olursun beni kapıda bekletme.
 
Çebiş”
 
 
 
Seni deli gibi seviyorum
 
27 Mayıs 1945
 
Direnidim!
Trendeyim, daha bilmiyorum nereye ve niçin gidiyorum? Aptallar gibi etrafıma bakınıyor, ne yapacağımı düşünemiyorum.
Biliyor musun hayvanları boğazlamak için bir yere götürürler, Türkçesini bilmiyorum ama Ermenice ‘Iskartaroz’ denir.
Ah! Hatırladım ‘mezbaha’ değil mi? Oraya sürüklenen hayvanların gözlerinde nemli nemli bir mana var. İşte aynada oraya giden bir Çebiş gözleri gördüm.
... Hani nar çiçekleri? Hani benim Bedir’im? Her gölgeyi sana benzettim, her sesi senin sandım. Gözlerim istasyon yollarında dondu kaldı.
Özledim!
 
 
Susadım gibi bir şiir
Daha yola çıkmadan ‘Susadım’ gibi bir şiir yazabilseydim.
Canım Bedir, o kadar fenayım ki! Başım üzerimde duruyor, cayır cayır yanıyorum. Tren müthiş sarsıyor, yazmanın imkânı yok.
Niçin gidiyorum?
Muhakkak yazacağım, şayet Yozgat’tan yollayamazsam kendim getiririm.
Etrafımda senin gözlerinden başka bir şey görmüyorum. Tünellerde, yaylalarda, vadilerde, dağlarda hep onlar trenle yarışıyor, tıpkı karanlıkla boy ölçüşen ateş böcekleri gibi cıvıl cıvıl.
Çebişim
Şimdi Hasanoğlan’dan geçiyoruz. Eserini görmek için pencereye koştum...
Canım Bedir seni deli gibi seviyorum.
 
 
Seven kaybediyor
 
10 Eylül 1945
Karacam,
İşte yine atelyedeyim! İşte yine Çebişten hiçbir haber yok!
Canım Bedir anlamıyorum ne diye cevap vermiyorsun?
Vapurun kalkıncaya kadar bekledim, sonra atelyeye dönüp sana uzun bir mektup yazmış ve o günküyle postaya atmıştım.
Daha sonra Perşembe günü senin onlarla beraber döneceğine o kadar inanmıştım ki, tek!
Geç vakitlere kadar hep seni bekledim.
Cumartesi:
‘Neyleyim!..’ diye bir telgraf çektim.
Hâlâ susuyorsun canım Bedir nen var? Yoksa hasta mısın?
Hemen yaz ve eğer kabilse üç gün için buraya gel de yaz geçmeden çebişler birbirlerini bir daha görsünler.
Gel gör, senin çebiş tam bir çingene kızı oldu.
Fazla yazamayacağım fena halde asabım bozuk. Ben zaten kötü kötü şeyler düşünürken bir de üstelik...
‘Seven kayıp ediyor!’
Halikarnas da burada.
Üç gün için bir şey icat et olur mu Karacam?
Geleceğin günü telle de iskeleye geleyim.
Seni iskelede bekleme arzuma kötü hatıralar karışmış olmasına rağmen içimde bir yerde öyle canlı ve tertemiz duruyor ki!
Bunu olsun Çebişinden esirgeme. Zavallı Çebiş vapur kalkıncaya kadar ürpere ürpere orada demir parmaklığın arkasında bekledi... bekledi...
Hâlâ bekliyor!
Çebişe
 
(Yahya Kemal’in büstü de seni bekliyor! Burgaz da seni bekliyor!
Her şey seni bekliyor.)

 

Susadım

Mari’nin mektubunda atıf yaptığı Bedri Rahmi’nin “Susadım” şiiri şöyle:    

“Susadım 
Üç tane elma soydular, üç tane portakal 
Nafile 
Bir bardak suyun yerini tutmadı. 
Acıktım 
Kuş sütü, kuru üzüm getirdiler 
Nafile 
Bir çimdik somunun yerini tutmadı. 
Seni düşündüm sevgilim şükrederek 
Su gibi aziz olasın her daim 
Ekmek gibi mübarek.”
 
 


EL YAZISI İKİ NOT

Sizi tatlı uykularınızdan uyandırmak istemedim. Çok çok teşekkürler. Allahaısmarladık.
 
Mari
 
 
Çay içmeden gidiyorum zannetmeyin sakın! Güner bana fevkalade bir çay hazırladı.
 
 
 
Hazin final
 
Sonra ne oldu?
Bu tutkulu aşk, hazin bir finalle son buldu.
Bedri Rahmi’nin,
“Önde zeytin ağaçları, arkasında yar/
Sene 1946, mevsim sonbahar” şiirini yazdığı yıl, “Karadut” hastalandı. Ağır bir tüberküloz geçiriyordu. Antibiyotiğe verecek parası yoktu.
İmdadına Bedri Rahmi yetişti. En kıymetli tablolarını yok pahasına sattı; ona ilaç aldı. Ama yetmedi.
“Karadut”, 1946’da, İstanbul’da, Alman Hastanesi’nde vefat etti.
Bedri Rahmi ardından kendini içkiye vurdu.
“Türküler bitti/ halaylar durdu/ horonlar durdu/ al damar, mor damar, şah damar sustu” diye yazdı sevdiğinin ardından…
“Karadut”u defnettikten sonra gözyaşları içinde eşine döndü.
Eren, onu sevgiyle bağrına bastı, teselli etti, yatıştırdı.
Ancak yaşadıkları “Karadut” parantezini ikisi de unutmadı.
1949’da bir gün Büyük Kulüp’teki bir gecede dostları Bedri Rahmi’den “Karadut”u okumasını istediler.
Şair ayağa kalktı; şiire başladı; okurken gözyaşına boğuldu.
Mari gitmiş, ama aşkı bitmemişti.
Eren, o günden sonra Paris’e yerleşmeye karar verdi.
Bir süre ayrı yaşadılar.
Sonra yeniden buluşup yıllar yılı birlikte sanat ürettiler.
Bedri Rahmi, 1974’ün bir Eylül günü, 63 yaşında hayata veda etti.
Cenazesinden sonra Eren eve geldi. Artık 35 yaşına gelmiş oğlu Mehmet’i karşısına oturttu ve dedi ki:
“Babanı uğurladık, ama şunu bil ki ona çok kırıldım. Yaşadığı ilişkiyi unutmadım. Buna katlandımsa, sadece senin hayatın kararmasın diyedir.”
Ölene kadar bir daha bu konuyu açmadı.