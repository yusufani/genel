E�itimde yap�lmas� gerekenler 
�ZEL Okullar Derne�i'nin art�k bir gelenek haline getirdi�i Antalya Sempozyumu'nda bu y�l tart���lanlar benim zihnimde �u kanaatleri olu�turdu veya peki�tirdi. 

1) 21. y�zy�la giden T�rkiye'de e�itimin merkezi bir yap�da inat ederek s�rd�r�lmesi imk�ns�zd�r.

E�itim ivedilikle yerel y�netimlere devredilmelidir.

Yerel ihtiya�lar ile k�resel dayatmalar� bir arada hazmetmeyecek, ba� d�nd�ren, h�zla de�i�en teknolojiye an�nda tepki vermeyecek e�itim 21. y�zy�la katiyen yeti�emez.

Bu a��dan iddial� bir ��kamu reformu��na soyunan h�k�metin, e�itimi yerel y�netimlere devretmeyerek bu konuda stat�koya ma�lup olmas�, ��iktidar olma�� iddialar�yla �eli�mektedir.

2) E�itimde merkezi yap�n�n g�revi ara�t�rma-geli�tirme (talim-terbiye), standart ve norm geli�tirme ve denetim ile s�n�rl� olmal�d�r. (Genelkurmay!)

Elinde yasama-yarg�-y�r�tme yetkisini tutan merkezin, yerel yetkililerden korkmas� ak�l ile izah edilemez.

3) Bedava e�itim safsatas� Anayasa'n�n e�itlik ilkesini de�il peki�tirmek, daha beter bozmaktad�r. Devletin b�y�k �ehirlerin zengin muhitlerinde bedava e�itim hizmeti vermesi; vergi m�kellefine darbe, esas hak sahibine de b�y�k haks�zl�kt�r.

* * *

4) Devletin asli g�revi sadece �deme g�c� olmayan ailelerin okul �cretlerini kar��lamakt�r. E�er e�itim paral� olur ve �deme g�c� olanlar �demeyi yaparlarsa, �deme g��l��� �eken ailelere daha fazla kaynak ayr�labilecek, b�ylece bu ailelerin sadece okul �cretleri �denmeyecek, ayr�ca onlara defter-kitap-har�l�k yard�m� yap�labilecektir.

5) �ster �zel, ister kamu kar��las�n, e�itim hizmeti de bir �retim faaliyetidir.

Okullar�n arzuland��� gibi verimlilik ve kalite kavramlar� ile bezenmesi isteniyorsa, t�m eksikliklerine ra�men daha iyisini �neremedi�imiz piyasa ko�ullar� okullara da y�n veren as�l y�nerge olmal�d�r.

Okullar kendi �cretlerini kendileri tayin edebilmeli, ��retmen istihdam�n� ve �cretlendirilmesini ��retmen-veli-y�netici ��leminde kendi yapabilmelidir.

* * *

6) B�yle bir anlay���n ba�ar�l� olabilmesi i�in e�itim kurumunun e�itim y�netimi konusuna daha �a�da� ve profesyonel bir g�zle bakmas� laz�md�r.

Okul m�d�r� tecr�beli ��retmenden �ok daha fazla bir insand�r.

7) �zel okullarda (veya paral� kurumlarda) ��renci okutan veliler, �dedikleri vergilerin e�itime giden pay�n� �dememelidirler. Aksi halde �imdi oldu�u gibi �ifte �deme yap�lmaktad�r.

�rne�in; e�er e�itimin b�t�ede pay� % 15 ise, e�itilmi� insan�n katma de�erinin 2/5'inin kendine kald���, 3/5'inin kamuya mal oldu�u varsay�m� ile bu veliler 0.15x0.40 (2/5)= % 6 daha az vergi �deme hakk�na sahip olmal�d�rlar.

* * *

T�m varl���m ile inan�yorum ki, T�rkiye'nin 21. y�zy�lda hangi ligde oynayaca��na ivedilikle e�itim kurumu karar verecektir.
