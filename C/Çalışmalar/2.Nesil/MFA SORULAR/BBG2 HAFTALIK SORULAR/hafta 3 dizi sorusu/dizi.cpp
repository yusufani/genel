/*Bir dizi tan�m�:
	a(0) = 0; 
	a(n) = a(n-1) - n if a(n-1) > n ise ve a(n-1) - n dizide o ana kadar yer almam��sa  
	a(n) = a(n-1) + n  else 
	dizinin ilk elemanlar� 0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11, 22, 10, 23, 9, 24, 8, 25, � 
	Girilen bir n say�s� i�in bu dizinin n. eleman�n� bulan algoritmay� yaz�p kodlay�n�z. 
	�N sonsuza giderken bu dizi t�m pozitif tamsay�lar� i�erir� �nermesini ara�t�r�n�z.
*/
#include <stdio.h>
int a[1000],i,n;
int fonksiyon (int i){
	return a[i-1]+i;
}
int main(){
	a[0]=0;
	int flag=0,j;
	printf("Lutfen dizinin kacinci elemanini istediginizi giriniz:");
	scanf("%d",&n);
	for(i=1;i<=n;i++){
		flag=0;
		j=0;
		if(a[i-1]>i){
			a[i]=a[i-1]-i;//terimi kar��la�t�rmak i�in yap�ld� yoksa bir �nceki terimi kar��la�t�r�r.
			while((j<i) && (flag==0)){
				//printf("a(j) = %d  ",a[j]);
				if(a[j]==a[i]){
					flag=1;
				}
				else{
					j++;
				}
			}
			if(flag==1){
				a[i]=a[i]+i-a[i-1];//normale dondurme i�lemi 
				a[i]=fonksiyon(i);
				//printf("girdik");
			}
		}
		else{
			a[i]=fonksiyon(i);
		}
		printf("%d.deger=%d\n",i,a[i]);
	}
	printf("\nAranan cevap : %d\n",a[n]);
	printf("*********Buradan asagisi siralanmis haldedir.***********************\n");
	int min,tmp;
	for(i=0;i<=n-1;i++){
		min=i;
		for(j=i;j<=n;j++){
			if(a[j]<a[min]){
				min=j;
			}
		}
		tmp=a[i];
		a[i]=a[min];
		a[min]=tmp;
	}
	for(i=0;i<=n;i++){
		printf("%d\n",a[i]);
	}
}
