﻿Bakırköy’e mi taşınsak? Orda ceza yokmuş

Gazetede arkadaşlarla “Acaba Bakırköy’e mi taşınsak” diye konuştuk.
Niye mi?
Yaptığımız haberler bizim mahallede suç; Bakırköy’de suç değil de ondan…
Adliye’ye bir baskın oldu, bir savcı şehit edildi. Savcıyı rehin alanlar öldürüldü, yataklık edenler bulunamadı. Kapıdan rahatça girmelerine göz yumanlar sorgulanmadı. Ama hadisenin fotoğrafını basan 18 gazeteci hakkında 7,5 yıla kadar hapis isteniyor.
Davayı neresinden tutmalı?
DHKP-C’nin başına silah dayadıklarının fotoğrafını basmak “teröre destek” sayılırken, IŞİD’in boynuna bıçak dayadıklarının fotoğrafını basmanın serbest olmasından mı?
Aynı fotoğrafları kullanan yandaş medyanın suçlanmamasından mı?
Yoksa aynı habere Çağlayan’da bakan savcı bunun 7,5 yıl hapis gerektiren bir suç olduğuna hükmederken, Bakırköy’de bakan savcının soruşturmaya gereç duymamasından mı?
Bir hukuk devletinde böyle garabet yaşanabilir mi?
Savcılardan bizi yargılatırken gösterdikleri cevvaliyeti, suçluları yakalamakta göstermesini beklemek hakkımız değil mi?
 
* * *
 
Bununla da sınırlı değil ki:
“Teröre yardım yataklık” iddianamesinden hemen sonra, dün hukuk servisimiz üç yeni hakaret davasının haberini verdi.
“Hakaret” dedikleri şeyler, gazetede çıkan gündelik haberler… Bir siyasetçi demeç vermiş, Meclis’te bir milletvekili bir söz söylemiş; onların söylemesi suç değil, bizim yazmamız suç…
Belli ki Hünkâr, “Yıldırana kadar üstlerine gidin” emri vermiş.
 
* * *
 
Bir de Almanya’ya bakın:
Bir internet sitesinin iki editörü hakkında, Alman devletinin sosyal medyada takip sınırlarını genişlettiğine dair gizli bir raporu yayınladığı gerekçesiyle soruşturma başlatıldı.
Savcı, gazetecileri “vatan hainliği” ile suçladı.
Tabii bizim yandaşlar hemen işin üzerine atladı.
“Almanya yargılıyor, biz niye yargılamayalım” diye tezahürata başladı. Çok bilmiş bazıları, “Bu Batı böyledir işte; kendisi yargılar, biz yargılayınca kızar” diye tartışmaya daldı.
Sonuç?
Kamuoyu ayağa kalktı. Yürüyüşler yapıldı. Alman medyası topyekûn gazetecilere sahip çıktı. Başbakan ve siyasi partiler savcıya tavır aldı. Ve sonunda savcı görevinden alındı.
Şimdi Adalet Bakanı’nın koltuğu sallanıyor.
“Almanya’yı örnek alın” diyen bizimkiler suspus…
 
* * *
 
İddianame açıklandıktan 2 saat sonra Büyükçekmece Belediyesi’nin “Yaşam boyu onur ödülü”nü aldım.     Orada da söyledim:
Ödüller ve cezalar peş peşe geliyor.
Alkışlar ve yuhlar…
“Yeter dur”cularla, “Durma yürü”cüler….
“Biz sana gösteririz” diyen yandaşlarla, “Yanındayız” diyen yoldaşlar…
Ve bu kargaşada hakkıyla gazetecilik yapmaya çalışan bizler…
Bunlar ne ki; gerçeği yansıtmanın bedelini canla, kanla ödemiş bir gazetede çalışıyorum.
İstediğiniz kadar soruşturma, dava açın,   tehdit, hararet edin, yargılayın; gerçeği yazmaktan vazgeçmeyeceğiz.
Demokrasi, basın özgürlüğü, hukuk devleti mücadelesinin bir parçasının da yargılanmak olduğunu bilerek, yandaşların sataşmalarına zerrece pirim vermeyerek ve asıl yargılanması gerekenler adalet önüne çıkana dek, yılmadan gazeteciliğimizi sürdüreceğiz.