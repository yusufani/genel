Hekimin re�etesinden elinizi �ekin  
       
BURSA'dan Dr. Kenan Ergus, SSK Genel M�d�rl���'n�n 17.1.2002'de Resmi Gazete'de yay�nlanan ila� y�netmeli�ine g�re, bundan sonra SSK'l�lara antidepresan ve antipsikotik ila�lar�n sadece psikyatri uzmanlar� taraf�ndan re�eteye yaz�labilece�ini veya ki�inin bu ila�lar� kullanmas� gerekti�ine dair heyet raporunun varsa di�er hekimler taraf�ndan da yaz�labilece�ini an�msatt�ktan sonra ��yle diyor:

��SSK'n�n tedavi giderlerini azaltmak i�in birtak�m tedbirler almas� do�ald�r. Tedbirler al�rken bilimsel ve etik de�erleri de g�zetmesi gerekir. Antidepresanlar sadece depresyonda kullan�lmazlar; migren, psikosomatik bozukluklar, kimi gastro-enterolojik hastal�klar ve a�r�l� durumlar ba�ta olmak �zere bir�ok alanlarda kullan�l�rlar. Antidepresanlar�n gelenekselle�mi� isimlerinden hareketle sadece depresyonda kullan�labilece�ini sanmak bizim dar kafal� b�rokratlar�m�za yara��r bir anlay�� olabilir ama d�nyan�n hi�bir yerinde akla hayale gelmeyecek bir komedidir. Bu karar� alanlar zahmet edip antidepresanlar�n prospekt�slerini incelemi� olsalard�; depresyon d���nda hangi hastal�klarda kullan�labilece�ini g�rm�� olurlad�. Anla��lan bu kadar basit bir �aba g�stermeyi bile gereksiz g�rm��lerdir. Kald� ki depresyon, iyi hekimlik yapan her doktorun hi�bir laboratuvar tetkikine gerek duymaks�z�n sadece iyi bir g�zlem ve anamnezle (hastan�n �ik�yetlerini sorgulama) tan� koyabilece�i bir hastal�kt�r.��

Dr. Ergus, usuls�z i�lemlerin �n�ne ge�ilmek isteniyorsa kay�td��� i��ilikle, hak edilmeden al�nan emekli maa�lar�yla, ihalelerde d�nen yolsuzluklarla m�cadele edilmesini istiyor ve ��E�er bu konuda yolsuzlu�a bula�m�� doktor varsa; hi� ku�kunuz olmas�n, onlara haddini bildirirken iyi hekimlik yapan doktorlar da yan�n�zda olacakt�r. Ama l�tfen iyi hekimlik yapan doktorun re�etesinden elinizi �ekin�� diyerek �u �rne�i veriyor:

��1 milyonluk Laroxyl i�in, 8-9 milyonluk Anafranil i�in, Tofranil i�in g�nlerce s�recek ve belki hi� muayene edilmeden verilmek durumunda kal�nacak heyet raporlar�yla vatanda��n depresyonunu daha da derinle�tirmeyin. On binlerce migrenli, irritabl kolon sendromlu vb. hastalar� d���nerek bu rezalete son verin. L�tfen ald���n�z kararlarda bilimselli�i, etik de�erleri, hasta-hekim ili�kisini ve �lke ger�eklerini g�z �n�nde bulundurun, kendinizden ba�ka herkesi defolu g�rme ve ��Ben yapt�m, oldu...�� anlay���n� terk ediniz.��



Urfal�lara TEDA� zulm�


��ANLIURFA TEDA�'ta ge�en y�l ya�anan yolsuzluk olay�ndan sonra bu kurum adeta kangrenli bir hastaya d�n��t�. Vurgunu vuranlar�n �arpt��� trilyonlar yan�na k�r kal�p sokaklarda gezerlerken olan binlerce �anl�urfal�'ya oldu. Saya� okuyan ta�eron firmalar�n anla�malar� ask�ya al�nd� ya da feshedildi. Sonu�ta 6-7 ayl�k 500-600 milyon gibi y�ksek faturalar binlerce fatura vatanda�a ��kt�. Taksitlendirme ile bunlar� g��l�kle �demeye �al��t�lar. Aradan ge�en zaman i�inde iyile�me g�r�lmedi; i�e politika girince bir y�l i�inde �� m�d�r arka arkaya de�i�tirildi. Bu arada tabii korkudan yeni m�teahhitlerle anla�ma sa�lamaya �� m�d�r de yana�amad�. Bu ihmalin ikinci b�y�k faturas� yine abonelere ��kt�. Bir kez daha y�zlerce milyonluk faturalar da��t�ld�. Nas�l olsa kurum su�lu diyerek bor�lar�n� taksitlendirmeye giden insanlara b�y�k bir pi�kinlikle 'Taksitlendirme art�k yok. O bir kereye mahsustu. Bilgisayar�n program� da ona g�re haz�rland�. Bilgisayar da bunu kabul etmiyor'' yan�t�n� verildi. D�nyan�n hi� bir yerinde hem su�lu hem de g��l� olman�n b�yle bir �rne�i yoktur. Bilgisayar�n kabul etmedi�ini hi�bir vicdan kabul etmez. Eskiden halk g�nlerinde ila� g�da yard�m� isteklerinin yerini �imdi milyonluk elektrik faturalar�n�n �denmesi yakar��lar� ald�. Vatanda�a art�k zulmetmeyin.�

Mehmet ALTUNDA�-�anl�urfal� TE�ADZEDELER ad�na


T�rk Gecesi maskaral�klar�


'T�RK Gecesi' ad� alt�nda d�zenlenen e�lenceler yetersiz ve hatal�d�r. Bizleri de rencide etmektedir. Dans�zler Arap havalar� ile dans etmekte; turistlere sanki Arap �lkesiymi�iz gibi y�zme havuzu etraf�nda fesli adamlarca deve gezileri yapt�r�lmaktad�r. M�zisyen ekipleri tam bir 'Sulukele ekibi' g�r�n�m�ndedir ve T�rk imaj� ile uzaktan yak�ndan ba�lant� kurulacak bir durumda de�ildir. K�yafetler, bir �ad�r tiyatrosu ortam�ndad�r. En k�t�s� de b�yle bir ortam�n g�zelim T�rk bayraklar� ile s�slenmesidir. Tatil k�yleri taraf�ndan y�redeki T�rk k�lt�r�n� bilmeyen �zel giri�imcilere ihale edilen geceler, tam bir panay�r g�r�nt�s� vermektedir. Bu geceler yasaklanmas�n ama bu organizasyonlar�n nas�l yap�laca�� ba�ka k�lt�rel boyutlarda bi�imlendirilmelidir. T�rk imaj�, dans�zl�, develi, fesli g�sterilere b�rak�lamaz.

Abdullah �EVK�-ANKARA


Van kedilerinin ac�nacak hali


23 N�SAN tatilinde Van'a gezi yapt�k. Daha �nce gazetelerden ��Van'a gelin ve kedilerimizi g�r�n, sevin. Koruma alt�na ald�k�� diye okuduklar�m kedilere olan tutkumu art�rd� ve rehberden rica edip Van 100. Y�l �niversitesi b�nyesindeki kedi bak�mevine gittik. Ancak kedievi kapal�; saat 10'dan 12'ye kadar bekledik; bak�c� gelmedi. Kedilerin durumu i�ler ac�s�. Kedi yollar� yap�lm�� olan yerlerden tel �rg�l� bah�elerine ��k�yorlar. Kiminin kulaklar�n�n yan� uyuz, kiminin o g�zelim g�z� akm��, kiminin kuyru�u yolunmu� gibi tel-tel, g�z ya�art�c� bir manzara... Kedievi mi yoksa kedi tutukluevi mi karar veremedik. Baz� kediler odalarda hapis...

Van'�n meydan�na kedi heykeli dikmek marifet de�il! Marifet canl� kediye bakmak. T�r� yaln�z bize ait olan bu kedilerin bak�c�s�n� da e�itmek laz�m. �lgililere sesleniyorum. Van kedisine sahip ��ks�n.

Ay�e �ENOK-�STANBUL


Su�lu, tepkisiz kalanlarda


�ATAT�RK�� b�rokratlar, Tayyip mitingini (Rize) g�rmediler mi?� diye soruluyor. Ne yaz�k ki o g�nlerde medyam�z�n gerekli tepkiyi g�stermedi�ini s�yleyebilirim.

12 Eyl�l askeri ve sonras�ndaki s�zde sivil y�netimin, T�rk-�slam sentezini esas alan ideolojisinin gere�i olarak �slami hareketlili�e yakt��� ye�il ���k, bir k�s�m siyaset adam� ve b�rokratlar taraf�ndan �ok iyi kullan�larak, bu sentez �slami yan� a��r basan �slam-T�rk sentezi haline getirilmi�tir. Eski Cumhurba�kan� Demirel'in �slam birli�inden s�z etmesi, Kur'an'�n getirdi�i nizami ya�am�n�n gerekti�ini savunmas�, dini e�itime �nem vererek 28 �ubat �ncesindeki 600'� a�an �mam Hatip okullar�n�n yar�s�ndan fazlas�n� a�makla �v�nmesi ve nihayet Tevhid-i Tedrisat Kanunu'nun yanl��l���n� ileri s�rmesi hep 12 Eyl�l'�n siyasal �slam'�n palazlanmas�na neden olan y�netim s�reci i�inde olmu�tur. Bu s�re� de laikli�in Kur'an ayetlerinde aranmas�yla ba�lam��t�r.

Siyasal �slam'�n iktidara gelme �ans�n�n artt��� bu d�nemde, bu y�netim i�inde yer alabilme d���ncesi, zaman�n siyaset adamlar�n�n ve siyasi b�rokratlar�n�n bu ve benzeri �slami ��k��lar� kar��s�nda sessiz ve tepkisiz kalmas�n� gerektirmi�tir. Ayn� s�re� i�inde 12 Eyl�l'�n T�rk-�slam sentezi ideolojisine yatk�n yeti�tirilen ve kendilerine '12 Eyl�l Atat�rk��leri' denen kesimden de ciddi say�labilecek bir kar�� tav�r da beklenemezdi zaten. Birka� y�rekli ger�ek Atat�rk�� de bu �zel Atat�rk��ler aras�nda seslerini duyuramam��t�. 

Dr. �erafettin YAMANER �ehremeni-�STANBUL


Durmu��un kaz���


EN kahraman 'sa�l�kl�' bakan�m�zdan al�n size bir bomba; �zel hastanelerde bile 12.5-15 milyona bak�lan 'otomatik hemagrom' tahlili Sa�l�k Bakanl��� hastanelerinde ka�a bak�l�yor biliyor musunuz? Tam 20 milyon liraya! Peki bunun maliyeti ne biliyor musunuz? Tam 200 bin lira! �stedi�iniz Devlet Hastanesi'ne gidin hemagroma bakt�r�n sizden 20 milyon lira al�nacakt�r. Herhangi bir �zel hastanede ise bunun % 60-70'ine ayn� tahlili yapt�rabilirsiniz.

Benden uyarmas�.

H�seyin D�Z-ORTAK�Y


G�N�N S�Z�


��Bir af yasas�n� bile elinize y�z�n�ze bula��rd�n�z, 'T�rkiye'yi y�netmeye haz�r�m�� demeye hakk�n�z yok.''

(Ayberk �.-G�ZTEPE)


MESAJ 


B�RLE�EN bankalardaki personel arkada�lar�m�z toplam hizmet s�releri kadar tazminatlar�n� al�rken biz Etibankl�lar hizmet s�remiz ne olursa olsun (20 y�ll�k olanlar var) 3 senelik tazminat ald�k. Dava a�mam�za ra�men kar��m�zda Etibank'�n t�zel ki�ili�i kalmad���ndan muhatap bulam�yoruz. Kanunda belirtildi�i �zere bir i�yerini sat�n alan firma personelin b�t�n �zl�k haklar�n� devralmas�na ra�men ge�mi� hizmetlerimiz yok say�larak resmen gasp edilmi�tir. Yeni bir i� bulmak i�in limit ya� olan 30 ya��nda olmana m� yanars�n, i�ini kaybetti�ine mi, 'Hukuk devletiyiz nas�l olsa hakk�m�z� al�r�z' diye d���n�p safdilli�ine mi yanars�n art�k sana kalm��.

Bizimle muhatap olacak biri yok mu acaba.
 
