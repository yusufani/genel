With even Toyota forecasting unprecedented losses, managers contemplating 2009 may be tempted to echo Louis MacNeice in "Bagpipe Music":

It's no go my honey love, it's no go my poppet;

Work your hands from day to day, the winds will blow the profit.

The glass is falling hour by hour, the glass will fall for ever,

But if you break the bloody glass you won't hold up the weather.

Storm warnings are indeed flying everywhere. But while there's no point in breaking the glass, you can stop making the weather worse, as most management nostrums unerringly do. Do good business, please your customers and cut costs at the same time. Here's how.

Quit thinking about cost - give people what they want. Customers aren't interested in your costs. They are only interested in being able to get from you a product or service with the minimum of fuss and the maximum of convenience - their convenience. 

Any organisation that can consistently deliver this will win both gratitude and loyalty. But it will also keep costs to a minimum. If that seems counter-intuitive, think of it this way: if you give people what they want, and only what they want, you're not paying to give them what they don't want. Ergo:

� Forget productivity, work on quality. Improved quality (defined in customer terms; this is important) leads to increased productivity for a disarmingly simple reason. Capacity, said Taiichi Ohno, architect of the Toyota Production System, equals work plus waste. Take out waste - anything that does not contribute to value for the customer and - hey presto - capacity, and therefore also productivity, increases in proportion.

� Stop obsessing about scale: think flow. In making a product or service, the critical cost is end-to-end - that is, measured from receipt of order to delivery. What determines end-to-end costs is not the cost of individual activities - taking calls in a call centre or making parts for a product - but the smoothness of flow between them. An organisation that delivers just enough effort to advance an item one step at a time from beginning to end will have lower overall costs and faster throughput than one building huge batches of parts at every stage to gain economies of scale. It's even better if production is synchronised, not only internally but externally, with the market - but that's difficult when demand is subject to wild swings, as at present.

Government and its agencies, egged on by the big management consultancies, are hooked on scale, which just magnifies the other traditional cost-containment mistakes. This is what drives the monumentally misguided rounds of enforced shared services among local authorities and other public-sector bodies. In most cases, these drive up end-to-end costs by fragmenting the flow and making it harder for customers to pull value - in other words, worsening quality.

� Size doesn't matter. Big and remote is the mantra of the past, the driving principle of General Motors and other beached whales of the industrial world. Small and local is the most efficient way to deliver most services, and plenty of products too. Economies of flow and effort far outweigh dinosaur-like economies of scale. Scale, says Tom Johnson, a prominent accounting professor, is dead: "Beyond very small volumes, [it] is a concept that should be discarded."

� Stop trying to performance-manage people; focus on improving the system. Trying to cut costs by tightening individual performance measures is self-defeating. ("There's no way they can raise my productivity faster than I can raise their costs," says an airline pilot meaningfully.) Fear - of punishment, ridicule, losing a job - is the biggest barrier to learning and improvement, which only happens when people control their working lives and are proud of what they do.

� Finally, forget about competition and build co-operation. The parts of a system - which includes customers and suppliers - have to work together. A system can only be optimised as a whole, to a recognised aim and purpose. Of course it competes with others in the marketplace, but the aim must be for everyone to gain - shareholders, employees and society- over the long term. Internally, competition is usually a wrecker.

This is how you get to the truth that by taking care of customers you serve the company's financial goals, while the reverse is not the case. Of course, we've really sensed this all along - we've just forgotten it in the Gadarene rush to get rich quick, the results of which are forlornly strewn around us.

Like all the points above, the truth that profit is a by-product, and as such can't be approached in a straight line, comes straight from the teachings of W. Edwards Deming, who was writing and lecturing 50 years ago. Pace MacNeice, it's time to put back the clock and change the weather. Happy new year.

