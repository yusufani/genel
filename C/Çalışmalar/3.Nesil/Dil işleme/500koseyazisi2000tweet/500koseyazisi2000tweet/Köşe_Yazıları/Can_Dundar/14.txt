Yolsuzluk ayarlı saat

Devlet Bahçeli’nin odasında, geniş pencereler boyunca Atatürk ve bozkurt heykelcikleri var.
Masasının üzeri kitaplar ve raporlarla dolu…
Karşısında büyük bir televizyon ekranı var.
Ancak odadaki asıl dikkat çekici ayrıntı, hemen çalışma masasının yanında duran bir ayaklı saat… Bel hizasında bir direk üzerine yerleştirilmiş düzenek, çevrilen sayfalarla saati ve dakikayı gösteriyor.
Bahçeli, saat tam 17.25’i gösterirken pilini çıkarmış.
Böylece saati bu iki rakamda sabitlemiş:
1 saatlik sohbetimizin sonunda ayrılırken saatin öyküsünü soruyoruz:
“Bunu bizzat ben yaptım” diyor:
“17.25’i gösterdiğinde pilini çıkardım. Her gün bu takvime bakıyorum.”
“Takvim mi, saat mi” diye soruyoruz.
“Onların takvimi, bizim saatimiz” diyor, “Buradan da anlayabilirsiniz ki biz, 17 ve 25 Aralık’ın hesabının sorulması vaadimizden asla geri adım atmayız.”
* * *
Birilerinin unutmadığını görmek güzel… Hele Ankara’nın puslu havasında, diğer parti karargahlarında, iktidar ortaklığı uğruna o dosyaları görmezden gelme eğilimi yaygınlaşırken…
Bahçeli, 7 Haziran gece yarısı Erdoğan muhalifleri bayram yaparken basının önüne çıktı ve AKP-HDP koalisyonu önerdi. “İsterlerse CHP’yi de aralarına alsınlar” dedi.
“Biz ana muhalefet görevini üstlenmeye hazırız” diye ekledi.
Dinleyen herkes şöyle bir sendeledi.
Bunu bir seçim taktiği sananlar oldu. Ancak Bahçeli’yi yakından tanıyanlar, blöf yapmadığına emindi.
Seçimden 20 gün sonra, görüşmeye gittiğimizde Bahçeli’nin aynı pozisyonda, aynı kararlılıkla durduğunu ve aynı kompozisyonu savunduğunu gördük:
“Onlar hepsi… Biz tek!”
* * *
Bahçeli, “barış ve kardeşlik projesi” diye başlayıp “açılım süreci” adıyla devam eden ve nihayetinde –kendi tahminiyle- bir Kürt devletine doğru evrilen “çözüm süreci”nin ülkeyi uçuruma götürdüğüne inanıyor.
Halkın önemli bölümünün bunu görmezden gelerek süreci destekleyen partilere oy vermesini de içten içe yadırgıyor. Ancak bu iradeye karşı durmak yerine, gerçeğin açığa çıkmasını beklemeyi daha uygun görüyor. Eninde sonunda batağa saplanacak sürecin, nihayetinde MHP’ye yarayacağını, MHP’siz koalisyonun fazla uzamadan yıkılacağını, bir erken genel seçimin kendisini daha da yukarı taşıyacağını hesaplıyor muhtemelen…
O yüzden hiç taviz vermeden, seçim gecesindeki pozisyonunu koruyor.
* * *
MHP’nin karşı safta olmasının süreci zora sokacağını görenler var. Onlar sık sık Genel Merkez’in kapısını çalıp “Taşın altına elinizi sokun” mesajları veriyorlar.
Bahçeli en çok onlara kızıyor.
“12 kötü adam”a, “63’lüklere”, “toplumu sürece hazırlamak için rol paylaşan lobiler”e, TÜSİAD’a, MÜSİAD’a…
Bunların MHP’nin net pozisyonunu bulanıklaştırmak, yozlaştırmak istediklerine inanıyor; onlar geldikçe daha da net ve sert konuşuyor.
“MHP ne yaptı da büyüyor” sorusuna da oradan cevap buluyor:
“Muhalefette net ve sert duruyoruz da ondan…”
* * *
Seçim denklemi, tahterevallinin bir kefesine, ne olduğu belirsiz, muğlak seçenekler doldurdu; MHP ise daha net olan öbür kefeye talip oldu ve ilk geceden gidip oraya oturdu.
Tahterevalli mantığıyla koalisyon, beklentileri karşılayamaz da inişe geçerse, çok yakında MHP’nin yükseldiğini göreceğiz demektir.
Bahçeli’nin odasındaki takvim, o günden itibaren piline kavuşup işleyebilir.