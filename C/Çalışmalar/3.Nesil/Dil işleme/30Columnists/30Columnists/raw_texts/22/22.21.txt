As Martin Johnson openly concedes, there is no hiding place for England tomorrow. Defeat to an improving French side would not, in itself, be a disgrace but woe betide the hosts if another couple of players are sin-binned and their defence fails to scale the heights of Cardiff and Dublin. Twickenham supporters are mostly a loyal bunch but the fraying fig-leaf currently preserving English modesty is in some need of reinforcement.

Johnson even felt it necessary yesterday to urge his players to supply the home crowd with something to cheer early on, tacit acknowledgement that England also have hearts and minds to conquer in the penultimate game of a frustrating Six Nations campaign.

"One of the things we used to say was that it's not up to the crowd to get us into the game, it's the other way around," said the 2003 World Cup-winning captain, recalling the halcyon days when England barely ever lost at home. "If you get beaten people are going to be unhappy, that's part of the pressure of what we do."

The outcome could well revolve around the French pack, which is almost as big and ugly as the Pompidou Centre and eminently capable of supplying a high-speed platform for the freight-train ball-carriers in the back row and midfield. Thierry Dusautoir and Sébastien Chabal need no introduction and Imanol Harinordoquy always relishes the sight of a white jersey.

"During the World Cup they were the most conservative French team I've ever played against but last year they were like Jekyll and Hyde, completely the opposite," observed Joe Worsley, braced for another stint as a human crash-test dummy. "Some of the stuff they did last season was suicidal but they seem to have struck a better balance now."

If Les Bleus do come out to play, it will be fascinating to see how the hosts respond. So far they have employed a mostly reactive game, concentrating primarily on stopping their opponents, with reasonable success. Johnson, though, seems to be accepting that one-dimensional sides are too easy to read at Test level and is optimistic of creating more chances on home soil.

"It's been the best training week we've had and the players' understanding of what we're trying to do with the ball is getting better," he remarked.

It will need to be. England have a long way to go before they can claim to have buried all their disciplinary ghosts and the Sale prop Lionel Faure is still smarting from the front-row penalties which cost his country dear in last year's corresponding match. "It isn't necessarily revenge," murmured Faure. "But we would like to show them that the penalties against us in the scrum weren't entirely justified."

The upshot is certain to be a shudderingly physical collision. Victory in what is now known as "Le Crunch du Credit" will set up France for a potential title while Johnson's squad are determined to avoid a bottom-half finish. If it boils down to pure desperation, England could just sneak it.
