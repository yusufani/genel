#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
int ogrenciSayisi;
int average=0;
int i=0;

struct ogrenci{
	int id;
	char isim[25];
	int not;
}ogrenciler[3];

void calculateAverage(struct ogrenci gelenOgrenci){
	
	if(i==0)
	{
		average = gelenOgrenci.not;
	}
	else
	{
		average = (average + gelenOgrenci.not) / 2;
	}
	
	if(i==(ogrenciSayisi-1)) printf("Listedeki Ogrencilerin Not Ortalamasi : %d", average);
	
}

void createStudent(int kacinciOgrenci){
	
	system("CLS");
	
	printf("%d. ogrencinin ID'sini giriniz : ",kacinciOgrenci+1);
	scanf("%d",&ogrenciler[kacinciOgrenci].id);
	
	printf("%d. ogrencinin ismini giriniz : ",kacinciOgrenci+1);
	scanf("%s",&ogrenciler[kacinciOgrenci].isim);
	
	printf("%d. ogrencinin notunu giriniz : ",kacinciOgrenci+1);
	scanf("%d",&ogrenciler[kacinciOgrenci].not);
	
	calculateAverage(ogrenciler[kacinciOgrenci]);
	
	printf("\n-------\n%d. Ogrenci Eklendi ! \n\n",kacinciOgrenci+1);
	
	printf("\nBir tusa basiniz!\n\n");
	
	getch();
	
}

int main() {
	
	printf("Kac ogrenci gireceksiniz : ");
	scanf("%d",&ogrenciSayisi);
	
	while(i<ogrenciSayisi)
	{
		int secenek;
		
		printf("1. Ogrenci Ekle");
		printf("\n2. Cikis");
		printf("\n\nSecim : ");
		
		scanf("%d",&secenek);
		
		switch(secenek){
			case 1:
				createStudent(i);
				i++;
				break;
			case 2:
				return 0;
				break;
			default:
				printf("Hatali Secenek!");
				getch();
				return 0;
				break;		
		}	
	}
	
	return 0;
}
