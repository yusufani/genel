myss segment para stackt 'stack'
    dw 20 dup(?)
ends
myds segment para 'data'
dizi db -2,9,1,-1,3                       
n dw 5
ends
mycs segment para 'kod'
    assume cs:mycs,ss:myss,ds:myds
ana proc far
    push ds
    xor ax,ax
    push ax
    mov ax,myds
    mov ds,ax
    mov cx,n
    xor si,si
    mov di,cx
    dec di
    call sirala
    retf
ana endp

sirala proc near
     push di  ; Ilk degerler ilk ve ikinci ifde lazim olucak o y�zden stackte tuttuk
     push si  ; ""
     mov dx,si; dx i pivot degerini hesaplamak i�in kullandim  ayarladim
     add dx,di; pivot =si+di / 2
     shr dx,1;  ""
     mov dl,dizi[bx]; dl artik pivotumuz
  l1:
     cmp si,di
     jg  ilkif
l2:  cmp dizi[si],dl
     jnl l3
     inc si
     jmp l2
l3:  cmp dizi[di],dl
     jng icif
     dec di
     jmp l3
icif:cmp si,di
     jg l1
     mov dh,dizi[si]
     XCHG DH,dizi[di]
     mov dizi[si],dh
     inc si
     dec di
     jmp l1
ilkif:pop bx
      cmp bx,di
      jnl ikinciif
      push si
      push di
      mov si,bx
      call sirala
      pop si
      pop di
ikinciif:pop bx
        cmp si,bx
        jnl son
        push si
        push di
        mov di,bx
        call sirala
        pop si
        pop di
son:    ret 
sirala  endp
mycs ends
end ana