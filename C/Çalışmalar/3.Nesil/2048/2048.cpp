#include <stdio.h>
#include <conio.h>
#include <windows.h>
#define MAX 5
const char *const kirmizi = "\033[0;40;31m";//40 arkaplan rengi yazilan yerdeki 30 lar ise renkler
const char *const yesil= "\033[0;4;32m";
const char *const turuncu = "\033[0;40;33m";
const char *const mavi = "\033[0;40;34m";
const char *const mor = "\033[0;40;35m";
const char *const magenta = "\033[0;40;36m";
const char *const beyaz = "\033[0;40;37m";
const char *const normal = "\033[0m";
void print_menu ();
int random_variable( int );
int direction(int map[][MAX],int );
int map_draw(int map[][MAX],int );
void placement (int map[][MAX],int side_lenght,int game_over);
void take_back_direction(int map[][MAX],int side_lenght, int tmp_direction );
int two_or_four();
void take_left(int map[][MAX],int side_lenght);
int summing (int map[][MAX],int side_lenght);
void game_over_screen(int score);
int main(){
	int selection=1,menu=1;
	print_menu();
	selection=getch();
	while (menu==1){
		system("CLS");
		switch(selection){
			case 49:{
				//This case starting game
				int side_lenght,game_over=1,i,j,tmp_direction,score;
				printf("%sLutfen %sKare sayisini%s giriniz:",turuncu,kirmizi,turuncu);
				scanf("%s%d%s",kirmizi,side_lenght,normal);
				int map[side_lenght][side_lenght]={0};// Clearing Map
				map[random_variable(side_lenght)][random_variable(side_lenght)]=2;//random variables for starting game
				map[random_variable(side_lenght)][random_variable(side_lenght)]=2;
				while(game_over=!0){
					score=map_draw(map,side_lenght);
					tmp_direction=direction(map,side_lenght);//tmp_direction for turn back the rotation
					take_left(map,side_lenght);
					game_over=summing(map,side_lenght);
					take_left(map,side_lenght);
					take_back_direction(map,side_lenght,tmp_direction);
					placement(map,side_lenght,game_over);
				}
				game_over_screen(score);
				break;
			}
		}
	}
}
void print_menu (){
	//this function is printer for menu
	printf("%s2048 Oyununa Hosgeldiniz!\n%s<1>Oyuna Basla\n%s<2>Nasil Oynanir?%s<3>Rekor Siralamasi%s<4>Cikis",mavi,turuncu,beyaz,turuncu,beyaz,normal);
}
int random_variable( int side_lenght){
	//This function create random variable
	return rand()%side_lenght;
}
int map_draw(int map[][MAX],int side_lenght){
	//This function draw a map for user and score
	system("CLS");
	int i,j,score=0;
	for(i=0;i<side_lenght;i++){
		for(j=0;j<side_lenght;j++){
			score+=map[i][j];
		}
	}
	printf("Skor=%d",score);
	for(i=0;i<side_lenght;i++){
		for(j=0;j<side_lenght;j++){
			if(j % 2 ==1 ) printf("%s%d%s",turuncu,map[i][j],normal);
			else printf("%s%d%s",beyaz,map[i][j],normal);
		}
		printf("\n");
	}
}
int direction(int map[][MAX],int side_lenght ){
	//This function take direction from user and all directions change into left
	int direction_value,i,j,tmp[side_lenght][side_lenght];
	direction_value=getch();
		if (direction_value!=97){//Copy of 
			
			for(i=0;i<side_lenght;i++){
				for(j=0;j<side_lenght;j++){
					tmp[i][j]=map[i][j];
				}
			}
		}
		else if (direction_value==115){
			for(i=0;i<side_lenght;i++){
				for(j=0;j<side_lenght;j++){
					map[j][side_lenght-1-i]=tmp[i][j];
				}
			}
		}
		else if (direction_value==100){
			for(i=0;i<side_lenght;i++){
				for(j=0;j<side_lenght;j++){
					map[side_lenght-1-i][side_lenght-1-j]=tmp[i][j];
				}
			}
		}
		else if(direction_value==119){
			for(i=0;i<side_lenght;i++){
				for(j=0;j<side_lenght;j++){
					map[side_lenght-1-j][i]=tmp[i][j];
				}
			}
		}
	return direction_value;
}
void take_back_direction(int map[][MAX],int side_lenght, int tmp_direction ){
	int i,j,tmp[side_lenght][side_lenght];
	if (tmp_direction!=97){
		for(i=0;i<side_lenght;i++){
			for(j=0;j<side_lenght;j++){
				tmp[i][j]=map[i][j];
			}
		}
	}
	else if(tmp_direction==115){
		for(i=0;i<side_lenght;i++){
				for(j=0;j<side_lenght;j++){
					map[side_lenght-1-j][i]=tmp[i][j];
				}
			}
	}
	else if (tmp_direction==100){
		for(i=0;i<side_lenght;i++){
				for(j=0;j<side_lenght;j++){
					map[side_lenght-1-i][side_lenght-1-j]=tmp[i][j];
				}
			}	
	}
	else if (tmp_direction==119){
		for(i=0;i<side_lenght;i++){
				for(j=0;j<side_lenght;j++){
					map[j][side_lenght-1-i]=tmp[i][j];
				}
			}
		
	}
}
void take_left(int map[][MAX],int side_lenght){
	//This function takes all values into the left side
	int i=0,j=0,t;
	while(i<side_lenght){
		while(j<side_lenght){
			if(map[i][j]==0){
				t=0;
				while((map[i][t]!=0) && (t<side_lenght)){
					t++;
				}
				map[i][j]=map[i][t];
				map[i][t]=0;
			}
		}
	}
}
int summing (int map[][MAX],int side_lenght){
	//This function sum same variable and also check end of the game
	int i=0,j=0,summer=0;
	for(i=0;i<side_lenght;i++){
		for(j=0;j<side_lenght-1;j++){
			if(map[i][j]==map[i][j+1]){
				map[i][j]*=2;
				map[i][j+1]=0;
				summer++;
			}
		}
	}
	return summer;
}
int two_or_four(){
	//This function create a 2 or 4 with %12.5 probability for 4
	if(rand()%100>12.5){
		return 4;
	}
	else return 2;
}
void placement (int map[][MAX],int side_lenght,int game_over){
	if (game_over!=0){
		while(1){
			if(map[random_variable(side_lenght)][random_variable(side_lenght)]==0){
				map[random_variable(side_lenght)][random_variable(side_lenght)]=two_or_four();
				break;
			}
		}
	}
}
void game_over_screen(int score){
	system("CLS");
	printf("Kaybettiniz:( Skorunuz:%d",score);
	printf("Skorunuzu kaydetmek ister misiniz?");
}

	
