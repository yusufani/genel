Gordon Brown and David Cameron have started the year with a burst of largely irrelevant initiatives and hectic trips around the country. But hyperactivity is not the same as action. Mr Brown's pledge to create 100,000 jobs and Mr Cameron's new savings package do not address the cause of the deepening recession: the lack of credit. Everything else is secondary. 

The rescue package of mid-October stabilised the position, preventing a further collapse. But that was just a first stage: the terms of the partial recapitalisation of the banks have unavoidably created perverse incentives. 

Their desire to sort out their balance sheets and the imbalance between loans and customer deposits discourages lending. In this respect, populist complaints against banks miss the point. As Richard Lambert, Director-General of the CBI, argued on the business pages on Monday, the shortage of credit is now creating increasing problems across the private sector. 

That links the current talks between the motor industry and the business department; the low level of new mortgage lending and the problems of the construction industry; and the drying up of trade credit insurance cover, hitting small and medium-sized companies in particular. 

The answer is not unconditional bailouts, as in the 1970s, but some way of breaking the credit logjam. In the short term this is likely to involve large government guarantees for loans. 

Of course, both the Government and the Opposition are well aware of the urgency of this problem. Mr Cameron and George Osborne first urged an extended state guarantee for loans well before Christmas, while Alistair Darling, the Treasury and the Bank of England are discussing further, early initiatives to stimulate lending. 

The aim is to retain an arm's-length detachment from specific lending decisions. But the larger that government guarantees become - and they could be enormous - the greater the demand for specific commitments in return. 

By contrast, the job and savings initiatives are distractions. They address symptoms not causes. Bringing forward capital projects makes sense, but is unlikely to have more than a marginal impact. There is a case for boosting saving in the long term, but not in the short term when the need is to raise spending. Moreover, the impact of tax cuts will be minor compared with the much lower returns for savers following the big drop in interest rates and likely lower dividends. 

Both the Brown and Cameron initiatives are about political positioning: to demonstrate their concern about the recession and to show they are doing something. The most damaging charge now is of inaction. 

The plans are also aimed at potential supporters, with Mr Cameron generating a positive response from Tory websites and activists seeking tax cuts, even though he has been criticised by independent commentators. 

So, please, fewer peripheral initiatives. The real story of the coming weeks will be the next stage of the financial rescue
