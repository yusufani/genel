Muhalefet ne yapacak?

Bugünden itibaren 1.5 yıl içinde Türkiye'nin gelecek 10-15 yılını şekillendirecek bir siyasal takvim başlıyor.        
Yerel seçimlere şunun şurasında bir yıldan az kaldı. 
Hemen akabinde cumhurbaşkanlığı için sandık başına gideceğiz. Arada, çok büyük ihtimalle önümüzdeki sonbaharda referandum yapılacak. Evet evet, 
Eylül-Ekim 2013'te yeni anayasa için halkoyuna gidileceği görülüyor. 
İktidarın "oyun planı" belli. Kazanmak için ortaya koyduğu/koyacağı argümanlar da...
Gücü de...
Üstelik Ortadoğu konjonktürünün rüzgârını kullanıyor. Önce Suriye, ardından İran denklemleri çözülene dek küresel sistem, iktidar partisinin yelkenlerini şişirmeye devam edecek. 
Peki muhalefet ne yapacak?
CHP ve MHP bu zorlu ralliye hazır mı?
Milliyetçi Hareket, "11 yıldır derin bir açmazdaydı."
Çok güçlü bir sağ parti iktidarına karşı "etkili bir muhalefet dili" geliştiremedi. Açıkçası, sağ iktidara karşı sağ eleştirel siyaset üretemedi MHP. 
Bu kez "sol/sağ mücadelesi" değil ki, karşı karşıya kaldıkları.
Hele 12 Eylül referandumunda "MHP adeta kimlik bunalımına" düştü. 
Karşısında, İslami duyarlılıkları çok yüksek, muhafazakâr tek parti iktidarı vardı ve aynı zamanda milliyetçiliği de kuşatma iddiasında devasa bir siyasal hareket bulunuyordu.  
Belki büyüyemedi ama MHP var olmayı bildi. Dallanıp budaklanamadı ama hiç olmazsa ayakta kalabildi. 
Son açılımla birlikte şartlar değişiyor. 
İktidar kanadındaki hâkim söylemin aksine ben "süreç"in MHP'yi güçlendireceğini düşünüyorum. Sadece bugün değil, 2008'deki açılımın ilk işaretlerinden beri aynı inançtayım. Toplumdan damıttığım gözlemlerim bu yönde. Nitekim, 2009 yerel seçimlerindeki Adalet ve Kalkınma Partisi'nin yüzde 39'a düşmesinin arka planında Kürt açılımına tepkisellik yatıyordu. Balıkesir, Antalya, Manisa gibi illerdeki belediyelerin kaybını buna bağlamıştım. 

CHP VE MHP NEYE KARŞI?
PKK terörünü bitirme amacıyla başlatılan son barış sürecine tepkili duran MHP tabanı, "bölünme endişesi" taşıyor. 
Sürece mesafeli yaklaşan CHP tabanı ise "Başbakan Erdoğan'a başkanlık yolunun açılacağı kaygısıyla" hareket ediyor. 
AK Parti, uluslararası dengelerdeki değişimin ve bunun Ortadoğu'ya yansımasının zorunlu karşılığı olarak bu zorlu ve riskli göreve soyundu. Aslında mecbur kaldılar diyebilirim. Önce referanduma, ardından yerel ve Çankaya seçimlerine terörsüz girilebilirse çok büyük avantaj yakalayacak. Erdoğan'ın kesin amacı her bir sandıktan yüzde 50 barajını aşarak çıkmak. Yani onların siyasal takvimi 2014 Ağustos'unu milat olarak gösteriyor. Erdoğan, "bu üçlü sınavı" başarırsa, 2023 hedeflerine de rahatlıkla ulaşır. Favori görünse de siyasetin neler getirebileceğini, nasıl sürprizlere gebe olduğunu bildiğinden işi sıkı tutuyor. Hele ki, kadim Kürt sorununu çözme konusunda böylesine risklerle yüklü bir işe giriştiyse...
Bahçeli, geçtiğimiz hafta partisinin CHP'nin önüne geçtiğini söylüyordu. Son anketlere bakarak, "bundan sonraki amacımız AKP'yi geçmektir" iddiasını dillendirerek... Eğer, "sağ-dindar-muhafazakâr" kesime dönük, şehirli hayatı da kapsayabilen bir kadro-söylem-eylem formülünü tutturabilirse Milliyetçi Hareket için tarihi bir çıkış yakalayabilir. Unutmayalım ki, önümüzdeki 15-20 yıl boyunca Ortadoğu'da Kürt sorunu siyasetin de diplomasinin de 1 numaralı belirleyeni olacak. Açılım başarıya ulaşsa da ulaşmasa da...