With the announcement of a second Premier League investigation, some think the Carlos Tevez affair is getting stupid. Not quite.
The day Lord Griffiths, 85, decided he could predict and measure in points the precise impact an individual player had on the football season, it got stupid.

When his panel processed the views expressed in a match report written by the chief football writer of The Daily Telegraph as if it were fact, not opinion, stupidity was pretty high on the agenda, too.
Indeed, at the moment when an independent committee first hit West Ham United with the biggest fine in the history of the Premier League, then announced they would have done something far worse had only they got their backsides in gear earlier, the Tevez inquiry pretty much decked common sense with a flying head-butt.
So to have a further investigation based on the meaning of an oral cuddle between Scott Duxbury, the West Ham chief executive, and Kia Joorabchian, owner of Tevez, cannot be classed simply as stupid.
It is post-stupid, in the way some TV shows are described as post-modern when concerned with ironic self-reference and absurdity.
Indeed, one such post-modern cartoon, the wonderful Ren and Stimpy, made by Canadian animator John Kricfalusi, actually contained a segment entitled Ask Dr Stupid, with foolish logic that pre-empted the Tevez inquiry by a good 15 years. 

Yet, somehow even a psychotic chihuahua and his idiot fat Manx cat accomplice never got quite as stupid as this lot.
