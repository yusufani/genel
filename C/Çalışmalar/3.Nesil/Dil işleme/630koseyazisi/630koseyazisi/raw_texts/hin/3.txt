Asl�nda bu ad� duymak bile t�ylerimi �rpertir. �nsanlardan nefret etmek gibi bir adetim yok. �stesem de edemem pek.. Ama birka� isim var listemde.. Adnan Hoca bunlardan biri.. Etraf�na toplad��� "M�rid" adl� gen�lere ise ac�r�m.. �fke dolu bir ac�ma..
O zaman nas�l oluyor da Adnan Hoca'ya te�ekk�r eden bir yaz�y� kaleme al�yor ve sayfam�n tepesine koyuyorum..
S�yleyeyim..
Adnan Hoca, T�rk Hukuk sisteminin nas�l la�ka oldu�unu kan�tlayan adamd�r.. Y�llard�r bunu anlatmaya �al���yor, son haftalarda yo�un bir �ekilde bunu ba�ta Adalet Bakan�m�z, sorumluluk duyusu olan, olmas� gereken herkesin kafas�na sokmaya u�ra��yordum.. Hacet kalmad�. Adnan Hoca, T�rkiye Cumhuriyeti'nde hukukun ne kadar bo�luklarla dolu, i�ini bilen usta ve kurnaz ellerde nas�l bir oyuncak, gere�inde nas�l bir silah gibi kullan�laca��n� �a�maz bir �ekilde ispat etti. Benim bin yaz� ile yapamayaca��m� yapt�. Te�ekk�r�m ondan..
�imdi hukuka bak�n..
Be� y�l �nce zaman�n ��i�leri Bakan� Sadettin Tantan'�n bizzat y�netti�i bir operasyonla Adnan Hoca ve tak�m�na yap�lan bask�nlar�, bu bask�nlar sonunda ortaya ��kanlar�, televizyonlara yans�yanlar� hat�rlay�n..
Ne oldu?.
Adnan Hoca mahkemeye verildi.. Ama tek, bir tek duru�mas� yap�lmadan kurtuldu. Adnan Hoca'n�n T�rk Hukuk sisteminin bo�luklar�n� �ok iyi bilen avukatlar�, davay� mahkemeden mahkemeye ta��d�lar. Her mahkemede yarg��lar� reddettiler.. Bunlar�n hepsinin kazand�rd��� zamanla be� y�l ge�ti. Adnan Hoca'n�n t�m yapt�klar� zaman a��m�na girdi ve dava bir dakika g�r���lmeden sona erdi.
B�t�n dosyalar mahkemenin elinde iken zaman a��m�?.. Olacak �ey mi?.
T�rkiye'de oldu.
Ama Adnan Hocay� dava edenler, b�yle rahat kurtulamad�lar.. Bask�n sonras� Hoca aleyhine dava a�an y���nla insandan pek �o�u zaman i�inde nedense (!) davalar�n� geri ald�lar.. Aslan y�rekli bir gen� k�z, Ebru �im�ek ve Fatih Altayl� d���nda.. Niye geri �ektikler �tekiler, hesab�n� vicdanlar�na versinler.
Ne var ki davas�n� geri almayanlar�n ba��na gelmedik kalmad�. Ebru �im�ek aleyhine a��lan davalar�n say�s� d�nya rekorlar�na ula�t�. Altayl� mahkemeden mahkemeye ko�turuldu. Operasyona karar veren ve y�neten Sadettin Tantan, hala Adnan Hoca ve m�ridleri taraf�ndan a��lan y���nla davan�n san���, s�r�n�yor. Operasyonu y�r�ten zaman�n �stanbul Emniyet M�d�r� Hasan �zdemir'i, AKP iktidar� k�za�a �ekti. Onun da y���nla davas� var, hocac�lar taraf�ndan a��lm��..
��te T�rk Hukuk Sistemi bu.. Adnan Hoca ve m�ridleri serbest.. Onlar� izleyenler, adalete teslim edenler, s�r�n�yor. Dava a�anlar, T�rk hukuk sistemi kullan�larak a�t�klar�na pi�man ediliyor, ba�lar�na gelmedik kalm�yor.
T�rk Ceza, T�rk Ceza Muhakemeleri Usul�, T�rk �nfaz yasalar� Adnan Hoca ve avukatlar�n�n elinde oyuncak edilmi�, gere�inde de silah gibi kullan�lm��. Olay�n �zerine giden gazeteler ve gazeteciler, a��lan y���nla dava, g�nderilen y���nla tekziple bezdirilmi�, b�kt�r�lm��, cayd�r�lm��.
T�rk Hukuku Adnan Hoca ve adamlar�n� aklamak, onlar�n �zerine gidenleri yok etmek i�in hem de nas�l kullan�lm��..
Say�n Adalet Bakan� Cemil �i�ek!.. Ben yazd�m siz hep sustunuz.. Tek sat�r, tek kelime yan�t vermediniz. Veremediniz..
Peki bu tablo da m� y�z�n�z� bir zerre k�zartm�yor?.
Bu tablo �n�nde de mi susma hakk�n�z� kullanacaks�n�z.
Siz nas�l bir Adalet Bakan�s�n�z?.
Su�lular� koruyan, masum ve mazlumlar� ezen bu hukuk sistemine "Adalet" demeye daha ne kadar devam edeceksiniz?. 
