myss segment para stack 'stack'                                                        ; Stacksg tanimliyoruz
    dw 32 dup(?)                                                                       ; Stacksg boyutunu giriyoruz.
myss ends                                                                              ; Stacksg tanimlamasi bitti
myds segment para 'data'                                                               ; Datasg Tanimliyoruz
cr equ 13                                                                              ; Cr Kursor satir basina demek , cr boyutunu girdik.
LF EQU 10                                                                              ;LF bir asagi demek , LF degerini giriyoruz.
DIZI DB 100 DUP(?)                                                                     ;Dizinin tipini ve boyutunu belirliyoruz.
MSG1 DB 'Lutfen dizinin boyutunu giriniz:',0                                           ;Dizi boyutunu almak i�in gerekli mesaji tanimliyoruz.
MSG3 DB CR,LF, 'Lutfen Elemanlari giriniz:',0                                          ;Elemanlari girmemiz i�in gereken mesaji tanimliyoruz.
HATA DB CR,LF, 'Dikkat sayi girmediniz yeniden giriniz. ',0                            ;Sayi girmeme durumu i�in gerekli mesaji tanimliyoruz.
HATA2 DB CR,LF, 'Gireceginiz sayi -128 ile 127 arasinda olmalidir.Tekrar sayi giriniz: ',0;Verilen araliga uymamasi halinde gerekli mesaj tanimliyoruz
ayirici DB ' ; ',0                                                                     ;En sonda siranmis sayilar arasina ";" isareti koydum.
MYDS ENDS                                                                              ;Datasg tanimlamasi bitti
MYCS SEGMENT PARA 'KOD'                                                                ;Code sg tanimliyoruz.
    ASSUME CS:MYCS,DS:MYDS,SS:MYSS                                                     ;Gerekli assume atamalarini yapiyoruz.
ANA PROC FAR                                                                           ;Ana adinda main fonksiyonum.
    PUSH DS                                                                            ;DS Stack'e g�nderildi.
    XOR AX,AX                                                                          ;AX sifirlandi.
    PUSH AX                                                                            ;AX Stack'e g�nderildi.
    MOV AX,MYDS                                                                        ;Datasg mi AX e atadim.
    MOV DS,AX                                                                          ;AX deki degeri DS ye atadim.
    xor ch,ch                                                                      ;CHin sifir olmasi lazim ki CX �evirimi dogru �alissin.(cl asagida)
    MOV AX,OFFSET MSG1                                                                ;Dizinin boyutunu aliyoruz.
    CALL PUT_STR                                                                      ;        ""(islem devam ediyor)
    CALL GETN  ; boyutu aliyorum                                                      ;        ""     
    MOV Cl,Al                                                                         ;        ""
    push cx     ;CX �evrim sayisi oldugu i�in elemanlari yazdirirken ihtiyac duyacagiz(Bir degisene de atabilirdik fakat �dev disina �ikmak istemedim)
    mov di,cx                                                                         ;DI algroitmamdaki j degiskenine denk geliyor
    dec di                                                                            ;Dizinin eleman sayini n ise son eleman n-1. elemandir.
    XOR SI,SI                                                                         ;SI yi dizinin elemanlarini alirken kullancagim.
    MOV AX,OFFSET MSG3                                                                ;Dizinin elemanlarini aliyorum.
    CALL PUT_STR                                                                      ;        ""
eleman_al:  CALL GETN                                                                 ;        ""
    MOV DIZI[SI],AL                                                                   ;        ""
    INC SI                                                                            ;        ""
    LOOP eleman_al                                                                    ;        ""
    xor si,si                                                                ;SI algoritmamdaki i degiskenine denk geliyor.Bu y�zden 0 ladim.
    call sirala                                                              ;Quick Sort iceren fonksiyonumu �agiriyorum.
    xor si,si                                                ;Yazdirirken dizinin elemanlarina erisirken SI'yi kullanacagim.Bu y�zden 0'ladim.
    pop cx                                                                       ;�evrim sayisini 27. satirda Stack'e atmistim.Geri �ekiyorum.
yazdir:  mov al,dizi[si]                                                              ;Dizinin Elemanlarini yazdiriyorum.
    CALL PUTN                                                                         ;             ""
    mov aX,offset ayirici            ; 12. satirdaki araya ';' isaretini koyma islemi.;             ""
    call put_str                                                                      ;             ""
    inc si                                                                            ;             ""
    loop yazdir                                                                       ;             ""
    RETF                                                                              ;Ana fonksiyonum bitiyor.
ANA ENDP                                                                              ;        ""
sirala proc near                                                                      ;Quick Sort yapan Sirala fonksiyonum basliyor.
     push di                            ;Algoritmadaki sag ve sol degerlerini ilkif ve ikinciif labellarimda kullacagim i�in stackte sakladik.
     push si;Not:Isteseydik ayri degiskenlerde de tutabilirdik fakat �devde bunun hakkinda bir sey bulamadigim icin �devi min degiskenle yaptim.
     mov dx,si                                                                        ;Dl algoritmadaki Pivot degisken// Byte tipinde olmali.
     add dx,di;                                                                       ;pivot=dizi[ (SI+DI)/2] Oldugundan DI ile topladik.
     shr dx,1;                                                                        ;2'ye b�ld�k.
     mov dl,dizi[bx];                                                                 ;dl artik pivot degerimiz
l1:  cmp si,di                                                                        ;L1 I�indeki i<=j ifadesini ger�eklestiriyoruz.
     jg  ilkif                                                                        ;i b�y�kse ilkif labelina gidebilir.
l2:  cmp dizi[si],dl                                                                  ;L2 i�inde dizi[i]<pivot ifadesini ger�eklestiriyoruz.
     jnl l3                                                                           ;dizi[i] b�y�k veya esitse L3 labeline gidebilir.
     inc si                                                                           ;Byte tipinde i++ ifadesini ger�eklestiriyoruz.
     jmp l2                                                                           ;Bir kez daha d�nmesi i�in L2 labeline geri d�n�yoruz.
l3:  cmp dizi[di],dl                                                                  ;dizi[j]>pivot ifadesini ger�eklestiriyoruz.
     jng icif                                                                         ;dizi[i] b�y�k degilse icife gidebilir.
     dec di                                                                           ;Byte tipinde j-- ifadesini ger�eklestiriyoruz.
     jmp l3                                                                           ;Bir kez daha d�nmesi i�in L3 labeline geri d�n�yoruz.
icif:cmp si,di                                                                        ;i<=j ifadesini ger�eklestiriyoruz.
     jg l1                                                                            ;i b�y�kse l1 labeline d�nebilir
     mov dh,dizi[si]                                     ;Iki memory ifadesi XCHG komutunda kullanilmadigi i�in birini bos registera atiyoruz.
     XCHG DH,dizi[di]                                                                 ;arr[i] ile arr[j] yer degistiriyor.
     mov dizi[si],dh                                                                  ;Registera atadigimiz degeri geri atiyoruz.
     inc si                                                                           ;i++ ifadesini ger�eklestiriyoruz.
     dec di                                                                           ;j-- ifadesini ger�eklestiriyoruz.
     jmp l1                                                                           ;Bir kez daha d�ng� basina gitmesini sagliyoruz.
ilkif:pop bx;50. satirda a�ikladigim sag ve sol degerleri icin stackten sol degerini ki bu o zaman ki di degerini LIFO Kurakiyla bx e atiyoruz.
      cmp bx,di                                                                       ;sol<j ifadesini ger�eklestiriyoruz.
      jnl ikinciif                                                                    ;sol,j'den b�y�k esitse ikinciif labelina atliyoruz. 
      push si         ;Recursive olarak bir sonraki fonksiyonu �agirmadan �nce dondugumuzdeki si,di degerlerini korumak i�in stack e atiyoruz.
      push di         ;                                                       ""      
      mov si,bx                                                                       ;Sol degiskeninindeki degeri SI  ya atiyoruz.
      call sirala                                                                     ;Recursive olarak sirala fonksiyonunu �agiriyoruz.
      pop di                                                                          ;Stackteki degerlerimi geri aliyoruz.
      pop si                                                                          ;               "" 
ikinciif:pop bx                                                                    ;50. satirdaki sag ve sol degerlerinden sag degerini bx e aliyoruz.
        cmp si,bx                                                                     ;i<sag ifadesini ger�eklestiriyoruz.
        jnl son                                                                       ;i,sagdan k���k degilse son labeline atliyoruz.
        push si                                                                  ;Bir �nceki ifdeki yaptigimiz islemleri aynisini bu if e uyguluyoruz.
        push di                                                                       ;                    ""
        mov di,bx                                                                     ;tek farki bu sefer sag degerini BX e atiyoruz.
        call sirala                                                                   ;Fonksiyonu �agiriyoruz.
        pop di                                                                   ;Bir �nceki ifdeki yaptigimiz islemleri aynisini bu if e uyguluyoruz.
        pop si                                                                        ;                    ""
son:    ret                                                                           ;Sirala fonksiyonunu bitiriyoruz.
sirala  endp                                                                          ;          ""

GETC 	PROC NEAR ;KITAPTAKI FONKSIYON ILE AYNIDIR.
		MOV AH,1h
		INT 21H
		RET
GETC 	ENDP

PUTC	PROC NEAR ;KITAPTAKI FONKSIYON ILE AYNIDIR.
		PUSH AX
		PUSH DX
		MOV DL,AL
		MOV AH,2
		INT 21H
		POP DX
		POP AX
		RET
PUTC	ENDP

GETN	PROC NEAR; KITAPTAKI FONKSIYON ILE TEK FARKI A�IKLAMA YAPILAN SATIRLARDADIR.
		PUSH BX
		PUSH CX
		PUSH DX
GETN_START:
		MOV DX,1
		XOR BX,BX
		XOR CX,CX
NEW:
		CALL GETC
		CMP AL,CR
		JE FIN_READ
		CMP AL, '-'
		JNE CTRL_NUM
NEGATIVE:
		MOV DX,-1
		JMP NEW
CTRL_NUM:
		CMP AL,'0'
		JB error
		CMP AL,'9'
		JA error
		SUB AL, '0'
		MOV BL,AL
		MOV AX,10
		PUSH DX
		MUL CX
		POP DX
		MOV CX,AX
		ADD CX,BX
		cmp cx,127                                                                 ;Sayimiz 127'den b�y�kse error 2 ye atlar.      
		jg error2                                                                  ;                 ""
		cmp cx,-128                                                                ;Sayimiz -128'den k�c�kse error 2 ye atlar. 
		jl error2                                                                  ;                 ""
		JMP NEW
error:
		MOV AX, OFFSET HATA
		CALL PUT_STR
		JMP GETN_START
error2:                                                                            ;HATA2 ADLI MESAJI EKRANA YAZIP YENI DEGERI BEKLER.
		MOV AX, OFFSET HATA2                                                       ;                   ""
		CALL PUT_STR                                                               ;                   ""
		JMP GETN_START                                                             ;                   ""
FIN_READ:
		MOV AX,CX
		CMP DX,1
		JE FIN_GETN
		NEG AX
FIN_GETN:
		POP BX
		POP CX
		POP DX
		RET
GETN    ENDP



PUTN	PROC NEAR                        ; KITAPTAKI FONKSIYON ILE TEK FARKI A�IKLAMA YAPILAN SATIRLARDADIR.
		PUSH CX
		PUSH DX
		XOR DX,DX
		PUSH DX
		MOV CL,10
		CMP AL,0                                                                   ;Yazdirdigimiz sayilar Byte oldugu i�in AL ye uyarlandi.
		JGE CALC_DIGITS
		NEG AL                                                                     ;                       ""
		PUSH AX
		MOV AL, '-'                                                     
		CALL PUTC
		POP AX
CALC_DIGITS:
		DIV CL                                                                     ;Yazdirdigimiz sayilar Byte oldugu i�in CL ye uyarlandi.
		ADD AH, '0'
		MOV DL,AH                                                                  ;Yazdirdigimiz sayilar Byte oldugu i�in DL ye uyarlandi.
		PUSH DX
		XOR AH,AH                                                                  ;Yazdirdigimiz sayilar Byte oldugu i�in AH ye uyarlandi.
		CMP AL,0                                                                   ;Yazdirdigimiz sayilar Byte oldugu i�in AL ye uyarlandi.
		JNE CALC_DIGITS
DISP_LOOP:
		POP AX
		CMP AL,0
		JE END_DISP_LOOP
		CALL PUTC
		JMP DISP_LOOP
END_DISP_LOOP:
		POP DX
		POP CX
		RET
PUTN	ENDP


PUT_STR	PROC NEAR     
		PUSH BX
		MOV BX,AX
		MOV AL,BYTE PTR [BX]
PUT_LOOP:
		CMP AL,0
		JE PUT_FIN
		CALL PUTC
		INC BX
		MOV AL,BYTE PTR [BX]
		JMP PUT_LOOP
PUT_FIN:
		POP BX                                                                 ;Sayilari 
	    XOR AH,AH                                                              ;Sayilar Byte oldugu icin ah kismi sifirliyoruz
		RET
PUT_STR	ENDP
mycs ends                                                                      ;Codesg'i bitiriyoruz.
end ana                                                                        ;Programi Bitiriyoruz.