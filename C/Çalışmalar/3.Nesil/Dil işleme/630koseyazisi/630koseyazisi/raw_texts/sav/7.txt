O sette olan biteni kendi g�zlerimle g�rd�m

Sevgili Ahmet Hakan karde�imiz Babam ve O�lum'un iyi bir film oldu�unu anlat�p; �ekimleri s�ren Hababam S�n�f� 3.5 i�in olduk�a a��r �u sat�rlar�
yazm��t�. "Demek ki neymi�? �yi bir film �ekmek i�in 'Att�r bir Mehmet Ali, kurtar filmi abi' �eklinde �zetleyebilece�imiz, pespaye pazarlama y�ntemine yaslanmak gerekmiyormu�."

Ay�p etmi� olurum
Kendi g�r���d�r. Kat�lmasak da sayg� duymak laz�m. Ama Hababam'�n setine bizzat gidip oradaki ola�an�st� �al��may�, harcanan eme�i, akan teri g�ren biri olarak 2-3 sat�r yazmazsam ay�p etmi� olurum. 

Oynayan kim?
Her biri k�ymetli sanat��lar olan kadroyu tek tek sayacak de�ilim. Sadece y�netmenini, onun da cemaz�yel evvelini anlataca��m size. 
A�a��da okuyacaklar�n�z sinemas� da yap�lacak kadar �nemli bir sinemac� baba-o�ul �yk�s�d�r. Ve o �yk�de, �ocu�u 'oynayan' ki�i Hababam S�n�f� 3.5'in y�netmenidir. 
