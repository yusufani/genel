Bas paray� �de borcunu� ��z�m de�il 
BAZI ki�iler Amerika�ya g�pta ediyorlar. Paralar� uluslararas� kabul g�rd��� i�in kendi bast�klar� para cinsinden yabanc�lardan bor�lanabiliyorlar. Halbuki, bizim gibi �lkeler basamad���m�z paralar, yani d�viz cinsinden yurt d���ndan bor�lanabiliyorlar. 

Amerika�da devlet borcunun yar�s�na yak�n� yabanc�lardan al�nan bor�lardan olu�uyor. Ama, bor�lar�n�n hepsi dolar cinsinden. Dolay�s�yla, Amerika�n�n �bas�p paray� �der bor�lar�n�� ilkesi ge�erliymi� gibi g�r�n�yor. Ge�ek ise �ok farkl�.

AMER�KA DA SORUNLU

Bizde, �basar�z paray� �deriz bor�lar�m�z�� laf�n� kap�l� kap�lar ard�nda politikac�lar �ok kullan�rlar. Dolar ya da Euro basamad���m�zdan, politikac�lar a��s�ndan sorun olan bor�, d�� bor�lard�r. Uzun s�re, bu nedenle T�rkiye i� bor�lanmaya yeteri kadar �zen g�stermedi. Ama, d�� bor�lanmay� �ok dikkatle yapt�.

Asl�nda, bor�lanman�n hangi para ile yap�ld��� bazen hi� �nemli olmuyor. �nemli olan bor�lanman�n boyutu oluyor. Son y�llarda, Amerikan ekonomisinin d�� bor�lanma ihtiyac� milli gelirlerine g�re y�zde 5�i ge�ti. Bizde de bu oran ge�en y�l y�zde 5�i ge�ti. Yani, g�reli olarak Amerika ile ayn� durumday�z!

Onlar d�� bor�lanma ihtiya�lar�n� dolar cinsinden bor�lanarak ��z�yorlar. Biz ise d�� bor�lanma ihtiyac�m�z�n �ok b�y�k bir b�l�m�n� d�viz cinsinden kar��l�yoruz.

Amerika�n�n d�� bor�lanma ihtiyac�n�n artmas� Amerika�ya bor� verenler aras�nda kayg� yaratmaya ba�lad�. Ellerinde dolar cinsinden Amerikan Hazine bonosu olanlar�n bu bonolar� sat�p kendi paralar�na d�nmek istemesi durumunda, dolar de�er yitirecektir, dolar faizleri y�kselecektir. Son Avrupa Birli�i krizine kadar, bu kayg�larla dolar de�er yitiriyordu. Bu kayg�lar� bir �l��de hafifletmek i�in Amerika�da faizler art�yordu.

G�reli olarak ayn� durumda olan T�rkiye ekonomisinde ise d�� bor�lar�n b�y�k bir b�l�m�n�n d�viz �zerinden olmas� nedeniyle, bas�p paray� �deyemeyece�imiz bor�lar s�z konusudur. Ama, T�rkiye ekonomisi konusunda hen�z ciddi kayg�lar olmad���ndan, kendi param�zla bor�lanamasak dahi, bu a��dan durumumuz Amerika�dan daha iyidir!

�nemli olan toplam bor�lar�n ve yeni bor�lanma ihtiyac�n�n g�reli d�zeyidir. Belli bir noktada, kendi paran�zla da bor�lansan�z, bor� verenin paras� cinsinden de bor�lansan�z, durum de�i�memektedir. �lkinde, bas�p paray� bor�lar�n�z� �deyebilirsiniz, ama ekonomik istikrar diye bir �ey kalmaz ortada. Di�erinde ise basamad���n�z parayla bor�lar�n�z� �deyemedi�inizden ekonomik istikrar bozulur. �kisinin de sonucu a�a�� yukar� ayn�d�r. Yabanc� parayla bor�land���n�zda, ek olarak kur riski al�rs�n�z. Bazen, bu risk de k���msenmeyecek boyutlarda olabilir.

BOR�LARIN D�ZEY�

Bug�n Amerikan ekonomisinin ek bor�lanmas�n�n �ok b�y�k bir b�l�m� geli�mekte olan ekonomiler (emerging markets) taraf�ndan kar��lanmaktad�r. Macaristan, Brezilya ve T�rkiye d���ndaki b�y�n geli�mekte olan �lkeler net bor�lanan de�il, net bor� veren �lkeler olmu�lard�r. Verilen bor�lar�n �ok b�y�k bir b�l�m� de Amerikan ekonomisinedir.

Bir anlamda, dolar�n gelece�i de, geli�mekte olan �lkelerin portf�ylerinde ne kadar dolar tutmak isteyeceklerine ba�l� gibi g�r�nmektedir. Buna kar��l�k, bu �lkeler Amerika�ya mal satt�klar� i�in bor� vermektedirler. Amerikan dolar�ndan vazge�meleri Amerika�ya mal satmaktan vazge�meleri anlam�na da gelebilecektir. Amerikan ekonomisinin sa�l���n� bozabilecek geli�meler herkesi tela�land�rabilecektir. Hangi taraf neyi g�ze alabilecektir?

Ekonomide hi�bir konu g�r�nd��� kadar basit ve d���n�ld��� kadar ��z�mler kolay de�ildir. Bu �e�it kayg�lar� azaltman�n tek yolu bor�lanma ihtiyac�n� kayg� yaratmayacak d�zeylere �ekebilmektir. 
