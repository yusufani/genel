#include <stdio.h>
#define max 100
void matrisiyazdir(int matris[][max],int n,int m);
void matrisisagayansit(int matris[][max],int n,int m);
void matrisiasagiyayansit(int matris[][max],int n,int m);
int main(){
	int i,j,n,m,tmp=1;
	int matris[max][max];
	printf("Lutfen matrisin satir sayisini giriniz:");
	scanf("%d",&n);
	printf("Lutfen matrisin sutun sayisini giriniz:");
	scanf("%d",&m);
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			matris[i][j]=tmp++;
		}
	}
	matrisiyazdir(matris,n,m);
	matrisisagayansit(matris,n,m);
	matrisiyazdir(matris,n,m);
	matrisiasagiyayansit(matris,n,m);
	matrisiyazdir(matris,n,m);
}
void matrisiyazdir(int matris[][max],int n,int m){
	printf("\nMatrisimiz\n");
	int i,j;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			printf("%d",matris[i][j]);
		}
		printf("\n");
	}
}
void matrisisagayansit(int matris[][max],int n,int m){
	int i,j,tmp;
	for(i=0;i<n;i++){
		for(j=0;j<m/2;j++){
			tmp=matris[i][j];
			matris[i][j]=matris[i][m-j-1];
			matris[i][m-j-1]=tmp;
		}
	}
}
void matrisiasagiyayansit(int matris[][max],int n,int m){
	int i,j,tmp;
	for(i=0;i<n/2;i++){
		for(j=0;j<m;j++){
			tmp=matris[i][j];
			matris[i][j]=matris[n-i-1][j];
			matris[n-i-1][j]=tmp;
		}
	}
}
