Neler yapmad�k ki T�rk Sanat M�zi�i i�in...

�yi niyet, saf y�rek s�yleyenler neyse. Bizatihi alemin i�inde olanlar s�yleyince g�c�k kap�yorum. 
Ahkam kesiyorlar. Neymi�; "T�rk Sanat M�zi�i �l�yormu�!" 
�ld�rme o zaman karde�im. Sen ��k bir �eyler yap. Madem i�in �rperiyor, s�va kolunu o �e�it bir g�ftebeste yaz. Ya da o t�rden bir �ark� aranje et. 

S�k�ysa 
Ard�ndan pamuk elini at cebine, parana k�y, kay�t yapt�raca��n bir st�dyo, t�m prod�ksiyonunu kotaracak bir firma bul. S�k�ysa iyi bir mastering yap. 

�u g��s�m y�rt�l�p baksan 
Sonra e�i dostu arac� k�l. Medyadan hat�rl� dostlara ula�. Sayfa s�tun program i�gal et. Telif haklar�n� bihakk�n �de. MESAM, SESAM dola�, belgelerini ��kar, bandrol ayarla, vergini yat�r. Da��t�mc� bul, promosyon s�reci ba�latt�r. 

Bitmedi ki 
Daha yapacak �ok i�in var. Haydi �ok �abuk klip y�netmeni, senaryosu, mekan�, yap�mc�s� tedarik et. Montaj�n�, kurgunu yapt�rabilece�in bir ekip ve o klipi d�nd�recek mecray� ara�t�r. 

Bekle g�r 
Sonra bekle ki; �ark�n kitlelere ula�s�n, harcad�klar�n maddi manevi geri d�ns�n. 
Ahh.. Ah be karde�lerim; "�u g��s�m y�rt�l�p baksan dikenler hangi g�ldendir" be!.. 
