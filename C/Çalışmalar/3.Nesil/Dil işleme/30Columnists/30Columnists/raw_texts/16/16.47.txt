When I was given a diagnosis of breast cancer in February 1999, many friends and readers wondered: "Why did you get breast cancer? You take such good care of yourself!" The same sort of remarks followed the surgery I underwent in December 2004 to replace my badly arthritic knees.

It seems that many people believe that if you do everything "right" (which is not to say that I did everything right, just most things), bad things won't happen.

But bad things can and do happen. And they happen to the "best" and the "worst" of us. Lisa Kron, the author and star of "Well," a very provocative and entertaining play now on Broadway, says: "People tend to think we're all the sole authors of our own fate. But life is more complicated than that.

"If you're blessed with good health, you can say, 'I did it.' But if you lose your health, you know that external forces beyond your control can get in your way." 

Healthy people tend to act as if beneath every sick person is a healthy person trying to come out � that, as Ms. Kron put it in an interview, "people who are sick are just not trying hard enough." Those afflicted by serious depression are often told by others to "pull yourself together," "snap out of it," as if they deliberately choose to suffer.

She added: "Empathy on the part of people who are well is very hard to come by. When you're sick and get better, it's hard to remember what it was like to be sick."

Growing Up Sick

Ms. Kron's play is autobiographical: about her mother, Ann, who has been plagued since childhood with unexplained bouts of extreme fatigue and lethargy that she attributes to allergies, and about herself, similarly afflicted as a child but who then recovered once she left home.

Was Ms. Kron's illness "real" (as the allergy clinic she checked into insisted it was) or were her symptoms, so like her mother's, a morbid form of identification with a mother she loved for her caring ways and admired for what she was able to achieve despite her illness? 

"I come from a family where everyone is ill," Ms. Kron explains in the opening scene. "It is the norm. The presumption of illness is so strong that it's how we keep time. People in my family say things like: 'Now I know for a fact the warranty's not up on that dishwasher. I got it the winter I had congestive heart failure seven times.' When I fill out forms with family medical history sections where you check off the little boxes, I check them all."

In the interview, Ms. Kron conceded, "To some extent, my illness could have been learned behavior. Part of the reason I could leave the family culture was that my mother completely encouraged me to do that. She wanted me to have a different kind of life than she had."

Ms. Kron said she recognized that she needed a new identity. "When your identity is wrapped around being a sick person, you see a need to keep saying, 'I'm really sick.' Who will I be if I'm not sick anymore? But that's not to say you caused yourself to be sick or that you don't want to get well."

And as far as Ms. Kron can tell, her mother is really sick, even though no doctor she consulted could ever put a proper label on it. One doctor told her on six occasions that she had mononucleosis. Another labeled it "tired housewife syndrome," and still another thought she suffered from boredom and challenged her to get out of the house and find something she's interested in.

All of these labels were laughable to her daughter, who described her mother as "an incredibly curious, engaged person who is unstoppable on the days she has energy. But a lot of the time she has bone-deep physical lethargy that she can't overcome."

Ms. Kron tells her audience: "My mother is a fantastically energetic person trapped in an utterly exhausted body. It's very confusing. Her energy level has two settings: all or nothing. Most of the time, it's nothing, but when she has a burst of energy, it's awe inspiring."

In the interview Ms. Kron said: "I could look at my mother and say she can do X, Y and Z and make herself better. But she may not have the same resources I have. Individuals can act upon opportunities, but not everyone has the same opportunities."

In the play, Ann herself says: "You blame yourself. Wasn't it Susan Sontag who pointed out that whenever the cause of an illness is mysterious, it's assumed to come from psychological problems or a moral weakness? And once science finally figures out the medical root of the illness, that assumption disappears."

Will we one day have a better � that is, more scientific � understanding of ailments like chronic fatigue syndrome, fibromyalgia, Gulf War syndrome, multiple chemical sensitivities or any of the other current "wastebasket" diagnoses that many medical and lay people consider psychosomatic? 

I certainly hope so. After all, we now have scientific explanations for past "mysteries" like depression, post-traumatic stress disorder, fainting spells and attention deficit hyperactivity disorder. And we now know that autism, migraine headaches and transsexuality are not caused by bad mothering.

Psychology's Role

This is not to say, of course, that psychological makeup plays no role in people's propensity to develop certain diseases, or their ability to overcome illness when recovery is at least a possibility. People who are pessimistic or fatalistic, for example, may choose behaviors � like smoking, overeating or drinking heavily � that reflect their belief that they are doomed no matter what they do, so they may as well do what they like.

Likewise, optimism doesn't hurt even when the decks seem clearly stacked against people. Dr. Dan Shapiro, a clinical psychologist and associate professor at the University of Arizona medical school, a determined survivor, somehow weathered two bone marrow transplants. The second transplant enabled him to beat Stage 4 Hodgkin's lymphoma and subsequently marry, become a father and make a career of teaching doctors that patients are people, not "the pneumonia in Room 12." 

However, optimism is clearly not a cure-all. The most optimistic person I know is being treated for her seventh recurrence of chronic lymphoma. Christopher Reeve, paralyzed from the neck down in a horseback riding accident in 1995, lived nine more years fostering research on spinal cord injuries, convinced he would walk again. 

What is the bottom line? We should not be too quick to dismiss symptoms that seem to lack a physiological basis. A little empathy can go a long way while medical scientists continue to uncover physical explanations for what may now seem to be psychosomatic symptoms.

