Blowing your nose to alleviate stuffiness may be second nature, but some people argue it does no good, reversing the flow of mucus into the sinuses and slowing the drainage.

Counterintuitive, perhaps, but research shows it to be true.

To test the notion, Dr. J. Owen Hendley and other pediatric infectious disease researchers at the University of Virginia conducted CT scans and other measurements as subjects coughed, sneezed and blew their noses. In some cases, the subjects had an opaque dye dripped into their rear nasal cavities.

Coughing and sneezing generated little if any pressure in the nasal cavities. But nose blowing generated enormous pressure � �equivalent to a person�s diastolic blood pressure reading,� Dr. Hendley said � and propelled mucus into the sinuses every time. Dr. Hendley said it was unclear whether this was harmful, but added that during sickness it could shoot viruses or bacteria into the sinuses, and possibly cause further infection. 

The proper method is to blow one nostril at a time and to take decongestants, said Dr. Anil Kumar Lalwani, chairman of the department of otolaryngology at the New York University Langone Medical Center. This prevents a buildup of excess pressure. 
