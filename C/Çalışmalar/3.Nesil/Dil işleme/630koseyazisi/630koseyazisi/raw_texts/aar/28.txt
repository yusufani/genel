Kuruyup kalm�� yer bezi gibiydim  
     

  
Suyu bo�alt�lm�� bir kovan�n i�inde kuruyup kalm�� yer bezi gibiydim.

Kaskat� yani.

Esneme yetene�ini kaybetmi� bir kuma� par�as�.

Kat�r kutur.

Pa�avra da diyebiliriz.

Hem size ne, ben kendimi �yle hissediyordum!

Korkak, g�vensiz ve yaln�z.

Kolumu k�p�rdatmaya halim yok, �ylece duruyorum.

Daha do�rusu u�uyorum.

Bir yerlere...

Gri, so�uk bir kente.

Cam kenar�nday�m, bir yan�m bo�, ondan sonra da �u 7B var.

��Yolculu�a ba�larken varaca��n�z yerin �nemli oldu�unu d���n�rs�n�z ama vard���n�zda esas �nemli olan�n yolculuk oldu�unu anlars�n�z.��

Elimdeki kitaptan b�yle bir c�mle okuyorum.

Elimde olmadan 7B'ye bak�yorum.

Konu�madan bu yol m�mk�n de�il bitmez.

Ama biliyor musunuz, konu�acak halim de yok.

7B de oral� de�il zaten!

* * *

Akl�ma 26C geliyor.

Hey gidi g�nler...

Bir New York yolculu�unda tan��m��t�k.

U�a��n en cazip adam�yd�.

Ben de en �ansl� kad�n�!

�ok keyifli bir yolculuktu.

Sonradan da arkada� olduk zaten.

Ne var ki... 

Ne ben ne de 7B birbirimize b�yle bir yolculuk vaadediyoruz.

Ayr�ca 7B de 26C kadar gen� ve parlak durmuyor!

Ee ben de yer bezi oldu�uma g�re.

Kader utans�n, ki�isel tarihimize ge�ecek bir u�ak seyahati olamayacak!

* * *

Kim ba�lad� ilk konu�maya hat�rlam�yorum.

Ama 7B'yi seyretti�i mafya filmi sarmam��t� galiba, g�z�n�n ucuyla okudu�um kitaba bakt�.

Belli ki, adam T�rk de�il.

Kitab�n ad�na bakacak da ne olacak!

Cidden bilmiyorum, neden, nas�l ama �ngilizce bir sohbete dal�verdik.

Birden 7B, bana ��Living well is the best revenge�� gibi bir laf ediverdi.

Hayattan al�nacak en iyi intikam iyi ya�amakt�r yani.

A aaaaa!

Ben bu laf� bir yerlerden hat�rl�yorum.

O zaman da �ok ho�uma gitmi�ti.

Hatta �lm��t�m.

�akt� �im�ek sonunda!

The Marmara'n�n cafe'sinde g�rm��t�m bu c�mleyi, Bodrum'daki otellerinin tan�t�m slogan�yd� ve cafe'nin duvar�nda as�l�yd�.

Bazen c�mlelere a��k oluyorum ben.

��Living well is the best revenge�� benim i�in i�te �yle bir c�mle.

* * *

��Nereden biliyorsunuz bu c�mleyi?�� dedim.

Yakalanm�� gibi.

Biraz salak gibi.

Damdan d��er gibi.

K���k bir tebess�m...

��Ben yazd�m�� dedi.

Ama ekledi:

��Bir �talyan atas�z�nden esinlendim.��

��Siz kimsiniz?�� dedim.

�stelik kendi dilimde!

��Ben Paul�� dedi.

* * *

O 7B, Paul McMillen ��kt�.

Hi� mi g�rmedin adam�n foto�raf�n� be kad�n?

Hay�r, g�rmedim!

Ama bin y�ld�r ismini duyar�m, hep merak ederim, nedense bir t�rl� f�rsat�n� bulup tan��amam��t�m.

7B bir derya!

O konu�tuk�a kovaya su doluyor, kat�l�p kalm�� yer bezi de yeniden esnekli�ini kazan�p, sa�a sola hareket etmeye ba�l�yor.

Adam ya�am verdi bana o u�ak yolculu�unda!

Ayaklar�m�n dibine kadar d��m�� ruhumu, ba��m�n �st�ne kadar ��kard� yeniden.

7B, m�thi� bir �rlandal�.

Ben hari� b�t�n T�rkiye tan�yor onu.

Ama T�rkleri o kadar iyi ��zm�� ki, �rlandal� m� T�rk m� bir t�rl� ay�ram�yorsunuz.

Bir de �����leri s�ylebilse hi� ku�ku duymadan T�rk oldu�una inan�rs�n�z!

* * *

RPM Radar CDP Europe'�n sahibi Paul McMillen, bir reklamc�. 

Ama ayn� zamanda bir foto�raf sanat��s�.

Her y�l sergileri oluyor 7B'nin ve yapt��� i�lere ku� kondurmaya bay�l�yor. Allah i�in adam�n elinden de bin bir t�rl� numara geliyor. Yabanc� birinin zaten bizim �lkemizde bu kadar sevilmesi, tolere edilmesi, pes yani. Adam resmen bir ekol yani. Okumaya, ara�t�rmaya merakl�. K���k bir o�lan �ocu�uyken metrolarda m�zik bile yapm��. 

Alg�lar� a��k.

Samimi, direkt.

New York'ta �irket kurmu�, ard�ndan Londra'da, sonra bir T�rk kad�na a��k olmu� kendini T�rkiye'de bulmu�.

Sizler onu nereden mi tan�yorsunuz?

Solo reklamlar�ndan, ��a� kapa��l� Artema'lardan, Pamukbank reklamlar�ndan filan falan.

Bula�mad��� alan, hizmet vermedi�i �irket var m� ki zaten 7B'nin?

* * *

Woodstock'tan a�ka, serseri hayatlardan medyaya, �nci Baba'dan Orhan Gencebay'a, Ertem E�ilmez'in sinemas�ndan �ener �en'in oyunculu�una, bayram i�in Katmandu'da bulunan e�inden bana benzeyen kuzenine kadar... 

Tonlarca �ey konu�tuk.

O kadar keyifli bir seyahatti ki... 

O u�akta 7B ailem oldu.

Ali'den (o benim ger�ek amcam) ve �lhan'dan (o bizim milli amcam�z!) sonra bir de Paul Amcam var art�k.

Bunu Paul McMillen g�ls�n diye yaz�yorum!

Yoksa, ona amca demek haddim de�il.

O zaten arkada��m.

Neyse...

O gitti�im kentte g�r��emedik onunla.

Ama burada birlikte yemek yeme�i bekliyorum.

Bu yaz�y� da zaten 7B beni yeme�e ��kars�n diye yaz�yorum.

G�rg�s�zl���m� ho�g�r�n.

Burnumun dibinde ya�ayan �ahane bir adam�, Allah'�n u�a��nda tan�y�nca sevindirik oldum da...

 
