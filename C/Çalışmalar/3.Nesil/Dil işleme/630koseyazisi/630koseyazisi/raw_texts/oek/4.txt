Ka�amak yap�yor.  

BA�BAKANLIK M�ste�ar� �mer Din�er kendisinin 1995'te yapt��� ve halen de arkas�nda durdu�unu itiraf etti�i konu�ma nedeniyle o g�revden ayr�lmas� gerekti�ini s�yleyenlere yan�t vermi�. S�zleri d�nk� gazetelerde vard�:

'Bu �lkede cumhuriyet d�neminde do�mu� ve cumhuriyete kar�� laf edebilecek insan�n, akl�n� peynir ekmekle yemi� olmas� laz�m'm��. 

Say�n Din�er bu tart��malar�n cereyan etti�i g�nlerde Fatih Camii'nin avlusunu dolduran sar�kl�lar�n da, 'cumhuriyet d�neminde' do�mu� oldu�unu herhalde reddedemez.

Onlardan birine gidip de sorun bakal�m, cumhuriyete kar�� neler s�yl�yorlarm��.

Demek istedi�imiz, Say�n Din�er'in 'ka�amak' yan�tlarla ger�e�i saklayamayaca��d�r. Nitekim bu s�zleri s�ylemek �zere �a��rd��� gazetecilerin 'laikli�e bak���' hakk�ndaki sorulara verdi�i yan�t da, en az yukar�daki kadar 'ka�amak't�r. ��yle diyor:

'Ben konu�tuk�a, kar�� taraf (kar�� taraf dedi�i laik cumhuriyeti savunanlar) cevap verdik�e b�y�yecek bir mesele. �lkeyi germeyelim.'

Yan�t�n anlam� �ok a��k. Din�er:

'Cumhuriyet ilkesinin yerini kat�l�mc� bir y�netime devretmesi gerekti�i ve nihayet LA�KL�K ilkesinin yerinin �slam'la b�t�nle�mesi (laiklikten vazge�ilmesi demek istiyor ama cesaret edemiyor) gerekli oldu�u kanaatini ta��yorum. B�ylece T�rkiye Cumhuriyeti'nin ba�lang��ta ortaya koydu�u b�t�n temel ilkelerin, LA�KL�K, CUMHUR�YET ve M�LL�YET��L�K gibi bir�ok temel ilkenin yerini  daha M�sl�man bir yap�ya devretmesi zorunlulu�u ve art�k bunun zaman�n�n geldi�i d���ncesini ta��yorum' �eklindeki s�zlerinden kesinlikle vazge�medi�ini ifade ediyor. 

�lkeyi germeyeceksek laik cumhuriyete ba�l�y�m desene!

An�msayaca��n�z gibi, 1995 tarihli konu�mas� g�ndeme gelince kendisi bir a��klama yapm�� ve 'o tarihte s�ylediklerinin do�ru ��kt���n�' vurgulam��t�. Amac� 'O zaman �ng�rd���m �slam motifli iktidar ger�ekle�ti' mesaj�n� vermek. Nitekim mahut konu�mada:

�ktidara geldikleri zaman 'modern devleti �slam'a terc�me ederek kullanmaya kalk��mak veya b�rokratik mekanizmada yer alacak memurlar� dindar insanlardan se�mek, devletin yap�s�n�, �slam'�n �ng�rd��� yap�ya kavu�turur mu?' dedikten sonra bunu yetersiz buldu�unu ��yle anlat�yor:

'Asl�nda bu t�r bir e�ilimin, �slami iktidar� de�il, be�eri yan� a��r basan bir iktidar� �ne ��karaca�� kanaatindeyim.

Modern devletin �slam'a terc�me edilerek kullan�lmas� B�Z�M A�IMIZDAN �NEML� MAHZURLAR DO�URACAKTIR. ��nk�, bug�nk� b�rokratik mekanizma do�rudan do�ruya dayatmac� bir mekanizmad�r.  Halbuki �slam'�n  dayatmac� olmad���n� hepimiz biliyoruz.'

Say�n Din�er g�r�ld��� gibi b�rokrasinin dindar insanlara teslimini bile yeterli g�rm�yor. Devletin de �slamile�tirilmesini istiyor.

Laik cumhuriyete inanan bir insan, b�yle bir zat Ba�bakanl�k M�ste�arl��� makam�nda otururken geceleri rahat uyuyabilir mi?
 
