﻿Umut veren şehir

Eğer büyük şehirlerimizdeki keşmekeşten bunalıyorsanız umutlarınızı kazanmak için bir turizm şehri olmaya başlayan Eskişehir ve civarına göz atmalısınız

Geçen hafta, Eskişehir’in merkez kazası olan Odunpazarı Belediyesi beni bir konferans için davet etti. Belediye başkanımız Avukat Kazım Kurt, bu devre görevine yeni başladı. Büyükşehir Belediye Başkanı Profesör Yılmaz Büyükerşen’le de görüştük. 1961’den beri muayyen zamanlarda uğradığım Anadolu Üniversitesi’nde bir dönem ders verdiğim bir şehir bu. İmparatorluk tarihimizin, cumhuriyete geçişimizin ve bugüne kadarki Türkiye’nin, Türk toplumunun geçirdiği değişimleri burada gözlemek ve izlemek mümkün. Osmanlı İmparatorluğu bu topraklarda doğup büyüdü. Geçen asırlarda Ertuğrul livasıydı.

Özellikle 1877 Osmanlı-Rus Savaşı’ndan sonra Balkanlar’dan ve Kırım’dan göç edenlerin yerleştiği bir bölgeydi. 1890’larda demiryolu buraya ulaştı; buradan Ankara’ya ve Konya’ya uzandı. Hat boyunda yerleşenler bu demiryolu sayesinde hayat buldular ve 1890’lı yılların başından beri İstanbul’un tahıl ihtiyacı, Avrupa’ya kadar uzanan tiftik ticareti bölgeye canlılık getirdi.

Sokak siyaseti hiç iltifat görmedi

1920-1922 yıllarında zor günler geçirildi ama Eskişehir bir tür demiryolu savaşı olan Kurtuluş Savaşı’nın merkezlerindendi. Demiryolu işletmesinin önemli bir kavşak noktası ve idare merkeziydi. Tahıl Türkiye’nin temel üretimiydi. Zirai teknolojinin geliştiği alanlardan biri Eskişehir’di. Bu durum sağlıklı bir çiftçi sınıfını ortaya çıkardı.

Denebilir ki 19’uncu asrın ikinci yarısından itibaren bu ülkede üç bölge ziraatın getirdiği zenginlikle atılım yapmıştır: Ege Bölgesi, Çukurova, Eskişehir-Ankara yöresi. Başka mekanizmalar da işledi. Her köyün okumuş gençleri toprağın aşırı parçalanmasını ve fakirleşmeyi önledi. Devlet sınai yatırımlarını yaptı ama özel sektör ve orta sınıf girişimciler de onu izledi. Bununla birlikte, 1960’ların başında dahi, mütevazı hayat yaşayan, Odunpazarı’nın eski evlerinde veya Muttalip Caddesi’nin çevresinde kireç badanalı orta avlusu ve etrafında göz göz odalarıyla tipik Anadolu ve Balkan Türk köylerinin havasını taşıyan bir şehirdi.

Lisesi ve ticaret yüksek mektebi eskidir. Birçok Anadolu şehrinin aksine burada kültürel faaliyetlere düşkün bir gençlik vardı. Ankara’daki tiyatro çevrelerinde Kızılay’a kan satarak tiyatro kuran gençlerden söz ediliyordu. O gençlerden biri, Anadolu Üniversitesi’nin ilk rektörü ve bugünün belediye başkanı Profesör Yılmaz Büyükerşen’dir.

Büyükerşen, Anadolu Üniversitesi’ni ilginç bölümlerle kurdu. 12 Eylül döneminde başka bölgelerden sıkıyönetim tarafından sürülen genç asistanları bu üniversiteye aldı. Üniversite gelişti. Sokak siyaseti ve kitle hareketleri burada pek iltifat görmedi. İstanbul ve Ankara’dan gelen profesörler sömestr boyunca yolculuk yapmaktan hiç yüksünmediler, üniversiteyi ve öğrencileri seviyorlardı.

Yeşil alanlar arttı, yeni mekanlar ortaya çıktı

Derken Büyükerşen, büyükşehir belediye başkanı oldu. Sanatları seviyordu. Kahvehanelerde rahatsız edilmeden oturan kızlı erkekli üniversite gençliği için pub’lar kurdu, şehirde tiyatrolar açtı, müzikhollerin ve salonların sayısını artırdı. Bugün kıymetli ilan edilen bunaltıcı sözde kentsel dönüşümü o sessizce ve ölçüyle yaptı.

Anadolu’da tramvayı ilk önce Eskişehir tanıdı. Taşan veya yazın kokan Porsuk Çayı düzen altına alındı; üstü kapatılmadı, aksine Venedik’teki gondollar taklit edildi, şehrin yeşil alanları artırıldı, insanların buluşacağı ve vakit geçireceği mekanlar, restoranlar ortaya çıktı. Şehirde konferanslar, oda müziği konserleri, senfonik konserler, hatta opera temsilleri bile veriliyor, misafir tiyatrolar turnelerinde mutlaka uğruyorlar.

Eskişehir gençliği bir üniversite şehrinde yaşıyor. Ama bu üniversite şehri sadece talebe sayısıyla bu unvanı almıyor, zira iki üniversitedeki öğrenci sayısı komşu vilayetlerdeki öğrenci sayısının çok altındadır. Daha iyi imkanlarla, daha çok öğretim üyesiyle üniversite gençliği öğretim görüyor ve şehir onları eğitmeye hazır. Elbette mükemmele daha yol var ama öbür taşra üniversiteleriyle karşılaştırılamayacak bir düzgünlük var.

Eski merkezde dönüşüm dikkatli yapılıyor

Şehrin eski merkezi Odunpazarı canlı, çekici bir şekilde restore edilmiş. Yeni alanlarda imar düzenine dikkat ediliyor. Eski merkezde dönüşüm dikkatlice yapılıyor. İlk bakışta gördüğümüz yeşil, düzenli büyüyen insanı betonla bunaltmayan şehir insanları ve üniversite gençliğini tanıdıkça daha başka yönleriyle ortaya çıkıyor; yenilenen sağlıklı büyüyen şehir, yeni bir şehirli kitlesi ortaya çıkarmış.

800 bin nüfuslu bu şehirde ne iç göç tahribatı ne şaşkın görünen öğrenci gençlik ne de işsizliğin yarattığı bunalım göze çarpıyor... Burada yeni bir insan tipolojisinin, yeni bir profilin ortaya çıktığı görülüyor. Bu büyükşehrin ve diğer belediyelerin dikkatiyle ortaya çıktı.

Büyük şehirlerle bağlantı, etrafı kontrol edebilme ve şehrin bizzat kendisinin yaşanır bir yer haline dönüşmesi tam bir başarıdır. Eğer büyük şehirlerimizdeki keşmekeşten bunalıyorsanız umutlarınızı kazanmak için Eskişehir ve civarına göz atmanızda fayda var çünkü inanılmaz bir gelişme; Eskişehir turizm şehri olmaya başladı ve geleceğin Türkiye’si böyle şehirlerle donanır diye umutlanabilirsiniz.