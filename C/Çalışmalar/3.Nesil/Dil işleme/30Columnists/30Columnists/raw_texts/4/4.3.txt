TIWONDER if Iain Gray did a dry run before that stunt at First Minister's Questions in Holyrood last week, when he ripped up a copy of the SNP manifesto to symbolise the breaking of Nationalist promises. Did he have a practice beforehand with a folded-up Woman's Realm?
Did his copy of the manifesto already have a wee rip in it to get him started? Imagine the ignominy if he hadn't been able to tear it in two. The sight of Gray standing there � like Charles Hawtrey wrestling with a telephone directory � would undoubADVERTISEMENTtedly have destroyed his political career. Scottish Labour would have been looking for its fourth leader in 18 months.

Gray may have survived this test but there is another one facing him: holding to account an SNP administration which, after last week's events, looks less and less like a national government and more and more like a minority sect. 

Credit where credit is due. First Minister Alex Salmond's acceptance of defeat on Local Income Tax (LIT) is to be welcomed. True, the policy had merits � its main advantage over the council tax was it more accurately reflected ability to pay. But its fundamental flaws were cruelly exposed by the economic recession. In a slump, income tax receipts slump too as unemployment increases and pay rises are curbed.

The burden of income tax falls on a diminishing number of people. Local government, unlike central government, cannot tweak other forms of revenue to compensate. The simple truth is that in a recession LIT cannot provide stability and isn't fit for the purpose. And what use is a tax policy that only works in the good times?

Last week's defeat should have been the cue for Scotland's political classes to defy their detractors. It should have been an opportunity for the SNP to say: "Okay, our plan isn't going to work � let's put our heads together and find one that does." It should have been the start of negotiations between all the Scottish parties, examining reforms to the council tax to make it more progressive and to curb its obvious iniquities.

Talks could have started on finding a new consensus: perhaps adding new council tax bands at the upper and lower ends to reflect ability to pay; introducing rebates for pensioners living in the former family home on fixed incomes; extending housing benefit to water and sewerage charges; to name just three possible ways forward. Through co-operation with other parties the SNP could have succeeded in its goal of making local taxation fairer.



But no. Just seconds after Finance Secretary John Swinney announced the U-turn he declared his intention to put the whole subject on hold. Instead, the SNP would make LIT a flagship policy at the next Holyrood election in 2011. In other words, rather than lose some political face, the SNP is happy to see Scots continue to suffer under a tax the party regards as one of the country's greatest social evils.

Maybe this is a new-fangled kind of campaign technique � punish the people in the short term so they're more likely to vote for you in the long term. If so, I'm not convinced it will work. If local taxation is broken, why not fix it now? If a pipe is leaking it's foolish to refuse to mend it because you can't use your favoured brand of duct tape.

Local government finance might seem like the dullest of dull subjects, but this is deceptive. It can, in fact, make or break governments and political careers. For many Scots the poll tax remains an emblem of pernicious Thatcherite rule. As a fresh-faced Scotland on Sunday reporter in the late 1980s, I was one of the hundreds of protesters in the Can Pay, Won't Pay camp, and for my trouble I had my wages arrested by the sheriff's officers. The principle behind that campaign � fairness in taxation � is one that's still worth fighting for.

Swinney's refusal to compromise on LIT is a disappointment but not a surprise. It's just one more example of the SNP refusing to accept that minority government brings a duty to compromise, with a view to getting things done in the cause of the national interest. SNP leaders seem more interested in protecting short-term political advantage. If they're not careful it may mean long-term political damage. 

Let's remind ourselves what Salmond set out to achieve on taking power. The aim was to challenge the perception that the SNP represented a risk. What the Nationalists had to do was to show they could preside over a stable and productive administration, and do Scotland proud. 

In his acceptance speech on being appointed First Minister on May 16, 2007, Salmond said this: "All of us in the Parliament have a responsibility to conduct ourselves in a way that respects the Parliament that the people have chosen to elect. That will take patience, maturity and leadership on all sides of the chamber. My pledge to the Parliament today is that any Scottish Government that is led by me will respect and include the Parliament in the governance of Scotland over the next four years."

Fine words. But has he lived up them? On the evidence of last week, the answer is no. The Parliament has shunned the SNP's tax plans, and Salmond has taken the huff. Wouldn't it do more for the SNP's long-term goals if the party could prove its ability to work with others to improve the government of Scotland and the lives of ordinary Scots? Where is that patience, maturity and leadership that Salmond promised less than two years ago? 

We expect more of our First Minister, we really do.
