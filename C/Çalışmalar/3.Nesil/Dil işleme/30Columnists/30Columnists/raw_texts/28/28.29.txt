MEMBERSHIP of �englandfans� �70. Wembley ticket �50. Car park �20. Barely edible fish and chips meal deal with flat fizzy drink �8. Programme �6. Booing Ashley Cole � priceless.

Yep, I handed over all that cash to watch the Kazakhstan match from a seat slightly lower than the height of Mount Everest.

No, I didn�t actually boo England�s left-back, but I understand why some folk did and they don�t deserve the lectures they have received since from some of my friends who were in the press box. 

Nor do they deserve ill-informed criticism from self-styled �real� fans who were not even there.

Cole has produced a sick note and will miss tonight�s game, but in case any of our chaps are jeered by England supporters in Belarus, here is the truth about what happened at Wembley.

The canard about the Wembley spectators not being �genuine� fans or �proper� supporters is an ignorant slur. Next to me sat a nice, ordinary couple with three sons all aged under 12. So they had spent �250 just on tickets. What more do you have to do to qualify as genuine?

Those in Belarus tonight have forked out about �300 a head just on travel costs and taken three days off work. They will all have been at Wembley as well. Are they proper enough for you?

Then there is the daft idea that it was Arsenal fans who had a go at poor little Ashley. 

The booing was started by spectators sitting close to Cole when he donated a goal to Kazakhstan.

The chorus of disapproval was quickly taken up by thousands of other fans around the entire stadium. Most of us didn�t join in, but those who did were of all shapes, sizes, ages and affiliations.

 

  

When Cole made that slipshod, careless blunder, some of the fans who help pay his exorbitant wages booed him.
Was that helpful? No. But what else could they do?

And it was only booing. It wasn�t the despicable abuse dealt to Sol Campbell by Spurs fans or the vitriol spewed out by Arsenal fans when Cole faces his old club. It was just booing.

Often, when England fans boo, they are badly mistaken. 

We can now see how stupid it was to boo Peter Crouch and Owen Hargreaves before the 2006 World Cup. Frank Lampard has had to live with unfair and unthinking insults.

Those three proved their hecklers wrong, and perhaps, with Wayne Bridge understudying for him tonight, we will start to appreciate just what a gifted player Cole is.

And then, without doubt, there will be another pantomime villain to boo It�s not a big deal. 
