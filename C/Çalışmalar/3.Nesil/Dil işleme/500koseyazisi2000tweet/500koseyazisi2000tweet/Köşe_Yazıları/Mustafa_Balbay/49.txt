﻿‘3-5 çapulcu!’
1984 yılında PKK terörünün başlamasının ardından devlet yaşananların adını koymakta zorlandı. Adım adım olay sayısı ve verdiğimiz şehit sayısı arttıkça, devletin kurumları farklı adlar takmaya başladılar. Askeri yetkililer çoğunlukla “Bu kış işi bitiriyoruz” mesajları verdiler. Hükümet yetkilileri, devletin büyüklüğünü anlattılar, hiçbir şekilde böylesi olaylara prim vermeyeceklerini vurguladılar. 
12 Eylül askeri darbesinin devamında 1983’te yüzde 45.1 ile iktidara gelen Özal, her konuya olduğu gibi buna da büyük bir özgüvenle ve yüksekten bakarak yaklaştı. 
Bir yaz günü yine terör haberlerinin gazetelerin birinci sayfasını işgal ettiği bir dönemde Özal, bir askeri birliği tatil bölgesinde olduğu için şortla denetlemişti. Özal gazetecilerin PKK terörüne karşı ne tür önlem alınacağı sorusuna şu karşılığı vermişti: 
“Devlet 3-5 çapulcuya teslim olmaz...” 
Devlet elbette gücünü göstermelidir, otoritesini kurmalıdır. Ama bunun her şeyden önce hukuk ve adalet zemininde olması gerekir. Eğer o zemin kayarsa, devlet gücünü kullananların çıkar amaçlı suç örgütlerini yönetenlerden farkı kalmaz. 
Özal öyle dedi ama ne yazık ki terör artmaya devam etti, boyutları değişti. Devlet gücünü kullananlar da “3-5 çapulcuya” haddini bildirmek için, hukuksal zemini tartışılan her türlü işe girişti.

***

1984’ün üzerinden 31 yıl geçti. AKP, Cumhurbaşkanı Erdoğan, 7 Haziran seçimlerinden sonra artan teröre karşı nasıl önlemler alınacağı sorusuna önceki gün şu karşılığı verdi: 
“Bunlar 3-5 çapulcu... Devlet bunları da aşar...” 
Tablo gösteriyor ki, bir arpa boyu bile yol gidememişiz. 
Özal, 3-5 çapulcuya haddinin bildirilmesi kadar basit olmadığını anlayınca, kamuoyunda “Eve Dönüş Yasası” diye bilinen terör örgütünce dağa götürülen gençlerin topluma kazandırılması için özel düzenleme yaptı. 
Özal’dan sonra da devlet bir dönem terör örgütüyle, onu destekleyen toplumsal kesimlerin bağını kesmek için “şefkatli” davrandı, “Eve dönün” dedi; bir dönem de devletin gücünün her şeyi yeneceğini ilan edip “kudretli” davrandı... 
Aynı gelgiti AKP de Erdoğan da yaşıyor. Özal’ın başlattığı eve dönüş yasalarının onuncusunu geçen yıl 1 Temmuz 2014’te Erdoğan çıkardı. Aradan bir yıl geçti geçmedi görüyoruz ki, her eve dönüş yasası bir süre sonra başa dönüş yasası olmuş.

***

Gelmişe geçmişe ders vermekten, ders almaya fırsat bulamayan Erdoğan, dün elini sıktığı kişilerin bugün canını sıkıyor. 
Biz bu satırlarda terörü irdelerken öncelikle “nereden gelirse gelsin, hedefi ne olursa olsun terörün her türlüsüne hayır” dedik. Devamında da terörle mücadeleyle, teröristle mücadelenin ayrı şeyler olduğunu, toplumu kazanmanın her zaman öncelikli olması gerektiğini söyledik. 
AKP 2008’de açılımlara başladığında, en kötü barışın bile en iyi savaştan daha iyi olduğunu vurguladık, şöyle devam ettik: 
İç barışı kurmak için bunun kalıcı olmasını sağlamanın yollarını ihmal etmemeli, iç barış günlük hesaplara, sandık çıkarlarına alet edilmemeli. 
AKP yakın geçmişten 180 derece döndü. Biz yine yukarıda söylediklerimizi haykırıyoruz. O gün bizi, barışa tam destek vermemekle suçlayanlar şimdi de savaşa tam destek vermemekle suçluyor. Gelinen noktada bu topraklarda iç barışın yolu çıkarcı, çok yüzlü AKP iktidarından kurtulmaktan geçer!