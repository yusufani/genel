Yaşayan ölüler ülkesi!..

İzmir-Aliağa’daki Alp Oğuz Anadolu Lisesi Resim öğretmenlerinden Zeynep Keklik’in, başarılı öğrencisi Benan’ın o yarışmaya katılmasını çok istiyor ve mutlaka derece alacağına inanıyordu.
Öğretmeninin ısrarına dayanamayan Benan Eryaşa da, Ulaştırma, Denizcilik ve Haberleşme Bakanlığı’nın 81 ildeki resmi ve özel okullar arasında açtığı resim yarışmasına katılmaya karar veriyordu.
2013 yılındaki yarışmanın UNESCO tarafından belirlenen konusu; büyük denizci ve bilgin Piri Reis’in çizdiği ilk dünya haritasının 500. yıldönümüydü.
Benan çoğu kez olduğu gibi bu yarışmada da torpil yapılacağını düşünüyor, ama kendisini teşvik eden öğretmenini de kırmak istemiyordu.
O nedenle, yarışmaya katılacak eserini iki ders arasında alelacele çiziverdi.
Yaptığı tabloda Piri Reis’in göz pınarından yaşlar süzülüyor ve o damlalar çıpaya (veya çapa) dönüşüyordu.
Yani Piri Reis ağlıyordu.
Acaba neden?

* * *

Benan Eryaşa bu sorunun cevabını, yarışma sonuçlanıp, ikinciliği kazandıktan sonra, TRT’nin yaptığı röportaj sırasında veriyordu.
“Piri Reis neden ağlıyor?” diye soran muhabir şu çarpıcı cevabı alıyordu:
“Piri Reis ağlıyor, çünkü günümüzde onun torunlarına, yani Deniz Kuvvetleri’nin pırıltılı subaylarına yapılan zulmü 500 yıl önceden görüyor!
Evet, Piri Reis ağlıyor, çünkü o deniz subaylarının kumpaslar ve iftiralarla zindanlara atıldığını biliyor.
Bu nedenle gözyaşlarını tutamıyor!..”
TRT muhabiri hiç beklemediği bu tokat gibi cevap karşısında şaşırıyor ve konuyu değiştirmeye çalışıyordu:
“Büyük katılımın olduğu böylesine önemli bir yarışmada derece aldığınız için mutlu olmalısınız!..”
- “Hayır, çok mutsuzum!.. Ama benim gözyaşlarım içime akıyor. Çünkü sevgili babam Deniz Kurmay Albay Koray Eryaşa, Balyoz ve Askeri Casusluk Davası adlı kumpaslar nedeniyle tam 5 yıldır cezaevinde bulunuyor. O ve arkadaşları zindanlarda çürürken ben nasıl mutlu olabilirim?..”

* * *

Tahmin edeceğiniz gibi, Benan’ın muhabiri şaşkına çeviren cevapları TRT’de yayınlanmıyor!
Kurmay Albay Koray Eryaşa mı?
Yaklaşık 6 yıl kadar Maltepe, Hasdal, Hadımköy ve İzmir Şirinyer’deki askeri cezaevlerinde süründükten sonra 4 Aralık 2014’te özgürlüğüne kavuşuyor!
Emekli Kurmay Albay Eryaşa şimdi bir yandan yaşadığı travmayı atlatmaya, diğer yandan iki kızına iyi eğitim aldırmaya uğraşıyor.
24 yaşındaki abla Berrak Fransa’da Genetik Mühendisliği üzerine doktora yapıyor, sonra da aldığı davetle Harvard’a gitmeye hazırlanıyor.
Liseyi bitiren Benan da Viyana’daki Almanca eğitiminin ardından Viyana Teknoloji Üniversitesi’nin mimarlık bölümünde okuyacağını belirtiyor.

* * *

Sohbetimizin sonuna doğru Koray Eryaşa acı acı gülüyor;
“Ne umutlarımız vardı” diyerek başladığı sözlerini; “Bizim hayatlarımızı kaydırıp, yaşayan ölüler haline getirdiler. Artık ben sadece çocuklarım için yaşıyorum!..” deyip noktalıyor.