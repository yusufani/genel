A drug developed by British scientists could offer fresh hope to women with ovarian cancer.

The deadliest gynaecological cancer, ovarian cancer affects almost 7,000 British women a year - and kills two thirds.

Dubbed 'the silent killer', it is symptomless in the early stages and so not usually diagnosed until it is too late.

No new drugs to combat the disease have been introduced for more than a decade.

The new treatment, which could be available in just five years, works in a similar way to the breast cancer 'wonder drug' Herceptin.

Doctors gave the drug, known only as CNTO328, to 18 ovarian cancer patients from north-east London and Essex in a trial that started in late 2007.

None had been expected to live more than a year because their cancer had returned despite several courses of chemotherapy.

Eight found their tumours stabilised or stopped growing - likely extending their life expectancy.

While the figure may seem small, it is much higher than the five to 20 per cent response rate seen with most experimental cancer drugs.

Lead researcher, Professor Iain McNeish, of Barts and the London School of Medicine and Dentistry, said: 'The hope with this group of patients was to slow down the progress of their ovarian cancer, improve their quality of life and possibly make them live longer.

'We have been quite successful in doing that.

'If this becomes a treatment, this is a whole new approach to ovarian cancer.'

CNTO328, developed in conjunction with a Dutch biotech firm now owned by Johnson & Johnson, is an antibody that zeroes in on a compound the cancer needs to grow and spread.

Professor McNeish told the Guardian: 'The dream scenario is that a combination of existing chemotherapy drugs and this type of antibody will be a big breakthrough and open up a new avenue for the treatment of ovarian cancer.'

Annwen Jones, of charity Target Ovarian Cancer, said the research was at an early stage but showed promise.

She added: 'Women being treated for ovarian cancer could be forgiven for despair, particularly when they grow resistant to chemotherapy and there are no drugs that can get them over this hurdle.

'Research projects such as this are vital if we are to develop desperately needed new treatments.'
