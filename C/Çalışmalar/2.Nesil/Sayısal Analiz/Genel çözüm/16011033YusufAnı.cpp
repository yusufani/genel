#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <math.h>
float fonksiyon(float [],int,float);
float turev(float[],int,float);
int isaret_degisimi(float,float);
void fonksiyon_goster(float [],int);
int main(){
	system("title Sayisal Analiz Araci");
	const char *const kirmizi = "\033[0;40;31m";//40 arkaplan rengi yazilan yerdeki 30 lar ise renkler
	const char *const yesil= "\033[0;4;32m";
	const char *const turuncu = "\033[0;40;33m";
	const char *const mavi = "\033[0;40;34m";
	const char *const mor = "\033[0;40;35m";
	const char *const magenta = "\033[0;40;36m";
	const char *const beyaz = "\033[0;40;37m";
  	const char *const normal = "\033[0m";
	int secim,t,x,i,j,z,satir,sutun,n;
	int derece,flag;
	float katsayi[10];
	float a,fa=0.0,b,c,fb=0.0,p=1.0,epsilon,fcold,fc,delta,d,h;
	printf("%s***Sayisal Analiz Araci'na hosgeldiniz***%s\n",yesil,normal);
	printf("%s<1>%sGrafik Yontemi\n%s<2>%sYariya Bolme Yontemi \n%s<3>%sRegula Falsi Yontemi\n%s<4>%sNewton-Raphson Yontemi\n%s<5>%sMatrisin inversini(tersinin) alma\n%s<6>%sGauss Eliminasyon veya Gauss Jordan Yontemi\n%s<7>%sTrapez Yontemi\n%s<8>%sSimpson Yontemi\n%s<9>%sNumerik Turev Alma\n",kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi);
	printf("%sLutfen istediginiz yontemin numarasini tuslayiniz:%s",mor,normal);
	secim=getch();
	int menu=1;
	while(menu=1){
	switch (secim){
		case 77:{	
			system("CLS");
			printf("%s***Sayisal Analiz Araci'na hosgeldiniz***%s\n",yesil,normal);
			printf("%s<1>%sGrafik Yontemi\n%s<2>%sYariya Bolme Yontemi \n%s<3>%sRegula Falsi Yontemi\n%s<4>%sNewton-Raphson Yontemi\n%s<5>%sMatrisin inversini(tersinin) alma\n%s<6>%sGauss Eliminasyon veya Gauss Jordan Yontemi\n%s<7>%sTrapez Yontemi\n%s<8>%sSimpson Yontemi\n%s<9>%sNumerik Turev Alma\n",kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi,kirmizi,mavi);
			printf("%sLutfen istediginiz yontemin numarasini tuslayiniz:%s",mor,normal);
			secim=getch();
			break;
		}
		case 49:{
			system("CLS");
			printf("%s-Grafik Yontemi-%s\n",yesil,normal);
			printf("%sLutfen fonksiyonun %sderecesini%s giriniz:",turuncu,kirmizi,turuncu);
			scanf("%d",&derece);
			for(i=0;i<=derece;i++){
				printf("Lutfen %sx^(%d)%s'li terimin katsayisini giriniz:",kirmizi,derece-i,turuncu,i+1);
				scanf("%f",&katsayi[i]);
			}
			fonksiyon_goster(katsayi,derece);
			printf("Lutfen bir %sbaslangic degeri%s giriniz:",kirmizi,turuncu);
			scanf("%f",&a);
			printf("Lutfen bir %sdelta(2 kok arasindaki artis miktari)%s degeri giriniz:",kirmizi,turuncu);
			scanf("%f",&delta);
			printf("Lutfen bir %sepsilon%s degeri giriniz:",kirmizi,turuncu);
			scanf("%f",&epsilon);
			printf("%s",normal);
			c=a+delta;
			fa=fonksiyon(katsayi,derece,a);
			fc=fonksiyon(katsayi,derece,c);
			while(fabs(fa-fc)>epsilon){
				printf("%sx=%f,%sx+delta=%f,%sf(x)=%f,%sf(x+delta)=%f%s\n%s",mavi,a,mor,c,mavi,fa,mor,fc,normal);
				
				if(isaret_degisimi(fa,fc)==1){
					delta/=2;
					c=a+delta;
				}
				else {
					a=c;
					c=c+delta;
				}
				fa=fonksiyon(katsayi,derece,a);
				fc=fonksiyon(katsayi,derece,c);
			}
			printf("%sKokumuz:%f",yesil,a);
			printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Grafik Yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
			secim=getch();
			if((secim==109) || (secim==77)) secim=77;
			else secim=49;		
			break;
		}
		case 50:{
			system("CLS");
			printf("%s-Yariya Bolme Yontemi-\n",yesil);
			printf("%sLutfen fonksiyonun %sderecesini%s giriniz:",turuncu,kirmizi,turuncu);
			scanf("%d",&derece);
			for(i=0;i<=derece;i++){
				printf("Lutfen %sx^(%d)%s'li terimin katsayisini giriniz:",kirmizi,derece-i,turuncu,i+1);
				scanf("%f",&katsayi[i]);
			}
			fonksiyon_goster(katsayi,derece);
			printf("Lutfen bir %sa(ilk) degeri %sgiriniz:",kirmizi,turuncu);
			scanf("%f",&a);
			printf("Lutfen bir %sb(son) degeri %sgiriniz:",kirmizi,turuncu);
			scanf("%f",&b);
			printf("Lutfen bir %sepsilon%s degeri giriniz:",kirmizi,turuncu);
			scanf("%f",&epsilon);
			fa=fonksiyon(katsayi,derece,a);
			fb=fonksiyon(katsayi,derece,b);
			printf("Ilk degerler icin %sF(a)=%f%s F(b)=%f%s\n",mavi,fa,mor,fb,turuncu);
			if(fa*fb<0.0000){
				c=(a+b)/2;
				fc=fonksiyon(katsayi,derece,c);
				if((fc==0) || (fabs(fc)<=epsilon)) {	
				printf("%sKokumuz:%f",yesil,c);
				}
				else{
					fcold=0;
					flag=0;
					while((fabs(fc))>epsilon){//Konu anlat�m kitab�ndaki de�ere g�re degil!!!!		
						printf("\n %sc=%f f(c)=%f    %sb=%f f(b)=%f    %sa=%f f(a)=%f\n",kirmizi,c,fc,mavi,fb,b,mor,fa,a);
						if((fa*fc)<0){
							b=c;
							fb=fonksiyon(katsayi,derece,b);
						}
						if((fb*fc)<0){
							a=c;
							fa=fonksiyon(katsayi,derece,a);
						}
						c=(a+b)/2;
						fcold=fc;
						fc=fonksiyon(katsayi,derece,c);
					}
					printf("%sKokumuz:%f",yesil,c);
				}
			}
			else{
				if(fa*fb>0.00000) printf("\n%sKok yariya bolme metodu ile bulunamaz.Koklerin isaretleri aynidir.%s",yesil,turuncu);
				if((fa==0.00000) && (fb==0.00000)){
					printf("\n%sa ve b cakisik kokdur ve degeri %f'dir%s",yesil,a,turuncu);
				}
				else{				
					if(fa==0.00000) printf("\n%sa koktur ve degeri:%f'dir",yesil,a,turuncu);
					if(fb==0.00000) printf("\n%sb koktur ve degeri:%f'dir",yesil,b,turuncu);
				}
			}
				printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Yariya bolme yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
		    	else secim=50;
				break;
			}
		case 51:{
				system("CLS");
				printf("%s-Regula Falsi Yontemi-\n",yesil);
				printf("%sLutfen fonksiyonun %sderecesini%s giriniz:",turuncu,kirmizi,turuncu);
				scanf("%d",&derece);
				for(i=0;i<=derece;i++){
					printf("Lutfen %sx^(%d)%s'li terimin katsayisini giriniz:",kirmizi,derece-i,turuncu,i+1);
					scanf("%f",&katsayi[i]);
				}
				fonksiyon_goster(katsayi,derece);
				printf("Lutfen bir %sa(ilk) degeri %sgiriniz:",kirmizi,turuncu);
				scanf("%f",&a);
				printf("Lutfen bir %sb(son) degeri %sgiriniz:",kirmizi,turuncu);
				scanf("%f",&b);
				printf("Lutfen bir %sepsilon%s degeri giriniz:",kirmizi,turuncu);
				scanf("%f",&epsilon);
				fa=fonksiyon(katsayi,derece,a);
				fb=fonksiyon(katsayi,derece,b);
				printf("Ilk degerler icin %sF(a)=%f%s F(b)=%f%s\n",mavi,fa,mor,fb,turuncu);
				if(fa*fb<0.0000){
					float c,fc;
					c=((b*fa)-(a*fb))/(fa-fb);
					fc=fonksiyon(katsayi,derece,c);
					if((fc==0) || (fabs(fc)<=epsilon)) {
						printf("%sKokumuz:%f",yesil,c);	
					}
					else{
						fcold=0;
						while(fabs(fc)>epsilon){//Konu anlat�m kitab�ndaki de�ere g�re degil!!!!				
							printf("\n %sc=%f f(c)=%f    %sb=%f f(b)=%f    %sa=%f f(a)=%f\n",kirmizi,c,fc,mavi,fb,b,mor,fa,a);
							if((fa*fc)<0){
								b=c;
								fb=fonksiyon(katsayi,derece,b);
							}
							if((fb*fc)<0){
								a=c;
								fa=fonksiyon(katsayi,derece,a);
							}
							c=((b*fa)-(a*fb))/(fa-fb);	
							fc=fonksiyon(katsayi,derece,c);
						}
					printf("%sKokumuz:%f",yesil,c);
					}
				}
				else{
					if(fa*fb>0.00000) printf("\n%sKok Regula Falsi metodu ile bulunamaz.Koklerin isaretleri aynidir.%s",yesil,turuncu);
					if((fa==0.00000) && (fb==0.00000)){
						printf("\n%sa ve b cakisik kokdur ve degeri %f'dir%s",yesil,a,turuncu);
					}
					else{				
						if(fa==0.00000) printf("\n%sa koktur ve degeri:%f'dir",yesil,a,turuncu);
						if(fb==0.00000) printf("\n%sb koktur ve degeri:%f'dir",yesil,b,turuncu);
					}
					}
				printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Regula Falsi Yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
			    else secim=51;
				break;
			}
		case 52:{
				system("CLS");
				printf("%s-Newton-Raphson Yontemi-\n",yesil);
				printf("%sLutfen fonksiyonun %sderecesini%s giriniz:",turuncu,kirmizi,turuncu);
				scanf("%d",&derece);
				for(i=0;i<=derece;i++){
					printf("Lutfen %sx^(%d)%s'li terimin katsayisini giriniz:",kirmizi,derece-i,turuncu,i+1);
					scanf("%f",&katsayi[i]);
				}
				fonksiyon_goster(katsayi,derece);
				printf("Lutfen bir %sx(baslangic) degeri%s giriniz:",kirmizi,turuncu);
				scanf("%f",&a);// x i a da tuttum ki degisken harcamay�m diyer!!!!!!!!!!!!!!!!! b de x1 olsun
				printf("Lutfen bir %sepsilon%s degeri giriniz:",kirmizi,turuncu);
				scanf("%f",&epsilon);
				b=a;
				do{
					printf("%sa=%f f(a)=%f   %sb=%f f(b)=%f\n",mavi,a,fa,mor,b,fb);	
					a=b;
					fa=fonksiyon(katsayi,derece,a);
					fb=turev(katsayi,derece,a);
					b=a-(fa/fb);
				}while(fabs(fa)>epsilon);
				printf("%sKokumuz:%f",yesil,a);
				printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Newton-Raphson Yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
		     	else secim=52;
				break;
			}
		case 53:{
				system("CLS");
				printf("%s-Matrisin inversini(tersini) alma-\n",yesil);
				int flag,k;
				float kat,tmp,det,ka;
				printf("%sLutfen matrisin %skenar%s sayisini giriniz: ",turuncu,kirmizi,turuncu);
				scanf("%d",&satir);
				float A[satir][satir],b[satir][satir],deta[satir][satir];
				for (i=0;i<satir;i++){
       				for(j=0;j<satir;j++){
            			printf("\n%sLutfen matrisin %s%d.satirdaki %d.sutundaki%s terimi giriniz\n %sA[%d][%d]=",turuncu,kirmizi,i+1,j+1,turuncu,kirmizi,i+1,j+1);
            			scanf("%f",&A[i][j]);
            			deta[i][j]=A[i][j];// Bu determinant al�rken matrisi bozmamak i�in
        			}
    			}
    			i=0;
    			flag=1;
    			while(i<satir && flag==1){
        			if(deta[i][i]==0){
            			k=i;
            			while(deta[k][i]==0 && k<satir){
                		k++;
            			}
            			if (k==satir){
                		flag=0;
            			}
						else{
                			for (j=0;j<satir;j++){
                    		tmp=deta[i][j];
                    		deta[i][j]=deta[k][j];
                    		deta[k][j]=tmp;
                    		tmp=A[i][j];
                    		A[i][j]=A[k][j];
                    		A[k][j]=tmp;
                			}
            			}
            			i++;
        			}
        			else i=satir+1;
        		}
        		for(i=0;i<satir;i++){
        			t=0;
        			flag=1;
        			while(t<satir && flag==1){// bu while tamamen herhangi bir yerde a11 a22 veya a33 geliyorsa b�l�mden sonsuz ��kmamas� i�in yer de�i�tirme yap�yor
        							if(A[t][t]==0){
            							k=t;
            							while(A[k][t]==0 && k<satir){
                						k++;
            							}
            							if (k==satir){
                						flag=0;
            							}
										else{
                							for (j=0;j<satir;j++){
                    							tmp=b[t][j];
                    							b[t][j]=b[k][j];
                    							b[k][j]=tmp;
                    							tmp=A[t][j];
                    							A[t][j]=A[k][j];
                    							A[k][j]=tmp;
                							}
            							}	
            							t++;
        							}
        							else t=satir+1;
        						}
        			d=deta[i][i];
        			for(x=i+1;x<satir;x++){
        				j=i;
        				kat=deta[x][j]/d;
        				for(j=0;j<3;j++){
        					deta[x][j]=deta[x][j]-(kat*deta[i][j]);
						}
					}
				}// determinant matirisini buraya kadar olu�turduk.
				for(i=0;i<satir;i++){
				
					for(j=0;j<satir;j++){
						if(i==j) b[i][j]=1;
						else b[i][j]=0;
					}
				}
				for(i=0;i<satir;i++){
						flag=1;
						t=i;
								while(t<satir && flag==1){
        							if(A[t][t]==0){
            							k=t;
            							while(A[k][t]==0 && k<satir){
                						k++;
            							}
            							if (k==satir){
                						flag=0;
            							}
										else{
                							for (j=0;j<satir;j++){
                    							tmp=b[t][j];
                    							b[t][j]=b[k][j];
                    							b[k][j]=tmp;
                    							tmp=A[t][j];
                    							A[t][j]=A[k][j];
                    							A[k][j]=tmp;
                							}
            							}	
            							t++;
        							}
        							else t=satir+1;
        						}			
					d=A[i][i];
					for(j=0;j<satir;j++){
						A[i][j]=A[i][j]/d;
						b[i][j]=b[i][j]/d;
					}
				 if(i!=satir-1){
					
					for(x=i+1;x<satir;x++){
						ka=A[x][i]/A[i][i];
						for(j=0;j<satir;j++){
							A[x][j]=A[x][j]-(ka*A[i][j]);
							b[x][j]=b[x][j]-(ka*b[i][j]);
						}
					}
				}				
				}
					for(i=satir-1;i>=0;i--){
						for(int x=satir-1;x>=0;x--){
						if(x!=i){
							ka=A[x][i]/A[i][i];
							for(j=0;j<satir;j++){
								if(i!=x){
									A[x][j]=A[x][j]-(A[i][j]*ka);
									b[x][j]=b[x][j]-(b[i][j]*ka);
								}
			  				}	
						}
					}
				}
						// buraya kadartersini ald�k
				det=1;
				for(i=0;i<satir;i++){
					det=deta[i][i]*det;
				}
				if(det==0) printf("%sMatrisin determinanti 0 dir.Bu yuzden tersi yoktur.",yesil);
				else printf("%sGirdiginiz matrisin determinanti =%f\n",yesil,det);
				if (det!=0){
					for (i=0;i<satir;i++){
        				for(j=0;j<satir;j++){
            				printf("%sB[%d][%d] : %s%f  %s",kirmizi,i+1,j+1,yesil,b[i][j],normal);
        				}
        				printf("\n");
    				}
    			}
				printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Matrisin inversini(tersini) alma yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
				secim=getch();
				if((secim==109) || (secim==77)) secim=77;
			    else secim=53;
				break;
			}
		case 54 :{
			system("CLS");
			printf("%sGauss Eliminasyon veya Gauss Jordan Yontemi\n",yesil);
			printf("%sKeyfiniz hangisini secmek ister?\n%s<1>%sGauss Eliminasyon\n%s<2>%sGauss Jordan\n%s",turuncu,kirmizi,yesil,kirmizi,yesil,normal);
			i=getch();
			if(i==49){
				system("CLS");
				printf("%s-Gauss Eliminasyon-\n",yesil);
				printf("%sLutfen matrisin %ssatir%s sayisini giriniz: ",turuncu,kirmizi,turuncu);
				scanf("%d",&satir);
				printf("%sLutfen matrisin %ssutun%s sayisini giriniz: ",turuncu,kirmizi,turuncu);
				scanf("%d",&sutun);
				float matris[satir][sutun],sonuc[satir];
				for (i=0;i<satir;i++){
       				for(j=0;j<sutun;j++){
            			printf("\nLutfen matrisin %s%d.satirdaki %d.sutundaki%s terimi giriniz\n %sA[%d][%d]=",kirmizi,i+1,j+1,turuncu,kirmizi,i+1,j+1);
            			scanf("%f",&matris[i][j]);
        			}
    			}
    			for(i=0;i<satir;i++){
    				printf("%sLutfen sonuclar matrisinin %s%d%s. elemanini giriniz:",turuncu,kirmizi,i+1,turuncu);
    				scanf("%f",&sonuc[i]);
				}
				for (i=0;i<satir;i++){
					d=matris[i][i];
					sonuc[i]/=d;
       				for(j=0;j<sutun;j++){
            			matris[i][j]/=d;
        			}
        			if(i!=satir-1){
        				for(x=i+1;x<sutun;x++){
        					a=matris[x][i];
        					for(j=0;j<satir;j++){
        						matris[x][j]=matris[x][j]/a;
        						matris[x][j]=matris[x][j]-matris[i][j];
        						matris[x][j]=matris[x][j]*a;
							}
							sonuc[x]/=a;
							sonuc[x]-=sonuc[i];
							sonuc[x]*=a;
						}
    				}
			}
		
			for(i=satir-1;i>=0;i--){
				for(j=sutun-1;j>i;j--){
					sonuc[i]=sonuc[i]-matris[i][j]*sonuc[j];
				}
			}
			for(i=0;i<satir;i++) printf("%s%d.kokumuz=%f\n",yesil,i+1,sonuc[i]);
		}
		else if(i==50){
				system("CLS");
				printf("%s-Gauss Jordan-\n",yesil);
				printf("%sLutfen matrisin %ssatir%s sayisini giriniz: ",turuncu,kirmizi,turuncu);
				scanf("%d",&satir);
				printf("%sLutfen matrisin %ssutun%s sayisini giriniz: ",turuncu,kirmizi,turuncu);
				scanf("%d",&sutun);
				float matris[satir][sutun],sonuc[satir];
				for (i=0;i<satir;i++){
       				for(j=0;j<sutun;j++){
            			printf("\nLutfen matrisin %s%d.satirdaki %d.sutundaki%s terimi giriniz\n %sA[%d][%d]=",kirmizi,i+1,j+1,turuncu,kirmizi,i+1,j+1);
            			scanf("%f",&matris[i][j]);
        			}
    			}
    			for(i=0;i<satir;i++){
    				printf("%sLutfen sonuclar matrisinin %s%d%s. elemanini giriniz:",turuncu,kirmizi,i+1,turuncu);
    				scanf("%f",&sonuc[i]);
				}
				for (i=0;i<satir;i++){
					d=matris[i][i];
					sonuc[i]/=d;
       				for(j=0;j<sutun;j++){
            			matris[i][j]/=d;
        			}
        			if(i!=satir-1){
        				for(x=i+1;x<sutun;x++){
        					a=matris[x][i];
        					for(j=0;j<satir;j++){
        						matris[x][j]=matris[x][j]/a;
        						matris[x][j]=matris[x][j]-matris[i][j];
        						matris[x][j]=matris[x][j]*a;
							}
							sonuc[x]/=a;
							sonuc[x]-=sonuc[i];
							sonuc[x]*=a;
						}
    				}
			}
			for(z=satir-1;z>=1;z--){//z bir sonraki sola gecmek i�in mesela en sa� sutunu s�f�rlad�m s�ra bir solundaki gibi
				for(i=satir-2+z-(satir-1);i>=0;i--){
					x=1;
					for(j=sutun-1+z-(satir-1);j>=0;j--){
						if(x==1) d=matris[i][j];
						matris[i][j]=matris[i][j]-(d*matris[z][j]);
						x=0;
					}
				sonuc[i]=sonuc[i]-(d*sonuc[z]);
				}
			}
				for(i=0;i<satir;i++) printf("%s%d.kokumuz=%f\n",yesil,i+1,sonuc[i]);
		}
	
		else printf("\n%s!1 veya 2 tuslayabilirsiniz!\n",kirmizi);
		printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Gauss Eliminasyon yontemini veya Gauss Jordan y�ntemini  tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
		secim=getch();
		if((secim==109) || (secim==77)) secim=77;
		else secim=54;	
		break;	
	}
	case 55:{
		system("CLS");
		printf("%s-Trapez Yontemi-\n",yesil);
		printf("%sLutfen fonksiyonun %sderecesini%s giriniz:",turuncu,kirmizi,turuncu);
		scanf("%d",&derece);
		for(i=0;i<=derece;i++){
			printf("Lutfen %sx^(%d)%s'li terimin katsayisini giriniz:",kirmizi,derece-i,turuncu,i+1);
			scanf("%f",&katsayi[i]);
		}
		fonksiyon_goster(katsayi,derece);
		printf("Lutfen bir %shassaslik(n) degeri%s giriniz:",kirmizi,turuncu);
		scanf("%d",&z);
		printf("Lutfen fonksiyon icin %sa(ilk)%s degeri giriniz:",kirmizi,turuncu);
		scanf("%f",&a);
		printf("Lutfen fonksiyon icin %sb(uc)%s degeri giriniz:(b)",kirmizi,turuncu);
		scanf("%f",&b);
		h=(b-a)/z;
		d=0;
		for(i=1;i<=z-1;i++){
			d=d+fonksiyon(katsayi,derece,a+i*h);
		}
		d*=2;
		d=h/2*((fonksiyon(katsayi,derece,a)+fonksiyon(katsayi,derece,b))+d);
		printf("%sIntegral Sonucu=:%f",yesil,d);
		printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Trapez yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
		secim=getch();
		if((secim==109) || (secim==77)) secim=77;
		else secim=55;	
		break;
	}
	case 56:{
		int dogruluk;
		system("CLS");
		printf("%s-Simpson Yontemi-\n",yesil);
		printf("%sLutfen fonksiyonun %sderecesini%s giriniz:",turuncu,kirmizi,turuncu);
		scanf("%d",&derece);
		for(i=0;i<=derece;i++){
			printf("Lutfen %sx^(%d)%s'li terimin katsayisini giriniz:",kirmizi,derece-i,turuncu,i+1);
			scanf("%f",&katsayi[i]);
		}
		fonksiyon_goster(katsayi,derece);
		printf("Lutfen bir %shassaslik(n) degeri%s giriniz:",kirmizi,turuncu);
		scanf("%d",&z);
		dogruluk=0;
		if(z%2==0) dogruluk=1;
		if(dogruluk==0) printf("\n N cift sayi olmak zorundadir.\n");
		while(dogruluk==1){
			printf("Lutfen fonksiyon icin %sa(ilk)%s degeri giriniz:",kirmizi,turuncu);
			scanf("%f",&a);
			printf("Lutfen fonksiyon icin %sb(uc)%s degeri giriniz:(b)",kirmizi,turuncu);
			scanf("%f",&b);
			h=(b-a)/z;
			d=0;//tek sayilarin toplam�
			c=0;//�ift sayili koklerin toplam�
			for(i=1;i<z;i++){
				if(i%2==1) d=d+fonksiyon(katsayi,derece,a+i*h);
				else c=c+fonksiyon(katsayi,derece,a+i*h);
			}
			d*=4;
			c*=2;
			d=(h/3)*(d+c+fonksiyon(katsayi,derece,a)+fonksiyon(katsayi,derece,b));
			printf("Integral Sonucu:%f",d);
			dogruluk=0;
			}
		printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Simpson yontemini tekrar kullanmak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
		secim=getch();
		if((secim==109) || (secim==77)) secim=77;
		else secim=56;	
		break;
	}
	case 57:{
		system("CLS");
		printf("%s-Numerik Turev-\n",yesil);
		printf("%sLutfen fonksiyonun %sderecesini%s giriniz:",turuncu,kirmizi,turuncu);
		scanf("%d",&derece);
		for(i=0;i<=derece;i++){
			printf("Lutfen %sx^(%d)%s'li terimin katsayisini giriniz:",kirmizi,derece-i,turuncu,i+1);
			scanf("%f",&katsayi[i]);
		}
		fonksiyon_goster(katsayi,derece);
		printf("%sLutfen %sx degeri%sni giriniz:",turuncu,kirmizi,turuncu);
		scanf("%f",&a);
		printf("Lutfen bir %sdelta(2 kok arasindaki artis miktari)%s degeri giriniz:",kirmizi,turuncu);
			scanf("%f",&delta);
		float t=fonksiyon(katsayi,derece,a+delta)-fonksiyon(katsayi,derece,a-delta);
		t=t/(2.000*delta);
		printf("%sSonuc= %f",yesil,t);
		printf("\n%sMenuye donmek icin %s'M' veya 'm'%s tuslayiniz .Numerik Turevi tekrar almak icin herhangi bir tusu tuslayiniz:%s",turuncu,kirmizi,turuncu,normal);
		secim=getch();
		if((secim==109) || (secim==77)) secim=77;
		else secim=57;	
		break;
	}
}
}
}
float fonksiyon(float katsayi[],int derece,float x){
	float p=1.0;
	float fx=0.0;
	for(int i=0;i<=derece;i++){
	fx=fx+p*katsayi[derece-i];			
	p=p*x;
	}
	return fx;
}
float turev(float katsayi[],int derece,float x){
	int tek=1;
	float p=0.0;
	float fx=0.0;
	for(int i=0;i<=derece;i++){
	fx=fx+p*i*katsayi[derece-i];		
	if(tek==1){
		tek=0;
		p=1.0;
		p/=x;
	}
	p=p*x;
	
	}
	return fx;
}
int isaret_degisimi(float a,float b){
	if((a<0) & (b>=0)) return 1;
	if((a>=0) & (b<0)) return 1;
	else return 0;
}
void fonksiyon_goster(float katsayi[],int derece){
	const char *const kirmizi = "\033[0;40;31m";
	const char *const turuncu = "\033[0;40;33m";
	const char *const mavi = "\033[0;40;34m";
	const char *const magenta = "\033[0;40;36m";
	int i;
	printf("%sGirdiginiz degerlere gore fonksiyon->",turuncu);
	for(i=derece;i>=0;i--){
			printf("%s(%.2f)%sx%s^(%s%d%s)",mavi,katsayi[derece-i],magenta,mavi,kirmizi,i,mavi);
			if(i!=0) printf("%s+",turuncu);
			
	}
	printf("\n");
}
