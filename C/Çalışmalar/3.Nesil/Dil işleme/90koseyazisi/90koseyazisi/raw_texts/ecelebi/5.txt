Karbonhidrat d���k� dedikleri diyet �r�nler, �i�manlat�yor 
ABD�nin T�ketici �rg�t� Consumers Union�un yapt��� ara�t�rmaya g�re, karbonhidrat miktar� d���r�lm�� light �r�nler, diyet yapanlar� zay�flataca��na, kilo almalar�na neden oluyor. 

Buna da d���k karbonhidrat i�eren bisk�vi, makarna, �ikolata, dondurma gibi light �r�nlerin y�ksek kalori i�ermesi yol a��yor.

CATHERINE Zeta Jones, Jennifer Anniston gibi �nl� sanat��larla g�ndeme gelen d���k karbonhidratl� diyetler, art�k cazibesini yitirdi. Amerika�n�n T�ketici �rg�t� Consumers Union�un yapt��� ara�t�rmaya g�re, bu diyetler i�in geli�tirilen ve T�rkiye�de de yayg�n olarak sat�lan d���k karbonhidratl� dondurma, makarna, �ikolata, bisk�vi gibi �r�nler, zay�flataca��na, aksine kilo ald�r�yor. 

Consumers Union�un yay�n organ� Consumer Reports Dergisi, �n�m�zdeki hafta payasaya ��kacak olan son say�s�n�n kapa��n�, d���k karbonhidratl� �r�nlere ay�rd�. �D���k karbonhidratl� �r�nlerin ger�e�i� ba�l���yla yay�nlanan ara�t�rmada, d���k karbonhidratl� bu �r�nlerin, y�ksek oranda kalori i�erdi�ine dikkat �ekiliyor. Bu da, zay�flamak i�in diyet yapanlar�n kilo almas�na neden oluyor.

YANILTICI

Ara�t�rmada, g�nde 40 grama kadar karbonhidrata izin veren Atkins gibi diyetlerin sak�ncalar�na da de�iniliyor. Ara�t�rmada, bu diyetler i�in �zel olarak �retilen karbonhidrat oran� d���r�lm�� �r�nlerden de �rneklere yer veriliyor. Ayr�ca, 100 gram�nda 3.2 gram karbonhidrat i�eren bir diyet �ikolatan�n 190 kalori i�erdi�ine dikkat �ekiliyor. Karbonhidrat� 8 grama indirilmi� bir makarnada, 197 kalori, 6 gram karbonhidrat i�eren bir dilim ekmekte ise 60 kalori bulunuyor. Ayr�ca, 70 gram�ndaki (bir porsiyon) karbonhidrat miktar� 3 grama indirilmi� dondurman�n kalorisi de 140�a ula��yor. Karbonhidrat miktar� 5 grama indirilmi� bu�dayl� diyet bisk�vilerin bir adedi 60 kalori i�eriyor. 

Bunun sonucunda g�nde 40 grama kadar karbonhidrata izin veren diyetlerde, bu �r�nler kullan�l�rsa, al�nan kalori miktar� 2 bin 500 ile 3 bin aras�nda de�i�iyor. Bu da, zay�flamak i�in yap�lan diyeti, bir anda �i�manlatma diyetine d�n��t�r�yor. 

Ara�t�rmaya g�re, 40 milyon Amerikal�, d���k karbonhidrat diyetlerini uygulayarak zay�flamaya �al���yor. Bunun y�zde 16�s� da, son 5 y�lda piyasaya s�r�len 930 farkl� d���k karbonhidrat i�eren �r�n� kullanarak diyet yap�yor. Bunun sonucunda, her y�l 6 milyon Amerikal�, �zay�flayay�m� derken, �i�manl�yor.

T�RK�YE�DE DURUM 

Arkada��m�z Ay�eg�l Akyarl��n�n yapt��� ara�t�rmaya g�re, bu durum T�rkiye�de de farkl� de�il. T�rkiye�deki b�y�k marketlerde 3 milyon 800 bin liradan sat�lan ve karbonhidrat miktar� 47 gramdan 12 grama d���r�lm�� olan diyet �ikolatalar, 555 kalori i�eriyor. Buna kar��n 975 bin liradan sat�lan diyet olmayan �ikolatalar ise 550 kalori i�eriyor. Ayr�ca, light diye 56 gram� 650 liradan sat�lan ve karbonhidrat oran� y�zde 50 azalt�lm�� �zeri �ikolata kapl� diyet bisk�vilerde ise 198 kalori bulunuyor. 2 milyon 650 bin liradan sat�lan ve 100 gram�ndaki karbonhidrat miktar� 47 gramdan 21 grama indirilmi� olan �ubuk krakerler de, 305 kalori i�eriyor. 
