D�rty�zelli trilyon! 

 
 

E�er futbolu ger�ekten seviyorsak, kendi kendimize soraca��m�z en �nemli sorulardan biri �u olmal�d�r:

"T�rkiye'de sa�l�kl� futbol oynan�yor mu?"

Bu sorunun yan�t�, eveleyip gevelemeden "HAYIR"d�r.

E�er T�rkiye'de sa�l�kl� bir futbol yap�s� olsa, en iyimiz F.Bah�e, �ampiyonlar Ligi'ndeki 3 ma��nda 13 gol yemezdi.

Yapt��� transferlerin r�zg�r�na kap�l�p "T�rkiye Ligi'nde �ampiyon olaca��z. Avrupa'da final oynayaca��z" diye yola ��kan Be�ikta�, bulundu�u yere d��mezdi.

Daha 4 sezon �nce UEFA Kupas�'n� T�rkiye'ye getiren G.Saray bug�n "annesinin ligine" hapsolmazd�!

En �nemli �rnek de �udur:

Japonya, Kore, �in gibi futbolun hen�z alfabesini heceleyen �lkeleri yenerek "D�nya 3,'s� olduk" diye futbolunun "Dev" oldu�unu sanan bir �lke, Letonya gibi s�radan bir Avrupa tak�m� kar��s�nda "P�RE" olmazd�!

�z�lerek s�yl�yoruz, y�llard�r ayn� �eyler oluyor, bir r�zg�r esiyor, r�zg�r� arkas�na alan havalan�yor, sonra r�zg�r ge�iyor, havadakiler "k�t" diye asl� olan yere �ak�l�yor.

Pekii bu g�r�nt�n�n "sa�l�kl� futbolla ne ili�kisi vard�r?" diyorsan�z, toplumun en k���k bireyi say�lan "aile" yap�s�na bakacaks�n�z...

Ailenin paras�, maddi g�c� varsa, ailenin �ocuklar� iyi beslenmekte, tahsil i�in d�nyan�n en �nemli e�itim kurulu�lar�na do�ru ufuk geni�letmektedir. Yoksa, �aresizlik dedi�imiz ak�nt�ya k�rek �ekebilenler "�ansl�" say�l�rken �ekemeyenler telef olup gitmektedir.

Uzatmayal�m; beslenme, e�itim, mutluluk, bo�anma, intihar, cinayet diye toplum ya�am�nda kendini g�steren g�r�nt�ler "varl�kla-yokluk" s�n�rlar� �evresinde toplanm��lard�r.

Bor� bata��
Futbola da bu a��dan bakt���n�zda T�rk futbolu "yokluk s�n�r�" etraf�nda kul�bele�mi�tir!

S�k� durun; 18 tak�mdan olu�an T�rkiye S�per Ligi'nin toplam borcu 450 trilyon (eski parayla) lirad�r.

Bu rakam ba�kanlar�na birebir ula��p a��zlar�ndan ald���m�z rakamlard�r ve bor� s�ralamas� ��yledir:

G.Saray: 169 trilyon 650 milyar
F.Bah�e: 165 trilyon 300 milyar
Be�ikta�: 56 trilyon 
Samsun: 14 trilyon 500 milyar
G.Antep: 10 trilyon 150 milyar
Malatya: 10 trilyon 150 milyar
Trabzon: 10 trilyon 
Rize: 7 trilyon 250 milyar 
Denizli: 6.5 trilyon 

Geri kalan �stanbulspor'dan bor� s�ralamas�nda 15. olan Sakaryaspor'a kadar olan b�lgedeki 6 kul�b�n bor� g�stergeleri 3 trilyondan 1 trilyona do�ru inerken, sadece 3 kul�b�m�z Ankaraspor, Kayseri ve G.Birli�i bor�lar�n�n olmad���n� s�ylemi�lerdir.

En az, en �ok!
��in ilgin� yan�, �rne�in 1.5 trilyon borcu olan Sebatspor, 169 trilyonla en tepede yer alan G.Saray'dan �ok daha yoksuldur. ��nk�, G.Saray 25 bin seyirciye oynayabilirken, Sebat'in trib�n kapasitesi 2 bin 907 ki�iliktir. Her kul�p F.Bah�e ile oynarken, "has�lat rekoru k�raca��z" diye sevinirken, Sebat bu sezon F.Bah�e ile oynad��� ma�ta bin 100 bilet satarak zarar etmi�tir. Cumhuriyetle ya��t 81 y�ll�k kul�p olan Ak�aabat Sebatspor, 17 ma�ta 86
milyarl�k zarar� bilan�osuna yazm��t�r. B�y�klerin mamullerini satma �ans�na kar��l�k, Sebat'�n b�yle bir gelir �ans� da yoktur. Dahas� Sebat'�n yay�n hissesinde pay�na d��en, b�y�klerin di� kovuklar�na s��abilecek k���k bir par�ad�r.

Sebat'� asl�nda �rnek olarak verdik; bug�n "Ligin en sorunsuz kul�b� biziz" diyen G.Antep'in bile borcu 10 trilyona ��km��t�r ki, Celal Do�an'�n y�neticilik kariyeri bu kul�b�m�z� yere serilmekten kurtarmaktad�r.

Asl�na bakarsan�z, birinin derdi, hepsinin derdidir. Ama Sebat gibi Samsunspor'un i�inde bulundu�u durum da Faruk Nafizin "Han duvarlar�" �iirinin sanki k���k bir de�i�imidir:

"... Bir deva bulmak i�in ba�r�ndaki yaraya

Toplanm�� garipler �imdi futbol sahalar�m�za..."

Koskoca Samsun Belediyesi'nin "elimi yak�yor" diye att��� Samsunspor ate�ini tekrar eline alan �smail Uyan�k ba�kan ill�zyonist Abra Kadabra gibi bu kul�b� ya�atmaya �al��maktad�r.

Hi� kimse bu "bor� bata��na" bak�p bug�n g�rev yapan ba�kanlar� su�lamas�n. Bug�n bu ligde Ekrem Cengiz, Veli Sezgin, �smail Uyan�k, Zafer Katranc�, Celal Do�an, �lhan Cavcav, Ahmet �an, Hikmet Tannverdi, Cemal Ayd�n, Atay Aktu� gibi "k�t imkanl�" fedak�r ba�kanlar olmasa, kepengi �ekilecek futbolun �zerindeki �u yaz�y� herkes okuyacakt�r: "�FLAS..."

��nk� bor� bug�n�n borcu de�ildir. D�nden bug�ne bir kartopu gibi yuvarland�k�a b�y�yen bor�, bug�nk� y�netimlerin kuca��na d��m��t�r! Onun i�in de T�rk futbolunun d�n� ile bug�n� aras�nda fazla da �nemsenecek fark yoktur. Asl�na bakarsan�z, T�rk futbolunun bu tablo ile AB i�inde de yeri yoktur.

Olamaz da... 

 
