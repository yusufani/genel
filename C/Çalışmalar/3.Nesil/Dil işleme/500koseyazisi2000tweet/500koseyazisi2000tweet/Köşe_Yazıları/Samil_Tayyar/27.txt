﻿Ve komutan kavgayı seçti

21.02.2011
Şamil Tayyar

Genelkurmay Başkanı Orgeneral Işık Koşaner’in bazı komutanları koluna takarak Hasdal askeri cezaevinde Balyoz sanıklarını ziyarete gitmesi gösteriyor ki, ayarı bozulan sadece CHP ve ABD değilmiş.

Mahalle baskısına maruz kalan ve hatta istifa seçeneği masasında bulunan Işık Paşa’nın izlediği bu yöntem, “hükümetle ya kavga et ya istifa et” diyen genç subaylara yönelik “kavgayı seçtim” mesajı gibi geldi bana.

Çünkü: Hasdal ziyareti bir meydan okumadır, sözsüz muhtıradır. Ayrıca, adil yargılamayı etkileme suçu işlenmiştir. Nüfuz kullanma gücü bulunan komutanların, tutuklu paşaları ziyareti, “insani” ambalajına sarılarak pazarlanamaz.

Aynı şekilde Başbakan da Cumhurbaşkanı da Hasdal’a veya yargılaması süren başka tutukluların kaldığı cezaevine gitseydi aynı ağır eleştirileri onlara da yöneltirdik. Kaldı ki, Balyoz’da ilk tutuklamaların başladığı günlerde Cumhurbaşkanı, Başbakan ve Genelkurmay Başkanı’nın Çankaya’da bir araya gelmesine tepki göstermiş birisiyim.

Buna rağmen itiraz edenlere, hatta komutanlara, 28 Şubat sürecinde Sincan Belediye Başkanı Bekir Yıldız’ın cezaevinde ziyaretine giden Adalet Bakanı Şevket Kazan’a yönelik ağır eleştirileri hatırlatmak isterim. Bu ziyaret, 28 Şubat’ın gerekçeleri arasında gösterilmişti.

Demokratik evrilme

Hava Kuvvetleri Komutanı Orgeneral Hasan Aksay ve Jandarma Genel Komutanı Orgeneral Necdet Özel’in bu ziyarete eşlik etmemesi de komutanlar arasında görüş ayrılıklarının bulunduğunu gösteriyor.

Daha doğrusu, yürekleri yansa bile hala hukuka saygı duyan komutanların varlığını göstermesi bakımından dikkat çekici bir gelişmedir. Aynı zamanda TSK için bir kırılma anıdır, demokratik bir evrilmedir.

Keşke hukuka saygıyı diğer komutanlar da gösterseydi. Tabi, sorunun bir de başka bir boyutu var.

Ahmet Altan’ın dün köşesinde yer verdiği gibi, generaller “yolsuzluk” veya “zimmete para geçirmek” suçundan sanık olsalardı, komutanlar, “insani” diyerek ziyaretlerine gider miydi? Misal, kuvvet komutanı emekli oramiral İlhami Erdil...

Yüzüne bakan olmadı. Çünkü bu suç, yüz kızartıcı bir suçtur. Sorun, darbe suçunun“yüz kızartıcı” sayılmamasıdır. Anıtkabir’de Atatürk’ün karşısına çıkabilecek yüzü kendilerinde bulabiliyorlar.

Ordu çökmez

Efendim, Türkiye’nin terörle mücadele ederken bu komutanların içeriye alınması komuta kademesinde zafiyet oluşturabilir!

Neden oluştursun?

Terörle mücadelede en etkin birimlerden biri, hava kuvvetleridir. Bu birimden Balyoz’da tutuklu tek sanık yoktur. Jandarma’dan hayli isim var içeride, onların yerlerine de atamalar ivedi şekilde yapıldı. Sanıkların neredeyse tamamına yakını, terörle mücadelede aktif görevlerde değil.

Ayrıca, aktif görevde olsa ne olur? Gece saldırıdan önce görüntülenen 50-60 civarındaki teröristi “çoban sanan” General, Hasdal’da değil de Hakkari’de olsa ne yazardı? O saldırıda kaç Mehmetçiğin şehit olduğunu bir kez daha düşünün.

Şunu da bilin; bazı medya organlarının yazdığı gibi, ordudaki general sayısı 301 değildir. 364 general kadrosu var ve bunların 349’u dolu. Bu generallerin 30’u tutukludur. 319 general aktiftir, görevinin başındadır.

Velev ki bu sayı da fazla olsun, ne fark eder? Türk Silahlı Kuvvetleri bu kadar aciz mi?

Geçmişe bakın...

27 Mayıs askeri darbesinden sonra cunta, tam 235 general ve 7 bin 200 subayı emekliye sevk etti, ordu çökmedi...

Yetmedi, Genelkurmay Başkanı Orgeneral Rüştü Erdelhun, Hava Kuvvetleri Komutanı Tekin Arıburun ve Deniz Kuvvetleri Komutanı Sadık Altuncan’ın rütbelerini sökerek Yassıada’ya gönderdi.

Dahası var; Kore gazisi Tuğgeneral Tahsin Yazıcı da DP’nin 1950’de işbaşına gelir gelmez atadığı Genelkurmay Başkanı emekli Orgeneral Nuri Yamut da Yassıada ekibine eklendi.

Ordu çökmedi...

Bir küçük hatırlatma daha; 1977 yılı Ağustos şurasında, CHP’nin iktidara gelmesini önlemek için Alparslan Türkeş’le birlikte darbe planı hazırladığı iddia edilen Kara Kuvvetleri Komutanı Orgeneral Namık Kemal Ersun emekliye sevk edildi.

Sadece o mu? Bu operasyonda 850 subay ordudan atıldı. Tamamı milliyetçi/ülkücü isimlerdi. Ordu yine çökmedi, aksine, “Türk Silahlı Kuvvetleri macera peşinde koşanlara asla iltifat etmeyecektir” diyen Genelkurmay Başkanı Orgeneral Semih Sancar’ın bu sözlerine rağmen, 3 yıl sonra darbe yapacak kadar güçlendi.

Peş peşe 12 Eylül oldu, 28 Şubat oldu, Sarıkız oldu, Balyoz oldu, oldu da oldu...

Sözde kalsa da geçmişte Genelkurmay Başkanları meydanlara çıkıp “macera peşinde koşanlara iltifat etmeyeceğiz” diyordu, şimdi maceracıların peşinde koşuyorlar.

Çok yazık...