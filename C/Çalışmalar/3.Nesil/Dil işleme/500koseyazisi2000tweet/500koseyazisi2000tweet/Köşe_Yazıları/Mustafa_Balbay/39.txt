﻿AKP sonrasını düşünebilmek!
Dünkü yazıya son noktayı koyarken, AKP’nin dayatmalarına karşı ülkeyi yönetecek yeni ruhun ne olacağını konuşmak, seçenek üretmek gerekir, demiştik. Bıraktığımız yerden devam edelim... 
AKP Türkiye’yi yönetemez hale geldiğini kabul ediyor ve kendisini yenileyerek yola devam edeceği mesajını vermeye çalışıyor. 
AKP’nin 5. kongesinin fotoğrafı budur. AKP’nin tam ve yarı resmi yayın organlarının kongre sonrası attıkları başlıklardan bazılarını paylaşalım: 
İlk günkü aşkla... 
Mesaj alındı, öze dönüldü... 
2002 ruhuyla.. 
AKP, 7 Haziran’a gider gibi 1 Kasım’a giderse yenileceğini gördü. Kendini yenilemiş gibi yapma ve 2002’deki heyecanı yaratma derdinde. Bu başlıklar bunun kabulünü gösteriyor. 
Yenilemede görüntü bu, ama gerçek şu: 
AKP yelpazesi giderek daralıyor. 
2002 seçimlerine giderken AKP listelerinde birbirini vura vura eriyen ANAP ve DYP kökenliler vardı, geçmişi solda olanlar vardı, ağırlıklı olarak Milli Görüş vardı, MHP’de siyaset yapmış olanlar vardı... Vardı da vardı... 
AKP zamanla bunların çoğuyla yolunu ayırdı. Arada kendisine seçenek olma tehlikesi olan lider adaylarını, partilerini kapattırıp listesine kattı. Son kurultayda partinin kurucu kökleriyle Gül yanlısı diye bilinenler tasfiye edildi.

***

AKP, 1 Kasım sonrasında da Türkiye’yi yönetecek parti olduğu havasını şimdiden yerleştirmeye ve bayramdan sonra yoğunlaşacak seçim havasına önde girmeye hazırlanıyor. Ancak Boştepe’ye giden anketler bu havayı pekiştirecek bir içerik taşınımıyor. Son günlerde Ankara’da metrekareye 5 anket sonucu düşüyor. Bunların hemen hiçbiri AKP’yi 40’ın üzerinde göstermiyor. Kimi AKP’liler, “oyumuz yüzde 45’i geçti, hatta yüzde 46” diyorlar ama biraz kelime oyunu ile karşı karşıya görünüyorlar. Yani, “oylar yüzde 40 altı” demek yerine “oylar yüzde 46”yı yeğliyorlar! 
AKP 12 yıldır ülkede en çok neyi tahrip ettiğini anlamış olmalı ki, Siyasi Erdem ve Etik Kurulu oluşturma kararı almış. Bir de insan hakları ve çevreye ilişkin iki genel başkan yardımcılığı oluşturulmuş. 
Kongre öncesi tartışılan yeni yönetim listesi, kongre sonrasında da gündemde. Erdoğan’ın avukatı, damadı, danışmanları başta olmak üzere tüm çevresi partinin yönetiminde. Öyle anlaşılıyor ki, Türkiye’yi değerli yalnızlığa iten Davutoğlu parti yönetiminde de değerli bir yalnızlık içinde.

***

Bütün bunlar bir yana... 
Seçimlerin seçimi 1 Kasım, içinde çözümleri de barındıran yeni bir tartışmalı ortamı getirebilir. Bu durumda ortaya 3 seçenek çıkabilir. 
1- AKP, medyasından devlet gücüne kadar her şeyi kullanıp kendini yenilediğine dair bir hava verir... Seçimlerden birinci parti çıkıp ötesini zorlar, gücü bırakmaz... 
2- Yeni bir iktidar hedefli toplama yapı çıkabilir. AKP içinden kopanları da kapsayan yeni bir merkez hareketi üretilebilir. 
3- CHP güçlü bir çıkış yapıp çekim merkezi olabilir. 
Yazı aramızda gönlümüzden geçen üçüncüsü ama bunu salt gönülden geçen bir istem olarak değil, görünen bir seçenek olarak paylaşıyoruz. 
AKP dönemi sona eriyor. AKP’liler de bunun farkında. O yüzden 2002 ruhunu arıyorlar. Ne olursa olsun Türkiye’nin siyasal birikimi yeni seçenek üretebilir. Önce AKP’nin, “benden başka seçenek yok, ona göre, ben yoksam tufan” dayatmasından korkmamak ve bunun esiri olmamak gerekiyor.