Gidelim mi, kalal�m m�  
     

  
B�R yandan Ba�bakan'�n rahats�zl���, se�imin ne zaman yap�laca�� tart��malar�yla siyaset �s�n�yor.

'Gen� Ar�' taraf�ndan ABD'deki Cumhuriyet�i Parti'nin Uluslararas� Cumhuriyet�i Enstit�s� (IRI) ile birlikte d�zenlenen 'Kat�l ve Gelece�ini Yarat-Gen�Net' konferans� Dalan'�n Yeditepe �niversitesi Kay��da�� kampusunda yap�l�yor. Hemen b�t�n illerdeki �niversitelerden getirilen 1000'den fazla ��renci Kemal Dervi� ve M. Ali Bayar'� dinliyor.

Yeditepe'nin az alt�nda Ankara yolunda MHP bayrakl� y�zlerce ara� konvoyu; Genel Ba�kan Devlet Bah�eli'yi kar��lamak �zere bekliyor. Bah�eli bir s�re sonra Abdi �pek�i Salonu'nda 'MHP'nin �stanbul Bulu�mas�' toplant�s�nda konu�acak.

Tansu �iller Swissotel'de '�stanbul'a Yeni Y�netim Modeli' a��kl�yor.

Mesut Y�lmaz, Konya'da a��l�� yap�yor.

DERV��'E �LG�

Salonda 1200'e yak�n ��renci var; Dervi�'i ilgiyle izliyorlar. Gen� Ar�lar, Dervi�'e de ti��rt giydiriyor.

Dervi�, 'Yeni T�rkiye'nin vizyonunu anlat�yor.

Ulus devlet mant���ndan sosyal d�zeyde liberal sentez tart��malar�na ge�ilmesi gerekti�ini anlat�rken ��Bu bizi nereye g�t�recek hen�z belli de�il. Ancak eskiyi d���nmeyin, yeni yap� �ok g��l��� diyor.

Bu anlay���n devletin baz� kurumlar�na ve insanlar�n davran��lar�na tam olarak yans�mad���n�n alt�n� �iziyor. Art�k kurumlar�n siyasi bask�lardan korunmas�; siyasetle ekonominin ayr��mas� gerekti�ini vurguluyor.

Dervi�'i 1200'e yak�n ki�i dinledi. ��leden sonra ��renciler �al��ma gruplar�na ayr�l�nca Bayar'� dinleyenlerin say�s� bunun ��te biri kadar oldu.

Dervi� uzun konu�mas�nda iki kez alk��land�.

NE D�YORLAR

Konu�malardan sonra ��rencilerle konu�tuk...

Hepsi de geleceklerinden endi�eliydi.

��Mezun olduktan sonra ne yapaca��z?��

��Yurtd���na m� gidelim, T�rkiye'de mi kalal�m? Kal�rsak i� bulabilecek miyiz?��

Ortak s�ylem b�yle ba�l�yor.

T�rkiye'nin her y�resinden getirilen ��renciler umutsuz; bir 'iyilik mele�i' bekliyorlar. Yitirilen inan�lar�n�n geri gelmesini istiyorlar; ya�am sevinci ar�yorlar.

O nedenle dikkatlice dinliyorlar konu�mac�lar�.

Hepsi de bilin�li, gazete-kitap okuyorlar, siyaset �zerine g�zel s�zler ediyorlar.

Duygu dengeleri bozulmu�; kafalar� gelip-geliyor... Hedef belirleyemiyorlar. D��a d�n�k heyecanlar� yitirilmi�.

Baz�lar� ise suskun... Aralar�nda 'sosyal demokrat' olan da var; kendisini 'demokrat�m' diye tan�mlayanlar da...

Hepsi bir ��k�� yolu ar�yor.

Sohbetlerde bize anlatt�klar� ��yle:

�LKAL TEZGEL (Antalya): M.Ali Bey'i a��k ve samimi buldum; a��k�as� etkilendim. Kemal Dervi� biraz siyaset�i gibi konu�tu.

KAD�R EFE (Eski�ehir): Dillendirme bak�m�ndan fark g�rmedim aralar�nda. Dervi� ekonomiyle ilintili oldu�undan ele�tiri ��kartmad�. Bayar ise her yerde duydu�umuz �ik�yetleri anlatt�. Eski siyaset�ilerden farkl� de�il diye d���nd�m.

EL�F USLU (Ankara): Dervi� akademisyen; siyasetten te�et ge�ti. Bayar �ok rahatt�; yeni siyasete giren birisi i�in bu ba�ar� say�lmal�d�r.

TUNCAY TREN (Gebze): Gen�likte b�y�k bir enerji var; �lkemiz bizi kullanacak m�? Yoksa yabanc� �lkelere gidi�imizi seyredecek mi? As�l sorun bu... Fizik okudum, kendimi bu dalda ba�ar�l� buluyorum. �� ar�yorum, bir hamal�n �� g�nl�k paras� kadar �cret �neriyorlar. S�yleyin, T�rkiye'de kalmal� m�y�m, gitmeli miyim?

SAL�H YILDIRIM (Bolu): Kemal Dervi�'in s�ylediklerinin altyap�s� yok; hep bankac�l�ktan s�z etti. Bayar, 18 May�s'tan sonra konu�acakm��.

A. AKT�F (�stanbul) Kendimi g�vensiz hissediyorum. �zg�venimi yitirdim, ben de ne olaca��m� bilmiyorum? Konu�malar bana bir �ey vermedi.

RON� ATALAY (Antalya): Kemal Dervi� gelinceye kadar siyasete g�venmiyordum. �b�rlerinin amac� koltuk, Dervi�'in ise ekonomiye, �lkesine bir �eyler katmak.

SEVG� K�LL� (Bilecik): Dervi�'in konu�mas�n� fazla iyimser buldum. Krizde THY k�r ediyor, dedi. Halbuki THY elindeki u�aklar� sat�yor.

BENG�L VARAK (Eski�ehir): Dervi�, ekonominin i�leyi�i konusunda 'ba����kl�k' diye laf etti. Bir konu �zerinde 'ba���l�k' kazan�lmas� olumlu bir geli�me de�il.

B�R ��RENC� (Ad� sakl�): Bizi geceden be� y�ld�zl� bir otelde kokteyle getirdiler; ilk defa be� y�ld�zl� bir otel g�rd�m. Oradaki manzaray� ve m�zi�i be�enmedim. Amerika'daki Cumhuriyet�iler Partisi'nin IRI adl� bir sivil toplum �rg�t� varm��. Bu, T�rkiye'de de Ar� Hareketi'ni y�nlendiriyormu�; acaba s�k s�k ele�tirilen Kemal Dervi�'i siyasete mi haz�rl�yorlar diye d���nmedim de�il.

ESK��EH�R'den bir ��renci de, kendilerinin bir konferans i�in �stanbul'a getirildi�ini, her �eyi �ok ��k g�rd���n�, ancak ekonomik durumlar� iyi olmayan arkada�lar�n�n da bu toplant�ya gelmesi gerekti�ini belirtiyor.

��KR� VEL�O�LU: Dervi� bana g�ven veriyor. Bayar'� daha izlemek gerekiyor.

Dervi�-Bayar fark�

YED�TEPE �niversitesi'nde birbirlerini izlemeden iki saat arayla konu�an Dervi� ekonomideki yap�lanmadan s�z ederken; Bayar daha �ok nas�l bir T�rkiye ile kar��la�t���n� anlatt�.

4Dervi�, 18 ayda T�rkiye'de yapt�klar�n�, ya�ad��� s�k�nt�lar� ve T�rkiye'nin hedeflerinin ne olmas� gerekti�ini anlat�yor.

4Bayar ise hen�z parti ba�kan� se�ilmedi�inden sadece g�zlemlerini aktarmakla yetiniyor.

4Dervi�, siyasete girip girmeyece�i yolundaki sorulara net bir yan�t vermiyor; sadece ��Siyasetle uyan�p kalkmayal�m. Siyaset her derde deva de�il�� diyor.

4Bayar, ��Siyaset may�nl� tarla; siyasetin ne getirece�ini bilmiyorum. Ancak insanlar� siyasete �s�nd�rmay� ama�l�yorum. ��nk� siyaset yeni y�z, yeni program, yeni umut aray��� i�inde. Bu nedenle siyaset yapmak anlam kazan�yor. Kendimi tecr�beli g�r�yorum; hedefim var, onun i�in siyasete giriyorum�� diyor.

4Dervi�, ge�mi�te ya�ananlara de�il ileriye bak�yor, ��Endi�eli de�ilim art�k. Temel ekonomik yap� oturdu�� diyor.

4Bayar, ge�mi�le hesapla��rken ��Anketlerde toplumun % 79'unun bu �lkede ya�amak istemedi�ini, bununla da �lkede bir ba�ar�s�zl���n s�z konusu oldu�unu�� s�yl�yor.

Her ikisi de hedefin AB oldu�ununda ayn� g�r��teler; ya gelece�i tercih edece�iz ya da ge�mi�imizde s�k���p kalaca��z, diyorlar.

4Dervi�, e�itimde �zg�r d���ncenin olmas� gerekti�ini s�yl�yor. Sivil toplum �rg�tlerinin Meclis kadar i�levsel oldu�unu vurguluyor.

4Bayar da, internetle demokrasinin daha da geli�ece�ini s�yl�yor.

Dalan�a kutlama

DERV�� ve Bayar'� izleyenler aras�nda bulunan i�adam� Selahattin Beyaz�t, ��Davet ettiler geldim�� dedi.

Yeditepe �niversitesi'ni ilk kez g�rd���n�, eskiden Dalan'a k�zd���n� ama g�rd��� manzara kar��s�nda kendisini kutlad���n� s�yledi. Beyaz�t, ��Dalan belediye ba�kan� se�ilince bir arsam� kamula�t�rm��t�. E�er onu yarg�dan kurtar�rsak �niversiteye ba���layaca��m�� dedi.
 
