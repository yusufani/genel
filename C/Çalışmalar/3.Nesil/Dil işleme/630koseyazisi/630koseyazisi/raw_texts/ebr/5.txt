 G�lery�zl� bir �ark� (2)

�stanbul�a d�nd���mden beri kiminle kar��la�sam, sanki fi tarihinden beri firari olan ben de�ilmi�im gibi, gayet �uursuz bir pi�kinlikle; �Yahu g�r��m�yoruz, nerelerdesin?� tonundan lafa giriyorum.

Muhabbet her seferinde ayn� seyirde geli�iyor: �Ben buralarday�m esas�nda. Sen pek g�r�nm�yorsun?�

�Do�ru be... Ben yoktum hakikaten, di mi?..�

�zmir Belediye Ba�kan� Aziz Kocao�lu�nun evsahipli�ini yapt��� ve Nebil �zgent�rk��n G�lery�zl� �zmir �ark�s� adl� belgeselinin de k�smen g�steriminin yap�ld��� gecenin d�zenlendi�i Feriye Lokantas��n�n giri�inde, Ha�met�le (Babao�lu) kar��la�t�k.

Ha�met, eksik olmas�n erken davran�p, �Nerelerdesin?� tonundan girmeme f�rsat b�rakmad�. Fakat tipik Ha�met olarak, yine zinciri bozdu, ezberi da��tt�.

Sar�l��t�k. �Aaa!� dedi, �Sen n�ap�yorsun ki burda?�

�D�nd�m ki ben �stanbul�a� dedim, devrik devrik...

�Ho�geldin de...� dedi, �BURADA n�ap�yorsun?�

��yle bir bakm���m y�z�ne... Sonra bir kahkaha tufan� �eklinde p�sk�rd�m tabii: �Sen hadiseyi iyice k�p�rtt�n art�k latan �zmirli! Hani hasbelkader �zmirli oldu�umuz i�in biz de davetliyiz biliyor musun? Gecenize kat�labilir miyim y�ksek m�saadenle?..�

Aram�zdaki eski bir koddur bu. Ha�met�e dair bin y�ll�k iddiamd�r. Ona isim mi takt�m, tan� m� koydum denir bilemiyorum ama kendisini y�llard�r b�yle �a��r�yorum: �Latan �zmirli...�

�lkgen�li�ini Moda sahillerinde ge�irmi� bir �stanbullu olmas�na ra�men, itiraz da etmiyor yani. Bil�kis, o gece yan�mda konu�tu�u, �zmirli olup olmad���n� soran herkese -g�z kenar�ndan bak�p yamuk bir s�r�t��la kendisini izledi�imi de g�rd���nden- �Valla, arkada�lar, latan �zmirli oldu�umu s�yl�yor. Kararl�y�m, kendime manifesto k�vam�nda bir hik�ye yazaca��m. Art�k bu meseleye bir ��z�m bulman�n zaman� geldi� dedi.

�yi bari... Senelerdir beni f�t�k ediyor. Hep ayn�, hep ayn�:

Ben, �kitelli�de kah�r doldurmaktayken, Ala�at� g�lgeliklerinin esintisinden bahseden yaz�lar�n� okuyup, yine arabaya atlay�p, bas�p �e�me ya da �zmir�e gitmi� oldu�unu ��reniyor, haset i�inde di� g�c�rdat�yor ve ona tehdit telefonlar� a��yorum.

Adam �l�m�ne susam�� olsa gerek ki aradan bir ay bile ge�meden, ak�amlardan bir ak�am vakti telefonuma bu kez; ��nanmayacaks�n, �aka gibi ama �u anda Kar��yaka Park��nda piknik gibi bir �ey yap�yoruz� diye mesaj at�yor.

Kafay� yemek i�ten de�il...

Barkovizyondan Nebil�in filmi g�sterilmeye ba�lad���nda, daha d�neli �unun �uras�nda ka� g�n olmu�, buna ra�men burnumun dire�i s�zlad� hasretten..

�n�m�zdeki her 9 Eyl�l�de ��kar�p ��kar�p tekrar yay�nlanaca��n� tahmin ediyorum ama umar�m, belgeselin b�t�n�n� izleyebilmek i�in ill� ki 9 Eyl�l�� filan beklememiz de gerekmez.

�u anda, elimde 2015�te EXPO�nun evsahipli�ini yapmay� hedefleyen �zmir�in B�y�k�ehir Belediyesi�nin projelerini anlatan kitap��k var. Ge�ti�imiz aylarda Universiade��n �ahane bir �ekilde alt�ndan kalkan �zmir i�in gayet ayaklar� yere basan bir gayedir derim... Tarih gelece�in teminat�ysa e�er, neden olmas�n hem�erim?

Mal�m, �zmir Enternasyonal Fuar�, �n�m�zdeki eyl�lde, kap�lar�n� 75. kez a�acak. Ki, projelerin anlat�ld��� dosyay� incelerken g�rd���m, �ahsen g�lmekten yar�lmama neden olan zarafet belgesi de yine, Belediye�nin her �eye ne kadar haz�rl�kl� oldu�unun delili:

1-10 Eyl�l 2006�da d�zenlenecek 75. �zmir Enternasyonal Fuar� i�in davetiye ��kt� dosyadan iyi mi!!! LCV�li me-ce-ve�li...

Ben telefon konusunda kendime pek g�venmedi�im i�in �imdiden buradan cevab�m� vereyim: Elbette icabet edece�iz efendim... Dedim ya, �imdiden �zmir�i �zledim...
