Annem benim cennetim ve cehennemim  

May�s ay�n�n her ikinci pazar� benim i�in kabus gibidir.

Gelir 130 kiloluk biri, g��s�m�n �zerine oturuverir.

Anneler G�n�'d�r.

Peki ben ne yazaca��m?

Ne yazaca��m da, bir �nceki y�l�n tekrar� olmayacak...

*

Bunca y�ld�r �e�itli numaralar denedim.

Ama art�k deneyecek numara da kalmad�!

Karadeniz istikametinde bat�rd���m gemilere do�ru, dalg�n dalg�n bakarken, �pek'in e-mail'i imdad�ma yeti�ti.

Bu sayede, bu y�l�n Anneler G�n�'nde size �pek'in dedesini anlatabilece�im.

Ger�i tan�m�yorum.

Tan�may���m�n nedeni ya��m�n yetmeyi�i.

Ama o�lu Ali, en yak�n arka�lar�mdan biri.

Biz sadece y�zy�ze kar��la�mad�k, yoksa ben de onu bas�nda �al��an herkes gibi biliyorum, bir d�nemin en �nemli gazetecilerinden biri, tan�yanlar efsane olarak anlat�yorlar...

*

Bunca �a�aal� bir ge�mi�ten sonra birilerinin kendi k��esine �ekilmesi, bunu becerebilmesi, al��t��� bir s�r� iktidar nimetinden vazge�ebilmesi beni b�y�l�yor.

Emeklili�i geldi�inde, en k�t� ihtimalle, apartman ahalisini zapturapt alt�na almaya �al��an onca insan varken, Sadun Tanju'nun s�kuneti bana inan�lmaz iyi geliyor ve iyi ki de bu adam�n o�luyla arkada��m, dedirtiyor.

Gendir ge�er.

Asalettir bula��r.

Gazetelerdeki haberler, koltu�una yap���p kalanlarla inim inim inlerken, Sadun Tanju'nun o geride durma hali, o ba���retmenlikten feragat edebilme yetene�i, m�thi� bir �ey. Her insana nasip olmaz. �ok bilgece bir tav�r. Tan�madan seviyorum yani onu. Sevmenin �tesinde �ok �nemsiyorum ve sayg� duyuyorum. Bu vesileyle Sadun Tanju'nun Anneler G�n�'n� kutluyorum!

*

Ayn� vesileyle �pek'i de kutluyorum.

Vallaha, o da �ahane bir torun.

L�tfen Sadun Tanju'nun di�er torunlar� k�skanmas�n!

Ama �ok esprili bir e-mail atm��.

��Dedemin 45 y�l �nce Anneler G�n� m�nasebetiyle anneanneme yazd��� bir yaz�y� size g�nderiyorum. Ortakokul kitaplar�na bile ge�mi�tir. Kendimi bildim bileli, b�t�n dernek ve vak�flar Anneler G�n�'nde bu yaz�y� yay�nlamak i�in izin isterler! Ama ben size g�nderiyorum. B�t�n annelerin Anneler G�n�'n� de, dedemin bu yaz�s�yla kutluyorum...��

HAM��: Ben de b�yle bir torun istiyorum! Do�mam�� torunuma not: Bak can�m, ne torunlar var. Yemiyor, i�miyor, dedesinin yar�m as�rl�k yaz�lar�n� ara�t�r�yor buluyor, ��kar�yor ve bir gazeteciye g�nderiyor. Sen de ayn�s� yap. ��kartacak bir yaz� bulursun nas�l olsa...

�ZHAM��: Mami. Kusura bakma. Bu y�l karambola geldin. Sadun Tanju'nun yaz�s�n� bulunca �zerine atlad�m. Her y�l seninle ilgili yeni bir �ey icat etmenin zorlu�unu en iyi sen tahmin edersin. Ger�i 60'�nda, ��yeni evine ta��nan yeni gelin halleri��ni yazmaya niyetlenmi�tim ama, olmad� i�te, ba�ka sefere in�allah. Senin de, babam�n da Anneler G�n�'n� kutluyorum. Halam�n ve nenemin de. Bir de ablam�n. Unuttuklar�m kald�ysa, onlar�n da...

Yar�m as�rl�k bir anneler g�n� yaz�s�

Sadun Tanju, Vatan gazetesi-1957

Ben anneyim

Seni, bir h�creden ya�amaya lay�k bir canl� haline getiren benim. Seni �st�raplar�n en b�y���yle do�urdum; sevin�lerin en b�y���yle kollar�ma ald�m. Sana ilk davran���, ilk g�l���, ilk bak���, ilk heceyi ben ��rettim. Seni kar��l�ks�z, menfaatsiz, tertemiz ilk ben sevdim. Sana hayatta ilk laz�m olacak dersleri ben verdim. Senin y�z�nden ilk ac�lar� ben duydum. �lk a�lay��lar�n� benim g��s�mde dindirdin. �lk s�rr�n� bana a�t�n. �lk dost beni edindin.

Ben anneyim!

Bana her zaman g�vendin. �lk a�k�n� ben hissettim. �z�nt�lerin benim �z�nt�lerim oldu. Seni pencerelerde bekledim, geli�inde kap�lara ko�tum. Seni her zaman ayn� duygularla ba�r�ma bast�m, seninle iftihar ettim, seninle ta�land�m, �ereflendim.

Ben anneyim!

Ben, Tanr�'n�n en b�y�k l�tfuna lay�k g�r�lm���m. Ben bereketim. Ben Tanr� gibi insan yaratabiliyorum. Ben yery�z�nde iyi ve g�zel, k�t� ve �irkin her�eyin mesuliyetini ta��yorum. Medeniyet benim, mazi benim, gelecek g�nlerin �midi benim.

Ben anneyim!

Ben insanl���n ba�� ve sonuyum. Ben hayata �ekil veren sanatkar�m. �stedi�im renkleri kullan�r, istedi�im gibi yontar�m. Beynine ilk nak�olacak s�zler benim, kalbe ilk yerle�ecek duygular benim duygular�md�r. Ben cennet ve cehennemim. Ben istersem sevgi karde�lik ve dostlukla b�y�t�r�m; istemezsem kinle, d��manl�kla i�ini doldururum. Ben d�nyaya nizam veren iradeyim.

Ben anneyim!

Ben sab�r ve tahamm�l�m. Ben en yumu�ak ve en sertim. Cesur olmay� nas�l benden ��rendinse, korkuyu da ben sana ��rettim. Seni ilk �pen ve ilk d�ven benim. Sevmek, a��k olmak, �efkat, kin, dostluk ve d��manl�k duygular�n�n hepsi bende. 

Ben anneyim!

Bir ac� duyarken beni �a��r�rs�n. Ben teselliyim. �lsem bile g�z�m arkamdad�r. Ben endi�elerin derin kuyusuyum. Kendi i�ime d��erim. Ben b�t�n alakalar�n mihrak�y�m. C�mert oldu�um kadar hasis, k�skanmaz g�r�nd���m derece de k�skanc�m.

Evet, seni k�skan�r�m. Sen benim eserimsin, sen benim eme�imsin. Sen benim g�zel g�nlerim, ge�en �mr�m, b�t�n hat�ralar�ms�n. Seni k�skan�r�m. Seni bu duygumla bunalt�r, isyan ettirir, �zerim. Seni kendime hasretmek isterim. 

Bunun i�in k�skan�r�m seni.

Ben anneyim!

Ben sayg�n�n mihrab�y�m. �n�mde diz ��kmeni isterim. G�nl�nde yer etmeyi isterim. Hakk�m �densin isterim. Unutulmaktan korkar�m. Ba� �st�nde ve ba� k��ende yerim.

Bu benim hakk�m.

Ben anneyim!

Ve son nefesimde...

Her zaman...

S�t�m ve hakk�m helal olsun yavrum derim.
 
