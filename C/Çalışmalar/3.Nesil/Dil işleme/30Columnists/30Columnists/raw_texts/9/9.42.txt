An injection that spurs adults' bone into healing as quickly as children's could revolutionise the treatment of fractures, doctors believe. 
The drug teriparatide more than doubles the speed at which broken bones mend  -  and greatly reduces pain, a study found. 
Researchers described some of the recoveries as 'miraculous', with patients confined to wheelchairs by long-standing fractures able to walk again.

 
They say teriparatide, which is already used to prevent osteoporosis, could have a profound impact on the treatment of broken bones. 
Given once a day as an injection at home, it could be used to mend fractures that have resisted healing, as well as common and painful breaks for which there is currently no treatment other than letting nature take its course. 
Pensioners and those in late middle age are likely to be the biggest beneficiaries, as bone loses its ability to heal with age. 
But professional athletes, soldiers and others who need to recover from bone damage as quickly as possible for their career could also find it useful. 
Studies show that teriparatide, also known as Forsteo, works by boosting production of stem cells  -  'master' cells with the ability to turn into other cell types, including bone and cartilage. 
Researcher Dr J Edward Puzas said: 'In many people, as they get older, their skeleton loses the ability to heal fractures and repair itself. 
'With careful application of teriparatide, we believe we've found a way to turn back the clock on fracture healing through a simple, in-body stem cell therapy.' 
The researchers, from the University of Rochester Medical Centre in New York State, were alerted to the possibilities of the drug when treating people with osteoporosis, or brittle bones. 
Dr Susan Bukata said: 'I had patients with severe osteoporosis, in tremendous pain from multiple fractures throughout their spine and pelvis, who I would put on teriparatide. 
'When they would come back for their follow-up visits three months later, it was amazing to see not just the significant healing in their fractures, but to realise they were pain-free  -  a new and welcome experience for many of these patients.' 
The researchers tried the drug on 145 patients with unhealed fractures, half of whom had been in pain for at least six months. 
After two to three months of injections, 93 per cent of the fractures healed and pain was greatly eased, a recent meeting of the Orthopaedic Research Society heard. 
Dr Bukata said: 'It takes three to four months for a typical pelvic fracture to heal. But during these three months, patients can be in excruciating pain because there are no medical devices or other treatments that can provide relief. 
'Imagine if we can give patients a way to cut the time of their pain and immobility in half. 
'That's what teriparatide did in our initial research. 
'We saw complete pain relief, callus (cartilage) formation and stability of the fracture in people who had a fracture that up to that point had not healed.' 
The team are now comparing the recovery of pelvic fracture patients given teriparatide for four months with another group given a dummy drug. 
But the need for further largescale trials means even if all goes well, teriparatide is around six years from being routinely used to mend breaks. 
