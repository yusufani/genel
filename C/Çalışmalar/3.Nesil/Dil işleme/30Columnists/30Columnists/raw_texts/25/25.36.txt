We�ve talked in recent weeks about various legislation being passed and implemented this year. Of course, the largest piece of legislation to date has been the stimulus package, aka the American Recovery and Reinvestment Act (ARRA). It has many, many provisions, quite a few of which deal directly or indirectly with health insurance and healthcare (see Deconstructing the Stimulus Package March 4, 2009). By far, the most comprehensive (and confusing part) has to do with the COBRA provisions (keeping your health insurance coverage after you leave your employer). It�s all really very simple if we keep in mind the rules. In fact, I�ve received so much correspondence asking for more clarification and information on the matter, I�d like to suggest we make a game of it. I�ll outline a particular employee�s situation, at which point we�ll try to determine what (if anything) this person is eligible for. To keep the game simple, we�ll ignore whether or not the person is eligible for regular COBRA and just concentrate on whether they qualify under the new law.  Here are the rules:

The employee must have been involuntarily terminated sometime between September 1st, 2008 and December 31, 2009.
The employer has to subsidize everyone that is eligible for COBRA 65% starting March 1, 2009. (Employers get a credit against payroll taxes).
The subsidy is only payable for nine months.
If a person becomes eligible  for other coverage, the subsidy ends (doesn�t matter whether or not they actually take the other coverage).
The actual time frame for them to keep COBRA hasn�t changed (18 months most instances).

That�s it (for our purposes we�re assuming that nobody is making more than $125,000 a year individual or $250,000 joint filers).  Ready, set, go.

Judy was laid off from her job October 7, 2008.  She started paying her COBRA premiums effective November 1st, 2008 and is paid through February 2009.  The following questions need to be answered.

Is Judy eligible for the subsidy?
If so, when will her subsidy end?
When will her COBRA end?
Take your time.

O.K., if you answered "yes" to question 1, give yourself a hand.  Because Judy was laid off, meaning she didn�t leave voluntarily, and she was terminated between September 1, 2008 and December 31, 2009 she is eligible for the subsidy. We�ve determined Judy is eligible to receive help with her COBRA premiums, so the next question is; when will it end? The simple answer is her subsidy will end on December 1st, 2009. That�s because the subsidy starts on March 1st and is payable for 9 months. Similarly, the simple answer for when her COBRA ends is�if you said May 1st 2010, you are more alert than most.  It would end May 1st because the time frame you can keep COBRA (18 months) hasn�t been changed by the law.  The key here is that it is 18 months from the COBRA start date (November 1st), not when the subsidy kicked in. 

Bonus Question:  I forgot to tell you, Judy�s husband is on his employer�s plan so he doesn�t need Judy�s COBRA coverage.  Does this change anything?

Yes. Judy wouldn�t be eligible for the subsidy, because she could go on her husband�s plan if she wanted to. If she decides not to, that doesn�t change the fact she�s not eligible for the subsidy.  She can however, still keep her COBRA coverage the full 18 months.

If you got 0-1 right, consider yourself semi-informed. 2-3 right, your above the curve.  And if you got the bonus question right, I�ve got three words for you.  Want a job ?

Until next time, stay healthy.

