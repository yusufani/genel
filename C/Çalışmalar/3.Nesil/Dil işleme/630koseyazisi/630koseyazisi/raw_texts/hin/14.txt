Pirelli'nin efsane takvimleri ve 2006!..

40 y�ldan fazla bir zamand�r s�ren bir efsane!.. Esteti�i, g�zelli�i ve foto�raf sanat�n� sevenler i�in hayat boyu saklanacak bir koleksiyon par�as�..
Pirelli takvimlerinin genel tan�m�n� b�yle yapabilirsiniz.. Bu takvimlerin, Pirelli ad� ile �retilen lastiklerin hatta �n�ne ge�ti�ini de s�yleyebilirsiniz..
Pirelli, efsane takvimlerini, y�llarca Londra'n�n birbirinden ilgin� mek�nlar�nda sunduktan sonra, d�nyada ba�ka kentler oldu�unu da hat�rlad�.. Ge�en y�l Rio'ya gittiler.. Bu y�l Paris'e.. 
R�ya kenti Paris'te takvimi sunduklar� mek�n Pirelli'ye hi� yak��mad� ilk defa.. D�rt duvar�na perde �ekilmi� bir hangar d���n�n.. ��te o.. Hangar cart diye g�zlere batmas�n diye ���klar� da iyice k�sm��lar ki, nerdeyse yan masay� g�remiyorsunuz, g�zleriniz lo�lu�a al���ncaya dek.. 
Nerde gezmeye doyamad���m Londra Do�a Tarihi M�zesi'nde dinozor iskeletinin alt�ndaki masam.. Nerede �nceki y�lki, Londra Adliye Saray�'ndaki muhte�em do�al dekor ve hava.. 
Efendim Paris'teki sunum asl�nda Paris G�zel Sanatlar Akademisi'nde planlanm�� ama, bakm��lar ki, 700 konuk orada t�k�� t�k�� olacak, iki g�n �nce karar de�i�mi�, bu hangar bulunmu�..
Pirelli'yi art�k yak�ndan biliyorum.. Buna, "�zr� kabahatinden b�y�k" derler.. B�ylesine dev bir b�t�e ile yap�lan bir organizasyonda salonun yetmeyece�i iki g�n �nce mi anla��l�r?. Salonun boyutlar� m� s�rpriz, konuk say�s� m�?.
Hay�r.. Bu mazeret inan�l�r gibi de�il.. Peki ger�ek ne?.. Bilmem.. Bildi�im bu efsane takvime ve Pirelli ad�na hi� yak��mayan, Paris'e gitti�imize bile de�meyen bir gece d�zenlenmi�ti bu defa.. Yemek bitsin de ka�al�m diye masadaki dostlar�n lokmalar�n� sayd�m resmen.. 
Takvime gelince.. Bak�n hemen �unu s�yleyeyim.. Bana alakas�z bir zaman ve mek�nda bu resimleri g�sterseler ve "Tahmin et" deseler "Pirelli Takvim Sayfalar�" olduklar� akl�mdan ge�mezdi.
��in �zeti bu.. Bu foto�raflar�n kendisi "Marka" olamam��.. Bunca y�l�n efsane takviminin fark�n� yaratamam��. Aral�k ay�nda t�m d�nya marketlerini dolduracak binlerce takvimde benzerlerini fazlas� ile bulaca��n�z siyah beyaz ve de renkli i�ler i�te..
Tabii.. Bunca masraf.. Jennifer Lopez'den Kate Moss'a bunca �nl�, d�nya cenneti Frans�z Rivieras� bir araya gelir ve binlerce kare deklan��re basarsan�z e�er, bir iki g�zel ��kar..
Ali At�f Hocam takvimin baz� sayfalar�n� "Hafif Porno" bulmu�.. �� tur d�nd�m.. Pornoyu ge�in, erotik resim yok i�lerinde..
Mert ve Marcus bu resimleri, Erkek�e Genel Yay�n M�d�r� H�ncal'�n �n�ne getirseler "Bu malzeme, bu b�t�e ile yapabildi�iniz bunlar m�" diye f�r�alard�m inan�n..
"Pornoyu ge�in, erotik bile de�il foto�raflar" diyorum ya.. Ne peki?..
Bir iki �ok ucuz, estetikten ve sanattan uzak ��plakl�k ve �o�unlukla s�radanl�k.. 
Fark� yaratmayan, fark� fark ettirmeyen foto�raflar.. 
A��rlama.. Bak�n oras� ger�ekten fevkalade idi.. �teki �lkelerden gelenleri bilmem.. 
Ama Pirelli sayesinde tan�d���m ve y�llardan beri �ok sevdi�im dostlar�m aras�nda yer alan Atilla ve Melda'n�n bizleri mutlu etmek i�in nas�l ��rp�nd�klar�n� anlatmam m�mk�n de�il.. �stelik bu ��rp�nman�n �smarlama de�il, y�rekten gelme oldu�unu hissediyorsan�z e�er.. 
Te�ekk�r ederim Sevgili Dostlar�m!.. 
