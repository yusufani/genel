THERE was a time when the nanny state seemed an attractive idea. Under the gentle guidance of dear nanny, we adults who did not know what was good for us would be guided in the right direction. Punitive taxes would be put on cigarettes and booze to discourage us, we would be lectured on the evils of these sinful pastimes and warnings would be posted on the fag packets and bottles.

Still, the choice was left up to us. We could go to a shop and buy these legal products and be left alone, within reason, to enjoy them.

Well, not any more. Nanny is out on her ear. She's been sacked and, unlike Fred Goodwin, she's getting no pension. Instead nanny is to be replaced by a bully; two in fact. Nicola Sturgeon the Health Secretary and Shona Robison the Public Health Minister have announced they want yet more restrictions on buying tobacco that go as far as they can, short of making it illegal. Next week it's booze.

Of course they would like to make tobacco illegal � Robison has called for Scotland to be smoke-free by 2020 � but they dare not put such Draconian laws on the statute for they know the public would put them on the naughty step beside Fred Goodwin.

So instead they are going to ban all tobacco products from open display and shove them under the counter.

There is no reliable evidence that this will make any difference to young people buying cigarettes � even the anti-tobacco campaign group ASH has admitted this. It's all about de-normalising smoking � making smokers feel guilty, turning them into pariahs.

We will be left with the situation where tobacco MUST be hidden in the newsagents � but pornographic magazines will be on display. Common sense doesn't come into it � drunk on power, bullies don't care what anyone thinks.

Tell me this. If the display of tobacco is the reason that young people smoke cigarettes, why is it that so many of our youth take cannabis, cocaine and heroin?

Pickled Salmond
Alex Salmond seems keen to pick a fight (and not for the first time) with Alistair Darling and Gordon Brown over �500 million cuts in the Scottish block grant each year for the next two years. What a false and daft quarrel to start.

Instead of trying to spend more taxpayers' money, he should be looking to cut his own cloth to fit the circumstances by finding savings and passing the proceeds back to the hard-pressed public.

For instance, he should be calling on the other parties to back him in cancelling the disaster that is the Edinburgh trams construction. Sure, more than �100m has been spent, but by saving on the further �400m, by cancelling the Borders railway line (�300m) and selling off Scottish Water as a mutual co-operative (�600m over two years) he could be reducing his budget, and then some.

The Scottish Government is too big and if Scotland were independent Alex Salmond would be making these cuts in an instant.

Give the money back
The outrage at the news that Sir Fred Goodwin � ex-chief executive of Royal Bank of Scotland � is already drawing some �650,000 a year as a part of his pension arrangements with his former employer is widespread and genuine. It is far worse than any of the annoyance felt when former first minister Henry McLeish was found to be drawing a substantial pension already after his resignation. Both pensions are, of course, funded by the taxpayer, for had the Government not stepped in to save RBS, it's hard to see how any pensions would be getting paid by the bank. This makes the amount of �650,000 all the more absurd.

Private companies should be free to set their own arrangements so they can attract the best people possible � but this deal stinks. What was the RBS remuneration committee thinking about? Goodwin might have a binding legal contract that gives him such a ridiculous entitlement but he has no moral authority to keep it � and as he's still employable (apparently) he certainly has no need of it.

If he wants to show his face publicly in Scotland without being shunned, catcalled and booed he should publicly renounce the arrangement � or better still, donate the funds to all the other RBS pensioners whose savings have been ruined by his banking misjudgements.
