An old wives� tale has it that a little kick to the palate before bed can lead to fitful sleep, if not nightmares. 

It�s the sort of wisdom that often turns out to be based on no evidence at all � or, worse, flat wrong. But in this case, it�s good advice. 

Research has shown over the years that a spicy meal at night can indeed lead to poor sleep. The most direct study to show this was published in The International Journal of Psychophysiology by a team of Australian researchers. The scientists recruited a group of young, healthy men and had them consume meals that contained Tabasco sauce and mustard shortly before they turned in on some evenings and nonspiced control meals on other evenings. 

On the nights that included spicy meals, there were marked changes in the subjects� sleep patterns. They spent less time in both the light phase of sleep known as Stage 2 and the deep, slow-wave Stages 3 and 4. All of which meant that they experienced less sleep over all and took longer to drift off. 

Several things may account for the effect. An obvious possibility is indigestion. But the scientists also noted that after eating the spicy meals the subjects had elevated body temperatures during their first sleep cycles, which has been linked in other studies to poorer sleep quality.

