You could call it Revenge of the Ninjas. While European and US banks have spent the past two decades piling on the leverage and stacking up the loans, their Japanese counterparts were stuck in the slow lane, repairing their balance sheets and restricting their lending.

That conservatism has left them ideally placed to take advantage of the credit-crunch carnage - and they are already snapping up the bargains. Last week alone, Japan's biggest investment banking group, Nomura, grabbed Lehman's Asian and European investment banking businesses from under the nose of Barclays, which had hoped to add these two to its purchase of the failed bank's US arm. Mitsubishi UFJ, the country's biggest bank, is spending up to $8.5bn (�4.6bn) on a 20 per cent stake in Morgan Stanley and there has been speculation that Sumitomo Mitsui Financial Group is to invest $2bn in Goldman Sachs. 

That follows deals like Tokio Marine's $4.7bn purchase of insurance group Philadelphia Consolidated, Mizuho Corporate Bank's 130bn yen (�663m) stake in Merrill Lynch and Sumitomo Mitsui Banking Corporation's 106bn yen investment in our own Barclays.

Simon Somerville, manager of Jupiter's Japan Income fund, thinks there will be more such deals. 'Japan Inc, not just the banks, has been very cautious. [The banks] have very strong balance sheets and are not leveraged and they have been criticised for it. Now they are taking advantage of distressed sellers and picking up some trophy assets.'

And at bargain prices: Nomura paid just $225m for Lehman's Asia Pacific business, and while it has not disclosed the price it paid for the European arm, it is unlikely to have been significantly more. A year ago, a 20 per cent stake in Morgan Stanley would have cost Mitsubishi three times as much. 

It is not the first time that Japanese banks have made concerted forays overseas. Two decades ago, they ranked among the biggest in the world and were snapping up foreign investment banks along with assets like the Rockefeller Centre in New York and Universal Studios. These were funded by the huge profits generated on their domestic property assets - profits that evaporated as that market collapsed, leaving the banks stuck with a welter of bad loans and enmeshed in a web of shareholdings in Japanese industry. 

The country and its banks are only just emerging from the banking failures, rescues and write-offs that followed: between 1990 and 2004, Japanese financial institutions wrote off more than 125 trillion yen, according to Kristine Li at KBC Securities Japan - more than the $500bn or so estimated losses from the current credit crunch. They face yet more write-offs on the Lehman collapse: Japanese banks are among the most vulnerable to the failed US bank, with a collective exposure of 320bn yen, but because their balance sheets are far less highly leveraged than European rivals - Barclays and Deutsche Bank, for example, have loans of 50 times their assets - the pain of writing this off will not be too severe.

For Somerville, the biggest surprise is that the Chinese, who are also flush with cash and have steered clear of the sub-prime crisis, have not joined the Japanese in their spending spree. That, he says, may reflect the huge opportunities in their domestic market, in contrast to the Japanese: 'Possibly they are also not ready for that kind of expansion,' he suggests.

Japanese banks certainly are. The authorities in Tokyo, having funded years of rescues, have been adept at deflating any new bubbles that threatened to emerge - last year, banks were encouraged to restrict their property lending after a steep rise in prices. 

But, as Li observes in an analyst's note, 'the problem for the banking industry is that it is very difficult to earn money in a business environment that does not have bubbles of some form or another.' 

The Japanese banking market is very mature, and serves an ageing population, so there is little growth in the home market. 'Without these bubbles, Japanese banks can barely have any earnings growth,' adds Li. 

The key question is whether the Japanese will be able to manage this wave of expansion better than the last. Previous forays into investment banking have met with little success because Japanese management has been reluctant to cede any power to the buccaneering executives in these divisions and hesitant to pay the kind of salaries that the 'bulge bracket' has - at least hitherto - demanded.

The fact that Nomura has earmarked $1bn to create a bonus pool for the European and Middle East operations it is taking on suggests it now appreciates the need to be generous - although with job losses, rather than year-end bonuses, on most bankers' minds, Oriental thrift may not seem out of place. 

'Over the last 20 years, Nomura has not punched its weight in international markets. It has tried to build businesses in international markets with varying degrees of success,' says Sadeq Sayeed, a special adviser to the bank. 'We have been looking for opportunities outside Japan for some time... We have shown that speed is a characteristic we relish and change is something we are not afraid of.'
