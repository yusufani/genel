Ald� saz� eline! 

 
 

Hani derler ya; ald� saz� eline, hem �ald�, hem oynad�, hem de oynatt�...

F.Bah�e'de aynen b�yle ald� futbolu eline, hem �ald� hem oynad� hem de "karamsar" g�nler ya�ayan �lkeyi zevkle, co�kuyla, ne�eyle d�nd�re d�nd�re oynatt�.

Oh be!

F.Bah�e'nin "akordu" m�kemmel futbolundan "inleyen nameler" de�il, "inleten nameler" ��kt�. Kimli�ini "d�nya devi" diye ��kartm�� olan �ngilizler'in gururu Manchester United, F.Bah�e'nin darbeleri kar��s�nda resmen inleye inleye t�kendi!

Diyen olabilir ki "Fazla abartmayal�m. Adamlar �stanbul'a gen� tak�mla geldi."

Do�rudur, adamlar �stanbul'a gen� bir kadro ile geldi. Ama bunu kabul ederken, �unun da bilinmesi gerekiyor; bu gen�lerden en az 7'si kendi �lkelerinin milli tak�m�nda oynuyor. Alex Ferguson, bu gen�lerden Portekizli Ronaldo'yu 20 milyon dolar kar��l���nda kul�b�ne kazand�rm��t�. Ferguson, kul�b�nde 17 y�ld�r devam eden g�rev s�reklili�ini de "nadas tekni�ini" �ok iyi bilmesine bor�luydu. Daha elindeki kadronun verimlili�i t�kenmeden geride tuttu�u "yetenekli kadroyu" haz�rl�yordu. Bak�n F.Bah�e ma�� sonras� demecine, anlayacaks�n�z. Aynen �unlar� s�yl�yordu �nl� hoca: "Buraya �nl� futbolcular�m� getirmedi�im i�in asla pi�man de�ilim. F.Bah�e kar��s�nda seyretti�iniz oyuncular� kafan�z�n bir yan�na yaz�n. Bir dahaki geli�lerinde A tak�m�n �nemli par�alar� olacaklar..."

��te, Ferguson'un Manchester United kul�b�nde meslek ya�am�n� uzatan da "bu kafa yap�s�"yd�. Elindeki kadro t�kenmeden yerini alacak kadroyu haz�rlamaya ba�l�yordu. F.Bah�e'nin kar��s�na ��kard��� gen� kadro da Ferguson'un �ok yak�n gelecekte d�nyaya kafa tutaca��na inand��� kadroydu.

3 b�y�k �zellik
��kr� Sara�o�lu Stad�'ndaki �ngiliz ekibine "gen� tak�m" diye dudak b�kenlerin �u ger�e�i de hat�rlamalar� gerekiyor, bug�n R��t� ve Van Hooijdonk'u bir kenara al�n, F.Bah�e de ya� ortalamas� 22-23 olan gen� bir tak�md�. F.Bah�e �mit Milli Tak�m�n unutulmaz kadrosundan 8-9 ismi b�nyesine katm��, bu devrimin ekme�ini yeni yeni yemeye ba�lam��t�.

Gen� kadronun �ok b�y�k 3 �zelli�i vard� ki; Manchester kar��s�nda da a��k ve net seyredildi:

1. Futbolculardan �o�u "�mit Milli" k�kenli oldu�undan "birbirleriyle oynama al��kanl�klar�" m�kemmeldi. Sel�uk aralar�na gelmi�ti, Kemal ve Serhat da gelecek ve yerle�eceklerdi. O zaman tak�m olma �zelli�i daha da �st seviyeye ��kacakt�.

2. Kazanma arzusu, iste�i, 10 kiremiti �st �ste koyup, bir el darbesi ile k�rabilen karatecinin ula�t��� "en �st �izgideki konsantrasyonun" ayn�s�yd�. Karateciler buna "delilik s�n�r�" diyorlard�. F.Bah�e'nin Manchester kar��s�ndaki kazanma iste�i de "delicesine'ydi. Delicesine kazanmay� istemi�ti. Gen� kadro olu�lar�n�n da, elbette bu zorlamada pay� vard�.

3. Teknik, taktik ve oyun disiplinindeki m�kemmelli�i b�rakal�m bir yana, F.Bah�e'ye ma� kazand�ran bir ba�ka m�thi� �zellik daha vard�; �l� top de�erlendirmeleri nerede ise "penalt�" kadar de�er kazanm��t�. Topu istedi�i yere eliyle b�rakm�� gibi kullanan Alex ve bu toplara kalabal�k aras�ndan ��k�p dokunabilen Tuncay-Nobre ikilisi tehlikenin ad�yd�. Allah nazardan esirgesin, Tuncay F.Bah�e'nin "tirbi�onu" gibiydi. D�ne d�ne �i�e mantar�na giren ve s�k�p ��kartan "tirbi�ondan" farks�zd�.

Kim kazan�r? 
Bir�ok dost, arkada�, taraftar soruyor: "G.Saray m�, F.Bah�e mi kazan�r?"

Kabul etmek gerekir ki, G.Saray i�in bundan daha m�kemmel bir ma� tarihi olamazd�.

F.Bah�e 4 g�n �nce oynad��� zorlu Manchester ma�� nedeniyle, G.Saray'�n kar��s�na baz� "�zellik erozyonuna" u�ram�� olarak ��kacakt�. ��yle ki;

1. K�sa s�re aral�kla 2 defa �st �ste "konsantrasyon �izgisini" karatecinin "delilik seviyesine" ��kartmak, kolay olmayacakt�. Yenilseler bile yine de G.Saray'�n �n�nde olacaklar� d���ncesi kafalar�nda istemeseler de duracakt�.

2. F.Bah�e'de bir de oyun plan� i�inde �ok �nemli yeri olan Aurelio cezal�yd�.

G.Saray kendi seyircisi �n�nde elbette bu F.Bah�e'yi yenebilir. Fakat yense ne de�i�ecekti? 30 ya� ortalamas�yla oynayan G.Saray, 23 ya� ortalamas�yla oynayan F.Bah�e'yi nereye kadar takip edebilecek?

�nemli olan bu! 

 
