ONE commentator on radio, who didn't sound as though he had studied much constitutional history, came out with the truism about no one being above the law, including MPs.
He implied that the police were just treating Damien Green as they would any other of Her Majesty's subjects suspected of having committed a crime.

Not so, M'Lud. Although MPs are bound by the same laws as the rest of us, the Westminster parliamenADVERTISEMENTt is sovereign � it makes the laws. 

In order to do so without intimidation or pressure � either from the monarch, in the old days before Charles I lost his head, or now, from an all-powerful PM in Downing Street � they exercise all the patronage formerly the preserve of monarchs. MPs have privilege within the precincts of the House of Commons to name names and tell it as it is. 

Allegations can be made in the House of Commons that would attract a libel charge if made elsewhere (in Scotland, it would be defamation). This is a cornerstone of our democracy because, as well as preventing a dictator or small unrepresentative group taking power, it enables MPs to question and accuse the Government of failure, and call Ministers to account.

MPs are expected to act honourably, in the public interest. If they discover the Government is lying, hiding facts from Parliament or falling down on the job, from material leaked to them in an unmarked brown envelope, it's time-honoured and in the public interest that they should make it known to the relevant people affected by the Government's action.

When the present Prime Minister was building his reputation in opposition as an MP not to mess with, he had a small army of moles who supplied him with sometimes devastating information to use against the Government. I would not have been able to demand an inquiry into the process of building the Scottish Parliament if someone hadn't sent me a report on the Holyrood project that was not made available to MSPs.

As an MSP � like MPs � I'm sent lots of information about inexplicable decisions or deliberately misleading statements made by councils, health boards, private companies, the Westminster and Holyrood governments. Like Damien Green, and Gordon Brown before him, sometimes the right course of action for me to take is to let the cat out of the bag, so the people I represent know the full facts. 

Sometimes, information is supplied by a person known to me. If I know my informant to be motivated by a genuine sense of fairness and democracy, and if I'm sure the information has no strings attached and should be in the public domain, I'll try to use it. 

From what I've read of Damien Green, his attitude towards making public information the Government would prefer people not to have is much like my own or most MPs and MSPs. So why on earth use anti-terrorist cops to search his house when at worst the Tory frontbencher had a steady stream of info from one contact. The security of the realm was not at risk, and the political state of the Government is not a police matter.

Speaker Michael Martin should have defended the sovereignty of the Commons and refused police permission to enter. He should resign while he retains some dignity. 

If the Home Secretary really wasn't informed of the police action before they arrested Damien Green, she should sack the police top brass, before offering her resignation.

Her appearance on TV on Sunday morning suggested she had been sent out to lie for the party. She'll regret having done so if a fall guy is needed and she's hung out to dry because someone in the hierarchy has to admit to having known that police would be entering the environs of Westminster to try and prove something sinister was being hatched. 

It was a put-up job. But who dunnit?

RBS show the way

Good for the Royal Bank � keeping people in their homes is an essential step towards righting the wrongs of the credit crunch and banking collapse.

Readers will know I've been banging on about the Westminster Government using its new position of influence as a major shareholder in the banks to prevent pointless evictions that will lumber banks with property many fewer people can afford to buy, while pulling the rug out from under the very people whom the economy needs to kick-start growth.

As the date of the HBOS shareholders meeting approaches, I'm growing increasingly angry and astonished at the lack of fight amongst the opposition parties in Holyrood, but in this crucial matter for the greater Edinburgh and wider Scottish economies, Scots have been betrayed by Westminster.
