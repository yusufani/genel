myss segment para stack 'stack'
    dw 32 dup(?)
myss ends
myds segment para 'data'
cr equ 13
LF EQU 10
DIZI DB 100
MSG1 DB 'Lutfen dizinin boyutunu giriniz:',0
MSG3 DB CR,LF, 'Lutfen Elemanlari giriniz:',0
HATA DB CR,LF, 'Dikkat sayi girmediniz yeniden giriniz. ',0
HATA2 DB CR,LF, 'Gireceginiz sayi -128 ile 127 arasinda olmalidir.Tekrar sayi giriniz: ',0
ayirici db ' ; ',0
MYDS ENDS
MYCS SEGMENT PARA 'KOD'
    ASSUME CS:MYCS,DS:MYDS,SS:MYSS
ANA PROC FAR
    PUSH DS
    XOR AX,AX
    PUSH AX
    MOV AX,MYDS
    MOV DS,AX
    MOV AX,OFFSET MSG1 ; Dizi boyutunu al
    CALL PUT_STR
    CALL GETN
    MOV CX,AX
    push cx
    mov di,cx
    dec di
    XOR SI,SI; DIZI ELEMANLARI AL
    MOV AX,OFFSET MSG3
    CALL PUT_STR
eleman_al:  CALL GETN
    MOV DIZI[SI],AL
    INC SI
    LOOP eleman_al
    xor si,si
    call sirala
    xor si,si
    pop cx
yazdir:  mov al,dizi[si]
    CALL PUTN 
    mov ax,offset ayirici ; araya ; isaretini koy
    call put_str
    inc si
    loop yazdir
    RETF
ANA ENDP
sirala proc near
     push di  ; Ilk degerler ilk ve ikinci ifde lazim olucak o y�zden stackte tuttuk
     push si  ; ""
     mov dx,si; dx i pivot degerini hesaplamak i�in kullandim  ayarladim
     add dx,di; pivot =si+di / 2
     shr dx,1;  ""
     mov dl,dizi[bx]; dl artik pivotumuz
  l1:
     cmp si,di
     jg  ilkif
l2:  cmp dizi[si],dl
     jnl l3
     inc si
     jmp l2
l3:  cmp dizi[di],dl
     jng icif
     dec di
     jmp l3
icif:cmp si,di
     jg l1
     mov dh,dizi[si]
     XCHG DH,dizi[di]
     mov dizi[si],dh
     inc si
     dec di
     jmp l1
ilkif:pop bx
      cmp bx,di
      jnl ikinciif
      push si
      push di
      mov si,bx
      call sirala
      pop si
      pop di
ikinciif:pop bx
        cmp si,bx
        jnl son
        push si
        push di
        mov di,bx
        call sirala
        pop si
        pop di
son:    ret 
sirala  endp

GETC 	PROC NEAR
		MOV AH,1h
		INT 21H
		RET
GETC 	ENDP

PUTC	PROC NEAR
		PUSH AX
		PUSH DX
		MOV DL,AL
		MOV AH,2
		INT 21H
		POP DX
		POP AX
		RET
PUTC	ENDP

GETN	PROC NEAR
		PUSH BX
		PUSH CX
		PUSH DX
GETN_START:
		MOV DX,1
		XOR BX,BX
		XOR CX,CX
NEW:
		CALL GETC
		CMP AL,CR
		JE FIN_READ
		CMP AL, '-'
		JNE CTRL_NUM
NEGATIVE:
		MOV DX,-1
		JMP NEW
CTRL_NUM:
		CMP AL,'0'
		JB error
		CMP AL,'9'
		JA error
		SUB AL, '0'
		MOV BL,AL
		MOV AX,10
		PUSH DX
		MUL CX
		POP DX
		MOV CX,AX
		ADD CX,BX
		cmp cx,127
		jg error2
		cmp cx,-128
		jl error2
		JMP NEW
error:
		MOV AX, OFFSET HATA
		CALL PUT_STR
		JMP GETN_START
error2:
		MOV AX, OFFSET HATA2
		CALL PUT_STR
		JMP GETN_START
FIN_READ:
		MOV AX,CX
		CMP DX,1
		JE FIN_GETN
		NEG AX
FIN_GETN:
		POP BX
		POP CX
		POP DX
		RET
GETN    ENDP



PUTN	PROC NEAR
		PUSH CX
		PUSH DX
		XOR DX,DX
		PUSH DX
		MOV CL,10
		CMP AL,0
		JGE CALC_DIGITS
		NEG AL
		PUSH AX
		MOV AL, '-'
		CALL PUTC
		POP AX
CALC_DIGITS:
		DIV CL
		ADD AH, '0'
		MOV DL,AH
		PUSH DX
		XOR AH,AH
		CMP AL,0
		JNE CALC_DIGITS
DISP_LOOP:
		POP AX
		CMP AL,0
		JE END_DISP_LOOP
		CALL PUTC
		JMP DISP_LOOP
END_DISP_LOOP:
		POP DX
		POP CX
		RET
PUTN	ENDP


PUT_STR	PROC NEAR
		PUSH BX
		MOV BX,AX
		MOV AL,BYTE PTR [BX]
PUT_LOOP:
		CMP AL,0
		JE PUT_FIN
		CALL PUTC
		INC BX
		MOV AL,BYTE PTR [BX]
		JMP PUT_LOOP
PUT_FIN:
		POP BX
		RET
PUT_STR	ENDP
mycs ends
end ana

