Taking vitamin D supplements in middle age could cut the risk of Alzheimer's disease in later life, scientists say. 

The vitamin, made when our skin is exposed to sunlight, is already credited with keeping the immune system and bones strong and preventing some cancers. 

And the latest study found high levels were also closely linked to keeping mentally sharp in old age. 

It is one of the first to suggest that the vitamin could provide a simple and cheap way of cutting the risk of dementia, which affects 700,000 in Britain. 

If the link was confirmed, adults could be given free supplements from middle age, said researcher Dr Iain Lang. 

'You can supply vitamin D very cheaply  -  a few pence a day,' he said. 

'Given the growing burden of care associated with dementia, even if it reduced 10 per cent of dementia, it would make a massive difference. 

'The amount that's contained in a regular multi-vitamin tablet is fine.' 

Dr Lang, of the Peninsula Medical School in Exeter, measured vitamin D in the blood of almost 2,000 men and women aged 65 and over. 

The volunteers were also set a test designed to assess mental decline linked to a raised risk of dementia. 

Those with the lowest levels were more than twice as likely to have problems with memory and attention, than those with the highest levels, the Journal of Geriatric Psychology and Neurology reports. 

Although vitamin D in the body largely comes from sunlight exposure, our ability to generate it decreases with age. 

Dr Lang added: 'For those of us who live in countries where there are dark winters without much sunlight, like the UK, getting enough vitamin D can be a real problem  -  particularly for older people, who absorb less vitamin D from sunlight.' 

It is thought the vitamin may work by triggering the production of new brain cells and protecting existing ones from damage. 

As there is no evidence that it repairs the damage done by dementia, men and women would benefit most by taking a supplement from middle age to ward off the condition, said the researchers. 


