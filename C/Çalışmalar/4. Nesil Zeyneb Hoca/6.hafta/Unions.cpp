/**
 * @author Yusuf Anı
 * @email yusufani8@gmail.com
 * @create date 2018-11-05 13:04:32
 * @modify date 2018-11-05 13:04:32
 * @desc [description]
*/


#include <stdio.h>
  typedef struct {
      unsigned int day :5;
      unsigned int month :4;
      unsigned int year :11;
  } DATE;
  typedef struct 
  {
      char ps_name[20],ps_tcno[11];
      DATE ps_birth_date;
      unsigned int TcCitizen :1;
      union {
            char nationality[20];
            char city_of_birth[20];        
         } location;
  }PERSONALSTAT;
  int main() {
      int j ;
      unsigned int val;
      PERSONALSTAT ps2[2];
      for(j = 0;j < 2;j++)
      {
          printf("Lutfen %d. kisinin adı ve soyadını giriniz:",j+1);
          scanf("%s",ps2[j].ps_name);
          printf("Lutfen %d. Tc Kimlik nosunu giriniz:",j+1);
          scanf("%s",ps2[j].ps_tcno);
          printf("Lutfen %d. kisinin dogun gununu giriniz:",j+1);
          scanf("%u",&val); ps2[j].ps_birth_date.day=val;
          printf("Lutfen %d. kisinin dogum ayini giriniz:",j+1);
          scanf("%u",&val); ps2[j].ps_birth_date.month=val;
          printf("Lutfen %d. kisinin dogum yilini giriniz:",j+1);
          scanf("%u",&val); ps2[j].ps_birth_date.year=val;
          printf("Eger Tc vatandasi ise 1 giriniz degilse 0");
          scanf("%u",&val); ps2[j].TcCitizen=val;
          if(val==1){
              printf("LUtfen dogum yerini giriniz:");
              scanf("%s",ps2[j].location.city_of_birth);
          }
          else {
              printf("Lutfen kisinin dogum yerini ulke olarak giriniz:");
              scanf("%s",ps2[j].location.nationality);
          }
        
      }
      for(j = 0;j < 2;j++)
      {
       printf("%d.kisi :%s\nTC:%s\nDogum tarihi:%u/%u/%u\n",j+1,ps2[j].ps_name,ps2[j].ps_tcno,ps2[j].ps_birth_date.day,  ps2[j].ps_birth_date.month,ps2[j].ps_birth_date.year);
      if (ps2[j].TcCitizen==1) printf("Dogdugu sehir: %s",ps2[j].location.city_of_birth);
      else printf("Dogdugu ulke: %s",ps2[j].location.nationality);
      printf("\n");
      }
    
          
      
}
