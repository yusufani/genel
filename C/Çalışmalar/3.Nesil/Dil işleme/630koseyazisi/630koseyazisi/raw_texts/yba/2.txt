O kayna�� be�enmedim 
T�RK TV'lerden d�n (�nceki) gece Bo�azi�i K�pr�s� ile ilgili haberi izlerken, bir �ey dikkatimi �ekti. 

Ta��y�c� halat papucunun kaynat�lmas� s�ras�nda kaynak tekni�inin yanl�� yap�ld���na kanaat getirdim. ��nk� yap�lan kayna��n, daha �nce yap�lan kaynak koptu�una g�re yanl�� yap�ld��� inanc�nday�m. Konunun i�indeki bir uzman olarak halen �al��makta oldu�um Azerbaycan Bak�'den d���ncelerimi aktarmak zorunda kald�m.

Kaynak yapan kaynak��, gayet rahat bir sanayi �ar��s� mant��� ile kayna�� yapmaktayd�. Bu kadar y�ksek mahalde so�uk ve r�zgar alt�nda kayna�a uygun �n �s�tma yap�lmal� ve de kaynaktan sonra sar�larak gerilme giderici i�lem olmal�yd�. ��in �nemlisi Bo�azi�i K�pr�s�'ndeki bu ta��y�c� halatlar, 1970 y�l�nda kal�n ve �zel bile�imdeki �elikten yap�lm��t�r. �lkin ar�ivlerden bu malzeme ile ilgili kaynak prosed�r�ne bak�l�p nas�l kaynak yap�laca�� ve hangi elektrod ile yap�laca�� tespit edilmeli idi. 34 y�l ge�ti�ine g�re muhtemelen bu kayna�� yapacak veya g�ren bir kimsenin Karayollar� kadrosunda kald���n� d���nemiyorum.

Kayna��n kopmu� olmas� sadece yanl�� kaynak metodundan kaynaklanmam��t�r. Ayr�ca g�r�nen manzaradan kayna��n tamamen etraf� kapal� bir ortamda hi� r�zgar almadan yap�lmas� hayatidir. Kaynak sonras�, �atlaklar�n tespiti i�in ger�ek ultrasonic muayenenin yap�lm�� olmas� da gerekirdi. Esas pani�e kap�n�lmamal�; Karayollar� kadrosunda bu i�i cesaretle ve bilgisine g�venerek yapt�racak bir uzman yoksa derhal firman�n yap�c�s� �ngiliz firmas�ndan uzman getirtilerek kayna��n yap�lm�� olmas� gerekirdi.

Muhtemelen bu kayna�� yapan kaynak��n�n ayn� malzeme ve kal�nl�k i�in yap�lm�� olan AWWA veya LYDO sertifikas� da bulunmamaktad�r.

Umuyoruz ki, Karayollar�'n�n da kendi bak�m kadrosunda bu teknolojiyi bilen bir grup mevcuttur. Bence bundan sonra hi�bir kaynak yapma te�ebb�s�nde bulunmadan i�in ehline yapt�r�lmas� gerekmektedir.

Bu uyar� ve g�r���m� yap�lan kayna��n tekrar k�r�lmas� ve g�r�nt�deki manzara kar��s�nda yap�yorum. Kald� ki kaynak prosed�r�n�n elektro ak yerine daha iyi sonu� veren MIG kaynak metodu ile olmas� da buradan olas� g�z�kmektedir.

Aslan �ZMEN-Tekfen �n�aat ve Tesisat A�/Bak�-CWP Platform Projesi Direkt�r�.

G�rtuna�n�n ilgin� anketi 

MEDYAYA �nceki ak�am �zeri '�stanbul B�y�k�ehir Belediyesi Ba�kanl��� Se�imleri Ara�t�rmas�' ba�l�kl� 27 sayfal�k bir anket ge�ildi. 12, 13 ve 14 Ocak'ta 52 denek �zerinde ger�ekle�tirilen ankette 18 soru sorulmu�. Ara�t�rman�n sonu�lar� AKP Bilim Kurulu Ba�kan� Prof. Kemal G�rmez'in denetiminde uzmanlar taraf�ndan da de�erlendirilmi�. Ara�t�rmaya g�re G�rtuna, AKP'den aday olmas� halinde AKP B�y�k�ehir'i kesin olarak kazan�yor. AKP se�meninin %88'i G�rtuna'ya s�cak bak�yor. G�rtuna, di�er partilerden aday olmas� halinde AKP'den %10-15 oran�nda oy kapar�yor. CHP'den aday olmas� halinde ise CHP'nin oyu 17'den 30'lara ��k�yor. AKP'ye kar�� olu�acak blokta DYP, ANAP, GP ve hatta MHP'den bir k�s�m oyun CHP'ye kaymas�na yol a�abilir, bu �artlarda da AKP B�y�k�ehir se�imlerini alamayabilir.

G�rtuna, AKP'ye son �a�r�s�n� yap�yor:Beni aday yapmak zorundas�n�z!' Ama AKP kendisine kap�lar� a�m�yor.

Memurlar feryatta

MERS�N'den emekli memur M�zrap G�k:Ge�mi� iktidarlar, bayramdan �nce maa�lar�m�z� veriyorlard�. Bayram� nas�l ge�irece�iz? Peki nas�l kurban kesece�iz? Torunlar�m�za hediye alamayacak m�y�z?'

AHMET Ekinci: H�k�met 1.800.000, aileleriyle 5 milyonu bulan emekli sand��� emeklisini unuttu. Memurlar 2003 y�l�nda kaybetti de, emekliler kaybetmedi mi? Memurlara verilen 160 milyonu emekliye vermediler. Sebebi ise emeklilerin art���n�n g�stergeden dolay� %6'dan fazla olmas� olarak g�sterildi. Ge�en d�nem �al��anlar�n aile yard�m� ve �ocuk paralar�n� art�rd�klar� zaman emekliye ne verdiler?

Ne yaz�k ki, emeklinin sahibi yok, Allah'tan ba�ka...''

Biliyor musunuz?

AKP Kemer Belediye ba�kan aday aday� S�leyman Yorulmaz'�n, se�ilmesi durumunda il�eye kilise yapt�raca��n� s�yledi�ini... DEHAP, Diyarbak�r Ba�lar beldesinden aday aday� olan �anakkaleli antropolog Yurdusever �zs�kmenler'in, adayl���na �a��ranlara kar��Ben T�rk�m, ancak T�rk ve K�rt halklar�n�n kaderlerinin ortak oldu�una inan�yorum' dedi�ini... AKP'den �stanbul Selimpa�a Belediye Ba�kan aday� g�sterilen (eski DYP ve ANAP'l�) S�leyman Ya�c�o�lu'nun bu adayl���na kar�� beldedekilerin, ge�mi�te �ok yak�n oldu�u S�leyman Demirel ve Kadir Has'�n ne d���nd�klerini merak ettiklerini... DSP'li Mustafa Y�lmaz'�n bakanl���nda, Genel M�d�rl�k �al��anlar�n�n oylamas�yla K�y Hizmetleri Genel M�d�rl���'ne getirilen, T�rkiye'deki t�m k�pr�lerde damgas� olan ve AKP d�neminde g�revden al�nan H�seyin Alio�lu (Arakl�) ile eski Turizm M�d�r� Volkan Canalio�lu'nun (Ma�kal�) CHP Trabzon Belediye Ba�kan adayl��� i�in yar��acaklar�n�... CHP'den Bodrum Belediye Ba�kan adayl�klar�nda Mazlum A�an (Belediye Ba�kan�), Osman �ne�, Musa G�kbel (eski milletvekili), Mehmet Kocair, Dr. Kerim Cang�r, Mehmet �zal�n ve Veysel Yap�c�'n�n; Milas'ta da Fevzi Topuz (Ba�kan), H�seyin Kurtulu� ve Ahmet �evikkol'un (CHP Kad�k�y Meclis �yesi) �eki�eceklerini... AKP Adana B�y�k�ehir aday adaylar�ndan Zafer Kara'n�n kamu ihalelerinden yasakl� oldu�unun ortaya ��kt���n�... �ZM�T AKP'den aday aday� olan �stanbul B�y�k�ehir Belediyesi K�lt�r Dairesi Ba�kan� Cengiz �zdemir'in�stanbul'da k�lt�r alan�nda yapt�klar�m�z ortada; ba�kan olursam �zmit'i d�nya kentine ta��yacak 41 projemizi hemen devreye sokaca��m' dedi�ini... Ba�bakan Tayyip Erdo�an'�n, Eski�ehir Belediye Ba�kan� Prof. Y�lmaz B�y�ker�en'in DSP'den yeniden aday olmas� �zerine, AKP �rg�t�ne kuvvetli bir aday bulunmas� talimat�n� verdi�ini... 

Biliyor musunuz?

A��klama

BAKIRK�Y Belediye Ba�kan� Ahmet Bahad�rl�, �zg�rl�k Meydan�'na AKP'nin �ad�r kurmas� �zerine ��kan olaylarla ilgili olarak'Bu meydan, ana arter say�ld��� i�in B�y�k�ehir'indir. �ad�r kurma iznini B�y�k�ehir'in verdi�ini, Kaymakaml���n da onaylad���n� ��rendik. Biz de, Cumhuriyet Bayram� etkinliklerimiz i�in B�y�k�ehir'den izin al�yoruz. Bunun d���nda bizim yapaca��m�z bir �ey yok.' dedi.

G�N�N S�Z�

��(Avrupa) K�rt sorununun ��z�m�n� istemezler. K�rt sorununu T�rkiye'nin ba��nda Demokles'in k�l�c� gibi sallarlar. 100 bin K�rt'e pasaport verdiler, bana vermediler. Niye, ��nk� oyunu bozaca��m� biliyorlard�. Avrupa T�rkiye'yi de kullan�yor.'

(Abdullah �calan) 
