THANK YOU �Cashley�. You are the most despised player in English football, but you have done the game a great service.

Ashley Cole, already loathed for bleating when Arsenal offered him �only� �50,000 a week, has been especially detested since he was accused of cheating on pop star wife Cheryl. Talk about not appreciating when you are well off.

And there was an act of wanton disdain by Cole on the pitch last season. In March, at Tottenham, the Chelsea player turned his back on referee Mike Riley � and provoked a backlash.

His contempt came just when the Football Association were launching a campaign to restore respect for refs. There could not have been a better timed or more blatant example of the behaviour they wanted to exorcise.

The Premier League, Football League, Managers� Association, the players� union and referees� representatives all joined the FA in talks during the summer. 

With unprecedented unison, they backed the �Respect� campaign.

A raft of new procedures was announced yesterday and starts at professional matches this weekend. A related set of measures will be introduced into the lower reaches of football�s pyramid. Great. But the abuse of referees does not start or finish with the insolence of wretches like Cole.

He is just a symptom of a corrosive climate of criticism of referees from parks to the Premier League which has 
infected every football internet message board, every terrace and stand, every pub discussion.

  

It has been caused by the facet of football which makes it the most popular sport on the planet � the passion it generates.

We are all in favour of respecting  referees � until one of them gives a decision against our team.

Let me give you an example. There are T-shirts on sale in Poland with slogans which threaten to kill English referee Howard Webb because he gave a penalty against the Poles in Euro 2008. That is outrageous, isn�t it?

But England fans behaved equally appallingly when Swiss official Urs Meier disallowed Sol Campbell�s goal against Portugal in the Euro quarter-final four years earlier.

And if you think to yourself, �Yes, but Meier was wrong�, you are part of the problem. Until we can respect referees, even when they give a decision with which we disagree and which hurts our  team, we can�t demand that others show respect.

What is really needed is a new attitude. But that is not up to the FA. That is up to you.

You decide what sort of game you want football to be.

You decide whether the Ashley Coles win.

