Bir g�n bile kalamaz...  

BA�BAKAN Tayyip Erdo�an kendi eliyle M�ste�arl��a getirdi�i �mer Din�er hakk�nda nihayet konu�tu.

Ba�bakan, gazeteci Yavuz Donat'a, ���mer Din�er'i g�revden almay� d���nmedi�ini�� s�ylemi�.

Bununla kalmam��. M�ste�ar�n�n ��d�r�st�� oldu�unu belirtmi�. ���retkendir�� demi�. ��Ona inan�yorum, ona g�veniyorum. Onunla �al��maya devam edece�im. T�rkiye bir hukuk devleti... Yan�mda �al��an arkada�lar�m�n hukukunu kimseye �i�netmem�� diye ilave etmi�.

Sonra kendini tutamam��. �mer Din�er'in 9 sene evvel yapt��� (ve halen ayn� g�r��leri savundu�unu s�yledi�i) konu�man�n ��9 y�l ge�tikten sonra... Ba�bakanl�k M�ste�ar� olduktan sonra... Kamu Y�netim Reformu �zerinde �al���rken�� ortaya ��kar�lmas�n�n ���irkin bir yakla��m tarz��� oldu�unu s�ylemi�.

Galiba konunun as�l �nemli noktas� i�te bu... Yani Ba�bakan'�n ��inand���, �retken oldu�unu g�rd���, d�r�st buldu�u�� bir insan� kendisine M�ste�ar olarak tayin etmesine kimse bir �ey s�ylemiyor. O nedenle Say�n Ba�bakan konuyu sapt�rmadan de�erlendirmeli ve o a��dan yan�t vermelidir.

�mer Din�er'in g�revden al�nmas�n� isteyenler onun ��hukukuna�� da tecav�z etmiyorlar. Tam tersine... ��Laik cumhuriyetin hukukunu�� korumak i�in Din�er'in ��M�ste�arl�k makam�nda oturmas�na�� itiraz ediyorlar.

Bir insan�n usul�ne uygun �ekilde g�reve gelmesi de, usul�ne uygun �ekilde g�revden al�nmas� da bir ��hukuka tecav�z�� olay� de�ildir.

Sapt�rmayal�m... Ortada laik T�rkiye Cumhuriyeti diye bir yap� var. Tayyip Erdo�an isimli T�rk vatanda��na ba�bakanl�k unvan� veren bir yap�... Belli bir felsefesi, kurallar�, organlar� olan bir yap�... ��te bu yap� diyor ki: ��Bana b�rokrat s�fat�yla hizmet edecek olanlardan ben devlet felsefeme ba�l� olmalar�n� isterim.��

Bu devletten g�rev isteyen herkes i�in ge�erli bir ko�ul. Ona sahip de�ilseniz yani bu devletin temel ilkelerine inanm�yorsan�z, kamu g�revi alamazs�n�z. En az�ndan icrai bir konumda olamazs�n�z. 

Say�n M�ste�ar �mer Din�er, 1995 tarihli konu�mas�na halen ba�l� oldu�unu s�yl�yor. Halen de savundu�u o konu�mada, T�rkiye'deki cumhuriyeti de o cumhuriyetin temel ilkelerinden biri (bizce en �nemlisi) olan laikli�i de art�k gereksiz g�rd���n�, ��T�rkiye'de daha M�sl�man bir devlet yap�s� olu�turmak gerekti�ini�� hi�bir teredd�de yer b�rakmayacak �ekilde s�yl�yor. 

Dahas�, bu y�nde ba�ar�l� olmak i�in ��b�rokratlar� de�i�tirmenin yeterli olmayaca��n�, devlet felsefesinin de de�i�mesi gerekti�ini�� savunuyor.

Devlet felsefesi dedi�i, Say�n Ba�bakan'�n dedi�i gibi ��Kamu Y�netimi Reformu�� de�il. Do�ruca ��Devleti M�sl�manla�t�rmay��� kastediyor. Bu g�r��leri savunan bir m�ste�ar�n o makamda oturamayaca�� i�te bu nedenle isteniyor. 

Say�n Erdo�an e�er ��Referans�m�z �slam'd�r�� �eklindeki o pek bilinen eski g�r���nde �srarl� ise, do�rudur, �mer Din�er bu g�r��e �ok uygun bir m�ste�ard�r. Ama Erdo�an sahiden de�i�ti ise bu su��st� yakalanm�� haldeki m�ste�ar� orada bir g�n bile tutmamas� gerekir.
 
