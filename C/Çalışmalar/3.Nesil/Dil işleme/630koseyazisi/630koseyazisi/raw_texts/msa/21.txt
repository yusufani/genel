Kadrolu doktoru da Ecevit'i g�remiyor  
     

BA�BAKAN B�lent Ecevit ve e�i Rah�an Ecevit'in on y�ld�r aile doktoru. �ki y�la yak�n zamand�r da Ba�bakanl���n kadrolu doktorlu�unu yap�yor. 

Bug�ne kadar kendisi i�in s�ylenen c�mle hep ��yle oldu: 

��Kendi v�cudunun durumunu dahi Ecevit'in v�cudu kadar bilmez...��

Dr. Arif Abac�, Ecevit'in sa�l�k durumu konu�uldu�unda susan, Hipokrat yeminine sad�k kalan kara kutu olarak bilinir. 

Yurti�i ve yurtd��� gezilerde Ecevit'i bir g�n olsun yaln�z b�rakmad�.

Sadece bunlar de�il, son 7 y�ld�r Ecevit ailesinin t�m tetkiklerine bizzat nezaret eden ki�iydi. 

Hindistan gezisi d�n��� Ecevit rahats�zland���nda, �al��t��� �ankaya Hastanesi'ne gizlice g�t�r�p tetkiklerini ger�ekle�tirdi. 

O d�nemde g�nde �� kez Ecevit'i evinde ziyaretine tan�kl�k ettik. 

Hatta, o d�nemde Ba�bakan'�n evinde bir hem�ire bile bekledi.

DR. ABACI NEREDE?

Ecevit'in son rahats�zl���ndan bu yana Dr. Abac� hi� g�r�lm�yor, ad� da duyulmuyor.

Haf�zam�z� zorlad�k, Ba�bakan'�n evinin �n�nde g�nlerdir n�bet tutan arkada�larla da konu�tuk. 

��Dr. Abac�, son g�nlerde Ecevit'i evinde ziyaret etti mi?��

Ald���m�z yan�t ayn� oldu; ��Hay�r, gelmedi...�� 

Ba�bakan �nceki g�n Sedat Ergin ile yapt��� g�r��mede de bunu do�ruluyordu.

Sadece y�llard�r doktorlu�unu yapan Abac� ile de�il, son rahats�zl���nda tedavisini �stlenen Prof. Turgut Zileli ile y�z y�ze g�r��medi�ini a��kl�yordu.

NEDEN ABACI DE��L

�imdi �u sorulara yan�t bulmak gerekiyor;

Ecevit'i bu kadar yak�ndan tan�yan, t�m rahats�zl�klar�nda yan�nda bulunan ve tetkiklerine bizzat nezaret eden doktoru Arif Abac� nerede?

G�nlerdir evinde dinlenmesine neden olan son rahats�zl���nda Abac� neden Ecevit'in yan�nda de�il? 

Bunlar bir yana, en k���k bir olayda dahi doktorlar hastan�n sa�l�k hik�yesine ihtiya� duyar. 

Ba�bakan i�in bu gerekmiyor muydu? 

Sorulara yan�t bulmak i�in Dr. Abac�'y� arad�k.

Sekreteri, Abac�'n�n Ba�bakan'�n sa�l�k durumu ile ilgili olarak �teden beri devam eden tavr�n� korudu�unu, konu�mayaca��n� s�ylemekle yetindi.

Oysa, Ba�bakanl���n kadrolu doktoru olarak g�revine devam ediyordu. 

Bu sorular�m�za yan�t yine Ba�bakanl�k koridorlar�ndan geldi: 

��Bunlara neden Say�n Abac� de�il...��

Daha da ilginci Ba�bakan'�n rahats�zlan�p hastaneye kald�r�ld���n� Abac� s�radan bir vatanda� gibi televizyon ve radyo haberlerinden ��renmi�. 

Ecevit'e ula�mak istemi�, engelle kar��la�m��. 

Y�llard�r doktorlu�unu yapt��� Ecevit'in son rahats�zl���nda uygulanan tedavi ve tetkikler konusunda da bilgi sahibi edilmemi�. 

Tabii, ba�ta da s�yledi�imiz gibi bunlar Ba�bakanl�k'ta konu�ulanlar... 

HAKKI VAR MI?

Sadece T�rkiye'ye de�il, d�nya siyasi tarihine damgas�n� vurmu� bir �ahsiyet olarak B�lent Ecevit'in, hatta Rah�an Ecevit'in sa�l��� herkesi ilgilendirir. 

Kayg� duyulacak bir durum olmasa da, en k���k bir rahats�zl���nda dahi doktor kontrol�n� ret yoluna gidemez. 

Evinde dinlenip kendi ba��na iyile�me y�ntemini de uygulayamaz. 

Daha da �tesi, hi�bir doktor Ba�bakan'�n tedavisini telefonla yapamaz.

 

