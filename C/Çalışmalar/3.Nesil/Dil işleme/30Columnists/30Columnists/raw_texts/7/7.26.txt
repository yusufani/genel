Friends have been teasing me this week about the Guardian's series on how Labour might retrieve its fortunes and win the next election. "Tell us again about Labour's fourth election win, Mike," they say. OK. Ha ha. It's not quite what the series suggested or what I picked up at a breakfast seminar I attended at No 10.

On the Guardian's political podcast this week, my colleague Will Woodward said he felt Gordon Brown's chances were about one in five, which is four to one against if I have done the odds correctly. That feels about right, as I have said here before. The odds on any party winning a fourth term are low; for a Brown-led regime in a deepening recession, they must be lower still.

But John Major pulled it off in 1992 at a time when no seismic shift in the political landscape could be detected, and no great enthusiasm for the opposition either. That's what I picked up � to my slight surprise � when talking both to Labour activists and (more importantly) to voters in the street during a visit to Leeds. It isn't over yet as ministers also argued during this week's series.

So what did I pick up from the 8 to 9am breakfast session at No 10 where thinktankers, academics, party officials and a sprinkling of hacks came to talk � in my case to listen � to brainy people explaining how the "progressive consensus" might regain the initiative?

The way these events are arranged is under what are called "Chatham House rules" after the venerable foreign policy thinktank, which opened in 1921. It means you can report what people said, but not who said it. That allows for free exchange without individuals having to pick their words too carefully.

Ed Miliband chaired the session, Brown spoke briefly at the start � repeating some of what he'd said in Tuesday's Guardian interview � and left halfway through to resume his daily grind.

On his "Me, me, me" blog, clever Matthew Taylor, the ex-Downing Street brain now running the RSA, sums up several thematic points that he extracted from remarks made, including his own.

Thus: this is an important time; things will not be the same again; public culture matters as much as policy-making; the Brits are stubborn in their dislike of both the state and the markets; jargon-laden government policy pronouncements about empowerment and reform leave people cold. Etc.

My notes are more anecdotal, include several jokes and a remark attributed to Rahm Emanuel, Barack Obama's chief of staff: "You should never let a crisis go to waste." I may also be the last person to hear this recession joke: what is the capital of Iceland? About one dollar.

One recurring warning was that the economic crisis should not be used as an excuse to return to the old, statist ways of doing business. "A centralising approach will be as big a mistake as protectionism," observed one speaker. And "this is a market failure, not the market failing". True, I think. The institutions which screwed up most were heavily regulated and the money they loaned so rashly was to borrowers like us.

So the excesses of markets ought to be reined in and better regulation enforced, but � said a university type � "the worst of all worlds would be if we went back to the traditional state or dampened down markets too much". Laissez-faire and the profession of economics has failed us: the lesson is to do better.

But the state remains the central player in tackling global economic disorder or climate change � the two biggest challenges � via strengthened international institutions or making sure public services, many devolved to non-state agencies, perform better. "We must make the state smarter, not necessarily bigger or smaller."

The watchword is "improvement", not "reform", someone emphasised. A columnist who had recently attended a similar Tory event on the "post-bureaucratic society" said things had gone really well until someone asked: "Who decides where the money goes?"

In next to no time they had appointed "30,000 bureaucrats" to do the job.

Historically, economic crises have not been good for the left. One of the lessons of the past 15 years is not to let the traditional right regain control of key electoral issues like law and order, migration and sovereignty. "We must continue to pioneer solutions."

Much of this sort of chat sounds a bit like airy-scary skating across surfaces, yes? It's bound to be so at events like this, 30 or so mostly-big egos around a table. Labour should not abandon liberalism or the search for greater pluralism and diversity. It needs an "injection of republican spirit" � equality � said someone else.

Culture in the broad sense matters. A moral vocabulary is part of it. People also need a vision of where society is going after the crisis passes, as it will � like the Beveridge report on the future welfare state in 1942. Personalised social services � even budgets � can be good, but there are risks. An MP recalled a constituent saying: "The local chiropody service has been abolished, I can't get my feet done, and you call it 'empowerment'."

Good point: and there is always a shortage of volunteers to be school governors.

The session, apparently the first of a series being organised over the coming weeks, formed no firm conclusions, but left the same impression as the Guardian's series: the government is gripped by the imperatives of recession like everyone else, but it isn't brain dead yet.

As we stepped into the sunshine one participant muttered: "They're all wall-to-wall Fabians, so reluctant to loosen their grip on power." Well, yes. But it's negative thinking. All smart modern governments try to devolve their quite unmanageable powers, but find it hard to do so. Margaret Thatcher did and so � if he gets his chance � will young Dave.
