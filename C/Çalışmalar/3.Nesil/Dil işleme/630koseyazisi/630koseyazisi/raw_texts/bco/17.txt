Mercedes�le cennete 
AKP�nin pe�ine tak�lmak sadece �cennet-i �l�ya gitmeye yaram�yor. Ayn� zamanda mezara da Mercedes ile gitmeyi sa�l�yor. 

Ba�bakan��n Alman otomotivcilere �Art�k cenaze arabalar�m�z Mercedes marka� demesinden sonra, i�lerin ne kadar yolunda oldu�unu daha iyi anl�yoruz. 

Misal vatanda� �ld�.

Kamyonet gelirdi eskiden. 

Ba�bakan �Cenaze arabalar�m�z art�k Mercedes� m�jdesini verdi�ine g�re, sonunda Mercedes�e binme �ans�na kavu�an vatanda�, ayn� zamanda ekonomik b�y�menin ve d�nyada y�ld�zla�man�n simgesi gibi bir �ey...

Kap�lar �kuupp� diye kapan�r.

Rahat ve konforlu.

��indeki rahmetli ekonominin h�l� iyi yolda gitti�inden ve T�rkiye�nin d�nyada y�ld�zla�t���ndan emin olmak i�in tabutun i�inden seslenir:

�Ne geldi?..�

�Mercedes...�

�Ka� vites?...�

�250 SEL otomatik... Ful donan�m, siyah metalik, �elik jant, orijinal d��eme, klima...�

*

Ba�bakan��n ba�ar�da �l�leri dahi Mercedes�e bindirme noktas�na gelmesi ve T�rkiye�nin d�nyada bir y�ld�z gibi parlamas� sonucudur ki, b�y�menin rekor k�rmas�na ra�men i�siz say�s�n�n niye durmadan artt���na kimsenin akl� ermiyor.

Yoksulluk s�n�r�n�n alt�ndakilerin say�s� art�yor. Sinek avlayan esnaf say�s� art�yor. �r�n�n� yakan k�yl� say�s� art�yor. ��siz say�s� art�yor.

Ama nas�l oluyorsa T�rkiye b�y�yor...

Biz asl�nda T�rkiye�nin neresinin b�y�d���n� biliyoruz; dengesiz beslenme sonucu �t� b�y�yor.

�t� (�zel Te�ebb�s�) b�y�d�k�e, yoksulluk artsa bile T�rkiye y�ld�zla��yor.

Ve sonunda vatanda� Mercedes�e biniyor.

*

�ocuklar �Mercedes geldi� diye ko�arlar.

Geride kalanlar da a�lamakta olan g�zlerinden bir tekini a��p bakarlar. Evet, b�y�yen ve d�nyada y�ld�z� parlamakta olan T�rkiye�nin i�areti olarak Mercedes kap�da.

Rahmetli Mercedes�e biner, tabutun i�inden sorar:

�Ka� yap�yor?..�

