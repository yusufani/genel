AFTER ten primary wins in a row, he's got the big Mo of momentum behind him, has taken the lead in the number of delegates, is raising more funds and is now the favourite � but has Barack Obama got it sewn up?

Not yet, but he could have it stitched up real soon. I don't know how many times I hear people saying in politics that something is inevitable. Britain joining the Euro currency? It was going to be inevitable. Why? We didn't have to, we didn't (oncADVERTISEMENTe Blair and Brown figured out that it would be unpopular) and I honestly don't think we ever will. The inevitability of politics was challenged and shown to be illusory.

Hillary Clinton will be the Democrats' nomination for President, "it's inevitable" people told me on both sides of the Atlantic. She was the strongest recognisable name, she was clearly associated with husband Bill, who, for all his personal faults is believed by Democrats at least to have brought prosperity to the States (let's forget Newt Gingrich's tax cuts on the Hill for the moment). Some people hanker after those good comforting times, pre-terrorism, pre-Iraq. Clinton's association with that nostalgic feeling could cause a warm glow in their hearts. Or so it went.

"She is seriously divisive and will cost Democrats many independent voters who will either stay at home or vote for a safe Republican" (if they could find one), I would argue. Well it's not turned out to be inevitable. 

Democrats across America have instead been warming to Obama's aspirational sound bites, his appealing rhetoric and the feeling that, in wanting to change America, there is something of the John F. Kennedy and the Martin Luther King in him.

Clinton is not yet finished though, she has concentrated on the big states of Texas, Ohio and Pennsylvania where she thinks she can make up a great deal of the lost ground. It is a risky strategy, however, as Texas and Ohio don't vote until Tuesday, March 4 and Obama is now able to work his magic on the voters that previously sided with Clinton. 

Blue collar workers, women and Hispanics prefer Hillary and there's plenty of all three in those states, but even if she wins there the fact that the Democrats use proportional representation means that growing support for Obama will give him some extra delegates to take to the convention.

And this may be Hillary Clinton's last hope � getting to the convention without being written off, without being forced to concede. For when it gets beyond the primaries and caucuses and on to the Denver convention floor, then anything can happen � for there party officials and leaders, the superdelegates, have great power.

Don't write Hillary off yet, she wasn't a quitter when Bill went walkabout, she's not going to be a quitter now.

Congestion charge
It's five years on since Ken Livingstone introduced the congestion charge in London � so what's the outcome?

Well it has cost a staggering �1.2 billion for starters. Where has it all gone, has it been a success? Well, one could ask one of Ken's 265 press assistants for the figures, but they might just prefer to avoid that question. You see the amount of cars entering the chargeable zone at rush hour has altered little while the number of casual trips during the day have slumped � which is why the retailers have suffered.

A survey found many routes are so congested the traffic now moves slower than it did before the charge � often slower than walking pace � on The Strand it was measured at less than 2mph! Car journeys into central London were already falling before Livingstone's charge was introduced � it didn't need �1.2bn in charges and fines to change peoples' behaviour. 

Thank goodness we had a referendum in Edinburgh and kicked a similar scheme into touch.

Roll up for Diana circus
George Foulkes is right, the coroner's enquiry into the death of Diana Spencer is turning into a circus. The biggest clown has to be the Harrods owner and father of co-deceased Dodi Fayed. It's sad he lost his son, but the suggestion that the British MI6 could � under the instructions of the alleged crypto-fascist Duke of Edinburgh � get the French intelligence agency to work with it and the mercenary paparazzi to assassinate her is � as Christine Hamilton described the accusation that she and her husband Neil Hamilton lured, drugged and then raped a woman � "Nonsense on Stilts".

While we're at it we might recall that it was the testimony of the same Mohammed al Fayed, and many of the staff in his pay who changed their sworn statements, that caused Neil Hamilton to lose his libel action, ruining him politically and financially. 

The veracity of that judgement now looks as realistic as a fish riding a bicycle. If only al Fayed was an MSP, he might then be investigated for perjury.
