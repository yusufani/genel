THE Woman's Support Project in Glasgow is the publicly funded successor to the Routes Out of Prostitution Project. It claims to represent the best interests of Glasgow prostitutes, and like Routes Out, aims to eliminate the city's sex trade. Routes Out failed. I fear the WSP will also.

Unfortunately, before this becomes obvious, the WSP may have persuaded this Holyrood government to apply its well-intentioned but woolly solutions to the objective of eliminating prostitution in Scotland. But before Alex Salmond's cabinet listens to ADVERTISEMENTthe spokeswomen for WSP, it should read through the evidence given by them in their various guises to the committees in the last parliament that debated kerb-crawling and other aspects of the sex trade.

Far be it from anyone with interest in the same matters to doubt the sincerity of the Routes Out employees in their mission to outlaw the Management Zones in Aberdeen and Edinburgh, but nor should it be forgotten that the record of Routes Out measured against its objective is abysmal. They have yet to produce a single figure or statistic, more than a year after Routes Out was asked how many women worked as prostitutes at the start of Routes Out's �1 million-plus grant, compared to the number at the date, years later, when they gave evidence to the Local Government Committee.

In contrast, Edinburgh's Scotpep, operating on a fraction of the money paid to Routes Out, produced annual figures, broken down by numbers of women supported, counselled and helped by Scotpep year on year, the numbers estimated to have switched to working indoors after the ending of the Management or Tolerance Zone, the numbers travelling to Aberdeen to work, etc. Scotpep could also produce numbers on the huge rise in gratuitous violence against sex workers by the men who bought sex.

Scotpep, like Routes Out/WSP, supported the idea of buyers and sellers of sex being treated equally if the law on paid-for sex were to be broken. The difference lay in what each thought the law should be. Scotpep wanted the new law to prevent nuisance, inconvenience or alarm to third parties unconnected to the attempts to buy or sell sex, while Routes Out wanted to take a step towards ending prostitution � a worthy, if imprecise aim, guaranteed to produce, as in Sweden, only a period with streets empty of sex-workers before some drift back to their old haunts. 

But this is what the WSP is now pressing on the Holyrood government � the Swedish solution to street prostitution. 

The women running the WSP would never accept the impossibility of imposing a uniform judgement of what constitutes illegal paid-for sex, nor that some women choose to sell or barter sexual services, which indicates that their approach will be no more successful than the Swedes if their aim is to eliminate the sex trade.

They're likely only to drive the trade underground, and out of reach of social workers and police, thus putting women at greater risk and depriving police of a conduit of information on other areas of criminality, like the supply of drugs.

But this time the WSP has statistics culled from a sample of 110 men who answered such searching questions as "Would you stop buying sex from prostitutes if your name was put on a sex register?" Unsurprisingly, considering the modest sample of largely self-selecting respondents, over 80 per cent said yes.

Whilst not denying the effort made by WSP to prove the case for outlawing the purchase of sex, the Scottish Government would be wise to solicit an independent assessment of whether the number of sex workers in Sweden has declined, and by how much, and how many now work indoors, before accepting the sometimes very confused interpretations of the results in this limited survey.

Details please, Kenny
Although endorsing the idea, I'm interested in the details of Justice Minister Kenny MacAskill's plan to give victims of serious crime the right to address the person found guilty, and the court that convicted him or her. 

The idea would be for the victim's statement to be given after the jury's decision, and before sentence is handed down. It is not intended as a guide to the judge's decision as regards the length or conditions of the sentence, but how will judges react, if at all, to public criticism of sentences that appear to be at odds with victims' statements?

Time to mend oil link
Some readers may recall the phrase "the commanding heights" of the economy and some may share my opinion that the Grangemouth dispute indicates it would be no bad thing to have the public interest represented on the Board of Ineos, at least. If ever an industry could be described as central to both community life and the economy, it's the energy industry.

Prime Minister Thatcher and her heirs broke the link between the oil industry and the state in contrast to Norway, where the publicly Statoil company is an influential part of the energy industry.

Whether or not such a set-up would have been able to get both sides round the table, I don't know � rightly or wrongly, the company chairman reminds me of an archetypical old-fashioned boss who wants victory over the union rather than a negotiated settlement. 
