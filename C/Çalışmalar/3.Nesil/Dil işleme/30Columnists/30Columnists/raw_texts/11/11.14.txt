Britain has the worst cancer survival record in Western Europe - and the figures are much worse than previously thought. 
Despite Health Service funding tripling under Labour, survival rates are on a par with Poland and the Czech Republic, even though they spend two-thirds less on cancer. 
A damning league table shows that Britain is 16th out of 19 countries surveyed. Patients in some European countries are 15 per cent more likely to be alive five years after diagnosis. 
The figures come from the hugely respected Eurocare-4 study, which compared the five-year survival rates of 2.7million adult patients up to 2004.
Provisional results released in 2007 showed Britain was lagging behind the rest of Western Europe. But the final calculations, published today in the European Journal of Cancer, show the situation is even worse than feared. 
Just 41.4 per cent of men and 51.4 per cent of women found to have cancer survive longer than five years after diagnosis - down on the 42 per cent and 53 per cent figures previously reported. 
Experts blame NHS waste, drug rationing and a lack of cancer specialists for the shameful showing. 
The study also found that the number of patients who are completely cured is much lower in the UK than in all of Western Europe and even the former Yugoslav republic of Slovenia. 

For example, 12.4 per cent of English patients are cured of stomach cancer compared to 13.7 per cent in Slovenia. 
The time people live after being told they have terminal cancer is the lowest in any of the countries studied. Terminal stomach cancer patients live for an average of months after diagnosis in Britain, compared to one year in Slovenia, and one year and one month in France. 
The massive differences in European survival rates come despite the Government tripling NHS spending since coming to power in 1997.
Much of the money was ploughed into cancer - last year �4.5billion was spent on cancer treatment - with the aim of bringing Britain up to the European average of five-year survival rates by 2010.
The UK spends twice as much per head of population on cancer as countries which are higher on the survival league table, such as Portugal, and three times as much as countries such as Slovenia.

Professor Karol Sikora, of London's Imperial College, said: 'The only countries that are below us on the list are eastern European countries such as Poland which have not long emerged from behind the Berlin Wall. 
'These countries spend far less on cancer treatment than we do, but they have put the money into patient care and radiotherapy. 
'We have spent much more but we have increased the bureaucracy around cancer, and still have staff shortages in radiotherapy and other technical areas. 
'In five years' time our record will be even worse because we're not accessing the drugs we ought, and we don't have the fancy radiotherapy that Europe can get hold of.' 
Harpal Kumar, chief executive of Cancer Research UK, said that as many as 5,000 extra deaths occur every year in the UK compared with the rest of Europe, largely because people were not diagnosed early enough and found it harder than in other countries to access specialists. 
He said: 'We need to reduce radiotherapy waiting times, invest in the training and specialisation of surgeons, drive recruitment to clinical trials and increase the UK's cancer drug spend up to the average of Western European countries.' 
Michael Summers, of the Patients' Association, said: 'Patients will be losing confidence, and the result is that patients will die who would survive if they were born across the Channel. That is unacceptable.' 
National cancer director Professor Mike Richards said: 'Cancer survival rates in England have been steadily improving. However, we accept that further improvements can be made especially when our survival rates are compared with the rest of Europe.' 
Millions of women in their twenties could benefit from cervical cancer jabs routinely given to teenage girls, a study suggests. 
Vaccines prevent up to 70 per cent of cases and are given to hundreds of thousands of girls aged 12 to 18 each year. 
Now research has found the Gardasil jab could also benefit women in their mid-twenties. 
A study of almost 18,000 women aged 16 to 26 found the jab worked well, cutting the number of abnormal smear tests and the need for follow-up examinations. 
With cervical cancer the second most common cancer in young women, the study - funded by Gardasil - has led to calls for the national vaccination programme to be extended to women in their early twenties. 
It would cost up to �800million to vaccinate all women aged 19 to 26, but the Department of Health said its advisers had concluded it would not be cost-effective.
