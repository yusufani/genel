Ask mothers why babies are constantly picking things up from the floor or ground and putting them in their mouths, and chances are they�ll say that it�s instinctive � that that�s how babies explore the world. But why the mouth, when sight, hearing, touch and even scent are far better at identifying things?

When my young sons were exploring the streets of Brooklyn, I couldn�t help but wonder how good crushed rock or dried dog droppings could taste when delicious mashed potatoes were routinely rejected.

Since all instinctive behaviors have an evolutionary advantage or they would not have been retained for millions of years, chances are that this one too has helped us survive as a species. And, indeed, accumulating evidence strongly suggests that eating dirt is good for you.

In studies of what is called the hygiene hypothesis, researchers are concluding that organisms like the millions of bacteria, viruses and especially worms that enter the body along with �dirt� spur the development of a healthy immune system. Several continuing studies suggest that worms may help to redirect an immune system that has gone awry and resulted in autoimmune disorders, allergies and asthma.

These studies, along with epidemiological observations, seem to explain why immune system disorders like multiple sclerosis, Type 1 diabetes, inflammatory bowel disease, asthma and allergies have risen significantly in the United States and other developed countries.


�What a child is doing when he puts things in his mouth is allowing his immune response to explore his environment,� Mary Ruebush, a microbiology and immunology instructor, wrote in her new book, �Why Dirt Is Good� (Kaplan). �Not only does this allow for �practice� of immune responses, which will be necessary for protection, but it also plays a critical role in teaching the immature immune response what is best ignored.�

One leading researcher, Dr. Joel V. Weinstock, the director of gastroenterology and hepatology at Tufts Medical Center in Boston, said in an interview that the immune system at birth �is like an unprogrammed computer. It needs instruction.�

He said that public health measures like cleaning up contaminated water and food have saved the lives of countless children, but they �also eliminated exposure to many organisms that are probably good for us.� 

�Children raised in an ultraclean environment,� he added, �are not being exposed to organisms that help them develop appropriate immune regulatory circuits.�

Studies he has conducted with Dr. David Elliott, a gastroenterologist and immunologist at the University of Iowa, indicate that intestinal worms, which have been all but eliminated in developed countries, are �likely to be the biggest player� in regulating the immune system to respond appropriately, Dr. Elliott said in an interview. He added that bacterial and viral infections seem to influence the immune system in the same way, but not as forcefully. 

Most worms are harmless, especially in well-nourished people, Dr. Weinstock said. 

�There are very few diseases that people get from worms,� he said. �Humans have adapted to the presence of most of them.�

