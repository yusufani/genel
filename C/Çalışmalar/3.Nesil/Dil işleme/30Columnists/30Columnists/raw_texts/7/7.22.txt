Was the governor of the Bank of England out of order in telling the government to be "cautious" about creating further fiscal stimuli to float the economy off the rocks of recession? No, that's part of his job. Will Gordon Brown be cross when aides tell him what Mervyn King told the House of Lords economic committee? Almost certainly; he's not a chap who likes to be told what to do.

The intriguing question is why the cautious King spoke about caution incautiously. At one point during his testimony I think I hear him say that words are the only weapons a central bank governor has. Not true, of course; he has � or had until recently � the power to vary interest rates, and history may judge that he failed to raise them soon enough to prick the financial bubble (and to drop them soon enough to deflect the recession).

But his job is to deliver inflation at 2% � neither higher nor lower, unlike the European Central Bank � and to help ensure a suitable economic environment for stable growth. So King is entitled to speak out, even if the governor's language is often more opaque than this:

Given how big those deficits are, I think it would be sensible to be cautious about going further in using discretionary measures to expand the size of those deficits.

He did not rule out "targeted and selected" measures on specific areas such as unemployment, which hit 2 million last week and could top 3 million by the end of the year. But he added: "The fiscal position of the UK is not one which says, 'Let's just go on another significant round of fiscal expansion,'" the Press Association reports.

Did he do it to assist the chancellor, Alistair Darling, in resisting pressure from the hyperactive ex-chancellor next door at No 10 to reflate more than the �20bn he pumped into the falling economy in his November pre-budget report? I don't know, but it seems a reasonable speculation. Angela Merkel in Berlin is saying much the same thing.

Darling recently hinted that a second round of pump-priming might be unwise � especially, I imagine, since the Bank has pumped �75bn worth of spending power � quantitative easing � into the economy in the past fortnight or so.

Scary times, so it is reassuring to see Mervyn King conducting a semi-public dispute in old-fashioned gentlemanly terms. Let's hope Brown responds in similarly humble spirit.
