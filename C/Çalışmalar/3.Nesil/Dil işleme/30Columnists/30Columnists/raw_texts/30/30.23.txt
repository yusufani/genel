Meltdowns aren't all bad. The current one in financial markets is "correcting" the prices for awful art. 

A Roy Lichtenstein bad joke, for example, went up for auction at Sotheby's in New York the other day. And nobody bid. Such a silence is not golden in the world of high esthetics, where the beauty of rising prices is felt as deeply as among homebuilders, stockbrokers and pork belly traders. 

But it's a macroeconomic sign that might mean common sense is returning among the (somewhat less) rich. 

They show reduced interest in buying pretentious nonsense for their walls and foyers. Maybe they still want some money for important things. And for when the economy climbs out of recession.

Art auctions seem to be paralleling the stock market. They aren't bringing in the megabucks of the recent past. 

Oh sure, if you call $125.1 million for 63 pieces of contemporary art megabucks. But Sotheby's thought this mess of painting, sculptures and who-knows-what would attract $202 million to $280 million in total bids. And it didn't. The bubble in dubious art has gone pop, perhaps. But as with the stock market and the housing market, nobody's sure it's a "bottom" yet. 

Take the Lichtenstein, titled "Half Face with Collar, 1963." The pre-auction estimate of likely offers in person and by telephone was $15 million to $20 million. But going once, going twice, going three times, nothing. 

What could have upset the forecast? Here was what experts called an "archetypal" piece of pop-art in the style of enlarged comic strip draftsmanship. It showed the bottom part of a man's face while he's in the act of tugging at his shirt collar. Yet nobody bid the price of a mere million shares of General Electric.

Timidity was rife the next evening at the rival Christie's. Seventy-five lots were sold for $113 million. But this was only half the presale low estimate of $227 million. Bloomberg News said nearly a third of the offered works failed to find buyers, including a self-portrait by the Irish painter Francis Bacon. His work drew "ooh, ahs" and puzzled stares but not the $40 million to $60 million anticipated. Back to the drawing board, Frankie! 

A masterpiece of daring monotony called "Archisponge" by Frenchman Yves Klein did sell for $21.3 million at Sotheby's, however. It depicts a dozen-odd sponges stuck to a pale purple canvas. The robot-like "Untitled (Boxer)" of Jean-Michel Basquiat knocked some buyer out for $13.5 million at Christie's. 

Richard Fuld, ex-CEO of bankrupt Lehman Brothers, got raked over the coals at a congressional hearing last month, but sold 16 drawings for $13.5 million. They'd been expect to draw $15 million, true, but they did yield better than Lehman shares.

An expert told Bloomberg that the art world is currently down with the same distemper as Wall Street: a great likelihood of much shrunken bonuses to pay for uh, er, non-essentials.

