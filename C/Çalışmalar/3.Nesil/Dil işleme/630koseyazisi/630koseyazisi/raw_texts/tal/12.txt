Cavcav haks�z m�? 

 
 

Gen�lerbirli�i ve Kul�pler Birli�i Ba�kan� �lhan Cavcav'�n da endi�eyle, biraz da �z�lerek s�yledi�i gibi "kaos" kap�dad�r... �lgili birimler "Kulu�kadaki tavuk" gibi olay�n �zerine oturmu�, "g�daklay�p" durmaktad�r. �sterseniz, olaylar� Cavcav ile "y�zle�tirerek" biraz anlamaya �al��al�m.

Cavcav diyor ki, "Kul�pler birli�i, naklen yay�n gelirlerinin hakkaniyet �l��lerinde da��t�lmas�n� g�ndeme getirdi�i an, 'mal�n kayma��n� yemeye al��m��' 4 B�y�kler, birdenbire sarma� dola� oldular. Daha d�ne kadar, �z�r dileyerek s�yl�yorum, birbirlerine neredeyse ana avrat k�f�r edenler, sarma� dola�, kul�pler birli�ine cephe ald�lar."

Yanl�� m�?... Hay�r do�rudur.

Cavcav olaya biraz daha a��kl�k getiriyor; "Bilindi�i gibi naklen yay�n hakk� gelirinin %50'sini 4 b�y�k kul�p kendi aralar�nda payla��yordu, geri kalan %50 de 14 Anadolu kul�b�ne da��t�l�yordu. �imdi bunu rakamlarla ifade edelim; �rne�in biz (G.Birli�i), ve ya Gaziantep, ya da bir ba�kas� ligi 2'nci, ya da 3'�nc� s�rada bitirmi� olal�m, bu hi� farketmez. 4 b�y�klerden birisi isterse ligi 10 tak�m�n alt�nda bitirsin, bizim alaca��m�z pay 4.5 milyon dolar, 10 tak�m�n alt�nda ligi bitiren 4 b�y�klerden birinin pay� ise yine 18 milyon dolard�r...

Bu hak m�d�r, adalet midir? B�yle bir gasp say�lacak payla��mla Anadolu kul�pleri nas�l rekabetin i�inde olabilir?"

�imdi herkes elini vicdan�na koyup kendi kendine sormal�d�r:

"�lhan Cavcav haks�z m�d�r?"

Reyting y�ksek
B�y�klerin "Bizim seyircimiz fazla, dolay�s�yla reytingimiz de fazla oldu�u i�in elbette naklen yay�n gelirinde pay�m�z da fazla olacak" iddialar�n� da Cavcav "haks�z" bulmuyor, hatta onayl�yor... Fakat burada da onun itiraz� var: 'Taraftar say�s�nda �o�unluk elbette onlarda.. Senede Galatasaray'la Fenerbah�e 2 ma� oynarsa "ezeli rekabet" afi�leriyle, reytingi tabii ki tavana vurduruyorlar... Ama �st �ste 3 hafta, 4 hafta birbirleriyle oynas�nlar bakal�m; ma�lar�ndaki seyirci say�s� ka�a d���yor, g�relim...

Yine eller vicdanlara uzans�n; soral�m:

Cavcav'�n hakl� yan� yok mudur?

"Sonu� ne olacak?" diye sordu�umuzda ise �lhan Cavcav ka�amaks�z, net olarak �unu s�yl�yor:

"Herkes palavray� b�raks�n bir kenara. Kimse kimseye bl�f�n� yediremez... Ne 4 b�y�kler bizsiz olabilir, ne de biz onlars�z olabiliriz. Yani "Ligden �ekiliriz" tehditleri palavra... O zaman tek nokta kal�yor ortada; hak�a payla��m... Bunun etraf�nda toplan�p el s�k��maktan ba�ka ��kar bir yol yoktur."

�lhan Cavcav'�n bu konuda federasyona da bir �ift s�z� var; "Kenardan k��eden bulacaklar� 3-5 kuru�la bizlerin, yani 14 Anadolu kul�b�n�n gelirinde iyile�tirmeye gideceklerini s�yl�yorlar. Bir defa daha sesimizi net duysunlar, biz kimseden sadaka istemiyoruz! �stedi�imiz, naklen yay�n gelirlerinin t�m kul�pler aras�nda hak�a b�l�nmesini sa�lamalar�. Hak eden fazla als�n, g�z�m�z yok. Ama hak etmiyorsa da almas�n..."

�leri derecede "�lhan Cavcav alerjisine" sahip olanlar bile, onu bu konuda "haks�z" bulmasalar da "Hakk�n ucunu" hep kendilerinden yana yonttuklar� i�in olay "kaosa" d�n��mektedir. Federasyon bunu, �stten bir yerlerden alaca�� g��le muhakkak ��zecektir.

G�n�l istiyor ki "mazlumun ah�n�" vicdanlar� rahats�z edecek "feryada" d�n��t�rmeden ��zs�n. 

 
