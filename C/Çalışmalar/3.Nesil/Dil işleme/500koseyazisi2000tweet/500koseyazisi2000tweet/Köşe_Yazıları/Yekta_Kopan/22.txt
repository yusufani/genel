﻿Yangın çıkarma hastalığı

"Ülke yangın yerine döndü" bir klişedir. Bu klişenin, sıklıkla kelime anlamıyla birebir karşılığını bulduğu bir ülkede yaşamak istemeyenler, ateşi söndürüp ülkeyi güzelleştirmek isteyenler var. O yangına körükle gitmek istemeyenler var. "Yangını kimin çıkardığını gördüm," diyecek kadar cesur olanlar var.
Birçok defa istekli ve kasıtlı olarak yangın çıkarmak, bir davranış bozukluğu.
Bu davranış-dürtü bozukluğuna PİROMANİ deniyor.
Pyros, Yunanca bir sözcük. Ateş demek.
Hafif bir tanımlamayla şöyle diyebiliriz: Yangın çıkarma hastalığı.
Konuyu öğrenmeye, dpsikiyatri.com adresinden aldığım bilgilerle devam edelim.
Piromaninin esas özellikleri birden fazla durumda düşünülmüş ve amaçlı olarak ateşe verme, ateşe vermeden önce gerilim ya da duygusal uyarılma, ateş ve itfaiyecilikle ilgili malzemelere hayranlık duyma, ateş yakmaktan ya da sonrasında tanıklık etmek veya katılmaktan zevk alma, rahatlama hissetmedir. Hastalar ateşi başlatmadan önce ciddi hazırlıklar yapabilirler.
Üç gün önce, Hürriyet gazetesi binasına yönelen öfke, bir twitter hesabında “Madımak gibi cayır cayır yakacağız” cümlesiyle karşılığını bulmuş. Tüyler ürpertici.
Bina önünde Abdurrahim Boynukalın “Seni başkan yaptıracağız,” diye haykırıyor. Ustalarına öykünen bir tavırla ve ‘üçleme’ yaparak.
Aynı dakikalarda milletvekilinden gazetecisine, trolünden takma isimlisine sosyal medya hesapları bu ‘yangına körükle gidiyor’, ‘fitili ateşliyor’.
Ertesi gün “Ülke yangın yerine döndü” klişesiyle çıkıyor gazeteler.
Ailesiyle Kürtçe konuşan 21 yaşındaki Sedat Akbaş bıçaklanarak öldürülüyor.
Cizre’de sokağa çıkma yasağı yüzünden bir anne, eve isabet eden bomba parçalarıyla hayatını kaybeden 13 yaşındaki kızının cesedini, evdeki derin dondurucuda bekletmek zorunda kalıyor.
Diyarbakır seferi yapan bir otobüsü durdurup ‘kimlik kontrolü’ yapmak isteyen göstericiler çıkıyor ortaya. Otobüsün camları taşlanıyor.
Ellerinde sopalarla gelen bir grup Kamp Armen’i basıyor.
Sırf Kürt oldukları için mevsimlik işçiler dövülüyor, yakılıyor.
Sosyal medyada örgütlenmeler başlıyor. HDP binaları basılıyor. Binalardan sokağa atılan eşyalar, parti bayrakları-tabelaları yakılıyor. Alevlerin fotoğraflarını çekiyorlar akıllı telefonlarıyla, sosyal medyada paylaşıyorlar anında.
Dağlıca saldırısının acısı bütün ülkeyi sarmışken, bu acıdan bir ‘öfke yangını’ çıkarmaya kararlılar. Teröre lanet okumak yetmiyor yangın yeri görmek isteyenlere. Barış cümlesi kuran bir anneye, “Senin de evladın ölsün,” diyecek kadar taş yürekli olabiliyorlar.
Siyaset hala koltuk sayısı derdinde, seçmen konsolide etme derdinde, küçücük çocukların üstünden oy toplama derdinde, karakter tahlili yapma derdinde, sünnet üstünden ırkçılık derdinde. Yangın büyüyor.
Piromaniyi öğrenmeye devam edelim:
Bu hastalık erkeklerde kadınlara göre çok daha fazladır. Bu kişilerin öyküsünde (geçmişinde) okuldan kaçma, evden kaçma ve görev ihmal gibi anti-sosyal eğilimler bulunabilir.
Piroman kişiler sıklıkla çevreden çıkan yangınlarda sıradan izleyicilerdir, sık sık gerçek dışı alarm verirler ve yangın söndürme gayretlerine ilgi gösterirler. Meraklılık çok açıktır. Yangının sebep olduğu mal ve can kaybına ilgisiz olabilirler ve suçluluk duymazlar.
Yangın çıkarıcılar sonuçtaki yıkımdan doyum sağlayabilirler. Sıklıkla açık ipuçları bırakırlar. Beraberinde sıklıkla görülen özellikler arasında alkol intoksikasyonu, seksüel disfonksiyon, ortalamanın altında zekâ seviyesi, kronik kişisel örselenmeler ve otorite figürlerine kırgınlık bulunur.
Evet, “Ülke yangın yerine döndü” bir klişedir. Bu klişenin, sıklıkla kelime anlamıyla birebir karşılığını bulduğu bir ülkede yaşamak istemeyenler, ateşi söndürüp ülkeyi güzelleştirmek isteyenler var. O yangının tanığı olmak istemeyenler, o yangına körükle gitmek istemeyenler var. “Yangını kimin çıkardığını gördüm,” diyecek kadar cesur olanlar var.
“Barış, hemen şimdi!” diyenlere saldırılan günlerde, korkmadan barış istemeye devam edenler var. Kaybedilen her bir can için gözyaşı döken ve bir can daha kaybedilmesin diye yangını söndürmek isteyenler var.
Yangın çıkarma hastalığına yakalanmış kitleye, bu alevlerin ancak barışla söneceğini anlatmak isteyenler var.
Şimdi nerede durduğumuza karar verme zamanı: Piromani mi, barış mı?
Barış. Hemen şimdi!