Barack Obama and Hillary Clinton held their political bonding ceremony in the middle of a field since the tiny town where they did the deed had no buildings big enough to host such a momentous occasion. The symbolism was obviously supposed to stretch way, way beyond mere unity. Think the signing of the Magna Carta. Or that baseball movie with Kevin Costner. If you concede it, they will come.

�Today I could not be happier and more honored and more moved,� said Obama, as Clinton stood next to him, nodding intently. She looked very cheerful, considering that she was living out the nightmare that haunted her toward the end of the race � standing on the side of the stage and bobbing her head earnestly, while the guy made his speech.

Oh, the ironies. Hillary lost the nomination because many Democrats looked at her and saw a candidate who lacked a principled core, who was prepared to do almost anything to get elected president. They liked Barack better because he was a change agent.

Then just as she was engrossed in her multiple concessions, his campaign started using the same political shape-shifting that Obama had decried in the Clintons. He attacked the Supreme Court�s anti-death-penalty ruling so that he wouldn�t look soft on sexual assailants the way that Michael Dukakis did. He welcomed the court�s anti-gun-control ruling so that he wouldn�t lose the hunters� vote like Al Gore. Public-campaign financing is for losers.

The Democrats could not care less. They want a winner, and most of them are prepared to forgive quite a lot of inconsistency in order to get one. A liberal opponent of the Senate wiretapping bill virtually wept with joy when Barack deserted the cause and voted with the law-and-order folk.

�Do you want a president ...� began one of the speakers at Unity, preparing to run off a laundry list of things Obama would do.

�Yes!� the people interrupted.

Still, it�s worrisome. You�d like to think that after 17 months of angst over its presidential nomination, the Democrats would not wind up with the exact same candidate they started out with, except for a different gender and a higher quotient of panache.

In Unity, the two stars arrived a little late, to swelling music, giving us a chance to recall that throughout the primaries he was the candidate who made a dramatic entrance, while she was often the third person from the right in a lineup of pols onstage. His powder-blue tie perfectly matched her pantsuit, a color-coordination we deeply hope was a coincidence.

Everybody watched them whispering in each other�s ear, the entwined arms. Do they really like each other? I think I speak for most of the nation when I say that on the list of things that keep us up at night, it does not make the top 1,000. It is, in fact, right down there with the fate of the Clinton campaign debt.

The citizens of Unity � or at least the ones who weren�t home with their doors locked for the day � seemed pleased with their central role in modern political history. The mayor bought a new pair of sneakers for the event, and expressed appreciation for this unanticipated �shot in the arm� for the local economy.

Unity not only has a cool name for coming-together purposes, its population had split right down the middle in the New Hampshire Democratic primary, giving 107 votes to Clinton and 107 to Obama. (As always, the John Edwards voters have faded away, unnoted.) Even the Unity residents who were too lazy to show up at the polls were crucial. If just one of them had gotten out of bed and shambled in to vote, the outcome might have been 108-107, and on Friday half the news media in the world would have been on its way to Friendly, W.Va.; Success, Mo.; or Smileyberg, Kan.

Or Celebration, Fla.; Ideal, Ga.; or Relief, Ky. We could keep this going forever, really.

The crowd here stood for hours under the hot sun, so useful for melding purposes. A few Hillary die-hards skulked around the edges. �You have to give them space,� the Obama campaign�s chief strategist, David Axelrod, consoled a supporter who had been grievously offended by the ununified behavior of the holdouts.

Take a lesson from the residents of Unity, Hillary fans. Everybody has to do their part. Sometimes it�s sleeping through Election Day so the final vote tally comes out even and your town gets to be on the evening news. Sometimes it�s trying to figure out how to get through a killer presidential campaign without losing every single quality that made people want to vote for you in the first place. And sometimes, it simply involves a lot of nodding. 

