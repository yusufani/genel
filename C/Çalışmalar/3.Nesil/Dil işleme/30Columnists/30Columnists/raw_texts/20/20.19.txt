It was by chance that I found myself in New York on the eve of the presidential election amid deepening gloom about the US and world economy. It so happened that when we planned a family break here, our minds were not on the date of the election; and, let's face it, the pound was riding a lot higher than it is today.

Fear about the economy here is palpable. One can't pick up a newspaper without reading about the dire state of General Motors, Chrysler and other household names. Bankruptcy proceedings are threatened and, after the vast sums committed to the attempted rescue of the banking system, pressure grows for very un-free-market government bail-outs for other sectors.

The first thing I heard when I turned on the radio was the excited exhortation, 'Don't go bankrupt!'. Some organisation was promising to reconstitute people's credit card debt to their putative advantage; more financial engineering to deal - or not - with the problems caused by previous excesses.

Books to re-read on this trip included the two 'Greats': The Great Gatsby and The Great Crash. Gatsby was published in 1926, an evocation of the Jazz Age that preceded the Great Depression; but The Great Crash was not published until 1954, a distance that helped JK Galbraith to provide the perspective which has ensured it has never been out of print.

It has always amused me that Nick Carraway, the narrator of The Great Gatsby, works in the bond market but does not feel the need to bore us with the technical details. He tells us that he 'studied investments and securities for a conscientious hour' each evening and that he worked for - wait for it - 'The Probity Trust'. The rest is not history, just one of the greatest novels ever.

It is not abundantly obvious that many of the practitioners in the modern financial markets devoted a conscientious hour to studying the dubious 'investments' that nearly brought Western capitalism to its knees. Neither can they have devoted much time - between partying and sidelining more cautious colleagues, who questioned the capitulation of 'banking' to 'marketing' - on The Great Crash in 1929.

It is all there, only in the Twenties the equivalent of the sub-prime fiasco was the 'Florida land boom'. Well before Alan Greenspan coined the term 'irrational exuberance', Galbraith warned of 'exhilarant optimism'. And, of course, as the markets have discovered, 'leverage [piling mountains of debt on a slender capital base in a bull market] works both ways... Geometric series are equally dramatic in reverse'.

One of Christopher's Dow's conclusions in Major Recessions is 'the bigger the boom, the bigger the bust'. The recent gyrations of the stock market have demonstrated nervousness, fear and even despair about what lies ahead. And there will shortly be an emergency 'global finance summit' in Washington which is, somewhat adventurously, being compared to a 'New Bretton Woods' after the seminal meeting in the Mount Washington Hotel in Bretton Woods, New Hampshire, in July 1944.

Now, the present world economic crisis - for that is what it is - is 'multi-causal', to put it mildly. 'Irrational exuberance' and 'exhilarant optimism' have interacted with asset-price inflation, with the impact of low-cost China on the world's economy, with huge gyrations in exchange rates and the price of oil, to produce a highly unstable world economy. Countries such as the US and UK have been living beyond their means. Chinese savings have helped to finance the US binge, while China, with an undervalued exchange rate, has relied heavily on exports.

So-called 'globalisation' has fostered an interdependence that means Asia has not, as some analysts used to argue, 'decoupled' from the West. So what is China's reaction to the threat of recession in the West? It is to sit on its undervalued exchange rate and to introduce government subsidies for exports.

If any attempt at a 'Bretton Woods II' is to be efficacious, participants would do well to note that the fundamental concerns of the original meeting are in danger of returning.

The Bretton Woods agreement was essentially about stabilising exchange rates and eventual 'convertibility' of currencies that had been subject to strict wartime controls. As the British economist Sir Alec Cairncross wrote on the 50th anniversary of Bretton Woods: 'The Americans' anxiety to establish early convertibility and eliminate discrimination in trading relationships derived from their view that discrimination, trade restrictions and competitive depreciation were a potent cause of war. Britain, however, saw the source of prewar troubles in the deflationary pressure on the world economy, exerted by an American slump.' 

Keynes, our principal negotiator at Bretton Woods, feared, in his biographer Robert Skidelsky's words, that 'the market system was liable to collapse into prolonged depression'. Those fears are back; so, given China's recent moves on the exchange rate and export subsidies, are fears about protectionism.

There is an awful lot at stake. Although the imminent financial summit will be hosted by George Bush, there will soon be a huge economic weight on the shoulders of the new President. Barack Obama is manifestly more aware of the economic problem; John McCain on TV the other night could not at first even remember the name of George Shultz, the distinguished former Treasury Secretary and Secretary of State, who is backing him.

I'll believe the result when I see it. I am old enough to remember how hot favourite Thomas Dewey lost in 1948 and, more recently, how top US reporters wrote Jimmy Carter off before he won in 1976.
