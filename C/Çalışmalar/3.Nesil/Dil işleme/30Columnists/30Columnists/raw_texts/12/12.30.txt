Suddenly Rafa Ben�tez has more to worry about than Sir Alex Ferguson. Two more dropped points, for a start, and the fact that Manchester United were able to win here last month. 

The Liverpool manager's selection policy will be questioned, as he seemed to play into Stoke's hands by leaving some of his most expensive players on the bench, and although Steven Gerrard hit the bar and post in the final stages, this ponderous performance was a far cry from the heights his players reached in their last league game at Newcastle. It is possible Ben�tez will be blamed for unsettling his side with his unprovoked attack on Old Trafford.

Diego Maradona picked the coldest of days to visit the noisiest of Premier League grounds to check up on Javier Mascherano and was lucky to escape without frostbite. Liverpool's Argentine midfielder, a bit like the unusually quiet Stoke supporters, seemed a little bit subdued by the sub-zero temperatures. 

Dirk Kuyt headed wide from an excellent Alberto Riera cross in the opening minutes, before the Liverpool goal survived a scare from Rory Delap's first long throw. The initial danger was dealt with, but when the ball was swiftly returned to the box from the opposite wing Delap himself arrived to slap a shot against Pepe Reina's bar, with Richard Cresswell unable to react quickly enough when the rebound came straight to him. 

Kuyt forced a save from Thomas Sorensen after some comedy Stoke defending midway through the first half, though as half-time approached the home side were beginning to get on top. That is not to say Reina had his work cut out, but most of the play was in the Liverpool half, the visitors had several more long throws to deal with, and when Ryan Shawcross headed in from Matthew Etherington's cross they were relieved to see an offside flag raised. 

This state of affairs was surprising given that Stoke's only gameplan, apart from the long throws, was to defend until Liverpool gave the ball away and then try to catch them on the break. Liverpool were unexpectedly accommodating, both in giving the ball away on a regular basis and in leaving most of their pace and penetration on the bench in the form of Fernando Torres, Ryan Babel and Robbie Keane. Any one of that trio would have given Stoke more to think about at the back than the pedestrian front running of Kuyt and Gerrard.

Stoke could have taken the lead at the start of the second half when an attempted clearance by Reina went straight to Dave Kitson, though the goalkeeper recovered well enough to force the striker wide and he shot wide. 

Ben�tez relented and sent on Torres after an hour, just in time to see Gerrard pick up a bizarre booking for hammering a free kick into the wall while the referee was still trying to march it back. Presumably the Liverpool captain had been hoping to avoid the wall, though it was hard to see how a goal could possibly have stood when the referee had his back turned as the free kick was taken. 

The only encouraging aspect from Liverpool's point of view was that when Stoke worked a perfect opportunity from a Delap long throw in the 73rd minute, Kitson put his free header over the bar. Glenn Whelan put a free kick narrowly wide at the match entered its final 10 minutes, before Gerrard was twice denied by the frame of the goal. 

First, from wide on the left he curled a superb free-kick against the bar then, as the game entered stoppage time, he stretched to send the ball goalwards, only for it to clip the post and go wide.

