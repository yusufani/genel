Kazaya kurban gittik 

 
 

Tromso ligde sondan ikinci. K�mede kalma m�cadelesi veriyor. Kalite olarak G.Saray ile k�yaslanabilecek durumda de�il. Ancak ma� �yle hava ve saha �artlar� alt�nda oynand� ki sanki futbol ma�� de�il bir rugby m�sabakas� izledik. B�yle bir ortamda aradaki kalite fark� da ortadan kalk�yor ve iki denk tak�m�n m�cadelesine d�n���yor. ��te b�yle ma�lar �ok tehlikeli ve d�nk� kazaya kurban gidebiliyorsunuz.

Ma��n kaybedilmesinde teknik direkt�r Gerets'in de katk�s� yok de�il. Oyuna mant�kl� bir kadro ve taktik anlay��� ile ba�lad�. B�yle a��r sahada �ift �n liberoya Cihan'� al�p U�ur'u da defansa �ekmesi do�ru bir uygulamayd�. Ancak ikinci yar�da Hakan ile forveti desteklemek isterken, U�ur'u oyundan ��kartarak orta alan� zay�flatmas� da son derece riskliydi ve korkulan da oldu. 
Asl�nda G.Saray'da sahada m�cadele eden oyunculara �ok fazla ele�tiri getirmek istemiyorum. Bu �artlarda ellerinden gelen her �eyi yapmaya �al��t�lar. Hasan �a� oyunun b�y�k b�l�m�nde hem iyi m�cadele etti, hem de h�cuma iyi toplar ta��d�. Necati ve �mit Karan bu zor zeminde rakip defans� yeterince rahats�z ettiler. Necati'nin att��� �utlar, �mit Karan'�n girdi�i pozisyonlardan gol ��kmamas� ve ayr�ca net bir penalt�n�n verilmemesi G.Saray ad�na b�y�k �anss�zl�k.

Meydan sava�� verdiler
Bug�ne kadar defans�n kenarlar�n� hep ele�tirdik. Ama d�n Tomas-Song ikilisi her zaman oldu�u gibi iyi m�cadele etti. Bu defa U�ur da Orhan da iyi oynayanlar aras�na kat�ld�lar. G��l� fizi�e sahip olan Tromso'lu futbolcular topu ileri vuruyorlard�. �amura tak�lan toplan takip ediyorlar. Bu sert ve agresif m�cadelede t�m defans oyuncular� adeta bir meydan sava�� verdiler.

Heinz'�n sol aya�� ger�ekten �ok kaliteli. Ama 2. yar�n�n b�y�k b�l�m�nde �ok yoruldu ve d��ar� al�nacaklardan biri o olmal�yd�. Aynca 1. dakikada ve 2. yar�da net pozisyonlarda sol aya��yla gol vuru�u yapaca��na, asisti d���nmesi G.Saray'�n skoru lehine �evirmesine engel oldu.

Sonu�ta G.Saray 30 dereceden 0 dereceye geldi�i ve adeta bir tarlada oynad��� ma�ta iyi de m�cadele etmesine ra�men bir kaza gol�ne kurban gitti. Bu tarz ikili ma�larda 1-0 ger�ekten �ok tats�z bir skor. Ama e�er i�i �stanbul'da ciddiye al�rsa -ki mutlaka alacak- ve ayn� m�cadeleyi sergilerse Tromso'yu �ok rahat ge�er ve grup ma�lar�na kat�lmaya hak kazan�r. 

 
