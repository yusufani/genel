Fallen media mogul Conrad Black was today sentenced to six and a half years in a US jail, following his conviction earlier this year for embezzling millions of dollars from his Hollinger media empire.

Black, who arrived at court accompanied by his wife Barbara Amiel, and his daughter Alana, could have faced 24 years in prison.

Chicago judge Amy St Eve also fined former media tycoon $125,000.

Black will spend Christmas at home - he has been given 12 weeks to put his affairs in order and report to prison in March.

She had come under pressure to impose a longer term in the light of Black's lack of remorse and his repeated attacks on the US judicial system, but in the end dispensed a more lenient term.

The former Telegraph owner, who spoke only briefly in court, had repeatedly denied his guilt, dismissing the US government's case against him as "rubbish".

He enraged prosecutors by dubbing them "Nazis" and "pygmies" - comments the prosecution team collected and pinned on an office noticeboard as they renewed their calls for a harsh sentence to reflect his contempt for the courts.

Black hired a leading sentencing consultant, Jeffrey Steinback, and the result was a noticeable softening of his stance. He recently described America's justice system as "one of the 10 best in the world".

In order to prepare Black for his time behind bars, experts say his lawyers are likely to have spent time showing him websites, books and pamphlets about particular jails. They can cite a preferred destination to the judge.

He recently said that going to jail would be a "bore, but quite endurable".

With no criminal history, Black will serve his time in a low-security prison.

In determining Black's sentence, Judge St Eve will have assessed his character, remorsefulness, track record and of the size of his fraud.

He was convicted on only four of the 13 counts of fraud against him, which his defence team argued meant he was in fact answerable to fraud of "just" $6.1m (�2.98m) compared with the prosecution team's assertion that the real figure was $31m.

More than 100 acquaintances and family members wrote to the court to urge leniency for Black, including former Telegraph editors Charles Moore and Dominic Lawson, the shadow foreign secretary William Hague, former Canadian prime minister Brian Mulroney, the singer Elton John and Monsignor Fred Dolan, head of the Canadian chapter of Opus Dei.

Also being sentenced today are Black's former lieutenants Jack Boultbee, Peter Atkinson and Mark Kipnis.

The case has meant a $107.7m bill for Black's former company, Sun-Times Media, which is required to pay defence costs under an directors' indemnity clause.
