Reform yapt�m  
     

  
BEN� u�a�a bindirin, isterseniz Mars'a g�t�r�n. Ka� bin saat s�rerse s�rs�n, raz�y�m. Ama arabayla burnumun dibine g�t�rmeyin.

U�a��n daha g�venli olu�undan falan de�il. Benimki kaza korkusu de�il ��nk�. Kara yolculu�unu sevmiyorum. Can�m tez oldu�u i�indir belki. Gidece�im yere de tez gitmeliyim. �yle her bir metreyi ya�aya ya�aya, sindire sindire de�il.

Ge�en g�n Ece, ��Gel Ye�ilk�y'de bal�k yiyelim�� dedi, ��Van'a gidelim�� demi� gibi tepki g�sterdim.

��Sar�yer'e kadar gidip bir �ay i�tik, geldik�� diyorlar. Taliban'la d�v���p gelmi� asker muamelesi yapmak istiyorum. Kucaklay�p s�rtlar�n� s�vazlamak, ��Aslan�m benim�� demek...

Peki binmiyor muyum, bir yerlere gitmiyor muyum? Tabii ki gidiyorum. Ama mecbursam ki, her g�n bir mecburiyet has�l oluyor. Ama keyfi gitmeler... Asla!

�imdi d�rt g�zle �ehir i�i helikopter ta��mac�l���n� bekliyorum. Asl�nda ge� bile kal�nd�. Daha �te i�ler yap�l�yor da, bu neden olmuyor bir t�rl� anlam�yorum.

Birka� zengin villas�n�n bah�esine, i�yerinin tepesine konuyor, o kadar. E, normaldir. Refahla ba�lant�l� hi�bir geli�me benden ba�lamad� daha...

Laf� �uraya getirece�im: Bozcaada'ya geldim.

Bunun benim i�in bir reform say�labilece�ini idrak etmeniz i�in de bu uzun giri�i yapt�m.

Evet, ben, Levent'ten kalk�p Yenikap�'ya geldim, oradan feribota binip Band�rma'ya ge�tim. Band�rma'dan Geyikli'ye arabayla geldim, tekrar feribota binip Bozcaada'ya vas�l oldum. Bak�n, yazmas� bile uzun.

��Hani mecbur olmadan bir yere gitmezdin�� diyeceksiniz.

Mecburdum. ��nk� �ok moda olmu�tu Bozcaada. Art�k topluluklarda sohbet d��� kal�yordum neredeyse. Bizim bas�n camias� ve edebiyat, sinema, reklam gibi karde� camialar i�in Bozcaada eski Bodrum gibi �imdi. Yerlilerden ev al�p onarmalar, filmler, klipler, k�� ortas�nda ka�amaklar falan.

Ger�i bek�reti bozuldu b�ylece ama hen�z elden ele gezen pula d�nmedi. ��Lakin yak�nd�r�� diyerek kalkt�m geldim. �imdi Bodrum'un o eski g�nlerinin tad�n� tam ��karamad�m diye k�z�yorum ��nk� kendime.

* * *

Hakikaten g�zel Bozcaada.

Giritlili�imi ate�ledi. Ben Rumlar� seviyorum arkada�lar. S�ylemesi ay�psa, kusuruma bakmay�n. B�rak�p gittikleri evlere bak�yorum, bir de bizim yapt�klar�m�za... Ke�ke yalvar yakar buralarda tutsayd�k onlar�.

��Gelincikler a�m��t�r�� demi�ti Ece ba�tan ��karmak i�in. A�m��.

Ger�i beni ba�tan ��karacak anahtar c�mle bu de�ildi. Bana ancak ���yle sessiz ki, �� g�n uyanmadan uyuyabilirsin�� ya da ��Bir pazar kuruluyor, g�rmelisin�� veya ��Bir bilmemne pi�iriyorlar, parmaklar�n� yersin�� denilmesi isabetli olur. Ama bozmad�m onu, kendime gelinci�e tav olmu� s�s� verdim.

* * *

Burada kesiyorum.

M�saadenizle denizin, bal���n, temiz havan�n, sessizli�in tad�n� ��karaca��m. Sizler bu sat�rlar� okurken de �stanbul'a d�nm�� olaca��m.

��Bozcaada'y� iyice anlatmad�n�� diyorsan�z ben de size ��Y�ll�k iznimin bir b�l�m�n� kullanmad���ma ��kredin�� diyorum.


MI�-MU�

Meclis'te 400 dokunmatik ekran, 150 mikrofon ile 20 parmak izi okuma sistemi, sivri cisimle �izmek, kurcalamak ve s�raya vurmak sebebiyle bozulmu�.

Ayr�ca 70 milyon insan�n kemerle beli s�k�lmak, ensesine vurulup a�z�ndan lokmas� al�nmak suretiyle can�na okunmu�tur.

*

N�khet Duru, ��Erkek 5 dakikada ba�tan ��kar�l�r�� demi�.

5 dakikada da ba�ka kad�n taraf�ndan ba�tan ��kar�laca��na g�re k�saca, ��Haydan gelen huya gider�� diyebiliriz.

*

Mahsun K�rm�z�g�l, ��Sevdi�im kad�n�n kap�s�nda yatar�m�� demi�.

Kad�n�n i�eri almayaca��ndan emin yani.
 
