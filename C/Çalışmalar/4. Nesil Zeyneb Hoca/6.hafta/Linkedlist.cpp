/**
 * @author Yusuf Anı
 * @email yusufani8@gmail.com
 * @create date 2018-11-05 19:14:24
 * @modify date 2018-11-05 19:14:24
 * @desc [Linked List Training]
*/
#include<stdio.h>
#include<stdlib.h>
#include <string.h>
struct personalstat {
    char ps_name[20], ps_tcno[11];
    unsigned int ps_birth_day : 5;
    unsigned int ps_birth_month : 4;
    unsigned int ps_birth_year : 11;
    // pointer to the next element in the linked list:
    struct personalstat *next;
};
typedef struct personalstat ELEMENT;
ELEMENT *dizi_listesi_olustur() { // Sadece Eleman listesi açar ve rootunu belirler eleman eklemez.
    ELEMENT *p;
    p=(ELEMENT * ) malloc (sizeof (ELEMENT));
    if(p==NULL){
        printf("dizi listesi olusturulamadı \n");
        exit(1);
    }
    p->next=NULL;
    return p;
}

static ELEMENT *root; // Baslangıçımızın hep kalması gerekir.
void add_element (ELEMENT *e){
    ELEMENT *p;
    if (root == NULL){
        root =e;
    } 
    else {
        p=root;
        while (p->next!=NULL){
            p=p->next;
        }
        p->next =e;
    }
}

void listeye_eleman_ekleme(ELEMENT *p, ELEMENT *q){ // p listede araya koyacağımız eleman q ise p den önceki eleman olmuş olacak.
    if (p==NULL || q== NULL || q->next== p || p==q) printf("Arguman secimi kotu ");
    p->next=q->next;
    q->next =p;
}

void eleman_sil(ELEMENT *goner){
    ELEMENT *p;
    if (goner==root) root=goner->next;
    else 
        p=root;
        while(p!=NULL && p->next!=goner ){
            p=p->next;
        }
        if (p==NULL) printf("Aradıgınız eleman bulunamadı.");
        p->next=goner->next;
        free(goner);
   }
ELEMENT *eleman_ara(char *name){
    ELEMENT *p;
    p=root;
    while(p!=NULL){
        p=p->next;
    if(strcmp(p->ps_name,name)==0) return p;
    }
    return NULL;
}
int main(){
	ELEMENT *p;
	int val, j;
	for(j=0; j<2; j++)
		add_element( dizi_listesi_olustur());
	p=root;
	for(j=0; p != NULL; p=p->next){
		printf("Enter name of the %d-th person:\n", j+1);
		scanf("%s", p->ps_name);
		printf("Enter tcno of the %d-th person:\n", j+1);
		scanf("%s", p->ps_tcno);
		printf("Enter the birth-date (day) of the %d-th person:\n", j+1);
		scanf("%u", &val); p->ps_birth_day=val;
		printf("Enter the birth-date (month) of the %d-th person:\n", j+1);
		scanf("%u", &val); p->ps_birth_month=val;
		printf("Enter the birth-date (year) of the %d-th person:\n", j+1);
		scanf("%u", &val); p->ps_birth_year=val;
        j++;
	}

	for(j=0, p=root; p != NULL; p=p->next, j++) //for(p=head; p != NULL; p=p->next)
	    printf("%d-th person: %s\t%s\t%u.%u.%u\n", j+1, p->ps_name, p->ps_tcno,
	    	p->ps_birth_day, p->ps_birth_month, p->ps_birth_year);	


	// CREATE A NEW ELEMENT AND INSERT IT IN BETWEEN THE 1st AND 2nd ELEMENTS IN THE LIST:
	p=dizi_listesi_olustur();
	// ask for the info to fill in the fields of this new record to be added to the list:
 	printf("Enter name of the new person:\n");
	scanf("%s", p->ps_name);
	printf("Enter tcno of the new person:\n");
	scanf("%s", p->ps_tcno);
	printf("Enter the birth-date (day) of the new person:\n");
	("%u", &val); p->ps_birth_day=val;
	printf("Enter the birth-date (month) of the new person:\n");
	scanf("%u", &val); p->ps_birth_month=val;
	printf("Enter the birth-date (year) of the new person:\n");
	scanf("%u", &val); p->ps_birth_year=val;
    ELEMENT *q=root; // to keep the first element, head and we'll insert p, after q:
    listeye_eleman_ekleme(p, q);
	for(j=0, p=root; p != NULL; p=p->next, j++)
	    printf("%d-th person: %s\t%s\t%u.%u.%u\n", j+1, p->ps_name, p->ps_tcno,
	    	p->ps_birth_day, p->ps_birth_month, p->ps_birth_year);	
    char * ara="yusuf";
    eleman_ara(ara);

    return 0;
}