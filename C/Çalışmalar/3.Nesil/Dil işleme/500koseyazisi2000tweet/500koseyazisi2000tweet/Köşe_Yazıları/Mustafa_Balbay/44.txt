﻿Dört yüzlülük!
Dağlıca’dan gelen acı haberlerle yüreğimiz bir kez daha dağlandı. 
Tablo akla 1990’lı yılları getiriyor. Terörün bütün acımasızlığıyla aralıksız sürdüğü günlerdi. Öyle ki; gazetelerin haber merkezlerinde, akşam saatlerinde günün terör olaylarını toplamakla görevli arkadaşlarımız vardı. Neredeyse tüm gazetelerin ortak tavrı şuydu; şehit sayısı 5’in altında ise haber birinci sayfadan tek sütun giriyordu, 10’un üzerinde ise büyüyordu. Zaman zaman daha yüksek rakamlar yaşanır ve “ne oluyoruz”, “nereye gidiyoruz” soruları yükselirdi. 
Bugün de ne yazık ki benzer durumu yaşıyoruz. Hatta daha kötü bir tablo söz konusu. Zira siyasal iradenin toplumsal bütünlük sağlamada daha zayıf kaldığı, bir başka deyimle partisel çıkarların ülkesel çıkarların üstünde göründüğü, bu yöndeki kuşkuların arttığı bir sürecin içindeyiz. 
Düne kadar, “devletin tek hâkimi benim, milli irediyi ben temsil ediyorum, benim dışımda hiçbir kurum açıklama yapamaz” diyen AKP, Dağlıca gerçeğini topluma açıklayamadı. Cumhurbaşkanı’ndan Başbakan’a hepsi “TSK açıklama yapacak” deyip, çekildi.

***

TSK, beklenen açıklamayı akşam saatlerinde yaptı. O ana dek, şehit rakamları birbirini kovalıyordu. Resmi açıklamaya göre 16 askerimiz şehit olmuştu. Böylesi olaylarda çok iyi bilinir ki, en tehlikeli yayın organı fısıltı gazetesidir. Hem tirajı çabuk yükselir hem de etkisini ölçmek güçleşir. Dün de öyle oldu. 
Ateş düştüğü yeri yakar derler ama, şehit haberlerinde öyle değil. Gerçekten tüm Türkiye’yi yakıyor. Böylesine acı günlerden geçiyoruz. Acının tam ortasında Cumhurbaşkanı’nın, 400 vekil verilseydi bunların olmayacağını söylemesi, kafalarda zaten kıpırdayıp duran kuşkuları daha da derinleştirdi. Daha önce de Sağlık Bakanı benzer bir imada bulunmuştu. 
Tüm ülkeyi yasa boğan acılarda, yöneticilerin ayrı bir sorumluluğu vardır. Herkes gibi davranamazlar... Sıradan yurttaşın kaygılarını gidermek, güven vermek, acıların aşılacağını inancını, umudunu diri tutmakla sorumludurlar. Bizdeki durumu, toplumun, Dağlıca’da ne oldu sorusuna yanıt aradığı saatlerde Cumhurbaşkanı özetledi!

***

7 Haziran seçimlerinin AKP’nin tek başına iktidarına son vermesiyle birlikte Erdoğan’ın çizdiği rotayı daha önce su sütunlarda paylaşmıştık. 
İlk günlerin şaşkınlığı geçtikten sonra 4 ayaklı bir plan görünüyordu. HDP’nin barajın altına çekilmesi, CHP’nin çözümsüz ilan edilmesi, MHP tabanının oyulması, devamında seçimin yenilenerek AKP’nin tek başına iktidar olması... 
Son iki aylık sürece baktığımızda bunların tümünün denendiğini görüyoruz. 
Dağlıca gibi büyük bir acıyı yaşadığımız şu günlerde, 1 Kasım planına girmeyi elbette istemezdik. Ancak Erdoğan’ın böyle bir gecede bile 400’ü aklından çıkarmaması, şu soruyu sorduruyor:
Bu işin kaç yüzü var? 
Bütün bunlardan öte gün, akan kanı durdurmak için her şeyi yapma günüdür. Yukarıda vurduladıklarımız, kimsenin âlemi sersem milleti kör sanmaması içindir. Parlamentonun çalışmasına soğuk bakan, bu kurumu fiilen işlevsizleştiren AKP, 7 Haziran sonuçları itibarıyla tek başına iktidar da değildir. Bu bağlamda devlet kurumlarını sağlıklı çalıştırmak, topluma güven vermek, ben yaptım oldu anlayışını terk etmek zorundadır. 
En tepedekinden hükümetin küçük büyük tüm ortaklarına kadar herkes bilmeli ki; Türkiye, AKP’den büyüktür.