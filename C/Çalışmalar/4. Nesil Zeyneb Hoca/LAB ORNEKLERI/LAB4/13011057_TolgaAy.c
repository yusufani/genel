#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char marka[20],model[20],yil[6],plaka[20];
}ARABA;

void arabaEkle(FILE* file){
    file = fopen("a.txt", "a+");
    if(!file)
        return -1;

    ARABA tempcar;
    printf("\nMarka : ");
    scanf("%s", tempcar.marka);
    printf("\nModel : ");
    scanf("%s", tempcar.model);
    printf("\nYil : ");
    scanf("%s", tempcar.yil);
    printf("\nPlaka : ");
    scanf("%s", tempcar.plaka);

    fputs(tempcar.marka, file);
    fputc(' ', file);
    fputs(tempcar.model, file);
    fputc(' ', file);
    fputs(tempcar.yil, file);
    fputc(' ', file);
    fputs(tempcar.plaka, file);
    fputs("\n", file);
    fclose(file);
}

void arabalariGoruntule(FILE* file){
    file = fopen("a.txt", "r");
    if(!file)
        return -1;

    char buffer[256];
    printf("\n\n\n                 ---ARACLAR---\n");
    while(fgets(buffer, 256, file)){
        printf("Marka : ");
        int i = 0, att = 0;
        while(buffer[i]){
            if(buffer[i] == ' ')
                switch(att){
                    case 0:
                        printf("    Model : ");
                        att++;
                        break;
                    case 1:
                        printf("    Yil : ");
                        att++;
                        break;
                    case 2:
                        printf("    Plaka : ");
                        att++;
                        break;
                }
            else
                printf("%c", buffer[i]);
            i++;
        }
    }
    fclose(file);
}

void printMenu(){
    printf("\n\n\nMenu:");
    printf("\n1-)Yeni Araba Ekleme");
    printf("\n2-)Arabalari Goruntule");
    printf("\n3-)Quit");
    printf("\n\nSelection : ");
}

int main(){
    FILE* fi;

    int secenek;
    do{
        printMenu();
        scanf("%d", &secenek);
        switch(secenek){
        case 1:
            arabaEkle(fi);
            break;
        case 2:
            arabalariGoruntule(fi);
            break;
        }
    }while(secenek != 3);
}
