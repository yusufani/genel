My friend Gail is allergic to cats. Yet she is devoted to the two adorable Siamese that share her home, and she chooses to tolerate her chronic sinus congestion and cough rather than give them up. She is not alone. Fully 75 percent of people advised by allergists to give up their pets refuse.

Can Gail and her allergic compatriots learn to live more healthfully with their pets?

Yes, says Shirlee Kalstone, author of "Allergic to Pets? The Breakthrough Guide to Living With the Animals You Love" (Bantam Dell, $7.99). The potential audience for her message is large indeed. There are more than 150 million pet owners in the United States alone, and 1 in 10 of them is allergic to the animals they love. 

Ms. Kalstone insists that "forgoing pet ownership or giving up a pet should be the last step an allergic person must take, not the first."

And while not everyone will be willing to adopt Ms. Kalstone's many suggestions, there are clearly steps that can help most pet-allergic people learn to live more comfortably with their cats, dogs, rabbits, birds, gerbils, hamsters, horses or what-have-you. Still, not everyone will find sufficient relief from allergic symptoms. 

If you don't want to be bothered, consider this: the only "safe" pets for the allergic are fish, frogs, toads, turtles, lizards, snakes and most insects  fun, perhaps, but not exactly cuddly.

Sources of Misery

Every animal that has hair, fur or feathers is capable of causing allergic reactions in people sensitive to their allergens  proteins in saliva, urine, secretions from oil glands in the skin, and dander, the tiny dead skin particles these animals constantly shed. 

These allergens can be dispersed directly into the air or carried on the hair, fur or feathers the animals shed. Pet allergens can then be deposited on clothing, furniture, bedding, rugs, shoes, curtains, window blinds and even walls. And they can be carried throughout a home via heating and air-conditioning ducts. Even after a pet is gone and the house has been thoroughly cleaned, allergens can remain imbedded in carpeting and furniture.

Allergies can develop over time with repeated exposures, so even if a person did not react when the pet first came into the house, there's no telling what may happen later. Scientists at Johns Hopkins have found that exposure to pets increases the risk of asthmalike symptoms in older children.

On the other hand, sometimes repeated low-grade contact with pet allergens results, happily, in desensitization, just as can happen after years of allergy shots. There is no such thing as a dog or cat that does not cause allergies, although some breeds stir up more problems than others. All dogs and cats shed, but those with double coats  like springer spaniels, collies, German shepherds and Samoyeds  shed more than others.

Among breeds deemed less allergenic are poodles, bichons frisιs, Maltese and Portuguese water dogs, which have soft, silky or curly single coats (no undercoats). Light-colored cats tend to be less allergenic than those with dark coats, Ms. Kalstone reports. Yet even a hairless sphynx cat can cause symptoms. And there are individual differences within breeds: a Mayo Clinic study found that some cats shed 100 times as much allergen as others of the same breed.

Some birds, too, cause more problems than others, especially the "powder down" ones  cockatiels, cockatoos, African grays and pigeons  that produce large amounts of white powdery dust every day.

The most important step in minimizing pet allergies is containment. Keep the pet out of the bedroom and, if at all possible, off the furniture people use. Get the dog or cat its own couch or floor cushion with a washable cover. Keep birds, rabbits and rodents in their cages. Never let the pet sleep with the allergic person.

If possible, confine the animal to just a few rooms without carpets, rugs or upholstered furniture. In the car, crate the pet or keep it on a washable cover with a nonporous underside.

The second most important step is frequent cleaning  of your pet, its quarters and your home  preferably by someone who is not allergic. A dog or cat should be washed every week, or at least cleaned with a pet allergy relief solution like Allerpet or AllerFree. Birds, too, enjoy a bath. Haven't you ever watched one splashing about in a puddle or pond? Even large birds will enjoy a light shower with a mist sprayer that can dampen allergens.
