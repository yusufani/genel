K���kken "Bayram gelmese ke�ke" derdim

Her �ocuk bayram� sever. Eskiden de b�yleydi, bundan b�yle de herhalde b�yle olacak. Ben istisnayd�m dersem yad�rgamay�n, inan�n olmaz m�? 

S�ker al�rd� 
�ocuklu�umda bayram gelmesi yaln�zl�k anlam�nayd� benim i�in. Anam babam 
sahne sanat�yla i�tigal ettikleri i�in bayram seyran y�lba�� filan g�nleri s�ker al�rd� onlar� �stanbul'dan. 

Duymak isteyene 
Gider uzun s�reler gelmez, gelemezlerdi. 
��ime art�k nas�l i�lemi�se bir g�n �iir tad�nda bir �yk� yazd�m. Ad�n� "�ark�c�n�n o�lu" koydum.
Duymak ister misiniz?.. �ark�c�n�n o�lu 
Anne!
Sen yoktun ya... 
Hani uzar giderdi ya turne aylar�... 
Sensiz koyard� ya �ark�lar...
�ark�larda sen hasreti s�ylerdin. 
Bir �ocuk hasreti bir ba��na �ekerdi ya
bir �sk�dar s���na��nda... 
Ne bileyim,
Bir defas�nda belki bir ak�am �st�,
Birinde bir ku�luk vakti belki...
D���nde olsun, daha fazla ya�atabilmek i�in seni,
Mahallede bir tek o �ocuk; 
'annesinin �l�s�n� �pmezdi' 
yemin ederken...
Yat�l� mekteplerin ac�mas�z h�km�
ge�mezken hafta sonu �ocuklar�na...
Bir tek o sensizli�e yenik ve esir ya�ard�
pazar� ve cumartesiyi de. 
�imdi k�f�rbazl��a d�n�yorsa dili;
Sevgi s�zc�kleri yerine,
Soka��n dilini emdi�indendir ana s�t� yerine. 
K�fr� sevgi gibi, sevda gibi, �zlem gibi, kavu�mak gibi belleyip, bileyip, bildi�indendir yani; 
terbiyesizli�ine verme... 
Peki ben �imdi
�imdi ben asl�nda, kimin g�zleriyim anne?
Elim, tenim, sa� telimde sakl� t�ls�mlar�n buhar� genzimi yakarken,
Kimin kuyular�ndan �ekilmi� sularla beslenir ak�tt���m gizli g�z ya�lar�? �ocuktum 
Elbette kanayacakt� di�im, dizlerim, yerlere d��t���mde 
Beni en �ok senin g�rmedi�in yerlerde ve yerler yerine 
sensizli�e d��t�m anne! 
En �ok yoklu�unda a�lad�m.
En �ok hasretinde kanad�m...
�imdi ben �ok uzak bir ge�mi�in kuyular�na d�� kovalar�m� dald�r�rken, 
Oyunlar, oyuncaklar yerine en �ok kulisleri an�ms�yorum anne; 
�ocuk uykular�m� �alan h�rs�z darbukalar�, g�r�lt�c� kemanlar�, 
F�rd�nd�s�z, tombalas�z, portakals�z ge�en y�lba�lar�n� hat�rl�yorum anne... 
Her bayram sabah�, seninkiler yerine ellerini �pt���m, yaln�zl���n dev analar�n�, Har�l�ks�zl��a de�il, sensizli�e sitem edip i� �ekti�im anlar�, 
Ba�tan a�a�� pekiyi karnelerimi ilk sana g�steremedi�imi an�ms�yorum... 
Bir de
Bir de �ark�lar�n g�zeldi anne! 
�yleyse �imdi bi kere de, i�inde hasretin zerresi bile ge�meyen o �ark�n�, Bu kez '�ark�c�n�n O�lu' i�in s�yle... 

Az �tede 
��te b�yle dostlar. Bayram g�nleri gelince hep eskiye, uzak ge�mi�ime dal�p gidiyorum hala. Haaa bu arada, g�zel anac���ma, " benim i�in oku" dedi�im �ark�y� merak eden vard�r belki.
Onu da az �teye koyu siyah harflerle 
yazd�m dedim. 
