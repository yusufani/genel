NO-ONE ever wants to see a repeat of the fiasco which blighted last year's elections. The thousands of rejected ballot papers, the counts delayed by electronic counting problems and voters disenfranchised because their postal votes failed to arrive oADVERTISEMENTn time, added up to a national embarrassment. Now change is under way to ensure it does not happen again.

The SNP is engaged in a battle with the Scotland Office over transferring responsibility for Holyrood elections from Westminster to the Scottish Parliament.

Already, there has been a promise to return to separate ballot papers for the list and constituency votes. The next parliament elections will be counted manually � though for council elections the voting system makes electronic counting the only realistic option.

The Scottish Government has set out its proposals for "de-coupling" the Holyrood and council elections, which have so far always been held on the same day.

The idea of separating them is not new. The McIntosh Commission, which reported just a month after the first Scottish Parliament elections back in 1999, suggested a two-year gap between the polls. Tory MSPs have proposed member's bills to bring about the split.

But the latest move has been sparked by the report by Canadian election expert Ron Gould into last year's election debacle, which recommended de-coupling.

There seems to be majority support for the plan among MSPs. In addition to the SNP and the Tories, the Greens back elections on different days. Labour, which ruled out separating the elections when it was in power, appears to have dropped its opposition. Only the Liberal Democrats are still against the idea.

But there is nothing in Mr Gould's report to show the combined election contributed to the problems last May. His findings put the blame squarely on the decision to have a single ballot paper for list and constituency votes; the design of the ballot paper; and trying to implement too many changes � not least electronic counting � at one go, without adequate testing. 

His argument for unlinking council and parliament elections has little to do with what went wrong last year � rather, it is that a combined poll means the issues in the parliament election inevitably overshadow those in the council contests.

The opposing argument is that a combined election helps lift turnout for council elections, which has traditionally been lower than national elections. Mr Gould concedes this, quoting average turnouts for council-only elections of 46 per cent, compared with 54 per cent in the three elections when council and parliament votes have been combined.

In Edinburgh, council-only elections stirred only 44.5 per cent to go and vote last time, compared with nearly 72 per cent for the 1997 general election when Tony Blair was first elected, and over 61 per cent for the first combined council and parliament election. Council-only polls have, in the past, seen turnouts as low as 26 per cent in Pilton, 29 per cent in Craigmillar and 30 per cent in Muirhouse.

Some supporters of de-coupling go further and suggest combined elections are concealing the level of political disengagament at local level by artificially boosting the turnout for council elections. Opponents of de-coupling argue timing council elections at the halfway point of a parliament's four-year life is simply an invitation to voters to use them as a mid-term verdict on the Scottish Government. Mr Gould implies separate elections will mean more media coverage for council contests. But papers such as the Evening News already offer thorough reporting and analysis of the local campaigns.

Separate elections will also be more expensive. But it seems unlinking is all but inevitable. The consultation paper published yesterday floats various options for achieving the transition � including still holding council elections in the same year as the parliament ones, just delaying them by six months; or fixing the council elections a year before or a year after the parliament poll.

The most likely plan is to give the current councils a five-year term by postponing the next elections from 2011 to 2012, followed by another five-year term to take the elections to the mid-point of the parliament, then revert to four-year terms. 

But Scotland also has to vote in Westminster elections, which can be called at any time. In the first ten years of devolution, these have been at four-year intervals, coinciding with the mid-term of the Scottish Parliament.

So even after the careful separation of council and Holyrood elections, voters could still find themselves facing two elections on the same day.
