We�re funny people, you and me and all our kin.
We�re desperate to find a simple key to a longer and healthier life, yet we passionately ignore the key (more accuraately, the keys) lying in plain sight in front of our eyes.
Thus, when a small and virtually totally ignorable study on a tiny group of post-menopausal women living in Iowa (hardly your average North American citizens) concludds (to the surprise even of the main researchers, which is nearly always a reason to discount the findings) that taking vitamin D supplements lowered the risk of �cancer�, hundreds of thousands of Canadians jumped up and drove full speed to our nearest pharmacy to strip the shelves of vitamin D (it also didn�t hurt vitamin D sales that no less a prestigious organization than the Canadian Cancer Society suggested that most of us could benefit from vitamin D supplements, which was in my opinion, an exceedingly premature bit of advice, not to mention that it might be a dangerous recommendation, too, in that lots of Canadians will undoubtedly exceed the CCS recommended dose of 1000 i.u per day, and who knows what the consequences of that may be).
So, do supplements of vitamin D really lower the risk of cancer, and more important, do they increase your life expectancy?
The truth is we really don�t know, and we won�t know until someone does a really good prospective study that enrolls enough people and that lasts long enough to measure life expectancy.
But hey, don�t get too down because we have excellent evidence about how you can cut your risk of death significantly from every cause, including even cancer.
And it�s not a secret.
Actually, it�s the same old, same old. 
In other words, the key to a longer life is the same key I�ve been telling you about for years and years and years - and which you all know about, I�m sure, namely, a healthy lifestyle, which means (in its simplest form) not smoking, keeping a lid on your weight (not staying slim but rather avoiding becoming obese), eating a healthy diet, and being active.
And before you stop reading cuz you�ve heard all this before, you should know that a recent great 15,000 person strong study that lasted two decades found that those people who followed the foregoing 4 healthy lifestyle habits had a 40 % lower chance of dying (FROM ALL CAUSES INCLUDING CANCER) during the course of the study than people who didn�t follow the same healthy lifestyle.
And most encouragingly, middle-aged people who started with bad health habits but who �converted� (so to speak) during the course of the study did way better than those who stuck with their poorer lifestyle.
So you do know how to improve your chances of living longer and better.
The rest is up to you. 

