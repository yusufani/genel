B�yle birliktelik olur mu? 
FUTBOL Federasyonu Ba�kan� Levent B��akc� bir kez daha s�n�fta kald�. 

Milli Tak�mlar Teknik Direkt�r� Ersun Yanal hakk�nda t�rl� iddialar ortaya at�ld�. 

Bunlar yenilir yutulur gibi de�il. 

Te�vik primi iddialar� korkun�. �imdi herkes �ink�r� etse de, kamuoyu Cafer�in do�ru s�yledi�ine inan�yor. 

B�yle bir teknik direkt�r�n, Milli Tak�m��n ba��nda kal�yor olmas� yeterince �korkun�; ama Levent B��akc��n�n durumu daha da korkun�. ��nk� bu yay�nlar yap�lmadan evvel, Star TV�nin Spor M�d�r� Serhat Ulueren, B��akc��ya bu yay�n� yapaca��n� s�yl�yor ve �Yap yap� yan�t�n� al�yor. Belli ki B��akc�, Ersun Yanal��n y�pranmas�n� istiyor. 

�yi de, Yanal�la birlikte Milli Tak�m da y�pran�yor. 

Bu yay�n�n yap�lmas�n� istiyorsan, sonras�nda da gere�ini yapars�n. Yay�n� yapt�r, Yanal��n rezil olmas�n� sa�la; ama sonra ��k �2006�ya kadar hocam�zla devam edece�iz� de. 

Bu ne demek, ben anlamad�m. 

B�yle bir �ey olur mu?

Bir benzetme yapmak gerekirse, evli bir adam d���n�n. Sa�da solda e�inin �ahlaki davran��lar�� konusunda konu�uluyor. Hatta konu�ulmas�n� bizzat kendi istiyor. 

Sonra da d�n�p, �Benim e�im iyidir. Biz devam edece�iz� diyor. 

Bana sorarsan�z, federasyonun durumu bundan daha da vahim. 

��nk� di�eri kar�-koca ili�kisidir, kimse kar��amaz; ama burada hakk�nda konu�ulan ki�i sadece federasyonun de�il, milli tak�mlar�n teknik direkt�r�.

Hani okuma �zg�rl��� vard�

�ALI�MA ve Sosyal G�venlik Bakan� Murat Ba�esgio�lu d�n ilgin� bir a��klama yapt�: �Gerekli i�g�c�n� tespit edece�iz ve �niversitelerde buna g�re kontenjan ay�raca��z. M�hendis laz�m de�ilse m�hendislik fak�ltelerinin kontenjan�n� k�saca��z.�

Do�rulu�u tart���l�r bir karar. 

�steyen istedi�ini okur. 

�stelik de bu h�k�metin mensuplar�n�n en �ok savunduklar� �zg�rl�klerden biri de bu de�il miydi!

Ya baz�lar� da, �Yahu memlekette bu kadar ilahiyat��ya gerek yok. �lahiyat fak�ltelerinin say�s�n� azaltal�m� derse...

Ek protokol d��mesine bas�ld�

AB�nin 3 Ekim�deki m�zakerelerin ba�layabilmesi i�in �art ko�tu�u �Ankara Anla�mas��n�n ek protokol ile yeni �yeleri de kapsamas�� meselesi hayata ge�iyor. 

T�rkiye, K�br�s Rum Kesimi�ni tan�ma anlam�na gelecek bu konuda �hevesli� davranm�yordu. 

Ancak AB buradan geri ad�m atmayaca��n�, �stelik bunun �tan�ma anlam�na gelmeyece�ini� s�yl�yordu. 

AB�nin son bir ay i�inde yapt��� uyar�lar sonras�nda T�rkiye, �istemeden� de olsa ad�mlar� ba�latmak zorunda kald�. 

Ek protokol�n imzalanmas� i�in g�r��meler 2 Mart�ta Br�ksel�de ba�l�yor. 

G�r��meleri T�rkiye ad�na Deniz B�l�kba�� ve Ertu�rul Apakan y�r�tecek. 

B�l�kba��, Irak Sava�� �ncesi ABD ile y�r�t�len pazarl�klarda ba�roldeydi. Apakan ise K�br�s uzman�. 

Pazarl�klar�n ve ek protokol�n yaz�lmas�n�n 2 ay i�inde bitmesi planlan�yor. 

Bu iki aya, 8 dile �eviri ve b�t�n �lkelerce onaylanma da dahil. 

H�k�meti en ge� iki ay sonra zor g�nler bekliyor, demek yanl�� olmaz. 

Yarg� karar� i�imize gelmeyince

SEKA�n�n �zmit fabrikas�n�n kapat�lmas� b�y�k olay haline geldi. ���iler direniyor. Baz� gazeteler, baz� gazeteciler i��ilere destek veriyor. Direnmek i��ilerin hakk�. Tek kelime edemem. 

Ama kapat�lmaya kar�� ��kan gazetelerin tavr� beni e�lendiriyor. 

Niye mi? Yazay�m. 

�darenin ald��� �kapatma� karar�na kar�� sendika ve i��iler yarg� yoluna ba�vurdular. 

Yarg�, fabrikan�n kapat�lmas�nda hukuka ayk�r�l�k olmad��� yolunda karar verdi. 

Ve kapatmaya kar�� ��kan yazar ve gazeteler, i�te tam burada a���a d��t�ler. 

��nk� e�er �y�ce yarg�� fabrikan�n kapat�lmamas� y�n�nde karar verseydi, bu gazete ve gazeteciler, �H�k�met yarg� kararlar�n� hi�e say�yor. Yarg�n�n kapat�lmaz karar�na ra�men SEKA kapat�l�yor� diye man�et atacak ve h�k�meti yarg� kararlar�na sayg�s�zl�kla su�layacaklard�.

Ama �imdi yarg�, idareyi hakl� buldu ve yarg� �n�nde haks�z duruma d��enler, kapat�lma kar��tlar� ve onlara destek verenler. 

Fakat i�lerine gelmedi�i i�in yarg�n�n ad�n� bile anm�yor, mahkeme karar� sanki yokmu� gibi davran�yorlar. 

O zaman da olmuyor. 

�ifte standart ortaya ��k�yor. 

Herkes kendi i�e gelen yarg� karar�n� ciddiye al�yor, i�ine gelmeyeni yok say�yor. 

Yarg� da eli kolu ba�l� seyrediyor.

NE ZAMAN ADAM OLURUZ?

Sevgiyle nefret aras�ndaki ince �izgiyi �ok zorlamad���m�z zaman. 
