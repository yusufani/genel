Y�zde 25�le cumhurba�kan� olmaya kalk�l�rsa... 
BA�BAKAN�a bakarsan�z Ba�kan Bush�la yapt��� g�r��me son derece verimli ge�ti. 

Bir sonu� ��kmad� diye yorumlar yapan Amerikal� stratejistler, Beyaz Saray�a yak�n olan uzmanlar, olumlu yazmak zorunda olmayan Amerikal� ve T�rk gazeteciler hepsi konuyu Ba�bakan��n deyimiyle �spek�le� ediyorlar.

Ben T�rkiye Cumhuriyeti�nin Ba�bakan��na inan�r�m. 

O nedenle geziden bir sonu� ��kmad� diyenlerin (bunlar�n aras�nda ben de var�m) yan�ld���n� kabul ediyorum! 

�Do�rular� s�yleyenler Ba�bakan��n �evresindeki bir avu� insan bile olsa onlar�n dedikleri ve yazd�klar� tamamen ger�e�i yans�t�yor!� diyelim ve bu konuyu ge�elim.

�imdi esas soruna, cumhurba�kanl��� se�imine gelelim ve biraz bu i�e kafa yoral�m.

Bence AKP 2007�ye kadar dayanabilirse Recep Tayyip Erdo�an cumhurba�kan� se�ilir. 

Sak�n kimse bir ihtilal filan bekledi�imi sanmas�n. Kendisinden �ncekiler gibi bu iktidar�n da erken se�ime gitmek zorunda kalaca��na inan�yorum.

Onun i�in AKP normal se�im tarihi olan 2007 Kas�m��na kadar dayanamaz diye d���n�yorum. 

* * *

Erdo�an��n iddias�n� esas alal�m ve se�imin 2007�de yap�laca��n� kabul edelim. 

Bu durumda 2007 May�s��nda cumhurba�kan�n� bu Meclis se�ecek. 

Bu Meclis se�ti�i zaman da Recep Tayyip Erdo�an cumhurba�kan�, Emine Han�m da �First Lady� olacak.

Bildi�iniz gibi Erdo�an��n partisi se�ime kat�lanlar�n y�zde 34.28�inin oyunu alarak tek ba��na iktidar oldu. 

Buna kar��n AKP Meclis�teki sandalyelerin y�zde 65�ini kazand�. Bu se�im sisteminin azizli�inden kaynakland�. 

Ku�kusuz bunun su�u AKP�ye de�il, ge�mi� iktidarlara aittir.

Bir de partilerine k�zd�klar� i�in sand��a gitmeyen 10 milyon se�men var. Bu se�menlerin tamam�na yak�n� AKP kar��t�. 

E�er bu se�menler sand��a gitmi� olsayd� AKP�nin ald��� oy y�zde 25 olacakt�. 

Bu durumda AKP b�rak�n Meclis�te y�zde 65 sandalye kazanmay�, tek ba��na iktidar bile olamayacakt�. 

Bug�n AKP y�zde 25 oyla tek ba��na iktidar, Erdo�an da y�zde 25�le ba�bakan.

* * *

Ba�bakan��n �Millet bize yetki verdi. Biz istedi�imizi yapar�z� yollu meydan okumas�n�n demokrasi a��s�ndan ne kadar havada kald���n� d���n�n. 

2007 May�s��na kadar AKP�nin y�pranmas�n� d���n�rseniz bu y�zde 25 oran� daha da a�a��lara d��er. 

Hadi d��medi�ini varsayal�m. E�er bu Meclis cumhurba�kan�n� se�erse Erdo�an se�menin sadece y�zde 25�inin iradesiyle �ankaya�ya ��kacak.

Siyaset erbaplar� y�zde 25�lik bir irade ile 7 y�l cumhurba�kanl��� koltu�unda oturmaya kalkman�n ne kadar s�k�nt�l� olaca��n� �ok iyi bilirler. 

Biz toplum olarak yak�n tarihimizde bunun �rneklerine Turgut �zal��n �ankaya�ya ��k���nda tan�k olduk. 

�stelik �zal, Erdo�an kadar sivrilikleri olan bir insan de�ildi ve d�rt e�ilimi birle�tirerek iktidar olmu�tu. 

Erdo�an e�er bu ko�ullarda cumhurba�kan� se�ilmeyi g�ze al�rsa, sahip oldu�u d�nya g�r��� �lkede s�rekli gerginli�e neden olaca�� i�in �ankaya�da rahat oturamayaca��n� bilmelidir.

B�t�n hesap, kitap ve yorumlardan ortaya ��kan sonu� �u: Partilerine k�z�p sand��a gitmeyenler acaba T�rkiye�nin �n�ne ne kadar b�y�k bir fatura koyduklar�n�n fark�ndalar m�? 
