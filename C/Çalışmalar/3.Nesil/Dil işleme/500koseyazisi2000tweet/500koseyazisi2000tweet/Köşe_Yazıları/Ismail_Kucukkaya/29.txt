Gül'ün kaygıları ve umudu

Zamanlamasından belliydi. Cumhurun başının gerçekten önemli açıklamalar yapması için daha uygun bir takvim olamazdı. Ülke, kelimenin gerçek anlamıyla kavşaktaydı. Kafalar da karışık. Gül'ün ne düşündüğünü ben de merak ediyordum. Yazılmamak üzere anlatacağı bölümlerle kesin anlayacaktım, biliyordum; ne var ki ciddi bir manifesto niteliğindeki sözleriyle adeta çıkış yaptı ve düşüncelerini açıkça ortaya koydu. Hem umutlu, çok umutlu; hem de kaygılı, fırsat kaçarsa diye çok kaygılı...

Haber metninde geniş biçimde okuyabilirsiniz. Burada biraz izlenim aktarayım.

İstanbul ve Ankara'da nefis bahar havasını arkamızda bırakıp, üç saatlik uçuşla Baltık seyahatine başladık. Çankaya Köşkü protokol, iyi ki önceden uyarmış, buz gibi hava, yoğun kar yağışı bizi Letonya'da karşıladı. Otele yerleştik, hemen Gül'le sohbetimize başladık, saatlerimiz 22.30'du.

Kısaca geziden bahsetti ve "Letonya'da mı kalırsınız Anadolu'ya mı gidersiniz?" diyerek sözü gazetecilere bıraktı. O sırada yanımızda iki bakan Binali Yıldırım ve Egemen Bağış, işadamı Hamdi Akın ve Köşk bürokratları da vardı. TAV, Letonya'da iş yapıyor. Heyette Remzi Gür ve İbrahim Cevahir'in de aralarında bulunduğu çok sayıda işadamı bulunuyor. Gama da çok büyük ölçekli santral yapımını üstlenmiş. Gül'ün özel davetlisiydiler. Erol Üçer son anda rahatsızlandığı için affını istese de Hayrünnisa Hanım, eşi Mine Üçer'in gelmesini istemiş.

KEŞKE SİYASAL İKLİM HAZIRLANSAYDI

İlk soru anayasaydı. Cumhurbaşkanı Gül, doğrudan anayasaya ilişkin sorulmasa da Kürt sorunu ya da benzer tüm konulardaki diğer sorularda da sözü bir şekilde muhakkak yeni anayasaya getirdi. En büyük ve hatta tek önceliği buydu diyebilirim.

Daha ilk cümlesinde uzlaşma için "siyasal iklimin hazırlanmamasını eksiklik" olarak nitelendirdi. Tıkanma görüntüsünden rahatsızdı. Saymadım ama defalarca "Bu fırsat kaçmasın" dileğinde bulundu ve "Aksi halde yazık olacak" diye uyardı.

Gül, tarihin ve talihin Türkiye'ye, bütün sorunlarını çözmesi, büyük sıçrama yapması ve en gelişmiş ülkeler arasına yükselmesi için fırsat verdiğini düşünüyor. Yeni anayasayı da hepsi için, tüm hedeflere ulaşılabilmesi açısından en kritik araç olarak görüyor.

Cemil Çiçek'in 1 hafta süre uzatımı haberi tam yazımı bitirirken geldi.

REFERANDUMA İŞARET EDİYOR

Gül, Kürt sorununa çözüm çabasını "Olağanüstü bir girişim, çok büyük risk" olarak nitelendirdi. Çok uzun zamandır hükümetten bu denli pozitif cümlelerle söz etmemişti.

Hem üstlenilen riskin ciddiyetini hem de sürecin şeffaflığını övgüyle ifade etti.

Üç dört kez, "Nasılsa halkoyuna gidecek" vurgusuyla "Makulü arayan formüllere" işaret etti. Halkın istemeyeceği bir düzenleme olmaz, kabul edilmez rahatlığındaydı.

Vatandaşlık tanımı konusunda rahat gördüm Gül'ü. Hiç tarif edilmemesini doğru buluyor. Yerel yönetimlerin güçlendirilmesinden yana da açık pozisyon alıyor; temel hakların en geniş şekilde tanımlanmasından yana da...

Hep "kesin sonuç"tan bahsetti. Terörün ve Kürt sorununun mutlak çözümlenmesinden. Kaygısı buradaydı, "Aksi halde Ortadoğu şartlarında Türkiye'nin rahat bırakılmayacağını" biliyor. "Üstelik terör bu kez daha çok tırmanır. İki taraf için de daha acı sonuçlar üretir" endişesi taşıyor. Nihai netice için de anayasal düzenlemeleri ve tam demokratikleşmeyi çare olarak görüyor.

Saatler gece yarısına doğru gelirken; Ahmet Sever'in uyarısıyla sohbetin sonuna yaklaşmıştık. Cumhurbaşkanı "Her dakika önemli" dedi. Süreci kastediyordu. Yerinde bir kaygısını da şöyle ifade etti:

"Siyasi partiler işin içine ne kadar girerse iş o kadar kolaylaşır. Hele ana muhalefet partisi."