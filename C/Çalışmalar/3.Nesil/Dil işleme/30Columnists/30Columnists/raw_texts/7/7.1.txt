Damian Mcbride has been forced to resign as a Downing Street special adviser this Easter weekend for doing things he should not have been doing on Gordon Brown's payroll, let alone at public expense. He got caught out and has paid the price.


If I understand the situation correctly, McBride got mixed up with Labour blogger and psychotherapist, Derek Draper  not always a wise move  in trying to create a leftwing counterweight to the right's dominance of the political blogosphere.


Political addicts and anoraks love this frenzied world of attack and counter-attack, gossip and exposé, usually keener on malice and outraged opinion than the finer dilemmas of policy-making.


Most sensible citizens ignore it, concentrating on their own online interests (which can be just as vehement). Political bloggers such as Guido Fawkes, instigator of McBride's doom, tend to be rightwing, free market or libertarian Tories, the kind of people who want to blame governments rather than bankers for the global economic crisis.


Actually, both are at fault, but the blogosphere does not do shades of grey. The medium lends itself easily to shoot-from-the-hip outrage. That is why many enthusiasts love it.


That said, the murky underworld of sleaze and gossip which permeates the backdoor politics  and most walks of life where power, money, or the lack of it, matter  existed before the internet was invented or McBride got involved.


It will continue to thrive in his absence, only much faster than generations ago. Then a prime minister of the day (Harold Macmillan) could be cuckolded by a Tory colleague for decades or another prime minister's (Harold Wilson) political secretary could have two children (by a political journalist) without most of us knowing anything about it.


The net has changed all that. Not just by virtue of its speed, cheapness and accessibility, all currently deemed to be democratic assets; also because of the commercial pressures it places on mainstream media to compete, even as its sales and advertising revenues shrink.


What differentiates Britain from most comparable countries in Europe, north America and the wider English-speaking world is twofold: there exists here a relatively open system (more like the US than France); and a media much more willing to print damaging claims (in this instance via Guido Fawkes) against the political class  whether they are true or false.


Mainstream US media would routinely be more cautious, but most American cities have long ceased to have the raucous tabloid tradition that Britain has. Instead it has rightwing "shock jocks" dominating the airwaves  a tradition dating from the 1930s  and, more recently, Murdoch-owned Fox TV.


Check out Fox if you have not done so. It's quite different. So is Rush Limbaugh's radio show for whom Barack Obama is a traitor. It also undermines the conventional explanation for the rightwing dominance of the British blogosphere, that it is a function of being in Opposition. In the Bush era, the same suspects were pushing the White House ever further to the right and smearing what passes for the left.


So Labour politicians feel even more beleaguered than they did when the Daily Mail ran the " slagheaps" scandal against Harold Wilson in 1974 or the Sun put a photo of Neil Kinnock inside a light bulb on polling day in 1992, telling the "last reader to leave Britain" to switch off the light if Labour won.


From that trauma came Tony Blair and Gordon Brown's fear of the media  much multiplied by commercial pressures and internet technology in the succeeding 20 years  and their desire to appease it when they could.


Alastair Campbell and Peter Mandelson became famous as Labour's media manipulators, countering what would later be called "spin" from the Tory press. They squared the tabloids in their own coinage and tried to square or squash the broadsheets, too.


The Guardian suffered many bruises in that period, many good stories  and some duff ones  which went to more accommodating newspapers. Not being willing to pay big bucks for dirt also matters in this market. Details about MPs' salaries or, in this case, McBride's blogging habits, get hawked to papers with large chequebooks. None of this gets talked about.


Mostly Campbell's square-or-squash strategy worked for a while, albeit at a high price in terms of the government's credibility, with swing voters who gave Blair his 1997 landslide and with the sympathy of its natural supporters.


Gordon Brown's spinner, Charlie Whelan, was less sophisticated and  being part of the Brown-Blair tussle for dominance  part of the problem for No 10. When Mandelson was brought down in 1998, Whelan was (wrongly, in this instance) blamed, and forced out, too.


Whelan is back on the circuit, a combative trade union official, fiercely loyal, a gut partisan who is never dull company, street-smart. He and McBride, who left the Treasury civil service to join the Brown inner team, are the sort of operators who could get on well, though they may also be too much alike in their instincts to do so.


Both reflect a puzzling aspect of Brown's career that combines high-minded intent, a very real desire to serve the public good, with a fatal streak of insecurity. Which prime minister, sitting in No 10 in such a crisis, would not feel insecure in 2009?


It creates the self-doubt which makes some men walk down the street with their rottweiller on a lead. "Don't mess with me unless you want trouble with my dog," is the intended message. And "Mad Dog" was one of McBride's many nicknames.


The trouble comes when such fierce dogs bite the wrong person  invariably it is the wrong person  and has to be put down. That is what has just happened to Damian McBride.


He shouldn't have been dabbling in what sounds like squalid stuff, but it helps to understand why people like him do what they do. They do it to protect their boss and undermine opponents whom they think enjoy an unfair advantage in a corrupted media environment.


Unlike the great banks which have brought the economy to its knees the political class do not have expensive lawyers to browbeat the media, as even the FT admits happens.


And do not think it is just politicians who get stick. Remember what was done to Kate and Gerry McCann  and many like them  in the name of the public's right to know.
