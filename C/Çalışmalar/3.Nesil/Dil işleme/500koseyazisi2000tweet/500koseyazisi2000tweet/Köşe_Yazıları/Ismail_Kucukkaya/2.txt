Orta demokrasi tuzağı

Çok ciddi ilerlemeler kaydettik. Sadece ekonomik yaşamda değil, demokratikleşme sahasında da... 
Son on yılın siyasal istikrarının katkısıyla “bölgesel ve küresel rekabet arenasında” hayli mesafe kat ettik. 
Ancak, gidilecek daha çok uzun ve zorlu yollar var. 
Öncelikle, “kazanımların teminat altına alınması” şart. Bunun yolu, “yeni anayasa”dan geçiyor. Ama gerçekçi olmak gerekirse “başka bahara kaldı.”  
“Katılımcı, özgürlükçü ve demokrat bir yeni toplumsal uzlaşma metni...”  
Maalesef heba edildi. Ama şimdilik. Siyaset kurumu, dip dalgadan gelen o sese çok uzun süre kulak tıkayamaz. “Yeni Türkiye” kendisini taşıyacak yeni anayasa bekliyor. 
Yurdun dört bir tarafındaki arama konferanslarında vatandaşlara yöneltilen “anayasadan ne bekliyorsunuz?” sorusuna ilk sırada hep “adalet” yanıtı geldi. 
MGK’nın ve sivil asker ilişkilerinin normalleşmesi AB reformlarının olağanüstü katkısıyla sağlandı. 
Vahim sonuçlara yol açabilecek “Parti kapatma davasının geri döndürülebilmesi” kamuoyunun demokratik duyarlılığıyla sağlandı. Çok açık ki; toplum zihniyet olarak darbeleri geride bıraktı. Sermaye, medya ve sivil toplum da...

10 YILA NELER SIĞDI? 
“İslamcı” diye suçlanan ve “kurulu yapı tarafından tehdit diye algılanan” bir partinin tek başına iktidara gelmesi karşısında “eski reflekslerle” harekete geçen ve “sisteme müdahale” arayan çevreler, öncelikle askerin içindeki Batı değerlerini özümsemiş, demokratik ilkelere bağlı kesimlerce engellendi. 
Asker yıllar içinde kendini toplumdan izole edip, yerinde sayarken, “modernize olan, Susurluk sonrası ödediği bedellerden dersler çıkaran emniyet teşkilatı”nın yürüttüğü hamleler... 
Özellikle de her çeşit mafyanın sistem dışına alınması... 
Yargı eski köhne yapısından bir nebze olsun kurtuldu. Teknik ve fiziki imkânlara kavuştu. Eskiyle kıyaslanamaz bir hale geldi. Ama sonra bir noktada durdu. Yargı kültürünün bağımsızlığı konusunda yapılması gerekenler ortada duruyor. 
Çok olumlu ilerlemeler sağlandı. 
Ancak gazetecilerin, akademisyenlerin beş yıla varan tutuklulukları... 
Yargılamalardaki yürek burkan ve Türk yargısına yakışmayan görüntüler. 
Allah aşkına, Mustafa Balbay’ın, Tuncay Özkan ve Mehmet Haberal’ın hâlâ tutuklu kalmasını kendimize nasıl izah edebiliriz? 
Bunlar “demokratikleşme karnemizin olumsuz bilançosu.” 

OLGUNLUK KÜLTÜRÜ 
Türkiye dünyaya açıldı. Orta sınıfın baş döndüren girişimciliği yerkürenin her yerinde kendini hissettirdi. O kesim de demokrasi talep ediyor. Ekonomik ve siyasal liberalizm istiyor. İkiz kardeş gibi bunların birbirinden ayrılamayacağını görüyor. 
Ekonomide büyüdük. Küresel kriz döneminde sağlandığı için bu başarıyı “mucize” gibi de yorumlayabiliriz. Ama ikinci büyük sıçramayı yapabilecek, yargı ve eğitim reformlarını uygulayabilecek, toplumsal barışı sağlayabilecek miyiz? 
Seslendirmekten, tanımlamaktan korkulduğunu görüyorum. Hayır, gururla kabul etmeliyiz ki; “Türkiye bir model ülkedir.” Ancak demokrasimizin olgunlaşması şart.  Kurum ve kültürüyle yerleşmesi gerek. Hayranlık duyulan bir ülke olarak yetinemeyiz. Saygınlık uyandırmak da zorundayız. 
“Orta gelir tuzağı”nı aşamazsak, yerimizde sayar, birinci ligde kalır, süper lige çıkamayız. Ama galiba, bugün gerçek problemimiz “Orta demokrasi tuzağı” ile karşı karşıya kalmak. 
Amerika’nın, kurulurken 300 yıl önce başardığını biz bugün yapabiliriz. Muhafazakâr-liberal, solcu-sağcı, Türk-Kürt, Alevi-Sünni hep beraber, özgür bir toplum yaratabiliriz. Ben, Gezi Parkı olaylarını işte bunun miladı olarak görüyor ve olumlu pencereden değerlendiriyorum. Onun “özünde” taşıdığı “demokrasi talebini” değerlendirmek ve “uzlaşma kültürünü yeşertmek” bize iki yıldır içine düştüğümüz “orta demokrasi tuzağından” kurtulmanın umudunu ve formülünü veriyor.