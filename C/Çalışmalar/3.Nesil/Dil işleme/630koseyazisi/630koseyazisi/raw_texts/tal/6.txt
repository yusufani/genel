Trabzon'u Sivas'a sor! 

 
 

Ba�kan "k�t� gidi�in" nedenini bulmak i�in arastama yap�yormu�. Frans�zlar "bonjuor" biz de "g�nayd�n" diyoruz bu t�r i�lere. 

Sporculuk ki�ili�i kadar "y�neticilik" vasf� da tar���lamayacak �l��de de�er kazanm�� sevgi ba�kan Atay Aktu�'a bu saatten sonra s�ylenecek tek kelime varsa, o da �u: Bonjuor! E�er kendilerini komaya sokan Sivas yenilgisinden �nce bu kul�b�n teknik heyeti, menajeri ve futbolcular�yla konu�sayd�, san�r�z "��renmek istedi�i ba�ar�s�zl�k nedeninin" en �nemli b�l�m�n� kendisine anlat�rlard�. ��nk� Sivasspor'un gen� ba�kan� Mecnun Odyakmaz. Trabzonspor'la oynad�klar� ma�tan �ok �nce bize anlatm��t�.

Sadece Mecnun Odyakmaz de�il, teknik direkt�r Werner Lorant, yard�mc�s� menajer B�lent Uygun (F.Bah�e'nin eski asker B�lent'i) ve futbolcular da anlatm��t�. Hepsinin birle�ti�i tek h�k�m vard�; Trabzon son derece disiplinsiz bir tak�md�.

�ddaya girdi
Nereden bu h�kme varm��lar diyorsan�z, Almanya'deki haz�rl�k kamp�n� ayn� otelde yapm��, Trabzonspor kafilesindeki "ba�� bozuklu�u" hi� hayra yormam��lard�.

Yeme�e isteyen istedi�i saatte iniyor isteyen de istedi�i an yemekten kalk�yordu. Sivas kafilesi ise ayn� saatte yeme�e oturuyor, yemekten sonra da "afiyet olsun" denilip kalk�l�yordu.

Sivas kafilesindeki futbolcular en ge� gece 22.00'de istirahate �ekilirken, Trabzonsporlu futbolcular gecenin 01.00'inde, 02.00'sinde bile ortal�kta dola��yorlard�. Hele Yattara, hele Yattara... Sanki "d�mensiz geminin �ark�� ba��"yd�! Kimseyi takti�i yoktu. �enol G�ne� bu olanlara g�z m� yummu� diyorsan�z, o da ayn bir meseleydi. Sivas Ba�kan� Mecnun Odyakmaz'a g�re �enol hocanm g�rev s�ras�nda estirdi�i "ter�r" tamamen g�steri�ti. ��nk� "rakipleri inceleme" bahanesi ile s�k s�k kamptan ayr�lm�� kafileyi yard�mc�lar�na b�rakm��t�. B�t�n bu "haz�rl�k d�nemi ba��bozuklu�unu" g�ren Sivas ba�kan� "Bunlar K�br�s tak�m�na bile elenir" dedi�inde, Sivas kafilesinden bir tek ki�i bile kar�� ��kmam�� Trabzonspor'u savunamam��t�.

Transfer bombas�!
San�yoruz, ba�kan Atay Aktu� da biliyordur i�in bu yan�n�. ��nk� baz� olanlar�n �ahiti kendisiydi!

Transfer seferine ��kt�klar�nda "demir" at�p g�nlerce, aylarca orada kalan 3 ki�ilik transfer heyetinden �mer San telefonla ba�kan� aram�� ��yle demi�ti: "Lemi santrfor Kahe'yi alal�m diye tutturuyor, sak�n inanmay�n ba�kan�m. Son derece a��r, i�e yaramaz. Trabzon amat�r k�mesini tarasan�z 10 Kahe bulursunuz..." Ama bir s�re sonra ayn� y�netici yine ba�kan� telefonla aray�p ��yle diyecekti: "Kahe'yi alal�m ba�kan�m. Bize �ok yararl� olur, �ok gol atar..." Ba�kan Atay Aktu� da "Yahu arkada� siz oraya oyun oynamaya m� i� yapmaya m� gittiniz?" diye ba��rm�� ve surdan s�ylemi�ti: "Bir �yle diyorsun, bir b�yle. Verdi�in rapora g�re camiaya a��klad�k. Olmaz dedik. �imdi de yan�lm���z m� diyece�iz."

Tamam m� ba�kan? Bu g�r��meler oldu de�il mi? Aylar s�ren Brezilya maceras�ndan ne ��kt�? Bir kalecisi vard�, tak�ma daha da al��m��t�. Yerine 22 ya��nda tecr�besiz bir kaleci al�nd�. Tolga gibi milli bir stoper vard�, sat�ld� yerine Elber al�nd�. Bir de �ek santrfor tam hay�l k�r�kl���yd�.

Evet sevgili ba�kan! ��renmek istedi�iniz ba�ka bir �ey var m�? Varsa Sivas ba�kan�n� aray�n, o size burada yazamad���m�z "�zelleri de" anlats�n.
Vay vay... Ge�en sezon bu tak�m "�ah" t�. �ampiyonluk elinden zorla almd�. Bu sezon daha da iyi olmas� gerekirken "�ahbaz" oldu!

Ba�kan h�l� "Neden b�yle oldu?" diye d���n�yor! 

 
