MY first reaction to the headlines that screamed Scotland has the eighth highest consumption of alcohol, per head, anywhere in the world, was probably not what the doctor ordered when they handed over the results of the study undertaken by the Medical Research Unit and Glasgow University.

Possibly because I've grown used to other studies reporting that Scottish school pupils have the worst and/or the fewest teeth in the UK, Europe, the world; that Scots middle-aged women are fatter than the UK's other middle-aged women; and that ScoADVERTISEMENTtsmen don't live as long as they should, I felt relieved that there are seven countries that should be more worried than us about their drinking habits.

Then I read the press reports of the study. I've come to the conclusion that even though I've occasionally thought Justice Minister Kenny MacAskill could drive me to drink as I've listened to him going into overdrive in the Holyrood Debating Chamber, he's right to persist with his mission to save us from the love of far too many of our lives . . . drink.

On one of the detailed changes he'd like to introduce, a ban on anyone under 21 being able to buy booze from an off-sales, I'm still opposed and likely to remain so. But as regards the broad thrust of the rest of the raft of changes he's floated, I think he's probably on the right lines.

There's little doubt the rise in consumption is linked directly to price. As we've earned more, the price of our favourite tipple has dropped as a proportion of the mad money in our pocket, or even the budget for the week's shopping. When I was a young mum, a bottle of wine, usually German, on the floral side, or Italian, usually for the decorative bottle, was a considered purchase.

Nowadays I don't think twice about scooping up a couple of bottles of "on offer" Cava because they just about represent the small change of our weekly supermarket spend.

So the Justice Minister's probably right to try for minimum over-the-counter prices, and an end to the "two for the price of one" offers. Yet it will be hard on grannies who know a bargain, and their limit, if they're banned. But having read about the almost 500 Scotswomen who die before reaching their allotted threescore and ten years, and the statistics on young women in Scotland drinking 21 per cent more than their big sisters did only three years ago, I've decided to find out more about this attempt to use higher pricing to make drink less accessible for our five teenage granddaughters.

Another of the reasons being advanced for the alcohol consumption statistics for women going past, way past, the Health Department's "safe" recommended number of units of alcohol per week is that lounge bars, and even public bars, are now comfortable and welcoming. This is in stark contrast to the good old boys atmosphere in some bars and the sweaty masculinity of others that underlined the social convention of 40 years ago that such places were not for ladies.

Since we can't, and shouldn't, attempt to turn the clock back on the standards of safety, comfort and decoration in pubs, we have to devise another way of ensuring that young customers in particular don't drink beyond a limit their body can process efficiently, nor produce behaviour that shocks, often physically harms, and sometimes kills. 

Obviously, drinkers must be exposed to education on alcohol use and abuse, and then be expected to take responsibility for their behaviour. But just as obviously, the law against selling alcohol to someone already showing signs of being inebriated should be enforced. Companies should be fined and/or lose their licence if they fail to train their staff.

So Kenny MacAskill gets my support for most of his ideas if he keeps them rooted in reality . . . although we're the eighth worst in the world, that still means most of us don't have a problem.

Euro bail-outs
There's a reasonable excuse for having little awareness of the other big money story currently just below the radar of most of our fellow citizens. 

Britain's not in the Eurozone, so for the moment it's of little interest that Germany's being thrust into the old routine that Germans had hoped they could leave behind them after they'd paid for the former East Germany coming into the EU fold at reunification.

Reluctantly, Ireland will be bailed out by European Central Bank, most experts now think. But the same top money men are hedging their bets on who's left standing in the Eurozone with strong enough reserves to bail out Italy or Spain.

And remembering the lessons of the 1930s, anybody got any ideas on preventing history being repeated?
