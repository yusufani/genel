There are those who believe managers like Sir Alex Ferguson have it easy. That all he needs to do most weeks is pluck 11 random names from a hat, sit back and collect three points for Manchester United. To which, two words: Aston Villa. 

As plucky outsiders, Villa were flying. When they were everybody's second team taking on the supremacy of the established elite four Premier League clubs they played football for fun. The pressure was on Arsenal to hold them off, minus their inspirational fulcrum, Cesc Fabregas.

Then a strange thing happened. Aston Villa overtook Arsenal and looked set fair for that fourth Champions League spot, at which point what had previously been a prize to win became a prize to lose and the players began to feel the pressure. Expectation rose, hence the booing that followed Sunday's defeat by Tottenham Hotspur, and Villa wobbled. It is not easy being top dog, or even in the top pack. It brings different stresses, an entirely fresh set of challenges. 

In recent weeks, there has been much talk of Ferguson's successor at Manchester United, sparked by Rio Ferdinand's endorsement of Jose Mourinho. The reaction to that has been the championing of a domestic candidate, most popularly the impressive David Moyes, manager of Everton. Therein the conundrum: how can the career of a manager punching above his weight at a small club prepare him for the Premier League's upper echelons? 

   
More recently, Moyes has established his club as the best of the rest. Everton are consistently in the UEFA Cup qualification places and, domestically, will contest a second semi-final in consecutive seasons when they take on Manchester United in the FA Cup on April 19, having lost to Chelsea over two legs in the Carling Cup last year.
In comparison to the top clubs, however, Everton are financially weak and, as a result, play their football a certain way. Even against inferior teams Moyes favours caution, which is understandable, but hardly the best grounding for career promotion. 

A top four club need to play on the front foot, they need to make the game, even away from home, and they need a manager to embrace that style. Could Moyes do this? The Manchester United board would have to take his word for it, because he will not be able to call on many examples that demonstrate a penchant for the cavalier at Everton.
Sam Allardyce was the last manager to achieve a similar level of respect in the game for over-achieving at Bolton Wanderers. His success was also built on dour determination and a team that lined up 4-5-1 and when he went to Newcastle United, a club with a tradition of open football, the relationship quickly soured. 


Largely this was down to unrealistic expectations - Newcastle fans want the team to play like Manchester United even if the players have more in common with Manchester City - but there was fault on Allardyce's side, too. Nothing in his recent career tended to fantastic football and he was uncomfortable with the demands. The same was true of Alan Curbishley, who switched from Charlton Athletic to West Ham United and disappointed with his conservatism. Allardyce and Curbishley rightfully expected top jobs after what they achieved but found it hard to adapt after so many years winning games with the odds against them. 

It does not seem to be such a problem in Spain, where even lesser teams favour an open style and managers progress up the career ladder without the struggle to acclimatise. Having said this, could some of Rafael Benitez's caution at Liverpool be explained by formative years with Real Valladolid, Osasuna, Extremadura and Tenerife? Even his successful Valencia team were not known for their abandon. 

The Champions League clubs enjoy great advantages. Superior wealth means better players, deeper squads, bigger grounds, and, taking this into account, coming sixth with Everton might be a superior achievement to coming second with Chelsea or third with Liverpool. It is a certain type of achievement, though, one that does not necessarily school a manager for the task of committing a team to attack or for circumstances when another win will only attract barely stifled yawns. 

We expect Manchester United to take the game to and defeat Fulham this weekend and, if they do, the event will pass unnoticed. Lose, however, and alarm bells will sound, heralding a seven-day inquest into the crisis. This is the pressure the elite handle every week and it is what ate away at Villa the moment it was assumed they were part of that group. 

Moyes is no doubt a very good manager, but the question of where he goes from here is not simply a case of onwards and upwards. British Rail once blamed delays on the wrong kind of snow. For Manchester United, might Moyes be the wrong kind of good?
