Yeni T�rkiye yeni siyaset  
     

  
GEN� Ar�lar, bu hafta sonu yap�lacak ve �lkenin �e�itli b�lgelerinden 1000 gencin kat�laca�� Gen�Net Konferans�'na hangi siyasilerin kat�lmas�n� istediklerini bizzat gen�lerin kendisine sormu�lar.

�nternet ortam�nda yap�lan bu ankete tam 2877 gen� cevap yollam��.

Anketten ��kan �arp�c� sonuca g�re, n�fusun % 65'inin 35 ya��n alt�nda oldu�u gen� T�rkiye'de gen�ler cari siyasileri katiyen merak etmiyor.

* * *

D�n Meral Tamer'in Milliyet'teki k��esinde yay�nlad��� sonu�lara g�re, ankete kat�lan 2877 gen�ten 1170'i (% 40) Kemal Dervi�'i, 966's� (% 33) Erkan Mumcu'yu merak ediyor.

* * *

�ktidar ortaklar�ndan B�lent Ecevit 2877 gen� aras�nda sadece 4, Mesut Y�lmaz ise 3 oy alabilmi�ler.

Ancak, gen�ler yeteri kadar denenmemi� Devlet Bah�eli'ye, sadece 77 oy verseler de, onu B�lent Ecevit ve Mesut Y�lmaz'dan ay�rt etmi�ler.

�imdi bu iki oy malul� zevat diyecekler ki, ��ne yapal�m, bizi kriz y�pratt�!�� �yi de, o zaman ekonomik krizin t�m ac� re�etesini y�klenen Kemal Dervi� nas�l 1170 oy alabiliyor?

O da insanlara ac� �ektiriyor.

Nerdedir bu fark?

Gen�ler, pek�l� fark�ndalar ki, iki genel ba�kan yeteri kadar denenmi�, �aps�z ve g�venilmez bulunmu�.

Gen�ler, belki re�etelerinin ac� faturas�n� sevmiyorlar ama Kemal Dervi�'i samimi buluyorlar, onu merak ediyorlar.

* * *

Anketten ��kan �ok ilgin� ba�ka bir sonu� ise bir partinin genel ba�kan� 3 istek oyu al�rken (Mesut Y�lmaz), kendi yard�mc�s� Erkan Mumcu'nun 966 istek oyu almas�.

Genel ba�kan neredeyse hi� talep edilmezken, yard�mc�s� rekor k�l�yor.

Dilerim, Erkan Mumcu gen�lerin kendisine verdi�i mesaj� nihayet al�r.

* * *

Hadi iktidardaki denenmi�ler b�kt�rm�� da, �imdi muhalefette ama daha �nce denenmi�ler ne durumda?

Tansu �iller sadece 8, Recai Kutan ise 2 oy alabilmi�ler.

* * *

K��ede g�n�n� bekleyenler ne durumda?

Kullan�lan 2877 oydan Melih G�k�ek 80, A. M�fit G�rtuna 54, Sadettin Tantan 15, �lhan Kesici 15, Deniz Baykal 1, Murat Karayal��n 1 oy alabilmi�ler.

Demek ki, onlar kendilerini alternatif olarak g�rseler de gen�ler b�yle d���nm�yor ve onlar� merak etmiyor.

* * *

Milletten yeniden g�rev bekleyen S�leyman Demirel, bu �a�r� i�in kime g�venirse g�vensin, ama asla gen�lere g�venmesin. Ayn� gazetenin ba�ka bir sayfas�nda gen�lerin S�leyman Demirel'i dinlerken nas�l uyuduklar�n�n foto�raf�n�n bas�ld��� g�nde onun oyu 7 olarak ilan ediliyor.

* * *

Beni �ok �a��rtan iki sonu� ise son g�nlerde pop�laritesi olduk�a y�kselen iki liderin bu gen�ler aras�nda talep bulmamas�.

R.T. Erdo�an 43, Besim Tibuk ise 33 oy alabilmi�.

* * *

Bu anket bilimsel bir tespit de�il.

Ancak, anlayana, anlamak isteyene �ok �ey s�yl�yor.
 
