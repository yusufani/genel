Ge�ti�imiz hafta  
     
  
Ba�l�ktan anlam��s�n�zd�r, bug�n geride b�rakt���m�z haftaya bir g�z ataca��m.

Biraz daha kibar olmam�, ��Birlikte bir g�z atal�m, ne dersiniz?�� gibi bir �eyler s�ylememi bekleyenler vard�r i�inizde. Hakl�s�n�z, ancak sizden cevap gelmesi uzun s�rer �imdi. Bu durumda ben g�z ataca��m, siz de mecburen okuyacaks�n�z.

* * *

Hemen ilk konuya ge�iyorum.

Ediz Hun olay�.

Tipik bir ��Sak�nan g�ze ��p batar�� durumu. Ediz Hun'u tan�yanlar bilirler, kendisi ��Haza beyefendi��lerin son temsilcilerindendir. De�il bir han�mefendiye sayg�s�zl�k etmek, di�i kedinin �n�ne ge�meyecek kadar nazik biridir.

Olay s�ras�nda orada de�ildim, Sa�lamer'in koltu�unun koluna ili�ti�ini gazetelerde g�rd�m. Ancak oradaym���as�na, Ediz Hun'un Sa�lamer'den �z�r dileyerek izin istedi�inden eminim. Ve oraya ili�mesinin nedeni asla Mesut Y�lmaz'a yak�n olmak, ayn� karede yer almak falan de�ildir; ifade etti�i gibi H�lya Ko�yi�it'e bir merhaba demektir, bundan da eminim.

Yok, b�yle de�ilse bug�ne kadar kimseyi tan�yamam���m demektir ki, bu durumda �lem ben, daha iyi.

Her �eyin sebebi ho�g�r� eksikli�i.

Sa�lamer, bir zamanlar filmlerini zevkle seyretti�i, hatta hayran� oldu�u iki sanat��n�n, iki eski dostun birbirine hal hat�r sormas�n� iki dakika g�l�mseyerek izleyebilirdi. Orada, o pozisyonda birka� dakikadan fazla kalmazd� ki Ediz Hun.

Sa�lamer olaydan sonra yapt��� konu�mada Ediz Hun'u kastederek ��Siyasetteki ayr�kotlar� temizlenmeli�� demi�. Evet, Ediz Hun'un ��ayr�kotu�� oldu�u do�rudur. Al��t���m�z kasabal� politikac�lar�n aras�nda ��ayr�kotu�� gibi duruyor hakikaten. Benim temennim, Meclis'in bir g�n tamamen Ediz Hun gibi ��ayr�kotlar���ndan olu�mas�.

* * *

BBG evindeki kedinin �l�m�n� biliyorsunz. Yok, tekrar olaya de�inecek de�ilim. Bir �ey dikkatimi �ekti, onu payla�aca��m sizinle.

Reha Muhtar, bu olay� yok sayd�.

Ki kendisi hayvanseverdir,

Ki haberi yoktan var edendir,

Ki daha �nemsiz �eyleri bile g�nlerce didikler.

Ne oldu da b�yle oldu bilmiyorum. �a��rd�m do�rusu.

* * *

��Haftan�n Foto�raf���n� se�mek gerekseydi e�er, Necmettin Erbakan'�n o�lu Fatih Erbakan'�n Milliyet Gazetesi'nde yay�mlanan foto�raf�n� se�erdim.

Kollar yukar�da ba�parmaklar havada, partilileri selaml�yor. Recai Kutan'sa oturdu�u yerden ona bak�yor.

��Recai Kutan'�n bak���ndan ne anlam ��kard�n?�� diye sorarsan�z bir �ey s�yleyemem ama, i�imden bir ses ��Recai Bey'i sev�� dedi.

��imdeki o sese �ok g�venirim ben. Dedi�ini yapt�m. �imdi de ��Recai Bey'i al o Saadet Partisi'nden, ba�ka yere koy�� diyor. Bunu beceremem herhalde ama ke�ke yapabilsem.

Fatih Erbakan da babas�n�n partisine m�barek olsun, otursun, kendi o�lu do�up b�y�y�nceye kadar birlikte kocas�nlar.

* * *

Siz bu sat�rlar� okuyuncaya kadar Ediz Hun'la G�ls�m Sa�lamer aras�ndaki mesele tatl�ya ba�lanm��, Reha Muhtar �len kedinin fark�na varm��, dolay�s�yla benim bu yaz�m daha okunmadan hurdaya ��km�� olabilir.

Yaz�y� yay�mlanaca�� g�nden d�rt g�n �nce teslim etmek durumunda olman�n en k�t� taraf� da bu zaten. ��Siz bu sat�rlar� okuyuncaya kadar...�� diye ba�layan c�mleler kurmak zorunda kalmak.


m��-mu�

Ebru �all�, ��Harun'la bu yaz evlenece�iz�� demi�.Ge�en sene ve ondan �nceki sene de ayn� �eyi s�ylemi�ti; her daim ��plak gezdi�inden k��� yaz� ay�ramay�p bir t�rl� evlenemiyor k�zca��z.

M. Ali Erbil'in kay�nvalidesi, ��Damad�m elimden tutacak�� demi�.M. Ali 40'�n� a�m�� bir kad�n�, elinden bile olsa tutmakla, bir ilki ger�ekle�tirmi� olacak.

K.Evren'in, cumhurba�kanl��� d�neminde kendisine hediye edilen m�cevherleri �nce sergilenmek �zere ba���lad���, sonra ��K�z�m �z�ld��� diyerek geri ald��� ortaya ��km��.Netekim bizi �a��rtmad�.
 
