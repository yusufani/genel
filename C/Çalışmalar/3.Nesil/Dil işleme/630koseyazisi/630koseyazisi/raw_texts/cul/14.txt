Camp David ruhuna g�re (III): AKP�nin stratejik derinlik yakla��m� ne i�e yar�yor? 
ENG�N G�ner�in 1991-Camp David toplant�s� �zel notlar�na bakarak aktard��� bilgilere g�re: 

1) Stratejik ortakl�k iki devlet aras�nda siyasi, ticari, ekonomik ve g�venlik ile d�� politikay� kavrayan �ok boyutlu bir ili�kidir.

2) Stratejik ortakl���n olmazsa olmaz �art�, taraflar�n ili�kiyi etkileyen herhangi bir eyleme ge�meden �nce birbirleriyle isti�are yapmas� gereklili�idir. �sti�are yapmadan eyleme ge�ilmeyecek ve m�mk�nse mutabakat aranacak. 

3) taraflar, yine de kendi eylemelerinde serbestler. Egemenlik esast�r! Eylem �ncesi isti�are �art ama mutabakat olmayabilir. �ki taraf, 3. �lke(lerle) birbirinin ana politikalar�n� incitecek eyleme girmemeye dikkat edecekler.

4) �ki devlet, d�nya �zerindeki her konuyu gizlilik i�inde birbirleriyle payla�acaklar. Taraflar en az�ndan entelekt�el seviyede birbirlerini e�it g�recekler, g�r�� al��veri�inde bulunacaklar.

* * *

T�rkiye Cumhuriyeti ise d�� politikas�nda hem Recep Tayyip Erdo�an, hem de Abdullah G�l�e ayn� anda servis veren Ahmet Davuto�lu�nun geli�tirdi�i ve ad�na �stratejik derinlik� denen yakla��m� uyguluyor. Ahmet Davuto�lu, her iki lidere de D��i�leri Bakanl����ndan ba��ms�z ve ayr� servis veriyor.

Stratejik derinlik yakla��m�, her bir �lkeyle teke tek ve derinli�ine uzanan bir ili�kiyi i�eriyor. Uygulamada Ortado�u �lkeleri �n planda.

�ddias�, T�rkiye�yi ABD�nin d�men suyundan kurtarmak ve �lkeye �ok boyutlu ve �ahsiyetli bir uluslararas� kimlik kazand�rmak! 

Haliyle; T�rkiye �derinlik� anlay���na g�re teke tek geli�tirdi�i ili�kiler �ncesi ABD�yle isti�are yapmak durumunda de�il!

* * *

Bu anlay��a g�re; ABD ile T�rkiye aras�ndaki ikili ili�kiyi yak�ndan etkileyece�i ayan beyan belli olan T�rkiye-Suriye yakla��m�nda T�rkiye, ABD�yle isti�are yapmak zorunda de�ildir.

Olsa olsa; T�rkiye ABD�nin hassasiyetlerini g�z �n�ne alabilir.

Nitekim, T�rkiye b�yle yapt���n�; Suriye�yle g�r��melerinde Esad�a �yapma etme!� dedi�ini beyan ediyor.

Suriye�yle eylemini �nden ABD�yle enine boyuna tart���p (isti�are edip), belki de mutabakat elde edemeyince (madde 3 ve 4) �ben yine de ��yle davranaca��m� demeyi tercih etmiyor.

B�ylece de yukar�da madde 3��n son paragraf�nda ifade edildi�i �zere;

��ki taraf 3. �lke(lerle) birbirinin ana politikalar�n� incitecek eyleme girmemeye dikkat edecekler� �iar�n� yok say�yor. 

(Madde: 4) Bilgi payla��m� ve entelekt�el tart��ma ili�kide zaten ruhen yok!

* * *

Benim bu k��ede son �� yaz�da s�ylemeye �al��t���m �u:

Bir h�k�met istedi�i politikay�, �cretini �demek �art�yla vazedebilir. Garip olan kendi politikas�n�, olmad��� �ekilde, �stratejik ortakl�k� olarak yutturmaya �al��mas�d�r. Madem, Ahmet Davuto�lu ad�n� �yle koymu�; h�k�metin politikas�na olsa olsa �stratejik derinlik� doktrini denebilir.

Ancak, stratejik derinlik doktrinine g�re, ABD de t�pk� Kuzey Irak�ta yapt��� gibi bizimle �Suriye ve �ran eylemlerinde� de: i) isti�are etmek, ii) bilgi payla�mak, iii) ��karlar�m�za dikkat etmek zorunda de�ildir! 
