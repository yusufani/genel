I hate to admit this, but my wife is probably right (so what else is new?): for most of us, vegetarianism is the healthiest way to eat. Why? 
Because a vegetarian diet (and that should mean no meat intake although surveys reveal that most self-described �vegetarians� eat meat and/or fish regularly) is best for weight control, it�s best for cholesterol level control, it�s best for blood sugar level control, and on and on. 
In fact, a vegetarian diet is best for most health parameters (perhaps except vitamin B12 levels), including even, according to a recent study from the UK published in the Archives of Internal Medicine, best for blood pressure. 
In this study that examined nearly 5,000 middle-aged people in 4 countries over several weeks, those people who had the highest veggie consumption also had the lowest BP. 
In fact, there was a direct link between plant food intake and BP, so that the more fruits and veggies a study participant ate, the lower their BP was. 
Even more impressive, even a small increase in plant food intake translated to a drop in BP. 
Now, you might argue that this link actually has nothing to do with produce but rather that people who eat lots of plant food (like my wife), also do most of those other healthy things that have been linked to lower BP (like my wife) such as exercising more (like my wife, the exercise maniac) and not smoking (you guessed it � she�s never smoked), but the authors of this study claim that they took these factors into account and that there was still a strong independent link between plant food intake and better BP levels. 
Why do veggies lower BP? 
No one really knows but the usual suspects apply: veggies and plant food are loaded with minerals (potassium, magnesium, etc.) and anti-oxidants and fiber and specific amino acids, not to mention that people who don�t eat meat generally also take in much less salt than carnivores do. 
But hey, before you panic that this means that if you want to control your blood pressure, you have to look forward to a lifetime of coming up with different recipes for zucchini casseroles, you can take comfort from this meat-lover. 
You see, even if you stay a carnivore, as I surely plan to stay until they carry me out, there�s no harm from a diet that includes meat (at least not from moderate levels of meat intake). 
Rather, if you loves yer meat, you should just focus on also eating lots of fruits and especially veggies with your meat and fish. 

