Some of the stories are horrific. 

Nour Miyati, an Indonesian, grabbed at a job as a housemaid in distant Saudi Arabia. For four years, she was treated decently and could send money home.

Then she went to a new employer, who proved to be a true son of the abusive "maid culture" of Persian Gulf countries. He kept Nour prisoner, withheld her pay and inflicted torture. Gangrene set in, and she lost toes and fingers to amputation.

Her troubles didn't end there, reports Mark P. Lagon in the "Policy Review" of the Hoover Institution at Stanford University. When Nour escaped, she was arrested under the Saudis' "sponsorship laws" -- for running away! 

Lagon says hundreds flee every month from jobs in the oil-rich kingdom where 7 million migrants do the "3D work," dirty, dangerous and difficult.

Trafficking in their sweat is what Lagon calls "the slavery of our time." Typically, a job recruiter in a poor land pockets a fee from victims he'll never see again. And at the other end waits an employer-exploiter.

Selling people into nightmares at a distance is all too common, says Lagon, who heads the State Department's little known Office to Monitor and Combat Trafficking in Persons. 

He cites 25,000 men from Brazil's slums who clear timber and cut sugar in boondocks camps practically impossible to escape from. But Brazil at least keeps a "dirty list" of employers who get caught operating gulags.

There are countless unexposed cases, however, like that of Lina, 19. Recruited from Romania to England as a housemaid, she was sold to a pimp and forced into prostitution. Two young Asian servants in Long Island, N.Y., endured years of "beatings, threats and confinement" before seeking help." Their upscale employers eventually were convicted of forced labor and "document servitude," which means taking travel documents away to keep a worker trapped. It's a common trick of the trafficker..

Another is "debt bondage." The victim never gets free of an exorbitant employment "fee." Ten Filipinos paid $1,200 each to a recruiter for a Midwestern U.S. hotel. Once here, they ran into new charges for "rent" to extort endless hours.

The United States has a law against such vileness, the Trafficking Victims Protection Act of 2000. A special "R" visa lets victims remain in the promised land -- if they'll testify against their oppressors. Some 2,000 have done just that from 77 countries.

One was Molina, 30, a Mexican migrant forced to work 18-hour shifts in a California dress factory that included locked dormitories. A shrimp farm in Thailand kept 800 refugee workers from neighboring Myanmar (Burma) behind 15-foot walls topped with barbed wire.

Countries at both ends of the world's wickedest employment line should be laying down much tougher penalties, says Lagon. The norm is "suspended sentences and fines comparable to slaps on the wrist." Tough prison terms would be more like it. And even, for poetic justice, hard labor.


