#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct R{
    int w;
    int l;
    int h;
    char name[20];
}ROOM;

typedef struct H{
    char* address;
    ROOM* rooms;
    int roomCount;
}HOUSE;

void setRoomProperties(ROOM* r, int width, int height, int length, char roomName[]){
    r->h = height;
    r->w = width;
    r->l = length;
    strcpy(r->name,roomName);
}

void printRooms(HOUSE* h){
    int i;
    printf("ROOM     |     WIDTH     |     HEIGHT     |     LENGTH     |     NAME");
    for(i = 0; i < h->roomCount; i++){
        printf("\n%d     |     %d     |     %d     |     %d     |     %s",
                i, h->rooms[i].w, h->rooms[i].h, h->rooms[i].l, h->rooms[i].name);
    }
}

int main(){
    HOUSE ev;

    int i;

    printf("How many rooms there are? : ");
    scanf("%d", &ev.roomCount);
    ev.rooms = (ROOM*) malloc(ev.roomCount*sizeof(ROOM));

    for(i = 0; i < ev.roomCount; i++){
        ROOM temp;

        printf("Name of the room : ");
        scanf("%s", &temp.name);

        printf("Height of the room : ");
        scanf("%d", &temp.h);

        printf("Width of the room : ");
        scanf("%d", &temp.w);

        printf("Length of the room : ");
        scanf("%d", &temp.l);

        setRoomProperties(&ev.rooms[i], temp.w, temp.h, temp.l, temp.name);
    }
    printRooms(&ev);

    return 0;
}
