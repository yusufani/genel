/**
 * @author Yusuf Anı
 * @email yusufani8@gmail.com
 * @create date 2018-11-18 16:53:48
 * @modify date 2018-11-18 16:53:48
 * @desc [To understand how can using pointer for arrays]
*/

#include <stdio.h>
#include <stdlib.h>
void bubble_sort(int **arr, int n, int m);
void selection_sort(int **arr, int n, int m);
int main() {
    int **arr,i,j;
    arr=(int **) calloc(5,sizeof(int *));
    for(i = 0;i < 5;i++)
    {
     *(arr+i)=(int *) calloc(4,sizeof(int));   
    }
    for(i = 0;i < 5;i++){
     for(j = 0;j < 4;j++){
         *(*(arr+i)+j)=rand()%10+1;
      printf("%d\t",*(*(arr+i)+j));   
     }
     printf("\n");
    }
    bubble_sort(arr,5,4);
    printf("\nBubble sort \n");
    for(i = 0;i < 5;i++){
     for(j = 0;j < 4;j++){
      printf("%d\t",*(*(arr+i)+j));   
     }
     printf("\n");
    }
    printf("\nNew matrix\n");
    for(i = 0;i < 5;i++){
     for(j = 0;j < 4;j++){
         *(*(arr+i)+j)=rand()%10+1;
      printf("%d\t",*(*(arr+i)+j));   
     }
     printf("\n");
    }
    selection_sort(arr,5,4);
    printf("\nSelection sort \n");
    for(i = 0;i < 5;i++){
     for(j = 0;j < 4;j++){
      printf("%d\t",*(*(arr+i)+j));   
     }
     printf("\n");
    }
   return 0; 
}
void bubble_sort(int **arr, int n, int m){
    int i,j,tmp,k;
    for(i = 0;i < n;i++)
    {
        for(j = 0;j < m-1;j++)
        {
            for(k=0;k<m-j-1;k++){
                if(arr[i][k] > arr[i][k+1]){
                    tmp=arr[i][k];
                    arr[i][k]=arr[i][k+1];
                    arr[i][k+1]=tmp;
                }  

            }
        }   
    }
}
void selection_sort(int **arr,int n,int m){
    int i,j,k,min,tmp;
    for(i = 0;i < n;i++)
    {
     for(j = 0;j < m;j++)
     {
         min=j;
         for(k=j+1;k<m;k++){
            if((*(*(arr+i)+k))<arr[i][min]){
                min=k;
            }
         }
         tmp=arr[i][j];
         arr[i][j]=arr[i][min];
         arr[i][min]=tmp;
     }   
    }
}