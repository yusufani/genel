﻿Seçimlerden önce bu kitapları okuyun

Baskının, nefretin, ötekileştirmenin karşısında duran gerçek gazeteciliğe ihtiyacımız var. Dünya üstüme geldiğinde, daha çok sığınırım kitaplara. Size de tavsiyem seçimden önce şu kitaplardan en az birini okuyun.
Canım sıkkın.
Seçime günler kala oluşan havadan mı, havaların bir öyle bir böyle gidişinden mi, kişisel olarak havamın bozuk oluşundan mı; bilmiyorum. Ama canım sıkkın.
Ölümlerin, cinayetlerin, haykırışların, azarlamaların ve en hafif deyimiyle deliliklerin arasında nefes almaya çalışmaktan mı bilemem; ama sıkkın işte.
“Haksızlığa uğramak” acımasızlığının bu kadar kolaylıkla içselleştirilmesini anlayamadığımdan, içime sindiremediğimden belki de; ne önemi var, sıkılıyorum.
Nasıl olur da bütün fiiller öfkeli olur bir coğrafyada?
Gazete okumaya çalışın, bu sıkıntıdan kurtulmak için. Bilgilenmek iyidir. Bir yanda kadın cinayetleri, bir yanda Survivor adasındaki cinsellik haberleri. “Kim kiminle öpüştü” haberine ilgi daha fazla elbette. Nasıl olur da, yaşadığımız topraklardan daha büyük ilgi duyarız bir adada yaşanan sahte gerçekliğe?
“Haberin okunsun istiyorsan ya şiddet olacak başlığında ya da cinsellik” diyen bir gazeteciliğin avlusunda volta atıyorsun. Canını sıkma yine de... Olmuyor ama...
Gazeteci gibi gazetecilik yapanlar, koridorları türlü hileyle dolu bir korku tünelinde yaşamaya mahkum ediliyor, nasıl sıkılmasın canım.
“Sayıklamalarını gazete yazısı diye yutturmaya kalkma” diyenler vardır şimdi, haklısınız. Ama böylesi yalan söylemekten iyi geldi. Çünkü yüreğim sıkışıyor bugünlerde.
Baskının, nefretin, ötekileştirmenin karşısında duran gerçek gazeteciliğe ihtiyacımız var. Dünya üstüme geldiğinde, daha çok sığınırım kitaplara. Sözü fazla uzatmadan, sizin de içinizi sıkmadan önerilere geçeceğim.
Seçimden önce aşağıdaki kitaplardan en az birini okumanızı tavsiye ederim.
Haberler “Bir Kullanma Kılavuzu” – Alain de Botton

 
“Haberlerin uğultusu ve telaşı benliğimizin en derinlerine sızmış vaziyette, artık bir dakikalık bir sükûnet ne büyük başarı sayılıyor” diyor Alain de Botton. Peki hiç bu “haberlerin” üretim cephesinin, yani haberciliğin bugününü ve yeni dinamiklerini düşünüyor muyuz? Evet, “haber kuruluşları, demokrasilerin barındırdığı rastlantısal bir özellik olmaktan öte, onların bizatihi kefilleridir” ama acaba “şimdiki durum” ne? Sarsıcı örnekler ve bakış açısı olgunlaştıran bir yaklaşımla haberciliği ele alıyor Alain de Botton. (Zeynep Baransel çevirisiyle, Sel Yayınları’ndan)
Asılı Adam “Ai WeiWei’in Tutuklanışı” – Barnaby Martin

1989 yılında bugün, Tiananmen Meydanı’nda 2binden fazla öğrencinin ölümüyle sonuçlanan o müdahale gerçekleşmişti Çin’de. Dört tankın önüne tek başına dikilen o ‘Meçhul İsyancı’nın fotoğrafı çoğu kişinin belleğindedir. Peki gerçekten neler oldu Çin’de, son otuz yılda neler yaşandı? Weiwei meselesine, modern sanatlara ve hatta Çin’in dünyayla olan ilişkisine uzak olanlar için bile kaçırılmayacak bir okuma fırsatı. Barnaby Martin, bu zorlu konuyu, neredeyse çoksatan bir roman tadında aktarmayı bilmiş. Sanatçının babası Ai Quing’in Mao ile olan ilişkisinden, Weiwei’in sorgulanma ve tutukluluk sürecine, Çin’in dünya siyaset sahnesindeki yeni rolünden modern sanat pazarının oyunlarına uzanan bir anlatı. Sadece sanatçının düşünce alanıyla değil, bireysel özgürlüklerin sınırlarıyla ilgilenen herkesin okuması gerekiyor. (Haluk Barışcan çevirisiyle, Metis Yayınları’ndan)
Toprağın Öptüğü Çocuklar “Adaleti Beklerken Roboski” – Sibel Oral

“Toprağın Öptüğü Çocuklar” bir büyüme öyküsü sanki. Hiç büyüyemeyecek çocukların hikayelerini takip eden okurların büyüme öyküsü. Okuduğumuz her bir satır, bizi bir cümle daha büyütüyor. Yüzleşmeden ve hesaplaşmadan büyüyemeyecek bir toplumun sorumluluğunu yüklenen Sibel Oral sayesinde, korunaklı dünyamızdan çıkıp büyümeye başlıyoruz. İşte bu yüzden “Toprağın Öptüğü Çocuklar” herkesin okumasını istediğim bir kitap. Suça ortak olmak istemeyen, o vicdan yükünü sadece yazarın taşımasına izin vermeyecek herkesin... Şimdilik geride bir cümle var: Roboskî hâlâ adalet bekliyor. (Can Yayınları etiketiyle.)
Türkiye’de Gazetecilik Masalı “Saray’dan Saray’a” – Ümit Alan
Türkiye'de gazetecilik -gerçek anlamda, hayallerimizdeki gibi bir gazetecilik- mümkün mü? Her dönemde gizli bir gündemi oldu mu gazetecilerin? Kapalı kapıların en çok devrede olduğu meslek grubu mu var karşımızda? İktidar-basın ilişkisi hangi dinamiklerle oluşuyor? Görevi 'göstermek' olan gazeteciler neden 'örter'? Samimiyet ve dürüstlük çeşmesinden su içmek çok mu zordur gazeteci için? Hep bir 'masal' olarak mı kalacak bu meslek? Ümit Alan bu inceleme kitabında Türkiye'de gazetecilik tarihinin izini sürüyor. Resmi kayıtları sivil tanıklıklarla karşılaştırarak. Bugünü dünle ilişkilendirerek. Dünü bugünün dinamikleriyle anlamaya çalışarak. Bu konuda önceden yazılmış kitaplarla-yazılarla ilişkisini kesmeden ama onların gölgesinde kalmayarak. Bu konuda yeni cümleler kurmanın, günümüzü otopsi masasına yatırmakla mümkün olduğunu bilerek. (Can Yayınları’ndan)
Ali İsmail “Emri Kim Verdi?” – İsmail Saymaz

Gezi Direnişi’nin ikinci yılı geride kaldı. Bu geçen iki yıl boyunca aklımızdan bir an bile silinmeyen isimlerden biri Ali İsmail. Zorlu bir kitap bu. Kapaktaki Ali İsmail fotoğrafına, o fotoğrafın gözlerine bakmak kadar zorlu. İsmail Saymaz tek "suçu" polis şiddetinden kaçmak olan Ali İsmail'in ölümüne neden olan olaylar zincirini ve bu cinayeti örtmek için oluşturulan örgütlenmeyi bir detektif titizliğiyle, en ince detayına kadar incelemiş. (İletişim Yayınları’ndan)
Ve son olarak...
1963’te bugün bu dünyadan ayrılan Nazım Hikmet’in her bir dizesinin önünde saygıyla eğilerek noktalayayım yazıyı.
“Canım sıkkın be Nazım Hikmet, sen de olmasan zor bu dünya.”