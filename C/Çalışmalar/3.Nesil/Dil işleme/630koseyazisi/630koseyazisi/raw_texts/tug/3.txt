Marie Antoinette'nin ikizleri 

 
 

Nerede kalm��t�k d�n? Yazar�n�z �arap tad�m dersleri almaktayd� ve hafiften kafay� bulmu�tu.

Esasen �arap tad�m dersi al�rken kafa bulmak �art de�il. �arab�n tad�n� ald�ktan sonra �n�nde duran kovaya t�k�rebiliyorsun. Fakat b�yle bir �eyi yapmam elbette ki m�mk�n de�ildi.. O g�zeller g�zeli mayiyi ziyan etmek..

Fakat �u da bir ger�ek ki �araptan al�nmas� gereken ve gerekmeyen kokulan ve tatlar� ��rendik�e o "g�zeller g�zeli mayi" "rezalet bir �ey" haline de gelebiliyor. Neden? ��nk� iyinin ne oldu�unu ��reniyorsun ve y�llard�r afiyetle i�ti�in X marka �arab�n mesela ne kadar rezalet bir �ey oldu�unu fark ediyorsun. �arap Kursu alacaklar�n (www.sarapkursu.com'dan bilgi edinebilirsiniz) �nceden bunu da d���nmeleri gerekiyor. Hadise san�ld���ndan daha pahal�ya patlayabiliyor. Sonu� itibanyla 800 euro'luk bir k�rm�z� �arap (Chateau Haut Brion 1989), 600 euro'luk bir �ampanya (Dom Perignon!!!) i�mi� bir �ah�s�m art�k! Bunlardan a�a��s� ne kadar tatmin eder beni derin ��pheler i�indeyim! ��in �akas� bir yana asl�nda �aka de�il ya neyse- iki g�n boyunca o kadar �ok �ey ��rendim ki bunlar� burada nas�l yazaca��m konusunda en ufak bir fikrim yok.

En iyisi beni en �ok e�lendiren konulan madde madde yazay�m:

- �yi �arap berrak ve parlak olmal�. Koklay�nca meyve, baharat ve �i�ek kokular� gelmeli. Aseton, ah�ap, �slak bez, k�f, rutubetli ev kokusu ve d�n sayd���m o korkun� kokular gelmemeli. (Benim �arab�m yapmaz �yle �ey diyorsan�z e�er bir de �imdi koklay�n!)

�ampanya bir yanl��l�k sonucunda olmu� bir �ey. Y�llarca �arab�n k�p�rmesini ("�eytan�n �arab�") nas�l �nleriz diye u�ra�m��lar, millet sevince t�pay� de�i�tirmi�ler.

-Jartiyer'in ad� "La Jarretiere" isimli bir �ampanya markas�ndan geliyor. Kan kan k�zlar� �apk�n erkeklere �orap lastiklerine s�k��t�rd�klar� bardaklardan "La Jarretiere" marka �ampanya ikram ederlermi�. �orap lasti�inin ad� jartiyer olmu�.

- K�rm�z �arap ve peynir bir arada yenemiyor arkada�lar. ��nk� peynir k�rm�z� �arab� yusyuvarlak yap�yor. Tad�n� daralt�yor ve tek bir tad� oluyor. K�t� ucuz �araplar� i�ebilmek i�in peynir kullanabilirsiniz ama iyi �araplarla asla. (Beyaz oluyor)

- Shiraz diye bir �z�m yok asl�nda. Asl� Syrah. Ama k�yl� Avustralyal�lar�n dili d�nmedi�i i�in diktikleri Syrah �z�m�ne Shiraz diyorlar, sonra da bir pazarlama harikas� olarak kakal�yorlar.

- 16. Louis'nin me�hur kar�s� Marie Antoinette �ampanyay� �ok severmi�. Kendi memi�lerinin al��dan kal�b�n� ald�rm�� ve o yuvarlakl�kta kadeh yapt�rm��. O al�� kal�p h�l� duruyormu�. (Ablan�nkiler pek iri de�ilmi� anla��lan) Uzun bardak adetine ge�ilmesi ise kan kan k�zlar� sayesinde olmu�. �orap lastiklerine s�k��t�rmak i�in uzun bardaklara koymu�lar, daha �ok sevilmi�.

-Uzun bir s�re kad�nlar�n �arap i�mesi g�nahm��. Antik �a�da kocalar�n �arap i�mi� kar�s�n� an�nda �ld�rme hakk� varm��. Dudaktan �p��meyi, Spartal�lar�n kanlan �arap i�mi� mi i�memi� mi kontrol etmek i�in icat ettikleri iddia ediliyor. (Vay be! Onlar olmasa o harikulade eylemi yapmayacakt�k yani! Allah onlardan raz� olsun!)

��te b�yle.. Daha �ok mevzu var ama yerimiz bildi�iniz �zere dar, ancak bu kadar. 

 
