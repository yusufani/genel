The public backlash against bankers and their bonuses is widespread and deep. But there are stark gender differences, with women more hostile than men, and professionals and managers more sympathetic to bankers than other social groups. Populus did a special poll for The Times of 503 adults on Wednesday evening, after former and current bank chairmen and senior executives had given evidence to the Treasury Select Committee. 

Bankers have suffered the double whammy of losing their equivocal status as masters of the universe and being seen as popular scapegoats for Britain�s severe economic troubles. But there is little comfort for Labour either. Nearly nine out of ten voters (89 per cent) say that, before bailing out the banks, the Government should have ensured that there would be no possibility of any of that money being used to pay bonuses to bank employees. 

The profuse apologies and acceptance of responsibility offered this week by the former executives are accepted by less than half the public (45 per cent), with 50 per cent disagreeing. 

Otherwise, the key points are that a clear majority (64 per cent in each case) believe both that partly nationalised banks should not be permitted to pay any bonuses at all to their staff and, even more strikingly, that those senior bankers who took decisions that led to taxpayer-funded bailouts should not only not get bonuses, but they should be forced also to repay the big bonuses they have received in previous years. 

When Harriet Harman raised the question of such retrospective action and the clawing back of past bonuses at Tuesday�s Cabinet meeting, she was strongly opposed by John Hutton and Hazel Blears, among others. Recognising the force of public opinion on the issue, Gordon Brown wants tighter controls in future to ensure that bonuses are based on sustainable performance, not only on one good year. More than four fifths of voters back capping of both the salaries (82 per cent) and bonuses (85 per cent) paid to senior bank staff, so that �never again will bankers earn the enormous sums they have in the past�. In the case of bonuses, the cap would apply �regardless of how profitable they may become in the future�. 

However, the public does not entirely reject bonuses. Two thirds (68 per cent) agree that, while those senior employees responsible for decisions that led to huge losses should not get bonuses, those who have worked in profit-making parts of the banks should be able to receive bonuses as usual. Moreover, only a narrow majority (50 to 46 per cent) reject the view that it is in the country�s best interests to allow bonuses to be paid out as in the past, �because the most capable senior staff may otherwise leave and go to work instead for foreign banks that still offer big bonuses�. 

There are big underlying divisions within the public. While men agree by 52 to 47 per cent that bonuses need to be paid to stop a brain drain, women disagree by 53 to 42 per cent. Similarly, on paying bonuses to those in profitable parts of banks, men agree by 73 to 25 per cent, but women do by 62 to 37 per cent. Professionals and managers agree by 75 to 23 per cent, though unskilled workers by 61 to 35 per cent. 

Men narrowly accept (by 49 to 48 per cent) that this week�s apologies were sincere and indicate acceptance of responsibility. But a clear majority of women (52 to 43 per cent) reject this view. 

On most of these questions, men tend to have stronger opinions than women. They agree strongly, rather than just somewhat, on high levels of bonuses and on the apologies. 

Bankers have a long way to go to restore their reputations. Saying sorry is not nearly enough. 

