Başkanlığı neden istiyor?

'Ya Nobel alacak, ya da politik intihar riskini üstlenecek.' 
Kolay yolu seçse, Cumhurbaşkanı olmasının önünde neredeyse hiçbir engel yok. Ama mevcut sistemle. Başka, belki tercih etmediği bir Başbakanla...
Oysa Başkanlığı zorluyor. Halkın seçtiği ilk Başkan olmayı istiyor.
Çok kararlı görünüyor. Israrlı da...
'Hamle' değil 'hamleler' dedim...
Zira sadece 'Erbil-İmralı sürecini' kastediyor değilim. 
O önemli, ama sadece birisi...
Her biri diğerlerini de etkileyecek ve belirleyen iç içe geçmiş çoğul hamlelerden bahsediyorum. İdari mekanizmayı, hukuk sistemini değiştirecek Başkanlık sistemi arayışları da dış politika tercihleri de aynı stratejik hedeflemenin unsurları...
BDP ile referandum işbirliği...
Çok tehlikeli değil mi?
KCK'lıları da dışarı çıkaracak yargı paketi...
Riski yüksek değil mi?
Balyoz ve Ergenekon tutuklularını kapsayacak bir af...
Anlatmanız, iklimi değiştirmeniz gerekecek. 
Son adımlar, ziyaretler, mesajlar bunun altyapısı. Şimdilik çok da iyi gidiyor. 
Erdoğan sonunu getirebilir, başarabilirse, güçlü bir özgürlük ve barış rüzgarı estirecek. Halk oyuyla iş başına gelmiş ilk Başkan da olacak. Nobel Barış Ödülü'ne aday gösterilecek. Kazanır. 
Bütün bunlar yalnızca bir politikacının kişisel hedefleriyle ilgili ve sınırlı görülemez. Her fani gibi bunları düşünüyordur elbette. İstiyordur. 
Ötesi de var. Ülkenin kaderi ve bölgenin geleceği açısından anlamı ve mesajı olacak hamleler. Getirisi de riski de geniş... Hem kişisel hem kurumsal... Tarihsel...

Kürtlerin gözü Türkiye'nin üzerinde

Başlıktaki sorunun yanıtını vermeli.
Erdoğan, Başkanlığı neden istiyor?
Ortadoğu dengeleri penceresinden bakmaya çalışıyorum. 
Irak mevcut haliyle kalacak gibi değil. Er ya da geç bölünecek. Belli. 
Maalesef Suriye de öyle. Sadece zamanlama meselesi.
İran'ın ne olacağı muamma. 'Bahar' denilen gösterilerin ilk kıvılcımı Tahran sokaklarında görülmemiş miydi?
İçinde Kürt nüfus barındıran bütün ülkeler 'telaş', hadi telaş demeyelim de 'alarm' durumunda. 
Türkiye'nin hızlanması da bundan. Kaybedecek zaman olmadığını görüyor devlet. 
Anadolu'nun Kürtleri ayrılmak, bölünmek istemez. İhtimal dışı...
'Sırf petrolü var' diye Kuzey Irak'a da bütün Ortadoğu'nun abisi ve hamisi rolü verilemez. 
Ayrıca Kuzey Irak, tek zenginliğini bütün coğrafyayla paylaşmaya yanaşmaz. Erbil, refahını artırmak ve Ankara üzerinden dünyaya açılmanın derdinde. Araplar'dan kurtulmaya çalışıyorlar. 
Türkiye, kendini Doğu ve Güneydoğu dahil olmak üzere refah, barış, demokrasi ve özgürlük coğrafyasına dönüştürebilecek mi? Yani cazibe noktası haline gelebilecek mi?
Hem kendi Kürt nüfusunu mutlu edecek, hem de Irak'ın, Suriye ve hatta İran'ın Kürt azınlığının hayranlığını kazanabilecek mi? 
Bütün Ortadoğu'ya parlak bir gelecek, güvenlik, umut ve istikrar vaat edebilecek mi? 
Sanıyorum, Erdoğan bir çeşit federatif sistemin zorunlu olacağını düşünüyor. İlan edilmemiş de olsa yerinden yönetimi güçlendirerek bunu yapmaya çalışıyor. Başkanlığı da bu nedenle istiyor. Belki de bütün Ortadoğu Kürtleri'ni yönetecek idari sistem peşinde. Büyük hedef değil mi? Öyle. Riskli de...