Cutting high blood pressure in middle age could save thousands from suffering and dying from dementia, experts claim.
They say even older people benefit from using blood pressure drugs because treatment results in a lower risk of vascular dementia.
Research from two separate studies shows reducing blood pressure does not only prevent heart disease and strokes but also combats devastating brain disease, which can lead to premature death.
 
Results from the Hypertension in the Very Elderly Trial suggest controlling hypertension - the medical term for high blood pressure - could lead to a 13 per cent fall in dementia risk in those aged 80 and over, according to a report published today in The Lancet Neurology.
At the same time, a review of nine studies - to be published later this year - found treating blood pressure among the middle-aged could cut by 600 per cent the risk of developing vascular dementia, which is caused when arteries narrow and restrict blood to the brain.
The Alzheimer's Society released data, involving 8,500 people whose blood pressure was checked around the age of 50.


It said early results from the review show treating hypertension in mid-life could prevent around 15,000 people a year dying from vascular dementia.

Chief executive Neil Hunt said: 'People fear dementia more than any other condition in later life; it is a devastating disease that robs people of their lives. 
'Everyone should get their blood pressure and cholesterol checked regularly and receive effective treatment if they are at risk.
'Having a low salt diet, maintaining a healthy weight and regulating your alcohol intake can also help tackle high blood pressure.'
Dementia affects more than 700,000 Britons, with about 500 new cases diagnosed every day. Most suffer from Alzheimer's.
Rebecca Wood, Chief Executive of the Alzheimer�s Research Trust, said: 'This is an exciting development, with the potential to enhance thousands of lives. 
'We urgently need more research like this if we are to offer hope to the 700,000 people in the UK who live with dementia.'
Cathy Ross, cardiac nurse at the British Heart Foundation said: 'Taken together, the two studies really state the case for people over 40 asking your GP for a health check. 

'This could flag up risk factors such as high cholesterol or blood pressure and begin to get them sorted out before they take their toll.'

