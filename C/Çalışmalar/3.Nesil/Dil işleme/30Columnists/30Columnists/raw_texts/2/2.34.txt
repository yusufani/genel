THE first elections to the Scottish Parliament back in 1999 put the Liberal Democrats into government for the first time in more than half a century.

It was a heady time for the party which had campaigned longer than any other for devolution.

Now, their eight years in the limelight sharing power with Labour is over, and the party is having to adjust to the obscurity of opposition.

As delegaADVERTISEMENTtes gather for the Scottish Lib Dem annual conference in Aviemore this weekend, they are congratulating themselves that at least they are making a better job of the transition than Labour.

However, as one senior figure Lib Dem admits: "Our problem is we struggle to get noticed."

The Lib Dems have just 16 out of the 129 MSPs, making them the fourth party at Holyrood. Their share of the constituency vote has increased steadily over the three elections since devolution � but only by two per cent � and that has been accompanied by a decline in their regional vote.

But when the Scottish Parliament was first set up, it looked as if the proportional representation voting system meant the Lib Dems could be in power forever. As the party in the middle, they would be the natural coalition partner for whoever ended up with the most seats.

Even during last year's election campaign, it seemed very much on the cards the Lib Dems would find a way around their opposition to the independence referendum which the SNP was insisting on, and do a deal with the Nationalists. But that, of course, didn't happen.

Even after Alex Salmond had been sworn in as First Minister, some predicted the SNP government would not last; without a majority, the Nationalists would soon find it impossible to stay in power; the Lib Dems and Labour might be back sooner than anyone thought. That didn't happen either.

The Lib Dems claim they are the only ones offering any real opposition at Holyrood. 

One senior figure says: "The Tories are in a tactical alliance with the Nationalists and Labour are in disarray, so the job falls to us."

Yet, apart from some well-aimed blows from Nicol Stephen at First Minister's Questions, the party has little to show for its first nine months of opposition.

The Lib Dems claim they exposed the "flaws" in the SNP's budget � but when it came to the vote, they abstained and the plans were approved.

The Tories can at least claim to have used their support for the budget to force the SNP to put more money into police recruitment than it had planned and speed up the cuts in business rates.

Lib Dem insiders say the party did approach the Government behind the scenes to press for better funding for universities, but was rebuffed.

Nevertheless, party leaders insist they have no regrets about not going into coalition with the SNP. 

One says: "In an age when parties flip-flop all over the place, we made our position clear � we could not support a government which was intent on a referendum on independence � and we stuck to it."

Another senior Lib Dem admits some disagreed with the decision at the time, but claims the further away they get from the election the fewer regrets there are.

"It was a difficult decision to make � not to go into power when there was an opportunity to do so � but it has been proven right."

Former Lib Dem MSP Donald Gorrie is not so sure. He would have been happy to see an SNP-Lib Dem coalition and says he argued the party should be ready to consider such a partnership back in 1999. 

"I said if you had a coalition, after four years, you could say we will have a referendum along with the election. But that was considered traitorous."

He believes even in opposition the Lib Dems could contribute more. "I think they are still to inclined to see it as the duty of the opposition to oppose. 

"They should look for more constructive dialogue where there are points of agreement, though to what extent the Nationalists are being difficult I don't know.

"It should be one of the merits of minority government that the parliament should reassert itself and you have a combination of people where there is cross-party support for sensible things, but they are all sitting in their own little tents.

"There are higher things than the minutiae of party warfare, but I'm afraid lots of people don't see it that way."

A Lib Dem MSP accepts the party could perhaps have taken a more positive role on occasion, but says on many issues where there could have been co-operation the SNP made no approach.

"The Lib Dems were accused of taking our ball away and refusing to play over the idea of coalition, but it could be said the SNP have been doing a bit of that since then."

IN the immediate aftermath of last year's election, there was speculation that Nicol Stephen might not last much longer as leader. He was judged not to have fought a good campaign, and may have thought he would soon be replaced by Tavish Scott.

Yet insiders say Mr Stephen is now on "pretty solid ground". His attacks on Mr Salmond � for example, responding to one jibe by quoting a poll where voters described the First Minister as arrogant and patronising � have hit home.

"Alex and Nicol are clearly not people who are going to be going out for a curry on a Friday night," says one colleague.

Another senior party figure predicts the conference will be an important test for Mr Stephen, saying: "He doesn't have the personality of Alex Salmond, but he is clever and sharp. This will be an opportunity for him to set the direction for the party to go forward."
