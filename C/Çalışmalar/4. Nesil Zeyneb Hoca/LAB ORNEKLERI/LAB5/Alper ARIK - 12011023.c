#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct ogrenci{
	char ogrIsim[16];
	int ogrNot;
	struct ogrenci *next;
};
struct ogrenci *head=NULL;
struct ogrenci *curr=NULL;

void menu(void);
struct ogrenci* createList(char *,int );
struct ogrenci* addToList(char *,int );

int main(void){
	char k,ogrIsim[16];
	int ogrNot,sayac=0;
	float ort=0.0;
	struct ogrenci *ptr=NULL;
	
	do{
		menu();	fflush(stdin);
		scanf("%c",&k);
		switch(k){
			case '1' :	printf("\nOgrencinin ismini giriniz:");
						scanf("%s",ogrIsim);
						printf("\nOgrenci notunu giriniz:");
						scanf("%d",&ogrNot);
						ptr=addToList(ogrIsim,ogrNot);
						break;
						
			case '2' :	
						while(head!=NULL){
							printf("\nOgrenci ismi: %s",head->ogrIsim);
							printf("\tOgrenci notu: %d\n",head->ogrNot);
							sayac++;
							ort+=head->ogrNot;
							head->ogrNot;
							head=head->next;
						}
						break;
						
			case '3' :
						printf("\n\nOrtalama %f dir.",ort/=sayac);
						break;
			
			default :  printf("\nTekrar giriniz.");
						
		}
	}while(k!='4');
	return 0;
}

void menu(void){
	printf("\n1.Ogrenci ekle");
	printf("\n2.Goruntule");
	printf("\n3.Ortalama");
	printf("\n4.Cikis");
}

struct ogrenci* createList(char *ogrIsim,int ogrNot){
	struct ogrenci *ptr;
	ptr=(struct ogrenci*)malloc(sizeof(struct ogrenci));
	if(!ptr){
		printf("\n Cannot allocate memory");
		exit(1);
	}
	strcpy(ptr->ogrIsim,ogrIsim);
//	ptr->ogrIsim=ogrIsim;
	ptr->ogrNot=ogrNot;
	ptr->next=NULL;
	head=ptr;
	curr=ptr;
	return ptr;
}

struct ogrenci* addToList(char *ogrIsim,int ogrNot){
	if(head==NULL){
		return createList(ogrIsim,ogrNot);
	}
	
	struct ogrenci *ptr;
	ptr=(struct ogrenci*)malloc(sizeof(struct ogrenci));
	if(!ptr){
		printf("\n Cannot allocate memory");
		exit(1);
	}
	strcpy(ptr->ogrIsim,ogrIsim);
//	ptr->ogrIsim=ogrIsim;
	ptr->ogrNot=ogrNot;
	ptr->next=NULL;

	curr->next=ptr;
	curr=ptr;
	
	return ptr;
}
