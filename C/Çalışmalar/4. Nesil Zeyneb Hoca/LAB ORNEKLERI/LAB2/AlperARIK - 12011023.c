#include<stdio.h>
#include<stdlib.h>
struct ogr *ogrOlustur(int );
void setOgrBilgi(int,struct ogr*);
void getOgrOrt(int,struct ogr*);

struct ogr{
	char ad[16],soyad[28];
	int note;
};

int main(){
	int n;
	struct ogr *ar;
	
	printf("\n Olusturulacak ogrenci sayisini giriniz:");
	scanf("%d",&n);
	
	ar=ogrOlustur(n);
	setOgrBilgi(n,ar);
	getOgrOrt(n,ar);
}

struct ogr* ogrOlustur(int n){
	struct ogr *ar;
	ar=(struct ogr *)malloc(n*sizeof(struct ogr));
	return ar;
}

void setOgrBilgi(int n,struct ogr *ar){
	int i;
	
	for(i=0;i<n;i++){
		printf("\n%d. ogrencinin adini giriniz :",i+1);
		scanf("%s",ar[i].ad);
		
		printf("\n%d. ogrencinin soyadini giriniz : ",i+1);
		scanf("%s",ar[i].soyad);
		
		printf("\n%d. ogrencinin  notunu giriniz : ",i+1);
		scanf("%d",&ar[i].note);
	}
}

void getOgrOrt(int n,struct ogr *ar){
	int i;
	float ort=0.0;
	
	for(i=0;i<n;i++){
		ort+=ar[i].note;
	}	
		
	printf("\n\nSinavin  ortalaması %0.f dir",ort/n);
}
