Komutanlarla Tunceli Cemevi'nde  
     

  
TUNCEL�

EL�NDE oklava benzeri sopa bulunan pos b�y�kl� ki�i, sa� elini kalbinin �zerine koyup ba��n� hafif�e e�erek bizi kar��l�yor. 

�kinci kattaki geni� salona giriyoruz. 

Kad�nlar, erkekler, �ocuklar duvar �evresine s�ralanm��lar. 

Kap�n�n tam kar��s�nda, minderlerle seki haline getirilmi� uzun postta, pos b�y�kl� iki ki�i ve bir gen� oturuyor. 

�nlerinde de iki saz.

Hac�bekta� Veli �zerine bilimsel �al��malar yapan Gazi �niversitesi �leti�im Fak�ltesi Dekan� Prof. Alemdar Yal��n, bu ki�ileri tan�ml�yor:

��Sa�da, pir makam�nda oturan Mahmut Dede. Solda m�r�it makam�nda oturan Uzun Mehmet Dede. Gen� olan zakir; deyi�leri sazla �al�p s�yleyecek. 

Bizi kar��layan fara���yd�, di�eri g�zc�, ortada duran da meydanc�..��

ALLAH ZEVAL VERMES�N

Selam verip biz de duvar�n k��esindeki minderlere ili�iyoruz. 

Yeni�eri b�y�kl� iki dede gelip yan�m�za oturuyor. 

Biri salonu g�z�yle tararken, hizam�zda s�ralanm�� k�sa sa� kesimli erkek gruba odaklan�p kal�yor.

Kolunu dirsek hizas�ndan kald�r�p uzun b�y�klar�n� s�vazlarken, o ki�ileri salonda g�rm�� olman�n sevincini yan�ndaki dedeyle payla��yor: 

��Bak komutanlar�m�z da gelmi�, sa�olsunlar. Allah zeval vermesin.��

Merak edip, i�aret etti�i ki�ilerin kim oldu�unu soruyoruz. 

G��s�n� hafi�e ileri do�ru itip ��yle diyor: 

��Genelkurmay Kurmay Ba�kan�m�z ve emrindeki komutanlar�...��

Do�an Haber Ajans� muhabiri arkada��m�z Ferit Demir d�zeltiyor:

��4. Komando Tugay� Kurmay Ba�kan�m�z Yarbay Fatih Musa ��nar. Yan�ndakinin biri binba��, di�erleri y�zba��.��

VAL� DE CEMDE

Bu s�rada Tunceli Valisi Mustafa Erkal ve e�i G�lhan Erkal kap�da g�r�n�yor. Al�akg�n�ll� bir tav�rla kenara ge�ip oturuyorlar. 

Bak�yorum, salonun yar�s�n� biz doldurmu�uz. 

Biz derken; Bas�n Yay�n Enformasyon Genel M�d�rl���'n�n d�zenledi�i Yerel Medya semineri dolay�s�yla Tunceli'de bulunan gazeteciler, �niversite ��retim �yeleri ve Bas�n Yay�n Genel M�d�rl��� b�rokratlar�.

Pir makam�ndaki Mahmut Dede, �u c�mlelerle s�ze giriyor:

��Cemimiz ba�layacak. E�er aram�zda k�sk�nler ve darg�lar varsa cem ba�lamaz. K�sk�nl���n�, darg�nl���n� gizleyenler bir daha ceme al�nmaz, koyunu koyunumuza, s�t� s�t�m�ze kat��t�r�lmaz. Varsa ��ks�n s�ylesin.��

DARGIN, K�SK�N VAR MI?

Hepimiz birbirimize bak�yoruz. Fara��� ve meydanc� �evreyi tar�yor, bir kez de onlar �a�r� yap�yor... Kimse ��km�yor.

Mahmut Dede, Bekta�i-Alevi deyi�leri ��G�lbang��larla ceme ba�l�yor. 

Ard�ndan ��ra�a geliyor ve �zerindeki �� mum t�renle yak�l�yor.

Mahmut Dede, ����te y�llard�r �zerinde yalan kopar�lan mum olay� budur�� diye a��klama getiriyor...

G�lbanglar ve Kuran'dan ayetleri saz �alarak T�rk�e s�yl�yor.

A�z�ndan ��kan her ��Allah, Muhammed Mustafa, Ali, H�seyin, Hasan�� kelimesinden sonra salondan ��Allah, Allah�� sesleri y�kseliyor. 

Zakirin ba�lang��taki sinirli hali ge�iyor, saz� eline al�p yan�k sesiyle ��turnalar�� deyi�ine ba�lay�nca salondakiler co�uyor.

�nce ihtiyarlar, ard�ndan gen�ler semah d�nmeye ba�l�yor.

S�p�rgecinin dualar� toplamas� ve ��lokma�� da��t�m�yla cem son buluyor. 

D��ar� ��karken vali ve komutanlar salondakilerle sar�l�p vedala��yor. 

Ya�l�lar ise onlar�n s�rtlar�n� s�vazlay�p, arkalar�ndan dualar ediyor.

Tunceli'de iki y�la yak�n zamand�r tek olay olmam��. 

�ehir i�indeki polis kontrolleri kald�r�lm��. 

93 bin olan n�fusunun 40 bini asker, polis ve kamu g�revlilerinden olu�an kentte, toplumsal bar�� yerle�meye ba�lanm��.

Y�re halk�n�n de�erleriyle b�t�nle�ilmesi, huzuru ve s�k�neti de getirmi�.
 
