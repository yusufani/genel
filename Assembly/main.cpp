#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include "image_processing.cpp"

using namespace std;

void sagaDondur(short n, int resim);
void solaDondur(short n, int resim);

int main(void) {
	int M, N, Q, i, j, k;
	bool type;
	int efile, islem;
	char resimadi[100], sonek[10];
	do {
		printf("Islem yapilacak resmin yolunu (path) giriniz:\n-> ");
		scanf("%s", &resimadi);
		system("CLS");
		efile = readImageHeader(resimadi, N, M, Q, type);
	} while (efile > 1);
	printf("%s\n", resimadi);
	int** resim = resimOku(resimadi);

	short *resimdizi;
	resimdizi = (short*) malloc(N*M * sizeof(short));

	for (i = 0; i < N; i++) 
		for (j = 0; j < M; j++) 
			resimdizi[i*N + j] = (short) resim[i][j];

	int resimadres = (int) resimdizi;

	do {
		system("CLS");
		printf("Ogrenci isim:Yusuf Ani\nNumara:16011033\nGrup:2\n");
		printf("\t     ISLEMLER\n");
		printf("------------------------------------\n");
		printf("1)  Resmi saga dondur\n");
		printf("2)  Resmi sola dondur\n");
		printf("0)  Cikis\n\n");
		printf("\"%s\" yolundaki resim icin yapilacak islemi seciniz\n-> ", resimadi);
		scanf("%d", &islem);
	} while (islem > 2 || islem < 0);

	switch (islem) {
		case 0:
			exit(0);
		case 1:
			sagaDondur(N, resimadres);
			strcpy(sonek, "_sag.pgm");
			break;
		case 2:
			solaDondur(N , resimadres);
			strcpy(sonek, "_sol.pgm");
			break;
		default:
			strcpy(sonek, "_orj.pgm");
			break;
	}

	for (k = 0; k < N * M; k++) {
		j = k % N;
		i = k / N;
		resim[i][j] = (int)resimdizi[k];
	}
	
	string::size_type pos = string(resimadi).find_last_of(".");
	resimadi[pos] = '\0';
	strcat(resimadi, sonek);
	resimYaz(resimadi, resim, N, M, Q);
	printf("\nIslem basariyla tamamlandi :)\n\"%s\" yolunda resim olusturuldu.\n\n", resimadi);
	system("PAUSE");
	return 0;
}

void sagaDondur(short n, int resim) {
	//KODUNUZU BURADAN BASLAYARAK YAZINIZ
		__asm {
			MOV CX, n											// CX'DE DIZININ BOYUTUNU TUTUYORUZ    									
			MOV EDI, resim										//RESMIN ADRESINI EDI'DA TUTTUK	
			XOR ESI, ESI 										//ESI DEGERINI SIFIRLADIK	
			XOR EBX, EBX										//EBX DEGERINI SIFIRLADIK	
			DISDON : PUSH CX										//DISDONUN CEVRIM SAYISINI PUSHLADIK.IKINCI DONGU ICIN CX AYARLAMAMIZA GEREK YOK.	
					 MOV EBX, ESI									//J INDISIMIZ I INDISIN DEGERINDEN BASLIYOR		
					 ICDON : PUSH CX								//CX ARA ISLEMLERDE	KULLANILMAK UZERE STACK E ATILMISTIR.		
							 MOVZX EAX, n						//N DEGERINI MOVZX YARDIMI ILE EAX REGISTERINA ATIYORUZ.					
							 MUL ESI								//RESIM[I][J]'YE ERISIM KOD BOYUNCA N*ESI+BX YOLUYLA OLACAKTIR.I ILE N I CARPTIK.			
							 ADD EAX, EBX						//(I*N) DEGERINE J YI EKLEDIK					
							 PUSH BX 							//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN INDIS OLARAK KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.										
							 MOV EBX, EAX							//(I*N+J)'NIN ADRESI EBX'E AKTARILIYOR.					
							 MOV CX, WORD PTR[EDI + EBX]			//DIZININ (I*N+J) DEGERINI CX REGISTERINA AKTARDIK.								
							 XOR EBX, EBX							//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.					
							 POP BX								//BX'E J DEGERI GERI DONDU.			
							 PUSH CX								//(I*N+J) DEGERI STACK'E ATILIYOR			
							 MOV ECX, EAX							//ECX'DE (I*N+J)'NIN ADRESI TUTULUYOR				
							 MOVZX EAX, n							//ARTIK RESIM[J][I] ADRESI HESAPLANMAYA BASLIYOR			
							 MUL EBX								//(J*N) ISLEMI GERCEKLESIYOR			
							 POP DX								//RESIM[I][J] DEGERI DX REGISTERINA ATILIYOR			
							 ADD EAX, ESI							//(J*N+I) ADRESI EAX DE HESAPLANIYOR				
							 PUSH BX 							//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.									
							 MOV EBX, EAX							//EBX E (J*N+I) DEGERI ATILIYOR				
							 XCHG WORD PTR[EDI + EBX], DX		//(I*N+J) DEGERI ILE (J*N+I) DEGERININ YERLERI DEGISTIRILIYOR										
							 MOV EBX, ECX							//ECX DEKI (I*N+J) ADRESI EBX E AKTARILIYOR			
							 MOV WORD PTR[EDI + EBX], DX			//DX DEKI (J*N+I)'IN DEGERI (I*N+J) ADRESINE AKTARILIYOR								
							 XOR EBX, EBX							//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.				
							 POP BX								//BX'E J DEGERI GERIN DONDU.
							 XOR ECX, ECX							//ECX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.			
							 POP CX								//CX'E ICDONUN CEVRIM SAYISI AKTARILIYOR 			
							 ADD BX, 2							//RESIM WORD TANIMLI OLDUGU ICIN BX ADRESI 2 ARTTIRILIYOR				
							 LOOP ICDON							//ICDON ETIKETINE DONULUYOR				
							 POP CX								//DISDONUN CEVRIM SAYISI CX'E AKTARILIYOR			
							 ADD SI, 2							//RESIM WORD TANIMLI OLDUGU ICIN SI ADRESI 2 ARTTIRILIYOR					
							 LOOP DISDON							//DISDON ETIKETINE DONULUYOR					
							 MOV CX, n											// CX'DE DIZININ BOYUTUNU TUTUYORUZ
							 XOR ESI, ESI											//ESI DEGERINI SIFIRLADIK
							 DISDON2 : PUSH CX									//DISDONUN CEVRIM SAYISINI PUSHLADIK.		
									   MOV CX, n									//IKINCI DONGU (N/2) KADAR DONMESI ICIN GEREKLI AYARLAMALAR YAPILIYOR.		
									   SHR CX, 1									//IKINCI DONGU (N/2) KADAR DONMESI ICIN GEREKLI AYARLAMALAR YAPILIYOR.		
									   XOR EBX, EBX									//J INDISIMIZ HER SEFERINDE 0'DAN BASLAMALI.		
									   ICDON2 : PUSH CX								//CX ARA ISLEMLERDE	KULLANILMAK UZERE STACK E ATILMISTIR.		
												MOVZX EAX, n						//N DEGERINI MOVZX YARDIMI ILE EAX REGISTERINA ATIYORUZ.					
												MUL ESI								//RESIM[I][J]'YE ERISIM KOD BOYUNCA N*ESI+BX YOLUYLA OLACAKTIR.I ILE N I CARPTIK.			
												ADD EAX, EBX						//(I*N) DEGERINE J YI EKLEDIK						
												PUSH BX 							//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN INDIS OLARAK KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.									
												MOV EBX, EAX						//(I*N+J)'NIN ADRESI EBX'E AKTARILIYOR.				
												MOV CX, WORD PTR[EDI + EBX]		//DIZININ (I*N+J) DEGERINI CX REGISTERINA AKTARDIK.										
												XOR EBX, EBX 						//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.					
												POP BX								//BX'E J DEGERI GERI DONDU.				
												PUSH CX								//(I*N+J) DEGERI STACK'E ATILIYOR			
												MOV ECX, EAX						//ECX'DE (I*N+J)'NIN ADRESI TUTULUYOR					
												MOVZX EAX, n						//ARTIK RESIM[I][N-J-1]'NIN ADRESI HESAPLANIYOR					
												MUL ESI								//(I*N) ISLEMI GERCEKLESIYOR.			
												MOV DX, n							//(N-J-1) KISMI HESAPLANIYOR.				
												ADD DX, n							//RESIM WORD TANIMLI OLDUGU ICIN N. ELEMANI ASLINDA 2N . ADRESTE OLUR.				
												SUB DX, BX							//(N-J) KISMI GERCEKLESIYOR.				
												SUB DX, 2							//AYNI SEKILDE RESIM WORD TANIMLI OLDUGU ICIN 2 AZALTILMASI GEREKIYOR.				
												ADD EAX, EDX						//RESIM[I][N-J-1]'NIN ADRESI EAX REGISTERINDA OLMUS OLDU.				
												POP DX								//RESIM[I][J] DEGERI DX REGISTERINA ATILIYOR		
												PUSH BX								//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN INDIS OLARAK KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.			
												MOV EBX, EAX						//EBX'E RESIM[I][N-J-1]'NIN ADRESI AKTARILIYOR.					
												XCHG WORD PTR[EDI + EBX], DX		//(I*N+J) DEGERI ILE (I*N+(N-J-1)) DEGERININ YERLERI DEGISTIRILIYOR.								
												MOV EBX, ECX						//EBX'E (I*N+J)'NIN ADRESI	AKTARILIYOR.			
												MOV WORD PTR[EDI + EBX], DX		//DX DEKI (I*N+(N-J-1))'IN DEGERI (I*N+J) ADRESINE AKTARILIYOR 									
												XOR EBX, EBX							//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.					
												POP BX								//BX'E J DEGERI GERI DONDU.			
												XOR ECX, ECX						//ECX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.					
												POP CX								//CX'E ICDONUN CEVRIM SAYISI AKTARILIYOR 			
												ADD BX, 2							//RESIM WORD TANIMLI OLDUGU ICIN BX ADRESI 2 ARTTIRILIYOR					
												LOOP ICDON2							//ICDON2 ETIKETINE DONULUYOR					
												POP CX								//DISDON2NIN CEVRIM SAYISI CX'E AKTARILIYOR			
												ADD SI, 2							//RESIM WORD TANIMLI OLDUGU ICIN SI ADRESI 2 ARTTIRILIYOR					
												LOOP DISDON2
		}
	//KODUNUZU YAZMAYI BURADA BITIRINIZ
}

void solaDondur(short n, int resim) {
	//KODUNUZU BURADAN BASLAYARAK YAZINIZ
	__asm {
		MOV CX, n											// CX'DE DIZININ BOYUTUNU TUTUYORUZ    									
		MOV EDI, resim										//RESMIN ADRESINI EDI'DA TUTTUK	
		XOR ESI, ESI 										//ESI DEGERINI SIFIRLADIK	
		XOR EBX, EBX										//EBX DEGERINI SIFIRLADIK	
		DISDON : PUSH CX										//DISDONUN CEVRIM SAYISINI PUSHLADIK.IKINCI DONGU ICIN CX AYARLAMAMIZA GEREK YOK.	
				 MOV EBX, ESI									//J INDISIMIZ I INDISIN DEGERINDEN BASLIYOR		
				 ICDON : PUSH CX								//CX ARA ISLEMLERDE	KULLANILMAK UZERE STACK E ATILMISTIR.		
						 MOVZX EAX, n						//N DEGERINI MOVZX YARDIMI ILE EAX REGISTERINA ATIYORUZ.					
						 MUL ESI								//RESIM[I][J]'YE ERISIM KOD BOYUNCA N*ESI+BX YOLUYLA OLACAKTIR.I ILE N I CARPTIK.			
						 ADD EAX, EBX						//(I*N) DEGERINE J YI EKLEDIK					
						 PUSH BX 							//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN INDIS OLARAK KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.										
						 MOV EBX, EAX							//(I*N+J)'NIN ADRESI EBX'E AKTARILIYOR.					
						 MOV CX, WORD PTR[EDI + EBX]			//DIZININ (I*N+J) DEGERINI CX REGISTERINA AKTARDIK.								
						 XOR EBX, EBX							//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.					
						 POP BX								//BX'E J DEGERI GERI DONDU.			
						 PUSH CX								//(I*N+J) DEGERI STACK'E ATILIYOR			
						 MOV ECX, EAX							//ECX'DE (I*N+J)'NIN ADRESI TUTULUYOR				
						 MOVZX EAX, n							//ARTIK RESIM[J][I] ADRESI HESAPLANMAYA BASLIYOR			
						 MUL EBX								//(J*N) ISLEMI GERCEKLESIYOR			
						 POP DX								//RESIM[I][J] DEGERI DX REGISTERINA ATILIYOR			
						 ADD EAX, ESI							//(J*N+I) ADRESI EAX DE HESAPLANIYOR				
						 PUSH BX 							//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.									
						 MOV EBX, EAX							//EBX E (J*N+I) DEGERI ATILIYOR				
						 XCHG WORD PTR[EDI + EBX], DX		//(I*N+J) DEGERI ILE (J*N+I) DEGERININ YERLERI DEGISTIRILIYOR										
						 MOV EBX, ECX							//ECX DEKI (I*N+J) ADRESI EBX E AKTARILIYOR			
						 MOV WORD PTR[EDI + EBX], DX			//DX DEKI (J*N+I)'IN DEGERI (I*N+J) ADRESINE AKTARILIYOR								
						 XOR EBX, EBX							//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.				
						 POP BX								//BX'E J DEGERI GERIN DONDU.
						 XOR ECX, ECX							//ECX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.			
						 POP CX								//CX'E ICDONUN CEVRIM SAYISI AKTARILIYOR 			
						 ADD BX, 2							//RESIM WORD TANIMLI OLDUGU ICIN BX ADRESI 2 ARTTIRILIYOR				
						 LOOP ICDON							//ICDON ETIKETINE DONULUYOR				
						 POP CX								//DISDONUN CEVRIM SAYISI CX'E AKTARILIYOR			
						 ADD SI, 2							//RESIM WORD TANIMLI OLDUGU ICIN SI ADRESI 2 ARTTIRILIYOR					
						 LOOP DISDON							//DISDON ETIKETINE DONULUYOR					
						 MOV CX, n											// CX'DE DIZININ BOYUTUNU TUTUYORUZ	
						 SHR CX, 1											//ESI DEGERINI SIFIRLADIK	
						 XOR ESI, ESI										//DISDONUN CEVRIM SAYISINI PUSHLADIK.			
						 DISDON2 : PUSH CX									//IKINCI DONGU (N/2) KADAR DONMESI ICIN GEREKLI AYARLAMALAR YAPILIYOR			
								   MOV CX, n									//IKINCI DONGU (N/2) KADAR DONMESI ICIN GEREKLI AYARLAMALAR YAPILIYOR.				
								   XOR EBX, EBX								//J INDISIMIZ HER SEFERINDE 0'DAN BASLAMALI.				
								   ICDON2 : PUSH CX							//CX ARA ISLEMLERDE	KULLANILMAK UZERE STACK E ATILMISTIR.					
											MOVZX EAX, n						//N DEGERINI MOVZX YARDIMI ILE EAX REGISTERINA ATIYORUZ.							
											MUL ESI								//RESIM[I][J]'YE ERISIM KOD BOYUNCA N*ESI+BX YOLUYLA OLACAKTIR.I ILE N I CARPTIK.				
											ADD EAX, EBX							//(I*N) DEGERINE J YI EKLEDIK					
											PUSH BX 							//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN INDIS OLARAK KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.				
											MOV EBX, EAX						//(I*N+J)'NIN ADRESI EBX'E AKTARILIYOR.							
											MOV CX, [EDI + EBX]					//DIZININ (I*N+J) DEGERINI CX REGISTERINA AKTARDIK.								
											XOR EBX, EBX 						//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.						
											POP BX								//BX'E J DEGERI GERI DONDU.				
											PUSH CX								//(I*N+J) DEGERI STACK'E ATILIYOR					
											MOV ECX, EAX						//ECX'DE (I*N+J)'NIN ADRESI TUTULUYOR						
											MOVZX EAX, n						//ARTIK RESIM[N-1-I][J]'NIN ADRESI HESAPLANIYOR						
											MOV DX, n							//DX REGISTERINA BOYUT BILGISI ATILDI					
											ADD EAX, EDX							//RESIM WORD TANIMLI OLDUGUNDAN 2N ADRESINE ULASILDI.					
											SUB EAX, 2							//(N-1) ISLEMI GERCEKLESIYOR.					
											SUB EAX, ESI							//(N-1-I) ISLEMI GERCEKLESIYOR.					
											MUL EDX								//N*(N-1-I)	ISLEMI GERCEKLESIYOR.		
											ADD EAX, EBX							//(N*(N-1-I)+J) 'NIN ADRESI EAX'DE OLUSTU.					
											POP DX								//RESIM[I][J] DEGERI DX REGISTERINA ATILIYOR				
											PUSH BX								//EBX ISTEDIGIMIZ ELEMANA ULASMAK ICIN INDIS OLARAK KULLANILACAKTIR.BX'IN ICINDEKKI J DEGERI STACKE ATILIYOR.				
											MOV EBX, EAX						//(N*(N-1-I)+J) 'NIN ADRESI EBX REGISTERINA AKTARILIYOR.						
											XCHG[EDI + EBX], DX					//(I*N+J) DEGERI ILE (N*(N-1-I)+J) DEGERININ YERLERI DEGISTIRILIYOR.								
											MOV EBX, ECX						//EBX'E (I*N+J)'NIN ADRESI	AKTARILIYOR.						
											MOV[EDI + EBX], DX					//DX DEKI (N*(N-1-I)+J)'IN DEGERI (I*N+J) ADRESINE AKTARILIYOR 								
											XOR EBX, EBX						//EBX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.							
											POP BX								//BX'E J DEGERI GERI DONDU.					
											XOR ECX, ECX						//ECX'IN EKSTRA KISMINDA DEGER OLABILECEGINDEN SIFIRLANIYOR.						
											POP CX								//CX'E ICDONUN CEVRIM SAYISI AKTARILIYOR 					
											ADD BX, 2							//RESIM WORD TANIMLI OLDUGU ICIN BX ADRESI 2 ARTTIRILIYOR						
											LOOP ICDON2							//ICDON2 ETIKETINE DONULUYOR							
											POP CX								//DISDON2NIN CEVRIM SAYISI CX'E AKTARILIYOR					
											ADD SI, 2							//RESIM WORD TANIMLI OLDUGU ICIN SI ADRESI 2 ARTTIRILIYOR						
											LOOP DISDON2
	}
	//KODUNUZU YAZMAYI BURADA BITIRINIZ
}