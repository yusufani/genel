THE Respect campaign is going really well. No, honestly, it is. Not at the top of the game admittedly, but among the muck and mud at the base of football�s pyramid it is slowly �changing the ethos of �football in this country.

At the top, managers are still heaping abuse on match officials, as they always have. Joe Kinnear brought his legendary calm, fair-minded analysis to bear on Newcastle�s defeat at Fulham and concluded that Martin Atkinson was �a Mickey Mouse ref�.

After watching Manchester City lose to Spurs, Mark Hughes �suggested it is all right to hack and shove opponents if it is raining.

And in the Championship, Cardiff�s Dave Jones said the entire Respect initiative is �a load of baloney�. Yep, you are right, his team lost as well. But down among the competitions below the Football League, things are �different. Things are getting better.

Yesterday, the first results from detailed monitoring of the Respect initiative arrived at the FA and revealed that, at grassroots level, there has been a quantifiable improvement in the attitude towards referees.

Across the country, the number of cautions for dissent is down by 12 per cent. In counties where the local FA have 
been particularly active about Respect, the numbers are better still. In Cheshire, for instance, dissent cases are down by 38 per cent.

OK, we haven�t quite reached the stage where every match starts with everyone singing Aretha Franklin�s R-E-S-P-E-C-T, but the campaign is clearly starting to catch on among the lower leagues.

 


You will say: �But it should start at the top.� That is a commonly held view, but it was from the parks rather than the Premier League that the cry went up for help. A year ago, the biggest opinion �survey of grassroots football found the
top priority was concern about appalling behaviour towards referees by players and spectators.

So the FA introduced a pilot Respect scheme in 20 leagues in seven counties. Only the captain could speak to the referee; touchline barriers kept parents and others back, while codes of conduct and related sanctions were brought in.

The experiment was such a �success that this season every league in the country has been invited to sign up to the scheme. Almost 400 leagues have done so  and that means 25,000 teams are bound by the new Respect rules.

There is a website called Full Time, on which referees comment on the behaviour of teams, officials and spectators. Hundreds of leagues have signed up to it, from the Accrington Combination to the Ziggy�s Cars Halifax Sunday League. Meanwhile, at the �pinnacle of the pyramid, the bad men, mad men and sad men are still showing that, although they �profess to love the game, they care only about their own jobs.

Yesterday, referees� chief Keith Hackett attended a meeting of the League Managers� Association because just three months after signing up to the Respect initiative they are still ranting and raving whenever a referee does anything with which they don�t agree.

Hackett was trying to persuade them to continue backing the �campaign, but he might as well have whistled in the wind because members take their cue from Sir Alex Ferguson. 

In March, Ferguson applauded the Respect plan and said his club had cleaned up their act. Last week he was charged by the FA for a finger-wagging tirade at referee Mike Dean. Like every other boss, Ferguson thinks Respect is terrific, until decisions go against his team or when he perceives an injustice.

The attitude of the managers is summed up by Derby�s Paul Jewell, who is still banging on about how Stuart Atwell handled his club�s match eight days earlier.

Atwell had a poor game, but he is just 25 and the youngest referee to officiate at the top in this country. His confidence has been shredded after awarding the �phantom goal� at Watford, when he took the word of an experienced assistant referee. 

He needs mentoring and training �  which he is getting, by the way � not scorn and abuse.

Jewell has made some terrible decisions in his time, yet he has the respect of people within the game.

The FA campaign asks that �football folk are as understanding to referees; to respect them for the vital, desperately difficult job they do. Respect without caveats and conditions. Not respect only when they give decisions your way.

One suspects men like Jewell will never understand and never agree, but the game can be grateful that, at its heart, there are an increasing number who do.

