﻿CHP’de Abdüllatif Şener iktidarı


Dersimli Kemal Kılıçdaroğlu Ankara’da CHP’deki “tek adam” iktidarını inşa ederken Dersim’de toplanan yaklaşık 2 bin kişi birahane taşlıyordu. Şiddet yoluyla Dersim’de ahlak zabıtalığına soyunanlar kimdir, necidir, inanın hiçbir fikrim yok. Merak edip araştırma ihtiyacı bile duymadım. Sorunun beni ilgilendiren tarafı, eylemin yöntemidir.

Kim olurlarsa olsunlar, neyi temsil ediyorlarsa etsinler, taşlarla ve sopalarla sokaklara çeki düzen vermeye, kendi yerel iktidarlarını kurmaya yeltenemezler. Elbette, içkili mekanların aileler üzerindeki sosyolojik etkisi tartışılabilir, ortada sorun varsa çözüme ihtiyaç duyulabilir ama hiç kimse şiddeti çıkış yolu olarak topluma sunamaz.

Polisin başkentteki park caddesinde içkili yerlere yönelik denetimini günlerdir tartışma konusu yapanların Dersim’deki birahane şiddetini görmezlikten gelmesi, kadın örgütlerinin köşelerine sinmesi ve yumurta sevicilerin bu haberi bir iki sütunla sınırlı tutması, soruna yaklaşımdaki çifte standardı gözler önüne sermesi bakımından dikkat çekicidir.

Sözgelimi birahane eylemi, Erzurum’da yaşansaydı, bugün Türkiye CHP kurultayını konuşmazdı. Hele Ankara veya İstanbul’da olsaydı, Türkiye’nin İranlaşmasından tutun şeriata kadar konuşulmadık konu bırakmazdık.

CHP kurultayını bu perspektiften izlediğimizde; Hürriyet, Vatan, Milliyet gibi gazetelerde atılan sevinç naralarını, “Korkmayın geliyoruz”, “CHP kurultayı tıpkı F1 gibi”, “Artık AKP yıpranma sürecinde”, “Nihayet CHP umut veriyor” türünden atılan başlıkları daha iyi anlayabiliriz.

Abdüllatif Şener yeni parti kurduğunda, Hüsamettin Cindoruk DP’yi ele geçirdiğinde de sevindirik olmuşlardı. Şimdi şu slogana sarıldılar: Nefes alıyorsak umut var demektir!

Olmak ya da olmamak

Bu kez başarmalarını diliyorum. İktidarı denetleyen güçlü bir muhalefet, demokratik kazanımları arttırır ve iktidar karşısındaki alternatif umudunu canlı tutar, nefes nefese bırakmaz. Ergenekon ve Balyoz gibi yer altına kaçışı da frenler.

Kemal Bey, genel başkanlık koltuğuna oturur oturmaz partideki ölü toprağını şöyle bir silkeledi, teşkilatları canlandırdı ve kitleleri heyecanlandırdı. Ne var ki, bu süreç, silkelenmeyi, canlılığı ve heyecanı oya tahvil edecek politikalarla güçlü şekilde desteklenemedi.

Dendi ki; Deniz Baykal ve Önder Sav gibi prangaları var.

Son kurultay, Kemal Bey’in tüm prangalarından kurtulduğu ve tek adam olarak direksiyona geçtiği sürecin başlangıcıdır. Artık bahane üretme lüksü kalmamıştır. Önümüzdeki seçim, günahıyla sevabıyla tümden kendine fatura edilecektir.

Başarırsa lider olur, başaramazsa birçok partili gibi kendisi de meyhane köşelerine düşer “mazi kalbimde bir yaradır” şarkısını terennüm eder. Tabi o meyhaneyi taşlamazlarsa...

Gidişi öyle Deniz Baykal gibi 18 yıl sürmez; gitsin diye Türkan Saylan gibi yıllarca ölüm korkusundan medet umanlar çıkmaz, zira atın ölümü arpadan onunki gürpeden olur.

O nedenle işi sıkı tutması gerekir.

Kemal Sunal Komedisi

Zira, seçime kadar kat etmesi gereken çok mesafe var. Kurultay konuşması zayıftı, parti meclisi daha beterdi. Toplam maliyeti eski parayla 200 katrilyon lirayı aşan, geçmişte denenmiş ancak sandığa gömülmüş Demirel usulü vaatleri “Kemal” ambalajına sararak yutturmak çok güçtür.

“Benim adım Kemal, ben parayı bulurum” demenin mali karşılığı yoktur. Vaatlerin içini dolduramazsanız, sözleriniz, komedi filmlerindeki gibi “Kemal Sunal” muamelesi görür.

O zaman adama çıkıp sorarlar: İstanbul’da büyükşehre aday olunca ihtiyaç sahiplerine 600 lira aylık bağlamayı vaat etmiştin, şimdi genel başkansın hadi CHP’li İzmir belediyesinde uygula.

Bununla kalmaz, Halk TV’yi hatırlatırlar: Bakın, Halk TV’de çalışanlar 2-3 aydır maaşlarını alamıyorlar, siz bırakın Türkiye’deki yoksullara maaş vermeyi önce kendi işçilerinizin ücretlerini ödeyin.

Ya yeni kadrolarınız?

Baykal ve Sav’a geçit vermediniz, iyi de ettiniz. Che Guevara posteri altında sol yumruğunuzu kaldırarak “Faşizm” dediniz, TÖB-Der’in mal varlığına sahip çıktığınız, 12 Eylül öncesine nostaljik gezinti yaptınız, Silivri’ye selam çaktınız, “Kürt” sözcüğünden korktunuz, başörtüsüne değinmediniz, askeri vesayete teğet geçtiniz, ama sağ kolunuz Gürsel Tekin’in karizmasını çizdiniz.

Kürt açılımın simge ismi Sezgin Tanrıkulu PM’ye giremedi. “Burası CHP, Kürt demek o kadar kolay değil” diyor, acısı büyük. “10.yıl değil 100.yıl marşı bestelemeyiz” şeklinde radikal çıkışlar yapan Enver Aysever liste çizikleriyle yara bere içinde. Yılların sosyal demokratı Servet Ünsal, kamuoyunda emekçi dostu olarak bilinen Ali Tezel ve Alper Taşdelen hayal kırıklığı yaşayanlardan.

PM’ye soktuğunuz en önemli kurmaylarınızdan ikisi Abdüllatif Şener’in ANAP ve DYP kökenli iki genel başkan yardımcısı Bülent Kuşoğlu ve Ali Arif Özzeybek... Hakkı Süha Okay’ı, Şahin Mengü’yü, Haluk Koç’u silerken tercihiniz bunlar mı olmalıydı?

Gariptir, Şener, CHP’de Baykal ve Sav’dan daha etkili hale geldi. Abdüllatif Bey, yeni partisini kurtaramadı ama kadrolarını CHP’de iktidara taşıdı.

Neyse...

Kemal Bey, böyle uygun gördü, çıktı yola. Bizlerin kanaatlerinden öte burada aslolan halkın vereceği nottur.

Hadi hayırlısı...