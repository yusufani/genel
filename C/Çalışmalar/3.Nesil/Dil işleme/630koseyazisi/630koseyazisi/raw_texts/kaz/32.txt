M�sy� Tigana korkak!

�yle diyorlar ya!.. Sorunlar� ortak. Kondisyon konusunda ayn� s�k�nt�y� ya��yorlar. Yani 60'�nc� dakikadan sonra ��k�yorlar. Peki �yle mi oldu? Yani ma��n kaderi son 30 dakika da m� belirlendi? Bu sorunun cevab�n� futbolcular sahada ��yle verdiler: 
"Yok �yle bir sorun say�n hocalar�m!" 
�ki teknik adam, Ziya Do�an ve Jean Tigana'ya itiraz�m ve �ikayetim �udur: �kisi de korkuya dayal� futbola prim verdiler. Bu, seyirciye yap�lan en b�y�k sayg�s�zl�k. �nce kaybetmemek, gol� karambollere b�rakmak gibi bir d���nceye sar�lmak, Be�ikta� ve Malatya'n�n vizyonunu k���ltt�.
Bak�n ilk 45 dakikan�n analizi �u; 
1- Be�ikta�'�n orta alan� felaket. Kleberson sorumluluktan ka��p ald��� toplar� en yak�n�na verdi. T�mer Metin gibi bir usta sakatlanmamak i�in oyunun i�ine sakland�. (�svi�re ulusal ma��n� d���nmesi elbette ge�erli bir mazeret...) �brahim Ak�n da ele�tirilerden korkup, risk almadan her topta Ailton'u arad�.
2- Malatyaspor, yenilmemek i�in savunmada alan daraltt�. Taktik faullerle Be�ikta�'� ��kerttiler. H�cuma ise �abuk ��kt�lar. Mustafa �zkan'�n fedakar oyununa kar��l�k Effa Owana'n�n egoist futbolu Malatya'n�n h�cumlar�n� �ld�rd�.
3- �lk 45 dakikada kaleciler hatas�z, savunmac�lar m�kemmeldi.
�kinci yar�ya Be�ikta� ve Malatya risk alarak ve tempo yaparak ba�lad�lar. Tek fark biraz ko�makt�! 
Tigana sistem �zerinde operasyon yapm��t�. T�mer Metin'i, �brahim �z�lmez'in �n�ne �ekip h�cumlar� sol taraftan organize ederken, sa�da Ali Tando�an daha �ok h�cuma ��kmaya ba�lam��t�. 
Bu noktada Be�ikta� iki sorun ya�ad�: 
1- �ki kanat iki�er ki�i ile oynad� ama Ailton'a bir tek top at�lmad�. ��te burada Ailton'un el kol isyanlar� ba�lad�. (��k olmayan bu tablo g�sterdi ki, Be�ikta�'�n tak�m olma sorunlar� var) 
2- T�mer Metin �izgiye ve geriye do�ru gelince yani kaleden uzakla��nca, Be�ikta� kendi silah�n� kendi imha eden tak�m oldu. (Oysa kaleye yak�n oynasa ya goll�k pas verecek ya gol atacak.) 
Son yar�m saate girince Do�an ve Tigana orta sahaya dinamizm getirmek i�in de�i�iklik yapt�lar. Daniel Pancu ve Souleymane Youla de�i�imi do�ruydu. Malatya'da ise Okan Y�lmaz'�n oyuna girmesi ge� kalm�� bir karard�.
Beklenen goller karambollerden geldi. Oscar Cordoba'n�n yedi�i gol hediye oldu. �yi oynarken k�t� oyuncu Effa'dan k�t� gol yedi. Son dakikada Fevzi Tuncay da savunmas�n�n hatas� ile gol� yedi. Ma��n hakk� buydu. 
MESAJ; Be�ikta� 1-0 ma�lup. �brahim Ak�n ��k�p Ahmet Dursun girerse, M�sy� Tigana'ya g�venim s�f�r! M�sy� Tigana risk alm�yor. Yani korkuyor. M�sy� Tigana; savunmadan bir ki�i ��kar (�brahim �z�lmez veya �a�da� Atan gibi) h�cuma bir ki�i al. Olay budur!.. 
