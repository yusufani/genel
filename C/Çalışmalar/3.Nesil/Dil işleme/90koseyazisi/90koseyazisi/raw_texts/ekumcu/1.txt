Yat�r�mc�y� korumak para kazand�rmak de�ildir 
SERMAYE piyasalar�na bak���m�z biraz ters. Yat�r�mc�y� korumak yat�r�mc�ya para kazand�rmakla e� tutuluyor. Bir kamu kurumunun yat�r�mc�lar�n haklar�n� korumakla y�k�ml� olmas� yat�r�mc�lar�n mutlaka para kazanmas� gerekti�i anlam�na gelmiyor. 

Sermaye Piyasas� Kurulu (SPK) yat�r�mc�n�n haklar�n� korumakla g�revlidir. Yat�r�mc�n�n hakk� do�ru ve zaman�nda bilgi almakt�r. Do�ru ve zaman�nda bilgi alan yat�r�mc�lar istedikleri �ekilde yat�r�m yaparlar. Bazen kazan�rlar, bazen kaybederler. Kaybettiklerinde, sorumlu SPK de�ildir. SPK�n�n yat�r�mc�lara para kazand�rmak gibi bir g�revi yoktur.

�stanbul Menkul K�ymetler Piyasas� (�MKB) hisselerini bireysel ve kurumsal yat�r�mc�lara satan firmalar�n hisselerinin el de�i�tirdi�i bir piyasad�r. �MKB�nin sorumlulu�u hisse de�i�imlerinin �effaf bir ortamda ger�ekle�mesi ve olu�an hisse fiyat�n�n arz ve talep dengesini yans�tmas�n� temin etmesidir.

�MKB�nin de yat�r�mc�lara para kazand�rmak gibi bir sorumlulu�u yoktur. �nemli olan �irketler hakk�nda bilgilerin do�ru ve zaman�nda yat�r�mc�lara ula�t�r�lmas�d�r. Risk yat�r�mc�lar�nd�r. Kar da, zarar da yat�r�mc�lar�nd�r. Kimse sorumlu tutulamaz.

K�R DA�ITIMI

Mali durumu �ok k�t� bir �irket de halka a��labilmelidir. �nemli olan, �irketin mali durumunun k�t�l��� de�il, �irketin k�t� durumda olmas�n�n yat�r�mc�lar taraf�ndan t�m a��kl��� ile bilinmesidir. K�t� �irketin fiyat� d���k olacakt�r. �yi �irketin fiyat� y�ksek olacakt�r. Karar� yat�r�mc�lar verir. 

Halka a��ld�ktan sonra bir �irketin hisse senedi fiyat� d��t���nde, yat�r�mc�lar k�zmaktad�r. K�zmaya hi�bir haklar� yoktur. T�m bilgiler kendilerine verildi�ine g�re, ya hak etti�inin �zerinde bir fiyatla hisse senedini alm��lard�r ya da beklentilerinin d���nda piyasa �artlar� de�i�mi�tir. T�m bunlar�n sorumlulu�u yat�r�mc�lar�n �zerindedir.

Halka a��lma ve hisse senedi piyasas� s�re�lerinde devletin devreye girmesiyle devletin bir bi�imde koruyucu g�rev yapmas� beklenmektedir. Bu beklenti yanl��t�r. Yine de, sermaye piyasalar� ile ili�kili kurumlar kendilerini piyasa geli�melerinden sorumlu tutmakta ve yat�r�mc�y� koruma ad�na piyasa kurallar� d���nda d�zenlemelere gidebilmektedir.

K�r eden halka a��k �irketlerin k�rlar�n�n bir b�l�m�n� nakit temett� olarak da��tmalar� y�n�nde y�r�rl�kteki d�zenleme bu �e�it bir d�zenlemedir. Y�l i�inde ger�ekle�tirilen k�r�n temett� olarak da��t�l�p da��t�lmayaca�� �irketin ileriye d�n�k planlar�yla ilgili bir konudur. Temett� almak i�in yat�r�m yapan yat�r�mc�lar yat�r�m yapt�klar� �irketin temett� politikas�ndan memnun de�ilse, o �irketten ��karlar. Kararlar� �irketin hisse senedi fiyat�na yans�r. Ama, �irketi temett� da��tmaya zorlamak �irketin i� i�lerine kar��mak anlam�na gelir.

ARAYA G�RME

SPK gibi kurumlar�n �irketlerin i� i�lerine kar��maya y�nelmeleri halka a��lma i�tah�n� azalt�c� bir etkendir. Bu �e�it d�zenlemeler, sermaye piyasalar�n� geli�tirelim derken, yat�r�mc�lar� koruma ad� alt�nda sermaye piyasalar�n�n g�d�k kalmas�na katk�da bulunulmaktad�r.

Sermaye piyasalar�n�n alt�n kural� yat�r�mc�larla �irketleri a��kl�k ve do�ruluk ilkeleri i�inde birbirlerine kar�� y�k�ml� k�lmakt�r. Bu iki oyuncunun aras�na girildi�inde, oyuncular�n t�m�n�n kaybetme olas�l��� �ok fazlad�r. 
