Yanl�� adres... 
T�RBANLI bir grup kad�n, �ankaya K��k��ne y�r�yerek Cumhurba�kan��na bir Kuran b�rakt�lar.

Bu, Cumhuriyet tarihimizde ilk kez oluyor. 

T�rbanl�lar�n K��k kap�s�nda okuduklar� bildiride ise Cumhurba�kan��na �Din ve vicdan �zg�rl���n�n �n�nde durmaktan vazge�in. Nur Suresi 31 ve Ahzap Suresi 59�uncu ayetleri okuyunuz. E�er i�inize sindiremiyorsan�z istifa edin� deniliyordu.

*

Ne anlama geliyor bu?

Cumhurba�kan��na Kuran g�t�rerek ve baz� ayetleri okumas�n� tavsiye ederek onu �imana� m� �a��r�yorlar?

��nk� o han�mlarda din-iman var, ba�ka kimsede yok, �yle mi?

Benim ise anlad���m tek �ey var:

A�a��larda halledilmemi� kurum, kurulu� kalmad�.

T�m� de�i�ti, may��t�, d�nd�, d�n��t�r�ld�, uydu, uyduruldu ve art�k ��l�ml� �slam modeline� engel de�iller.

�ankaya kald�...

Oraya y�r�yorlar...

*

E�er ama� sadece t�rbansa, bence �ankaya yanl�� adres.

Ba�bakanl��a y�r�meleri, o m�barek kitab� Ba�bakan�a vermeleri ve ayetlerin surelerini hat�rlatmalar� gerekir.

Oradan da Meclis Ba�kan� Ar�n��n kap�s�na... ��nk� yasama ve y�r�tmeyi elinde bulunduran, yasalar� de�i�tirmeye g�c� olan onlar.

Nas�l ki �ecnebilerin� istedi�i yasalar�, �nemli kararlar�, reform(!) dedikleri de�i�iklikleri bir gecede yap�yorlar, bunu da yapabilirler.

H�ristiyanlar�n istemlerini hemen yerine getiriyorlar da, t�rbanl�-�ar�afl� M�sl�man karde�lerimizin istemlerini niye yerine getirmeyecekler?

Cumhurba�kan��n�n sadece bir defa imzalamamaktan; ama ikinci kez �n�ne gelirse imzalamaktan ba�ka yapaca�� bir �ey var m�?

Yok... 

Meclis Ba�kan�, Ba�bakan orda duruyorlar.

E�er t�rban�-m�rban� kullanarak iktidara geldikten sonra, �imdi de yine t�rban�-m�rban� kullanarak Cumhurba�kanl����n� ele ge�irmek istemiyorlarsa...

Kad�nlar�n kafas�ndaki bez par�as� onlara siyasette yol a�m�yorsa...

Oradalar...

Cumhurba�kan� yanl�� adres. 
