Cinayetleri gördüm katilleri açıklıyorum!..

İki gündür İzmir Çeşme’de kan donduran gerçeklere tanık oluyorum.
Örneğin Dalyanköy’ün sevilen sayılan esnaflarından Emin Algan anlatıyor:
“Geçenlerde ambulans sesiyle dışarı fırladım. Biraz ötemizdeki Sahil Güvenlik botundan çığlıklar yükseliyordu.Hıçkıran küçük çocukları bottan çıkarıp ambulansa taşımaya başladıklarında, onların denizde boğulmak üzereyken kurtarılan yavrular olduğunu anladım. Ama bot komutanı çok üzgündü. Sel gibi akan gözyaşları arasında, son çocuğu kurtaramadıklarını, avuçlarının içinden kayıp gittiğini söylüyordu. Sanki kendi öz evlâdını denizin derinliklerine bırakmış bir baba gibi üzgündü. Öyle ki kendini tutamayıp duvarları tekmeliyordu. Sert bir asker olarak tanıdığımız o genç subay, ambulans gittikten sonra da dakikalarca ağladı…”

* * *

Çeşme Otogarı’nda işyeri olan ama isminin açıklanmasını istemeyen bir başka esnaf konuşuyor:
“Eskiden minibüslerle ya da üzerinde şirket ismi yazmayan otobüslerle gelirlerdi. Ama artık adı herkesce bilinen ünlü firmaların otobüsleriyle geliyorlar. Önceki gün 3 otobüs dolusuydular. Ayakkabısı olmayan, üzerlerinde soğuğa dayanıklı giysi bulunmayan çocukların imdadına hayırsever Çeşmeliler yetiştiler. Kimine ayakkabı kimine, kazak ve battaniye verdiler…”
Esnaf Suriyeli kaçakların doğruca Dalyanköy’e gidip, Emlâk Bankası’nın metruk binalarına sığındıklarını ve kendilerini Sakız’a götürecek insan tüccarları gelinceye kadar kırık camları naylonlarla kaplı o buz gibi yapılarda kaldıklarını söylüyor…

* * *

Yaz aylarında teknesiyle turizm hizmeti veren bir kaptanın söyledikleri ise tüyler ürpertiyor.
“İflâs etmek üzereyken göçmenleri Sakız Adası’na götürecek botları yapmaya başlayan bir plâstik üreticisi bu yoldan müthiş paralar kazandı. Bot dediğime bakmayın. Gerçek botlar denize ve basınca dayanıklı malzemeden imâl edilir. Ayrıca özel yapıştırma teknikleri kullanılır. Oysa gözünü para hırsı bürümüş bu vicdansızlar, 18 bin liraya sattıkları botları, brandadan yapıyorlar. Bu sözde botlar denize açıldıktan bir süre sonra can havliyle binen göçmenlerin de ağırlığıyla su almaya başlıyor. Bu sırada oluşan kargaşada da, uyduruk malzemeyle yapıştırılan yerler patır patır patlıyor!..”
Esnafa “Peki can yelekleri imdada yetişmiyor mu?” diye sorduğumda “Can yeleği mi?
Onlar can yeleği falan değil, insan öldürmek için üretilmiş cinayet aletleri!..” cevabıyla karşılaşıyorum.
Şoke olduğumu gören esnaf, “Gidin Marina’da, ya da Dalyan’daki çöp konteynerlerine bakın, bu ürpertici gerçeği gözlerinizle göreceksiniz!..” diyor.

* * *

Dediğini yapıyorum.
Söylediği mevkideki ilk konteynere baktığımda, içinin tıka basa can yelekleriyle dolu olduğunu görüyorum.
Fotoğraflarını paylaştığım adına canyeleği (!) denilen bu cinayet aletleri merdiven altlarında üretliyor. Kiminin üzerinde sahte markalar bulunuyor. Hatta Avrupa Birliği garantisine sahipmiş gibi bir izlenim bile yaratılabiliyor! Topladığım canyeleklerinden birinde, 90 kilodan fazla yükü kaldırabileceğini gösteren AB güvenceli etiketi görünce bir deneme yapmaya kalktım.
Yaklaşık 20 kilo ağırlığındaki taşı canyeleğine bağlayıp denize attığımda ne oldu biliyor musunuz?
15-20 dakika sonra battı!..
90 kiloyu taşıyabileceği garantisiyle satılan yeleğin minicik bir çocuğu bile kurtaramayacağı ortaya çıktı!..

* * *

Şimdi bu gerçeğin tüm ayrıntılarını bilip de göz yuman, boğulma görünümündeki seri cinayetleri görmezden gelen, devasa insanlık sorununu cansiperane çalışan Sahil Güvenlik personeli ile Çeşme Polisi ve Jandarması’nın omuzlarına yıkan gerçek sorumlulara soruyorum.
Bu katliamlara daha ne kadar seyirci kalacaksınız?
Şu anda 11 bebeğin de içinde bulunduğu bir botun deniz altında olduğu, tüm aramalara karşın bulunamadığı söyleniyor.
İnsanlıktan çıkmış, paraya tapan seri katillerin cinayetlerine ve insanlık suçlarına daha ne kadar ortak olacaksınız?
Bu katillerin vurgun hırsıyla canlarını aldığı masum bebeklerin görüntülerine baktığınızda bile insan olduğunuzu hatırlamayacak mısınız?

