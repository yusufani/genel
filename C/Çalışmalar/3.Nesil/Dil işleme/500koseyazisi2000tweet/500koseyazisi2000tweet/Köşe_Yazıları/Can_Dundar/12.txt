#herşeydenbiraznesli

Seçim zor be abi...

Çünkü biz, "Her şeyden biraz" nesliyiz.

Mesela duyuyoruz, bizden önceki nesil sofraya oturur, yemek yer, sonra çalışır, akşam televizyon seyreder, arada telefonla konuşurmuş.

Biz bilgisayar ekranı başında çalışırken, kupada çorbamızı içiyor, bir yandan sevgilimizle skype'laşırken bir yandan televizyona göz atıyoruz.

Fark bu...

Diyeceksin ki, kızla konuşurken çorbanın tadını alabiliyor musun, televizyona bakarken bilgisayarda okuduğundan bir şey anlıyor musun, helada bile Facebook sayfana göz atmaktan yorulmuyor musun?

Cevabım:

Hayır, yorulmuyorum.

Tersine bunlar olmadığında sıkıntıdan patlıyorum.

"Tek meşgale", bana/bize göre değil.

Helada bile...

***

"Açık büfe jenerasyonuyuz biz abi...

"Günün menüsü"yle yetinemeyiz. O, aşçının dayatmasıdır.

Biz ortalıkta ne varsa onunla tabağı tıka basa doldurur, sırasıyla tadına bakarız.

Manita işlerinde de böyledir bu... Tek eşle geçinemeyiz. O, ailenin kuşatmasıdır. Biz, ortamda kim varsa deneme yanılmayla karara varırız.

Oy mu vereceğiz; ideolojik karar vermeyiz. O, mahallenin baskısıdır.

Bakarız ortama, en elverişli kimse ona atarız. Bu sene buna, seneye öbürüne... Duruma göre...

Mesela duyuyoruz; eskiden pikaba bir albüm koyar, akşamaca dinlermiş babamgiller... Taş olsa çatlar. Biz, her albümden biraz bir şeyler bulur, kaydederiz. Gün boyu kulaklıktan dinleriz.

'Tek kanal izlerdik" diye anlatıyorlar, inanamıyorum. Uzaktan kumanda, bizim özgürlük meşalemiz. O ışın tabancamızı parmaklaya parmaklaya 8 kanalı bir arada izleriz.

Tek gazete okurlarmış eskiler; biz o site senin bu site benim gezerek bilgileniriz -demiyim de haberlere göz gezdiririz;

Derine dalmak eskilerin işiymiş, biz sörf severiz.

***

Açgözlülük mü?

"iştah" demek daha doğru...

Evinin, mahallesinin, şehrinin sınırlarını aşamamış kuşakların dünyasından, Arjantin'de tanımadığı biriyle satranç oynarken, Singapurlu'yla tavla attığımız bir evrene geçtik.

Biraz hızlı oldu, kabul; hazırlıksız yakalandık. Ama gördüğümüz her şeyi alma, tatma, kullanma iştahıyla donandık.

Seçenekler sınırsızdı; imkanlar dar... Alamasak da, her şeyden biraz hayal ettik.

Şöyle ortaya karışık bir dünyamız oldu.

***

Bak misal ben:

Che'yi seviyorum; çok okumadım, ama yakışıklı çocuk, destekliyorum. Şarkısı var, "Komandante" diye... Kulağımda onu dinliyorum. Sonra Rock'a geçiyorum.

HDP iyi parti... Güzel şeyler söylüyor, destekliyorum. Ama milli bayrağımı seviyorum, onu her şeyin üstünde tutuyorum.

Partileri de kendime benzettim. Hepsi her şeyden biraz vaat ediyor.

Ben hepsine biraz hak veriyorum.

Seçimde kararım kesin:

Hepsine basacağım damgayı...

Hepsi kazansın; ben hepsinden biraz faydalanacağım.