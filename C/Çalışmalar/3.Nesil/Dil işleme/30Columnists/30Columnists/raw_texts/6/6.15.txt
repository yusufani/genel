The Equitable Life scandal is not going away, nor should it. The Public Administration Committee of MPs today produces a coruscating analysis, entitled Justice Denied?, about the Treasury�s handling of the affair, notably its slippery response to last July�s damning report by the Ombudsman. 

The origins lie in the former management of Equitable Life�s offer of guaranteed minimum payments to some policyholders, which it later could not honour. After a reduced bonus policy was found to be unlawful, the company closed to new business in December 2000, leading to big cuts in the value of policies and pensions. The main fault clearly lies with the company. 

However, a series of regulators � the Department of Trade and Industry, the Government Actuary�s Department and the Financial Services Authority � were accused of not acting vigorously enough, a failure that �fell far short of acceptable standards of good administration�, according to the Ombudsman. Remedial action was not taken in time and policyholders were not fully informed. So there should be a compensation scheme. 

The Government�s response was evasive, appearing to embrace the thrust of the Ombudsman�s report, while, in practice, differing substantially. Ministers put most of the blame on Equitable Life. They offered only to make ex gratia payments on a complicated and still uncertain basis. 

The MPs argue that, while regulators were not primarily responsible for the losses, they were responsible for injustice and should not be allowed �to escape liability for that injustice altogether�. In particular, ministers� argument that Parliament has considered that financial regulators should not normally be held liable in the courts for financial loss was introduced late in the day �in a way we find to be shabby, constitutionally dubious and procedurally improper�. 

There is a key issue of principle. The Government has accepted that public bodies were responsible for maladministration which caused injustice, but, at the same time, argues that it is under no duty to put right these wrongs. This, the MPs say, �may be a legally valid position, but we think most people would consider it to be a morally unacceptable one�. 

Much bigger questions have arisen now in view of the banking collapse and rescue. But some, at least, of the damage in the Equitable Life affair could be remedied by a rapid government decision on the timetable and basis for payments. 

