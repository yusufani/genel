Scientists have pinpointed a gene that triggers the most deadly form of skin cancer, paving the way for new treatments for the disease. 
Up to 70 per cent of cases of malignant melanoma could be sparked by a genetic mutation caused by ageing and over-exposure to the sun. 
Scientists at The Institute of Cancer Research had previously linked the rogue version of the so-called BRAF gene to the disease but did not know if it actually caused the cancer. 
 
Now, the same group of researchers has shown that acquiring the BRAF mutation can be the first event in the cascade of genetic changes that eventually leads to melanoma, the most deadly form of skin cancer. 
While the mutation could occur naturally, the odds of it appearing are likely to be exacerbated by intensive exposure to the sun. 
Lead author Professor Richard Marais from the institute said: 'We know that excessive sun exposure is the main cause of skin cancer, but not much is known about the genetics behind it. 
'Our study shows that the genetic damage of BRAF is the first step in skin cancer development. 
'Understanding this process will help us develop more effective treatments for the disease.'
There are around 9,500 new cases of malignant melanoma and more than 2,300 deaths from the disease each year in the UK.
Over-exposure to sunlight causes at least two thirds of all malignant melanomas and up to 90 per cent of other skin cancers. This excessive exposure damages DNA and causes genetic mutations.
Dr Lesley Walker, director of cancer information at Cancer Research UK, said: 'Skin cancer is the seventh most common cancer in the UK, but relatively little is known about the genetics behind the disease. 

'There's lots of exciting research focused on developing new therapies that will block the function of mutant BRAF.
'A better understanding of the genetics of skin cancer can help scientists develop more targeted drugs with fewer side effects to treat the disease.
'This week, Cancer Research UK launches our SunSmart campaign to help raise awareness of the risks and causes of skin cancer.'
Those heading for the sun are advised to limit their sunbathing to before 11am and after 4pm, when the sun is less intense. 

Skin should be covered up wherever possible and broad spectrum sunscreen applied liberally and regularly.
The number of people treated for malignant melanoma  - blamed on short periods of intensive exposure to the sun -  has risen by 40 per cent in just six years.
But the sharpest increase in the disease is among men and women in their 30s. 

For men in this age group, the rate has increased by 38 per cent.  For women, it has soared by half.
The shocking rise in the disease traditionally diagnosed in the elderly and middle-aged suggests sun-worshipping Britons have ignored warnings about the dangers of UV radiation in pursuit of the perfect suntan. 

Cheap air fares and an abundance of high street tanning salons mean it has never been easier to top up a tan.
In addition, those in their 30s may be starting to reap the legacy of a childhood spent playing outside without the protection of sun screen.
Today's youth may also be more aware of the signs of skin cancer than their older counterparts.
Men are more likely to develop the cancer on their chest or back but the danger areas for women are legs and feet.
