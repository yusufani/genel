#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

typedef struct {
	
	char tel[16];
	char ad[32];
	char soyad[32];
} Contact;

void ToUpper(char *chr) {

	if (*chr < 'z' && *chr > 'a') {
		*chr -= 'a' - 'A';
	}
}

char SearchStr(char str1[], char str2[]) {
	
	int i = 0; 
	int j = 0;
	
	while(str1[i] != 0 && str2[j] != 0) {
		if (str1[i] == str2[j] || 
			str1[i]+32 == str2[j] || 
			str1[i]-32 == str2[j]) {
			j++;
			if (str2[j] == 0) {
				return 1;
			} else if (str1[i + 1] != str2[j] || 
						str1[i + 1]+32 == str2[j] || 
						str1[i + 1]-32 == str2[j]) {
				j = 0;
			}
		}
		i++;
	}
	return 0;
}

void Search(Contact *kayitlar, int toplamKayit, char aranacak[]) {
	
	int i;
	int bulunanlar = 0;
	printf("Bulunanlar : ");
	for (i = 0; i < toplamKayit; i++) {
		if(SearchStr(kayitlar[i].ad, aranacak) ||
		   SearchStr(kayitlar[i].soyad, aranacak)) {
		   	bulunanlar++;
			printf("Ad : %s\n", kayitlar[i].ad);
			printf("Soyad : %s\n", kayitlar[i].soyad);
			printf("Tel : %s\n\n", kayitlar[i].tel);
		}
	}
	if(bulunanlar == 0) {
		printf("Hicbirsey bulunamadi.");
	}
	
}

void KayitYap(Contact * kayitlar, int toplamKayitlar, int sonKayitYeri) {
	int i;
	for (i = sonKayitYeri; i < toplamKayitlar; i++) {
		printf("%d. kayit icin :\n", i+1);
		printf("Tel numarasi girin : "); scanf("%s", kayitlar[i].tel);
		printf("Isim girin : "); scanf("%s", kayitlar[i].ad);
		printf("Soyisim girin : "); scanf("%s", kayitlar[i].soyad);
	}
}

void PrintMenu() {
	printf("Giris yapiniz :\n");
	printf("1) Veri ekleme\n");
	printf("2) Arama yapma\n");
	printf("3) Cikis\n");
}

int main(int argc, char *argv[]) {
 	int input = 0;
 	int kayitSayisi = 0;
 	Contact *kayitlar;
 	int toplamKayitlar = 0;
	char aranacak[32];

	while(input != 3) {
		PrintMenu();
		printf("Giris : "); scanf("%d", &input); printf("\n");
		
		switch (input) {
			
			case 1:								// Veri ekleme
				printf("Kac kayit yapmak istiyorsunuz : ");
				scanf("%d", &kayitSayisi);
				if (kayitlar == 2) {
					kayitlar = (Contact*)malloc(kayitSayisi * sizeof(Contact));
					toplamKayitlar = kayitSayisi;
				} else {
					toplamKayitlar += kayitSayisi;
					kayitlar = (Contact*)realloc(kayitlar, toplamKayitlar);
				}
				KayitYap(kayitlar, toplamKayitlar, toplamKayitlar - kayitSayisi);
				break;
			case 2:								// Arama yap
				printf("Arama yapilacak ismi giriniz : ");
				scanf("%s", aranacak);
				Search(kayitlar, toplamKayitlar, aranacak);
				break;
		}
	}
	printf("cikti");
	return 0;
}
