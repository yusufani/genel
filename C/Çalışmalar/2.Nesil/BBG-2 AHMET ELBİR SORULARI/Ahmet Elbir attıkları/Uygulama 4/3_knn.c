/* Initial Data Set:
x1  x2  Output  fizik matematik alan
80  80    1  					say�sal
90  75    1  					say�sal
75  80    1  					say�sal
50  60    0  					s�zel 
55  65    0  					s�zel
50  50    0  					s�zel
*/

/* Test Data Set:
x1  x2  Output  fizik matematik alan
40  40   0      				S�zel
90  75   1      				Say�sal
75  65   1      				Say�sal
*/
#include<stdio.h>
#include<limits.h>
void main()
{
    int i,j,k;
    int train[6][3]={{80,80,1},{90,75,1},{75,80,1},{50,60,0},{55,65,0},{50,50,1}};// f1,f2 + class label
    int test[3][3]={{40,40,0},{90,75,1},{75,65,1}}; // 3 test �rne�i
    
    int d_temp, counter=0;
    int nearestIndexOfSample=0;
    double distance=0.0, temp=LONG_MAX;
    printf("TRAIN\n");
	printf("fizik   Mat   Alan\n");
    for(i=0;i<6;i++)  // 6 �rnek egitim
    {
    	for(j=0;j<3;j++)  // 3 �zellik
    	{
    		printf("%3d\t",train[i][j]);
			//scanf("%d",&train[i][j]);
    	} printf("\n");
    }
	 printf("TEST\n");
	printf("fizik   Mat   Alan\n");
	for(i=0;i<3;i++)  // 6 test 
    {
    	for(j=0;j<3;j++)  // 3 �zellik
    	{
    	    printf("%3d\t",test[i][j]);
			//printf("Test[%d][%d]:",i,j);
		//	scanf("%d",&test[i][j]);
    	}  printf("\n");
    }
	
	for(i=0;i<3;i++)  // 3 test verisi i�in
    {
    	for(j=0;j<6;j++)  // 6 e�itim �rne�i i�in
    	{
    		for(k=0;k<2;k++) // 2 �zellik i�in
			{
				d_temp=train[j][k]-test[i][k];
				distance += d_temp*d_temp;
			}
		//	distance=sqrt(1.0*distance);
			if(distance<temp)
			   {
			   	temp=distance;
			   	nearestIndexOfSample=j;
			   }
		   distance=0.0;
		}
		
		if(train[nearestIndexOfSample][2]==test[i][2])
		{
			printf("%d. Test Verisi=>Siniflandirma Basarili\n",i+1);
			counter++;
		}
		else
		    printf("%d. Test Verisi=>Siniflandirma Basarisiz\n",i+1);
	}
	printf("Toplam %d Dogru Tahmin Vardir. Basari %% %3.2f",counter,100.0*counter/3)	;
   
    getch();
    return 0;
}
