Outside the heavy rock fraternity, it has always been considered a rather ridiculous custom.

But now, it emerges, the practice of headbanging is not only slightly silly � it�s downright dangerous.

Experts are warning that violently rocking your head back and forth to loud music can damage your health.
 
So when rock fans look dazed and confused, it�s probably caused by headaches and dizziness from their incessant head and neck motion.

The faster the song, the greater the chances of neck injury, according to researchers in Australia. They even suggest protective equipment such as neck braces could be the next must-have accessory at a gig.

Professor Andrew McIntosh, of the University of New South Wales, said headbanging to songs such as AC/DC�s Highway to Hell put fans at risk of �mild traumatic brain injury�. He added: �This study helps to explain why metal concert-goers often seem dazed, confused and incoherent.�

The study, published in the Christmas issue of BMJ (British Medical Journal) Online, says headbanging started in 1968 at a Led Zeppelin concert where fans hit their heads on the stage in time to the music.

It has developed into a collection of distinctive styles including the up-down, the circular swing, the full body and the side-to-side. 

The practice is celebrated in the movie Wayne�s World when Mike Myers, Dana Carvey and friends headbang to Queen's epic Bohemian Rhapsody.

Previously, there have been anecdotal reports of headbanging-related injuries including hearing loss, stroke and brain injury.

For their study, Professor McIntosh and researcher Declan Patton, of the university�s school of risk and safety sciences, attended a number of concerts by artists
including Motorhead and Ozzy Osbourne.

They identified the up-down style as the most common headbanging technique and constructed a theoretical model to examine the effect of the head and neck motions.

The researchers found an increasing risk of neck injury beginning at tempos of 130 beats per minute.

The average headbanging song has a tempo of about 146 beats per minute, at which head banging may cause headaches and dizziness if the range of movement of the head and neck is more than 75 degrees.

Professor McIntosh said: �To minimise the risk of head and neck injury, headbangers should decrease their range of head and neck motion, headbang to slower tempo songs by replacing heavy metal with adult oriented rock, only headbang to every second beat, or use personal protective equipment.�

