A jab to prevent children developing diabetes came a step closer last night following a British breakthrough. 
Studies show a common tummy bug is strongly linked to childhood diabetes, which can shorten life and lead to blindness, heart disease, kidney failure and amputation. 
The findings could lead to a vaccine that would protect against the bug and 'drastically reduce' the prevalence of childhood, or type 1, diabetes. 
The studies focused on enteroviruses - a family of more than 100 bugs that cause vomiting, diarrhoea and cold symptoms.
 
The first showed that enterovirus infection of the pancreas is much more common in children with type 1 diabetes than those without. 
Researchers from the University of Brighton, Glasgow Royal Infirmary, and the Peninsula Medical School in the South West of England found that more than 60 per cent of diabetic pancreases studied had traces of the bug. 
But it was hardly spotted in those without the disease, the journal Diabetologia reports. 

The finding suggests infection plays a key role in the development of the disease. It is thought that enteroviruses trigger a rogue immune response that destroys insulin-producing cells in the pancreas in those genetically vulnerable to diabetes. 
Further work on the role of enteroviruses could lead to the development of a jab which would 'drastically reduce' the number of people developing diabetes, said the researchers. 
Type 1 diabetes affects around 300,000 in the UK, including 20,000 children under 15. 
Professor Adrian Bone, of Brighton, said: 'There has been a lot of evidence about peaks of diabetes following outbreaks of viral infections but this is the first direct link. 
'What nobody else is done is put the viruses at the scene of the crime. It is extremely exciting. 
'If you can narrow it down to a specific virus or small family of viruses, then you are in the business when it comes to developing vaccines.' 
Professor Noel Morgan, of the Peninsula school, said: 'The next stages - to identify which viruses are involved, how cells are changed by infection and the ultimate goal to develop a vaccine - will lead to findings which we hope will drastically reduce the number of people around the world who develop type 1 diabetes.' 
Dr Alan Foulis, of Glasgow, said: 'With 250,000 sufferers, the idea of a vaccine and being able to prevent this disease would be my life's work.' 
A second study, by Cambridge University, has linked a gene called IFIH1, type 1 diabetes and enteroviruses, the journal Science reports. 
Karen Addington, of the Juvenile Diabetes Research Foundation, said the studies would give fresh hope to those with type 1 diabetes. 
She added: 'This life-threatening condition requires a lifetime of painful blood tests and insulin injections. Incidences are increasing by four per cent each year. Research such as this brings us closer to curing this condition.' 
Dr Iain Frame, of the charity Diabetes UK, said: 'The next steps to identify the viruses and find out what they are doing to the infected cells will be hugely exciting and will take us a step closer to preventing type 1 diabetes.' 
