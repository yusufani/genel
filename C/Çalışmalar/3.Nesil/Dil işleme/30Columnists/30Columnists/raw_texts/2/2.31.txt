IT sounds like magic. With a wave of the SNP wand, an unpopular tax disappears and is replaced by a new tax which makes everyone better off � just like that.

That seems to be more or less what Finance Minister John Swinney was promising when he unADVERTISEMENTveiled the Scottish Government's plans for a local income tax in place of the council tax.

But the proposals have been greeted with much scepticism, claims of a potential "black hole" in the finances and confident predictions that it will never happen.

The idea of a local income tax sounds good in principle � it would mean people paying according to their means and the poor being relieved of an unfair payment burden.

But there are problems too. One is that a family or couple who are both earning, but who live in a relatively modest house, could suddenly find themselves with a far bigger bill.

Whether or not that's fair, it is likely to go down badly with a lot of voters � especially in Edinburgh, where prosperity is common but house prices are high.

The SNP's plans would also allow the very rich, who live off income from their stocks and shares, to escape the new tax altogether because it would cost more to collect the tax from them than it would bring in.

Mr Swinney and his colleagues claim that everyone, except the top ten per cent of earners or four out of five households, will be better off under the proposed new 3p-in-the-pound tax.

On the government's figures, a single person in a Band D house � taken as the average across Scotland � would need to earn more than �33,675 before their local income tax would be higher than their council tax. 

A family or couple in a Band F house would need a joint income of around �66,000 before they became losers from the change.

Critics say there is a "black hole" of nearly �700 million in the calculations that allows the government to come up with these figures.

But part of the "magic" trick is that the SNP plans to pump an extra �280m of central government cash into local authorities to keep the new tax down.

The sums have also been done on the assumption that Scotland will continue to receive the �400m which currently comes north of the Border in council tax benefit. 

The SNP insists this is an integral part of UK Government funding for Scotland, while Labour argues that if the council tax is scrapped, the council tax benefit would also go.

The independent Burt report on local taxation, published in 2006, estimated that without the benefit cash, local income tax would have to be levied at 6.5p in the pound to raise the same as council tax. It said that would mean, for example, a household with two adults in a Band D home, each earning around �20,000, would find themselves paying around �1670 income tax, compared with �1129 council tax.

Burt came down in favour of a local property tax based on individual valuations for every house, and worked out it would have to be levied at flat rate of one per cent to raise the required amount.

But that idea was swiftly rejected by the then Scottish Executive before the report was even published. The SNP never mentions the council tax without putting the word "unfair" before it. But how unfair is it? The value of someone's house is not an exact measure of their wealth or income, but it does bear some relation. Of course, there are pensioners living in bigger houses who find it difficult to pay the bills.

And because of the banding system � which still uses 1991 prices � there are people living in houses valued at over �1m back then who pay no more than those in houses valued at �212,000. 

But when it was first introduced in 1993 � to replace the hated poll tax � the council tax was largely uncontroversial.

Its unpopularity now probably has more to do with the steep rises there have been over the years � 60 per cent in the last decade, according to the SNP � rather than the principles behind it.

AND some of the unfairness could be tackled � by greater take-up of the council tax benefit (about a third of families and 40 per cent of entitled pensioners are not claiming it) and by adding extra bands to spread the load more equitably.

The promise of scrapping the council tax and switching to a local income tax was a key part of the SNP's manifesto at last year's Holyrood elections. But it is still unclear whether the party can build a majority in the parliament for the change. 

It would need the support of the Lib Dems and either the support or abstention of the two Green MSPs and independent Margo MacDonald.

The Greens and Ms MacDonald are sceptical of the proposals � and although the Lib Dems believe in a local income tax and are having talks with ministers on the issue, their version involves each council setting its own tax level rather than central government fixing it at 3p.

It has been suggested before that the potentially damaging electoral consequences of local income tax has made the Lib Dems wary of the whole policy.

One Labour ex-minister says: "When we were in government with the Lib Dems they never pushed it. I don't think they'll be in any rush to sign up to it now. I don't think it is going to happen."

The government's proposals are now out to consultation, but even Mr Swinney says the new tax would not be introduced before 2011/2012 at the earliest.
