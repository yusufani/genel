Oil giant Royal Dutch Shell is facing renewed accusations of profiteering after reporting record profits of $27.6bn (�13.9bn) - the highest-ever figure reported by a British company.

The huge profits - equivalent to more than �1.5m an hour and a 9% increase on last year - will cause anger amongst Britain's motorists who are paying over �1 a litre for petrol after the huge increase in oil prices in recent months.

The Shell figures were immediately branded "obscene" by Unite, Britain's biggest union, which called on the government to levy a windfall tax on the oil industry. 

Joint general secretary Tony Woodley said the union had no problem with profits but that consumers should question the "excessive, mega-profits" of the oil companies.

"Shell shareholders are doing very nicely whilst the rest of us, the stakeholders, are paying the price and struggling," he said.

"Record profits of over thirteen and a half billion pounds at Shell and cumulative oil industry profits in excess of fifty billion in the last three years are, quite frankly, obscene. It is time the government acted."

Shell chief executive Jeroen van der Veer described the results as "satisfactory" and said the group had made good progress in 2007
