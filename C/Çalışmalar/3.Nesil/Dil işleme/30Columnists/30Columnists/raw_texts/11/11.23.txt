British patients are to test a jab to beat the deadly Clostridium difficile bug.
The vaccine is the first in the world to be developed against C. diff infections which affect thousands of elderly hospital patients each year. 

Altogether 600 men and women who are acutely sick with C. diff at 30 hospitals will be involved in a new clinical trial.
They will be split into four groups, with three groups given a series of shots with the jab and antibiotics while the fourth group is given antibiotics alone. 

Although the new drug is being used as a treatment in the trial, if the results are successful it will eventually be marketed as a vaccine. 

It is hoped that people over 65 years would be given a one-off preventive shot - much like the pneumoccocal vaccine.
But it may be necessary to have a course of three jabs followed by a booster after 10 years.
Those over 65 are at highest risk especially when treated with antibiotics that destroy the normal balance of the gut allowing C. diff to take hold.

Previous trials have shown the vaccine to be safe, causing few side effects, and U.S. research showed a course of injections cleared up recurrent infections.
The most severely ill of the patients studied, a 71-year-old woman, had been taking antibiotics for two years to combat diarrhoea. A series of shots of the jab succeeded where the drugs had failed.
Originally developed by the Cambridge-based biotech company Acambis, the jab is now being put through extensive patient trials by the vaccines giant Sanofi Pasteur.
Barry Cookson, Director of the Laboratory of Healthcare Associated Infections, Centre for Infections, Health Protection Agency and the lead investigator of the trial, said: 'Treatment of C. difficile infection includes the use of one of two antibiotics. 

'Non-antibiotic approaches for managing C. difficile infection are badly needed since the alteration of the gut flora associated with antibiotics triggers the infection in the first place. 

'There is also considerable concern about the emergence of antibiotic-resistance in C. difficile and other bacteria. 

'Vaccination has the potential to be a very effective strategy to combat gastrointestinal pathologies caused by C. diff along with better antibiotic stewardship and infection control practices.'
There were 7,000 C. diff infections reported in people aged 65 and over from July to September last year in England and Wales.
It was mentioned on 8,000 death certificates in 2007 - a doubling of the number in 2005.
The vaccine works in a similar way to the tetanus jab, where it works not on the bug itself but the poisons it produces. 

C. diff produces a pair of poisons which irritate the lining of the bowel, causing diarrhoea and even a potentially fatal infection.
Official surveys show up to one in three hospitals is flouting guidelines aimed at controlling the spread of C. diff, which include hand-washing and isolation of infected patients.
Unlike MRSA, hospital control is difficult to achieve because alcohol hand scrubs are ineffective and its spores are resistant to routine hospital cleaning
