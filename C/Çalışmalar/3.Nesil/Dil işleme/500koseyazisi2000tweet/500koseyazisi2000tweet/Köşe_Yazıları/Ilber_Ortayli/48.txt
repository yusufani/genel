﻿100’üncü yılında Çanakkale zaferi

Çanakkale’ye saldıran donanmanın mayınlar yüzünden ağır tahribat geçireceği ve savunma alanlarından gelen tepkiyle de geri çekileceği açıktır. Bazı zırhlıları batmış, bazıları geri çekilmişti



Çanakkale Savaşı’nda İtilaf Devletleri ordusuna geçit vermeyen askerlerimiz bir dinlenme anında.

Birinci Dünya Savaşı’na yeryüzünün büyük bir kısmı katılmıştır. Ülkeler genellikle temmuz sonu ve ağustos başında Avusturya-Macaristan, Almanya, Rusya, nihayet Fransa ve Britanya İmparatorluğu olmak üzere karşılıklı savaş ilan ettiler. Türk imparatorluğu Çanakkale ağzına sığınan ve Enver Paşa ile Büyükelçi Wangenheim görüşmesi üzerine sözde satın aldığı ama Alman mürettebatını tahliye etmediği, isim değiştiren Goben ve Breslau (Yavuz ve Midilli) zırhlıları ve refakat muhribiyle Sivastopol, Yalta limanlarını bombaladıktan sonra 31 Ekim 1914 tarihinde savaşa resmen girmiş oluyordu.

Savaşa zorunlu girdik

2 Kasım’da da Rusya, Osmanlı’ya  savaş ilan etti ve ardından savaş ilan eden İngiliz ve Fransız harp gemileri Çanakkale Boğazı’ndaki Seddülbahir’i, Kumkapı ve Orhaniye tabyalarını bombaladılar. Eylül başından beri Boğaz’da tahkimat yapma emrini Enver Paşa vermişti. Bununla birlikte birleşik donanma karşısındaki savunma büyük kayıpla sonuçlandı.
Beş subay ve 80 asker şehit oldu.

Boğaz hareketinin başlamasından bir müddet sonra Rusya Dışişleri Bakanı Sazonov, Britanya’yı protesto etmekle kalmadı, tehdit etti. Rusya savaştan çekilecekti. Zira kendisine vaat edilen İstanbul ve Boğazlar İngiltere’nin bu manevrasıyla Rusya’nın elinden alınıyordu. En büyük kara ordusuna sahip Rusya (5 milyon asker) bu savaşta niçin İtilaf Devletleri safında olduğunu sorguluyordu, Churchill ise İstanbul ve Boğazlar’ı Rusya’ya vermeyi taahhüt eden bir anlaşmaya gitti.

1912’den beri fiilen savaş içinde olan Osmanlı İmparatorluğu aslında I. Cihan Savaşı’na zorunlu olarak giriyor, Avrupa devletleri ve Rusya’daki gibi çılgın zafer çığlıkları atılmıyordu. Türkiye durumun vahametini ve harbin uzun süreceğini anlayan tek genelkurmaya sahipti. Ekim sonunda dahi Genelkurmay ikinci başkanı İsmet Bey (Paşa), birinci başkan Alman Bronsart von Schellendorf idi. Almanya ile ittifakın Almanların Rusya’ya karşı Tannenberg’de kazandıkları zafer dolayısıyla fazla abartılmamasını, bu ordunun kuvvetinin Marne cephesinde Mareşal Geoffrey başkanlığındaki Fransızlar karşısında duraklamasından sonra sorgulanması gerektiğini ve ittifaktan kaçınmak gerektiğini ileri sürdü.

Ne var ki Çanakkale’deki Churchill ve Sazonov inadı ve saldırganlığı aksini düşünmeye pek imkan bırakmıyordu. Türkiye Esad Paşa, Fevzi Paşa, Yarbay Mustafa Kemal Bey, Kazım Karabekir ve İsmet Bey gibi  değerli kurmaylarının görüşüne rağmen Alman safında bu harbe sürüklendi. Beylerbeyi Sarayı’na çekilen sabık Sultan II. Abdülhamid’in “Bu cihad öyle bir silahtır ki kullanılmaması kullanılmasından daha etkilidir” sözünü hatırlayalım. Bununla beraber Çanakkale’de ne İngiliz ne de Fransız saflarında kayda değer miktarda Müslüman sömürge askeri vardı.

Doğru bir savunma

İlk hücum ve savunmadan sonra savaşın Çanakkale Boğazı aşılarak İstanbul’a yönelmesi meselesi Churchill’in kesin kararıyla oldu. Bu planın uygulamaya konmasıyla da Rusya’nın malum itirazı tekrar ortaya çıktı. Fakat diğer yandan da Başkomutan Grandük Nikola bu operasyonun bir an evvel bitmesini ve Rusya’ya yardımın ulaşması gerektiğini belirtiyordu.

Üç ay boyunca hazırlıklar devam etti. Türk imparatorluğu doğru olarak kara ordularının yapacağı savunmaya önem veriyordu. Çanakkale müstahkem mevkiinin başında Esad Paşa vardı. Lakin birleşik ordular komutanı olarak İstanbul’a gelen teftiş heyetinin başkanı Liman von Sanders’i başkomutan vekili Enver Paşa Gelibolu’ya tayin etti.

Birleşik ordu sözüne bakarak Alman-Avusturya askeri kuvvetlerinin sayısını abartmayalım: Gelibolu’da müttefiklerimizin daha çok asri teknolojiyi temsil eden mühimmat yardımı söz konusudur. Liman Paşa Prusya ordusunda sivrilmiş bir isim değildi, lakin geçen zaman içinde düzgün bir kurmay olduğu, Türk komutanların görüşlerine itibar etmekte makul davrandığı görüldü. 

Tarihe geçen başarı

Bu savaşta Türk ordusunun genç ama tecrübeli ve bilgili kurmay grubunun bir cephede toplandığı görülür. Asıl savunmanın başlayacağı 18 Mart’tan bir ay evvel Boğaz’daki mayınlı alanları Müttefikler temizlemesine rağmen saldırı başlayacakken yeniden gizlice mayın döşendi. Bu Nusrat mayın gemisinin askerlerinin tarihe geçen bir başarısıdır.

Bir gün sonra hücuma geçen İtilaf Devletleri donanmasının bu yüzden ağır tahribat geçireceği, savunma alanlarından gelen tepkiyle de geri çekileceği açıktır. O anda dünyanın en mükemmel zırhlı gemisi yüzen kale Queen Elizabeth yara almış çekilmişti, Ocean ve Bouvet battı, Agamemnon sahayı terk etti. Bu bir hezimet sayılıyordu. Boğaz’ı gemilerle geçmekten vazgeçildi. Bununla birlikte “Çanakkale’nin geçilmezliği”
18 Mart Deniz Zaferi’yle değil, daha çok sonraki kara savaşlarıyla tescil edilmiştir. Nisan ortalarında Çanakkale Kara Savaşları anılacak. Bu bölümü o zaman konuşacağız.

“Olsaydı tutsaydı” hesabı

Burada bir konuyu belirtelim; basındaki garip meşhurlarımızdan biri “olsaydı, tutsaydı” hesabıyla “Çanakkale’de direnmeyip gemileri geçirsek ne olurdu”yu yazıyor. En çok güldüğüm, adamın Britanya işgalindeki İstanbul’da kendisinin İngilizce gazetecilik yapabileceğine inanması. Ne iş yapardı bilmiyorum ama bugünkü işini dahi yapamazdı. Zira İstanbul’un azınlıkta kalacak Türklerinin dertlerini anlatmak için daha nitelikli gazetecilere ihtiyacı olurdu.

1915’teki işgal ve Osmanlı’nın  bertaraf edilmesi savaşın bitimini hızlandıracaktı. “İşgal” ve “mütareke” aynı şey değildir. Mütarekede gelen sözde galip ama yorgun orduları savaş içinde tecrübeleri artmış, güçlenmiş komutanlar Türkiye’den attılar. Bunu o gün Britanyalılar anlayamadı. Bir tek General Metaksas anlamıştı. 1919’un mayısında İzmir’e çıkmaya hazırlanan Yunan ordularına “Çıkmayın, karşıda ordu ve subay var; hele hiç ilerlemeyin” demişti. Haklı da çıktı.