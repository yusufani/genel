'Sürece' bir de bu açıdan bakalım Zehir kime, şerbet kime?

Nevruz kutlamalarını "normalleştirmek" bundan sonraki ortak yaşantımız için gerekli. Bir zamanların Kürtçe kaset yasağı gibi, Nevruz'u "sakıncasız bayram
kutlamasına" dönüştürmeli.
Paris infazlarından sonra, cenaze törenlerindeki olağanüstü hassasiyet "kitlesel kontrolün istenince
sağlanabileceğini" gösteriyordu. Nevruz da böyle olmalı.
Kışkırtıcı görüntü ve sloganlardan "iki taraf da kaçınmalı."
Toplumda, "sessiz ve derin bir bekleyiş" hâkim. Bunu görmeli. İstismar etmemeli.
"Terörün bitmesine bir şans verme adına" Türkiye olgunluk gösteriyor.
Fakat ben bir taraftan da
korkmuyor değilim doğrusunu
isterseniz.
Hassasiyetler gerçekten karşılıklı mı?
Dün İstanbul'daki Nevruz
kutlaması görüntüleri gelecek için endişemi artırdı.
Geçtiğimiz hafta ilginç bir olay yaşamıştım. AK Parti sözcüsü Hüseyin Çelik'le konuşmuştum. Beş dakika sonra aradı ve
"yazmamamı" rica etti. Ertesi gün Enerji Bakanı Taner Yıldız'la İstanbul'da bir araya geldik. Bir buçuk saat sohbet ettik. "Peki" dedim, "Peki Sayın Bakan, barış süreci nasıl gidiyor?"
"Ben o konuya girmeyeyim" yanıtını verdi. Oysa gelişmeleri yakından takip ettiğini biliyorum. Özellikle Kuzey Irak bağlamında...
Yıldız'dan bir gün sonra da Ulaştırma, Denizcilik ve Haberleşme Bakanı Binali Yıldırım İstanbul'da bir grup gazeteciyle yemekte buluştu. Kendisine "Kandil'de BDP'li
vekillerle PKK'lı teröristlerin fotoğrafları" soruldu. Binali Yıldırım, hissiyatını anlattı, sonra da neden öyle düşündüğünü... "Yazabilir miyiz?" diye soruldu, "Tabii" dedi Bakan. Notları aldık. Gazeteye
dönmeden hepimizi telefonla arayıp, söylenenler için "off the record/kayıt dışı" ibaresi düşüldü. Üç bakanın da gerekçesi aynıydı:
"Süreç çok hassas. Dikkat etmek, zarar vermemek gerekiyor. Başbakan Erdoğan kimse konuşmasın diye uyardı."
Çok güçlü bir tek parti iktidarından bahsediyoruz. On yıldır iktidarda. Riskli bir girişimi
sahiplendi. Sorunu çözmeye de kararlı görünüyorlar. Ama, neredeyse olağanüstü derecede temkinli davranıyorlar. Yeri geliyor,
içlerindeki tepkiyi dile getiriyorlar ama baldıran zehiri içip, boğazlarını dokuz düğüm yapıp susuyorlar.
Güzel.
Aynı özeni acaba "karşı taraf"
gösteriyor mu?
İşte İmralı tutanaklarının sızdırılması...
Sahi siz o hikâyeye inandınız mı? Çaycı, fotokopici, basın müşaviri, gazozcu hikâyesine...
Geçelim...
Bu Nevruz kutlamalarına da BDP'lilerin açıklamalarına da o gözle bakmalı.
Barış "ya her yerde ya hiçbir yerde" ise "fedakârlıklar da öyle" olmalı.
Türkiye'nin büyük bölümündeki "sessiz ve zımni onayı" bozmamak, kışkırtıcı olmamak adına BDP de zehir içsin. Sussun.