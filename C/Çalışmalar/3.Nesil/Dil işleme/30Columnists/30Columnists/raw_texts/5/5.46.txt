As the sun was sinking on Hillary Clinton�s presidential campaign, the nation�s wounded feminists were burning up the Internet.

�The passion is very intense,� said Muriel Fox, a retired public relations executive in New York who was one of the founding members of the National Organization for Women. �It�s very much a feeling that Hillary has not been respected.�

Feel free to make fun of them. The women of Fox�s generation ought to be used to it by now. The movement they started was the first fight for equality in which the opposition deployed ridicule as its most lethal weapon. They won the ban on sex discrimination in employment by letting a conservative congressman propose it as a joke. When they staged their historic march in New York in 1970, they heard themselves described as �braless bubble-heads� by a U.S. senator and were laughed at on the evening news. 

They had always seen a woman in the White House as the holy grail. Now their disappointment is compounded by the feeling that Clinton�s candidacy was not even appreciated as a noble try.

�She stayed in and showed she could take it. I feel she�s taken this beating for us � the abuse and the battering and the insults,� said Fox.

I get asked all the time whether I think Hillary lost because sexism is worse than racism in this country. The answer is no. She lost because Obama ran a smarter, better-organized campaign. It�s possible that she would have won if the Democratic Party had more rational primary rules. But Obama didn�t make up the rules, and Clinton had no problem with them until she began to lose. 

Here�s where the sexism does come in. If Barack had failed in his attempt to make history by becoming the first African-American presidential nominee, you can bet we�d have treated his defeat with the dignity it deserved. Even if he went over the deep end at the finale and found it hard to get around to a graceful concession. 

For a long time, Obama supporters have seen party unity as something that Hillary could provide by capitulating. It also requires the Democrats to acknowledge what she�s achieved. If that makes them feel like wimps, let them take it out on John McCain.

Clinton is very much a product of the generation that accepted a certain amount of humiliation as the price of progress. She wrote in her autobiography that when she ran for president of her high-school class against several boys, one of them told her she was �really stupid� if she thought a girl could be elected president. She lost, and later, the winner asked her to head a committee �which as far as I could tell was expected to do most of the work.� She swallowed hard, accepted and, she admitted, really liked organizing all the school parades and dances and pep rallies.

This is one of the things you have to admire about Hillary Clinton. She still enjoys the work.

Over the past months, Clinton has seemed haunted by the image of the �nice girl� who gives up the fight because she�s afraid the boys will be angry if they don�t get their way. She told people she would never, ever say: �I�m the girl, I give up.� She would never let her daughter, or anybody else�s daughter, think that she quit because things got too tough.

And she never did. Nobody is ever again going to question whether it�s possible for a woman to go toe-to-toe with the toughest male candidate in a race for president of the United States. Or whether a woman could be strong enough to serve as commander in chief. 

Her campaign didn�t resolve whether a woman who seems tough enough to run the military can also seem likable enough to get elected. But she helped pave the way. So many battles against prejudice are won when people get used to seeing women and minorities in roles that only white men had held before. By the end of those 54 primaries and caucuses, Hillary had made a woman running for president seem normal.

Her campaign was messy, and it made some fatal tactical errors. But nobody who sent her a donation could accuse her of not giving them their money�s worth. 

For all her vaunting ambition, she was never a candidate who ran for president just because it�s the presidency. She thought about winning in terms of the things she could accomplish, and she never forgot the women�s issues she had championed all her life � repair of the social safety net, children�s rights, support for working mothers.

It�s not the same as winning the White House. But it�s a lot. 

