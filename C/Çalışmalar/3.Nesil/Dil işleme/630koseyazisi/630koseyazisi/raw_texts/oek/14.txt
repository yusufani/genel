Aldanm���z... 
FRANSIZLAR ve Hollandal�lar, onaylanmas� i�in �nlerine konan Avrupa Birli�i (AB) Anayasas��na g��l� bir sesle �hay�r� dedi�i zaman, bunun sonu�lar� �zerine yorum yapanlar�n �o�u -bu sat�rlar�n yazar� dahil- �O bizi ilgilendirmez� demi�tik.

Aldand���m�z �imdi net bir �ekilde ortaya ��k�yor. 

Me�er o ret oylar� bizi �ok derinden ilgilendiriyormu�.

�stelik aldanma, ne sadece bizlerle ne de sadece o olayla s�n�rl� imi�.

�ok daha geni� boyutlu bir aldanma-aldatma s�recini ya�ad���m�z� Br�ksel�de �nceki g�n ba�layan AB liderleri toplant�s� ortaya koydu.

Br�ksel�den gelen ��at�rt��lardan anla��ld���na g�re �nce Avrupa Birli�i binas�n�n �ok sa�lam bir yap� oldu�unu d���n�rken de aldanm���z. 

Ger�i �ngiltere ile Fransa aras�nda patlayan �b�t�e� krizi (biliyorsunuz �ngilizler, AB�den her y�l ald�klar� 4.6 milyar Euro�dan (Avro) vazge�mesini isteyen Fransa�ya itiraz ediyor ve Fransa da 3 milyar Euro�dan vazge�medik�e evet demiyorlar) bu defa zor da olsa ��z�l�r deniyor ama as�l mesele orada de�il, Fransa�n�n kendisinde...

Fransa -d�n Ertu�rul �zk�k��n de yazd��� gibi- �zellikle �AB Anayasas��na hay�r� denmesi �zerine adeta kimlik de�i�tirdi. Jacques Chirac��n Fransa�s�, AB liderli�ini Almanya ile payla�an, etkin �lke yerine, AB i�inde sorun yaratan bir �lke g�r�nt�s� vermeye ba�lad�. 

S�ze devam etmeden soral�m:

Bu anayasan�n ger�ek mimar� Fransa de�il mi? Eski Frans�z Cumhurba�kan� Giscard d�Estaign�i bu i�in ba��na s�s olsun diye mi koydulard�? 

O halde kabahati ret oyu verene veya T�rkiye gibi konunun d���nda kalan �lkeye bulacaklar�na, kendi yapt�klar� anayasada bulsunlar.

Frans�zlar malum, her t�rl� kusurdan m�nezzeh (ar�nm��) olduklar�na inand�klar� i�in halk�n �ret� oyu kar��s�nda, pusulas�n� kaybetmi� tekneye d�nd�ler.

Art�k kar��m�zda sadece �a�k�nl�k i�inde bocalayan Fransa yok. Ayn� zamanda �d�nk� s�zlerini ink�r etmek i�in bahaneler uyduran� bir Fransa var...

Cumhurba�kan� Chirac bir yandan �taahh�tlerimize ba�l� kalmal�y�z� derken, �ortaya ��kan yeni durum kar��s�nda geni�leme meselesini g�zden ge�irmeyi� �neriyor. Onun yar�m b�rakt��� laf� Ba�bakan� Dominique de Villepin tamamlay�p, Frans�z Parlamentosu�nda yapt��� konu�mada, �Bulgaristan ve Romanya 2007�de �yeli�e kabul edilmeli. Ama sonras�nda yeni geni�lemeleri tart��maya a�mal�y�z� diyor. 

Neyi tart��acaks�n�z? 

Bunca y�l oyalad���n�z T�rkiye ile �m�zakerelere ba�lama� karar�n� m�?

Zaten �ucu a��k� dedi�iniz m�zakerelerin ucunu, �T�rkiye�ye kapal��ya �evirmenin yolunu mu?

�imdi s�zden cayman�n yolunu arayanlar bir ay �nce �Reformlar� hemen ya�ama ge�irin... H�z�n�z� kaybetmeyin� diye bizi s�k��t�rm�yorlar m�yd�? 

Belli ki bir hatay� da �Bat�l� verdi�i s�z� tutar� derken yapm���z. 
