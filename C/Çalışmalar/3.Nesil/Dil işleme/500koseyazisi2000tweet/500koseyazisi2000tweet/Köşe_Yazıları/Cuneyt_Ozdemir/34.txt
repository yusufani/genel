Hay kedi canını senin!

Seçim gecesi güya trafoya kedi kaçtı. Türkiye 70 yılın en karanlık gecesini geçirdi. Tarih, oy oranları kadar esrarengiz kedi örgütünün bu eyleminin arka planını da yazacaktır.

Evvel zaman içinde, kalbur zaman içinde, çok uzak değil yakın bir ülkede sevimli uslu küçücük gözlü küçük kediler yaşarmış. 

Son 70 yılın en karanlık seçimindeki elektrik kesintilerinin bahanesi olarak gösterilen kedilerin Bülent Ortaçgil’in şarkısındaki kediler olmadığı kesin. Aklımızla dalga geçer gibi rastlantı bu ya tam da seçim gecesinde son 70 yılın en karanlık gecesini geçirdik. Güya trafoya kediler kaçtı… 

Anayasa Mahkemesi’nin Ankara seçimleri ile ilgili kararı ne olursa olsun bu kapsamlı elektrik kesintisi bile 2014 yerel seçimlerinin üzerine gölge düşürmeye yetip de artar. Tarih, elbette oy oranları kadar bu esrarengiz kedi örgütünün yaptığı elektrik kesintilerinin arkasında ne yattığına dair manidar soruyu da yazacaktır. 

ABD’nin Ankara Büyükelçisi gibi AYM’nin Ankara seçimleri kararını beklerken, Bülent Ortaçgil’in kediler şarkısını mırıldanmaya devam edelim. 

Yakının ülkenin yanında, dönemeci dönerken, rüzgârların sağında, ormanların solunda, sesli hırslı kocaman gözlü büyük kediler varmış. 

Siz kediler hangi kedileri seversiniz hangi kediler gibi yaşamak istersiniz? 

Sevimli uslu, sesli hırslı hangi kedilerdensiniz? 

Piyasa seviciler

Kültür sanat dünyamızın amiral gemisi İstanbul Art News’ın nisan sayısının sayfalarını çevirirken ilginç bir ilana rastladım. ‘Koleksiyoner ve Sanatseverlerin Dikkatine’ başlıklı bu ilan tam sayfa verilmişti. Siyah zemin üzerine beyaz harflerle yazılı olan metin bir manifesto niteliğindeydi. Özetle “Sanatı bir menkul kıymetler borsası olarak görmek kabul edilemez. Sanat ve alışverişin hızlı bir kâr, bir gösteriş rekabeti, kapitalin kontrol ettiği bir piyasa oyunu olarak kabul edenler ve bu hırsı ortaya dökenler en büyük zararı sanata ve sanatçıya sonra kendilerine vermektedir” diyordu. İlan uzun olduğu için tamamını alıntılayamıyorum. İnternette aradım hiçbir yerde konu edilmediğini gördüm. Bu da sanattan artık ne kadar koptuğumuzun bir göstergesi. Asıl ilginç olan, ilanın altındaki galerinin ismiydi: Piramid Sanat! Bilmeyenler “E ne var bunda” diyebilir. Piramid Sanat sevgili Bedri Baykam’ın sahip olduğu bir galeri. Geçen günlerde Bedri Baykam’ın ‘boş çerçeve’ adlı eserini 125.000 dolara, siyaseten yerden yere vurduğu Murat Ülker’e satış meselesi ve sonrasında kopan fırtınaya hiç girmeyeceğim. Sonra kızıyor… 

Başörtülüler de bölündü!

Zaman zaman burada yayımladığım okur tepkilerinin ne kadar büyük infial yarattığının farkındayım. Kimi zaman bir blog’dan, kimi zaman bana gönderilen bir mail’den yaptığım alıntıları sanki benim düşüncelerimmiş gibi eleştiren, yayan kötücül kimselerin olduğunu da biliyorum. Elbette bu tür dezenformasyon cambazları var diye okur mektuplarını görmezden gelecek değilim. Öylesine mektuplar alıyorum ki inanın bazen okurken zamanın ruhuna dair hiç bilmediğim durumları öğrenip şaşırıyorum. Bunlardan bir tanesini daha sizlerle paylaşmak istiyorum. Bu sefer okurum mail’ini Samsun’dan yazmış. Adını vermeden bana gönderdiği mektubu paylaşmak istiyorum: “Ben 44 yaşında 3 çocuk annesi bir kadınım. Samsun’da yaşıyorum. Her sabah 5 yaşındaki kızımı okula götürürken sokakta gördüğüm ama tanımadığım insanlarla selamlaştığım oluyor. Bazı insanlar peygamber efendimizin ‘Aranızda selamı yayın’ tavsiyesine uyarak tanımasalar da yolda gördükleri insanlara selam veriyorlar. İki gün önce de zaman zaman yaptığım gibi şahsen tanımadığım ama aynı saatlerde aynı yolu kullanmaktan dolayı simasına aşina olduğum bir kadınla göz göze geldik ve selamlaştık. Kadın uzaklaştı ama benim aklıma bir şey geldi ve sonrasında düşündüklerimden dolayı dehşette kapıldım. ‘Acaba’ dedim ‘Bu başörtülü kadın hangi tür başörtülülerden? Yani Ak Partili mi yoksa cemaatten mi? Benim için fark etmez, her halükârda ben yine selamı esirgemem ama eğer o benim kendisiyle aynı zihniyette olmadığımı öğrenseydi yine bana selam verir miydi?’ Benim de elbette bir görüşüm var ve benimle aynı düşünmeyenleri düşman olarak görmüyorum. Ama neden böyle düşünmeye başladım? Ne hale geldik Allahım! Bu ülkede insanları başörtülü başörtüsüz diye ayırdıkları yetmiyormuş gibi başörtülüleri de cemaatten ve hükümetten yana olanlar diye ayırdılar. Yazıklar olsun! Allah’a bunun hesabını nasıl vereceksiniz? ‘Vatan tehlikedeydi’ mi diyeceksiniz?” 

İster cemaatten olun, ister Ak Parti’den fark etmez. Buyrun verin cevabını…