Ballack is like all the other ingrates among Chelsea players and supporters. He does not give Grant the credit he deserves. 



Last summer, Jose Mourinho briefed media mates that Ballack was unlikely to be in his team. Ten months on, Ballack is central to Grant�s plans for success. 



Germany�s Ballack was at his most influential in the first leg of the Champions League semi-final at Liverpool. In the second leg he won the crucial penalty. He scored the Premier League goals that beat Manchester United. On Monday he added another goal at Newcastle so Chelsea are still breathing down United�s necks. 



Yet, interviewed by German magazine Der Spiegel, Ballack said there was so much class in the Chelsea team that they might triumph in spite of Grant rather than because of him. Asked whether it was true that players call their manager Average Grant, Ballack said: �I�ve read that.� 



By refusing to give Grant proper praise, Ballack tells us much about his own colossal self-regard. By spurning the opportunity to rebut the manager�s spiteful nickname, he tells us everything about the thanklessness of Grant�s position. 
 



Ballack�s story tells us something else as well. It illustrates why the abrasive Mourinho had to go and why the emollient Grant is the perfect replacement. 



Ballack was Germany�s poster boy in the 2006 World Cup. His face was everywhere you looked. He strutted through the competition and was included in FIFA�s team of the tournament. But at Chelsea the following season he was just one of five national team captains. The others were Claudio Pizarro (Peru), Didier Drogba (Ivory Coast), Andriy Shevchenko (Ukraine) and John Terry (England). 



Normally top of the bill, Ballack found himself just part of the cast. And among all those vast egos, the biggest belonged to Mourinho. 



It is not true, despite the common perception, that Mourinho did not want to sign Ballack. Nor is it true that Mourinho resented adapting his formation to accommodate Ballack. Mourinho often praised Ballack�s contributions while the rest of us were expecting a lot more for his �120,000 a week. 



But Mourinho reacted with fury when he thought that Ballack had acted imperiously by going to Germany when he was injured. Only one person was allowed to be imperious at Mourinho�s club. 



Ballack, perturbed when the injury did not heal, returned home for a second medical opinion. 



German doctors found a fragment of bone floating about, and operated. 



It is ridiculous to imagine that Ballack would deliberately rule himself out of a Champions League semi-final and the FA Cup final, but Mourinho was often ridiculous. He thought Ballack wanted to make sure he was fit for Germany�s summer fixtures. 



On the pre-season trip to the USA, Mourinho told journalists that Ballack would be his fourth choice for two central midfield places this season. 



Then, as Ballack�s ankle took longer than expected to recover, Mourinho speculated that he was malingering. And when Mourinho informed UEFA of his squad for the Champions League group matches, he omitted Ballack and named Steve Sidwell instead. 



Mourinho informed favourite journalists he desired one of three responses � Ballack would work harder to get fit and prove that he deserved a place in the team, throw a strop and demand a move or, the third option, sulk. 



If Ballack sulked, then Mourinho thought he would persuade Roman Abramovich to sell him. But Mourinho�s mishandling of Ballack was one of the reasons Abramovich became exasperated. The owner�s vexation with his bothersome manager was compounded by a lacklustre start to this season. 



Nobody admires Mourinho quite as much as he adores himself but he still has plenty of acolytes, so it is worth reminding them that Chelsea took 11 points from their first six league games, a paltry return that could well cost them the title. 



Then they just about managed a draw at home against Norway�s Rosenborg in Europe, and were booed off the pitch. Mourinho was sacked two days later and Grant given the job. It appeared to be like asking a street busker to replace a great maestro. 



But Grant should not be undervalued because he is understated. 



Unlike Mourinho, he puts players and the team before himself. So he has managed to get all the voices to sing from the same hymn-sheet and to orchestrate what could be Chelsea�s greatest triumph. 



Ballack, of all people, should give him his due. So should all of us. 

