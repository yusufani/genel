AB�ye son tren Bulgaristan�dan kalk�yor 
KOM�UMUZ Bulgaristan�la ilgili yaz�lar�m�z�, ekonomisi ve T�rk giri�imcilerin ne yapt�klar�yla bitirelim:

Bulgaristan�da en b�y�k T�rk yat�r�m�n� �i�e Cam ger�ekle�tiriyor. 

Eski Cuma�da (T�rgovi�te) 160 milyon dolarla, t�m�yle T�rk sermayesiyle ger�ekle�tirilen (arsa ve �zel tren hatt�n� Bulgaristan yapt�) ve yap�m�n� �Sistem Yap��n�n �stlendi�i yat�r�m�n ger�ekle�tirilmesi sonucu �n�m�zdeki ay d�z cam �retimine ba�lanacak. Z�ccaciye �retimine de �n�m�zdeki y�l ge�ilecek. Ba�bakan Simeon Saksoburgotski ile �i�e Cam��n ve Hamzabeyli s�n�r kap�lar�n�n temel atma t�reninde bulunan Tayyip Erdo�an��n, siyasi tablonun de�i�mesiyle bu tesisin a��l�� t�reninde kar��s�nda 36 ya��ndaki Sosyalist Parti Genel Ba�kan� Sitani�ev�i bulmas� muhtemel g�r�n�yor.

Do�u� Holding (Ferit �ahenk) ve Eko �n�aat (Cengiz K�ksal) konsorsiyumunun �stlendi�i 39 km�lik Burgas-Karnobat (Sofya yolu) otoyolu ve 14 km�lik yan yollar�n yap�m� bitiyor.

56 milyon Euro�ya mal olan otoyolun t�m� y�l sonuna kadar hizmete girecek. (Bulgar Bay�nd�rl�k Bakan��n�n, yolun a��l���n� siyasi propaganda olarak de�erlendirilmemesi i�in se�im sonras�na b�rakmas�, bizim siyaset�ilerimizin kula��n� ��nlatm�� olmal�!) Do�u�-Eko�nun yeni otoyol projelerine ilgi g�sterdikleri belirtiliyor.

H�H lideri Ahmet Do�an��n giri�imiyle davet edilen d�nyan�n en b�y�k in�aat ve baraj yap�m firmalar�ndan Avusturyal� ALP�NE projeyi y�r�t�yor. 220 milyon Euro�luk yat�r�m� kendi kaynaklar�ndan kar��l�yor. Bulgaristan, AB�ye girerken, Erdo�an h�k�metinin bu yat�r�mla ilgili sorunlar� ��zmesi, iki �lkenin gelecekteki ili�kileri a��s�ndan �ok �nemli say�l�yor.

Ko� Grubu�nun, Bulgar Telekom�u (BTK) almak i�in ihaleye girdi�i ve kaybetti�i malum... �haleyi alan Avusturya k�kenli V�VA Ventures firmas�n�n patronlar�n�n arkas�nda Bulgar h�k�metindeki baz� bakanlar�n oldu�u s�yleniyor. T�rkiye, �ucuz� ayak oyunlar�yla ihale d��� b�rak�lm��; ancak �diplomatik� giri�imlerde de T�rkiye�nin �ihmalk�r� davrand��� bir ger�ek. Ger�ekten yaz�k olmu�... Daha da ileri gidilirse �Bulgar pazar�� i�in ABD ile Rusya aras�nda gizli bir sava��n h�k�m s�rd��� ayr�ca de�erlendirilmeli... BTK�n�n, i�inden ��kan GSM operat�r� M-Tel�in lisans hakk�n�n 1.2 milyar dolara bir Avusturya firmas�na satt���n� k���k bir not olarak aktaral�m.

�LG�N� YATIRIMLAR

Bulgaristan�da 14 akaryak�t istasyonu kurmu� olan Ko�-Opet ortakl���, yeni yat�r�mlarla bu say�y� 32�ye ��karmak istiyor. Ko�, Sofya�daki Ramstore ma�azalar�n�n say�s�n� ��e ��kard�ktan sonra �imdi de Makedonya�ya y�nelmi�.

�umnu�da, Fikret Kuzucu�nun Alcomet�inin �zelle�tirmeden ald��� al�minyum fabrikas�n�n bug�n 10 milyon dolara yak�n ihracat yapt��� bildiriliyor. I��klar Holding k���t; FAF Metal ve Maser tekstil, onlarca orta �l�ekli k���k sermaye sahibi parke, kereste, y�k paleti, tekstil ve konfeksiyon sekt�rlerinde �retim yap�yorlar. TAV��n orta�� Akfen, Sofya�da modern bir i� merkezine imza atm��; Alt�nba� Kuyumculuk�un da Sofya�da ��k bir ma�azas� bulunuyor. Bankac�l�k sekt�r�nde ise Ziraat Bankas� ve D-Commerce bulunuyor.

��NEADA�NIN D�B�NDE

1990�l� y�llar�n ba��nda Bulgaristan�a gelen ilk T�rk i�adamlar�ndan olan Zeki Bayram��n, Grup ve Etap adl� iki otob�s firmas� bulunuyor. 60 otob�sl�k filosuyla pazarda s�z sahibi. Ayr�ca, Sofya Belediyesi ile ortakla�a olarak 10 milyon Euro�ya yeni yap�lan otogar da, Balkanlar��n en b�y�k ve modern otogar� say�l�yor. Bayram, Burgas��n Kiten adl� sayfiye kentinde eski bir dinlenme evini alarak �Dodo Beach� ad�yla 5 y�ld�zl� otele d�n��t�rm�� ve az ilerisinde 2 km�lik sahilde 3 bin kapasiteli Cote d�azur benzeri bir plaj yaratm��.

Eski milletvekili Yal��n Ko�ak, ge�en y�l, �Avrupa e�itim alan��na giren Bulgaristan��n Varna Kenti�nde, ilk �zel T�rk �niversitesi �Balkan Universite�yi a�m��, ge�en y�l 170 ��renciyle e�itime ba�lam��... Ko�ak, ��ngilizce dilinde i�letme, bilgisayar, elektrik-elektronik gibi b�l�mleri bulunan �niversite, Varna Teknik �niversitesi�yle i�birli�i yap�yor. Biz AB�ye haz�r ��renciler yeti�tirmeyi hedefliyoruz� diyor.

Ge� de olsa yedi ay �nce kurulan T�rk-Bulgar Sanayi ve Ticaret Odas��na bug�ne kadar 50 T�rk ve Bulgar firmas� �ye olmu�... Sofya Ticaret ve Ekonomi M��avirlikleri�nin, May�s 2007�de AB�ye girecek Bulgaristan�da T�rk yat�r�mlar� i�in �ok dikkatli �al��malar yapmas� gerekiyor. �ki �lke aras�ndaki ticaret hacminin 1.8 milyar dolar�n �zerine ��kmas� i�in her iki taraf�n da ticaretin �n�ndeki engelleri kald�rmas� ve �zendirici tedbirler almas� gerekiyor. 

��adamlar�m�za uyar�: AB�ye son tren Bulgaristan�dan kalk�yor.

Mahcup oluyoruz

YAPIMI Ceylan Holding�e �stlendirilen ve g�rkemli a��l�� t�renine ra�men bug�ne kadar bir �ivi bile �ak�lmam�� olan Arda Nehri �zerindeki �Gorna-Arda� hidroelektrik santral� ve Pilovdiv-Kap�kule aras�ndaki 150 km�lik otoyol projesi, 7 y�ld�r bekliyor... Mesut Y�lmaz��n ve Kostov�un ba�bakanl�klar� d�neminde imzalanan anla�ma, Ceylan��n T�rkiye�de kar��la�t��� sorunlar ve Bulgaristan��n devlet garantisi vermek istememesi, kar�� taraf�n da yabanc� kaynak bulamamas� gibi sorunlar nedeniyle 400 milyon dolarl�k proje bug�n at�l olarak duruyor. Anla�maya g�re, T�rkiye bu yat�r�mla Bulgaristan�dan elektrik enerjisini alacakt�. Bulgaristan��n, m�teahhit firman�n de�i�mesini istedi�i, ancak Ceylanlar��n da �hukuki� g�vencelerini elinde koz olarak tuttu�u belirtiliyor. Ceylan Holding bu yat�r�m� ger�ekle�tiremezken, Vica Nehri �zerinde iki y�l �nce �Sankov-Kamak� adl� ikinci bir baraj�n yap�m�na ise �oktan ba�lam��...

K�y evleri ve tarlalar kap���l�yor

BULGAR�STAN�da, 354 km k�y� �eridine, son d�rt y�lda 2 milyar dolar tutar�nda 2000�e yak�n konaklama tesisi yap�lm��. Ayn� s�rede yarat�lan 160 bin ek yata��n 65 bininin sadece Slan�hev Br�yag�ta (Sunny Beach) yap�ld���n� g�rmek �a��rt�c� say�l�yor.

Bu b�lge 15 y�l �nce sazl�kt�. Bir b�l�m� AB fonlar�ndan turizm ama�l� kullan�lan kredilerle 9 km uzunlu�undaki sahilde, yo�un bir imar hareketi g�zleniyor.

Rant i�in mi? O da var; AB fonlar�n� kullanmak da var. Ama �nemlisi, Bat�l� turist Antalya�da sessiz bir ortamda g�ne� ve denizden yararlanmak istiyorsa, buraya gelen �ngilizler de kalabal�k ortamda g�venli bir �ekilde 24 saat �e�lenmeyi� istiyor. 

Didim�de, 300 sterline bir hafta tatil yap�p bol bol ucuz bira i�en �ngilizlerin bir �st s�n�f�, buraya ayn� s�re i�in 700 sterlin �d�yorlarm��. Ne ilgin�tir ki, Bulgaristan�dan tatil yapmak i�in �lke d���na ��kanlar en �ok T�rkiye�ye geliyorlar. Sudi �zkan��n Sofya ve Filibe�de otel ve casinolar� d���nda elinde kom�nizm d�neminin �nl� �Corecom� ma�azalar� da bulunuyor. Antalya�daki Club Otel Sera�n�n sahibi Ya�ar S�nmez �casino� yat�r�mlar�n� Bulgaristan�da b�y�t�yor. Avrupal�lar, Bulgaristan��n Istranca-Rodop ve Balkanlar�daki eski k�y evlerini �ya�am ve dinlence� ama�l�; verimli tar�m alanlar�n� da eko-tar�m yat�r�mlar� i�in kap���yor. Termal sular�n�n ��kt��� sa�l�k ve kayak merkezleri ile av alanlar� da yabanc�lar�n g�zbebe�i say�l�yor. Biliniz ki, T�rk yat�r�mc�lar h�l� uyuyor. 
