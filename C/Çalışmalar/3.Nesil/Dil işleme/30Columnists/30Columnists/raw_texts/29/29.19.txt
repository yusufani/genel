Every year, myths about the flu vaccine spread as widely as the flu itself.

Most people seem to know that the flu shot, which uses killed viruses, cannot cause symptoms. But its newer counterpart, the nasal spray FluMist, is slightly different. It uses live but weakened viruses, which can still replicate for as long as three weeks.

But that alone is not enough to cause sickness or result in passing the virus to others. In a report published in January, researchers at the Mayo Clinic noted that the amount of virus shed by people vaccinated with the spray is below what is needed to infect an adult. Children are slightly more susceptible. (One researcher was listed as a consultant for MedImmune, which makes FluMist.) 

But in several studies of transmission rates, there was only one case in which a vaccinated child passed on the virus. It occurred in a day care center with 200 children, most 3 or younger. The child who contracted the virus from someone else showed no signs or symptoms of flu, apparently because the virus remained weakened and was capable of only limited replication. Over all, studies suggest, the odds of transmitting the virus after receiving the nasal spray are about 2.5 percent. 

