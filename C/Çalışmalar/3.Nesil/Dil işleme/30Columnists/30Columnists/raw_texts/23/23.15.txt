In a previous column we explored fun strength training games for young children. As promised, we�ll now address exercises that cater to the needs of older kids, specifically pre-teens. The kids in this tricky category require exercises that aren�t too juvenile but still contain an underlying element of fun. Before we get started, you may want to refer to previous columns on the subject. Click here to read about the safety and benefits of youth strength training. Click here to learn how to teach proper form.

Wall Sit

Muscles Trained: Quads
Exercise: Stand against a sturdy wall. Slide your feet about a foot away from the wall and sink down into a sitting position. From a side view, it should look like you�re sitting in an invisible chair. There is a straight horizontal line from your hips to your knees, and a vertical line from your knees to your heels. Hold the position for 30 seconds. Stand up and stretch each quad by bending at the knee and holding your foot from behind. Repeat the exercise for 3 � 5 sets.
Element of Fun: Since it�s a natural reaction to grab onto the knees to lessen the load on the quads, tell the kids �No hands allowed!� Instruct them to occupy their hands by playing an imaginary Game Boy, talking on their imaginary cell, doing the Hand Jive, etc. while performing the wall sit.
Curls 

Muscles Trained: Biceps
Exercise: Stand with feet shoulder width apart, knees slightly bent. Using small hand weights, elastic tubing, or even canned food, raise your hands towards your shoulders, bending at the elbows. Pay special attention the wiggles. The back, shoulders, and knees should remain steady while the only movement happens at the elbow joint. Do three sets of 10 � 15 reps.
Element of Fun: Just using a prop, especially the tubing, is fun for kids.
The Plank

Muscles Trained: Core
Exercise: Lie face down on the floor. Bend at the elbows, and place your forearms at your sides, palms down. Curl your toes under and lift your entire body about one inch off the floor so that your toes and forearms are supporting your weight. Keep your body aligned, straight as a plank. You can use the cue, �No stinkbugs!� to remind kids to keep their rear from rising up. Hold this position for 20 seconds. Rest briefly and repeat for 3 � 5 sets.
Element of Fun: Kids love the �gross-out.� Have them imagine the floor below them is covered in something really gross, like manure or snail guts. To avoid landing face first in the muck, challenge them to hold the plank for the full 30 seconds.
Squats

Muscles Trained: Quads and Glutes
Exercise: Stand with feet slightly wider than shoulder length, toes facing forward. Extend your arms out in front of you. Bend your knees like you�re sitting in an invisible chair. Your hips should not go any lower than your knees. Return to standing position. Do three sets of 10 � 15 reps.
Element of Fun: To encourage proper form, have the kids imagine their rear end is on fire and there is a bucket of water behind them. As they squat, have them imagine they�re trying to put out the fire. In addition to making them smile, it will prompt them to stick out their rear as they squat rather than tuck it under.
Superman/Wonder Woman

Muscles Trained: Erector Spinae (lower back muscles), Glutes
Exercise: Lie face down on the floor, arms extended above your head. Engage the gluteal muscles to raise your legs a few inches off the floor. At the same time, raise your arms and upper torso off the floor. Hold for 10 seconds. Return to rest position. Repeat 3 to 5 times.
Element of Fun: Flying, of course! During the rest phase, you can instruct them to count backwards from ten. As they hit the pose, they can yell out �Blast off!� Another variation is to say �It�s a bird, it�s a plane, no it�s�� during the rest position. Then yell out �Superman!� during the exercise.

A final word about strength training: Breathe! Kids have a tendency to hold their breath during challenging strength training exercises. They�re so focused on the sensation of their working muscles that they simply forget to breathe. During an exercise with repetitions, like bicep curls, cue them to exhale on the work phase of the exercise. Encourage steady, easy breathing during static exercises like the plank.

