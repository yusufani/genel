Bu bir reklamd�r 
��YLE geyikler vard�r.

���nsan, �mr� boyunca yiyece�i ekme�i birarada g�rse...��

Ya da i�ece�i suyu, y�kayaca�� bula����, �unu, bunu... 

Ge�ti�imiz g�nlerde ba��ma geldi. Be� y�ld�r yazm�� oldu�um yaz�lar�n tamam�n� bir arada g�rd�m de hakikaten korktum.

Gazetedeki elim aya��m, g�z�m, h�z�r acilim, sevgili Cem'im ve ar�ivdeki arkada�lar�m CD'ye aktard�lar, sonra da A4'lere d�k�l�p �n�me geldi yaz�lar. E�er be� sene �nce ayn� miktar bo� k���d� g�sterip ��Bak, sen bunlar�n hepsini dolduracaks�n�� deselerdi can�ma kast var diye savc�l��a ko�ard�m.

Yaz�lar� g�rmekle kalmad�m, tek tek g�zden ge�irdim, g�ncelli�ini koruyanlar� se�tim, tashihlerini yapt�m.

Yok, �ik�yet etmiyorum. Son zamanlarda i�ten bunald���mda uygulamaya koydu�um bir y�ntemim var. Size de tavsiye ederim.

��yle:

Size �ok zor gelen, hayatta yapamayaca��n�z� d���nd���n�z bir i�i kendinize �rnek alacaks�n�z. Ben cerrahl��� ald�m.

Ne zaman i�imden �ik�yet edecek noktaya gelsem, ��Mesela �imdi bir beyin ameliyat� yap�yor olsayd�m daha m� iyiydi?�� diyorum ve o dakika s�k�nt�mdan eser kalm�yor.

* * *

Neyse uzatmayay�m... Eski yaz�larla ha��r ne�ir olmam�n nedenini anlam��s�n�zd�r mutlaka ama ben yine de s�yleyeyim.

Bir kitap olu�turmak.

Pardon iki kitap. M��-Mu�'lar� ayr� bir kitapta toplad�k zira. K���k, cebinize s��acak gibi. ��Hediyelik e�ya�� tad�nda. Ad� da ��YenMi� YutulMu� S�zler.��

Esas kitab�n ad� ��A��z Tad�yla Sevi�emedik��.

��inde esas k�zla esas erke�in her t�rl� durumu mevcut. A�k, ihtiras, araya giren k�t� kad�n (!) kin, nefret, aldatma, neler oluyor hayatta...

* * *

Biliyorum, �o�u yazar, ele�tirmen, hatta okur, k��e yaz�lar�ndan derlenmi� kitab� kitap yerine koymuyor. Ama ben �ok �nemsiyorum. ��nk� okuduklar�n�n �o�unu kesip saklayan okur grubundan�m. E, sararm�� gazete par�alar� olarak kalaca��na kitap olsun gelsin kitapl���ma otursun.

Sonra �u da var, yazd�klar�m�z�n sadece bir g�nl�k saltanat� olmas�n� i�ime sindiremiyorum. Onca d�hiyane fikir, ertesi g�n bir �ey yok. E, okura da bize de yaz�kt�r.

Epsilon Yay�nlar�'n�n sahibi �mer Yenici, ��Baz� M��-Mu�'lar�n�z atas�z� gibi�� dedi. Ben de kat�l�yorum �mer Bey'e.

G�lmeyin l�tfen!

Atalar�m�z s�ylenecek ne varsa s�ylediler, bitti mi yani?

Ne beni ne kendinizi k���mseyin. Bizim de edece�imiz iki �ift �nemli s�z vard�r elbet. Onlar da sizin benim gibi insanlard�, g�kten zembille s�rf s�z s�ylemeye inmediler.

Ya�amaksa ya�amak...

G�zlemse g�zlem...

�mbikten ge�irmekse, imbikten ge�irmek.

Neyimiz eksik?

Ha, bir tek atalar�m�z�n arkas�nda, elinde k���t kalemle gezen, art�k ��Atas�z� Kay�t Memuru�� mu desem, a��zlar�ndan ��kan� ��p diye kaydeden birileri varm��. �imdi bu yok i�te.

* * *

Bu kitab�n ortaya ��kmas�nda en b�y�k etken tabii ki okur oldu. �ok �srar etti okur. Yolumu kesti, telefon etti, faks �ekti, mail yollad�. Kald� m� atlad���m bir ileti�im yolu?

��Ka��rd���m�z yaz�lar�n�z var, okumaya doyamad�klar�m�z var... Aman Pakize Han�m kitap.��

Hadi bakal�m g�rece�iz �imdi. Say�lar yalan s�ylemez.

Ha, bir de �u var: H�rriyet okuru olmayan yurdum insan� benden mahrum mu kals�n?

Durum budur.

Yoksa ��Kar��l��� gazete taraf�ndan �denmi� etmi� yenmi� bitirilmi� yaz�lara birinin tekrar bir �cret �demeye kalkmas�n�n dayan�lmaz cazibesi��nin hi�bir �nemi yoktur(!)

* * *

Netice olarak...

�ki kitab�m ��kt�.

�kisinin sat�� rekoru k�rmak i�in aralar�nda yar��acaklar�na inan�yorum. Zira i�i garantiye ald�m. Her iki kitab�n da kapa��n� Latif Demirci yapt�. Ayr�ca M��-Mu�'lar�n aralar�nda karikat�rleri de var.

Okur ilk bak��ta kitaplar� Latif Demirci'nin zannedecek. Pakize Suda'y� kitab�n g�r�nmeyen bir yerine k���c�k yaz�n diye de tembih ettim.

Nas�l akl�m?

Ger�i durumu burada a��k ettim ama buna ra�men umutluyum.

Kitaplara Latif Demirci'nin kitab� s�s� verme numaras� tutmazsa diye ve ayn� zamanda,

��Er'inden sonra kalkan kar�dan,

Bilbordu olmayan yazardan hay�r gelmez!�� manisinden hareketle bilbordlar�mla donatt�k anayurdu d�rt yandan.

Bakal�m ne olacak?

* * *

Bu yaz�y� ay�playanlar olacakt�r muhakkak. Fakat be� senedir her t�rl� halimi yaz�p dururken �imdi kitap falan yokmu� gibi davranmam samimiyetsizlik olmaz m�yd�? 
