Asya Kalk�nma Bankas� toplant�lar�n�n ard�ndan 
�K� y�l �nce �stanbul�da yap�lmas� planlanan Asya Kalk�nma Bankas� (AKB) y�ll�k toplant�lar� �stanbul�daki bombalama olaylar� nedeniyle ertelenmi�ti. Ancak bu y�lki toplant�lar birka� hafta �nce �stanbul�da yap�labildi. 

T�rkiye�de D�nya Bankas� ya da Avrupa Yat�r�m Bankas� denince bize uzun vadeli kredi veren kurulu�lar anla��l�r. T�rkiye ekonomisinin d�� finansman ihtiyac�n�n bir k�sm� bu kurulu�lardan gelir. AKB farkl�d�r.

�HMAL ED�LEMEZ

AKB bizim i�in projelerimizin finansman�na y�nelik kredi al�nabilecek bir kaynak de�ildir. T�rkiye AKB�n�n katk� yapan ortaklar�ndan biridir. Ama, AKB�ndan be kamu ne de �zel sekt�r kredi kullanamaz. Ayn� olgu Avrupa Kalk�nma ve Yeniden �n�a Bankas� (EBRD) i�in de ge�erlidir. T�rkiye EBRD�ye ortakt�r, ama kredi kullanamaz.

AKB Asya �lkelerinin geli�mesi i�in kredi ve teknik yard�m veren bir bankad�r. D�nya Bankas��n�n Asya k�tas�nda faaliyet g�steren bir modelidir. Bizim gibi �lkelerin AKB�na ortak olmas�n�n nedeni ABK taraf�ndan finanse edilen Asya k�tas�ndaki projelere T�rk firmalar�n�n girebilmesidir.

EBRD de bizim i�in ayn� g�revi g�r�r. AKB ve EBRD kar��la�t�r�ld���nda, T�rk firmalar�n�n EBRD�yi g�reli olarak yo�un kulland���n� g�r�yoruz. Buna kar��l�k, AKB�n� EBRD kadar kulland���m�z s�ylenemez. Acaba Asya ile eski Sovyetler Birli�i Cumhuriyeti �lkeleriyle ilgilendi�imiz kadar ilgilenmiyor muyuz?

B�y�k bir olas�l�kla, konunun bir boyutu budur. Bir ba�ka boyutu ise AKB�n�n finanse etti�i projelerdeki rekabette T�rk firmalar�n�n geri kalmas� olabilir. �zerinde durulmas� konulardan biri mutlaka bu olmal�d�r. T�rk firmalar� rekabet�i olamad�k�a AKB gibi kurulu�lar�n finanse etti�i projelerde rol almas� zor olacakt�r. EBRD�yi daha �ok kullanmam�z�n bir nedeni EBRD�nin proje finansman�nda ortakl�k yoluyla katk� yapmas�d�r. Do�al olarak ortakl�k ile yap�mc�l�k farkl� konulard�r.

Y�ll�k toplant�lar s�ras�nda AKB Ba�kan� �srarla T�rk firmalar�n� Asya�ya davet etti. T�rkiye ekonomisinin geldi d�zeyi �vd�. Bu d�zeydeki T�rkiye�nin Asya k�tas�nda daha aktif olmas� gerekti�ini dile getirdi. �ok da hakl�yd�. T�rkiye, Asya k�tas�ndaki ekonomik potansiyeli ihmal edemeyecek kadar b�y�k bir ekonomidir.

Asya �ok b�y�k bir k�ta. K�ta i�inde gelir farkl�l�klar� �ok fazla. D�nyan�n en zengin �lkeleri de, en fakir �lkeleri de Asya�da bulunuyorlar. Dolay�s�yla, k�tan�n fakir �lkeleri zengin �lkelerin bir anlamda himayeleri alt�nda kalm��t�r. 

DI�A A�ILMAK

Rekabet g�reli olarak bu piyasalarda daha ac�mas�zd�r. ��nk�, Asya�n�n geli�mi� �lkeleriyle geli�mekte olan �lkeler piyasas�nda rekabet edilmesi s�z konusudur. Buna ek olarak, bizim gibi, AKB�na �ye olan d�nyan�n di�er geli�mi� �lkelerinin �irketleri de bu piyasalarda boy g�stermektedirler. K�sacas�, Asya pazar� ahbapl�k ili�kileriyle i� al�nacak bir pazar de�ildir. 

T�rkiye�de kamu kaynakl� projeler azal�rken, T�rkiye�deki belli bir bilgi birikimine eri�mi� �irketlerin eskiye g�re �ok daha fazla rekabet�i bir bi�imde d��a a��lmalar� ka��n�lmazd�r. B�yle �irketlerimizin say�s� artmaktad�r. Daha da artmal�d�r. 

D��a a��lamayanlar�n kamu sekt�r�nden proje beklemeleri �ok ger�ek�i de�ildir. Bu nedenle, AKB gibi kurulu�lar�n destekledi�i projelerde rekabet�i olabilmek i�in yeniden yap�lanmak hedeflerden biri olmal�d�r. 


