#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define PI 3.141592653589793238462643383
double rastgele(void);
void deney(int);
int main()
{
   int i,n;
   srand( time(NULL) );   
   for(i=10; i<=50; i++) // 10 dan 50 ye kadar i defa deney
   { deney(i); }
   for(i=1; i<=8; i++){   /* 10'un katlari */ 
   n = pow(10, i); 
      deney(n);
   }  
   printf("Gercek Deger = %f",PI);
 return 0;
}
	/* 0 - 1  aras� rastgele say� �retir */
double rastgele(){
	double r = (double) rand()/RAND_MAX;
	return r;
}
	/* deneylerin yap�l��� ve ekrana yazd�r�lmas� */
void deney(int n){
	int i=0, m=0;
	double x, y, pi;
	/* deneyleri baslat */
	clock_t before = clock();
	for(i=1; i<=n; i++){
		/* [-1, 1] aral���nda iki adet rastgele say� */
	    x = -1 + 2.0*rastgele();
	    y = -1 + 2.0*rastgele();
	    if( x*x + y*y < 1.0 ){
			m++; }
	}
	/* deney sonucu hesaplanan pi */
	pi = (double) 4.0*m/n;
	clock_t diff = clock() - before;
	printf("sure %5d sec %9d : %9d  |  pi = %10.7lf \n",diff, n, m, pi);
}	
