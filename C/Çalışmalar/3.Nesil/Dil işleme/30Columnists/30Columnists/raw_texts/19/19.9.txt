Barclays will kick off the banks' reporting season tomorrow with a set of results it hopes will set it apart from the rest of its troubled industry. 

The presentation is expected to follow the rather defensive tone set by the unprecedented open letter to shareholders from chairman Marcus Agius and chief executive John Varley last month. It goes a bit like this: you City sceptics may think we are living in a dream world, but we can assure you we have hardly any toxic assets and plenty of capital - and our �6bn profit proves it.

Certainly, Barclays' figures will be markedly better than those that follow them. Royal Bank of Scotland has warned that its losses in 2008 will be more than �28bn, a record in British corporate history. Lloyds Banking Group will struggle to convince shareholders that its government-brokered takeover of HBOS was a good idea, given that provisions and write-offs have wiped out the latter's profits. 

And with the economy slowing rapidly there could be even more losses on HBOS's sizable portfolio of commercial property and private equity loans to come. Mounting losses in HSBC's American business, and a slowdown in its Asian markets, is putting it under pressure and increasing the likelihood that it will either have to cut its dividend, join the list of banks that have raised new capital - or very possibly both. 

There is grudging acceptance that Barclays is not in such dire straits as RBS and Lloyds, which have both had to accept government funding and thus give up large chunks of their ownership to the taxpayer. But there is still a feeling that it is simply a matter of time before the cracks start to show.

Ian Gordon, banking analyst at Exane BNP Paribas, summed it up in a recent note: "The upside, if management's assurances are entirely borne out by fact, are considerable. But if, contrary to Barclays' very specific assurances, it is forced to seek fresh capital before 1 July, the dilution [of other investors] would be immense, with significant downside risk to even the current price."

Investors are unsure which side to take: while Barclays' shares bounced after the assurances in the open letter, they fell sharply again last week after rating agency Moody's downgraded its debt, saying: "Barclays' profitability and capitalisation will continue to be pressured by the ongoing need to implement further writedowns and build larger loan-loss reserves."

The downgrade simply brought Moody's into line with the other ratings agencies, but it put the spotlight back on the two issues that concern Barclays shareholders: whether it will have to raise more capital, and whether its BarCap investment banking subsidiary will continue to prosper, in contrast to rivals such as Merrill Lynch and Citigroup, where losses are still rising.

Barclays bulked up BarCap last year with the acquisition of Lehman Brothers' US business and will reiterate its confidence that it got a bargain by taking a gain on the deal in its 2008 accounts, claiming the business is worth some $2bn (�1.4bn) more than it paid. Varley's and Agius's open letter also boasted of the "strong and diversified income performance in Barclays Capital" and that its income generation was at record levels. 

But the City can see the pain being felt at Merrill and Citi and wonders whether Barcap's success can continue. And analysts will be poring over the finer detail of the expected breakdown of its exposure to toxic assets to determine for themselves whether it has been realistic in its writedowns. These will certainly rise sharply this year as the global recession intensifies: John-Paul Crutchley, banking analyst at UBS, predicts that provisions this year will rise to �6.3bn, compared with �3.8bn in 2008, and will fall only slightly to �5.7bn in 2010.

On the question of raising capital, Barclays will try hard to resist going back to the market until after June, to avoid triggering clawback provisions that would leave the Middle Eastern investors who backed last year's fundraisings with a majority stake in the bank. The City remains concerned that it will end up having to join Lloyds and RBS in taking funding from the government - and that the terms will be far more onerous than afforded to those two last year. 

Agius and Varley were bullish in their letter: "Client activity levels have been high. As a result, we have had a good start to 2009. In particular, the operating performance of Barclays Capital ... has been extremely strong." The onus is on them to demonstrate that they can build on that good start.
