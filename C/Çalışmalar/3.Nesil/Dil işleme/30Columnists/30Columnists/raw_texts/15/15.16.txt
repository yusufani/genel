Minus the tissue box, but with visible emotion, Alex Rodriguez admitted Monday to taking an illegal drug from 2001 through 2003. 

But was he upset about his image being sullied once again, or was he chastened by being caught cheating? It was hard to tell. 

At least Rodriguez told a version of what might have been an approximate slice of truth. In a way, that seemed like an improvement over how some other stars have discussed what will forever be known as The Steroid Years. But ultimately, what A-Rod did was not exactly the same thing as revealing the truth.

He said, �and to be quite honest, I don�t know exactly what substance I was guilty of using,� which sounds a trifle unbelievable, given the obsessive attention he and other wealthy professionals pay to their bodies. 

He talked in vague terms about the �culture� of the times and claimed that he might have tripped up over something he could have bought in a drug store.

But Primobolan, an anabolic steroid, which showed up in his 2003 test, is not sold legally in the United States. Rodriguez assured Peter Gammons of ESPN that it really didn�t matter who supplied him, or how. 

In more than half an hour of a one-on-one interview, Rodriguez seemed both overpackaged and underprepared, his body twitching with emotion. He did not have much to volunteer other than that he took something in 2001 and 2002 but he quit in spring training of 2003 after he injured his neck. 

His basic reason for trying stuff was that it was a �loosey-goosey� era. In other words, everybody else was doing it, and he was young and stupid. Now he seems to be banking on Mark Teixeira and C. C. Sabathia and his old buddy Derek Jeter to help win a World Series and get him into the Hall of Fame. 

Complaining that Sports Illustrated, which broke the story over the weekend, had been stalking him, and denying Jos� Canseco�s old claim of introducing Rodriguez to a Miami steroid dealer, Rodriguez once again made himself sound like a victim of circumstance. 

When Gammons alluded to a recent divorce, mercifully stopping short of referring to the dalliance with Madonna, Rodriguez passively said he had just gone through a rough 15 months. 

His limited confession tried to imitate the nonspecific apology Jason Giambi had used to calm down the mob. Seen in its entirety, however, the apology by Rodriguez sounded trite. But consider the source. 

In a way, Rodriguez did surpass Barry Bonds and Mark McGwire. With 553 home runs, Rodriguez has been the great hope of baseball to pass Bonds, who holds the career record with 762 homers. McGwire was openly using a steroid substitute in 1998, and was accused of using the real stuff. Bonds apparently was so jealous that he looked up the seedy Balco laboratory, an involvement that has led to Bonds� indictment by a San Francisco grand jury. McGwire�s face turned red and he seemed to shrink in front of a Congressional panel in 2005 as he refused to discuss whether he had used performance-enhancing drugs. 

The industry cannot penalize Rodriguez for the 2003 positive test because the players association and Major League Baseball managed to delay serious penalties until later. Rodriguez was apparently one of 104 players who tested positive, a result that should have remained confidential and been destroyed. 

In the same report on Saturday, SI.com said that Gene Orza, the chief operating officer of the players union, had notified Rodriguez in September 2004 that he was about to be tested. Orza denied it Monday. If anything like that happened, it would be a much more serious breach of confidence than anything a player did. 

This is never going to go away for Rodriguez, who heads into spring training with the title A-Fraud pasted across his broad back, courtesy of an unnecessary book by his former manager, Joe Torre. 

Yankee fans don�t particularly care for A-Rod because he has never won them a World Series, and in the Bronx what else is there? And before he ever joined the Yankees in 2004, Rodriguez was set apart for sniping at Jeter, who has never forgotten or forgiven. A-Rod brought it upon himself, just as he brought this scandal on himself.

Baseball will never be able to quantify what Bonds and McGwire and Rodriguez � or pitchers like Roger Clemens � owe to their use of illegal drugs. What�s the formula? An arbitrary 10 percent of home runs or strikeouts? They are great players, with or without cheating, but ultimately they have to pass the schlubby Greek chorus of baseball writers who vote for the Hall of Fame. 

Some people will say the Yankees should just cut him loose, but nobody is taking up his 10-year contract of $275 million, which runs nine more years. The fans will boo in New York and elsewhere, and baseball will continue to sell tickets. 

When the first quotes of his confession surfaced in midafternoon, it sounded as if Rodriguez really understood he had done something wrong. In the full interview, the more he talked, the more disassociated he sounded. He still doesn�t get it. 

