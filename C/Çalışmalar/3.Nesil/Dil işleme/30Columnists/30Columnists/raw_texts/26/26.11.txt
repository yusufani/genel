HARRY Redknapp probably thinks credit crunch is a breakfast cereal. Like some provincial 'Flash Harry' up in the smoke for the weekend, he has been splashing the cash around with cavalier lack of restraint. It seems he only has to hear a rumour that a mid-ranking journeyman might be up for sale, than he's on the blower, running a thumb over a wad of used millions.

It is hardly surprising that there is a lack of broad sympathy for the plight of Redknapp's Spurs side. They spent �30million in ten days acquiring Jermain Defoe and Wilson Palacios, while seeming to express an interest or lodge a bid in every otherADVERTISEMENTJanuary deal doing the rounds, from Craig Bellamy through Stewart Downing and Carlo Cudicini to (the latest) Carlton Cole. Every unsettled player at every club can usually be linked with interest from Spurs. 

You can attribute part of Redknapp's transfer ubiquity to the novelty factor. For all his reputation as a wheeler-dealer, Redknapp has never had a chequebook quite as capacious as the one with the cockerel crest, nor a chairman as compliant as Daniel Levy (or an owner with such deep pockets as the Bahamas-based tycoon Joe Lewis). With Spurs holed beneath the waterline by the Juande Ramos debacle, Redknapp quickly established a line of credit with some respectable results. With performances now tailing off, Redknapp has not been slow to lay the blame on his mish-mash of a squad, acquired by Ramos, and by Levy. You got us into this mess, is Redknapp's implied argument, now buy us out of it.

Redknapp will have deliberated before coming out with the criticism. The message he is sending out is that he wants to build a Tottenham squad to his own specifications. If players are not willing to conform to his blueprint, then he wants a complete overhaul of the roster. This cannot happen in a week. The fees for Defoe and Palacios were inflated by the customary January desperation tax. If it is true that Redknapp had a budget of �30million, then he has blown it, and must sell in order to spend more. 

Levy appointed Redknapp in haste, and now has the leisure to repent. One look at Redknapp's bulldog jowls should have told him that you can't teach an old dog new tricks. Like any elderly hound, he clings to familiarity. There are players he trusts, and types he is unwilling to countenance. 

Consider the case of two forwards. Jermain Defoe has always been a decent goalscorer who falls a little short of true international standards. Redknapp has known and valued him since he signed him as a 16-year-old for West Ham, and made him his transfer target at Portsmouth, and again this month. The 19-year-old Mexican prodigy Giovani dos Santos joined Spurs in the summer for an initial �5million. Last season he made 28 appearances for Barcelona, earning excited notices in the Spanish press, and rounding off his career at the club with a hat-trick against Real Murcia.

With Redknapp loudly bemoaning his attacking options, you might have thought Dos Santos would have been given a few opportunities. Instead, Redknapp seems to view the languid Mexican as a possible bargaining chip to finance a bid for a more familiar kind of professional. 

Meanwhile, matches keep getting in the way of business. This afternoon's FA Cup tie against Manchester United comes along at an awkward moment. 

Spurs are the obstacle in both domestic cups to a United quadruple this season, although Redknapp is singularly unenthusiastic about today's game.

He has promised to send a "very, very weakened team" to Old Trafford. Dos Santos might get his chance at last, although it would be a tough ask to find a team weaker than the line-up at Turf Moor on Wednesday that stumbled into the Carling Cup final late in extra-time. 

Given that the FA Cup is the only significant prize Redknapp has ever won in football, it might not be smart to dismiss it so easily. It could, though, be part of his masterplan to expose the limitations of his squad as embarrassingly as possible. A thorough humiliation this afternoon might persuade Levy to find a few more million for a shopping spree next week. 

In the naked Premier League there are eight million possible transfer deals. Redknapp will hardly be content with only two. 
