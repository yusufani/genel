Dying from a broken heart might not be a sufficiently scientific explanation to convince everyone - but it does exist and now it can be measured. 


Losing a wife puts the widower at a sixfold higher risk of death, while a widow's chances of dying are doubled, says research published yesterday. 

The risk peaks for either surviving partner in the first year after bereavement and then declines, according to the Cass Business School in London. 
Experts believe the stress caused by loss can depress the immune system of a surviving spouse, which may make existing medical conditions worse. 
The emotional strain also causes some bereaved partners to kill themselves, while others neglect their health and diet.


Dr Jaap Spreeuw, senior lecturer in actuarial science at Cass, said his research confirms the impact of "broken heart syndrome". 

He added: "We all know that the death of a loved one will have massive impact on the life of the husband or wife left behind, but this shows it will have direct impact on their mortality. 

"It statistically proves that people can die of a broken heart during the earliest stages of bereavement. 

"The effect is stronger for older people who have been married longer. The good news is that after the first years of mourning, the chance of dying goes down. 

"Although it remains higher than for couples where neither partner has died, it does lessen over time." 

Dr Spreeuw's research was based on an analysis of 11,454 life annuity policies held by a Canadian insurer. 

In the study, 195 couples died at the same time. 
In 1,048 cases the man died and the wife survived and in 255 couples the woman died and the husband survived. 

The highest death rate was among those who had lost a partner in the preceding 12 months, and the highest risk of dying was for men. 

"This seems to suggest that the broken heart syndrome has a stronger impact on men than on women," said Dr Spreeuw, whose work was sponsored by the Actuarial Profession to help insurance companies price life assurance and pension policies. 

Andrew Papadopoulos, a consultant psychologist who works at Birmingham and Solihull Mental Health Trust, said studies suggest suicide pushes up the death rate among the newly bereaved. 

He added: "The loss of a muchloved marriage partner is an enormous emotional event and, for older men, it may bring huge role reversal changes. They may not have had to cook or do laundry regularly before. 

"The stress on the immune system can have a deleterious effect on health, but research also shows that those who feel their lives are meaningless and hopeless after the death of a marital partner are particularly at risk. Anyone in this situation should speak to their GP." 
