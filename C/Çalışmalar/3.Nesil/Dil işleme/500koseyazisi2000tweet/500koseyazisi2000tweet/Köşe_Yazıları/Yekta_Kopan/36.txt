﻿Peki şimdi ne olacak?

Bugünden yarına değişim hemen olmayacak ama çok güzel şeyler olacak. Nasıl mı? Açıp CHP ve HDP'nin seçim bildirgesine bakın.
Şimdi bi’ şeyler olacak...
Bi’ şeyler, bi’ şeyler...
Örneğin AKM’nin nedensiz tutukluluk hali bitecek. AKM açılacak.
Emek Sineması sürecinde yaşananlar sorgulanacak.
TÜSAK artık konuşulmaz olacak.
Devlete bağlı sanat kurumları yeniden yapılandırılacak.
Devlete bağlı sanat kurumlarında yöneticilik yapan kimi isimler, bu konumlarını korumak için el pençe divan durmak zorunda olmadıklarını hatırlayacak.
Atamalarda “bizden olsun da yeterli olup olmaması dert değil” dönemi bitecek.
Türkiye Sanat Meclisi Kültür Bakanlığı bünyesinde oluşturulacak.
Sanatın özgürleşmesinin önündeki tüm yasal engeller kalkacak.
Keyfi yasaklara ve sansüre son vermek amacıyla çağdaş bir Sanat Yasası oluşturulacak.
Kültür ve sanat alanında Devlet Sanatçılığı kurumu kaldırılacak.
Tiyatroda, sinemada ve diğer alanlarda devlet desteği almanın birinci şartı yandaşlık olmayacak.
Devlet destekleri kurullarıyla, komisyonlarıyla şeffaf olacak.
Sponsorlukların üstündeki baskılar kalkacak, vergiler düzenlenecek. Hükümeti mutlu edecek etkinliklere sponsor olma hikayesi sonlanacak.
Sanatçıların sendikal örgütlenmesi teşvik edilecek.
Kültür ve sanat alanındaki vergileri asgari düzeye çekilecek.
Milli Piyango gelirlerinin bir bölümü, kültür-sanat alanını desteklemek için tahsis edilecek.
Yerel yönetimlerin kültür-sanat alanına ayırdıkları kaynakların, denetlenebilir ve şeffaf bir mekanizma aracılığıyla, kâr amacı gütmeyen kültür- sanat oluşumlarına, sanatçılara veya sanatçı inisiyatiflerine kullandırılması sağlanacak.?
Başbakanlık Tanıtma Fonu'nun bürokratik durumu yeniden yapılandırılacak ve bağımsız uzmanlar karar sürecine dahil edilecek. ?
Ülkemizdeki yaşayan ve kaybolan dillerdeki sanat eserlerinin üretilmesi ve sergilenmesine destek verilecek.
Müzelerde evrimle ilgili konular sansürlenmeyecek. Antik çağdan kalma bir heykelin “edep yerleri görünüyor” diye üstü örtülmeyecek.
Fazıl Say’ın “Nazım Oratoryosu” eserinin keyfî bir şekilde program dışı bırakılması gibi “bir telefonla hallederiz” durumları tarih olacak.
Kraldan çok kralcı valilikler, belediyeler “Ben olsam yasaklamam, sansürlemem, ama yukarıdan gelen baskıyı biliyorsunuz,” yalanına sığınamayacak. Festivallerin-konserlerin kaderi keyfî bir şekilde değiştirilemeyecek.
Sansür baskısı bitecek.
Medya kuruluşları sahiplerinin başka sektörlerde faaliyet göstermesine izin verilmeyecek.
Siyaset ve ticaret ilişkileri nedeniyle toplumun doğru haber alma özgürlüğünün kısıtlanmasının ve medya aracılığıyla vatandaşın istismar edilmesinin önüne geçilecek.
Bütün o baskılara direnerek, ellerinden geldiğince gazetecilik yapmaya çalışanlar, artık “yukarıdan gelen telefon” baskısından ve “dava açılma tehdidinden” uzakta, daha özgürce yazacak.
O baskılara direnemeyip, “Ne yaparsın, ben de istemiyorum burada çalışmak ama ekmek parası,” diyen “arada kalmış gazeteciler/televizyoncular” vicdanlarıyla konuşmak için bir nefes alacak.
Sosyal medya hesaplarındaki paylaşımları bile kontrol altında olan medya çalışanları, “Yazdıkların canımızı sıkıyor, kovuldun,” kabusu yaşamayacaklar.
RTÜK, demokratik ve özgürlükçü bir anlayışla yeniden yapılandırılacak, yasakçı, tutucu, ahlakçı bir konumdan çıkarılacak.
RTÜK’teki değişim, televizyon dizilerinin üstündeki baskıyı ortadan kaldıracak. Hükümetin aile politikalarına doğrudan hizmet eden diziler yığını azalacak. Hiç değilse birbirini seven insanlar ekranda rahatça öpüşebilecek.
Özellikle sinema ve televizyon sektöründe rastlanan sömürü ve insanlık dışı çalışma koşulları gözlenecek, kültür ve sanat eserlerinin üretim süreçleri insanileştirilecek. Dizi süreleri normalleşecek, televizyon yayıncılığının dinamikleri değişecek.
“Şimdi falanca ismi ekrana çıkarırsak, hükümet aleyhinde konuşur, başımız belaya girer,” tedirginliği bitince, yıllardır ekranlarda göremediğimiz isimleri hatırlayacağız. Üstelik böylece ikiyüzlü televizyoncuları da tanımış olacağız.
İnternete yönelik her türlü sansür uygulamasına son verilecek. Keyfi internet yasakları ve sosyal medyadaki düşünce özgürlüğü önündeki engeller kaldırılacak.
Açık kaynak kodlu özgür yazılım teşvik edilecek.
İletişim özgürlüğü korunarak, bu özgürlüğü kısıtlamaya yönelik dış müdahaleler, sansür ve özdenetim gibi her türlü girişimlere karşı durulacak.
Diz çöküp, el öpüp hak yiyenler hesap verecek.
Bugünden yarına değil elbette. Ama olacak. Çünkü bu yazdığım maddelerin çoğu, CHP ve HDP’nin seçim bildirgelerinden alınma. Diğer maddeler de bu vaatlerin doğal sonucu. Nasıl bir hükümet süreci olursa olsun, bu vaatlerin sahipleri mecliste olacak.
Şimdi bi’ şeyler olacak.
Ama güzel olacak....