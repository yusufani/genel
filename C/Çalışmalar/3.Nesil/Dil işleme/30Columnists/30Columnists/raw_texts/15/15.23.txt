Some young children I know like the exotic name of Barack Obama but cannot possibly understand why older people are so abuzz about the historic aspect of Tuesday�s inauguration.

The children, white children, have grown up around African-American teachers, neighbors, friends and, for that matter, relatives of mixed ancestry. And, really, how does one explain the way things were 50 to 60 years ago?

�People did what?�

On the eve of Martin Luther King�s Birthday, it is important for children to know why people were crying that November night in Grant Park in Chicago, why Jesse cried, why Oprah cried.

My parents would be crying, too, if they were still alive. They were part of a pioneer discussion group in Queens in the late �40s, intentionally half black, half white � earnest bootstrappers, civil servants, teachers, who discussed novels, the Bible, politics. 

They were doing their middle-class postwar best to bring about a day when a striking young couple could be depicted in the recent issue of The New Yorker, photograph circa 1996, posing in their Chicago home, full of entitlement, our best and brightest, clearly on their way to somewhere.

Our favorite baseball team had something do to with the handsome young couple from Chicago. We rooted for the Brooklyn Dodgers, who in 1947 called up Jackie Robinson, the first black major leaguer of that century. I would have a hard time explaining to children the vile language that came out of opposing dugouts and from the stands, but Robinson endured it, and so did the black players who followed him. 

The other day, I was talking to Don Newcombe, a great pitcher in Brooklyn, who willed himself to sobriety four decades ago and still works for the Dodgers, alas in L.A. A great man. We were talking about something else, but Newk brought up the old days.

The young children who are entranced by Obama�s name should know what Newcombe and Robinson saw outside the ballpark in St. Louis in 1949 or 1950.

Thousands of Negroes, as they were called, were milling around the street, unable to pay their way into the game because the outfield pavilion, where blacks had been confined, was sold out. By some accounts, the stands had been legally integrated a few years earlier, but apparently, many of Robinson�s fans were not exactly welcome there. 

�Jackie looked out and saw a bunch of people in the street with radios on,� Newk said, recalling how Robinson summoned Burt Shotton, the manager, who said, �I never saw anything like this.� Newk said a Dodgers official informed the Cardinals that the Dodgers would not take the field until the fans were inside. 

�You made money for the Cardinals,� I said.

�We just wanted them to watch us play, not stand out in the streets with radios,� Newk said somberly.

Five years later, Newcombe came back after missing two pennant-winning seasons because he was drafted during the Korean War. Now he was in St. Louis, where the black Dodgers had always stayed at a black hotel, which had no air-conditioning. (In 1953, while Newk was in the service, Robinson stayed at the Chase Hotel, the oasis alongside Forest Park, with all the white players.) 

Now, in 1954, Newcombe said he told Robinson he was taking a cab to the Chase.

�When we got there, the manager took us into the dining room and wanted to know what we wanted and Jackie said: �You know why we�re here. Don just got back from the military for two years, and he wants to stay at your hotel, like any other man in America.� The manager said, �Gentlemen, the only thing I can think is that we don�t want you swimming in the swimming pool.� �

In one version, Robinson said, �Mister, I don�t even know how to swim.� Appeased, the manager gave them rooms. Away from the pool. From that day forth, all the black Dodgers had air-conditioning in St. Louis. 

That was more than half a century ago. Two players said to be uncomfortable with Robinson in 1947 were Dixie Walker of the Dodgers and Harry Walker of the Cardinals, brothers from Alabama.

Twenty years later, Manager Harry Walker started eight black or Latino players with the Pittsburgh Pirates, with only a white pitcher, whose turn it was. And Walker would become a hunting buddy of Bill White � first baseman, broadcaster, National League president � who has never suffered bigots or fools. Walker died in 1999, his decent life a testimony that people can evolve. 

I did not ask Newcombe what he thought of Obama. But a year ago, I asked Rachel Robinson, the widow of Jackie, and the prime force in the Jackie Robinson Foundation, how she felt about Obama�s candidacy. 

�George, that is a political question,� she said, putting her hand on my shoulder and giving me her beautiful, resolute smile. �And I�m not going to answer it.� But I think she did.

The Robinsons and Newcombe and Walker all went through turbulent times before a young couple in Chicago could pose for a portrait, clearly on their way to somewhere. On Tuesday, we can all marvel at this journey. 

