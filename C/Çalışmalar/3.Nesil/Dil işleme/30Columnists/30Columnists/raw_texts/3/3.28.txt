IT was breathtaking ... to hear politicians from David Miliband, the next Labour leader we are told, to the former Portuguese Prime Minister Jose Manuel Barroso, who was lucky enough to be appointed President of the EU Commission before he was run out of Lisbon, saying that we shouldn't consider the Treaty of Lisbon to be dead in the water, even though the decisive "NO" vote by the Irish people in last Thursday's referendum has left the power-grab by the EU elite at the bottom of
Jim Murphy, whom it's a fair bet you didn't know was the Minister for Europe, pontificated on the "need to respect Ireland's decision" and the "need for the House of Lords to ratify the Treaty of Lisbon", in spite of the Treaty's own requirement to hADVERTISEMENTave every member state endorse it before it becomes legal, and therefore before any of its provisions can be put into effect. 

Mr Murphy's high-handed pomposity was typical of the comments made by other EU ministers. The French foreign minister blatantly ignored the undemocratic nature of carrying on regardless as she, and the others, tried to punt the big lie ... that Irish people were the only EU citizens to reject the re-wrapped constitution voted down a couple of years ago by the French and the Dutch.

There are still eight countries that haven't decided on whether to endorse the Treaty. The UK Government has reneged on its promised referendum on the Lisbon Treaty; most people believe the reason is that Gordon Brown knows the UK would have voted as the Irish.

But once again, there's an arrogant assumption that the EU elite know better than we do about what's good for us, with Westminster ministers saying that the Irish have made a mess of things, so they'll just have to find a way of changing the opinion democratically expressed by the majority. Wouldn't it show more respect for the people of Europe for them to remember that the Irish are the only ones who've been able to express what opinion polls suggest is the majority view in the Czech Republic, Poland, Sweden, Netherlands and perhaps even Spain, and possibly France?

One of the reasons for Ireland's "NO" to the Lisbon Treaty was the proposed loss of a commissioner, nominated by the Republic's government. With the expansion of the EU to include the former Soviet Bloc states, smaller states were to lose their commissioners, resulting in the big two, Germany and France, having power centralised in their representatives' hands in Brussels. 

Should Scotland achieve membership as a sovereign nation, we'd be in the same boat as the Irish, even though Scotland would be a net contributor to EU finances and would be the only member state with major oil deposits.

The centralising instincts of the elite political class intent on building a federal union to rival the United States are more likely to fracture the unity of purpose that exists amongst Europe's peoples in specific policy areas like the environment, and organised, international crime. 

The Euro-nuts want an EU foreign minister. Why? To impose an entirely false common attitude and policy on 27 very different countries moulded by their differing experiences of war and peace. In our own case, together with Ireland, our traditional ties, trade and cultural, have been transatlantic, whereas Germany has always looked east, for example.

And why do we need to have a full-time President with the same status as democratically-elected Prime Ministers, instead of sharing the Presidency? The federalists know that if the EU becomes a legal entity and not an agreement amongst sovereign states as at present, it will have much more power than democratically-elected national parliaments.

Democrats in Ireland said "NO"... so should we. 

Thatch enough
A friend, one of my non-political pals, asked why there was so much interest in Margaret Thatcher when she was long gone from Downing Street. Why indeed, when the 30th anniversary of her world-shaking win doesn't come round till next year.

The timing of the TV programmes is explained by the cut-throat competition to get stories on screen first, but, of course, the reason for telly's interest is the social and industrial revolution she achieved through her own determination, clear focus and the backing of a small number of eggheads like Keith Joseph or salesmen capable of selling snow to Eskimos, like the Saatchi brothers.

She was original, and brave ... and she ruined the lives of thousands of people.

Kids are alright
I attended the Evening of Celebration at Craigmount School last week. Much is said about young people's bad behaviour and legislation is being introduced to deal with their abuse of alcohol, etc. 

But we should remember most young people are like the high achievers and kids who just try their best whom I had the pleasure of meeting.
