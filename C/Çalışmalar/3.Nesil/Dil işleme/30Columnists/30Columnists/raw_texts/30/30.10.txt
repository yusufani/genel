The numbers begin to grow numbing. Maybe our nerve-ends have lost the sensitivity to react to $1 trillion anymore. Even $1 trillion more of new national debt -- in just one year!

Sorry for that exclamation point. But going deeper in hock on such an astronomical scale is something about which we perhaps ought to throw cold water in our faces.

A billion dollars used to sound like a lot. A congressional wit once famously said, "A billion here, a billion there, soon it adds up to real money." How could Sen. Everett Dirksen have jested about trillions?

Adding three new zeroes on the end of incomprehensible dollar totals can't be as painless as all that. 

Americans seem bound toward a comeuppance in bad inflation. Even hyperinflation. Can the children and grandchildren possibly escape it?

But inflation is not today's crisis, mainstream wise men tell us. Deflation is. Falling prices: a decline in so many asset values -- houses, stock prices, commodities, art -- as to paralyze buying and selling.

So recession becomes depression. Millions more jobs are lost. Poverty and desperation spread. And politicians must do more and more to rescue capitalism. Throw another $1 trillion at it ... and another.

This is a nightmare scenario. A comforting bipartisan promise of sorts holds that in due course we should wake up and find ourselves in a sweat, but safe after all. 

Still, the fiscal year is young. It runs until Sept. 30, by which time federal budget deficit will more than double, the Congressional Budget Office reported last week.

No generation in American history has seen a number like this before. Not in World War II. Less than half the debt we're taking on was judged awful just a year ago. The $455 billion deficit of fiscal 2008 -- piffle.

How'd we get in this deep? Well, start with bailing out the over extended financial industry. Add the "rescue" of Fannie Mae and Freddie Mac, those two guarantors of crummy mortgages. And watch tax revenues shrink with the economy. Another $1.18 trillion on the national debt becomes a cinch.

The debt has more than doubled on President Bush's eight-year watch, from $5 trillion to $10.6 trillion. And at 8.6 percent of Gross Domestic Product, it's higher than any time since World War II, even as a proportion.

Next year's deficit -- already pegged at $703 billion at least -- doesn't even include President-elect Barack Obama's two-year economic stimulus package of $750 billion, mostly in public works spending and tax cuts. 

Two upbeat observations. Tax cuts should generate more revenue by quickening business activity and industry modernization. And new roads, bridges, sewer systems and other infrastructure will be a bonanza to special interest construction companies and unions. But they'll juice up jobs and tax revenues.

But taking the country trillions more into debt? Will we have the self-control to pay it off someday. Or go numbly into the mire of runaway inflation?

