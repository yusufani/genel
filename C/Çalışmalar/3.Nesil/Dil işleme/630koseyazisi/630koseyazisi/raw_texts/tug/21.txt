Berdu�lu�a �vg� 

 
 

Ameliyat olman�n en acayip taraf� ne? Y�kanam�yor olmak� Bandajlar�n sa�l��� a��s�ndan yasah� Benim gibi sa� dahil her g�n y�kanan bir su ku�u i�in zor bir durum. 

Ama dayan�lmaz de�il� �nsan bir s�re sonra al���yor. Hatta normal buymu� gibi gelmeye ba�l�yor. Ya da ��yle s�yleyeyim: Bir acayiplik oldu�u hissi g�nden g�ne kayboluyor. �nsan�n ki�isel temizlikle ilgili hassasiyeti ilk d�rt g�n en y�ksek seviyede. Sonra tuhaf bir �ekilde sana eskiden acayip pis gelen kokuna al��maya, onu sevmeye ve ona ba�lanmaya ba�l�yorsun� ��te BU diyorsun. Hakiki insan kokusu� (Patrik S�skind a�bimize h�rmetler..)

Sap�k�a m� buldunuz? ��yle s�yleyeyim: �Berdu�� olmaya do�ru giden yol san�ld��� kadar ta�l� de�il. Bir ay dayanmaya bak�yor. Sonra �mr�n�n sonuna kadar b�yle ya�ayabilece�ini d���nmeye ba�l�yorsun. (Her ���p evin�, her �hane berdu�un� temiz bir mazisi vard�r�) 

�yle rezil kepaze durumda de�ilim tabii.. En az�ndan siliniyorum, giysilerimi de�i�tiriyorum� Fakat zaman�nda Gobi ��l��ndeki acayip yolculu�um s�ras�nda 17 g�nl�k bir y�kanmama rekorum vard� ki b�rak silinmeyi, giysi de�i�tirmeyi y�z�m� bile y�kayamam��t�m. Di�imin �st�nde bir di� daha olu�mu� gibiydi� �stelik kendi kokuma ilaveten �zerime d�k�len bir termos dolusu kahve ve �eyrek bidon dolusu mazot y�z�nden hakikaten rezil bir hale gelmi�tim. 

Ne olmu�tu? Hi�. Hasta falan olunmuyor. Dalgan� ge�meye ba�l�yorsun. �A bak sa�lar�m ipek gibi oldu� �Blucinim katur kutur ediyor� �Esmer olmak demek b�yle bir �eymi�� gibi mavralar yap�yorsun� Portakal yedikten sonra ellerini �zerinde kuruluyorsun, (parf�m niyetine.. heh�) istedi�in yere rahat rahat oturuyorsun, yeterince yumu�aksa k�vr�l�p uyuyabiliyorsun ve daha bir dolu rezillik. (Fakat sa�lar cidden ipek gibi oluyor�)

Bu da bir l�ks asl�nda. Titizlik u�runa harcad���m�z zaman� d���n�rsek �y�kanmak, sa� kurutmak, di� f�r�alamak, �ama��r y�kamak, asmak, �t�lemek, yerine kald�rmak� ki bunlar sadece ev i�inde yapt�klar�m�z� Temiz bir yer bulup oturmak i�in dakikalarca y�r�mek, banklar�n �zerine selpaklar sermek, umumi tuvaletlerde tuvalet ka��d�ndan klozet kapaklar� yapmak, araba y�katmak, �amura basmamak i�in seke seke y�r�mek- en az iki bu�uk saat eder mi? Belki de daha fazla. Pis insanlara ��k��klamak ve sinirlenmek i�in harcad���m�z enerjiyi de kat�n buna.. Pehee� Tam bir zaman ve emek kayb�� 

�nsan neye tak�ld�ysa onun sonuna kadar gidebiliyor. Titizlik de biliyorsunuz gayya kuyusu gibi. Onun da sonu yok. Belediye otob�s�nde hi�bir yere tutunmamak i�in denge uzman� oldu�um g�nleri de bilirim. (Dizler hafif b�k�l�, ayaklar�n aras� en az 50 santim a��k, �of�r taraf�na bakacak �ekilde durmak� Ba�ar�, �of�r�n delilik oran�na g�re de�i�se de fena bir y�ntem de�il. Fakat virajlar ve ani frenler i�in daha karma��k bir pozisyon gerekiyor�)

Pisli�e �vg�ler d�z�yor gibiyim ama demek istedi�im hi�bir �eye fazla tak�lmamak gerekti�i. 

G�bek deli�imdeki d��me giderek daha komik bir hal al�yor, tek ona tak���m� Ne olacak bu, bilen var m�? 

 
