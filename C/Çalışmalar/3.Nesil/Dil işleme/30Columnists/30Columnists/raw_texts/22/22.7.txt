Harlequins' presence in the Heineken Cup's last eight this season would have impressed one famous military strategist. "I don't measure a man's success by how high he climbs but how high he bounces when he hits bottom," said General George Patton. Given that Quins had dropped into National League One when Dean Richards rode into town four years ago, Sunday's significant date with Leinster is almost worth a gallantry medal in itself.

No wonder the club's chief executive, Mark Evans, hailed Richards this week for the "brilliant" job he has done. Quins are not merely a couple of big wins away from a European final, they also lie second in the Guinness Premiership with two games to go. No club in the league has conceded fewer tries and only London Irish have scored more. As well as amassing 94 tries in all competitive games this season, they have beaten Stade Fran�ais at home and away, the latter result coming at a packed Stade de France. Mighty Quins indeed.

This renaissance has been achieved with a pack conspicuously light on household names. Mike Ross, Ceri Jones, George Robson, Chris Robshaw, Will Skinner � only the England No8 Nick Easter and the ex-Springbok hooker Gary Botha boast international profiles. Their progress, not least in an inspired defensive triumph at Bath last week, has been matched off the field, too. If you go down to the redeveloped Twickenham Stoop � where Quins have not lost in the league or Europe since September � you had better book in advance. Evans fully deserves a share of the credit for transforming the multi-coloured jesters into serious players since he left Saracens nine years ago this week.

And yet, for all the front-of-house activity, the story of Quins' resurrection has mostly been an object lesson in collective effort and man-management. The players may have put the tackles in but the head coach, John Kingston, and his assistants, Colin Osborne and Tony Diprose, not to mention the club's psychologist, Pieter Kruger, have been pivotal. When Richards describes Kingston as "the driving force" he is not simply being modest.

"John is very different to any forwards coach I experienced at Leicester," says Skinner, the club's articulate captain who learned his trade at Welford Road. "The amount of analysis work he puts in is unbelievable and he knows the game inside out. He's brilliant in that sense."

Kingston is also a proven fighter. Having played for the club in 1986-87, before a knee injury ended his career, he was in charge at Richmond before their brief professional joyride hit a brick wall. After a stint at Connacht he was made head coach at Quins, only to be demoted to forwards coach by Evans early in 2002, with the club bottom of the Premiership table.

"The easiest thing would have been to turn my back on it all," he says, "but I made the decision that the club hadn't seen what I was capable of."

Later that same year doctors discovered a blood clot near Kingston's brain which necessitated a lengthy operation. In 2005, when relegation came knocking, he hung in there again.

"I felt there was blood on my hands and everybody else's," he says. "As a consequence I felt that if Harlequins were to go forward and I wasn't part of it I would regret it for the rest of my life."

Richards certainly knew what he was doing when he offered Kingston his old role back last year, following Andy Friend's decision to return to Australia.

"It was this week last year when Dean asked me," says Kingston. "My mother had just died and there was a lot on my mind. With due respect to Mark and everyone else, I felt I hadn't been given the opportunity previously to show what I was about."

He duly challenged the players to improve on last season's sixth-placed finish and to embrace an amalgam of hard work, honesty, integrity and positivity.

"It's not about patterns of play, it's about what we stand for," he says. "We also made a commitment that, unlike some others, we weren't going to whinge about the experimental new laws."

While the worker ants get on with it, the bear-like figure of Richards sits back and delegates in his own way.

"He's got this big aura of respect but he's actually quite a shy guy," says �Skinner. "Some people, particularly new players, say they don't find him the easiest to talk to. But once you make an effort to talk to him, he warms to you. As I often say to the boys, 'When he takes the piss out of you, you're fine. It's when he doesn't talk to you that you're in trouble.'"

Richards can smell trouble like farmers sniff approaching rain � "I don't like bad apples and I'm renowned for not �accepting them," he says � and is not bothered by anything that Wasps or Saracens may be doing up the road. "This weekend [against Leinster] is a great opportunity but if it doesn't happen I'm pretty sure it will next year or the year after because we've got the right people in place," he says.

Evans describes his director of rugby as "a great selector � probably the best I've come across" and Kingston says: "If the player isn't thinking, 'You haven't picked me, you git,' the relationship with me as a coach is so much more relaxed," he says. "Dean doesn't do the pre-match or the half-time talks, or get involved in the tactical or technical side, but he's an excellent sounding board. He just gives you the opportunity to express yourself."

If Richards's men can see off Leinster and then Munster in a likely Croke Park semi-final, it would arguably eclipse anything he achieved at Leicester, where he won the Heineken Cup in 2001 and 2002. It is Evans's belief, prior to Quins' first European quarter-final since 1998, that the club is in better shape than most.

"There aren't going to be 12 clubs left in England, you can be sure of that," he says. "You just can't keep asking your owner for another �5m. The world's changed. Anyone who thinks rugby is different is an idiot."

The secret of survival, he says, is continuity. "If we lose every single game from now until the end of the season we'll be desperately disappointed but we won't change anything."

Make no mistake, Quins are going places.
