T�rkiye Cumhuriyet Ba�savc�s� geliyor 
MECL�S, yarg�da �nemli bir reformunun ilk ad�m�n� bug�n atacak. 

Anayasa de�i�iklik teklifi ile her il, il�ede 21 y�ld�r g�rev yapan Cumhuriyet ba�savc�l�klar� d�nemine son verilecek. 

Bunun yerine T�rkiye Cumhiyet Ba�savc�s� sistemine ge�ilecek. 

Anayasa'n�n 158'inci maddesinde yap�lacak de�i�iklikle, Cumhuriyet ba�savc�lar�, T�rkiye Cumhuriyet Ba�savc�s�'n�n vekilleri olacak. 

T�rkiye Cumhuriyet Ba�savc�l��� g�revini de Yarg�tay Cumhuriyet Ba�savc�s� �stlenecek. 

Buna neden ise Yarg�tay Cumhuriyet Ba�savc�s�'n�n, yarg� �yeleri ve savc�lar da dahil, devletin �st d�zeyinde g�rev yapan ki�ilerle ilgili dava a�ma yetkisinden kaynaklan�yor.

29 EK�M SOHBET�

T�rkiye Cumhuriyet Ba�savc�l���'n�n olu�turulmas� i�in Yarg�tay Cumhuriyet Ba�savc�s� Sabih Kanado�lu uzun s�redir u�ra��yordu. 

Meclis'in son d�nemde Anayasa de�i�ikliklerine h�z vermesi Kanado�lu'nu cesaretlendirmi�. 

29 Ekim Cumhuriyet Bayram� kutlamalar� i�in An�tkabir'de yap�lan t�ren �ncesinde TBMM Ba�kanvekili Y�ksel Yalova, Genelkurmay �kinci Ba�kan�, MGK Genel Sekreteri ve Kanado�lu 11 Eyl�l sald�r�s� ve beraberinde ter�r�n globalle�mesinin �zerinde sohbete ba�lam��. 

Su� �rg�tlerinin yeniden yap�lanmas� do�rultusunda bir�ok �lkenin yarg� sisteminde de�i�ikli�e gitti�i an�msanm��. 

T�rkiye'nin de kendini buna g�re dizayn etmesi gere�i vurgulanm��. 

Bundan sonra haz�rl�k h�zlanm��. 

Genelkurmay, MGK, Yarg�tay ve Yalova di�er �lkelerdeki �rnekleri de al�p incelemeye ba�lam��. 

Yalova bundan sonras�n� ��yle anlat�yor:

��Klasik ekonomik modelin terk edilmesi ve turbo kapitalizme ge�i�le su� �rg�tleri kendini buna g�re dizayn etti. Baz� devletler yap�lar�n� ortaya ��kan yeni tip su� �rg�tlerine kar�� koyacak �ekle d�n��t�remedi. T�rkiye'nin de bir an �nce bunu ger�ekle�mesi gere�inde hemfikir olduk.��

Yalova T�rkiye'de su� te�kil eden her fiilin, i�lendi�i yerdeki g�revli savc�l�k taraf�ndan takip edildi�ini an�msat�yor. 

Oysa, bir b�lgede ele ge�en su� �rg�t� elemanlar�n�n, uluslararas� faaliyette bulunabildiklerini kaydedip ekliyor: 

���rne�in T�rkiye ile birlikte A-B-C �lkelerinde ayn� anda bombalama eylemleri oldu. Veya T�rkiye'nin de�i�ik b�lgelerinde eylem ger�ekle�ti. Birden fazla savc�y� ilgilendiren su�larda delillerin s�ratle saptan�p, yarg�da i�birli�inin sa�lanmas� i�in mevcut sistem uygun de�il.��

ONLINE YARGI

Sistemin nas�l �al��a�a��n� ise bankac�l�k sekt�r� �rne�iyle anlat�yor:

��Eskiden bir banka �ubesindeki hesaplar�n ve kredilerin durumunda merkez g�nler sonra haberdar olurdu. �imdi online sistemiyle merkezden b�t�n �ubeler an�nda g�r�l�p koordine edilebiliyor.��

Ba�savc�'n�n bu koordinasyonu sa�layaca��n� bildiriyor. 

Yak�n ge�mi�te ya�anan bir �rne�i de hat�rlat�yor: 

��Orhan Asl�t�rk ABD'de g�zalt�na al�nd�ktan sonra �ngiltere'ye ka�t���nda her bakanl�k birbirini su�lad�. Bu sistem olsayd� Asl�t�rk �imdi T�rkiye'deydi.�� 

�lke Ba�savc�s� sisteminin ABD, �ngiltere, Almanya, Fransa, Rusya, Macaristan, Arnavutluk, �in gibi bir�ok �lkede bulundu�unu belirtiyor. 

Meclis Uzla�ma Komisyonu bug�n toplan�p tek maddelik teklifi g�r��ecek.

Belki bir madde daha ekleyip, haftaya iki maddelik de�i�ikli�i komisyona sevk edecek. 

Anayasa'n�n 158'inci maddesinde yap�lacak de�i�ik, T�rkiye'de uzun s�redir ger�ekle�medi�inden yak�n�lan ��yarg�da reformun�� ilk ad�m� olacak. 
