Vikram Pandit, chief executive of US bank Citigroup, started last week by telling the group's staff that 52,000 of them would lose their jobs. A few days later, it looked as if he could soon be following them. The bank's share price halved in just four days and continued to slide as investors and rivals started to bet it could not survive the financial crisis, despite a chorus of protests from its executives, one of its leading investors and some high-profile analysts.

As the bank's executives staged a crisis board meeting, rumours swirled around Wall Street that it was preparing to ditch its Smith Barney brokerage business, or to sell off its cards business. Or that it would need an extra $100bn (�68bn) from the government, on top of the $25bn that it already has; or that Pandit's fellow directors were calling for his head. Others said it was poised to merge with Morgan Stanley, Goldman Sachs - or, indeed, any bank with the financial capacity to absorb it. 

But as Pandit appeared to rule out a Smith Barney sale and putative buyers like Britain's HSBC - one of the few global banks with the wherewithal to stage a rescue - made it clear they were not interested, the crisis intensified.

'It's fear and panic at this point,' said Gerard Cassidy, a banking analyst at RBC Capital Markets in Portland, Maine. 'Investors have seen similar movies this year, and the endings are very unpleasant.'

Pandit told key employees on Friday morning that they should not focus on the falling share price as that was not what concerned regulators and rating agencies. Instead, he said, they should remember that it has a solid capital position and a good business model.

Unfortunately, however, the market was beginning to suspect he was wrong on both counts: and his warning of job cuts was one of the things that brought those concerns to the fore.

While rivals like JP Morgan, Bank of America and Wells Fargo have taken advantage of the financial crisis to make government-brokered acquisitions of Bear Sterns, Merrill Lynch and Wachovia respectively, Citi has missed out. These moves gave its investment banking rivals the much stronger balance sheets of deposit-taking banks. When the deals are completed, these three will be the only US banks with more than $600bn in deposits, three times that of Citi. Pandit must regret having allowed Wachovia, with which it had agreed a deal, to defect to Wells Fargo. The $400bn of deposits it would have brought would have significantly bolstered its financial strength.

Instead, last week's announcement of job cuts - which will affect a fifth of the workforce and, some insiders fear, even more to come in the future - made it clear that Citi's only real option now is to shrink its business, whether by shedding staff or selling businesses. Neither is particularly palatable: a workforce which is concerned about who is going to be next out of the door is unlikely to be productive. Nor will it be easy to sell any of its businesses: apart from a lack of buyers with the cash to pay for them, the prospects for financial business profits are grim in the teeth of a global recession and a race to reduce debt by consumers and businesses alike. David Trone, an analyst at Fox-Pitt Kelton Cochran Caronia Waller, wrote in a note last Wednesday that, although the sale of more assets was crucial to 'fortify the capital base,' it is unclear whether Citi 'will be able to continue to find buyers'. 

As significant as the job cuts was the announcement two days later that Citi had completed its withdrawal from SIVs - structured investment vehicles investing mainly in sub-prime mortgages, which have proved to be some of the most toxic assets in the financial crisis - by taking �17.4bn of them from one of its subsidiaries on to its own books. That made investors worry about two things: first, that by taking on the SIVs, Citi was effectively admitting that it would be one of the biggest losers from the US Treasury's decision to abandon its TARP programme, under which it had committed to buying the worst of these toxic assets from the big US banks.

And second, without that support, Citi would be forced to make yet more swingeing write-downs. It has already written off more than $70bn this year alone and is the only one of the big US banks to have made losses in four consecutive quarters - the latest was more than $2.8bn. 

It is a far cry from the swaggering Citigroup created by Sandy Weill, one-time chairman and chief executive of the bank. At the peak of its fortunes in 2006, its $205bn of annual revenues eclipsed those of countries like New Zealand and made it the 50th largest company in the world, with operations stretching across the globe - including one of the largest operations in Asia by a Western bank. But a series of regulatory breaches exposed the difficulties of managing such a sprawling and gung-ho investment banking empire, and led to Weill's replacement as chief executive by Chuck Prince. 

He proved himself as ebullient as his predecessor: when the financial markets were starting to show signs of strain in July 2007, he told the Financial Times: 'As long as the music is playing, you've got to get up and dance. We're still dancing.' 

That aggressive expansion into the teeth of the downturn was one of the factors which led to him being replaced by Pandit, but the former Morgan Stanley executive has not had an easy start. While he has been much more aggressive about writing down assets than many of our British banks, some have questioned whether he has the strength of will to rein in the global empire, or the vision to determine where its future lies.

His presentation to staff was full of platitudes about the future of its business like: 'Today, our strategy is simple: To be the world's truly global universal bank.' And on its financial strength: 'Over the past 15 months, Citi has added approximately $75bn in new capital, including approximately $50bn through public and private offerings.'

But the bank ended the week worth less than a third of the amount it has raised this year alone. And even a robust statement of support from one of Citigroup's biggest shareholders, the Saudi prince Al-Waleed Bin Talal - accompanied by a $350m investment in the bank's shares - failed to have any impact on its decline. 

Ladenburg Thalmann's Dick Bove, one of the most influential banking analysts in the US, said in a note: 'I see no reason why [Citigroup should fail]. The only reason banks fail is because their cash flows turn negative and it does not appear that this is likely at this bank. This is because the bank is able to roll over its liabilities and because its net interest income is positive.

'It would take a Depression every bit as large and long as the Thirties debacle to shake this company's viability.' 

The trouble is, that some commentators are now starting to worry whether we could be in for something worse than a Thirties-style depression as unemployment continues to rise and America's car giants look in danger of joining the list of casualties, risking putting many more millions out of work. 

In that climate, no amount of reassurance from analysts or bosses is enough to stem the panic.
