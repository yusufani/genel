Tekel�e b�y�k g�zalt� 
TEKEL�le ilgili yeni soru: �Tekel�den yurtd���na iade edilecek olan 8 adet sert kutu makinesi ne oldu?� 

�Makineler yolcu� (8.5.2005) ba�l�kl� yaz�m�zda, �spanya�n�n MTS firmas�ndan al�n�p Tokat sigara fabrikas�na montaj� yap�lan makinelerin, 2. el oldu�unun tespiti �zerine y�netim kurulu karar�yla yerinden s�k�lerek �mahre�ine� iade karar� al�nm��t�. Bu arada s�z konusu makinelerle ilgili harcamalar�n ve bu makinede kullan�lacak malzemelerin ve ihtiya� olmad��� halde KKTC�ye al�nan makinelerin de ak�betinin ne olaca��n� sormu�tuk.

O g�nden beri kimseden yan�t yok.

Haber verelim... Tokat�tan s�k�len makineler, yurtd���na g�nderilmek �zere �stanbul Erenk�y G�mr����ne teslim edildi.

Bu arada ilgin� bir geli�me oldu. G�mr�k M�ste�arl��� Ba�m�fetti�li�i�nin talimat� ile �ikinci bir emre kadar� makinelerin g�mr�kte muhafaza edilmesi dikkat �ekti.

B�yle bir karar�n hangi nedenle al�nd��� hen�z bilinmiyor. �artnamede yer ald��� gibi makinelerin �yeni ve kullan�lmam��� olmas� gerekirken, �2. el� oldu�unun tespitinden sonra yurtd���na ��kart�lmas�ndan do�an G�mr�k Kanunu�na muhalefet y�n�nden mi bir inceleme yap�l�yor acaba? 

Peki hukuki sorunlar ��z�mlenebildi mi? �rne�in, s�zle�me esnas�nda al�nan bir �kati teminat� var m�? Varsa, ortaya ��kan olumsuzluklar sonucu bu para Hazine�ye irat kaydedildi mi?

��te bu konu �ok �nemli... Bu makineler al�n�rken, s�zle�me a�amas�nda �artnamesi gere�ince kontr-garantiye dayal� teminat mektubu al�nmas� gerekiyor. Nitekim bu teminat mektubu al�nm��, ama ihaleyi �stlenen MTS firmas�ndan m� yoksa bir ba�ka ki�i veya kurulu�tan m�?.. Burada da kar��m�za �enol �elik ismi ��kmas�n sak�n? Tekel Genel M�d�r� Sezai Ensari�yi, Ispanya MTS firmas�na g�t�ren �enol �elik...

Bunlar�n yan�tlar�n� kim verecek?

YAPILAN YANLI�LAR

Tekel y�netimince 2. el makinelerle ilgili olarak yasa ve y�netmeliklere kar�� bu kadar m�cadele edilece�ine yap�lmas� gereken i� gayet basitti; dolamba�l� yollara sapmaya ve aylarca zaman kaybetmeye gerek yoktu.

11 adet 200 devir-dakikal�k 2. el makine yerine Samsun Ball�ca fabrikas�na mevzuata uygun �ekilde al�nd��� gibi DPT onay�ndan ge�mi� 4 adet 550 devir-dk�l�k makine ile sorun ��z�lemez miydi? 

Mevcut yasa ve y�netmenliklere g�re al�nan makinelere kimsenin diyece�i hi�bir �ey yok.

Ocak 2005 tarihinden beri �ilgili ve yetkili� denetim elemanlar�n�n ne i�lem yapt�klar� bilinmiyor ama herhalde savc�lar olay� incelemeye alm��lard�r.

Tekel burada bitmez... 

Herhalde �n�m�zdeki g�nlerde �stanbul�da toplanacak TBMM K�T Komisyonu, 200 milyon dolarl�k ucuz �t�t�n sat���� ve Ba�bakanl�k m�fetti�lerinin bir y�la yak�nd�r raporunu yazmad��� Tekel�in �stanbul Avrupa yakas� toptan sat�c�s� G�m-Bak/T�m-Bak olay�n� da g�r��eceklerdir.

Tekel�le ilgili iddialar, iktidar�n sessizli�i kar��s�nda gazete k��elerinde s�r�p gidecek.

Bayrak yakmada �ifte standart

HATIRLANIRSA, bayra��m�za sayg�s�zl�k yap�ld� diye Yunanistan�la kriz ya�ad�k. Tepki g�sterdik. En �st d�zeyde Yunanistan�dan �z�r istedik.

Bu sefer L�bnan�da bayra��m�z yak�ld�. Niye tepki yok! Niye gene en �st d�zeyde �z�r istemiyoruz? Sessizli�imizin sebebi L�bnan��n bir M�sl�man �lke olmas� olabilir mi? Bu y�zden L�bnan�a kar�� ho�g�r�l� m�y�z?

Bu durum, Yunanistan�a tepki vermek, L�bnan�a tepki vermemek bir �ifte standart de�il mi?

Suriye�nin topra��m�za d��en f�zesine de tepki vermedik. Neden?

Can MERA

Harabe SSK binalar�

CHP �stanbul Milletvekili M. Ali �zpolat, SSK a��r mali g��l�kler i�indeyken ve bunun faturas� halk�n s�rt�na y�klenirken; y�ksek maddi de�erlere sahip olan binalar�n de�erlendirilmeyi�inin ve harabeye terk edili�inin nedenlerini g�ndeme getirdi. �zpolat, �al��ma Bakan� Ba�esgio�lu�na soruyor: Yurt genelinde m�lkiyeti SSK�ya ait olup kullan�lmayan ka� adet gayrimenkul vard�r? Bunlar nerelerdedir ve ka� y�ld�r kullan�lmamaktad�r? Olu�an ekonomik kay�p ne kadard�r?

Bu durumdan kim veya kimler sorumludur? Bundan sonras� i�in ne yap�lmas� d���n�lmektedir?

Akkaya�ya acil �nlem

N��DE�nin, Bor �l�esi�ndeki Akkaya Baraj��nda son aylarda farkl� bir de�i�im ya�an�yor. �lk kez baraj �evresinde de�i�ik t�r ku�lar�n g��� var. Kayseri Sultan Sazl��� ve Tuz G�l� b�lgesine giden ku�lar bu kez Akkaya�ya g�� ettiler. 

Baraj 2003�te, kirlenme sonucu bal�klar�n �lmesiyle g�ndeme gelmi�ti. Bu kez ku�lar b�lgede dokuyu de�i�tirdi. Dik Kuyruk, Flamingo, Deniz K�rlang�c�, Ang�t gibi 100�e yak�n ku� g�� etti�i baraj �evresinde hi�bir �nlem al�nmay�nca sapan� ile ava merakl� �ocuklar, �oban k�pe�ini yan�na kat�p baraj kenar�na giden vatanda�lar ku�lara zarar vermeye ba�lad�. Ku�lar�n b�rakt�klar� yumurtalar tahrip edildi. Acil �nlem al�n�p b�lgenin korunmas� gerekiyor. Yoksa yuvalar�, yumurtalar� tahrip olan ve baz�lar� da avlanan hayvanlarla �nemli bir do�a dengesinin ���klar� daha s�necek. Ku� cenneti olmaya aday b�lge i�in yetkilileri konuya duyarl� olmaya �a�r�yorum.

�mer Fethi G�RER- End�stri M�hendisi- BOR-N��DE

G�N�N S�Z�

�T�rban�n �zg�r oldu�u yerde �zg�rl�k �zg�r de�ildir.� 

(S�leyman Ekim)

Biliyor musunuz

CHP�nin 2002 se�imlerinde %36; yerel se�imlerde ise (�stanbul B�y�k�ehir-Sefa Sirmen) %46 oy ald��� Adalar�da; Sedat Peker�in dinlenen telefon konu�malar�nda ad� ge�en Demir Karahan�a belediye ba�kan aday� olarak %18 oy ��kt���n�...

TEM�in Ankara�dan �stanbul�a giri� k�sm�nda (14.6.2005, saat 15.15) yabanc� plakal� (HSK-AK469) gri Peugeot�nun otomatik ge�i�ten ka�ak olarak ge�ti�ini...

DYP Genel Ba�kan Yard�mc�s� Saffet Ar�kan Bed�k��n, lise ��rencileri aras�nda genel lise ��rencilerinin oran�n�n %68.5, mesleki lise ��rencilerinin oran�n�n %31.5 oldu�unu, halbuki Bat� �lkelerinde bu oran�n tersine %35-65 olmas� gerekti�ini s�yledi�ini...

ISPARTA�da vefat eden T�P�in kurucular�ndan, sendikac� U�ur Canko�ak��n (71) cenazesinin bug�n D�SK Genel Merkezi (�i�li Nakiye Elg�n Sokak) �n�nde saat 14.00�de yap�lacak t�renden sonra Edirnekap� Mezarl����nda topra�a verilece�ini...

MESAJ PANOSU

BULGAR�STAN�da 25 Nisan�da yap�lacak se�imler �ncesinde Rumeli-Balkan Dernek ve Vak�flar��n�n d�zenledi�i �Balkanlarda Dostluk ve Karde�lik ��leni� yar�n ak�am 19.00�da Yedikule Zindanlar��nda yap�lacak. Sanat��lar; Rafet El Roman


ADANA B�y�k�ehir Belediye Ba�kan� Ayta� Durak��n �Orman-Ke�i ve Erozyon� konulu resim sergisi bug�n �ankaya Belediyesi �a�da� Sanat Merkezi�nde 15.00�de Devlet Bakan� Mehmet Ayd�n taraf�ndan a��l�yor. 
