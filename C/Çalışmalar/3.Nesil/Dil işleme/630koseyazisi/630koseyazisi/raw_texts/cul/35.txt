Susurluk davas� yeniden a��lmak zorundad�r  
     

  
D�N H�rriyet'te yer alan s�rman�et haber, bir tarih d�nemini ve kesinle�mi� bir yarg� karar�n� alt�st edecek de�erde:

��Korkut Eken her �eyi bilgimiz dahilinde yapt�.��

PKK'yla m�cadelede Korkut Eken ile birlikte aktif g�rev yapm��, ba�ta d�nemin Genelkurmay Ba�kan� Do�an G�re� olmak �zere �� general, Susurluk davas� g�r�l�rken sessiz kal�rken, nedense �imdi vicdanlar�n�n sesini dinlemek zorunda kal�yorlar (!) ve �erefli bir hareketle Susurluk davas� ba�san��� Korkut Eken'e sahip ��k�yorlar.

��Yapt��� her i� bilgimiz dahilindedir.��

* * *

��in daha da ilgin� olan�, Yarg�tay 8. Ceza Dairesi ��ter�rle m�cadele ad� alt�nda yola ��k�p, bir s�re sonra yasalar�n kendilerine verdi�i yetkileri (a�arak)�� derken, Do�an G�re�;

��...ald��� emirleri eksiksiz yerine getirmi�tir�� demekte.

Yine mahkeme karar� ��kendi ��karlar�n� g�zeterek, her y�ntemi uygun y�ntem olarak benimsedi�i anla��lmaktad�r�� derken, Do�an G�re�;

��Hi�bir zaman kontrolden ��kmam�� ve y�ksek disiplin anlay���.��  tabirlerini kullanmaktad�r.

Emekli General Necati �zgen ise;

��Yarg�n�n karar�n� tart��mak istemiyorum, ancak mahk�miyetin bilgi eksikli�inden (olu�tu�unu), ya da Korkut Eken'in �st�n yurt sevgisiyle mahkemede konu�mad���n� ve kendisini yeterince savunmad���n� d���n�yorum�� diyor.

* * *

Korkut Eken kendisini yeterince savunmad� ise di�er silah arkada�lar�n�n neden onu mahkeme s�ras�nda savunmad�klar�, tersine her �ey olup bittikten sonra neden �imdi savunduklar� sorusu bo�lukta kal�rken; ortaya yeni bir ger�ek ve soru ��k�yor:

Bu yeni bilgiler dahilinde Susurluk davas� yeniden g�r�lmek zorundad�r.

* * *

�u sorular�n samimi cevaplar�n� yeniden tart��madan yak�n tarihimizi ve hatta baz�lar�m�z a��s�ndan vicdanlar�m�z� temizleyemeyece�iz:

1) Korkut Eken ve arkada�lar�na verilen ola�an�st� yetkileri �st makamlar (�rne�in MGK) m� vermi�tir?

2) Bu yetkileri, mahkeme karar�na g�re ��kendi ��karlar�n� g�zeterek�� kullananlar somut olarak kendilerine ne gibi ��karlar sa�lam��lar, bu ��karlar� kimlerle payla�m��lard�r?

3) Devlet gere�inde hukukun �st�ne ��karak kendi varl���n� korumak hakk�na sahip midir?

4) Yok de�ilse, ter�rle m�cadelenin d�zensiz ordu mant��� d���nda ba�ka y�ntemleri var m�d�r?

* * *

1) Susurluk davas� yeniden g�r�lmek zorundad�r.

2) �imdi konu�arak belki de tarihe ���k tutan bu �� emekli general, neden dava s�ras�nda sustuklar�n�, neden �imdi bu a��klamalar� yapt�klar�n� kamuoyunu tatmin edecek �ekilde a��klamal�d�rlar.
 
