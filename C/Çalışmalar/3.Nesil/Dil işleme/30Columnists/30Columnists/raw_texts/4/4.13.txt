THE annual shindig for the Scottish Politician of the Year Awards, held at a swanky Edinburgh hotel, is a wonderful opportunity to watch the nation's political class as they strut, bitch, letch, imbibe and backbite.


They do this with an energy and enthusiasm that is truly impressive, while dressed up to the nines like guests at the Oscars. Particularly enjoyable is the sight of politicians squirming after they've been nominated but lost out to a deadly rival (ADVERTISEMENTespecially when that rival is from their own party). For those of us invited to observe, seldom is schadenfreude such a toothsome pleasure.

The night can throw up some politically troublesome moments. At last week's event Alistair Darling was named Best Scot at Westminster, but he couldn't be there on the night and so sent an underling to collect the gong. That underling was Iain Gray, leader of the Scottish Labour Party. That's just great, Gray told the audience in a rueful tone. Just as I was trying to persuade people I wasn't Westminster's man in Scotland, I end up being the Chancellor's delivery boy.

It was nicely done and earned a small laugh from a tough crowd. Yet like all good gags it contained a skelf of truth. Of all the contenders for the Scottish Labour leadership, Gray was rightly seen as the one with the closest ties to London. The Nationalists have already accused him of owing his primary loyalty to UK Labour rather than Scottish Labour.

Well, Gray now has the perfect opportunity to meet that accusation head on, and at the same time demonstrate that Labour is capable of responding to Scots' growing sense of ambition for their country. The party is currently drawing up its submission to the Calman Commission, the body set up by the Scottish Parliament to examine the case for more powers for Holyrood. And for Labour it's a moment of truth.

For those of us who favour a much stronger form of home rule, short of independence, there are some promising signs. Sources close to Gray insist Calman must be "a vehicle for change" rather than an excuse to defend the status quo.

That's a welcome contrast to the tone of the UK Government submission to Calman, published last week. It gave no ground at all, arguing that the current settlement was serving Scotland well. It seemed to be saying: "Come and have a go if you think you're hard enough."

Is Gray hard enough? There are signs he wants to see powers transferred from Westminster that would allow, for example, Holyrood control over the seabed off Scotland's shores � a crucial factor in the harnessing of tidal and wind power. Labour is also likely to push the case for Holyrood to have formal rights of legislative scrutiny for certain powers that will remain at Westminster, such as broadcasting. Watch out for the new buzz-phrase on this: "Shared responsibility."

But it's on the contentious subject of financial powers that Gray and his party will be judged. Again, some of the signs here are heartening. Some are less so.

What's encouraging is the strong signal from sources close to Gray that taxes raised in Scotland should be spent in Scotland. That way, the Scottish Government would benefit from policies that expanded the Scottish economy, and would be penalised if the economy shrunk. This system of "assigned revenues" would, so the theory goes, foster greater responsibility and accountability in the way Scotland was run.

Less encouraging are the noises Labour is making on the Scottish Government actually getting its hands on the levers of economic power � the ability to finesse tax law and vary rates of income tax and corporation tax in response to Scottish circumstances and priorities. On this, Gray is said to be lukewarm.

This, I believe, is where he's in danger of squandering a golden opportunity for Labour. Is it really enough to have the Scottish finances rise and fall in line with the success or failure of the Scottish economy, while at the same time denying the Scottish Government the most powerful means of influencing that economy? I suggest not. If anything, it would only raise new questions about accountability and responsibility. To borrow a phrase from the past, there would be a new democratic deficit.

Tomorrow is the 10th anniversary of the passing of the Scotland Act, the legislative foundation stone for devolution. On that day a decade ago, as the Bill passed its final stage in the House of Lords, there was a small party held in the Government whips' office. Alex Salmond had once famously sneered that "Labour couldn't deliver a pizza, let alone a parliament". So, naturally, the Labour lords celebrated with champagne and pizza.

Labour confounded the doubters then. It can do so again. But only if Gray is able to cajole and persuade his party that its stance on the constitution in 2008 must be aligned with the gut instincts of the Scottish people � who support more powers by large majority � rather than the gut instincts of the party faithful. If that argument fails to persuade Labour's MSPs, MPs and affiliated trade unions, then a more base political reasoning instinct should kick in. Because if Labour falls short of the mark on this, it will hand the Nationalists a very big stick indeed.

Those who have worked closely with Gray say he is a sceptic on more powers. But he is also a pragmatist who recognises that for Scottish Labour to thrive the party cannot be seen to be a brake on the desire for greater self-reliance and self-government. If this moment passes him by, little of what he achieves in other areas of his leadership will count for much, because this will be all he's remembered for. No pressure, then, Iain.

