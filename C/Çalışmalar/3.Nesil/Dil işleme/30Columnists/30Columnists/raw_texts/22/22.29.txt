
The England attack coach, Brian Smith, has apologised to South Africa's Jonathan Kaplan for publicly criticising his refereeing performance after last month's 23�15 Six Nations defeat by Wales in Cardiff.

Smith claimed that Kaplan, the world's most experienced Test official, did not referee both sides equally and suggested the Welsh management had influenced his performance. He was duly rebuked for his comments by the International Rugby Board and warned to direct any future concerns through the proper channels.

"We have spoken since the Wales Test match and I believe it was a positive conversation," said Smith. "I apologised for expressing my views publicly because it would have been more productive to have raised those directly with him after the match. Jonathan is one of the best referees in the world and I'm sure we'll continue to have a healthy working relationship."

England had two players sin-binned at the Millennium Stadium, after which their team manager, Martin Johnson, claimed his side were suffering from a "perception issue". Since then their tally of yellow cards has increased to 10 in four games and the penalty count has risen. It would suggest Smith's complaint cut little ice with Kaplan's compatriot Craig Joubert.

Any hint of simmering resentment is the last thing England need and Smith's apology is clearly designed to prevent the problem from recurring. Johnson has already taken steps to ensure his players offend less often against France next weekend and has threatened to drop individuals if his words are not heeded. There was, ironically, yet more bad disciplinary news yesterday when the England Under-20 back-row Josh Ovens was banned for three weeks for a dangerous tackle.

At least England are making a decent impression at the Rugby World Cup Sevens in Dubai where they have eased into the quarter-finals by topping Pool E. Isoa Damu's two brilliantly taken tries helped secure a fine 26-7 win over Kenya, although Ben Ryan's team had to survive a late rally before defeating Tunisia 26-24. Tom Varndell has already scored five tries.

The former Irish and British Lions lock Jeremy Davidson is to return to Ulster as forwards coach next season. Cardiff Blues fear they will lose Nicky Robinson to the Guinness Premiership, probably Sale or Worcester. Robinson's brother, the centre Jamie, will leave the Blues for Toulon in the summer. They have signed the utility back, Sam Norton-Knight, after Robinson put off signing a new contract.
