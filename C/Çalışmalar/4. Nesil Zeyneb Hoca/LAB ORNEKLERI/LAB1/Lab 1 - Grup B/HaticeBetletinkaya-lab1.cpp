#include<stdio.h>
#include<stdlib.h>

int main(){
	int i,j,x,y;
	int satir, sutun, deger;
	int **a, b[4][4];
	
	printf("Matrisin satir sayisini giriniz:");
	scanf("%d",&satir);
	printf("Matrisin sutun sayisini giriniz:");
	scanf("%d",&sutun);
	a=(int**) malloc(satir * sizeof(int**));
	for(i=0;i<satir;i++){
		a[i]= (int*) malloc(sutun * sizeof(int));
	}
	printf("Matrisin elemanlarini 0<=x<=3 olacak sekilde giriniz: \n");
	for(i=0;i<satir; i++){
		for(j=0; j<sutun; j++){
			scanf("%d", &deger);
			a[i][j]=deger;
		}
	}
	for(i=0;i<satir; i++){
		for(j=0; j<sutun; j++){
			printf("%d", a[i][j]);
			printf(" ");
		}
		printf("\n");
	}
	printf("\n");
	for(i=0;i<4; i++){
		for(j=0; j<4; j++){
			b[i][j]=0;
		}
	} 
	for(i=0;i<satir; i++){
		for(j=0; j<sutun-1; j++){
			x = a[i][j];
			y = a[i][j+1]; 
			b[x][y]++;
		}
	}
	for(i=0; i<4; i++){
		for(j=0; j<4; j++){
			printf("%d", b[i][j]);
			printf(" ");
		}
		printf("\n");
	}
	return 0;
}
