A huge public backlash against MPs over a string of disclosures about their expenses is laid bare today by the latest Populus poll for The Times. 

The survey, undertaken between Friday and yesterday, shows the majority of people believe that most MPs are abusing the system. The findings reveal the damage done to the public standing of Members by the recent string of stories. This has caused widespread worries at Westminster over growing cynicism about the political class. 

The survey also shows that there has been no G20 bounce for Gordon Brown or for Labour, despite a favour-able view of last Thursday�s summit in London. Labour is unchanged compared with a month ago on 30 per cent, while the Tories are one point up at 43 per cent. The Liberal Democrats are one point down at 18 per cent. Other parties are unchanged on 9 per cent. 

More than two thirds of voters think that all or a majority of MPs abuse their expenses and allowances. Some 27 per cent say that all or nearly all MPs abuse the system, and 42 per cent think that a majority of MPs do so. By contrast, 20 per cent say that �a majority of MPs do not abuse the system, but many do�, while just 8 per cent say that very few MPs do. 

Working-class voters and Tory supporters are most inclined to say that most MPs abuse the system. Professionals and managers are most sympathetic to MPs over their allowances. 

Moreover, voters do not have much confidence in their own constituency MP. A third (34 per cent) think that their local MP abuses the system, while only slightly more (38 per cent) think that their MP does not. As many as 28 per cent say they don�t know, including those who do not know the identity of their MP. This is worrying for MPs, because previous polls have shown a contrast between the low standing of Parliament and the higher reputation of constituency MPs. 

There is no comfort for MPs in voters� rejection of possible solutions to the allowances problem. Only just over a fifth (22 per cent) think that the present system of allowances for second homes should be retained but the rules tightened up. Just under a fifth (19 per cent) believe that the allowance should be scrapped but say that MPs� salaries should be raised to compensate. However, more than half the public (56 per cent) think not only that the second-home allowances should be scrapped but say that there should be no rise in MPs� pay. 

The voting intentions figures are in line with other recent polls, though contrast with a YouGov survey, taken immediately after the G20 summit, which registered a three-point gain for Labour. Any small boost may have disappeared two or three days later when Populus finished its interviews. This will reinforce the Labour leadership�s belief that it will be a long haul, both economically and electorally. 

This conclusion is underlined by a question on whom you most trust to deal with Britain�s economic problems. Mr Brown and Alistair Darling have edged up one point since early March to 34 per cent, but this is still behind David Cameron and George Osborne, unchanged on 37 per cent. 

Nonetheless, the G20 was still a plus for Mr Brown. A quarter (26 per cent) feel more positively towards him as Prime Minister because of the way that he chaired the meeting, while 11 per cent feel less positively with 62 per cent saying no difference. The most positive are professionals and managers and Labour and Lib Dem voters. 

The G20 measures have produced a glimmer of hope. Just 8 per cent (up two points on last month�s expectations before the meeting) have a great deal of confidence that the G20 measures will help the recession in Britain, while 66 per cent (up ten points) have a little confidence. Some 24 per cent have no confidence, down 14 points. Similarly, just 11 per cent (up six points) have a great deal of confidence in the G20 measures helping the global economy to recover. 

The G20 summit does not appear to have changed the generally settled views of voters about the parties. Labour has been in the range of 30 per cent, plus or minus two points, since the onset of the banking crisis in the autumn, while the Tories have fluctuated around 42 per cent, below their peak of last summer but well above their level from 2005 until spring last year. The Lib Dems have been about 16 per cent over the past six months, though edging up slightly this year. 

