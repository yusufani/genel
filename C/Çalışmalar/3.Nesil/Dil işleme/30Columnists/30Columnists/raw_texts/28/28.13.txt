As the mob outside in the street bayed for Glenn Roeder�s head, his wife sat hunched on a sofa in an upstairs lounge at Carrow Road.

It had been a bad year, Fay Roeder said. Her mum died during the summer and now Glenn�s mother was gravely ill. And the football...

The football was grim. Norwich lost to Charlton in an FA Cup replay and afterwards, about 200 supporters gathered outside the main entrance to vent their anger and frustration. Upstairs, behind windows on which the blinds had been pulled, Fay Roeder heard it all.

GET ALL THE LATEST PREMIERSHIP NEWS NOW

Around her sat a sad-eyed group of family and friends, perched on comfortable chairs which, that night, were anything but comfortable. The Roeders� adult daughter sat on the arm of one chair, her body language evoking support and protection for her mum, her words defiantly proud of her dad.

Other people in the room, Norwich City�s inner sanctum on match days, were dotted around in twos and threes. Some were seated, some stood nursing long-cold cups of tea and coffee. 

There was a lot of silent head-shaking and, when folk spoke, they kept their voices mournfully low. They avoided eye contact with Fay as the chants drifted up from the street: �We want Roeder out! We want Roeder out!�

The campaign against the manager had started weeks earlier. It was well organised. There was a �Roeder out� website. The anger was largely because, although Roeder saved the club from relegation from the Championship last season, they are back in peril this season. But there was also a personal antipathy towards him. At the club�s AGM he rounded on one critical fan with withering sarcasm and that played very badly in Norfolk.

 

Even so, this week�s demonstration showed new depths of hostility. Upstairs in the lounge a senior member of staff told the Roeders� daughter that, when she was ready, he would escort her to her car via a discreet exit so that she could avoid the mob.

Michael Wynn Jones, Delia Smith�s gentle, donnish, 67-year-old husband, was told that his car had been moved because, if it had remained outside the directors� entrance, it might have become a target.

Delia herself was not present. The TV cook had a writing deadline and an early-morning appointment in London the next day and so, for a change, she had not watched the match involving the club of which she and her husband are joint majority shareholders.

It was a good game to miss. Norwich gifted an early goal to a team who had not won for 18 matches, and then could not muster a single first-half shot. At half-time Roger Munby, the Norwich chairman, stood at the front of the directors� box, where the players could see him, and made an ostentatious show of encouragement. A knot of supporters abused Munby, faces contorted with rage.

I happened to be near Munby�s wife. She shuddered involuntarily as she saw him insulted and muttered: �He doesn�t deserve that.� Norwich were marginally better in the second half. Next to me in the directors� box, Wynn Jones�s mood fluctuated between impotent fury and fatalistic despair. Later, in the lounge, he looked utterly forlorn.

He repeatedly telephoned his wife but the line was busy. He suspected she was talking to her mother, the remarkable Ettie, who goes to home games whenever Delia does and has some extremely forthright opinions when they lose. Wynn Jones and Munby talked in whispers and then disappeared somewhere. Neither of the other directors � businessman Michael Foulger and chief executive Neil Doncaster � was present in the lounge. An emergency meeting was obviously taking place. Presumably Delia had been reached by phone.

Roeder was typically pugnacious at a press conference, then was driven home by Fay. The next day he and first-team coaches Paul Stephenson and Adam Sadler went to the training ground to supervise �training down� � the light workout which follows a match.

At lunchtime, Munby and Doncaster arrived. They were not there to try the pasta. Roeder, Stephenson and Sadler were sacked on the spot.

It was the board�s experience with Nigel Worthington that convinced them Roeder�s time was up.

Worthington took Norwich into and out of the Premier League and, when the fans turned against him, Delia and her husband suffered personal abuse as they steadfastly refused to sack him.

But as supporters�  hatred grew stronger, confidence seeped from the players and defeats gathered their own momentum. Eventually, Smith and Wynn Jones concluded Worthington had to go.

With those memories still raw this time, and with some must-win matches imminent, the board reasoned backing Roeder would cause a poisonous atmosphere that would choke the team. 

The decision was not taken lightly or without considerable soul-searching. And so, at the very heart of a family club, a lot of families got hurt.

