No one in power has been here before. Just as the banking sector�s difficulties have partly been because the key players have known only the boom years, and not past hubris leading to bust, so institutional memories among policymakers are short: few go back to the 1980s, let alone the 1970s. 

Almost all current politicians and officials have spent their entire careers in an era shaped by the battles over inflation. That has been reinforced by a fall in the average age of senior Treasury officials: few are over 50, and many are under 40. That is why Vince Cable, the Liberal Democrat treasury spokesman now aged 65 and an economist for more than 40 years, has an advantage in remembering an earlier era. 

Most officials, let alone ministers and their even younger Conservative shadows, have operated solely in the policy framework created by the aftermath of Black Wednesday in 1992, and the 1997 changes. This has meant a central focus on inflation targets, with tight fiscal rules to ensure low budget deficits (in aspiration, if not always in practice). The exchange rate has been treated as secondary, and no one has worried about how to finance the deficit with sales of government debt. 

All that has now changed. This has been dramatised by the big shift in approach in the past three months shown in this week�s Inflation Report from the Bank of England. Mervyn King, the Governor, has argued that the world has changed, not his fundamental view. But that is not how it looks. 

No one, outside Japan in the 1990s, has worried about deflation, a decline in the general price level, since the 1930s. That limits the effectiveness of even very low interest rates and accounts for the surge in support among economists for a big fiscal stimulus. Hence, also, the St Augustine-like approach to the public finances likely in the Pre-Budget Report in ten days: make me virtuous, but not yet. 

However, much less discussed is the return of familiar problems from the pre-1992 era: a weak exchange rate, and the need to sell more than �100 billion annually in gilt-edged stock for the next few years. 

The link between the two was a perennial dilemma in setting interest rates, leading to sharp movements up and down. That is now complicated further by the prospect of very low inflation or deflation. (For a reminder of what it used to be like, read the fascinating Decline to Fall � The Making of British Macro-Economic Policy and the 1976 IMF Crisis, produced this year by Sir Douglas Wass, Treasury Permanent Secretary in the 1970s and early 1980s.) 

At present, these fears have been put to one side as policymakers focus on kick-starting a recovery. But the next crisis, not very far ahead, could be over sterling and gilt sales. One result could be a renewed debate over euro membership, however improbable that looks politically. 

It is time to forget the economic speeches of the past 16 years and to study economic history. Meanwhile, we can enjoy the sight of the Treasury and the Bank reinventing themselves, as they had to do in the 1970s. 

