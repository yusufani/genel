School is in full swing, it�s fall, and strep-throat season is about to begin. 

All physicians and most parents by now know the importance of recognizing and adequately treating a throat infection caused by Group A streptococcal bacteria. These organisms, if not stopped in their tracks by appropriate antibiotics, can result in rheumatic fever and permanently damaged heart valves, among other serious complications.

Thanks to penicillin, which readily kills strep bacteria, rheumatic fever has all but disappeared in countries like the United States. Now physicians are worried about overtreatment, the prescription of antibiotics for children whose sore throats are caused not by bacteria but by viruses that do not cause long-term damage and are not susceptible to antimicrobial therapy. 

Some children and adults are healthy carriers of strep bacteria; the organisms reside in their throats but do not make them sick. They rarely, if ever, spread the infection to others. But when such carriers develop a sore throat for any reason, a positive test result for strep typically leads to treatment with antibiotics, which is often needless and possibly hazardous.

Complicated Decision

Symptoms of a strep throat and a sore throat caused by a virus can overlap (children may experience stuffy noses, coughs and sneezing with a strep infection as well as with a cold), further complicating a doctor�s decision on whether to treat the illness or to let nature take its course. Nationally, 70 percent of children with sore throats who are seen by a physician are treated with antibiotics, though at most 30 percent have strep infections. And as many as half who are treated with antibiotics because a throat culture was positive for strep are healthy carriers and actually have a cold or some other viral infection, says Dr. Edward L. Kaplan, a pediatrician at the University of Minnesota in Minneapolis and an expert on streptococcal illness. 

Antibiotic treatment is best reserved for illnesses in which it is likely to be effective. Overuse of antibiotics can give rise to dangerous antibiotic-resistant bacteria. Antibiotics can wipe out friendly bacteria in the gut, and they sometimes cause life-threatening allergic reactions.

Both Dr. Kaplan and Dr. Alan L. Bisno, an internist at the University of Miami School of Medicine and the Veterans Affairs Medical Center in Miami, say there are usually good ways for physicians and parents to distinguish between sore throats caused by a strep infection and those caused by a virus or some other bacterium.

Group A strep causes 15 percent to 30 percent of sore throats in children, Dr. Kaplan and Dr. Bisno reported in the September issue of Mayo Clinic Proceedings. The illness is most common in school-age children as old as 15. 

Strep infections are less common in adults, who are also far less likely than children to develop a serious complication like rheumatic fever if a strep infection goes untreated.

Strep bacteria are shed from the nose and throat of infected people and easily spread to others. This is why strep often makes the rounds in classrooms and day care centers. Occasionally, strep infections �ping-pong� among family members, but �there�s no strong evidence that the family pet is a source,� Dr. Bisno said.

In a small proportion of children, strep infections occur repeatedly over the course of several years. Jennifer L. St. Sauver and colleagues at the Mayo Clinic in Rochester, Minn., analyzed cases of strep throat occurring at least one month apart among children ages 4 to 15 in Rochester between Jan. 1, 1996, and Dec. 31, 1998. They found that 1 percent of the children (2 percent of those from 4 to 6 years old) had repeated episodes. 

Dr. Kaplan and Dr. Bisno point out, however, that as thorough as the Rochester study was, in all likelihood the incidence of repeated strep throat infections is lower than what the researchers found. In only about a third of the cases counted as strep were data available that showed the cases met the accepted clinical profile of a strep infection. 

