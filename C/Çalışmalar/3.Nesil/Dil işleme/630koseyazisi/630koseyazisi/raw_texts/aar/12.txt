�nteraktif ili�kiye girelim mi? 
Dubai s�ca��nda ne�eli bir g�n�mdeyim. ��imden ba�l��� b�yle atmak geldi. Ho� g�r�n�z. Ve l�tfen g�c�kl�k yapmay�n�z. G�r���r�z... 

GEL�NL���M� G�LE G�LE KULLAN

Aksilik ��kmazsa, 6 A�ustos�ta evleniyorum, en �ok gelinlik se�imi konusunda zorlan�yorum. Ben senin zevkine ve ald���n kararlara g�venen bir okurunum. �imdi diyeceksin ki, �Yand�k! Gelinlik isteyecek galiba.� Hay�r, ba�ka bir �ey istiyorum: Birinci evlili�inde giydi�in gelinli�i �ok be�enmi�tim, �Tam benim istedi�im gibi� demi�tim. San�r�m, Cemil �pek�i�ydi. Benim ona diktirme �ans�m yok. Ancak resmi olursa, belki buradaki gelinlik�ilere benzer bir �ey diktirebilirim. Gelinli�in bir foto�raf�n� yollamak seni �ok yorar m�?

- Hemen Leman�� ar�yorsun ve diyorsun ki, �Gelinlik, gard�robun en dibinde bir elbise torbas�n�n i�indeymi�. O bana verilecek, ben gelin aday� G.� Sen tabii ad�n� a��k haliyle s�yl�yorsun. Parola gibi ismin var, ba�ka birinin kendini sen diye yutturma ihtimali yok! Gelinli�i bir �ekilde Leman�dan ald�r�yorsun; ister ayn�s�n� diktir, ister temizlet o g�n giy, o art�k senindir, ne yaparsan yap. Bol �ans dilerim. Leman��n numaras�: 0537 587 01 68 (Makul bir saatte ara olur mu, kocas� Mustafa Bey�den azar i�itmek istemiyorum!)

��YLE K���K B�R K��E YETER BANA

Adanal�y�m. Ben de k��e yazar� olmal�y�m. �yle �nemli �eyler yaz�p, d�nyay� kurtarmak de�il amac�m. Her g�n bir iki sat�r bir �eyler karalar�m. Hayata dair, a�ka dair, ili�kilere dair, erkeklere dair. E�er gazetenizde bir k��e bo�al�rsa, haber verir misiniz? ��yle k���k bir k��e yeter bana... 

- Sevgili hem�erim. Gazete k��elerine kiral�k daire muamelesi �ekmeniz beni g�ld�rd�. Ay�pt�r s�ylemesi kendinize g�veniniz de... Sizi �zmek istemem ama zannetti�iniz kadar kolay olmuyor bu i�ler... Ne diyeyim, siz yine de bo� k��eleri kollamaya devam edin!

HAZ�RANDA DUBAݒDE BALAYI MI 

Merhaba g�zel anne. Bug�ne kadar sana bir�ok konuda yazma iste�i duymu�umdur. Bazen kar�� bir fikir bildirmek i�in, bazen hemfikir oldu�umu s�ylemek i�in, bazen i�ten bir tebrik bazen de Alya�ya kucak dolusu sevgilerimi iletmek i�in. K�smet bug�neymi�. 26 Haziran�da �ocukluk a�k�mla evleniyorum. Senden de �Dubai�de Balay�� ba�l�kl� bir yaz� yazman� bekliyorum. Ben de ne yap�l�r, ne edilir, nereye gidilir, nereye gidilmez ��renirim.

- Ben nereye gidilmeyece�ini hemen s�yleyeyim: Dubai�ye! Haziran sonu, temmuz ve a�ustosta buradaki havay� ��yle tarif edebilirim: F�r�n� a� kafan� sok! O kadar s�cak yani. Ger�i o tarihlerde Dubai�de balay� yapman�n ��yle bir avantaj� da var: Otel odas�ndan ��kamazs�n�z! Se�im size kalm��.

�NSANIN K���L���YLE KURDU�U KALE

Rekabet ortam� i�inde oldu�unuzu hissediyor musunuz? Yoksa, �Ben Ay�e Arman��m, markay�m� gibi d���nceler i�inde misiniz? Evet, belki insan kendi ki�ili�iyle bir marka yaratabilir ama insan hayat�nda �yle bir an gelir ki, kendini besleyemez, yenileyemez, bir �ekilde tak�l� kal�r. Ya insan �yle bir d�nemin i�ine girer de... ��kmak i�in debelendik�e batt�k�a batarsa... O zaman insan�n ki�ili�iyle kurdu�u kale ��kmez mi? 

- ��ker. ��nk� sadece ki�ilik, s�kmez. �al��mak gerek. Hep. Yarat�c� olmak gerek. Hep. Kafay� farkl� �al��t�rmak gerek. Ama hayat da sadece i�ten ibaret de�il be g�zelim! Ba�ka alanlarda beslenmezsen de, i�te �i�ersin.

BEN B�R KADIN E�C�NSEL�M

1 y�l 11 ay sonra, 26 y�ll�k hayat�mda, kollar�nda g�ven buldu�um tek insandan ayr�ld�m. T�rkiye�de e�cinsel olmak o kadar zor ki... �nce kendinizle m�cadele ediyorsunuz, sonra �evreniz ve ailelerinizle... Ve b�t�n bu gerginlikler ortas�nda siz... Kaybolup gidiyorsunuz, ili�kiniz de... Ben �ok sevdim, �ok m�cadele ettim ama bir g�n bakt�m ki, ili�kinin ba��ndaki o etrafa ne�e sa�an kad�n gitmi�, yerine her �eye kusur bulan, bir t�rl� memnun edilemeyen bir kad�n gelmi�. Ben kendimden yoruldum, sevgilim de benden. �imdi i�yerindeki bilgisayar�n ba��na ge�mi� i�imdeki bu ac�n�n dinmesini bekliyorum ve hi� tan�mad���m birine hayat�m�n en �zel anlar�n� anlat�yorum. Can� s�kk�n oldu�unda, geceleri uyurken di�lerini g�c�rdat�rd� pembe surat, ben de can� ac�mas�n diye parma��m� a�z�na sokar �yle devam ederdim uyumaya, parma��m� �s�r�rd� fark�nda olmadan, ama olsun. Ben onun i�in her �eyi yapabilirdim, o da benim i�in. Ama olmad�. Etraf�m�zdaki bask�n heteroseks�el d�zen bizi y�pratt�. Herkes i�in �nemli olan bizim bir an evvel ayr�lmam�z ve m�mk�n oldu�u kadar �abuk �oluk �ocu�a kar��mam�zd�. G�zleri baldan tatl� ar�m� bunu s�ylemekten hep utanm�� olsam da e�imi, kar�m� kaybettim ben. Heteroseks�ellerin zaferi bir kere daha kutlu olsun! 

- Siz mutsuz oldu�unuz i�in �z�ld�m tabii ki. Ayr�l�k herkese koyar. Ama ge�er. Allah yard�mc�n�z olsun... Gibi palavralar s�kmak istemiyorum. Cinsel tercihinizden �t�r� bu sorunlar hep olacak. Bu �lkede ya�ad���n�z s�rece. Ya da bu �lke de�i�medi�i s�rece. K�sa vadede de�i�ece�e de benzemiyor... Kuvvet diliyorum.

B�R DAHA HAM�LE KALMAK MI?

O�lumun hemen �zerine 2. kez hamile kald�m. Planl� de�ildi, kaza kur�unu. Ama ne yalan s�yleyeyim, sevindim. Ya hemen bir tane daha yapacaks�n ya da bu i�lere asla kalk��mayacaks�n. Ger�i 1. hamileli�im �ok rahat ge�mi�ti, bu da �yle ge�er zannettim. Halt etmi�im, hi�bir hamilelik birbirine benzemiyor haberin olsun, hepsini kendi i�inde de�erlendirmek gerekiyor. Ama sen zaten 2. hamileli�i d���nm�yorsundur. 

- Uyar�n i�in te�ekk�r ederim. Ama niyetim ikinciye de ula�mak. Ben karde�lerimle b�y�d�m, Alya da �yle b�y�s�n isterim. Tabii sadece benim istemem yetmiyor, sevgilimin de onay vermesi gerekiyor. �imdilik hi� oral� de�il, bakal�m. 

TEYZENE �Z�LD�M AMA ALYA DAHA �NEML�

Teyzenle ilgili yaz�y� okurken bo�az�m d���mlendi, g�zya�lar�m� tutamad�m, a�lad�m. Allah teyzenin yard�mc�s� olsun, evlatlar�n�n ba��ndan onu eksik etmesin. Sen de uzakta, �aresiz, kuzeninin ve teyzenin bu durumuna �z�l�yorsundur. Kendine dikkat et, bu durumlarda �z�nt�den insan�n s�t� kesilebilir. Aman ha evlad�m, d�nya tatl�s� bebe�in her �eyden �nemli, annesinin s�t�nden mahrum kalmas�n. Alya�y� ve seni �perim. 

- Bay�l�r�m ve �l�r�m size... �ok ho�uma gitti s�yledikleriniz, beni derinlerde bir yerden yakalad�n�z. Allah sizden raz� olsun! 

YAPAMAM JALE HANIM

Bence bug�nden itibaren k��enizde kulland���n�z foto�raf�n yerine, �imdiki halinizi yans�tan bir foto�raf�n�z� kullanmal�s�n�z. ��nk� halihaz�rda olan foto�raf ger�ek sempatikli�inizi kamufle ediyor. O ��lg�n ve bence size �ok yak��an �zg�r ve kendine g�venen bir kad�n� ortaya koyan sa� modeliniz ve yaramaz �irin bir �ocu�u �a�r��t�ran g�l���n�z sizi medyada markala�t�rd�. Ben sizin g�l�mseyen g�zlerinizi g�rmek istiyorum. L�tfen bu �nerimi dinleyin. 

- B�kmad�n�z �u resmimle u�ra�maktan! De�i�tirmeyece�im i�te. Bir defa esrarengiz. O foto�raftaki kad�n�n neler yapabilece�i me�hul. Kendi surat�mdan s�k�ld���m zaman o resme bak�yorum, oyalan�yorum. �zniniz olursa, yay�n hayat�ma o eski foto�raf�mla devam etmek istiyorum!

HAZ�RAN GECES� �NT�HAL M�

Pek fazla dizi m�ptelas� de�ilim ama Haziran Gecesi�ni izleyece�im tuttu. O g�nlerde de bir arkada��m evini ta��yordu, fazla kitaplar�ndan kurtulmak istiyordu. Elime Danielle Steel�in bir roman�n� tuttu�turdu. Sana ne diyebilirsin ama s�ylemeden edemeyece�im: Haziran Gecesi, Danielle Steel�in Umutlar Ye�erecek kitab�yla olduk�a benziyor. Hatta fazlaca benziyor. Hani �yk� �zcan Deniz�e aitti? 

- ��te bir intihal iddias� daha. Bu kelime art�k dilimize �ok fena yap��t�. Ba�ak�����m, s�z konusu kitab� okumad�m, ama benim okumama gerek kalmayacak, merakl�lar o kitab� hemen yak�n takibe alacaklard�r, i�in rahat olsun. Bu arada ben Haziran Gecesi�ni severek izliyorum...

SADUN TANJU YO�UN BAKIMDA

Dedem, eski dostunuz, 55 y�ll�k gazeteci-yazar Sadun Tanju, �ok sevdi�i Turgutreis�teki yazl���nda, ciddi bir koroner rahats�zl��� ge�irdi. Bodrum Universal Hastanesi�nde yap�lan ilk tetkiklerin ard�ndan, �stanbul�a getirildi. 24 May�s�ta Alman Hastanesi�nde Op. Dr. �smail Y�celtan ve ekibi taraf�ndan by-pass yap�ld�. �u an yo�un bak�mda. Ameliyattan �nce en �ok neye bozuldu biliyor musunuz: 35 y�ll�k ak sakallar�n�n kesilmesine! Ameliyathanenin kap�s�na kadar el ele gittik. O anda bile espri yapmay� ihmal etmiyordu: �Ben bile kendimi tan�yam�yorum, sen beni nas�l tan�yorsun?� �p�lesi ak sakallar, �p�lesi al yanaklar oluvermi�, dedo�um 4 saatte gen�le�mi�ti... 

- Sevgili Sadun Tanju. Sakallar�n�z� kaybetmi� olabilirsiniz ama �z�lmeyin, bunu sa�l���n�z� kazanman�z�n kar��l���nda k���k bir bedel olarak d���n�n. Sizinle Bodrum k�y�lar�nda yeniden bir y�r�y�� yapabilmenin keyfi tonlarca sakaldan k�ymetlidir benim i�in. Ben sizin eski toprak oldu�unuzu biliyorum. �yile�ece�inizi de... H�l� Dubai�deyim, �stanbul�a d�n�nce en k�sa zamanda ziyaretinize gelece�im. Hem o zamana kadar sakallar da biraz uzam�� olur. Acil �ifalar diliyorum. Ve sizi �ok seviyorum... 

