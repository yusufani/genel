Tanr� korudu! 

Be�ikta� Ba�kan� Say�n Y�ld�r�m Demir�ren'in bir �z�r borcu var. Bu bor�, Be�ikta�l�lara de�il, Be�ikta� ba�kanl�k koltu�una... Hani �u ba�kan olmak i�in verilen s�zler vard� ya! Ne diyordu Say�n Demir�ren: "Ba�kan se�ilirsem, Be�ikta�'�n antren�r� Lothar Matthaeus olacak. B�ylece, T�rkiye Ligi'nde Daum- Matthaeus sava�� olacak."
�imdi buraya bir nokta koyuyorum. Diyorum ki: Say�n Demir�ren, Genel Kurul'a verdi�i s�z� tutmamakla iki b�y�k olay yaratt�.
Birincisi; Be�ikta�'� kurtard�. �kincisi ise Ola�an�st� Kongre'nin yolunu daha ilk g�nlerden kapatt�. Yani diyorum ki, e�er Say�n Demir�ren verdi�i s�z� tutup Lothar Matthaeus'u Be�ikta� Antren�r� yapsayd� bak�n neler olurdu.
1- Matthaeus asla Daum'u yenemezdi. Zaten antren�rl�k kariyerine bakarsan�z, Matthaeus'un, Daum'un �antas�n� bile ta��yamayaca��n� g�r�rs�n�z.
2- Matthaeus, Be�ikta�'� de�il, kendi futbol kariyerini d���nd��� i�in her t�rl� ��lg�nl��� yapacakt�. Yani �ok b�y�k paralar harcat�p transferler yapacak, sonra da hi� ba�ar�l� olamadan ka��p gidecekti.
G�rd�n�z de�il mi? Tanr�, Be�ikta�'� korumu�.
Bazen verilen s�zlerden de d�nmek, do�ru yolu bulmak demektir.
Say�n Y�ld�r�m Demir�ren, uyar�lar� da dikkate alarak, Matthaeus'tan vazge�ip, Real Madrid'in eski antren�r� Vicente Del Bosque'yi getirerek, hem Be�ikta�'� hem de kendini kurtard�. Ve de Be�ikta� ba�kanl�k koltu�unu.
Say�n Demir�ren'in �n�nde �u sorunlar var. Ama isterse bunlar� da ��zebilir.
1- Ge�en y�llarda Be�ikta�'�n k�lavuz kaptan� olan Sinan Engin gibi bir menajeri aray�p bulmas� laz�m. Asl�nda fazla aramas�na da gerek yok. Bu konuda tek isim Metin Tekin.
2- Son 5 y�ld�r, Be�ikta�'�n gelece�ine yat�r�m yap�lmad�. ��te bu y�zden Del Bosque'nin g�reve gelmesi b�y�k f�rsat. Ayr�ca Sinan Vardar gibi �zkaynak d�zenini �ok iyi bilen bir y�neticinin de g�revde olmas� b�y�k �ans. 
3- Be�ikta� ge�en y�l �ampiyonlu�a 13 bin seyirci ile oynad�. Stat yar�n 30 bin ki�ilik olacak ve bu stat nas�l dolacak. El- bette kaliteli ve �ok sevilen bir futbolcu al�nmas� gerekir. "Pascal Nouma'n�n ruhunu getirece�im" demek, kendi ilkelerine ihanet etmek demektir. 3-4 futbolcu al�naca��na bir futbolcu al�ns�n. Mesela Luis Figo transfer edilsin.
4- Ge�en y�l, hakem seminerine Be- �ikta�l� hi�bir y�netici gitmedi. Antren�r seminerine Lucescu kat�lmad�. Ya�anan Ulusoy-Bilgili sava�� sonunda Be�ikta�'�n elinden �ampiyonlu�u al�narak,
F.Bah�e'ye verildi. Say�n Demir�ren'in �arts�z ve ko�ulsuz �imdiden Haluk Ulusoy'a vermesi b�y�k hata.
Son s�z: Say�n Demir�ren, trib�n r�zgar�yla ba�kan oldu. Ama ona kendi s�z�n� hat�rlat�yorum: Trib�n ate� gibidir, herkesi yakar. 
