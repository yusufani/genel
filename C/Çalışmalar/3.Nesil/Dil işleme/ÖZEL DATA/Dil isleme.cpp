#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <conio.h>
	FILE *dosya,*yenidosya,*buyukharf,*kok,*yapimeki,*cekimeki,*noktalama;
	const char *const kirmizi = "\033[0;40;31m";
	const char *const mavi = "\033[0;40;34m";
	const char *const normal = "\033[0m";
	int buyuk_harf_kontrolu(char[30]);
	int kok_kontrolu(char [30],int *);
	int cekim_eki_kontrolu(char kelime[30],int r);
	int yapim_eki_kontrolu(char kelime[30],int r);
	void bosluk_koy();
	void yukleniyor(int );
	int main(){
		int z,j,i=0,flag,uzunluk,animasyon=0;
		FILE *dosya,*yenidosya,*buyukharf;
		char metin[2000];
		char kelime[30];
		void rewind (FILE *dosya);
		dosya=fopen("Lutfen Metni giriniz.txt","r");
		yenidosya=fopen("Duzeltilmis metin.txt","w");
		if(dosya!=NULL){
			while(!feof(dosya)){
				yukleniyor(animasyon);//isleniyor animasyonu icin
				animasyon++;
				fgets(metin,2000,dosya);
				uzunluk=strlen(metin);
				i=0;
				while(i!=uzunluk){
					z=0;
					for(j=0;j<30;j++){
						kelime[z]=metin[i+j];
						z++;
					}
			
			   		 flag=0;
					if(flag==0) flag=kok_kontrolu(kelime,&i);
					if (flag==0){
						yenidosya=fopen("Duzeltilmis metin.txt","a");
						fprintf(yenidosya,"%c",kelime[0]);
						fclose(yenidosya);
						i++;
					}
				}
			}
			system("CLS");
			printf("\n%sMetin basariyla duzeltilmistir.\nDuzeltilmis metin programi calistirdiginiz dizinde Duzeltilmis metin.txt dosyasidir.%s",mavi,normal);
			printf("\nProgrami Sonlandirmak icin herhangi bir tusu tuslayiniz");
			getch;
		}
	fclose(dosya);
	fclose(yenidosya);
}
void yukleniyor(int animasyon){
	system("CLS");
	printf("%s",kirmizi);
	if(animasyon%3==0) printf("Isleniyor...");
	if(animasyon%3==1) printf("Isleniyor..");
	if(animasyon%3==2) printf("Isleniyor.");
	
}
int buyuk_harf_kontrolu(char kelime[30]){
	buyukharf=fopen("buyuk harf.txt","r");
	int sonuc=0;
	char harf;
		while(!feof(buyukharf) && (sonuc==0)){
			harf=fgetc(buyukharf);
			if (harf==kelime[0]) sonuc=1;
		}
		void rewind (FILE *buyukharf);
	fclose(buyukharf);
	return sonuc;
}
int noktalama_kontrolu(char kelime[30]){
	noktalama=fopen("noktalama isaretleri.txt","r");
	int sonuc=0;
	char harf;
		while(!feof(noktalama) && (sonuc==0)){
			harf=fgetc(noktalama);
			if (harf==kelime[0]) sonuc=1;
		}
		void rewind (FILE *noktalama);
	fclose(noktalama);
	return sonuc;
}
int kok_kontrolu(char kelime[30],int *r){
	int uzunluk2,t,sonuc,max=0,a,b;
	char anlamlikok[30];
	int dogruluk=0;
	sonuc=1;
	kok=fopen("anlamli kok data2.txt","r");
		while(!feof(kok)){
			fgets(anlamlikok,30,kok);
			t=0;
			sonuc=1;
			uzunluk2=strlen(anlamlikok);
			if(uzunluk2>max){
				while((sonuc==1) && (t<uzunluk2-1)){
					if(anlamlikok[t]!=kelime[t]) sonuc=0;
					t++;
				}
				if((sonuc==1) && (anlamlikok[0]!='�')) {
					max=uzunluk2-1;
					dogruluk=1;
				}	
			}
		}
		if (dogruluk==1){
			*r=*r+max;	
			a=yapim_eki_kontrolu(kelime,max);
			b=cekim_eki_kontrolu(kelime,max);
			*r=(*r)+a;
			max=max+a;
			*r=(*r)+b;
			max=max+b;
			yenidosya=fopen("Duzeltilmis metin.txt","a");
			for(t=0;t<max;t++)	fprintf(yenidosya,"%c",kelime[t]);
			fprintf(yenidosya," ");
			fclose(yenidosya);	
		}	
	if((buyuk_harf_kontrolu(kelime)==1) || (noktalama_kontrolu(kelime)==1)) bosluk_koy();
	fclose(kok);
	return dogruluk;
}
void bosluk_koy(){
	yenidosya=fopen("Duzeltilmis metin.txt","a");
	fprintf(yenidosya," ");
	fclose(yenidosya);	
}
int yapim_eki_kontrolu(char kelime[30],int r){
	yapimeki=fopen("Yap�m Ekleri.txt","r");
	char yapimekleridata[7];
	int sonuc,dogruluk=0,uzunluk2,t,max=0;
	while(yapimekleridata[0]!='�'){
		fgets(yapimekleridata,7,yapimeki);
		t=0;
		sonuc=1;
		uzunluk2=strlen(yapimekleridata);
		if(uzunluk2>max){
			while((sonuc==1) && (t<uzunluk2-1)){
				if(yapimekleridata[t]!=kelime[t+r]) sonuc=0;
				t++;
			}
			if(sonuc==1) {
				max=uzunluk2-1;
				dogruluk=1;
			}	
		}
	}
	if (dogruluk==1) return max;
	else return 0;
	}
int cekim_eki_kontrolu(char kelime[30],int r){
	cekimeki=fopen("Cekim Ekleri.txt","r");
	char cekimekleridata[7];
	int sonuc,dogruluk=0,uzunluk2,t,max=0;
	while(cekimekleridata[0]!='�'){
		fgets(cekimekleridata,7,cekimeki);
		t=0;
		sonuc=1;
		uzunluk2=strlen(cekimekleridata);
		if(uzunluk2>max){
			while((sonuc==1) && (t<uzunluk2-1)){
				if(cekimekleridata[t]!=kelime[t+r]) sonuc=0;
				t++;
			}
			if(sonuc==1) {
				max=uzunluk2-1;
				dogruluk=1;
			}	
		}
	}
	if (dogruluk==1) return max;
	else return 0;
}
