An 18-year-old student at a state college on Lake Ontario died on Friday from a suspected case of bacterial meningitis. Separately, two students at Cornell University, a 21-year-old woman and a 19-year-old man, have been hospitalized in the span of less than a week with the condition, health officials said.

The three cases have prompted health officials in two counties to warn students at the colleges that they may need preventive medication, and left them wondering whether other cases have yet to come to light. If autopsy results confirm the suspected cause of death of the 18-year-old, it will mark at least the fourth recorded death from bacterial meningitis in New York State this year.

The student who died, Craig Schiesser, was a freshman at the SUNY College at Oswego, Deborah F. Stanley, the president of the school, said in a statement. She said Mr. Schiesser was found on the floor of his dorm room in Oneida Hall shortly after 9 a.m. Friday.

Although it was too early to confirm that Mr. Schiesser, who was from Westbury, in Nassau County, had meningitis, Ms. Stanley said state and Oswego County health officials were recommending that the university respond as if he did. As a result, the school sent out an alert to all students warning them that they should seek medical attention if they had had contact with Mr. Schiesser in the past 10 days.

The disease, an infection that attacks and inflames the outer membranes of the brain and spinal cord, typically kills a few hundred people nationwide each year.

Officials at Cornell, meanwhile, said on Friday that state and Tompkins County health officials were pursuing the possibility that the two cases detected there were directly related, though how they might be connected was not yet clear.

Both of those students, who were not identified, were in hospitals on Friday in stable condition, the man at Cayuga Medical Center at Ithaca and the woman at an undisclosed hospital close to her home in another part of the state.

Simeon Moss, a spokesman for Cornell, said the first case there was discovered on March 8, when the woman was hospitalized at Cayuga Medical Center with symptoms of the disease. On Thursday, the man arrived at the same hospital and was presumed to have meningococcal meningitis, he said.

As health officials investigated the two cases, they learned that the two students might have crossed paths at one or more of three recent parties in Ithaca.

�We know that certainly they were in contact with folks who were at the same parties,� Mr. Moss said.

Those parties all took place on or around the Cornell campus in early March. One was at 124 Catherine Street on March 6, another the same day at 118 Cook Street, and the third on March 8 at 306 Highland Road, at a Tau Epsilon Phi fraternity house. Students who were at those parties were told to get a preventive antibiotic at a medical clinic as soon as possible, Mr. Moss said.

The most common form of bacterial meningitis is usually transmitted through saliva and sneezing, and symptoms may include nausea, vomiting, confusion and sleepiness. It is rarely fatal.

