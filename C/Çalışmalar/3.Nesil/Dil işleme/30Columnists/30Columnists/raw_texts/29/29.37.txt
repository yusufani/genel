Like a cup of tea for a cough, a batch of ice for a sunburn may seem like the perfect remedy for millions of Americans who will spend a little too much time in the sun this summer. 


But many home remedies that seem like common sense are less than helpful, and the old ice-for-a-burn technique is no exception. It can help soothe some initial pain, but in the end it will slow the healing process. 

That has been borne out over the years in various studies of simple treatments for minor scalds and sunburns. In one randomized study by Danish researchers in 2002, 24 healthy volunteers were inflicted with first-degree burns and subjected to different treatments. Those who received a cooling treatment similar to ice did not experience reduced pain or inflammation compared with those who received a placebo treatment. 

In another study in the journal Burns in 1997, another team of scientists compared easing burns with ice cubes for 10 minutes with other remedies and found that ice caused ''the most severe damage.'' ''Using an ice cube immediately after injury,'' the authors added, ''is harmful in some instances.'' 

According to the Mayo Clinic, putting ice on a burn can cause frostbite and damage the skin. For better results, try running cool water over the area and taking a pain reliever. Then cover the area with gauze but no ointment. Most minor burns heal without further treatment, the clinic says. 

