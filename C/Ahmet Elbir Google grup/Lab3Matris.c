#include <stdio.h>
#define MAX 50
void rotateRight(int matrix[][MAX], int m, int n);
void matrisWrite(int matrix[][MAX], int m, int n);
void rotateDown(int matrix[][MAX], int m, int n);
int main(void){
	int m, n, i,j,k=0, matrix[MAX][MAX];
	printf("M ve N giriniz:");
	scanf("%d %d",&m,&n);

	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			matrix[i][j]=k++;
		}
	}
	printf("\nOrijinal Matris\n");
	matrisWrite(matrix,m,n);
	
	rotateRight(matrix,m,n);
	printf("\nSaga Mirror Sonrasi\n");
	matrisWrite(matrix,m,n);
	
	rotateDown(matrix,m,n);
	printf("\nAsagi Mirror Sonrasi\n");
	matrisWrite(matrix,m,n);
		
	return 0;
}

void matrisWrite(int matrix[][MAX], int m, int n){ //grup1 ve 2
	int i,j;
	for(i=0;i<m;i++)  {
		for(j=0;j<n;j++)  {
			printf("%4d",matrix[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void rotateRight(int matrix[][MAX], int m, int n){  //grup1
	int i,j,tmp;
	for(i=0;i<m;i++)  {
		for(j=0;j<n/2;j++)  {
			tmp=matrix[i][n-j-1];
			matrix[i][n-j-1]=matrix[i][j];
			matrix[i][j]=tmp;
		}
	}
}

void rotateDown(int matrix[][MAX], int m, int n){ // grup2
	int i,j,tmp;
	for(i=0;i<m/2;i++)  {
		for(j=0;j<n;j++)  {
			tmp=matrix[i][j];
			matrix[i][j]=matrix[m-1-i][j];
			matrix[m-1-i][j]=tmp;
		}
	}
}
