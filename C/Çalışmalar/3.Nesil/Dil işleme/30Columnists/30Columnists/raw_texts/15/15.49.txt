Just in time for opening day in North America, baseball is about to firm up its latest drug-testing policy � the one that strengthens year-round testing of your favorite superstars, and not a moment too soon. 


Baseball should name the new policy in honor of Jos� Canseco, the patron saint � you should pardon the expression � of baseball testing.

As soon as the lawyers are done, testers will theoretically be able to knock on the door of a Major League Baseball player enjoying a vacation in the Caribbean or Europe or Asia. 

As they grab for a towel or a bathrobe, the players can thank their friend Canseco for making it all possible. He did it for their own good, because he cares. 

Canseco has allowed ballplayers to experience the same loving attention Olympic athletes have been receiving for years: the unexpected visit from a doping agent. 

Lance Armstrong, the self-styled most-tested athlete in the world, became used to checking into a hotel and immediately hearing the rap on the door from the local testing agency. It was all part of the cycling game. Knock-knock. Who�s there? Dixie. Dixie who? Dixie cup.

And imagine the surprise of Tom McVay, a doping control officer for the United States Anti-Doping Agency, who recently recalled a surprise visit he paid to Tammy Thomas, a world-level cyclist, in March 2002.

She had shaving cream on the left side of her face, McVay testified, and was apparently in the act of shaving her excess facial hair. She then tested positive for illegal substances and was barred from competition. 

This is cheating at the top level of sport. As part of his public service, Canseco claimed in 2005 that most sluggers of his generation were, as the title of his first book put it, Juiced.

When he appeared in the now-famous Congressional subcommittee hearing on St. Patrick�s Day 2005, Canseco managed to make Mark McGwire, Sammy Sosa and Rafael Palmeiro shrivel up in front of the world. (Curt Schilling, who had been billed as a critic of steroids, professed to know nothing, nothing, and also fizzled, like a balloon with the hot air rushing out.) 

We all know that sequels never work. Now Canseco is back with another book, which could have been titled �Honey, I Shrunk the Kids� but instead is called �Vindicated,� which is not exactly the case. Canseco is still a cheat and a blowhard, but sometimes even cheats and blowhards, by accident, reveal the truth.


This new book doesn�t sound like much. The major revelation is that Alex Rodriguez, as a bachelor, may have pursued Canseco�s former wife. Around these parts, loose morals would qualify Rodriguez to be governor of a major northeastern state, at least for a while. 

Canseco says he told A-Rod about a guy who dealt in illegal steroids. Sorry, that�s not nearly good enough. Canseco also says he has no proof Roger Clemens bulked up illegally, but Clemens apparently liked to joke around about Vitamin B12 shots, the code name for bulking-up drugs. Sorry, that won�t do, either. 

Now we are about to upgrade the minimal out-of-season testing, which should be administered by an independent agency because, frankly, I wouldn�t trust baseball to supervise testing without warning its stars in advance.

Meantime, George J. Mitchell is right in advising baseball not to penalize the players who turned up in his investigation, which was about avoidance during the regime of Bud Selig and Donald Fehr, the Frick and Frack of baseball. 

It was up to baseball to have testing in place, and baseball did not. If prosecutors can get Clemens and Barry Bonds for perjury, more power to them. 

I take my perspective from that wonderful sport of cycling. I saw Armstrong beat everybody on the steepest mountains and tightest time trials in France. I know people who are convinced Armstrong was dirty, but when cycling was forced to get serious, it never implicated Armstrong while busting many other stars. C�est la vie. 

Bonds may have tested positive for amphetamines and steroids at one point or another, but baseball was not ready. Now it�s time to move on, albeit with a vengeance.


My friend Scott Ostler of The San Francisco Chronicle recently joined a preseason tour of the Giants� ballpark and found virtually no trace of Bonds and his home run records, in either the clubhouse or the stands. And except for a few collector jerseys, Bonds has vanished from the Giants� store, like one of those generals who used to vanish from the May Day photographs in Red Square. Barry Lamar Bonds has become a nonperson. 

The Giants used him to sell tickets the last few years, and now he is gone. Can�t even get a job offer. That is not collusion. That is common sense. 

It�s time for baseball to move on. But let us honor Canseco for his contribution to the national sport. By blabbing for dollars, Canseco has brought in a brave new world in which an agent can knock on the door of a vacation retreat in the middle of the winter, and tell some $15-million-a-year superstar the code: �Pssst. Jos� sent me.�

