WHEN Liverpool buried their dead following the Hillsborough disaster, two decades ago this year, Kenny Dalgish went to as many of the funerals as possible.

He and his wife, Marina, also gave practical help to bereaved families. The emotional and psychological toll must have been almost too much to bear.

According to Peter Beardsley, in a revealing interview with The Observer, the experience caught up with Dalglish two years later when, drained and exhausted, he quit as Liverpool manager.

Contrast that genuine empathy with fans to last year�s interview on Sky in which Tom Hicks sat by his fireside in Dallas, wearing a Liverpool shirt, sipping from a Liverpool mug, and taking us all for mugs as he feigned a deep affection for the club he is now desperately trying to flog.

Hicks and co-owner George Gillett have to sell up by the summer. It is about the only thing they agree upon.

They borrowed the money to buy the club and the loans are due to be repaid in July. So, just as before the Americans moseyed on down to Anfield two years ago, a proud old club is being touted for sale around the planet.

That is the big story just now at Liverpool.

It�s not Rafa Benitez demanding to be put in sole charge of transfers and then demonstrating his expertise by isolating Robbie Keane (for whom he paid �2million just six months ago).

It�s not the inability to beat neighbours Everton, nor the suspicion that Sir Alex Ferguson was right when he speculated that Benitez�s boys might blow their best title chance since Dalglish left.

    

The story is that a club whose average attendance has not dipped below 42,000 for 20 years must be sold abroad for a second time to anyone who will have it.

Yet we take that for granted, because the sale of our clubs has become commonplace and because we understand that our top clubs need foreign billions to compete with each other.

My objection is not that the new owners, if any are found, will not be British. Our game has had too many homegrown crooks and charlatans to be as jingoistic as that. 

No, the disappointment must be that any buyer will read about the club�s history but have not lived it, and can�t understand what it means to those who pay to watch matches.

