#include <stdio.h>

//12011083
//Atilhan Emre Dursunoglu

struct Student
{
    char name[16];
    char sname[16];
    int grade;
};

void add(FILE **fp, struct Student **list1, int no);

void view(FILE **fp, int no);

int main(void)
{
    FILE *fp;
    struct Student *list1;

    int no, option;

    while(option != 3)
    {
        printf("Please select an option");
        printf("\n1)Add Student\n2)View Student\n3)Exit\n");
        scanf("%d", &option);
        if (option == 1)
        {
           printf("How many student?\t");
           scanf("%d", &no);
           add(&fp, &list1, no);
        }
        else if (option == 2)
        {
            view(&fp, no);
        }
    }



}

void add(FILE **fp, struct Student **list1, int no)
{
    *list1 = (struct Student*) malloc(no * sizeof(struct Student));

    int i;

    for (i = 0; i < no; i++)
    {
        printf("name, surname, grade, in order\n");
        scanf("%s", &(*list1)[i].name);
        scanf("%s", &(*list1)[i].sname);
        scanf("%d", &(*list1)[i].grade);
    }

    *fp = fopen("data.txt", "wb");

    for(i = 0; i < no; i++)
    {
        fprintf(*fp, "%s\t%s\t%d\n", (*list1)[i].name,(*list1)[i].sname,(*list1)[i].grade);
    }

    fclose(*fp);
}

void view(FILE **fp, int no)
{
    char name[16], sname[16];
    int grade, totalGrade = 0, i;
    *fp = fopen("data.txt", "rb");
    for (i = 0; i < no; i++)
    {
        fscanf(*fp, "%s\t%s\t%d\n", &name, &sname, &grade);
        printf("%s\t%s\t%d\n", name, sname, grade);
        totalGrade += grade;
    }
    printf("Average grade is %d\n", totalGrade/no);
    fclose(*fp);
}
