Hesaps�z kartlar

�� ma�ta d�rt puan toplayarak Schalke kar��s�na ��kan F.Bah�e puan almak niyetindeydi. �stanbul'daki 3-3'l�k m�cadele F.Bah�eli futbolcular�n " her tak�m� yenebilece�i inanc� "n� g��lendirmi�ti. Bu inan� F.Bah�e tak�m�n�n bir numaral� moral kayna�� idi.
Bu duygularla Alex'siz ma�a ba�layan F.Bah�e'yi seyircisi de yaln�z b�rakmad�.
�lk dakikalarda Schalke ak�nlar�n� kendi ceza sahas�nda kar��layan F.Bah�e, Alman tak�mlar�na kar�� �anss�zl���n� yenme �abas�ndayd�.
Volkan'�n harika kurtar��lar� ikinci bir moral kayna�� oluyordu. Ama paslardaki isabetsizlik ve top kay�plar� d�zeni bozan �nemli etkenler oldu. 22. dakikada �nder gol� atsayd� F.Bah�e bu moralin kar��l���n� alm�� olacakt�. 31. dakikada gelen gole uzanamayan Volkan �ok hatal� olmasa da 40. dakikada k�rm�z� kartla at�lan Luciano'nun pozisyonu l�z�ms�zdu. 55. dakikada Aurelio'nun da at�lmas�yla ��phesiz umutlar k�r�l�yor ve bu sorumsuzlu�un ba� akt�rleri de hoca ve at�lan futbolcular oluyordu. 
Schalke �stanbul'daki ba�ar�l� performans�n� evindeki oyununa yans�tamad�. Fener'in umutlar�n� y�kan bu hesaps�z k�rm�z� kartlar oluyordu. Ve 2. gol futbolda hesap edilemeyen ger�e�i bir kez daha ispatl�yor, F.Bah�e de kaderine boyun e�iyordu.
Sonu�ta umut dolu ba�lay�p k�rm�z� kartlar nedeniyle h�sran ile sonu�lanan Schalke ma��n�n bedelini kim �demeli kestiremiyorum. Daum mu? Luciano mu? Aurelio mu? 
