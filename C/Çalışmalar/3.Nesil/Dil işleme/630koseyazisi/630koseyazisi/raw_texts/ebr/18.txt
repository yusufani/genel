Hayal et sevgilim

Magazin programlar�ndan biri, Recep B�lb�lses�in �nihayet� murad�na erdi�ini m�jdeliyor. Mahsun K�rm�z�g�l, �eker Bayram� program� s�ras�nda birka� �ark� s�ylemesi i�in sahneye ��kmas�na izin mi vermi� ne...

Do�an g�r�n�ml� �ahin, pardon, �ahin g�r�n�ml� kuzgun, yok, bu da olmad�, kuzgun g�r�n�ml� kuzgun B�lb�lses de f�rsat bu f�rsat, f�rlay�p kapm�� mikrofonu, �ak�yor.

Tam bu noktada, arada ka��rm�� olanlar�n�z var m�d�r diye, mevzunun dizginine as�lmam gerekti�ini fark ettim. 

Do�ru ya, �Recep B�lb�lses kim?� diye sorabilirsiniz. Adam durup durup karabatak gibi kar��m�za ��kt��� i�in biliyorsan�z da unutmu� olabilirsiniz...

Recep B�lb�lses hani... Be�-alt� senedir, ���hret olacam da olacam� diye, durup durup kendini bir vesileyle kameralar�n �n�ne atar... H��k�rarak a�lar, muhtelif ��hretlerimize �Niye beni de ��hret yapm�yorsun?� diye posta mosta koyar; yapar i�te bir �eyler...

Ben art�k bu 30 saniyelik ��hret i�in k���n� y�rtan insanlara ak�l erdirmeye �al��maktan vazge�mi�, bunun herkesin en do�al hakk� oldu�unu kabul etmi�im...

Hayat bu hayatsa, evet abi, �B�lb�lses�lerin varl���n� g�z�m�ze sokma hakk� engellenemez!� diye slogan atacak k�vama gelmi�im... Pes etmi�im... 

Daha do�rusu etmi�tim... Ki... ��kmayan candan �mit kesilmezmi� azizim...

Bir r�portaj okudum, havam de�i�ti. Sal� g�n� Kelebek�e man�et olan Mevl�t Tezel r�portaj�n� okudunuz mu? �ahaneydi...

Bir s�redir internette �ehir efsanesi gibi dolanan Hayalet Sevgilim �ark�s�n�n sahibi, Ko� �niversitesi Hukuk Fak�ltesi ��rencisi �rem Ya�c��y� bulup konu�mu� Mevl�t.

�ark�n�n esas isminin Hayal Et Sevgilim oldu�unu ve �ark�yla ilgili o melodramatik hik�yelerin hi�birinin do�ru olmad���n�, bil�kis �ark�y� iki saatte ka��na ka��na yazd���n� anlatan �rem Ya�c�, kendini anlat�yor: 

�A�k ac�s� �ekip a�layan birisi de�ilim. Gayet mutluyum. Ya�amay� seviyorum, yurtta kal�yorum. Bilirsiniz ��renci hayat�n�, i�te ona tak�l�p gidiyorum.�

�� sene kl�sik gitar e�itimi alm��, yedi senedir de �al�yormu�. Ama yap�mc�lar�n t�m �srarlar�na ra�men, alb�m yapmak filan istemiyormu�: �Hem ben hukuk�u olaca��m can�m, herkes �nl� olmak zorunda m�? �u anda m�zi�e dair hi�bir plan�m yok. Belki �ok ilerde �lhan �e�en gibi avukatl���m� yap�p zevk i�in m�zisyen olabilirim.�

G�zel karde�im, seni �pebilir miyim?

Kim ne derse desin, can�n ne istiyorsa onu yap e mi?.. Yeter ki �imdiki akl�na ve sindirim sistemine mugayet ol. Ortal�k i�g�r� yoksunu, �uursuz haz�ms�zdan ge�ilmiyor zira ve onlar�n haz�ms�zl���, bizde gaz yap�yor.

�imdiden bir m�vekkilin ve bir dinleyicin var; katil olur da mahkemelere d��ersem, kendimi sana emanet edece�im, ilerde alb�m yaparsan da ilk alan ben olaca��m, haberin olsun...

�ark�n�n ad� Hayal Et Sevgilim ya... Bende uyand�rd��� duygu da Martin Luther King�in o me�hur s�ylevindeki gibi bir �ey: �Bir hayalim var!� diye ba��rarak sokaklara f�rlamak istiyorum. �rem gibi milyonlarcas�n� hayal ediyorum. Hepsini de teker teker �pmek istiyorum.

Evet abi, delirdim... Ama yani... ��yle bir hayal et sevgilim...