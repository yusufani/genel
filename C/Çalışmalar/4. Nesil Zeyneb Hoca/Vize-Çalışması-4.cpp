/**
 * @author Yusuf Anı
 * @email yusufani8@gmail.com
 * @create date 2018-11-19 15:25:11
 * @modify date 2018-11-19 15:25:11
 * @desc [malloc training]
*/
#include <stdlib.h>
#include <stdio.h>
int main ( ) {
    int *p;
    p=(int *) malloc(3*12);
    if (p== NULL) printf ("oLUŞTURULAMADI");
    else printf("Basarili");
}