2 milyon T�rk��n se�imi 
BULGAR�STAN�da genel ge�imler yar�n yap�l�yor... 22 partinin afi�leri, T�rkiye gibi olmasa da belirli cadde ve sokaklarda dikkat �ekiyor; ��irkin� s�slemelere rastlanm�yor. 

Her�ey televizyonlar�n egemenli�inde; tart��malar centilmence ve d�zeyli yap�l�yor. Hak ve �zg�rl�kler Hareketi�nin Genel Ba�kan� Ahmet Do�an d�n gece T�rkiye�nin Okan Bay�lgen�i say�lan Slavi Trifonof�un program�na ��kacakt�. (Ahmet Do�an, �Erdo�an ve baz� bakanlar�n Bulgaristan�da kat�ld��� �i�e Cam��n temel atma ve Hamzabeyli g�mr�k kap�s�n�n a��l�� t�renlerine neden kat�lmaz, gibi sorulara �ok yerde rastlan�yor nedense.)

Do�an��n d�n bas�nda yer alan bir demecinde, iktidar olduklar� NDSV (�ar Simeon�un partisi) ve DPS (kendi partisi) koalisyonu i�in ��yle diyor:

��ktidar�m�z�n bitti�i d�nemde sayg� vard�, sevgi yoktu.�

H�k�met orta��, oldu�u NDSV�nin lideri Ba�bakan Simeon�u bir anlamda �d��layan� Do�an��n bu s�zlerinden bir �ok �itiraf� ��kar�labilir. Yeni bir sayfa a�arak, muhtemel Sosyalist Parti iktidar�na �ortakl�k� i�in ye�il ���k m� yak�yor acaba? �lk ba�larda partisini �liberal demokrat� tan�t�rken, eski kom�nistlerin a��rl�klar�n� ortaya ��kmas� �zerine politikalar�nda rotay� de�i�tirmek istedi�i anla��l�yor.

Se�im gezilerinde helikopter kullanan Do�an �unu da s�yl�yor:

�Sa� ve sol partilerle i�birli�i yapabiliriz, bize ra�men ne sa� ne de sol partiler h�k�met kuramaz�

Bunlar �ok iddial� s�zler... Silistre�nin Dulova kasabas�ndan T�rk k�kenli bir i�adam�na

Ahmet Do�an��n g�c�n� koruyup koruyamad���n� soruyoruz. Yan�t� ��yle oluyor:

�Acaba g�veni ve itibar� kald� m�? H�H 1990�lar�n ba��nda kuruldu�u ilk y�l 418 bin oy alm��t�. 2001 se�imlerinde bu say� 330 bine, ge�en y�lki yerel se�imlerinde 236 bine neden d��t�?�

H�H��N ZORLU�U

Ahmet Do�an bu ger�e�i kabul ediyor ancak %4 baraj�n� yeni ve �gen� taraftarlar��yla %10�a y�kseltmeyi hedef ald�klar�n� s�yl�yor.

Bulgaristan siyasetinde son g�nlerde, eski Ba�bakan Kostov�un partisinden ayr�lanlar�n kurdu�u ve Bulgar milliyet�ili�i �zerine s�ylemleri ile bir anda g�ndeme oturan ATAKA partisinin oy oran�n�n anketlerde 5.8�e kadar ��kmas� hem sosyalistleri, hem de sa� partileri tedirgin ediyor. �� ay �nce kurulan ATAKA�daki bu y�kseli�, �T�rklerin oylar�n�n da H�H�e gitmesini sa�layabilmek amac�yla yap�lm�� organize bir d���nce mi�, sorusunu akla getirmiyor de�il. Bulgaristan�daki T�rklerin, H�H�e deste�inin bir �l��de azalmas� kar��s�nda T�rkiye�deki se�menlerin otob�slerle buraya getirilmesi, bu yoldaki yorumlar� g��lendiriyor.

Bir not... Ahmet Do�an��n gir�imiyle ��kar�lan bir yasa ile T�rkiye�deki ��ifte vatanda�lar�n� aday olmalar� engellenmi�ti. Bu durumda T�rkiye�deki se�menler sadece oy kullanabiliyor. 

E�itim�de siyasi rant

SA�LIK Bakanl���, Sa�l�k Meslek Liseleri�nin ders kitaplar�n� kar��lamak i�in, Sa�l�k Bakanl��� Yay�n Kururu Karar�yla 100 civar�nda ders kitab� onay� verildi. Bunlar�n 20�ye yak�n�n� da AKP h�k�meti Sa�l�k Bakan� verdi. Sa�l�k Bakanl����na ba�l� Sa�l�k Meslek Liseleri, Milli E�itim Bakanl����na (16.6.2004) tarihli protokolle devredilince, Sa�l�k Bakanl��� Yay�n Kurulu karar�yla 100 civar�nda ders kitab� hi�e say�larak, e�itlik ilkesine uyulmadan kendi yanda�lar�na sipari� usul� ile yazd�r�larak ve uzman taraf�ndan incelettirilmeden denetimsiz olarak acele ile yap�larak ders kitaplar�n� devletle�tirmi�tir. Bu kitaplar�n devletin paras�yla bast�r�larak ve b�ylece y�ksek fiyatla mal edilip zarar�na ��rencilere sat�lmas� (2 YTL) kararla�t�r�ld��� s�ylenmektedir. B�ylece, devletin paras�yla siyasi rant elde edilmektedir. Ayr�ca mahkeme karar�yla sivil yazarlar haklar�n� alsalar dahi, Milli E�itim�in zarar�na satacaklar� kitaplarla rekabet edemeyeceklerdir. Buna da devlet eliyle uygulanan haks�z rekabet denir. ��in siyasi �ovu da budur.

Sa�l�k Bakanl����n�n verdi�i ders kitab� hakk�n�, Milli E�itim Talim Terbiye Kurulu yetkilileri yok say�yor, Sa�l�k Meslek Liseleri�nde okutulmamas� y�n�nde ambargo uygulan�yor.

H�seyin DURSUN

Deliormanl� Ali�den anlaml� mesajlar

�DEL�ORMANLI Ali�, se�imleri ��yle de�erlendiriyor:

��lk �nce �unu s�yl�yorum; 1980�lerin sonlar�nda itiltik, kak�ld�k, s�r�ld�k. Ama Jivkov�u devirdik, Bulgar halk�yla elele vererek totaliter rejimi yok ettik. Berlin duvar�n�n y�k�lmas�na �nc�l�k ettik. Kan�m�z, can�m�z pahas�na demokrasiyi getirdik Bulgaristan�a...

Ey soyda�lar,

Sand��a gidin, demokrasiyi koruyun. AB vatanda�l���n�z� onaylat�n. G��l� Bulgaristan�a sahip ��kal�m, Balkanlar�daki varl���m�z� tescil ettirelim. Politikay� �ahsi ama�lar� ve �konforizm� i�in kullananlara ge�it vermeyelim. Sa�duyulu Bulgar karde�lerimizle etnik bir yap�lanma d���nda, girece�imiz AB�nin s�rt�nda kambur olmayacak, yeni ve g��l� Bulgaristan��n in�aas�na katk�da bulunal�m. Ve T�rkiye�nin en yak�n kom�usu, AB i�indeki en g�venilir savunucusu olal�m. AB fonlar�ndan adaletli bir �ekilde aktar�lacak, b�lgeler aras�ndaki geri kalm��l��� ortadan kald�racak olu�umlar�n �n�n� a�al�m. Niye burada T�rk k�kenli Bulgar vatanda�lar�n�, yani bizleri cehaletten kurtaracak okullar�m�z, televizyonumuz, radyomuz ve gazetemiz olmas�n?�

�zel Halk Otob�s� �of�rlerine e�itim

EL�MDE bir kitap��k var. �ETT, �stanbul Otob�s �zel Halk Otob�s� Sahipleri ve ��letmecileri Esnaf Odas� ve �ZULA� Toplu Ta��m San. ve Tic. A.�. taraf�ndan haz�rlanm��. Kitap����n �st�nde �Personel E�itim Projesi� yaz�yor. Mart 2005�te bas�lm��. Y�l i�inde 350 �HO �of�r�ne �bilgilendirme ve bilin�lendirme semineri� verilecekmi�.

Bu seminerde; �of�rlere durak harici yolcu al�nmayaca��, seyir halinde cep telefonu ile konu�ulmayaca��, sigara i�ilemeyece�i... Muavinlere her yolcuya bilet kesmek zorunda olduklar�, yolcular�n suratlar�na baka baka tespih �ekmelerinin ho� bir davran�� olmad��� da anlat�lacak m�?

E�er seminer program�nda bunlar yoksa derhal eklenmelidir. �zel Halk Otob�sleri, �stanbul trafi�inde bir yarad�r. �ETT taraf�ndan ciddi denetimlerden ge�irilmelidir.

Sevimg�l CANTA�KIN

Devlete zarar

KIRIKKALE�de DSP-MHP h�k�meti d�neminde in�aat� tamamlanm�� hekimevi kaderine terk edildi. �l Sa�l�k M�d�rl����n�n yak�n�nda bulunan geni� alanda hekimevi ve at�l durumda ��r�meye y�z tutmu� 5-6 tane dubleks lojman var. Hepimizin paras�yla yap�lm�� binalar�n durumunu ��renmek i�in �almad�k kap� b�rakmad�k ama yetkililere ula�mak ne m�mk�n. Kullan�ma a��lmayacaksa ni�in yap�ld�. ��imiz s�zlayarak binalar�n ��r�mesini izliyoruz.

K.C

G�N�N S�Z�

�Kaybedilmi� g�nlerin en k�t�s�, bir defac�k daha olsun g�lmeden ge�ilenidir.�

(Chamfort)

MESAJ PANOSU

D�NK� �Hangi OGS� yaz�m�za Vak�fbank �u a��klamay� g�nderdi: �Karayollar� Genel M�d�rl��� ve bankam�z aras�nda bir i� birli�i olmay�p s�z konusu OGS hizmeti bankam�z taraf�ndan verilmemektedir. Vak�fbank��n OGS y�kleme yetkisi yoktur. 

DALYAN Belediye Ba�kan� Suat Tufan�dan... �6. Caretta Caretta Festivali� 27 Haziran- 2 Temmuz tarihleri aras�nda yap�l�yor. Festivalde Ukrayna, G�rcistan, Bursa Nil�fer Belediyesi, Yata�an Halk Oyunlar��yla birlikte Suat, S�mer Ezg�, Murat G��ebakan ve �brahim Tatl�ses halk konserleri, y�zme, ya�l� kaz�k, kano yar��lar�, bal�k a�� �rme ve �amur G�zeli yar��mas� yap�lacak.
