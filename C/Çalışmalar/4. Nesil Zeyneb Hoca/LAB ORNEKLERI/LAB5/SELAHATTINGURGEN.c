#include<stdio.h>
#include<stdlib.h>
typedef struct ogr
{
    int not;
    struct ogr *next;
} Ogr;
typedef struct list
{
    Ogr* first;
    int ogrSayisi;
} List;
void menuGoster();
void hazirla(List* list);
void ekle(List *list,int not);
void yazdir(List *list);
float ortHesapla(List *list);
int main(int argc,char *argv[])
{
    List list;
    hazirla(&list);
    int not,secim;
    float ortalama;
    while(1)
    {
        menuGoster();
        scanf("%d",&secim);
        switch(secim)
        {
        case 0:
            printf("\nProgramdan cikiliyor\n");
            exit(EXIT_SUCCESS);
            break;
        case 1:
            yazdir(&list);
            break;
        case 2:
            scanf("%d",&not);
            ekle(&list,not);
            break;
        case 3:
            yazdir(&list);
            ortalama=ortHesapla(&list);
            printf("\nORTALAMA: %.2f \n",ortalama);
            break;
        default:
            printf("Gecersiz secim.Lutfen gecerli bir sayi giriniz.\n");
            break;

        }

    }

    return 0;
}
void menuGoster()
{
    printf("\n---------\n  [MENU]  \n---------\n");
    printf("1.Listeyi yazdir\n2.Listeye Ogrenci Ekle\n3.Ortalamayi Goster\n0.CIKIS\n");
    printf("Seciminizi giriniz\n");
}
void hazirla(List *list)
{
    list->first=(Ogr*) malloc(sizeof(Ogr));
    list->first->next=NULL;
    list->ogrSayisi=0;
}
void ekle(List *list,int not)
{
    Ogr* newOgr=(Ogr*) malloc(sizeof(Ogr));
    newOgr->not=not;
    if(list->ogrSayisi==0)
    {
        list->first->next=newOgr;
    }
    else
    {
        Ogr* ptr=list->first;
        int i=0;
        while(i<list->ogrSayisi)
        {
            ptr=ptr->next;
            i++;
        }
        ptr->next=newOgr;
    }
    newOgr->next=NULL;
    list->ogrSayisi++;
}
void yazdir(List *list)
{
    printf("\nListedeki Ogrenci sayisi: %d\n",list->ogrSayisi);
    Ogr* ptr=list->first;
    int i=1;
    while(ptr->next != NULL)
    {
        printf("%d . Ogrenci : %d \n",i,ptr->next->not);
        ptr=ptr->next;
        i++;
    }

}
float ortHesapla(List *list)
{
    float ort;
    int toplam=0,k,i=1;
    Ogr* ptr=list->first;
    k=list->ogrSayisi;

    while(ptr->next != NULL)
    {
        toplam+=ptr->next->not;
        i++;
        ptr=ptr->next;
    }

    ort=(toplam*1.0)/k;


    return ort;

}


