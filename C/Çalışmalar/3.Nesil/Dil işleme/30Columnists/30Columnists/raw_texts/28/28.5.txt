JUST when you thought the fans could not �become any more �preposterous, buffoons at Aston �Villa and Arsenal booed their teams for having the temerity not to win.

Both drew at home and left the field to catcalls from fans vying for the title of the most ignorant ingrates ever to make fools of themselves. The idiocy of booing Villa is so self-evident I imagine those who did have woken every day since thinking: �I must get my brain checked.�

Villa have won nothing significant for 27 years but this season have gatecrashed the top four. Nobody with a fully-functioning bonce would jeer a temporary setback of conceding two goals and two points to a Stoke side fighting for their Premier League lives.

Meanwhile, at Arsenal, supporters so sated on success they take it for granted have rounded on the man who provided the football on which they have feasted for 12 seasons.

Every club in the land � perhaps in the world � has so-called supporters whose natural disposition is to complain. But calling for Arsene Wenger�s head is spectacularly barmy. By the standards Wenger set, his team are faltering and his reluctance to sign a couple of physical midfielders has ushered in the possibility of not qualifying for the Champions League. That is precisely why Arsenal will need Wenger�s unique qualities more than ever.

A quarter of the posh apartments built at Highbury remain unsold and so Arsenal are trying to reschedule repayments of the massive loans used to build the Emirates. The plan was for profits from Highbury�s redevelopment to feed into the football but that will not happen any time soon.

If they lose Champions League income as well, Arsenal will not have a war chest of cash to spend on big-name, big-fee players for some seasons. So they will need a manager who can scour the world for unpolished diamonds then burnish them. Nobody alive does it better than Wenger.

Emmanuel Petit was an unremarkable centre-back. Wenger made him a World Cup-winning midfielder. Thierry Henry an unfancied winger. Wenger made him the best striker on the planet. Cesc Fabregas was a precocious kid at Barcelona. Wenger saw the player he would become.

    

He has got some things wrong but so will the next bloke. The boneheads who booed should pray the next bloke is not needed for several years.

