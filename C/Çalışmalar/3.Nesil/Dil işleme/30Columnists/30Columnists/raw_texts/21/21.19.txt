The apparent green shoots of an uptick in house prices reported by the Nationwide Building Society earlier this week were trampled underfoot yesterday by Halifax's rival index, which showed the exact opposite � namely that prices continued to fall last month. The juxtaposition of the two indices reverses that of a month ago, when it was the Halifax saying that prices were recovering and Nationwide that they were still on a downward trajectory. 


The dichotomy is easily explained. The housing market is so illiquid right now that a relatively small number of transactions can easily distort the monthly figures one way or the other. 

So which way is the underlying market heading: up or down? The betting has to be that it is still the latter. Using the Halifax index, UK house prices have fallen a little more than 20 per cent in nominal terms since their 2007 peak. This is not so far off the peak-to-trough fall of the early 1990s, so on past precedent the market should be bottoming out soon. 

Unfortunately, US experience points to more substantial falls to come. The housing correction started much earlier in the US than here, but still, and despite zero interest rates, there is no sign of the crash coming to an end. The cost of a mortgage is much lower than it was back in the early 1990s, but the size of mortgage debt relative to equity is much higher. 

What's more, in a deflationary environment the size of that debt will begin to inflate relative to earnings. One of the reasons interest rates are so low is that assets such as houses are depreciating in value. If house prices are rising, you are forced to pay a price for your gains. When they are falling, banks are grateful just for the repayment of principal. What you gain on the cost of your mortgage you more than lose on the size of your debt, which is inflating relative to your equity and income. 

For years, the dynamic has worked like this. Say you spent �100,000 on a house using �50,000 of mortgage finance and �50,000 of equity. By the time you came to sell the house, the price might have doubled to �200,000. If you kept the same proportion of debt to equity, you would then have �300,000 to spend on your next house. 

That process has now gone into reverse. If house prices fall by 25 per cent in value rather than double, your equity would be worth just �25,000 but you would still have the same quantity of debt. Assuming you kept the same debt-to-equity ratio for your next purchase, the maximum you would pay would therefore be �50,000. 

OK, so in practice, the housing market is quite unlikely to conform to such an artificial model, but what is true is that once a deflationary debt spiral takes hold, it's very hard to get rid of. The process of asset depreciation becomes self-feeding and the debt burden assumes ever more monstrous proportions. 

Policy is, of course, wholly fixated on trying to avoid just such an outcome, but it hasn't succeeded in reversing the trend yet, and if it works at all, it may take a while. In the meantime, unemployment is rising fast, prompting growing numbers of repossessions and forced sales. That's hardly a conducive backdrop to a revival in the housing market. 

