Nobody knows how the Dodgers and the Phillies will do in this series, but this much is certain: the teams are evenly matched in 60-something managers who have been some places and learned some things.

Before the Phillies beat the Dodgers, 3-2, in Thursday night�s opening game of the National League Championship Series, Joe Torre was talking about being an immature 28-year-old star, traded to the Cardinals, and watching Red Schoendienst, a player�s manager. That, Torre said, was when he began to wise up. 

Everybody knows Joe Torre, now 68, for winning four World Series titles as manager of the Yankees, but fewer people know Charlie Manuel, 64, who was never a star in his homeland but learned about life from sources as diverse as Billy Martin and Walter Alston and Japanese baseball. Manuel, like Torre, is a cancer survivor, and he has also come back from two mild heart attacks and colon surgery, managing for a while in Cleveland with a colostomy bag under his uniform. 

Manuel projects the sense that he is too old to take it all that seriously � except when it needs to be taken seriously, like when he went nose to nose in the dugout with Brett Myers, who had shown up Manuel on the mound during a game this season.

That outburst by Manuel was a touch of blunt Appalachian justice, stemming from his birthplace in �West By God Virginia,� as people proudly say, and his youth in the neighboring hills of southwestern Virginia. Appalachia was even more of a separate region back then, before cable and interstates, but the World Series did come through the mountains, loud and clear. 

The other day, Manuel was talking about Gil Hodges � another son of the coal fields, from southern Indiana � who had a ghastly World Series in 1952 against the Yankees. 

�I remember when I was a kid, I used to be a big Dodger fan,� Manuel said in his news conference, which can sometimes be as pleasant as sitting around a general store. 

�If I�m not mistaken,� Manuel continued, �you go back, I don�t know what year it was, Gil Hodges, he had a big season, and I want to say he went like 1 for 18 or something in the World Series, and I thought I was going to die. I couldn�t believe that Gil Hodges went 1 for 18. When I think about it, and once I became a major league player or also just a professional player, 1 for 18 is pretty easy to get sometimes. I mean, you can get there real quick.� 

The reporters laughed at his delivery, but he made the point, even if he underestimated the misery of Hodges, who went 0 for 21 in that lost World Series. 

Then Manuel added a life lesson: �If you panic, you can dig a bigger hole for yourself. It�s an every-day game, and you go out there and perform every day.�

He never got a chance to perform every day in the majors. He came up with Minnesota, observing the intense Martin, and then he mostly sat for the Dodgers, who were managed by Alston, out of southern Ohio, and not exactly an extrovert.

But one day in the Dodgertown cafeteria, Alston spotted Manuel with grits on his plate � the staff figured a guy from West Virginia had to like grits, which Manuel did not. Alston sat down with the guy with grits on his breakfast plate and talked to him. Talked to him in the dugout. Told him he was going to be a manager someday. Sought him out in coffee shops around the league. Surreptitiously grabbed the check. 

Does Manuel grab the check today? �As long as the Phillies are paying for it,� he said.

After the Dodgers, Manuel wound up in Japan, playing six years, being named the most valuable player. Once he chased a pitcher, who had hit him on purpose, into the dugout, where the pitcher hid behind the manager to avoid the enraged mountain man. But Manuel was also observing the communal ethic of Japan.

�I�ve said this often,� Manuel said Thursday. �If I had never gone and played baseball in Japan, I don�t think I would have been a coach or manager.� He also learned, he said: �There�s more people in the world than Charlie Manuel. And I mean that I learned to respect things more.� He also learned to appreciate the Japanese belief in repetition in workouts. 

While in Japan, Manuel became friendly with Davey Johnson, another observer, playing for the Tokyo Giants. Johnson turned him on to Tom Boswell�s book �The Heart of the Order.� Manuel now buys copies online and gives them to friends.

Asked about his managing philosophy, Manuel said, �To me you don�t let the game get away from you and stuff like that.� On Thursday, the Phillies let Manny Ram�rez swing four times, producing two hits and a run. Asked if they would continue to pitch to Ram�rez, Manuel said, �Not really.� 

Torre was occasionally pithy like that with the Yankees and now is that way with the Dodgers. He and Manuel have styles as different as Brooklyn and Northfork, W.Va. But they both have been some places and learned some things.

