Program time;
uses Dos;
{This program demonstrate the gettime function.}
Function L0(w:word):string;
var
  s: string;
begin
  Str(w,s);
  if w<10 then
  L0:='0'+s
  else
    L0:=s;
end;
var
  Hour,Min,Sec,HSec:word;
begin
  GetTime(Hour,Min,Sec,Hsec);
  writeln('Current time');
  writeln(L0(Hour),':',L0(Min),':',L0(Sec));
  readln;
end.
