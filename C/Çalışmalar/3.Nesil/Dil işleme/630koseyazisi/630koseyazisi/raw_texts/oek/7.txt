Temcit pilav�...  

�ANKAYA'da d�n yap�lan K�br�s zirvesinden ne ��kt�?

Yay�nlanan resmi a��klamaya bakarsan�z, bilinenler d���nda yeni hi�bir �ey yok. T�rkiye Birle�mi� Milletler Genel Sekreteri'nin K�br�s sorununu ��zmek i�in s�rd�rd��� gayretlere aktif destek verdi�ini �teden beri bin defa s�ylemedi mi? 

K�br�s'�n bir ��ulusal dava�� oldu�unu her f�rsatta vurgulamad�k m�?

T�rkiye, ��z�m� K�br�s T�rklerine b�rakmakla beraber Denkta� ve KKTC h�k�meti ile ��yak�n temas ve i�birli�i�� i�inde olmaya dikkat etmedi mi?

Sorunun ��z�m� i�in yeni bir g�r��me s�recinin ba�lamas�n� istedi�imiz her zaman s�ylenmedi mi?

Keza, T�rkiye'nin ��adan�n ger�ekleri temelinde bir ��z�me, m�zakere yoluyla h�zla ula��lmas� konusundaki siyasi kararl���n��� bilmeyen mi var?

Ger�i buradaki ��adan�n ger�ekleri temelinde�� c�mlesi biraz tuhaf. ��nk� bu s�z insan�, ��Hangi ger�ekler?�� sorusuna a��k bir yan�t istemeye zorluyor. Ancak Ba�bakan Tayyip Erdo�an'�n 15 Kas�m 2003 tarihinde Kuzey K�br�s'ta da kulland��� bu c�mle Denkta� ve KKTC h�k�meti taraf�ndan ���ki ayr� egemen devlete dayanan bir yap�n�n esas al�nmas�n� istiyor�� diye alg�land��� i�in burada da o anlama geliyormu� gibi de�erlendirilebilir. Yine de bu c�mlenin ucunun a��k oldu�u unutulmamal�d�r.

�te yandan resmi a��klamada yer almayan bir bilgiye g�re �ankaya zirvesinde Annan Plan� temel al�nm��. D��i�leri Bakanl��� b�rokratlar� ile Genelkurmay yetkilileri buna ili�kin g�r��lerini ayr� ayr� sunmu�lar ve ��neticede bir�ok noktada mutab�k kalm����lar.

Bir�ok noktada mutab�k kalmak demek ��Baz� noktalarda mutab�k kalamad�k�� demektir.

Bu ��baz��� noktalar ayr�nt�ya ili�kin de olabilir, sorunun �z�n� de ilgilendirebilir.

�rne�in Denkta�'�n Annan Plan�'na kar�� ��kmas�n�n temel nedeni olan ��KKTC'nin egemenli�inin hi� de�ilse 24 saatlik bir s�re i�in kabul edilmesi�� ko�ulu temel bir noktad�r. 

E�er Genelkurmay Ba�kanl��� ��Denkta�'�n g�r��� do�rudur�� tezinde �srar ediyor buna kar��l�k D��i�leri b�rokrasisi ��Egemenlik talebinden vazge�ilebilir�� diyorsa s�z�n� etti�imiz ��baz���lar �ok �nemli olur.

Ger�i d�n K�br�s'la ilgili g�r��me ve tart��malar sadece �ankaya K��k� toplant�s�yla s�n�rl� de�ildi. Ba�bakan Tayyip Erdo�an �ankaya'dan sonra KKTC'nin �nde gelen siyasi parti liderlerini Ankara'ya �a��rd� ve onlarla da tek tek g�r��t�. Bu g�r��me KKTC'de kurulmas� beklenen h�k�metin (tabii kurulabilirse) Annan Plan�'na d�n�k yakla��m�nda ne kadar ve hangi y�nde de�i�ikliklere yol a�acak hen�z bilmiyoruz. Zaten bilsek bile T�rkiye'nin kesin g�r���n�n as�l 24 Ocak'ta yap�lacak Milli G�venlik Kurulu'nda belirlenece�ini bildi�imize g�re... Daha bir s�re bu konuyla oyalanmaya mecburuz.
 
