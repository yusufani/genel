Makul �o�unluk mu?  
     

  
��MAKUL �o�unluk�� yeni bir tan�m...

Ak�ll�, mant�kl�, �l��l�, sesi �ok ��kmasa da verece�i bilin�li kararlarla i�leri d�zeltmesi beklenen, rejimin g�vencesi �o�unluk...

B�yle bir �ey var m�, yok mu?..

Bilemem...

Varsa, o zaman esas itibar�yla ��e ayr�l�r:

Birincisi, uyku halinde, sessiz, g�zleri asla a��lmam�� ve a��lma olas�l��� olmayan, hi�bir zaman uyan�p aya�a kalkmayan �o�unluk:

- Mahmur �o�unluk...

�kincisi, kendisi uyanmad��� halde, s�rekli olarak ba�kas�n�n uyanmas�n� bekleyen ve s�k s�k ��Nerde bu devlet, nerde bu millet?..�� diyerek kendi kendini arayan �o�unluk:

- Malul �o�unluk...

���nc�s�, bu d�nya ile mark baz�nda ili�ki kurmakla birlikte, ahiret i�in siyasi haz�rl�k yapan, yobaz politikac�lara oy verince Cennet'e gidece�ini sanan �o�unluk:

- Merhum �o�unluk...

*

Belki ��makul �o�unluk��a bir d�rd�nc�s�n� eklemek olas�:

Bizim medyan�n yaratt���, kendisi hi�bir zaman iyi bir �ey yapmamakla birlikte s�rekli olarak iyi bir �eyler olmas�n� bekleyen ve ���yi �eyler de oluyor�� haberleri ile yetinen, imal edilmi� �o�unluk:

- Mamul �o�unluk...

O zaman buna bir ilave olarak:

- Me�gul �o�unluk...

Tak�m� gol at�nca y�z�n� boyay�p soka�a d�k�len... Ya da diskotek pisti ile televole aras�nda ya�ay�p, bo� zamanlar�nda ��Biri Bizi G�zetliyor��un anahtar deli�i ile me�gul olan �o�unluk...

��Makul �o�unluk�� mahmur, malul, merhum, mamul, me�gul �o�unluklardan meydana geliyorsa, buna �� ilave daha:

- ��i t�k�r�nda; mahdut �o�unluk ile...

- Ac�ndan �lm��; maktul �o�unluk...

- Ve d�nya y�k�lsa a�z�n� a�mayan; mazbut �o�unluk... 

*

��te bu ��makul �o�unluk�� i�inde yer alan, ama ��makul �o�unlu�un�� kurban� olan, bilin�li-ak�ll�-ayd�n, kuru i�inde yanan insanlar elbette var...

Onlar; ma�dur az�nl�k olmal�...

Yoksa �o�unlu�un oylar� ile y�netilen bir demokratik �lkede, ��makul �o�unluk�� varsa, o �lke nas�l b�yle s�r�m s�r�m s�r�nebilir?..

Nas�l kara yazg�s�n� de�i�tiremez?..

Nas�l?..
 

