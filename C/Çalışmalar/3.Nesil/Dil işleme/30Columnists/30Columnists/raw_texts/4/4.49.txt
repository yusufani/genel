SIR John Junor had two famous catch-phrases he used in his newspaper columns in the Sunday Express. "Pass the sick bag, Alice," was deployed when some new fad or fashion had earned his disgust. The other one was his favourite pay-off: "I don't know, but I think we should be told."
Junor is mostly remembered, though, for his sentimental attachment to Auchtermuchty, the Fife town where he grew up. Any aspect of modern life he disliked was contrasted unfavourably with the solid values and unimpeachable virtues of Auchtermuchty foADVERTISEMENTlk. Their watchwords were decency, discipline, restraint and thrift. Their judgments were harsh and their vices few. Alternative lifestyles might come and go, but the Auchtermuchty way of life was immutable. Naturally, it was also morally superior. 

Whether or not it existed in real life or just in his imagination is a moot point, but Junor's Auchtermuchty came irresistibly to mind last week when I was reading a speech by Gordon Brown setting out the kind of society he would like Britain to become. 

While Junor had his Auchtermuchty, Brown has his beloved Kirkcaldy, or at least the rose-tinted version of it that remains lodged in the Prime Minister's mind from his childhood. 

"The community where I grew up," Brown said in his speech, "revolved not only around the home, but the church, the youth club, the rugby team, the local tennis club, the Scouts and Boys Brigades, the Royal National Lifeboat Institution, the St John and St Andrews Ambulance Society. 

"While some people say you have only yourself or your family, I saw every day how individuals were encouraged and strengthened, made to feel they belonged and in turn contributed as part of an intricate local network of trust, recognition and obligation encompassing family, friends, school, church, hundreds of local associations and voluntary organisations." 

Brown is right to laud the many millions of people who give their time and energy to strengthen the social fabric of communities both rich and poor. Yet there is something about his vision of a brave new communitarian Britain that leaves me inexplicably queasy, especially in the light of his recent policy pronouncements on drink, drugs and gambling. Barely a month into his premiership it is becoming all too clear where Brown's moral compass is taking us. Cheap drinks promotions in supermarkets are to be banned to stop us boozing, supercasinos are to be scrapped to stop us gambling and cannabis is to be reclassified, making the casual use of a soft drug a serious criminal offence. Brown is competing with David Cameron to see whose party is the more socially conservative, and so far Labour is winning by some distance. 

There are good debates to be had on each of these policy issues Brown has revisited, but the cumulative effect speaks of a government taking a punitive line on anything that would set the curtains twitching in Auchtermuchty or Kirkcaldy. A new puritanism is heading our way, where the government has a very firm view on the way it wants its citizens to live their day-to-day lives. Simply living within the law is no longer enough. We are moving decisively away from the more liberal approach of the past decade, and a glance at the contrasting hinterlands of Blair and Brown maybe helps to explain the difference. 

Kirkcaldy is a world away from the cosmopolitan north London boroughs where Blair used to live before Labour came to power. Kirkcaldy's strength lies in its sense of solidarity, north London's virtue is in its diversity. One places a high value on community, the other on individualism. One favours conformity, the other tolerance. 

As anyone who has escaped small-town Scotland knows only too well, the rosy picture painted by Brown of a tight-knit community is only part of the story. There can also be an unpleasant censoriousness about small-town Scotland, a thin-lipped expectation that people conform to a rigid social code. There is a very different interpretation of what constitutes acceptable behaviour within the law. 

There is a side to small-town Scotland that doesn't like to see anyone 'getting above themselves' or living a life that differs too radically from the social norm. It values quiescence and order above distinctiveness and difference. 

I suspect this is the core reason for my queasiness about the direction of Brown's government. It doesn't help that a similar puritan trajectory is being taken by the SNP administration in Edinburgh, where ministers are planning similar curbs on alcohol to those under consideration at Westminster. 

The SNP draws most of its support from rural and small-town Scotland. It has only recently gained substantial numbers of votes in the cities of the Central Belt, where there is a far more liberal and tolerant view of the diverse ways people choose to live their lives. 

I've checked on a map - there are only 19 miles between Kirkcaldy and Auchtermuchty. How far along this road is Gordon Brown intending to take us? What other issues of lifestyle and morality are going to be revisited by this Labour government in the months ahead? 

As John Junor might say: I don't know, but I think we should be told.
