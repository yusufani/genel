Ey bu ülkenin namuslu haysiyetli, şerefli ve vicdanlı insanları…

“Namus, haysiyet, şeref ve vicdan, genel ahlâk ve meslek ahlâkı kurallarına göre değerlendirilen, insanın hem başkalarının, hem de kendi gözündeki yerini belirleyen kavramlardır.

* * *

Emirle veya çıkarı için, inandığının tersine oy veren politikacı…
Emirle veya çıkarı için, suç olduğuna inanmadığı halde, dava açan, gerekmediği halde tutuklama isteyen ve yapan, insanları haksız yere hapse atan, cezalandıran, özgürlüklerinden mahrum eden adalet mensubu…
Emirle veya çıkarı için, gerçeklere aykırı yalan haber yazan, manşet atan, yorum yapan, insanların kişiliklerini haksız yere karalayan, efendilerini göklere çıkaran medya mensubu…
Namussuzdur,
Haysiyetsizdir, 
Şerefsizdir,
Vicdansızdır…

* * *

Ben her zaman, insanların asgari ahlâk sahibi olduklarına, politikacıların, adalet mensuplarının ve medya çalışanlarının da (bütün öteki meslek mensupları gibi) meslek ahlâklarına asgari ölçüde bağlı olduklarına inandım.
Halâ da inanıyorum.

* * *

Ama çevreme baktığımda, topluma en üst düzeyde hizmet etme mesleği olan politikacılar arasına…
Hepimizin güvencesi olan, canımızı, malımızı, özgürlüğümüzü devlete ve başkalarına karşı koruyan adalet mensupları arasına…
Görevi topluma doğru haber ve bilgi vermek olan medya mensupları arasına…
Namussuz, haysiyetsiz, şerefsiz ve vicdansız olanların da sızdığını, bu nedenle de toplumda siyasete, adalete ve medyaya güvenin azalmakta olduğunu gözlemliyor ve hem toplum, hem de bu meslekler adına çok kaygı duyuyorum.

* * *

Namussuzlar, haysiyetsizler, şerefsizler, vicdansızlar, genel ahlâk ve meslek ahlâkına karşı olarak neler yaptıklarını bildikleri için, korkaktırlar.
Hemen birbirleriyle ittifak eder, güçlerini birleştirirler.
Namuslu, haysiyetli, şerefli ve vicdanlı olanlar ise korkusuzdurlar.
Bu nedenle yalnızdırlar, ittifak aramazlar, güç birliğine gitmezler.
Ve böylece zayıf kalırlar.

* * *

Ey bu ülkenin namuslu, haysiyetli, şerefli, vicdanlı politikacıları, adalet mensupları ve medya çalışanları:
Birleşiniz…
Kaybedecek namusunuz, haysiyetiniz, şerefiniz, vicdanınız, mesleğiniz, kişiliğiniz, kısacası tüm bir yaşamınız var.”

* * *

Sevgili okurlarım bu satırları, önceki akşam Halk TV’de büyük ilgiyle izlenen Halk Arenası’nın konukları arasında yer alan dünyaca saygın toplumbilimci-yazar Prof. Dr. Emre Kongar’ın ‘Tarihimizle Yüzleşmek’ adlı kitabından alıntıladım.
Genişletilmiş baskısı yeni çıkan bu başyapıtı okumanızı içtenlikle öneriyorum.
Zira kitabın her satırı, yukarıda okuduklarınız gibi, alkışı hakeden bilgi, birikim ve tespitleri içeriyor.

