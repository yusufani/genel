/* 20prg05.c: Makro  tanimlama. */

#include <stdio.h>
#include <math.h>

/* makro fonksiyonlar */
#define kare(x)   (x*x)
#define topl(x,y) (x+y)
#define carp(x,y) (x*y)
#define hipo(x,y) sqrt(x*x+y*y)

int main()
{
   float a=3.0, b=4.0;
   printf("kare(2)   = %f\n",kare(2.0,5.0));
   printf("kare(2)   = %f\n",kare((2.0,5.0)));
   printf("topl(a,b) = %f\n",topl(a,b));
   printf("carp(a,b) = %f\n",carp(a,b));
   printf("hipo(a,b) = %f\n",hipo(a,b));
}
