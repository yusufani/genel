Here�s a new word for you: obsolagnium. You may not find it in an ordinary dictionary. But if you are over 50, you may well be familiar with the concept, because it means �waning sexual desire resulting from age.�

In fact, it is rarely age per se that accounts for declines in libido among those in the second half-century of life. Rather, it can be any of a dozen or more factors more common in older people that account for the changes. Many of these factors are subject to modification that can restore, if not the sexual energy of youth, at least the desire to seek and the ability to enjoy sex.

Nor is it just hormones. Addressing only the distaff half of the population, the Boston Women�s Health Book Collective, in its newest work, �Our Bodies, Ourselves: Menopause,� points out: �Our sexual desire and satisfaction may be influenced by our life circumstances, including the quality of our sexual relationships, our emotional and physical health, and our values and thoughts about sexuality, as well as by the aging process and the shifting hormone levels that occur during the menopause transition.�

The same, of course, is true of men. Difficult life circumstances can do much to dampen anyone�s libido. Stress at work or home, looming bankruptcy, impending divorce, serious illness, depression, a history of sexual abuse and a host of medications are among the many things that can put a big crimp in your desire for sex at any age.

Feel Attractive, Be Attractive

As people age, both physical and emotional changes occur that can influence libido. Wrinkles, hair loss, declining muscle mass and accumulation of body fat, among other age-related changes, can make men and women feel less attractive. And if you don�t see yourself as attractive, your brain may respond by dampening any impulse you might have to be intimate with someone.

I have no studies to corroborate this idea, but I strongly suspect that older people who stay in shape physically, keep their brains stimulated and remain interested in a variety of activities are likely to feel more attractive and be more attractive � and thus more libidinous � than those who let themselves go to pot, as it were. I�m not suggesting that people in their 60s and 70s start dressing and acting like 20-somethings, but there are any number of age-appropriate actions that can help people see themselves � and help others see them � as sexually desirable beings.

Of course, illness, both mental and physical, can seriously disrupt a healthy libido at any age. Diseases of the adrenal, pituitary or thyroid glands can diminish sexual desire, as can depression and anxiety. Likewise, several common cancers � especially cancers of the breast, testes or prostate or the drugs used to treat them � may suppress the desire for sex.

Many commonly administered medications can interfere with sexual desire, performance or both. Among the most frequent offenders are antidepressants and antianxiety drugs, blood pressure medications and opioid pain relievers. High doses of alcohol likewise blunt desire as well as performance. Even drugs taken to curb heartburn can curb the desire for sex. In some instances, changing the dose, switching to a different drug or taking a brief drug holiday (say, for the weekend) can boost libido. 

A Change of Scene

While a drug like Viagra may help a man temporarily overcome disease- or medication-induced erectile dysfunction, it does nothing to increase desire, which is essential for these potency-enhancing drugs to work. 

Knowing how to please each other sustains sexual interest for many long-established couples. But for others, familiarity can breed boredom; they lose interest in doing the same old thing the same old way time after time.

Novelty is a well-established sexual stimulant. An unattached man or woman in midlife or beyond who had all but forgotten about sex meets someone new and attractive, and suddenly the flames of sex are reignited. This can happen, too, to very old people. Stories abound in assisted living and nursing home facilities of elderly widows and widowers whose long-dormant sexuality is reawakened by attraction to a new, albeit equally old, partner.

Of course, changing partners is not a realistic option for those in a long-standing monogamous relationship in which sexual intimacy is just a fond memory. 

But there are ways for such couples to introduce novelty � ranging from a change of venue or techniques to an exchange of fantasies or even the introduction of sex toys � that may rekindle sexual feelings.

Even young couples can find their interest in sex diminished by a fear of interruption or being overheard by children or an elderly parent. It can take some effort � and perhaps a lock on the bedroom door and background music � to reduce the risk of distractions that blunt the flame of desire. 

Women may think that the decline in estrogen at menopause is responsible for their loss of interest in sex. But estrogen loss is only an indirect factor; it can result in vaginal tightness and dryness that renders intercourse painful rather than pleasurable. The use of lubricants and a dildo or more frequent sex can often counteract these effects. But for some women, the use of a vaginal estrogen cream or suppository is necessary to make sex comfortable and more desirable.

The Testosterone Factor

But the real libido hormone, for both men and women, is testosterone, which women produce in their ovaries and adrenal glands. As other ovarian hormone levels drop after menopause or surgical removal of the ovaries, so does the amount of desire-boosting testosterone. This has prompted some women to use testosterone replacement therapy to get their sex lives back on track. One drug commonly prescribed off-label is Estratest, a combination of small doses of estrogen and testosterone. Some doctors tailor-make low-dose testosterone preparations for women. A testosterone patch for women has not been approved by the Food and Drug Administration because of insufficient safety data. 

Women taking testosterone should be carefully monitored, because safe levels of the hormone for women have not been determined. Common side effects include unwanted hair growth and a deepening of the voice. Women who have had breast or uterine cancer or diseases of the liver or heart should avoid testosterone replacement. 

Sexual desire among men, too, can be squelched by low levels of testosterone. While there is no official recognition of male menopause, men experience declining levels of hormones as they age � what some experts called andropause � that can affect sexual desire and performance. Other symptoms of this deficiency may include enlarged breasts, loss of body or facial hair, and osteoporosis before age 65.

Testosterone replacement is helpful in restoring the sex drive only of men who have low levels of the hormone. A test of testosterone levels should be done and other causes (besides age) should be ruled out before the hormone is prescribed. Risks include prostate enlargement and prostate cancer.

