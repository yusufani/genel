High school textbooks call it the tongue map � that colorful illustration that neatly divides the human tongue into sections according to taste receptors. There is the tip of the tongue for sweet, the sides for sour and salty, and the back of the tongue for bitter. But recent studies show that while scientists still have much to learn about receptors, the map, at least, is wrong

What is known is that there are at least five basic tastes: sweet, sour, salty, bitter and the most recently discovered, umami. This last flavor, which means �savory� in Japanese, can be detected in miso, soy sauce and other Asian foods, particularly those that contain monosodium glutamate. And scientists suspect that there are receptors for other flavors as well.

In a study published in the journal Nature in 2006, a team of scientists reported that receptors for the basic tastes are found in distinct cells, and that these cells are not localized but spread throughout the tongue. That said, other studies suggest that some parts may be more sensitive to certain flavors, and that there may be differences in the way men and women detect sour, salty and bitter flavors.

