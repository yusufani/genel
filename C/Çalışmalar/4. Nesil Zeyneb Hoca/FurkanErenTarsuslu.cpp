#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>// türkçe karakter sorunu için eklendi
void sifrele ( char *);
int main(){
    setlocale(LC_ALL, "Turkish");// türkçe karakter sorunu için eklendi.
    char *password;
    password=(char *) calloc (200,sizeof(char));//Assume that password can be max 200 characters
    printf("Lutfen sifreyi giriniz:");
    scanf("%s",password);
    printf("%s",password);
    sifrele(password);
}
void sifrele ( char * password){
    int n=strlen(password);// n is the size of the password
    int i,sifrelenen_Sayisi=0;
    char tmp;
    printf(" ");
    for(i=n-1;i>=0;i--) {
        if(password[i]=='a'){
            password[i]='!';
            sifrelenen_Sayisi++;
        }
        else if (password[i]=='e'){
            password[i]='?';
            sifrelenen_Sayisi++;
        }
        else if (password[i]=='i' || password[i]=='ı'){
            password[i]=',';
            sifrelenen_Sayisi++;
        }
        else if (password[i]=='o' || password[i]=='ö'){
            password[i]='=';
            sifrelenen_Sayisi++;
        }
        else if ((password[i]=='u') || (password[i]=='ü')){
            password[i]='#';
            sifrelenen_Sayisi++;
        }
    }
    for(i=0;i<(int)n/2;i++){
        tmp=password[i];
        password[i]=password[n-1-i];
        password[n-1-i]=tmp;
    }
    printf("Sifrelendikten sonra parola:%s\n",password);
    printf("Sifrelenen karakter sayisi:%d\nSifrelenmeyen karakter sayisi:%d",sifrelenen_Sayisi,n-sifrelenen_Sayisi);
}