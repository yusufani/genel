#include<stdio.h>
#include<conio.h>
#include<stdlib.h>

int main()
{
 int m,n,s=0,i,j,x,y;
int **A;
int **B;
 printf("\nEnter the order m x n of the sparse matrix\n");
 scanf("%d%d",&m,&n);
 
  A = (int **)malloc(m * sizeof(int *));
  B = (int **)malloc(4 * sizeof(int *));
  
  for (i=0; i<m; i++)
         A[i] = (int *)malloc(n * sizeof(int));
 
  for (i=0; i<=3; i++)
         B[i] = (int *)malloc(4 * sizeof(int));
  
 for(i=0;i<m;i++)
 {
  for(j=0;j<n;j++)
  {
   printf("\n%d row and %d column:   ",i,j);
   scanf("%d",&A[i][j]);
  }
 }
 
  for(i=0;i<3;i++)
 {
  for(j=0;j<3;j++)
  {
   	B[i][j] = 0;
  }
 }
 
  for(i=0;i<m;i++)
 {
  for(j=0;j<n-1;j++)
  {
   x = A[i][j];
   y = A[i][j+1];
   B[x][y]++;
  }
 }
 
 
 printf("The given matrix is:\n");
 for(i=0;i<m;i++)
 {
  for(j=0;j<n;j++)
  {
   printf("%d ",A[i][j]);
  }
  printf("\n");
 }
   printf("\n");
  printf("\n");
    
 printf("\nThe co-occurence Matrix B is given as");
 printf("\n");
 for(i=0;i<=3;i++)
 {
  for(j=0;j<=3;j++)
  {
   printf("%d ",B[i][j]);
  }
  printf("\n");
 }
 getch();
 return 0;
}