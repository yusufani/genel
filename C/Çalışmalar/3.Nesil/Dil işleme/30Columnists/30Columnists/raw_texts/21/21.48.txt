Poor old Michael Grade. Two years ago, the media veteran quit the licence fee-insulated environment of the BBC to take on the challenge of turning around ITV, and he actually produced a half-way decent plan for doing so.


This has now had to be binned amid what Mr Grade describes as the most challenging advertising market he has experienced in more than 30 years in broadcasting. Out the door go all the previously set targets for revenue growth in content, free-to-air TV and online. They looked relatively unambitious when first announced. Now they are just fantasy.

Jobs and programming budgets are being slashed, non-core assets such as Friends Reunited have been put up for sale, and the dividend has been axed. Mr Grade insists he's got no financing problems at least until October 2011, when a �335m bond comes due, but he's refusing to rule out a rights issue. The betting must be that ITV will survive, but it's going to be touch and go.

Even Greg Dyke, who once took a tilt at ITV in conjunction with Goldman Sachs, wouldn't envy Mr Grade's position now. Managing for decline is a depressing old business at the best of times, but it is particularly grim now, with a vicious cyclical downturn piling on the agony for an industry already in the midst of profound structural change.

All that said, Mr Grade's underlying strategy still seems sensible enough. Or rather, it is hard to see what the alternatives might be. Mr Grade claims to have reversed the decline in audience share, but however ITV positions its free-to-air channels, long-term decline remains a virtual certainty. ITV's licence to print money expired many years ago. A bygone age in which there were only a small number of TV channels competing for people's time has been replaced by a multitude of alternatives, from the myriad of digital TV channels that flood the airwaves to the internet and the interactive games industry. Today, ITV is just one of the crowd.

To counter this fragmentation, ITV is pinning its hopes firstly on continued ability to produce winning, popular entertainment formats, for which there is a growing market around the world, and secondly on development of ITV.com, which, like the BBC's iPlayer, allows you to watch programming outside the disciplines of the schedule.

Neither initiative has been entirely successful yet, but it is early days and you wouldn't expect significant progress in a downturn. In blue skies thinking submitted to Ofcom, the industry regulator, ITV suggested a three-way merger with Channel 4 and Channel Five. That's not the way to go. There is, perhaps, more to be done on shared costs, but even if full-scale merger was thought acceptable on competition grounds, there is no reason to believe it would bring salvation to any of these troubled companies. ITV is itself already the product of multiple mergers, and little good has it done too, either in halting the decline of free-to-air commercial TV or in improving the product to the viewer.

No, ITV must struggle on alone. Progressive release from public service broadcasting obligations will allow a dwindling pool of revenues to go further, but the main task for Mr Grade and his team is not that of winning regulatory concessions but of finding new ways of exploiting the franchise.

ITV is never going to produce the next big thing in media, or it seems unlikely anyway, but it is still good at what it does, better in some respects than the BBC, which is tied by bureaucracy. The challenge for ITV, as for all "old" media, is to find new ways of monetarising this talent. Regrettably, the challenge is much easier to define than to meet.

