#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

struct Araba {
	
	char plaka[16];
	char marka[32];
	char model[32];
	int yil;
};

void PrintMenu() {
	
	printf("1-) Yeni Araba Ekleme\n");
	printf("2-) Araba Goruntule\n");
	printf("3-) Cikis\n");
	printf("Giris : ");
}

char ReadFile(char fileName[], struct Araba a[]) {
	
	FILE *inputFile;
	int i = 0;
	inputFile = fopen(fileName, "r");
	
	if (inputFile == 0) {
		return -1;
	}
	
	while(fscanf(inputFile, "%s %s %s %d", &a[i].plaka, &a[i].marka, &a[i].model, &a[i].yil) != EOF) {
		i++;
	}
	
	fclose(inputFile);
	
	return i;
}

char WriteFile(char fileName[], struct Araba a[], int count) {
	
	FILE *outputFile;
	int i;
	outputFile = fopen(fileName, "w");
	
	if (outputFile == 0) {
		return -1;
	}
	
	for(i = 0; i < count + 1; i++) {
		fprintf(outputFile, "%s %s %s %d\n", a[i].plaka, a[i].marka, a[i].model, a[i].yil);
	}
	
	fclose(outputFile);
	
	return 0;
}

int main(int argc, char *argv[]) {
	
	struct Araba arabalar[100];
	int input;
	int i;
	
	int count = ReadFile("./list.txt", arabalar);  // result kayit sayisi
	
	if (count == -1) {
		printf("Hata. Dosya okunamadi.");
		exit(-1);
	}
	
	
	
	while(input != 3) {
		PrintMenu();
		scanf("%d", &input);
		if (input == 1) {
			printf("===ARABA EKLEME===\n");
			printf("## %d. Araba\n", count + 1);
			printf("Plaka : "); scanf("%s", &arabalar[count].plaka);
			printf("Marka : "); scanf("%s", &arabalar[count].marka);
			printf("Model : "); scanf("%s", &arabalar[count].model);
			printf("Yili  : "); scanf("%d", &arabalar[count].yil);
			printf("\n");
			int result = WriteFile("./list.txt", arabalar, count);
			if (result == -1) {
				
				printf("Hata. Dosyaya yazilamadi.");
				exit(-1);
			}
			count++;
		} 
		
		if (input == 2) {
			
			for (i = 0; i < count; i++) {
				
				printf("%d- %s %s %s %d\n", i + 1, arabalar[i].plaka, arabalar[i].marka, arabalar[i].model, arabalar[i].yil);
			}
			printf("\n\n");
		}
	}
	
	//ReadFile("./in.txt", arabalar);
	
	
	return 0;
}
