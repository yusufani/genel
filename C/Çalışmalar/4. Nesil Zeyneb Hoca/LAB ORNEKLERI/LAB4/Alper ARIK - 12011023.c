#include <stdio.h>
#include <stdlib.h>

 struct ARABA{
	char plaka[16];
	char marka[16];
	char model[16];
	char yil[5];
};

struct ARABA * arabaOlustur(int );
void arabaEkle(struct ARABA* ,int );
void arabaDosyayaYaz(struct ARABA *,int ,FILE *);
void dosyadanArabaOku(struct ARABA * ,int ,FILE*);

int main(void) {
	char kontrol;
	int n;
	struct	ARABA *araba,*test;
	FILE *fi;
	
	do{	
		printf("\n1. Yeni araba ekle ");
		printf("\n2. Araba goruntule ");
		printf("\n3. Cikis ");
		scanf("%c",&kontrol);
		
		if(kontrol=='1'){
			printf("\nOlusturulacak araba sayisini giriniz:");
			scanf("%d",&n);
			araba=arabaOlustur(n);
			arabaEkle(araba,n);
			fi=fopen("araba.txt","w+");
			arabaDosyayaYaz(araba,n,fi);
			fclose(fi);
		}
		else if(kontrol=='2'){
			fi=fopen("araba.txt","r+");
			dosyadanArabaOku(test,n,fi);
			fclose(fi);
		}
	}while(kontrol!='3');
		
	return 0;
}

struct ARABA * arabaOlustur(int n){
	struct ARABA *araba;
	araba =(struct ARABA*)malloc(n*sizeof(struct ARABA));
	return araba;
}

void arabaEkle(struct ARABA *araba,int n){
	int i;
	for(i=0;i<n;i++){
		printf("\n%d. Arabanin plakasini giriniz:",i+1);
		scanf("%s",araba[i].plaka);
		printf("\n%d. Arabanin markasini giriniz:",i+1);
		scanf("%s",araba[i].marka);
		printf("\n%d. Arabanin modelini giriniz:",i+1);
		scanf("%s",araba[i].model);		
		printf("\n%d. Arabanin uretim yilini giriniz:",i+1);
		scanf("%s",araba[i].yil);	
	}
}

void arabaDosyayaYaz(struct ARABA *araba,int n,FILE *fi){
	int i;
	for(i=0;i<n;i++)
	fprintf(fi,"%s\t%s\t%s\t%s\n",araba[i].plaka,araba[i].marka,araba[i].model,araba[i].yil);
}
void dosyadanArabaOku(struct ARABA *test ,int n ,FILE*fi){
	int i;
	for(i=0;i<n;i++){
		fscanf(fi,"%s %s %s %s",test[i].plaka,test[i].marka,test[i].model,test[i].yil);
		printf("\n%d. Arabanin plakasi: %s\n",i+1,test[i].plaka);
		printf("\n%d. Arabanin markasi: %s\n",i+1,test[i].marka);
		printf("\n%d. Arabanin modeli: %s\n",i+1,test[i].model);	
		printf("\n%d. Arabanin uretim yili: %s\n",i+1,test[i].yil);
 	}
}
