Topba� kimin, neyin ba�kan�?..

Televizyonda Kadir Topba�'� izliyorum.. ���nc� k�pr� ile ilgili konu�uyor.. 
"O i� �zerinde ba�bakan ve ekibi, Ankara'da �al���yorlar." 
�stanbul'un en �nemli konular�ndan biri, Ankara'da ��z�l�yor. Ba�bakan ve ekibi taraf�ndan..
�a�mad�m.. 
��nk� Topba�'tan umudunu �oktan kesen eski �stanbul Belediye Ba�kan� Recep Tayyip Erdo�an'�n kendi ekibi ile kenti y�netme karar� verdi�i �oktand�r iyi haber alan �evrelerde konu�uluyordu. Topba�'�n haberi olmadan, Ba�kan Vekili �dris G�ll�ce imzas� ile uygulamaya konan kararlar da, Ba�kan'�n ger�ekten by-pass edildi�ini g�steriyordu.
Yerel se�imler �ncesi, RTE'nin, Kadir Topba�'� partiden gelen bask�lar �zerine �ok g�� hazmetti�i, Ba�bakan'�n g�nl�ndeki aday�n Tuzla'da fevkalade ba�ar�l� bir performans g�steren G�ll�ce oldu�u hep s�ylenmi�ti. Se�im sonras� da, G�ll�ce Ankara'n�n iste�i �zerine "Vekil Ba�kan" olmu�tu.





�n�mde Kadir Topba�'�n bir yaz�s� var.. 
Onu ve �stanbul Valisi sevgili dostum Muammer G�ler'i, bir sal� g�n� Sabah'a davet etmi�tim, �stanbul'un se�ilmi� ve atanm�� sahipleri olarak. K�� gelirken, galeri rezaletinin bir b�l�m�n� g�sterecek ve "�stanbul halk�n�, karda, buzda, �amurda akan trafik i�inde y�r�meye zorlayan bu usuls�z, ruhsats�z ka�ak yap�lanma ile nas�l m�cadele edeceklerini" soracakt�m.
Sal� g�n� randevu saatinde �stanbul Emniyet M�d�r Muavini Ali Kemal Hanl�, elinde bir dosya ile geldi. "Beni say�n vali, vekilen g�nderdi" diyerek. Tam o s�rada da vali bizzat arad�.
�stanbul Vilayeti davetimi dikkate alm��, gereken ilgiyi g�stermi�ti. 
Kadir Topba� ise zerre umursamam��t�. Belediye Ba�kan�-Gazeteci ili�kisinin �tesinde dost oldu�umuz (Ya da ben �yle sanm��t�m) halde..
Gelen dosya ve Ali Kemal Hanl� ile kritik noktalar� dola��rken dinlediklerim, as�l sorumlunun belediye oldu�unu ortaya t�m ��plakl��� ile koydu. Valilik hatta belediyeyi galeriler konusunda defalarca uyarm��, "Bir eylem yapacaks�n�z, Emniyet M�d�rl�klerimiz size destek olacakt�r" demi�ti. Buna ra�men Belediye konuya el atmam��, yasal olarak g�rev ihmali su�u i�lemi�ti. 
Bunlar� anlatan yaz�m yay�nlan�nca, Topba� galiba tela�land�. Defalarca, evden i�ten arand�m. Bu defa da ben geri d�nmedim. Ba�kan�n bana tavr�n� kendisine aynen iade ettim.
Bunun �zerine bir yaz� geldi. 
"S�z konusu i�yerleri, B�y�k�ehir Belediyesi'nin g�rev yetki ve sorumlulu�unda bulunmamaktad�r." 
�zah da ediyor..
"Galerilere ruhsat verme i�i il�e belediyelerinin, kald�r�ma park etmi� arabalar� �ekme i�i de vilayetindir." 
Yani "Ben anam�n ak s�t�, partimin rengi gibi temizim.." 
Topba� herkesi k�r, alemi sersem san�yor.. 
Say�n Topba�..
Galerilerin can� cehenneme.. Ben kald�r�mdan s�z ediyorum.. Vatanda��n y�r�d��� kald�r�mdan.. Ana caddeler kimin?.. Anakent'in.. �l�e belediyeleri bu caddeler �zerine proje geli�tirdi�inde, an�nda reddediyorsun, "Sizin i�iniz ara sokaklar.. Ana caddeler Anakent'in, kar��may�n" diye..
Peki ana caddeler �zerindeki kald�r�mlar�n bord�r ta�lar� s�k�l�yorsa, kald�r�m boydan boya e�ik bir rampa haline geliyorsa, bunu eski haline getirmek ve kald�r�m� bozan firma ya da ki�iye ceza vermek kimin i�i?..
Bu soruma yan�t verir misin Say�n Topba�?. 
Vatan Caddesi'nde vatanda� kald�r�m� kullanma hakk�ndan yoksun k�l�n�p, caddede y�r�meye zorlan�yorsa, sorumlu kim.. G�revli kim?.. Yetki kimde?.. 
Kald� ki Say�n Topba�.. 
Belediye kurallar� gere�i, ana caddeler �zerine galeri ruhsat� vermek yasakt�r. Galeriler ancak ara sokaklarda kurulabilir. Bu y�zden zaten galeriler il�e belediyelerinin sorumlulu�una girer..
Ama benim yazd�klar�m ana arterler �zerinde, yani size ait, galerilere yasak olan caddelerde kurulmu� i�yerleri.. 
Nas�l ruhsat al�yorlar?.. Ruhsatlar� yoksa nas�l �al���yorlar, s�yler misiniz?. 




Say�n Topba�, ge�en hafta i�inde Baltaliman� Tem ba�lant� yolunu ilk defa kulland�m. Gecekondu b�lgesinden ge�en muhte�em bir yol yapm��s�n�z.. fevkalade de i�levli.. Elinize sa�l�k..
Bunu yaparken Armutlu y�resinde y�zlerce gecekonduyu da y�kt���n�z� g�rmek m�mk�n.. Y�k�mlar�n bir b�l�m� bitmi� yolun �evre g�zelli�i ad�na devam ediyor h�l�..
G�zel.. Bravo!..Ama tablo a��k de�il mi, Say�n Topba�..Fakirin gecekondusunu hem de y�zlercesi ile y�kmaya yetiyor g�c�n�z.. Ama i�, zengini daha zengin eden galerilere, yani arkas�nda para ve de bir b�l�m�nde karapara g�c� olanlara gelince, su�u ba�kalar�na y�kleyip ka�acak delik ar�yorsunuz.. 
Bay Kadir Topba�!.. 
Siz �stanbul'un Belediye Ba�kan� de�ilsiniz!.. Recep Tayyip Erdo�an sizi istememekte hakl�ym��. �stanbul halk� (ki i�lerinde ben de vard�m) sizi destekler ve se�erken hata etmi�.. Mesele bundan ibarettir!.. 
Not: Yaz�y� bitirip gazeteden ayr�ld�ktan sonra yeni bir haber geldi. Ba�kan Topba� vekillik makam�n� iptal ederek, �dris G�ll�ce'yi g�revden alm��t�. Bu demektir ki AK Parti'de durumlar kar���k. Yak�nda kokusu ��kar.. 