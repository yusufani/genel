Everyone believes that that stress, and perhaps especially workplace stress, is a destroyer of health and a killer. And everyone, of course, especially workers under stress, believes that to be a truth as indisputable as the fact that politicians will say anything they think will help them get elected. 
But here�s a poser: how exactly can you prove that workplace stress can make you sick? 
It�s not nearly as easy as you think because for a start, it�s not that easy to define stress. 
For example, who is more at stress: the boss giving out the jobs and tasks and the person who has to ensure that the jobs get done properly and productively, or else he either loses money or his own position? 
Or is it the poor schmuck who has no control over his job and simply has to do what the boss tells him to do? 
Well, according to most researchers, it�s probably mostly the latter person, and researchers have taken to defining stress in the workplace as a job with �high demand and little control�. 
I think that�s probably true, but only to an extent. 

Then there�s the question as to how exactly workplace stress affects health, that is, what are the mechanisms through which stress affects health, either positively (high achievements, for example, through high demand might have a positive effect on health), or negatively. 
Hey, told you this wasn�t easy. 
Anyway, everyone agrees on one thing: the more stress a person is under, the more symptoms � fatigue, mood changes, headaches, etc. � they get.
But symptoms are not disease, so an important question for researchers is this: what are the physiological changes that lead to disease that can be linked to high workplace stress? 
And happily, there seems to be some emerging data that is beginning to help answer that question. 
Take for example, a recent study published in the British Medical Journal that involved 10,000 civil servants from the UK. 
In this study, the higher the stress load, the higher the risk of metabolic syndrome, that collection of health problems (including high cholesterol, high blood pressure, and high insulin resistance) that is very clearly linked to higher risks of diabetes, heart disease, and stroke. 
This doesn�t, of course, tell us how high stress load leads to the metabolic syndrome (neurological changes? Hormone changes? Reduced immunity?), but it does make a pretty good case for reducing stress in the workplace to the extent that you can � without getting fired, of course. 
It also leads to this bottom line: if you suffer lots of stress at work, you�d better work at reducing your other lifestyle risk factors for metabolic syndrome, which include the same, same old: watch your weight, eat properly, and do some exercise. 

