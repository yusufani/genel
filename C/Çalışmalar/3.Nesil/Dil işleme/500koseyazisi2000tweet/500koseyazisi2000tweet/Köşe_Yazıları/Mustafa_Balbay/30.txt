﻿Yanlış hesap Moskova’dan döndü!
Son bir hafta içinde yaşananlar gösteriyor ki, bugünkü Obama-Putin görüşmesinde iki lider, Suriye’de önceliğin IŞİD terörüyle mücadele olduğunu açıklayıp şöyle devam edecek: “Bunun için gerekirse Esad yönetimiyle de işbirliği yapılacak.” 
Bu durum küresel aktörlerin dünyaya bakışını biraz yakından izleyenler için şaşırtıcı değil. Zira bu ülkeler için asıl olan kendi çıkarları. Örneğin iki büyük ülke üçüncü bir ülke toprakları üzerinde karşı karşıya gelmişse, ikisinin de istediği olmuyorsa, ikirciklenmeden 
o ülkeye karşı birleşebilirler. Haziran 2012’de Obama ile Putin buluşmuş ve dünyaya şu kararı duyurmuştu: 
“Suriye’de Esad’sız bir geçiş için anlaştık.” Aynı iki lider bugün bir anlamda tersini söyleyecek!

***

İşte İnönü, yukarıda yaptığımız özetin kendi dönemindeki izdüşümlerine bakarak o ünlü sözünü söylemiş, büyük devletlerle işbirliğini ayıyla yatağa girmeye benzetmişti.
AKP iktidarı bu ülkenin geçmişine azıcık saygı duysaydı, birikimlerine birazcık ilgi gösterseydi, Suriye politikasında bu kadar hata yapmazdı. 
Geçen çarşamba günü, Kurban Bayramı öncesinde Moskova’ya giden Erdoğan’ı bu köşeden selamlamış. şöyle seslenmiştik: 
Yanlış hesap Bağdat’tan döner derler. AKP’nin Suriye konusundaki yanlış hesabı dünyayı iki kez turladı, bakarsınız Moskova’dan döner! 
Bu değerlendirmemiz rastgele vurguladığımız bir öngörü değildi. Rusya’nın diplomasiyi daha sert kullandığı bilinen bir gerçek. Erdoğan’ın bir günlük Moskova gezisi öncesi, Rusya’nın Suriye’ye silah gönderdiği, daha da ileri giderek asker konuşlandırdığı haberleri fiilen doğrulandı. 
Bu durum kuşkusuz, Rusya’nın Esad yönetiminin ayakta kalmasını istediği şeklinde yorumlandı. 
Erdoğan’ın çevresindekiler de sonunda gidişin ne anlama geldiğini gördüler. 
Bütün bunların ardından Erdoğan, Rusya dönüşü bayramlık ağzını açtı ve geçiş sürecinin Esad’la olabileceğini ilan etti. 
Sonradan öyle demedim, şöyle dedim türü açıklamalar yapsa da 2012’den beri 3 ayda bir Esad’a ömür biçen Erdoğan’ın Rusya’ya kendi düşünceleriyle gidip Putin’in düşünceleriyle döndüğü anlaşılıyor. 
Kim bilir belki de Putin’le Erdoğan arasında ayrı bir ilişki vardır. Belki de ikisi de birbirine kıyamıyordur. 
Çıkar derken kişisel demiyoruz tabii, ülkeseldir! Erdoğan bu tür ilişkilerde ülkem der başka bir şey demez!

***

Bütün bu bunlar olurken Suriyeliler de ölmeye devam ediyor. 
Artık Bodrum’dan bir sel-felaket haberleri geliyor bir de botu battığı için yaşamını yitiren Suriyeli haberleri... 
Edirne yolunda umutsuz bekleyişlerin de haber değeri yok artık. 
Bugün yeni öğretim yılı başlıyor. 18 milyon öğrencimiz dersbaşı yapacak. Öte yandan 600 bin Suriyeli çocuğun okul hayali sokakta kalacak. 
Toplum içinde Suriyelilere ilişkin çok farklı yaklaşımlar var. Ancak düşüncemiz ne olursa olsun onlar aramızda. 
Araçta giderken kırmızı ışığın dibinde... Yolda yürürken köşebaşında... Alışverişten çıkarken kasanın hemen ötesinde... 
Bu insanlık dramı küresel hesapların gölgesinde kalmamalı...