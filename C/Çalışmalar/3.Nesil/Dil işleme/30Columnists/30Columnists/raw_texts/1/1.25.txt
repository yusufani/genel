JUST how deep and how long will Britain's economic recession be? The daily assault on our senses as the news of rising unemployment trends, repossession numbers and desperate trading figures are reported makes for depressing reading.

Rarely are any two economic booms or busts exactly the same � there are, however, often common factors that will determine just how painful or painless the economic news will be. The strength of the British pound, relative to other currencies, is neaADVERTISEMENTrly always a crucial component and I expect it will be no less important than usual. That's why there has been some political skirmishing in the last week as the Shadow Chancellor, George Osborne, finally had something to say about Britain's economic plight, suggesting that the pound is about to suffer a very serious fall. 

The value of a country's currency is as close to a share price of a nation as it is possible to get. Currency markets tend to reflect the sentiment of traders in the fundamental strengths and weaknesses of a country's economy and what Osborne was saying was that the pound will take a hammering as the country is badly prepared to face the economic downturn.

Slow off the mark he may have been, but correct? More likely than not, I wager � for it is the very poor state of the Treasury's tax revenues and ballooning borrowing commitments that are causing the pound to be marked down by traders. Shareholders, investment managers and currency dealers are all shifting to the dollar as they anticipate an American recovery ahead of most other economies.

It could be worse for Britain though, for there is plenty evidence from past economic recessions to suggest a falling pound is no bad thing � and we should be thankful that we have our own currency rather than be locked into the euro and its cumbersome one size fits all interest rate. It was when the pound was able to find its own lower value, when Britain left the forerunner of the euro � the ERM, that our record 16-year economic growth began.

If there's any currency that is due a tumble it is the euro, but while it is, I believe, trading at well above its real value it is slowing down the chances of recovery for Germany and France � as well as Ireland and Italy. Thank goodness we're not in it.

The pound may be at its lowest ever value against the euro but this partially reflects the low interest rates set by the Bank of England � compared to the euro's central bank in Frankfurt.

The fast drop in interest rates is already working its way through to most people's mortgage payments and making a bigger difference to real spending power than any tax cuts that will take an age to appear and may be targeted at marginal activities or small groups of people.

The freedom to reduce Britain's interest rates would not have been available to us had we become members of the euro � and whether or not Gordon Brown or Tony Blair planned it that way (for it is just as much Blair's recession as it is Brown's) matters not � we should all be grateful.

When the historians come to look at all the private documents of the Blair and Brown years one question that will be asked is did Labour ever really wish to take Britain into the euro � or was it all a political ploy to maintain the very real and public division inside the Conservatives � thus making them unattractive and unelectable?

It may be only an academic discussion, but the decision taken between Brown and Blair to stay out of the euro could, like giving the Bank of England a large degree of independence, prove to be one of the best decisions they ever made.

No interest in loans

Staying on financial matters I was astounded to open my mail to find yet another piece of junk, this time trying to sell me a loan for Christmas. 

Fair enough, you can't blame people for trying, especially when there will undoubtedly be some who are cash-strapped. What floored me, however, was the interest rate being offered, making a loan of �300 cost �513 � the typical APR (Annual Percentage Rate) was advertised as 189 per cent!! That much � at a time when interest rates at three per cent are at a 53-year low?

My advice to anyone needing financial help is make sure you shop around � or get advice � there are some sharks out there looking to make a killing.

Strictly hubris

It is normally left to politicians to show us the perils of pomposity and pride � when they fall it is often because of their own vanity or arrogance and it is called hubris.

Funny then that it should be left to John Sergeant, the BBC's former political editor, to show, by his resignation from Strictly Come Dancing, that its judges are guilty of hubris. 

That they haven't noticed the public often votes for the worst performer is what is really laughable.
