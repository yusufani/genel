A much-noted study last year raised more than a few eyebrows when it suggested that digital music players could set hearts aflutter � by interfering with pacemakers. 

The study, published in the journal Heart Rhythm, found that all it took to cause electrical interference in an implanted pacemaker was holding an iPod two inches from a patient�s chest. In some cases, the study found, an iPod caused interference when it was held within 18 inches of a patient. 

But many scientists were skeptical, and apparently for good reason. More recent studies that looked at music players have found they have little effect on pacemakers. 

The latest, carried out by scientists at Children�s Hospital Boston and Harvard Medical School, tested four types of music players on patients 6 to 60 with active pacemakers or implantable cardioverter defibrillators. The scientists found during hundreds of tests that the music players had no effect on the �intrinsic function� of pacemakers. The only slight effects occurred when the heart devices were being programmed, �but not in a way that compromised device function,� the authors said.

As a result, they concluded, patients need be wary only while their doctors are reprogramming their devices. 

Another study, in January by a scientist at the Food and Drug Administration, echoed those findings. The scientist, Howard Bassen, tested four different types of iPods and found that they did not hinder pacemakers.

