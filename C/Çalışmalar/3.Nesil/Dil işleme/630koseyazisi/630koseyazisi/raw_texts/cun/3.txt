Favori yine kaybetti 

 
 

Milan'an fark yiyen, y�ld�zlar� sakat olan F.Bah�e kar��s�nda evinde oynayan Cimbom'un favori olmas� do�ald�. Ama yine "Favoriler kaybeder" s�z� ger�ekle�ti ve G.Saray kendi sahas�nda hi� ummad��� bir sonu� ald�, �ampiyonluk yar���nda �ok b�y�k �eyler kaybetti.
Asl�nda F.Bah�e, Milan kar��la�mas�ndan biraz daha diriydi. Ancak G.Saray'� yenecek kadar iyi de�ildi. �lk yar� boyunca ilk pozisyonda gol yapt�. Sa� kanad� hi� i�lemedi, soldan k�t� olan Tuncay ile hi� do�ru d�r�st ��kamad�. G.Saray defans� Anelka ile Nobre'yi kilitledi�i i�in bu oyuncular da varl�k g�steremediler. Ta ki o yenen gol ve ikinci yar�da G.Saray'�n risk almas�ndan do�an Anelka'n�n sevdi�i pozisyonlar d���nda.

"G.Saray niye kaybetti" sorusunun iki cevab� var Birincisi; derbilerde �ok fazla gol pozisyonu olmaz. Yakalad�n m� atacaks�n ve b�yle basit gol de yemeyeceksin. G.Saray kendi evinde ilk 18 dakikada rakip kaleye gidemedi ama 18 ve 25 aras�nda 3 net gol pozisyonu var. Necati'yle, Orhan'la bunlardan birini gol yapsa ma��n g�r�nt�s� de skoru da �ok farkl� olabilirdi. Hele yenen gol inan�lacak gibi de�il. Hasan �a�'in top kayb�, iyi oynayan Tomas ve Song'un ��k�� an�nda Nobre'yi ka��rmalan ve Mondragon'un hatal� ��k���... Zincirleme hatalar sonucu her �eyin sonuna haz�rlayan gol... �lk yar�da gerek orta alanda, gerek defansta, gerekse de forvette zaman zaman iyi �eyler de oldu. Ama futbol hatalar oyunu. Gol de bu hatalar sonucu geldi.

�mit'i oyuna ge� ald�
Yenilginin e�er aranacaksa ikinci sebebi; Gerets. B�yle bir durumda yakalad��� ezeli rakibini evinde �ok daha farkl� bir kadroyla yenebilirdi, fendisin! defalarca kurtaran �mit'in kenarda oturmas� yapt��� ilk yanl��. �mit'i oyuna �ok ge� ald�. Tak�m�n b�t�n dengelerinin bozuldu�u, dengesiz h�cum yapmaya ba�lad��� bir anda �mit ancak o kadar yapabilirdi. Girdikten sonra G.Saray'�n 2-3 pozisyonunda imzas� var.

Hasan belki �ok iyi oynamad�, �ok top kaybetti ama ilk yar�da tam bir orta alan gibi oynad�, soldan bir f.Bah�eli gelemedi. Gerets onu almakla hem yenilginin su�unu Hasan'a y�kledi hem de giren isim Sabri da��n�k ve tela�l�yd�. B�yle bir ma�a orta alanda Ayhan'la ba�lanabilirdi. Vesaire...

Sonu�ta G.Saray b�y�k bir f�rsati tepti ve bu yenilgi korkar�m, tak�mda �ok daha olumsuz g�nlerin habercisi. 

 
