AKŞAM’a veda...

Bu benim AKŞAM’daki son yazım. 
2000 yılı Nisan ayından beri aralıksız devam ettiğim, 4 yıl Ankara Temsilciliği’ni yürüttüğüm, 5 yıldır da Yayın Yönetmenliği’ni yaptığım bu gazetedeki “serüvenimin” finalindeyim. 
“Hiç bir vedaya sözcükler yetmez. Her hoşçakal biraz eksik kalır”, bilirim. Ama her hikaye “samimi bir iç dökmeyi” hak ederek bitirilmeyi bekler. 
Gazetecilik asla bir kariyer değil, duruştur, “hayata karşı bir duruş.” 
Çok şanslıydım. Gazete yapmak en büyük, hatta tek hayalimdi. 5 yıl boyunca her gün son derece özgür bir ortamda gazeteciliğin bütün keyiflerini yaşadım. Bu açıdan öylesine mutluydum ki; türlü zorlukları memnuniyetle göğüsledim. Çok yoruldum belki, ama her gün huzurlu ve coşkuluydum. Ekip arkadaşlarımı tanımlamak için başka hiçbir kelime bulamam: “Olağanüstüler.” 
İlk günden bu yana ısrar ettiğimiz “muhabir odaklı ve özel habere dayalı gazetecilik” galip geldi, bunu herkes görüyor. Bütün bu süre boyunca her gün özel bir manşetle çıktık sizlerin karşısına. Sizler bizim yol arkadaşımızdınız, size mahçup olmamak adınaydı tüm uğraşımız. 
10 yıldan fazla zamandır içinde bulunduğu tüm olumsuz şartlara rağmen, AKŞAM’ın bugün sahip olduğu o algı, eriştiği o etki gücü başka nasıl açıklanabilir ki? 
Bizim yaptığımız gazeteye bakınca Türkiye’yi görürdünüz. Bir yarısını, bir kısmını değil, tamamını... 
Sadece dününü değil, bugününü ve yarınını da... 
Ülkemizin içinden geçtiği çok özel bir değişim döneminde, baştan beri hep “üçüncü yolu” savunduk. Tarafsız kalarak, kutuplaşmaları reddederek. 

MESLEĞİMİN RUHU İZİN VERMEDİ 
Bizim mesleğimiz birkaç günlük, hatta birkaç yıllık bir iş değildir. Yaptığımız haberler, hazırladığımız sayfalar, kullandığımız karikatürler, yayınladığımız yazılar hayatımız boyunca bizi takip eder. Yaşlandığımızda öz muhasebemizi yaparken hep karşımıza çıkar. Bizim mesleğin “asıl ağır yükü” işte budur. 
Hep bu bilinçle hareket ettik. Gayet tabii ki üzgünüm. Ama kimseye kırgın veya kızgın değilim. Hakkımın yendiği oldu ama bu dünyada görülecek hiçbir hesabım yok. Gelişmelerle ilgili herkesle gözlerinin içine baka baka konuşma imkanı buldum. Bu bana yeter. 
Genel Yayın Yönetmenliği özel bir konumdur. Patronların veya yönetimlerin özel tercihleriyle ilgilidir. O nedenle yapılan tasarrufa ancak saygı duyarım. Medya Grup Başkanı Cengiz Özdemir’den, başka bir formülle, hatta daha iyi şartlarda beraber çalışma teklifi aldığım doğrudur. Sağolsun, kendisi ısrarcı da oldu. “Keşke mesleğimizin ruhu ve benim doğam kabul etmeme izin verseydi...” 
Hem TMSF üst düzey bürokratları hem de Cengiz Özdemir’in yaklaşımları ve vizyonları gerçekten olumluydu. Onların istediği gibi “3-4 ay içindeki hızlı satış süreci”ne katkı verebilmek, taşıyabileceğim bir görevdi. Ancak sürecin seçimlere kadar uzama ihtimalini gözönüne alınca benim adıma mesele “sürdürülebilir” olmaktan çıkacaktı. Ayrıca şartlar, ilerleyen günlerde her zaman öngörüldüğü gibi gerçekleşmiyor. O noktaya gelince “zorlamamak”, karşılıklı teşekkür etmeyi bilmek gerekiyor. 

HUZURLA AYRILIYORUM 
Yaşadıklarımızın dramatize edilecek bir yanı yok. Hayatın olağan akışına uygun gelişmeler olarak görüyorum. Beklemediğim hiçbir şey olmadı. 
2008 yılında Genel Yayın Yönetmenliği’ne getirildiğim gün elime Hasan Cemal’in “Cumhuriyet’i Çok Sevmiştim” kitabını almıştım. Son üç gündür de Can Dündar’ın kaleminden Mehmet Ali Birand’ı okuyorum. Bizim gazetecilik tarihimiz biraz da kırılma ve kopuşlardan oluşan sarsıcı ve öğretici bir tecrübeler bütünüdür. Ustalardan öğrendiğimiz bütün bu kopuş ve kırılma süreçlerinin de geçici olduğudur.  
Büyük zaferler kazandığımı iddia edecek değilim. Ancak görevini yapmış insanların gönül huzuru ile doluyum. Hepinize ve her birinize içtenlikle teşekkür borçluyum. Şu son 3 günde aldığım telefonların sevinci ve gururu hayatımın sonuna kadar bana yeter. En kısa zamanda bir başka gazetecilik serüveninde ve durağında buluşmak üzere. Şimdilik hoşçakalın. 