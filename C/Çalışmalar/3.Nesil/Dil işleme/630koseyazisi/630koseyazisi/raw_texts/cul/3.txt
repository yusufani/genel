K�resel d�nyaya haz�rlan�yor muyuz? 
E��T�M �zerine yazd���m ilk yaz�da, pazartesi g�n�, e�itim sistemimizin nas�l bir insan tipi �ng�rd���n� belirtmi�tim: 

Asli niteli�i devlete ba�l�l�k olan, sorgusuz emir almay� ��renmi�, emir tekrar�nda ihtisasla�m��, ezberi g��l�, b�rokratik elite biat etmeyi hazmetmi� memurlar!

Peki k�resel d�nya nas�l bir d�nya?

Tek ve her santimetrekaresi ula��labilir ve bilgiye ula��m maliyetinin s�f�ra yakla�t��� bir d�nya.

Art�k d�nyadaki en �cra nokta, bir di�er �cra noktaya mal veya hizmet satabiliyor, bilgi �a�� internet �zerinden her t�rl� bilgiyi d�nyan�n her bir noktas�na ula�t�r�yor.

* * *

B�yle bir d�nya nas�l bir insan tipi dayat�yor?

�nlenemez, ka��n�lamaz k�yas�ya bir rekabete dayanabilecek,

Bilgiye ezber �ekmi� de�il, onu yorumlayarak fark�n� ortaya koyacak,

En �nemli niteli�i ekonomik performans� olan,

Verimlilik ve etkinlik �iar� ile yeti�mi�,

Bilgiye ula�acak becerileri kazanm�� ve ortak dili (�ngilizce) kullanabilen,

Uluslararas� normlara uygun �ekilde ihtisasla�m��,

Bilgi teknolojisinin dayatt��� �effafl��a ve hesap verilebilirli�e (demokrasi) g���s gerebilecek insan!

T�rk e�itim sistemi memur/g�ruh/cemaat/teba �retmeye �al���rken ve bu amac�na b�y�k ba�ar� ile ula��rken k�resel d�nya bize insan�m�z� �ahsiyet/birey yapmam�z i�in dayat�yor.

* * *

Ancak insan�m�z �ahsiyet/birey olursa:

Sorgulayan,

�ahsi tercihini ortaya koyan,

Y�netime do�rudan kat�lan,

De�i�ime kucak a�an

bir tipoloji yaratacakt�r.

B�yle bir insan tipolojisi sivil-asker b�rokratik elitin ba�akt�rleri oldu�u stat�konun i�ine gelmez.

Onlar e�itimin amac�n�n k�resel d�nyan�n dayatt��� insan tipinin tam tersi oldu�unu d���nmektedirler.

* * *

Her �eyden �nce k�resel d�nyaya uygun insan yeti�tirmek i�in e�itimin yap�s�n�n de�i�mesi gerekir.

E�itim:

Merkez egemenli�inden kurtar�lmal�, her bir okul birer �retim birimi olarak organize edilmeli ve birer k�r-zarar merkezi olarak y�netilmelidir.

E�itim evrensel de�er ve bilgiler ile yerel ihtiya�lar� ayn� anda meczetmek zorundad�r.

�niversite giri� s�navlar�n� gelir da��l�m�nda en tepe % 20'de bulunan ailelerin �ocuklar�n�n kazand���, halbuki vergi y�k�n� 2'nci ve 3'�nc� % 20'lik dilimin �dedi�i �lkemizde bedava e�itim fakirin zengini finanse etmesi gibi inan�lmaz bir z�rvay� yaratmaktad�r. E�itim paral� olmak zorundad�r.

Devletin e�itimde g�revi sadece ara�t�rma, denetim, standart geli�tirme ile s�n�rlanmal�d�r.

Devlet; kamu veya �zel okullarda okuyan ��renciler aras�nda sadece �deme g�c� olmayan ailelerin okul �cretlerini ve di�er e�itim giderlerini kar��lamal�d�r. 


