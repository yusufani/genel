G.Saray ve Wolfsburg! 

 
 

Ge�en hafta ba�kentte Ankarag�c� kar��s�nda izledi�im G.Saray, son 4 y�ld�r ortaya koydu�u futbolun en muhte�emini oynad�.

Ortaya ��kan 1-0'l�k sonu�, asla ortaya konan futbolun g�stergesi de�ildi. Sadece Ili�, rakip kaleci ile kar�� kar��ya kald��� pozisyonlar� gol yapsa, G.Saray'�n gol say�s� yar�m d�zineyi ge�erdi.

G.Saray'�n bu ma�taki en b�y�k �anss�zl���, �ok say�daki pozisyonu de�erlendirememesinin d���nda, ma�� "ruh g�z�" tr�b�nlerde olan Sel�uk Dereli gibi bir hakemin y�netmesiydi.

Belki �ok ki�iye g�re bu d���nce "�zel fantezi" say�lacak ama, Dereli'nin "ruh g�z�" yani d���ncesi kesinlikle tr�b�ndeydi. Sel�uk Dereli tr�b�nden ma�� izleyen Erman Toro�lu'nun, formas�n� uzun y�llar giydi�i Ankarag�c�'n�, "yedi�in ekmekler y�z�ne dizine dursun" dedirtmeyecek kadar sevdi�ini biliyordu.. Ve mutlaka ��yle d���n�yordu:

- "Ankarag�c� aleyhine �ald���m d�d�klerde falso yakalarsa, Maraton'da beni par�alar..."

"Par�ala Beh�et par�ala (!)" diyemeyecek kadar da cesur olmad���ndan, Galatasaray'�n iki mutlak penalt�s�n� �almad�. �ki futbolcusu hastanelik olan Galatasaray'� sert futbolda korumad�.

Korumas�z, adaletsiz bir hakemle oynamas�na ra�men, b�ylesine y�re�ini sahaya koyan
G.Saray, elbette alk��lanmal�yd�. M�kemmeldi, ger�ekten �ok g�zel oynad� G.Saray: "Futbolu art�k bitti" denilenler bile m�kemmel oynuyorlar.

Wolfsburg gibi olmas�n
D��tan g�r�nt�s� ile ger�ekten "aslan" g�r�nt�s� veren Gerets'in, G.Saray'� olumlu y�nde de�i�tirmeye ba�lad��� kesindi. G.Sarayl� futbolcu ve y�neticiler Hagi gibi "ge�imsiz" bir d�nemden sonra Gerets ile sanki s�k� s�k� birbirlerine sar�lm��lard�. 

Fakat... "Acaba?" diyece�imiz bir durum da vard� ortada...

Gerets, ge�en sezon da Almanya'da Wolfsburg ile zirveyi "dut" silkeler gibi silkelemi�ti... Almanya Ligi'nin 9. haftas�nda Stuttgart'� ge�ip liderli�i ele ge�irmi�, tam 5 hafta yan�na kimseyi sokmam��t�... 

Fakat 14. haftada Hamburg'a 3-1 yenilip liderli�i kaybettikten sonra patinaj yapmaya ba�lam��, ligi 9. bitirmi�ti.

Gerets G.Saray'la da ger�ekten muhte�em bir ba�lang�� yapt� ama... Sonu gelecek mi, yoksa "havanda su d�ven" bu y�netimle G.Saray'�n gelece�i de Wolfsburg'a m� benzeyecek

Fatih hoca sen de dur!
Hani me�hur hikaye;

Dadayl� asker olmu�... Komutan "tak�m dur" deyince tak�m duruyor, ama bizim Dadayl� y�r�meye devam ediyor... Bir-iki-��... Bak�yor ki de�i�en durum yok, komutan, komutu de�i�tiriyor:

- "Tak�m dur... Dadayl� sen de dur!..."

Sofya hezimetinden sonra bas�n sallamaya ba�lad�. Teknik direkt�r Fatih Terim de geri kalmad�...

�imdi "Bas�n dur, Fatih hocam sen de dur" dense cuk oturmaz m�?

Yahu arkada�lar, 3 Eyl�l'de Danimarka ile oynayaca��z... O ma��n sonucuna g�re D�nya Kupas�na gitme umudunun kap�s�n� ya aralayaca��z, ya kapataca��z.

�urada 3 Eyl�l'e ka� g�n kald�? Vurmakla, da��tmakla... "Aman ne g�zel yazm��s�n ko�um" u�runa kalem oynatmak, ya da laf ebeli�i yapmakla ne kazanaca��z?

L�tfen;

Bas�n dur!

Adanal� Hocam, sen de dur! 

 
