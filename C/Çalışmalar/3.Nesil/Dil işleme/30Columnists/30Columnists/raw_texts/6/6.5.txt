Ending discrimination against women and Roman Catholics in the Royal succession should be simple, but it originates in bitter 300 year old arguments about the nature of the British state. 

At one level, yesterday�s private member�s bill from Dr Evan Harris, a Liberal Democrat MP, was merely intended to end an obvious anomaly. A big majority of voters think it is wrong that men get precedence over women in the line of succession and that heirs to the throne cannot marry Roman Catholics. 

Admittedly, any change involves negotiations both with Buckingham Palace, already begun by Gordon Brown, and with the 15 other Commonwealth countries where the Queen is still head of state. Jack Straw told MPs that Dr Harris�s bill was not the �appropropriate vehicle�. The bill was �talked out� without a vote, so will make no further progress. There is no timetable for when action will be taken. 

The easiest part is ending the preference for men, which originates in ancient property laws - not least because some of the most successful monarchs have been female, including the present Queen. 

Laws against monarchs or their heirs being Roman Catholics, or marrying them, go back to the ousting of James 11 and the arrival of William of Orange to become King in 1688. The Bill of Rights of that year, the cornerstone of British freedoms and rights, states that �it has been found by experience that it is inconsistent with the safety and welfare of the Protestant Kingdom to be governed by a popish prince or by any King or Queen marrying a papist�. That was also central both to the Act of Settlement of 1700 securing the succession to the Protestant house of Hanover, rather than to James and his Catholic heirs, and to the Act of Union with Scotland. 

The threat from the Jacobites and the French has, however, receeded since the early 1700s. These provisions were intended to safeguard the Church of England. Defenders of its position as the established Church argue that ending religious discrimination would raise questions about the monarch�s role as Supreme Governor of the Church of England. The Harris bill would only have lifted the ban on marrying Roman Catholics, but not the bar on the monarch being a Roman Catholic. 

The �time is not ripe� argument often prevails in such cases with tricky implications. But it is often more sensible to act when an issue is not urgent. 

