#include <stdio.h>
#define TO_LOWER(e) ((e)+ ('a' - 'A')) // alınan harfi buyuk harfe cevirir
#define min(a,b) ((a)<(b) ? (a):(b) )
#define CHECK(a,b) \
if(a!=b) fail(a,b,__FILE__,__LINE__)
void fail (int a , int b , char *p, int line){
    printf("Check failed in the file %s at line %d expected %d reiceved %d",__FILE__,line,a,b);
}
int main() {
     int x=1,y=2,z;
     char c1, c2;
    c1='F';
    c2=TO_LOWER(c1);
    printf("after the conversion: %c\n", c2);
    z = min(x++, y);
    printf("x: %d, y:%d, min value: %d\n", x,y,z);
   
    printf("This utility is compiled on %s at %s\n",  __DATE__, __TIME__);
    if(__STDC__==1)
	    printf("This utility is convenient for the ANSI standards \n");
	else
	    printf("This utility is not convenient for the ANSI standards \n");
}
