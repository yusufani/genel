A�k �tanazi istiyor 
Hep diyorum ki... �u �b�y�k a�k�lar� bir kenara not edeyim. Fakat ihmal ediyorum. 

Amac�m takip etmek. Bir nevi bilimsel ara�t�rma. Hani sonu nereye var�yor, nihayetinde birbirleri i�in neler s�yl�yorlar, biti�i de muhte�em oluyor mu, falan filan.

Ama bu sefer i�te �uraya not ediyorum:

A�k�n taraflar�: Nez-Davut G�lo�lu

A�k�n b�y�kl���: �ok b�y�k, nah bu kadar!

A�k�n �iddeti: Her biri saatte 200 km. h�zla gitmekte olan iki kamyonun �arp��mas� kadar.

A�k�n dili: �Sonunda adam gibi bir adamla beraberim.�

�Y�llarca a�ka inanm�yordum ama demek ki varm��.�

��u an kamyon �arpm�� gibiyim.�

�24 saat onunla beraberim ama zaman�n nas�l ge�ti�ini bile anlam�yorum.�

�B�y�k bir a�k ya��yoruz.�

��u an mutlulu�un doru�unday�z.�

��u an ya�ad���m g�zelli�in tarifini yapamam.�

�E�i benzeri g�r�lmemi� bir d���n yapaca��z. �yle ki bu yery�z�n�n d���n� olacak.�

Bilmemka��nc� Noter Pakize Suda!

�10.2.2005 tarihinde b�romda oturmaktayken, saat 14.32�de kap� a��ld�...�

Galiba b�yle ba�lan�yordu. Fakat kimse kalk�p bana gelmedi tabii ki. Ben g�n�ll� noterim. Ayn� zamanda i�killi. �B�y�k a�k�lardan i�killeniyorum. Nez�le ya da Davut G�lo�lu�yla bir al�p veremedi�im falan yok. Bakmay�n, sadece kabak onlar�n ba��na patlad�. Bu aralar �en b�y�k� onlar�nki oldu�undan...

Asl�nda noter de�ilim ben. Avukat�m. Hakikaten b�romda oturmaktayken kap� a��ld�, biri girdi i�eri...

�M�vekkiliniz olabilir miyim?� diye sordu.

Kullan�lmaktan �ik�yet�iymi�. �olu�un �ocu�un, bilenin bilmeyenin, anlayan�n anlamayan�n dilinin persengi olmaktan... Fakat ne y�zle savunay�m... Ben de ba�ka t�rl� kullan�yorum kendisini. Kalemime dolad�m.

Yok, asl�nda doktorum ben. Kap�ma gelen de �tanazi isteyen bir hasta. �Yerlerde s�r�n�yorum, �lsem daha iyi.�

Ben de diyorum ki �Size iyi bir haberim var, zaten �oktan �ld�n�z, ortada gezen suretinizdir.�

Et el de�i�tirdi

Ta� devrinden bu yana insano�lunun de�i�medi�ini ��renmi� bulunuyoruz. O zamandan bu zamana ya�anan onca heng�me, ke�ifler, icatlar, �u bu... Fasa fiso. �nsan denen yarat�k �z�nde o zaman neyse �imdi de o.

Zaten g�nl�k hayatta �e�itli vesilelerle bunun b�yle oldu�unu idrak etmi�li�imiz vard� da yetkili a��zdan da duyduk sonunda.

Erciyes �niversitesi T�p Fak�ltesi Psikiyatri Ana Bilim Dal� Ba�kan� Do�. Ertu�rul E�el suskunlu�undan �ik�yet etti�imiz erkeklerin bu �zelli�inin eski �a�lardan geldi�ini belirtmi�. �Eski �a�larda erkeklerin eve et getirmesi yeterliydi. Kendisinden konu�mas� beklenmiyordu. Binlerce y�l bu tarzda ya�ad�ktan sonra erkeklerin de�i�mesi bekleniyor� diyor E�el.

Yani �erkeklerle ileti�im kuraca��m diye bo�una u�ra�may�n� demeye getiriyor. �leriki y�zy�llarda durumlar�nda bir de�i�iklik olur mu olmaz m� bunu belirtmemi� E�el.

Fakat ben umutluyum.

�Neden?� derseniz, �Eve et getiren ka� erkek kald�?� diye sorar�m size. Ya�ad���n�z kasabadaki erkekleri d���nmeyeceksiniz hemen... Bilimsel tespitler t�m insanl�k alemini kaps�yor, ona g�re verin cevab�n�z�!

Ya da ben sizin yerinize vereyim de mevzu y�r�s�n. Et, el de�i�tirdi arkada�lar!

Eski �a�larda evde et beklerken bir yandan da habire konu�tu�u s�ylenen kad�n, konu�mas�na ara vermedi ger�i ama beklemekten vazge�ti, eti kendi temin etme yoluna gitti.

Bak�n �imdi kendili�inden ne ��kt� ortaya... Yani benim kay�rmam falan de�il. �nsano�lu her ne kadar de�i�mediyse de kad�n k�sm� hi� olmazsa et hususunda a�ama kaydetmi�. �Bekleme�den �Getirme�ye ge�mi�. Erke�in elinden alm�� �stelik bunu. Erkek k�sm� yeg�ne meziyetini kaybetmi� b�ylece.

Ama i�te bu bir yandan da erkekler a��s�ndan iyiye i�aret. �Umutluyum� demem bundan. Demek de�i�meye meyilleri var. Belki biraz zamana ihtiya� duyuyorlar. Birka� y�zy�l kadar. Bir bakm��s�n�z ki, yani torununuzun torununun torununun torununun torunu bakm�� ki erkekler b�lb�l olmu�!

MI�-MU�

AKP�den istifa eden Afyon milletvekili Reyhan Baland� �AKP�de kad�n sadece dekor� demi�.

CHP�de de ayn� merak etmeyin! T�rkiye�de kad�n�n adamdan say�ld��� parti hen�z kurulmad�.

�stanbul Valisi Muammer G�ler kar i�in �Bu sefer iyiydik� demi�.

Ola�an duruma ��kretti�imizi kimsecikler duymas�n!

Ge� do�um �m�r uzat�yormu�.

�Do�um izni� �teki tarafta da var demek.

Turizm 2005�e h�zl� ba�lam��. Biz onun h�z�n� kesecek bir �eyler yapar�z evvel Allah! 
