Bizim büyük azınlığımız

Ak Parti'nin %44.18 ile kazandığı yerel seçim sonrasında, Türkiye'nin %55.82'i azınlığa düştü!
Pazar günü ailece seçim sandığına oy kullanmak için gittiğimde çok mutlu oldum. Oy vermek için sıramızı beklerken 2 yaşındaki oğlumun peşinde diğer sıralarını bekleyen insanların arasında koştururken bir ara gözlerim doldu. "Ne kadar güzel" diye içimden geçirdim. "İnsanlar kendi geleceklerini belirlemek için sevdikleri partiye özgürce oy veriyorlar. İradelerini kullanıyorlar. Ortadoğu coğrafyasında ne kadar büyük bir lüks" diye düşündüm. 

Oylar verildi. Sandıklar açıldı ve pazar akşamı yerel seçimlerde kazananlar ile kaybedenler ortaya çıktı.
Dün sabah oturup uzun bir yerel seçim değerlendirmesi yazısı yazdım. Sonra sildim. Baktım yazdığım yazıda yeni bir şey yok. Herkesin (ben dahil) onca canlı yayında söylediği sözleri, yazdığım yazıda üç aşağı beş yukarı geveleyip durmuşum.

Türkiye bir yerel seçim sonrasında tanıdık bir zafer sarhoşluğu ve bildik bir kaybetme hüsranı daha paylaşıyor. Kazananların ya da kaybedenlerin cephesinde önceki seçimlerle kıyaslandığında pek bir fark yok. Tek fark yeni oluşan siyasi psikolojide ve yeni Türkiye atmosferinde oldu sanırım. Bu ruh hali sonuçlardan bile önemli hale geldiği için bundan bahsetmek gerektiğini düşünüyorum. Daha önceki birkaç seçim gecesinde en azından sembolik de olsa bir ‘balkon’ konuşması yapılır, birlik beraberlik mesajı verilirdi. Haftalardır seçim nedeniyle meydanlarda yükselen tansiyon biraz olsun indirilip, normalleşme sinyalleri verilir ve "Önümüze bakalım" denilirdi. 
Oysa bu balkon konuşmasında gördük ki 2014 yerel seçimleri, ağustostaki Cumhurbaşkanlığı seçimi için sadece bir ara durak olmuş. 
Günlerdir sızdırılan tapeler, fezlekeler sonrasında Başbakan’a ve ailesine moral vermiş, siyasi kararlılığı perçinlemiş, hırslandırmış. 
Yoksa kızgınlık aynı. Öfke aynı. Kendine güven bin misli artmış. 

Bu seçim sonrasında benim diğer seçimlerle kıyasladığımda gördüğüm tek fark ufak da olsa kitleleri birleştirme ihtimali olan o simge ‘balkonun’ yıkılması oldu. Bu yeni ruh halinin en somut yansımalarını iktidarı destekleyen gazetecilerin köşelerindeki zafer sarhoşluğu satırlarından görebiliyorsunuz. Daha geceden listeler yapılmaya başlanmış;
muhalif gazeteciler işlerinden attırılmış, gönderilecekleri cezaevi beğeniliyor! 

Seçim yarışını kaybetmiş partilere yeni genel başkan isimleri öneriliyor! 
İktidarı desteklemediği için ‘bitirilecek’ işadamlarının listeleri yayımlanıyor. Seçim dönemi objektif duran yayın gruplarının devlet düşmanı ilan edileceği haykırılıyor. 

Onca yolsuzluk iddiası, ses kayıtları, fezlekeler sıfırlanmış gibi bir hava yaratılıyor. 
'Cemaat'in bu tehditler sonrasında zaten artık yatacak yeri kalmamış gözüküyor!
Bunlar ve onlar ayrımı pekiştirilip, netleştiriliyor.

Bu manzaraya bakınca pazar günü yerel seçimlerde muhtarı, belediye başkanını değil de sanki yeni bir rejimi oylamışız da onun sonucu açıklanmışa benziyor.

Belki biz farkında değiliz. Belki gerçekten öyle oldu. Biz sandık başına belediye seçimleri için gittiğimizi sanarken meğerse rejim değişikliği oylaması için gittik haberimiz olmadı.

Ak Parti'nin %44.18 ile kazandığı yerel seçim sonrasında, Türkiye’nin %55.82’si azınlığa düştü! 

Hay Allah!

FABRİKA AYARLARINA DÖNÜŞ

- Madem Türkiye’de belediyeleri yine Ak Parti yönetecek, madem halkımızın teveccühü bu yönde o zaman artık Başbakan ve ailesi ile ilgili her türlü rüşvet ve yolsuzluk iddiasını unutabiliriz! Belediye başkanlık seçimlerini Ak Parti kazandığına göre mahkemeye gerek kalmadı onlar da ‘ak’lanmış demektir.

- Bakanların yolsuzluk fezlekelerini Meclis’te şöyle yüksek bir rafa kaldıralım. Öyle ya Antalya Belediye Başkanı Ak Partili Menderes Türel oldu. 

- Ankara’da büyükşehir belediye başkanlığını Melih Gökçek kazandığına göre Suriye’de muhaliflere gönderildiğini öğrendiğimiz 2000 TIR’lık askeri yardımı hemen hafızalarımızdan silebiliriz. Evet nerde kalmıştık; Süleyman Şah Türbesi ile ilgili planları konuşuyorduk… 

- Şanlıurfa’da BDP belediye başkan adayı Osman Baydemir kaybedip, Ak Parti’den Celal Güvenç kazandığına göre hepimizin desteklediği Kürt barışını sağlam kazığa bağlamaya da gerek kalmadı. Barışı sağlama almak için Meclis’ten yasa çıkartmaya gerek yok! 

- Gaziantep Belediye Başkanı Fatma Şahin olduğuna göre Alevilerin bin yıldır verilmeyen haklarını dile getirmenin artık bir önemi kalmadı!

- Dünya yerel seçimlerde Ak Parti’nin bu büyük zaferini tescil ettiğine göre Twitter ve YouTube’dan sonra Facebook’un da özgürlüğüne bir Fatiha okuyabiliriz.

- Bursa’da yerel seçimleri Ak Parti silip süpürdü… O zaman “Ey AB ruhu gelince üç kez kapıyı çal!”

- Hopa’yı bile Ak Parti almış ya… E, o zaman hadi bize eyvallah!