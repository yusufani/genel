How, it is asked, are ordinary football supporters supposed to relate to the mind-blowing wages Manchester City have offered Kaka in this time of recession? Easy, really. The same way they relate to Paul McCartney. 

Estimates vary, but a ballpark figure suggests the former Beatle earns in the region of �40million per year. It doesn't make The White Album any less impressive, though. You wouldn't discard it from your collection on the grounds that you and the artist had little in common as human beings. 

The same applies to a trip to the cinema. People like blockbuster films with highearning stars. This is why the next in the James Bond series is more eagerly anticipated than the new one from Jim Jarmusch.
 
Take the Kaka argument to its logical conclusion and nobody would have paid to see Frank Sinatra, either. 'He's talented, he can sing, he's fabulously wealthy, how can I relate to that? I prefer Charlie Scuttles at the Rose and Crown. He's tone deaf and can't play the piano. Just like me.' 

Only sport has to affect this literal association with the community. Tom Finney went to work on the bus, we are self-righteously told. True, but Tom Finney was getting royally shafted by chairmen who were the equivalent of Victorian mill owners. What happened was not right. Finney played in the era of the biggest attendances in English football, he was the man the fans were turning up to see and he received a maximum of �14 a week with no freedom of contract. Non-League players were better rewarded because their part-time income supplemented a full-time job. 

The pay offer Jerry Seinfeld turned down to continue his NBC show beyond its final season was $5million per episode. The only reason the network could afford to pay that was because the show was generating even more in advertising, so it represented an investment. The same is true of Kaka. 

Manchester City are now funded as a way of publicising the resort potential of the state of Abu Dhabi and if this cross-fertilisation is successful, the money paid for one attention-grabbing transfer will represent a fraction of what is later accrued.
    
Dave Whelan, the Wigan chairman, was complaining recently about rows of empty seats at his stadium. Well, they wouldn't be empty if Kaka were playing. 

Manchester City are not living in the real world, according to Arsene Wenger, the Arsenal manager and, as usual, commentators nod sagely at his wisdom. Yet how is this so? The sheik is real, Manchester City is real, Kaka is real and that cheque is real, so why is it not the real world? 



It is a different world from that inhabited by Arsenal, yes, just as Wenger's reality is different from that of Tony Pulis at Stoke City, propped up, as it is, by the inflationary rewards of the Champions League, a tournament that allows a manager to come fourth and be successful. 

Ron Atkinson would have liked that at Manchester United. He achieved similar League positions to those occupied by Wenger in recent years and got the bullet. Back then, first was first, second was nowhere and fourth might as well have been Siberia. So is Wenger not in the real world? No, this is his reality, just as it was Finney's to commute on public transport and David Beckham's to own, not a car, but a fleet. 
Whenever Wenger talks on financial issues we are solemnly informed that he has an economics degree as if this precludes further discussion; strange, because people with economics degrees have not been looking too bright lately. 

There are plenty of economics degrees flying about among the people responsible for the global financial crisis, for instance. Chances are the person who screwed up your pension and recommended the endowment policy that no longer covers your mortgage is a refugee from the London School of Economics, too. Since when did a degree become a full stop in any debate about fiscal matters? 

It is fair to assume that if Kaka signs for Manchester City, at least part of his �20m annual salary will be susceptible to PAYE, currently running at 40 per cent and very soon 50 per cent. So, in answer to the standard question about how many hospitals could be built for the money Kaka is earning, the reply is considerably fewer if he stays in Italy. 

His salary is money coming directly from Abu Dhabi and a slice is going into the Government's pot. Now there is an equation to which we can all relate, even without a first from the LSE.
