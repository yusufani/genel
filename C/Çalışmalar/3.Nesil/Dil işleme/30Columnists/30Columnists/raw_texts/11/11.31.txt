Taking multi-vitamin supplements as 'health insurance' to help you live longer is a waste of time, warn researchers. 
The biggest ever study of its type found no benefits from vitamin pills against cancers or heart disease, and they did not protect against dying prematurely. 
Sales of supplements have exploded to more than � 330million a year in the UK, with one in ten adults regularly buying them. However, there is little evidence of any benefits. 
It is thought the results of studies showing a protective effect may have been skewed by the type of people habitually taking vitamins.
 
They also tend to look after their health in other ways, such as eating plenty of fruit and vegetables and doing exercise - making it hard to determine if supplements are responsible. 
The Food Standards Agency has warned that taking pills is no substitute for a healthy diet. 
Scientists in New York combined data from two Women's Health Initiative trials involving 161,808 post-menopausal women aged 50 to 79 years. 
Some 41 per cent took vitamin pills for a total of 15 years - mostly multi-vitamins with minerals, says a report in the Archives of Internal Medicine journal. 
There was no beneficial effect from multi-vitamin use on the risk of breast, colorectal, endometrial, kidney, bladder, stomach, ovary or lung cancers. 
Researchers also found no link between using vitamins and the risk of heart disease, or death. The findings also showed multi-vitamins did not increase the risk for these conditions. 
The researchers, from Albert Einstein College of Medicine of Yeshiva University, said women were more likely than men to take supplements and the proportion taking them went up after the age of 30. 
Sylvia Wassertheil-Smoller, professor of epidemiology and population health, said: 'Based on our results, if you fall into the category of the women described here, and you do in fact have an adequate diet, there really is no reason to take a multi-vitamin.' 
But it may still benefit some women to take multi-vitamins, she added, because the women in the study were relatively well-educated and had better health habits than the general population. 
The Health Supplements Information Service in Britain said yesterday that vitamin supplements help people with poor diets. 
Dr Carrie Ruxton, scientific advisor to HSIS, said: 'A significant proportion of British people, particularly women, young children and teenagers, have inadequate intakes of key nutrients, such as iron, folate, calcium, vitamin D and vitamin A. 
'As a nation we are still failing to meet the five-a-day fruit and vegetable intake targets. As a result, multi-vitamins remain a valid way of supplementing the diet, alongside improvements in healthy eating.' 
Nutritionist Rachel Di Leva, from HSIS, said European regulations prohibit claims being made for supplements without 'robust substantiation'. 
She added: 'The Advertising Standards Authority is also vigilant. Reputable multi-vitamin products do not carry claims that cannot be substantiated, and this protects consumers.' 
Dr Ruxton said: 'Vitamins and minerals are not intended to be used like drugs. 
'Both cancer and heart disease are conditions with multiple causes such as genetics, smoking, obesity and diet, so it is unlikely that simply taking a multi-vitamin supplement would prevent them. 
'But a multi-vitamin, for instance, could be the answer for someone leading a hectic, busy lifestyle, where meals are skipped or taken on the run.' 
