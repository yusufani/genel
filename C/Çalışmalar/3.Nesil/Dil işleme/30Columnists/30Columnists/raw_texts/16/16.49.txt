Christine Baze of Marblehead, Mass., was 31, married and hoping to start a family when she learned she had invasive cervical cancer and would need a total hysterectomy. 

Now 36 and childless but free of cancer, she is waging a campaign to keep what happened to her from afflicting other women. She created a Web site, popsmear.org, and the Yellow Umbrella Tour, a national concert tour, to spread the word.

"A Pap test is not enough," she now knows. "I was tested every year, and my cancer was missed. There is now new technology - a liquid-based Pap and HPV testing - that wasn't available to me."
HPV, the human papillomavirus, is a cause of genital warts and cervical cancer. Though this cancer was long recognized as a sexually transmitted disease, not until the 1990's was HPV identified as the likely culprit. In the years since, medical researchers have slowly unraveled the complex relationship between this very common virus and the not-so-common cancer it can cause.

What they have learned is altering the way women should be examined to detect precancerous changes in cervical tissue, the recommended frequency of the exams and how doctors should treat women with these cervical variations.

A Ubiquitous Virus

Over 100 types of HPV have been identified, and about 15 of them cause nearly all cases of cervical cancer. These are called high-risk types, or oncogenic. Two, HPV 16 and 18, together account for about 60 percent of cervical cancers. 

But becoming infected with an oncogenic virus only rarely results in cancer. Even when it does, a very long time elapses between the initial viral infection and the development of malignancy, making it possible for doctors to intercept and prevent cancer.

Low-risk HPV's more often cause genital warts; they rarely, if ever, result in cancer. 

An overwhelming majority of sexually active women can expect to be infected with one or another HPV. In one three-year study, 60 percent of 608 college women tested positive for HPV at one time or another. Infection is most common in young women - from the mid-teens through the mid-20's - with the peak incidence occurring at age 19. Often, several types of HPV infect a woman's genital tract at the same time. 

Most HPV infections are subclinical, showing no outward sign. Similarly, the infections often suggested by cellular changes can go undetected in a Pap test. Three studies of college-age women indicated that hidden HPV infections may be 10 to 30 times as common as those that cause cellular abnormalities.

Among older women, the lower rate of HPV infections is independent of their sexual behavior, strongly suggesting that over time and with multiple viral exposures, women develop immunity to many HPV types.

While it is rare for a virgin to be infected with HPV, a woman need not have sexual intercourse to become infected, said Dr. Gregory L. Brotzman of the Medical College of Wisconsin. Dr. Brotzman, who wrote about HPV diseases in The Journal of Family Practice in July, explained that the virus is spread through contact with infected genital skin, mucous membranes or fluids from an infected partner. Any form of sexual encounter - including oral-genital and manual-genital contact - can spread the virus.

Condoms and diaphragms only partly protect against an HPV, Dr. Brotzman said, since the virus can infect tissues not covered by barrier contraceptives. Further, these contraceptives may not be used until after genital contact.

Factors that increase a woman's chances of acquiring HPV include having multiple sexual partners (especially those who have had multiple partners), having another sexually transmitted disease, cigarette smoking (including previous smoking), pregnancy and any condition or treatment that impairs immunity.

The Immune System at Work

In most cases, however, the body's immune system knocks out the virus over a period of months or years. When this happens, genital warts clear up and cervical cells harboring the virus do not gradually turn malignant. 

Among the 608 college women studied, for example, HPV infections lasted eight months on average, and after two years, only 9 percent of the women were still infected. Three studies have indicated that when their partners use condoms, women are more likely to clear the HPV infections and the cervical cell abnormalities.

Next to abstinence from all sexual contact, long-term monogamy with a single sexual partner who is also monogamous is most likely to prevent an HPV infection. A reduced risk of viral transmission has been found among men who have had a prolonged period of abstinence and those who have been circumcised.

Detecting a Cancer Risk

Sometimes, a woman's HPV infection persists, and when the virus is an oncogenic type, her risk of eventually developing cervical cancer increases significantly. Knowing this, physicians have devised screening recommendations that go beyond simple Pap smears, which can miss as many as half of incipient cervical cancers.

In April, the American College of Obstetrics and Gynecology published new guidelines for detecting and dealing with abnormal cervical changes and HPV infections. The new screening schedule depends heavily on a relatively new test for the DNA from 13 high-risk types of HPV in women with or without abnormal changes in cervical cells. The target population for this test is women 30 and older who are less likely to have wiped out the viral infection on their own.

Every year in the United States, millions of women are found to have abnormal cellular changes on the cervix. But only some of these lesions are associated with a cancer-causing virus and likely to progress. 

The new screening guidelines are meant to be a cost-effective way to distinguish between those who are likely to clear the cellular problem on their own and those who may develop cancer or already have it and, therefore, need more frequent screening or further treatment.

The best way to check for both cell abnormalities and HPV is to have a liquid-based Pap and, using the same fluid, a test for high-risk HPV.

Women 30 and over with negative results on both tests should be rescreened no more often than every three years. Those with a negative Pap but who are positive for HPV should have both tests repeated 6 to 12 months later. Those with atypical squamous cells of undetermined significance, or Ascus, on the Pap but are negative for HPV should have a repeat Pap in 12 months.

Those with Ascus and who are positive for HPV should undergo a colposcopy, a microscopic look at highlighted cervical cells, and biopsy. Those with cell changes more ominous than Ascus should undergo colposcopy regardless of the HPV results.

Two HPV vaccines are in the final stages of testing. One, by GlaxoSmithKline, protects against the two HPV types, 16 and 18, that are most often involved in cancer. The other, by Merck & Co., protects against these two and two others, HPV 6 and 11.

When either vaccine is approved, the ideal group to be immunized will be girls and boys 9 to 15 who aren't yet sexually active. Studies show that education by doctors and an explanation of the high rate of infections among older teenagers can help overcome parents' objections.

