This time of year, we tend to seek stories of courage and inspiration. I think I have one. 

Last April, Reggie Williams drove up from Florida, expecting he might need to outfit his car with special controls for driving with his left leg, in case his right leg was amputated.

Williams is a former linebacker for the Bengals who played in two Super Bowls and was a supporter of Nelson Mandela while a member of the Cincinnati City Council. But he limped away from football with artificial knees, and a lingering infection in the right one, and last spring he left a lucrative job with Disney to see if doctors in New York could save the leg.

When I caught up with him, he was just out of the hospital, where doctors had saved him from a leaky aneurysm in his leg. He was resting in a Manhattan apartment, his leg a moist, purple horror refusing to heal. A few weeks later we went out for lunch; he had to stick a Velcro IV unit on the wall so the antibiotics could drip into the leg. Friends privately shuddered at his prospects. 

�If I doubted for a moment, the fear was real,� he admitted the other day.

Illness and bravery were twin themes I kept encountering in the past year, as good people fought back. John Fernandez, a West Point graduate wounded in Iraq, was playing lacrosse on prosthetic feet and working for the Wounded Warrior Project. Robin Roberts was gracing the camera for ABC after treatment for breast cancer. (The other day Robin e-mailed me, recalling how last year she was �too bald and bloated from chemo� to be with family, but on Friday she is heading home to Mississippi. �I don�t take anything for granted,� she added.)

In the past month I have come to know Michael Goldsmith, a lawyer and baseball buff who is valiantly lobbying for research to fight Lou Gehrig�s disease after it sneaked up behind him. And over the summer I wrote about Eric Shanteau, who put off surgery for testicular cancer so he could swim in Beijing. The other day The Associated Press caught up with Shanteau � in the pool, of course. 

Such brave people. Sometimes when I reached Reggie Williams, he sounded cool, but it turned out he was in the hospital. In July, the leg began to heal. On Aug. 19, he received a second artificial knee. On Sept. 5, he had another procedure to clean the implant. Then he began reintroducing his linebacker body to the complexity of motion. He thanks Dr. Stephen J. O�Brien and all the other medical stars at the Hospital for Special Surgery. 

When I asked what kept him going, Reggie said it was love from God, although not necessarily religion. In his darkest days, he said, he was buoyed by reading Viktor E. Frankl�s enduring book, �Man�s Search for Meaning,� the perspective of a Holocaust survivor. �He had a chance to escape, but he didn�t do it,� Williams said. �He had to maintain a certain sanity and not have negative thoughts.�

Williams was sustained by a network of great friends, many of them African-Americans of talent and character who had been recruited to Dartmouth College in the tumultuous but idealistic �60s and �70s � the generation that led to our new president, to new hope.

While Reggie was recuperating, he met somebody, which made it all the harder for him to realize he could not get in enough walking on the crowded sidewalks of the West Side, particularly with warning blasts of winter freezing up his reconstructed leg. 

In mid-November he visited his friend Dr. Tom Price, who had played football before Williams at Dartmouth. Reggie had an ulterior motive � his white Lexus was waiting in Price�s garage in the suburbs. 

�I didn�t know if it would start,� Reggie said the other day. �I turned the key and got comfortable, and I plugged in my iPod,� loaded with music of joy and grief, and songs for the open road. 

�Their driveway is narrow,� Reggie recalled. �I backed up, and then I said to myself, �I can go.� � But he was not going anywhere without the permission of Price�s wife, Patricia, who was born the same day Reggie was, which gives them a psychic connection. 

�Patricia is very pragmatic,� Reggie said. �I had to prove to her I could drive.� He drove her into the city as she grilled him on the pitfalls of driving with a rebuilt leg. He promised he would go two hours, then rest for two hours, but as soon as he dropped her off, he began packing his car. The next morning he headed down the turnpike, with no more than 60 degrees of motion in his right knee, not ideal for driving. 

After two hours, he stopped for coffee and decided there was no turning back. After stopping to visit his brother in Atlanta, he got home to Orlando in time for his son�s football banquet. 

As the holidays approach, I smile whenever I think of Reggie barreling down the interstate, a linebacker who got his mitts on the football, heading for the end zone. I wish the same blessing he received for all the other good people I met this year.

