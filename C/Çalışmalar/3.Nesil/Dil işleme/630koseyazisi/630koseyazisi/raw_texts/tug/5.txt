Vurun abal�ya kampanyas� 

 
 

Ne kadar insan�n me�er derdi varm�� �clal Ayd�n ve Tuna Kiremit�i�yle! K��ecilerimiz vurmak i�in me�er en ideal zaman� bekliyormu�! 

Kimsenin avukatl���n� �stlenmi� de�ilim. �clal�i tan�r�m, severim, Tuna Kiremit�i�yle ise tan��m��l���m bile yok. Fakat derdim tan�d���n sevdi�im birini koruma kollama hadisesi de�il. Derdim yeni filizlenmi� bir ili�kiden yola ��k�larak kopart�lan g�r�lt�ye, kusulan �fkeye ve alayc�l��a duydu�um tiksinti. 

Ne ahlaklar� kald�, ne edebiyat��l�klar� kald� ne yapayl�klar� kald� ne gamzeleri kald� ne de yeni yapt�r�lan di�leri kald� de�ilmeyen. Bir haftad�r utanmaz bir kampanya y�r�t�l�yor. Alt taraf� da �st taraf� da iki insan �evet �nl� ve pop�ler iki insan- birbirlerine a��k olmu�lar, beraber olmaya karar vermi�ler. 

Bu kadar m� katlan�lmaz bir �eydir bu? Bu kadar m� ay�pt�r? Bu kadar m� alaya mahzar bir �eydir bu? E�lerinden ayr�l�p ba�ka bir ili�kiye giren ilk insanlar onlar m�d�r? Mutsuz bir ili�kiyi �ocuklar u�runa s�rd�rmek bu kadar m� kahramanca bir �eydir? Bu kadar m� m�himdir? Bu kadar m� d�nyan�n ba�� ve d�nyan�n sonudur? Yery�z� �zerinde daha faydal� ba�ka tek bir �ey yok mudur? Bu mudur bizi cennetlik yapacak tek �ey?

Kimse kimsenin ili�kisini yarg�layabilecek kadar ne bilgedir, ne �st�nd�r ne de -en �nemlisi!- masumdur. �Bu bir reklam ili�kisidir� diyenler pop�ler olmak i�in her bir taraflar�n� y�rtmaya haz�r olmad�klar�n� iddia edebilirler mi? Son derece k�t� bir kitap i�in �st �ste �� parti vermi� olanlar hangi hakla edebiyattan, edebiyat��l�ktan s�z edebilirler ki? �Overlok�ular�n g�zdesi� �sekreterlerin yavuklusu� diye dalga ge�enler Nobel Edebiyat �d�l� alm�� insanlar m�d�r? Yapayl�ktan dem vuranlar d�nyan�n en samimi insanlar� m�d�r? �Ah ama �� ay �nce b�yle diyordu� diyenler be� ay �nce �imdi dediklerinden farkl� hi�bir �ey demiyor, hi�bir �ey d���nm�yor mu? 

�Kar�s�n� bir ya��ndaki bebe�iyle b�rakt�, gitti� diye birini okura ispiyonlayanlar hayatlar�nda hi� mi zor durumdaki birini terk etmedi? �sterler miydi kendi yapt�klar� da gazetelerde insanlar taraf�ndan �t�� rezil� diye �ar�af �ar�af ele�tirilsin? �Pop�ler olmak istiyorsa bari gay olsun� diyenler Tuna Kiremit�i kendilerine a��k olsayd� her �ey tamam m� olacakt�? O zaman mesele kalmayacak m�yd�? 

Bir insan�n yapt��� i�i be�enmemek ve ele�tirmek ba�ka bir �eydir f�rsat� gelmi�ken �vurmak� ba�ka bir �eydir. Yapt��� i�i be�enmezsin, d�zg�n bir �slupla ele�tirirsin veya yok sayars�n. Bu yap�lanlar tamamen �bunlar nas�lsa a��k, nas�lsa aptalla�m��lard�r, nas�lsa birbirlerinden g�� al�rlar, ne desek umurlar�nda olmaz, vur abal�ya� denilerek yap�lan son derece merhametsiz, son derece ucuz i�lerdir. 

�clal Ayd�n-Tuna Kiremit�i iki g�n sonra ayr�labilir, herkes kendi evine de d�nebilir. Ama bu kendi ili�kini en y�ksek, en de�erli, ba�kas�n�nkini en al�ak en rezil g�rme hakk�n� vermez bize.. Kimse bir evde ve iki ki�i aras�ndan neler olup bitti�ini bilemez..

Hami�: Ta�� �illa at�lacaksa- g�nahs�z olan ats�n. 

 
