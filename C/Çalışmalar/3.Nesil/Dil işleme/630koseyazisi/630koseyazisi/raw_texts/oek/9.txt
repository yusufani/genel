Dikkat... May�nl� saha!  

B�ZDE malum, ya i�i zaman�nda yapmamak veya �nceden d���n�lm�� olmas� gerekenleri, i�i yapt�ktan sonra d���nmek ilkesi ge�erlidir.

Son zamanlarda �nemli bir mesele, sanki usul�ne uygun �ekilde yani �nceden t�m haz�rl�klar yap�lm��, zaman�nda herkesin g�r���ne sunulmu� ve iktidar partisinin vaatlerine uygun �ekilde herkesin ele�tirisine a��lm�� gibi y�r�t�l�yor. 

Kamu Y�netimi Temel Kanunu Tasar�s� ile ilgili geli�melerden s�z ediyoruz.

Tasar�y� Ba�bakanl���n laik devlet yap�s�na kar�� g�r��leri ile g�ndemde bulunan M�ste�ar� �mer Din�er ile �e�itli 6 bakanl�k temsilcisinin haz�rlad��� biliniyor. Yani h�k�met d�zeyinde bile eksik bir kat�l�m s�z konusu.

Tasar� Meclis'e sunuldu. Bilindi�i gibi Ba�bakan Tayyip Erdo�an da tasar�n�n bir an �nce yasala�mas�n� istiyor.

Esas diyece�imize gelmeden �nce belirtelim:

Biz bir s�re �nce tasar�yla ilgili ��ilk izlenimlerimizin�� olumlu oldu�unu yazm��, ileride bu konuya tekrar girece�imizi belirtmi�tik. Birinci nokta bu...

�kincisi... Tasar�yla ilgili ele�tiriler �zerine bir bas�n toplant�s� d�zenleyen Ba�bakan Yard�mc�s� ve Devlet Bakan� Mehmet Ali �ahin, ��Tasar�n�n T�rkiye'nin �niter devlet yap�s�n� bozacak h�k�mler i�erdi�i kanaatine var�rsak bunlara izin vermeyiz�� anlam�nda �ok �nemli bir vaatte bulundu.

Tasar�y� biz, ��iki k�y aras�ndaki yolun tamiri i�in bile Ankara'daki b�rokratlar�n onay�n� alma�� hantall���ndan ve anlams�zl���ndan kurtulaca��z diye sevin�le kar��lam�� ama ��okullar�n yerel y�netimlere devrini�� �ok sak�ncal� buldu�umuzu vurgulam��t�k.

Nihayet uzmanlar konu�maya ve g�r��lerini ortaya koymaya ba�lad�lar.

Bunlardan tasar�ya ��olumlu�� bir g�zle bakan TESEV (T�rkiye Ekonomik ve Sosyal Et�tler Vakf�) hem bas�na hem de siyasilere g�r��lerini sundu.

TESEV bile ��Bug�nk� Anayasa de�i�tirilmeden bu tasar�n�n yasala�mas� do�ru de�ildir�� diyor.

Haks�z da de�il... ��nk� bilindi�i gibi 1982'den beri y�r�rl�kte olan bu 12 Eyl�l Anayasas�, ��k��la y�netimi�� anlay���yla d�zenlendi�i i�in, Kamu Y�netimi Temel Kanun Tasar�s�'n�n ��yetkileri yerel y�netimlere (illere, il�elere) devretmeyi�� �ng�ren h�k�mleri, bu Anayasa'ya ters d���yor. 

O nedenle tasar� bug�nk� haliyle yasala�sa bile Cumhurba�kan�'n�n -veya muhalefet partisi CHP'nin- Anayasa Mahkemesi'ne ba�vurarak ��kar�lm�� yasay� iptal ettirmesi i�ten bile de�ildir.

TESEV'in konuyla ilgili �teki -ve ilgin�- g�r��lerine sonra tekrar de�iniriz. Ama as�l Malatya'da bulunan �n�n� �niversitesi 18-19 Aral�k 2003 tarihlerinde konuyla ilgili, 5 oturumlu ve ��Kamu Y�netimi Birinci Ulusal Kurultay��� ba�l�kl� bir toplant� d�zenlemi�. Bilim adamlar� gelip bildiriler sunmu�lar ve sonu�ta, ortak kanaatlerini ��Kurultay�n Sonu� Bildirgesi�� ba�l���yla yay�nlam��lar. Bildiride bu tasar�n�n Anayasa'daki ��sosyal devlet�� kavram�na taban tabana z�t oldu�unu -yoksullar�, �aresizleri sahipsiz b�rakan bir devlet yarataca��n�- ve daha da �nemlisi, tasar�n�n getirece�i d�zenin ���niter de�il federal, hatta federallik �tesi bir da��lma tehdidini�� i�inde bar�nd�rd���n� s�ylemi�ler.

Biz bundan daha b�y�k bir yanl�� yap�lamaz diye d���nd���m�z i�in �imdilik �teki konulara girmiyoruz. Ama sonra tekrar konuya girece�iz.
 
