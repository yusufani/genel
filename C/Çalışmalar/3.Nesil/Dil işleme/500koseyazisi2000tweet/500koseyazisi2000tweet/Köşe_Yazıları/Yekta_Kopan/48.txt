﻿Kavramsal sanat mekanı olarak Cumhurbaşkanlığı Sarayı

16 Türk devletini simgeleyen askeri üniformalı tören tam bir kavramsal sanat çalışmasıydı! "Şeffaf toplum" güvenceleriyle yaşayan biz ölümlülerin bir isteği var. Bu çalışmaların DVD formatında yayınlanmasını ve burada da "Kamera Arkası" görüntülerin mutlaka yer almasını istiyoruz.
Kavramsal sanat mekanı olarak Cumhurbaşkanlığı Sarayı
Benzersiz bir tören.

On altı Türk devletinin askeri kıyafetlerini giymiş adamlar sağlı sollu dizilmişler merdivene. 

Bu adamların işe nasıl alındığını merak etmemek elde değil. Hangi aşamalardan geçildi acaba? Başvuru formları, güvenlik soruşturması, deneme çekimleri, provalar. Eseri sahneye koyan yönetmenin rol dağılımında zorlandığı dakikalar. Oyuncular arasında çekişmeler; kim Harzemşah Devletini temsil edecek, kim Timur İmparatorluğu'nu? Kostümlerin hazırlanmasından makyaja uzayan zorlu bir süreç. Proje büyük olunca, prodüksiyon da büyük oluyor elbette.

Cumhurbaşkanı'nın merdivenlerden inişiyle doruk noktasına ulaşılıyor. 

Gerçekten benzersiz bir tören. Kavramsal sanat mekanı olarak Cumhurbaşkanlığı Sarayı. 

Filistin Devlet Başkanı Mahmud Abbas, böylesi ‘performans çalışmaları’nın ilk izleyicisi olarak konumlanmış durumda. “Sanatçılarla İftar” isimli çalışmadan sonra, şimdi de “Göğe çıkan merdivene dizilmiş 16 Türk Devleti” isimli yerleştirme çalışmasıyla karşılandı. Abbas, yakında görevini bırakıp çağdaş sanat eleştirmenliği yapmaya karar verebilir. 

“Şeffaf toplum” güvenceleriyle yaşayan biz ölümlülerin bir isteği var elbette. Bu çalışmaların DVD formatında yayınlanmasını ve burada da “Kamera Arkası” görüntülerin mutlaka yer almasını istiyoruz. Tarihsel verilerin tükendiği anlarda, kostüm uygulamalarında bir ‘uydurma’ yapıldı mı? Oyuncular, can verdikleri Türk Devletleri’nin tarihi konusunda ön okumalar yaptılar mı? Örneğin bir-iki dövüş sekansı gibi, eserden ‘çıkarılan sahneler’ var mı? Sesli anlatımlı ‘yönetmenin kurgusu’ görüntüleri devlet arşivine mi alındı? Cumhurbaşkanlığı Sarayı’nın kostüm odasında başka neler var? Bunları izlemek istiyoruz. İstiyoruz işte.

Erdoğan-Abbas buluşmasında elbette Paris’teki olaylar da gündeme geldi. Cumhurbaşkanı Erdoğan, batının ikiyüzlülüğünden dem vurdu. 

Oysa, Cumhurbaşkanlığı Sarayı’nda, sanatla dolu geçen bir günde, öldürülen karikatürcülere odaklanmasını beklerdik. Eminim dünyanın terörü lanetlerken, kimler için ağladığının altını çizmek sanatla hemhal olmuş bir ortama daha uygun düşerdi.

Kişisel notumu düşeyim buraya: Öldürülen karikatürcülerden özellikle Wolinski’nin hayranıydım. On beş-on altı yaşlarımda, Ankara günlerimde, karikatür ve çizgi konusundaki akıl hocam Levent Gönenç’le ustanın bir karikatürünü bulabilmek için ne maceralar yaşamıştık. Kimi zaman saatlerce bir karikatürünün detaylarına bakar, olmayan Fransızcamızla ne dediğini anlamaya çalışır, sözlük karıştırıp tuhaf çeviriler yapardık.


Olayların yaşandığı gün Levent Gönenç ile birer üzüntü mesajı gönderebildik birbirimize. Daha fazlasını konuşacak halimiz yoktu. Yaşananların akıl almazlığı dışında bir yerden bakıyorduk. Wolinski ölmüştü.

Geçen günlerde Levent Cantek ve Levent Gönenç’in “Birikim”deki yazıları gibi, olaya bu noktadan bakan, mutlaka okunması gereken yazılar çıktı. Politik değerlendirmelerin çoğunda aynı bakışın tekrarı olsa da, her birini satır satır okudum-okuduk. 

Sonunda dayanamayıp Levent’i aradım ve “Birer paragrafta öldürülen karikatürcüleri anlatsan ne yazardın?” diye sordum. Aşağıdaki notlar geldi.


Georges Wolinksi: Wolinski “7 Ocak 2015 Saldırısı”nda öldürülen karikatürcülerin en yaşlısı. 80 yaşında yaşama veda etti Wolinski. Paris’in en civcivli semtlerinden bir olan St. Germain Bulvarı’nın eski sakinlerinden ve tanınmış simalarından. Onu tanıyanlar her sabah Rue Bonaparte’ta bir kafeye uğrayıp esprossosunu yudumlarken kafede çalışanlar dahil herkese takılıp sohbet ettiğini anlatıyorlar. Wolinski, hınzır bir mizah anlayışına sahip. Özellikle cinsellik konusunda çizdikleri ülkesi Fransa’nın sınırlarını aşarak seviliyor. Wolinski’nin hınzır mizahı karısına miras bıraktığı şu cümlede yaşıyor: “Öldüğümde yakılmak istiyorum. Küllerimi tuvalete dökün. Böylece her gün kıçını seyredebilirim.”

Jean Cabut ya da bilinen mahlasıyla Cabu: 77 yaşında. Wolinski ile hemen hemen yaşıt. Cabu Fransız karikatürünün en tanınan ve sevilen isimlerinden biri. Neredeyse üç kuşak Cabu’nün karikatürleri ve çizgi romanları ile büyümüş. Cabu sinemayla, tiyatroyla, müzikle de ilgileniyor. Sıkı bir caz hayranı. 2005 yılında sevdiği caz klasiklerini “Le Jazz de Cabu” ismiyle 38 deseninin eşlik ettiği 4 CD’de derlemiş. Anarşist ruhu genetik. Oğlu Fransa’nın en tanınmış punk-rock müzisyenlerinden biri, Mano Solo. 2010 yılında AIDS’ten ölmüş.


Philippe Honoré: Charlie Hebdo’nun sessiz kahramanlarından biri. Dergi için genellikle eğlenceli bulmacalar ve vinyetler hazırlıyor. “7 Ocak 2015 Saldırısı”nda 73 yaşında arkadaşlarıyla aynı kaderi paylaşıyor.

Bernard Verlhac ya da dergide kullandığı mahlasıyla Tignous: 57 yaşında. Wolinski ve Cabu’den bir sonraki karikatürcüler kuşağının temsilcilerinden. Bir söyleşisinde; “Karikatür demokrasinin tanığıdır.” demiş. Kaderin cilvesine bakın ki, Tignous Cartooning for Peace (“Barış için Karikatür”) örgütünün aktif üyelerinden biri.

Öldürülen karikatürcüler arasında en genç olanı Charb ya da gerçek ismiyle Stephane Charbonnier: 47 yaşında, Charlie Hebdo’nun hem editörü hem karikatürcüsü. İflah olmaz radikal bir solcu. Sürekli tehditler alan, polis korumasında yaşayan Charb bir söyleşisinde şöyle demiş: “Bana yönelik tehditlerden korkmuyorum. Çocuğum yok, karım yok, arabam yok, hatta kredi borcum bile yok. Böyle söylediğimde bu size çok kibirli bir ifade gibi gelebilir ama dizlerimin üstüne çökmektense ölmeyi yeğlerim.”

Terör kurşunlarıyla sonlandırılan hayatları birkaç satıra sıkıştırmak yakışıksız. Bu söylediklerimiz sadece birer çıkış noktası olabilir. Eminim Cumhurbaşkanlığı Sarayı’nın değerli danışmanları, bir sonraki kavramsal sanat buluşması için, bu paragraflardan çok daha donanımlı bilgilerle çıkacaktır karşımıza. Ve Cumhurbaşkanı Erdoğan hem bize hem de tüm dünyaya, terörün sanatı susturmayacağını anlatan harika bir konuşma yapacaktır. 

Kendimizi kandırmayalım.

Olmayacak şeyler bunlar.

Sarayın ‘performans sanatları odası’ndan çoktan yeni çalışmalar başlamıştır bile. Belki bu kez Mahmud Abbas’ın ziyaretini bile beklemezler. 

Işıklar kararır. Merdivenler figürlerle donatılır. Hafiften bir müzik başlar. Cumhurbaşkanı merdivenlerin başında görünür. Kararlı adımlarla aşağı doğru iner.

Ve perde!