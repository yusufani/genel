By my calculations, we�re approaching the height of winter. For some of you, cabin fever may be setting in. Even during the mild winters we experience here in southern California, I often find it difficult to leave the house to exercise. Circuit training is a great way to get your workout in without leaving your warm, cozy home. 

All of the family circuits presented in this column include stations simply labeled �Cardio,� �Muscle,� and �Fun.� The �Fun� stations will often focus on coordination and balance, but sometimes they may just be good, old-fashioned fun. Older kids may enjoy coming up with a fun station in addition to those I present here. Just make sure what they choose is a safe and effective exercise.

Here�s what you�ll need for Family Circuit #4:

2 Hula Hoops
Step or large, sturdy book
Cardboard square, markers
Foam noodle
Medicine ball (image at right)
Cone

Set up your circuit stations wherever you have the most space in your home. Warm-up together as a family for five minutes. This may be as simple as walking around the block, if weather permits. If not, nominate the �ham� in the family to lead the family in a short warm-up to music. The rule is that everybody, even Dad, has to follow the leader during the warm-up. If you have more than one kid who loves the spotlight, you can break the warm-up into segments to give everyone a turn to lead. Next the family disperses to the stations. 

Station #1 Cardio
Hula Hoop Hop: Place the two hula hoops on the floor about six inches apart. Put one foot in the center of each hoop. Following the rhythm of the music, shift your weight from side to side. The leg that isn�t carrying weight lifts at the knee so that there is only one foot in a hoop at a time. Kids with shorter legs will need to push the hoops closer together. 

Station #2 Muscle
Calf Raises: Stand on the edge of a bottom step in your home. If there are no stairs in your house, a large, sturdy book can act as a substitute. Hang your heels off the step so that your weight rests on the balls of your feet. Lift and lower your heels by engaging your calf muscles. For safety, make sure there is a wall or railing nearby you can grasp if you lose your balance. 

Station #3 Cardio
Square �Dance�: Kids can use markers to label the sides of a cardboard square: top, bottom, right, left, and center. Place the square on the floor. Someone in the family designates a pattern to follow for today�s circuit. For example: two hops on the right, two on the left, one hop on top, and one in the center. Older kids and adults can manage a longer, more complicated sequence. Repeat the pattern, challenging your mind and body.

Station #4 Fun
Noodle Balance: Hold a foam noodle vertically in the palm of your hand without using your fingers. Try to balance it for the duration of the station.

Station #5 Muscle
Medicine Ball Bounce: You can purchase a medicine ball at a sporting goods or exercise equipment store. Kids love using them. A weight of 4�5 lbs. is appropriate for school-age kids, as long as they�re supervised. For this exercise, stand with your feet apart, bent at the knees. While keeping your back up straight, toss the ball to about head level, than catch it. The power to propel the ball into the air should come from your quads, not your lower back. 

Station #6 Cardio
Free Dance: Use the cone to mark this station and set it up near the speakers, where the music�s beat is the strongest. It�s time to release your inner Fred and Ginger. Or perhaps Beyonce and Justin are more your style. Increase your cardiovascular intensity by including lots of arm movements into your dancing style. Inevitably, someone in the family will want to avoid this station. Instead of skipping it, do something else cardio like jogging in place or jumping jacks. 

A good length of time at each station is about 30 seconds. Family members can take turns being in charge of watching the clock and yelling �Next station!� Repeat the circuit until you get in a good twenty minutes of activity. Fun music will help you and your kids to stay motivated. Finish with a five-minute cool down and stretch. 

As a mom, I realize circumstances don�t always allow you to accomplish everything you have planned. In reality, you may not have the time, space, or energy to create the perfect family circuit. Remember that any activity is better than no activity. So leave out a couple stations or improvise if needed. The goal is to establish healthy habits for your family by allocating time to move and sweat together. 

