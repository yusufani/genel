Britain's consumers may have cut back on spending on themselves over Christmas, but they still splashed out on the family pet.

Figures today from Pets at Home, Britain's biggest retailer of pet products, showed sales in the six weeks to January 10 jumped by 8.3%, boosted by purchases of chocolate reindeers and treat-filled Christmas stockings.

Chief executive Matt Davies said sales had been strong all year and the trend continued through the festive period. "Continuing like for like sales growth and improving margins, coupled with our extensive store opening programme gives us confidence for the future," he added.

Over the six week Christmas period the private-equity-backed chain sold 286,000 of its Christmas stockings, 180,000 dog toys, 17,000 specially formulated chocolate reindeers and 13,000 dog outfits.

Private equity firm Bridgepoint bought a controlling stake in the company in 2004, in a deal which valued it at �230m. The company is considering a stock market float, which analysts reckon could value the 211-store chain at more than �600m.
