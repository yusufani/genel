Gosh, a constitutional ding-dong over Princess Anne's prospective promotion from 10th to fourth in line to the throne and the right of Peter (11th) Phillips's wife to become a Catholic again if she so chooses. It's just what we need in the depth of the most serious economic crisis for almost a century.

Yes, that's what we seem to have woken up to this Friday morning. From distant Brazil (are they all Catholics or animists?) Gordon Brown has confirmed his support  "in principle"  for a backbench bill being promoted at Westminster today by the gnomic Lib Dem MP, Dr Evan Harris.

The Daily Mail and Daily Telegraph are all steamed up too. Though the Archbishop of Canterbury says he's relaxed about it, the Mail insists  in the next sentence  that the change "would be a further blow to Christianity". Hey, what's an archbishop's opinion when the Mail is in need of a scare story?

Actually we should all be pretty relaxed about the change too; Anne would be a less self-pitying monarch too than you-know-who (if we decide to keep them on). That is, provided we understand what it was all about in the first place: not mere sectarian prejudice, but the survival of the British state in the turbulent 16th to 18th centuries.

So jokes about the Princess Sophia Precedence Act are all very well provided the Today programme crowd know who she was and what she represented. It sounded this morning as if they didn't and when I tried to explain Jacobitism to the campaigning Harris last night the medic's eyes quickly glazed over and he asked if it was a disease.

What the Harris bill would propose is the proposition, which has  we learn  been discussed (again) with officials at the palace, to abandon the Norman practice of primogeniture. Incidentally, it discriminates against younger sons as much as it does against female heirs. But it would be removed in favour of what is known as "lineal primogeniture" whereby daughters get equal entitlement. The Swedes were first to do it in 1980 when Prince Carl was bumped in favour of his big sister, Victoria. Norway, Belgium and the Netherlands have followed suit.

That's the easy bit, and even the Telegraph's poll today shows 78% of voters in favour of the change. We should note in passing, incidentally, that the widely used European practice of excluding women altogether  the Salic Law as applied in France  never prevailed here. Hence a mixed bag of queens, including Victoria and England's top monarch (says me), Queen Elizabeth I.

The Catholic bit is trickier and would require amendment or repeal of nine acts keeping the Stuarts, their successors and co-religionists well clear of Buck House.

As the papers report today it's been in place since the Glorious Revolution of 1688 when James II's purge of key Protestants in the church, universities, army etc (plus the birth of a Catholic heir to his second wife) persuaded the national elite to throw the silly man out.

James's Protestant daughters, Mary and Anne, having failed to produce a surviving heir of either sex, the Act of Settlement of 1701 fixed the Protestant succession on their nearest kin, Princess Sophia of Hanover. It was her son, George, who actually got the job in 1714. So far as we can tell Elizabeth II is his descendant, though there are interesting doubts about Queen Victoria's paternity.

It all seemed sensible at the time. Religion had ripped apart most early modern European states. All the great powers were Catholic  the ruler's religion was the state's  and far more oppressive than their Protestant opponents. Spain and France repeatedly sought to impose their will. Though sensible Catholic writers such as Dennis Sewell, author of Catholics, sometimes speak of the "Elizabethan and Jacobean terror", the numbers don't remotely compare with, say, the Inquisition on a quiet day.

The best comparison is with fear of communism in our own times. Who were they loyal to? A foreign power? Were many of them traitors, willing to resort to violence to advance their cause? Since the fall of the Berlin Wall, militant Islamism has filled what may be a psychological need. But it's not all fiction. There are such people with such goals. In 18th century England the fear of French and Spanish armadas remained real; it wasn't mere sectarianism.

Excluding Catholics from the British throne is now an anachronism, though anti-Catholic prejudice lingered well into the late 20th century. As I never tire of reminding the young, the leftie New Statesman board put Paul Johnson on probation as its editor in the 60s because Leonard Woolf  Virginia's widower  thought he'd be on the phone to the pope.

"Anti-Catholicism is the antisemitism of the left," murmur some. Yet the Labour party has a huge Catholic membership, locally in big cities and at Westminster  all those smart Scottish and Irish immigrants so visible on the green benches.

The collapse of prejudice (or is it the rise of indifference?) is such that we recently had 2.5 Catholics as main party leaders: Iain Duncan Smith, Charles Kennedy and Tony Blair. No one cared and the roof did not fall in. Even the Telegraph was being edited by Catholics by then; times change.

So changing the law and squaring the Commonwealth is a respectable project, albeit for quieter times because there's bound to be trouble, not least a theoretical problem for the established Church of England. The Mail is already talking it up as a leftie atheist and republican plot  which it isn't.

But we should also be wary of being over-casual; long-abandoned feuds are like "extinct" volcanoes  not extinct after all. The change is right for our times, but Protestant-Catholic issues linger.

You see it every time abortion or living wills get debated in parliament: the Catholic lobby becomes very conspicuous, as it is entitled to be. Why, only the other day I was chatting with a Labour MP for whom that fight over Princess Sophia remains very vivid and important.

The MP feels James II was a tolerant, misunderstood man (we plan to exchange notes) and could fairly be called a Jacobite. Unlike Harris, he knows that means a supporter of the Stuart claims  of the 1715 and 1745 rebellions. He made a joke about the "little gentleman in velvet"  the mole whose molehill tripped William III's horse and killed him.

It also goes without saying, though it's thought bad form to say so, that religion is central to murder on a large scale  three this year  as part of the residual 17th century dispute over the six counties of Northern Ireland. A former Sinn Fιin councillor was charged with murder only yesterday. Religion is central to the identity of both sides.

Put another way, Catholic plotters tried to inflict mass murder on Margaret Thatcher and John Major's cabinets just as they did, equally unsuccessfully, to James I in 1605. The ancient ritual of the daily bomb search of parliament  part of the Protestant triumphalism we so deplore  became very real again in the 1970s.

I see the coppers and the dogs every working morning. They're not looking for Christian bombs these days, but those of Islamists. No primogeniture or feminist problems in a polygamous society, by the way. If you want to know how the once-mighty Ottoman dynasty sorted out the succession you'd better look it up.

On second thoughts, don't. We don't want to encourage religious prejudice.
