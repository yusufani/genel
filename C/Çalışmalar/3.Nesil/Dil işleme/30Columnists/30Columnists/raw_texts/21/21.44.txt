Has any takeover or merger ever produced long-term value for investors and other stakeholders? 


There must be cases of it, though it is hard to think of them, and many recent examples seem only to have succeeded in wiping out the bidding company almost entirely. Lloyds TSB's disastrous acquisition of HBOS is another case in point. 

Even the chief executive, Eric Daniels, now admits he probably could have avoided taking any Government money but for the HBOS takeover. Most of the rubbish he's now being forced to put into the Government's asset protection scheme comes from HBOS. 

The upshot is that the Treasury ends up with a 75 per cent economic interest in the combined entity, diluting original Lloyds TSB shareholders down to a tiny fraction of their original interest. In hindsight, this is almost as bad a deal as Sir Fred Goodwin's acquisition of the "toxic" bits of ABN Amro, which similarly holed Royal Bank of Scotland below the waterline, forcing it to fall back on the tender mercies of the politicians. 

Just to complete the hat-trick, HSBC finally got round to admitting this week what everyone else has known since the start � that the acquisition back in 2002 of the US sub-prime lender Household International was a mistake. HSBC is so big it still seems unlikely Household will end up destroying the mother ship, but it's already done untold damage and there is undoubtedly more to come. 

Ironically, one of Sir Fred's former acquisitions, that of National Westminster Bank in 2000, is generally regarded as a model takeover which did succeed in creating lots of value. Yet the point is that NatWest didn't have to be taken over by a tiny little Scottish upstart to succeed: it only needed to be managed a bit more effectively. What's more, the success Sir Fred achieved with NatWest plainly went to his head and greatly contributed to the hubris which eventually did for RBS. 

That's one of the other problems with takeovers. If by any chance they do succeed, the emperor who created the resulting behemoth nearly always ends up overstaying his welcome, with the result that eventually the rot sets in and mistakes are made. Lord Browne, former chief executive of BP, was a classic example of it. 

A series of mega-mergers succeeded in re-establishing BP's footprint as one of the world's leading oil majors, yet standards of safety and operational efficiency were allowed seriously to slip. Lord Browne was eventually forced out by personal scandal, but the ship was listing badly by the time he left. His successor found much of the necessary integration had yet to be done. 

Both Vodafone's takeover of Mannesmann at the turn of the century and Glaxo Welcome's merger with SmithKline Beecham around the same time succeeded in creating a couple of very big companies, but even now, all these years later, they have failed to add a penny to shareholder value. To the contrary, it can reasonably be argued that they destroyed it. 

All academic research shows most takeovers don't work, in the sense that they fail to create any value for the bidding company. In many cases, the resulting clash of corporate cultures can kill the organisation stone dead. So why do companies keep doing them? 

Pursuit of monopoly is one reason, and provides the best raitionale for the Lloyds takeover of HBOS. But it nearly always turns out to be illusory, and monopolists never succeed in keeping an acquired dominant market position. The most charitable explanation is that M&A is another form of industrial restructuring, which allows the corporate landscape to evolve and change. Mergers may not create value, but it is certainly possible that many of these companies would be in even worse shape on their own. Yet it's debatable. Takeovers are what companies do when they mature, or when managements have run out of ideas for creating new growth opportunities. They are more the result of executive boredom than imaginative thinking. 

Financial engineering, or asset stripping, is another major motivation, particularly among the takeover kings of private equity. This purpose too has turned to dust in the downturn of the past couple of years. It only works when there are plentiful quantities of credit around to produce the leverage, and, in any case, its consequences for investment and jobs are frequently far from benign. 

But the main explanation is our old friends the investment bankers, who seem to be the root of all ills these days. They provide both the wherewithal to make takeovers happen and the almost invariably flawed logic behind them. Managements are made to feel inadequate unless they are also fee-paying deal-makers. 

The only consistently successful deal-makers in my experience are individuals acting in their own self-interest and therefore able to look beyond the siren calls of the investment bankers. Empire-building bosses of public companies do not on the whole make good takeover kings. Few takeovers work out as they are supposed to, and not infrequently they end up destroying the whole company. 

