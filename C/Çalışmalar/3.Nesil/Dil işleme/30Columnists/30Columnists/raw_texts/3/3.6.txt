GORDON BROWN says he's angry with the Royal Bank of Scotland. He's not the only one. Spare a thought for the small shareholders who thought they had provided for their old age, and for the RBS employees who thought themselves lucky to be working for the world's fifth largest bank. Security was the name of the game for both groups. That, and the pride in being attached to the shining example of Scottish know-how and probity in the business of looking after the bawbees.

On a visit to the new Gogarburn HQ, I too felt a vicarious pride in a confident assertion of what could be achieved by the financial services sector of the Scottish economy. But that was months before early warnings of the financial tsunami had starADVERTISEMENTted to lap around our ankles. Like just about everyone else who willingly believed in the world-class expertise of our moneymen (and women), I've grappled with the mystery of why they stopped practising prudence and lent as much as borrowers could take away, without bothering about repayments.

It took a while for the billions owed to, not just RBS, but banks across the globe to reach the totals they have, so was nobody on their boards or management teams alerted to the possibility of a crash if loans were not repaid? And where was the Financial Services Authority, or the RBS auditors?

But we are where we are. So how do we get from here to where we can once again feel confidence in our banks? Will the insurance cover promised by Alistair Darling and Gordon Brown to cover irrecoverable debts be enough? We don't know. Without full nationalisation, will the banks lend to small businesses and people to get the economy going again? We don't know.

In the midst of such uncertainty, we must do what we can to wriggle through the holes and spaces, planned and unplanned, in the financial and economic tapestry woven by the Chancellor and the Prime Minister. 

I've been boring people for months now with my fears that, in Scotland, the Edinburgh region would be worst affected by the fall-out from the financial system's crisis because of the number of families employed in the banks themselves, as well as in professional offices servicing the financial services companies, and the full range of businesses like hairdressers, florists, gyms and cinemas.

I've been worried on hearing politicians in all parties give over-optimistic predictions for the sector's recovery. People more qualified than I am to analyse the financial situation in Scotland have been remarkably cool in the face of possible catastrophe for Edinburgh. But the balance of probability swung my way when Oxford Economics published its investigation into which local authority areas of the UK are most vulnerable to the credit crunch.

Edinburgh is the only Scottish local council to appear on the first chart (coming in at 14th out of 480) and our capital city tops the list of the cities most likely to experience high unemployment, homelessness, etc. My heart sank when I saw the numbers. But, it stiffened my resolve to argue for Edinburgh to be seen as a special case.

Right across Scotland people and families will be affected by the near-collapse of what had become the commanding heights of our economy, but Edinburgh plays such a vital part that it's in the interest of everyone living north of the Solway and Tweed to mitigate the pounding Edinburgh will take. 

The Capital must continue to act as an efficient and attractive gateway to Scotland, provide the facilities for business to be done, for conferences to be held and respected higher and further education qualifications obtained.

We must minimise unemployment as a first priority. So the council needs money to build houses, schools, improve sports facilities, and re-introduce the policy of helping people to renovate their tenement closes. Apart from tackling a needs that saw 40,000 bids placed for 2700 council-owned homes last year, and sprucing up traditional housing stock, youngsters could get apprenticeships and skills would not be lost. 

Finance Minister John Swinney has enough problems, like no borrowing powers, so he's looking for solutions that don't cost money he hasn't got. I've suggested that Edinburgh could pay for much of the city's own recovery if the �100 million of NDR, business rates, generated in Edinburgh, but presently spread thinly across Scotland, were to be retained here, for everyone's benefit. 

Glad to have Obama
I've loved being here when the USA realised part of Martin Luther King's dream has come true, and in the White House a family with the blood of slaves in its veins proves that blacks no longer sit at the back. 

But I'll try not to download my hopes for the world on to him. 

