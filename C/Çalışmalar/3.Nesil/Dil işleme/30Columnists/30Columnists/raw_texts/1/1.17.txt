THE Scottish Parliament is in recess, the UK Parliament is in recess, the politicians, media and circus troupe of advisers and researchers that hang on their every word are looking out their buckets and spades for Blackpool, Benidorm or the Bermuda. It can only mean one thing � the silly season is about to begin.

Surveying this week's media my suspicions were alerted by some environmentalist wanting to canonise Alex Salmond and call him a Saint.

Our laughing Cavalier of a First Minister had just spoken at the World Renewable Energy Congress, in Glasgow, and had confirmed that the Scottish Government was granting permission for the largest wind farm in Europe � at Abington in South Lanarkshire. Well, if you are going to put a wind farm any place that's about as suitably dull and uninviting a place given that most of Scotland is choc full of stunning views and vistas.

The last laugh may yet be on us, of course; Saint Alex may become a sinner when a nuclear-free independent Scotland has to import nuclear-generated electricity from England when the wind isn't blowing hard enough. Oh, and the turbines don't work when it's blowing too hard either!

Of course, when they do turn they're still relatively uneconomic and are only being built thanks to hidden subsidies and taxes dished out by � yep, Westminster. 

So we would have to pick up that bill as well.

In fact the more turbines you build the more of a baseload of electricity generation you need from conventional power stations for when they can't turn � so the power is not free as their adherents try to suggest.

Some people say the wind turbines are beautiful things but you won't find any being proposed for Linlithgow, where Alex Salmond stays, or those beautiful shores that his buddy Donald Trump wants to develop.

I do take my hat off to Salmond, yet again though. His upping the ante at the Glasgow East by-election this week by talking of earthquakes was strong leadership that Labour can only watch jealously with open mouths.

Salmond may be a gambling man, but he's a calculating gambler, and pretty good at it. He correctly reasoned that even if the SNP lost the by-election, the result would be close enough to make Labour look the loser.

In the event the SNP won and Salmond's gamble paid off. Maybe he deserves his canonisation after all?

Just one mod con

ON my travels again, this time to Lagos, Nigeria, where there are some really swanky and expensive hotels � all out of my league I'm afraid, so I had to do some research and found one described in Time Out as "simple" that gave "comfort and convenience".

I checked the hotel's website and found it offered collection at the airport in an air conditioned bus, an Olympic sized swimming pool, 24-hour room service and five price ranges of room � so I booked in at the mid-range "luxury" grade at a third of the price of the local Sheraton.

The hotel forgot to send the minibus and it was an hour late while I was propositioned regularly with a variety of life-threatening offers. It did have air conditioning all right, it's called windows and the driver kindly opened them for me.

The swimming pool was Olympic in scale only in regard to the number of organisms inhabiting its opaque soup that I declined to put even a toe in. When I asked for breakfast to be brought to my room 24 hours translated into "only if you're ordering dinner, sir", but I didn't want dinner for breakfast, just eggs and bacon!

The waiter feigned ignorance. As for the "luxury" epithet, well my room had some exotic decor on the wall � from the flowering plaster � and the Spartan cream walls might have passed for minimalist in Copenhagen.

Ho hum. Like America, Nigeria and Britain are divided by a common language. The main thing my hotel had was Wi-Fi � or you would be reading John Gibson instead of me!

Getting pasta my best

THINGS are changing down Easter Road yet again. No, not some calamity with the glorious Hibees, but the end of another institution that made Easter Road superior to Gorgie Road � Ristorante Tinelli.

Standing at the check-in queue at Edinburgh Airport I bumped into my friend Giancarlo Tinelli � who told me he was on his way to Italy for a month's rest after selling up to a new generation of Italian Scots, whose venture will be called Al Dente. I wish them Bon Appetito.

I gave Giancarlo my good wishes and recalled I first went there in 1984 with Robin Dunseath and it had been a favourite ever since. The news follows on from Lang's the butchers closing last autumn � and the owners of Prego and Martin's, elsewhere in the city, putting their feet up in recent years. I thought you felt old when you noticed policemen getting younger than you, but now my favourite restaurateurs are beginning to retire. I feel I'm due a free bus pass. Goodness! I wonder what age John Gibson feels?
