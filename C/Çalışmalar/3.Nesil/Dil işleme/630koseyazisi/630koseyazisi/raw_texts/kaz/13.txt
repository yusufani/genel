�eref bizim Lance 

Bana sorsan�z, "Bisiklet�i Lance Armstrong'un kanseri yenmesi mi olay. Yoksa Fransa turunu tam 6 kez �st �ste �ampiyon bitirmesi mi olay?" diye, "Kanserli birinin �ampiyon olmas� daha b�y�k olay" derim.
Kanser sava��nda umutlar�n� yitiren insanlara umut a��layan bisiklet�i Lance Armstrong i�in H�ncal Usta, The New York Times'�n ba�l��� �zerine harika yazm��. Ellerine sa�l�k. Sevgili Usta, bana da tatl� bir mesaj atarak diyor ki: "O art�k tek isimliler kul�b�nden."
Her insan�n soyad�n� aileye sayg� ad�na yazan, sayg�n insanlara Say�n diyen biri olarak Lance'nin ba�ar�s�n� iki nedenle iyi bilirim: Birisi o ac�y� ben de ya�ad�m, ya��yorum.. �stelik bir de�il tam 3 kez (Kolon, karaci�er ve akci�er) kanseri yendim. �kincisi ve daha �nemlisi bir zamanlar�n, iyi say�labilecek kadar bisiklet yazar�yd�m. (Sevgili Esat Y�lmaer, Faik G�rses, Bilal Me�e ve Salih Sezer'le birlikte. Ne g�nlerdi ama...)
Lance'e ne kadar sayg� duyuyorsam, �u an Sar�yer kamp�nda ��lg�nlar gibi ama hasta hasta �al��an �ampiyon g�re��i �eref Ero�lu'na da ayn� sayg�y� duyuyorum. �stelik her iki ayda bir kan� de�i�tirilip, yorgunluktan �lse bile �ikayet etmiyor. ��nk�: �eref Ero�lu bir yandan yakaland��� Akdeniz anemisi (Kanserin yeni bir y�z�...) hastal���n� yenmek i�in �al���rken, bir yandan da Atina Olimpiyatlar�'nda alt�n madalya i�in �al���yor. Hem de nas�l �al���yor, �l�m�ne. Bir yandan ise d���n�nde kar�s�n�n o g�zelim ellerine tak�lan son bilezikleri de satarak hastal���n� yenmek i�in �al���yor.
��te bizim �eref'in de gururu bu!
O �u an yapayaln�z ama gururlu. O da biliyor ki �nce, olimpiyat �ampiyonlu�u.
H�ncal Usta'ya diyorum ki:
Senin Lance'inin arkas�nda milyonlarca dolar veren sponsorlar var. Benim �eref'in arkas�nda ise kar�s�n�n kolunda ki son bilezik. Yani diyorum ki, �ok de�il bir ay sonra Atina'da �eref Ero�lu hasta hasta olimpiyat �ampiyonu olursa o da Lance gibi, tek isimliler kul�b�n�n �yesi olacak m�?

NOT: �lker grubu Ero�lu'nun ABD'de tedavisi i�in 15 bin dolar verdi. Genel m�d�rl�k vize alamad�. Para iade edildi. Ero�lu �u an kanser hastas� ama T�rkiye'nin de Olimpiyat'taki en b�y�k alt�n umudu. �nsanlar� �ampiyon yapan bu umut de�il mi H�ncal Usta!.. 
