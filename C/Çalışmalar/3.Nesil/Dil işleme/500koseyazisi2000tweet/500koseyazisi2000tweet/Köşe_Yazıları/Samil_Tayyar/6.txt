﻿Affetmem vururum


Dün sadece Türkiye değil neredeyse tüm dünyanın gözü, Başbakan Tayyip Erdoğan’ın grup konuşmasındaydı. Suriye’nin Türk jetini düşürmesinden sonra ilk kez kürsüye çıkan Başbakan Türkiye’nin yeni yol haritasını açıklarken, eş zamanlı olarak toplanan NATO krize ilişkin yaklaşımını netleştirdi.

NATO “şiddetli kınama” kararıyla yetinerek Suriye’yi ıskaladı. Diplomatik dille nasıl ifade edilir, bilmem, ancak amiyane tabirle Türkiye’yi açıkça sattı.

Oysa uluslar arası hava sahasında NATO üyesi ülkeye karşı gerçekleşen bu saldırı, tüm NATO’ya karşı bir saldırıydı.

Kınama kararına ekledikleri “bir daha olmasın” türünden açıklamaları, “Kahrol düşman al sana bomba” geyiğinden bile daha komikti.

Suriye jetimizi düşürdüğünde kimileri “Bunlar bu kadar beyinsiz mi, NATO üyesi bir ülkenin uçağını neden düşürsün?” diyerek Türkiye’yi karşı komployla suçlarken, bu ihtimali öngördüklerini düşünemediler veya art niyetli olmayı tercih ettiler.

Bu kuyuya sazan gibi atlayan ilk kişi tahmin edildiği gibi CHP Lideri Kemal Kılıçdaroğlu oldu. Başbakanlık’taki bilgilendirme toplantısından sonra yaptığı açıklamada saldırıyı adeta Suriye’nin meşru hakkı olarak gören Kılıçdaroğlu, Rus yapımı Suriye füzesinden iktidar çıkarma umuduna kapıldı.

Okuyanlar hatırlayacaktır; son yazımda Suriye ve arkasındaki iradenin üç temel referans noktasına dikkat çekmiştim: 1-Türkiye’nin karizmasını çizmek istediler, 2-Savaş ve BM/NATO arasında tercihe sürükleyip ortada kalacağımızı varsaydılar, 3-İç siyasete benzin döktüler.

Dünkü gelişmelere baktığımızda şu ana kadar hesaplarının tuttuğu anlaşılıyor.

Suriye’ye uyarı

MHP’yi bir kenarda tutuğumuzda hem içeriden hem dışarıdan ciddi bir kuşatma altında olduğumuz aşikar. Bu çember yarılmazsa Türkiye hızla yalnızlaşır, içine kapanır ve “Büyük Devlet” iddiasını kaybeder.

Böyle bir ihtimali düşünmek bile istemem.

Türkiye artık makas değiştirip yıllar öncesindeki ürkek ve içe kapanık statüye yeniden dönemez, dönmemelidir.

Başbakan’ın dünkü meclis grup toplantısında açıkça ifade ettiği gibi; güçlü bir Türkiye kimliği bölgesel güç ve küresel oyuncu karakteriyle, uluslar arası rekabet gücü yüksek ekonomi, dış politika, demokratikleşme ve sosyal kalkınma hamleleriyle somutlaşır.

O nedenle çevredeki ve dünyadaki gelişmelere gözlerimizi kapatamayız.

Ne var ki, Türkiye’nin bu hedefleri karşısındaki tek engel, soğuk savaş döneminden kalma ittifaklar değil aynı zamanda batılı sözde dostlarıdır. Küresel denklemi az oyuncuyla kurup dış faktörlere kapatanlar, yeni oyun kurucu istemiyorlar.

Hem karşıdan hem yandan...

Ama Türkiye inandığı yoldan asla taviz vermeden yürümeli, denklemin bir parçası haline gelmeyi başarmalıdır.

Gelişmeler, Suriye’yi bu sürecin en kritik test alanı haline getirdi. Uluslararası kuruluşlar Türkiye üzerinden ortak oyuna tutuşur ve yalnızlaştırarak etkisini azaltmayı hedeflerse, kendi göbeğini kesmek zorundadır.

Başbakan’ın dünkü çıkışı bu manada çok önemlidir: “Türkiye yeri, zamanı ve yöntemini kendisi tayin ederek uğradığı haksızlık karşısında uluslararası hukuktan doğan hakkını kullanacaktır.”

Yani, “meşru müdafaa hakkı” saklıdır. Başbakan’ın dün ısrarla üzerinde durduğu gibi, zaman, tarih yazma değil yapma zamanıdır: “Kimin yazdığına, kimin okuduğuna karışmayız.”

 

Başbakan bu ifadeyi biraz daha açtı. Dedi ki: “Türkiye’nin angajmanları değişti, Suriye’den gelecek her türlü askeri unsur artık tehdit unsurudur.”

 

Tercümesi şudur: En ufacık hatanı asla affetmem.

Savaş yetkisi

Doğrusu da budur. Uluslararası hava sahasında Türk jetini düşürerek Türkiye’nin caydırıcılık gücünü test etmeye kalkanlara haddi bildirilmezse sözün değeri kalmaz.

“Had bildirme” illa savaş ilanı demek değildir. Sınır ihlali yapan Suriye uçaklarının düşürülmesi, Suriye’deki PKK kamplarının vurulması, muhaliflere her türlü desteğin verilmesi gibi birçok seçenek sıralanabilir.

Kullanmaya niyetli olmasanız bile “savaş kartı” her zaman masada durmalıdır. Bu minvalde hükümet meclis tatile girmeden önce tezkereyle savaş yetkisi alsa iyi olur.

Şu anda yürüttüğümüz kontrollü kriz yönetimi, arkasında “savaş kartı” yoksa zamanla cılızlaşır ve etkisini kaybeder.

Başbakan’ın grup konuşmasındaki “kararlı” tavrının aksine bir hükümet üyesinin “savaşmaya niyetimiz yok” türünden açıklaması, samimiyet içerse bile stratejik açıdan hatadır.

Dün AK Partili vekiller arasında en fazla alkış alan Başbakan’ın şu sözü aslında işin özetidir: “Türkiye’nin dostluğu ne kadar değerliyse gazabı da o kadar şiddetli ve kahredicidir.”

 

Aksi halde; Başbakan’ın ifadesiyle “kadastro mühendisleri” bölgede yeni haritalar oluşturur, 376 milyar dolara çıkan dış ticaret hacmini 2002’deki 88 milyar dolar seviyesinin altına düşürür, sosyal kalkınma ve demokratikleşme hamleleri akamete uğrar, AB hedefinden uzaklaşır şamar oğlanına dönersiniz.

Başbakanın dünkü grup konuşmasını dikkatlice izlediğimde, sürecin çok iyi tahlil edildiğini ve kararlı bir pozisyon alındığını görüyorum. Askeri angajmanların değiştirilmesini bu bağlamda çok stratejik buluyorum.

Allah Türkiye’nin, inananların, sadece halka ve hakka teslim olanların yolunu açık eylesin.