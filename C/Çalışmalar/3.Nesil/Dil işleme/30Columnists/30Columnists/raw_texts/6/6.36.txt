All politicians pay lip service to the goal of improving social mobility, even though it is hard to define, measure and influence, and can be undermined by adverse economic forces. 

Liam Byrne, the latest in the line of bright, youngish Cabinet Office ministers, claimed yesterday that, after three decades of no movement, �there is a sense that since 2000 we have been making a difference�. 

His claims were based on a new 95-page discussion paper, Getting On, Getting Ahead, produced by the Cabinet Office�s strategy unit (see its website cabinetoffice.gov.uk/strategy). The analysis is, as usual, impressive but, even though the paper is not a statement of policy, the unit is hardly independent and the paper�s theme is that the current approach is producing improvements. 

Social mobility is not the same as equality of outcome, and is better likened to fairness: ensuring that there are more high-quality jobs for each successive generation and that everyone has the opportunity to obtain jobs in line with their potential. There are few certainties here, with a vigorous debate about whether a parent�s background became a bigger influence on a child�s success for those leaving schools in the 1980s compared with the 1970s. However, upward mobility of women, while lower than for men, has improved. 


Family background may have had less impact on GCSE results for those born in 1990 than in 1970, while the chances of moving up the earnings ladder during a career have picked up since 2000. 

Recent trends can be assessed only tentatively because children born in 1997 have only just started secondary schools, while many government initiatives have only recently begun to build up (with 255 children�s centres open in 2005, but 2,914 by this year). 

Mobility depends on two broad clusters of factors: first, by offsetting disadvantages of family background through early years childcare, the quality of teaching in schools, post16 education and training, and improving workplace skills; and secondly benefiting from global opportunities via the creation of more high-skilled jobs. For instance, changing lifestyles and rising expectations are likely to increase domestic demand for high-quality professional and personalised goods and services. 

Both these points are in a sense obvious, but that does not make it easier for policymakers. How much can the State intervene to offset the adverse impact for early brain, and even foetal, development of poor maternal diet and lifestyle? The post-1997 initiatives can be defended in their own terms, but how much have they achieved overall? 

There is a debate between the Tories and Labour about how much can be done by voluntary action (the type of schemes proposed by Iain Duncan Smith�s Centre for Social Justice) and how much by the State. 

A White Paper is promised soon, but the prospects for further action have been changed by the downturn. This will reduce the chances for children from poorer families with few skills or qualifications and will increase poverty and the deterioration in public finances will not only limit the scope for further initiatives but will squeeze existing ones. Social mobility is likely to be another casualty of the recession. 

