WATCHING Wendy Alexander trying to explain to a bemused TV interviewer that she, and not Gordon Brown, was responsible for the Labour Party in Scotland brought back old memories.

Long ago, in the bad old days before Scotland was granted a devolved parliament by a House of Commons stuffed with New Labour rookie MPs, a group of bold young Labour Party members broke with the Labour Party in Scotland and set up the Scottish LabouADVERTISEMENTr Party.

That these young McTurks were able to so name their new political party was due to the party that used to be known as socialist being a British party with a Scottish section. At the time, this served to underline the embarrassing fact of political life that the National Executive of the Labour Party in London called the shots and the annual conference in Blackpool, Bournemouth or Brighton made policy for the party from Land's End to John o' Groats.

The embarrassment lay in the futility of the Scottish section of the party, at its annual conferences in Dunoon, Rothesay or Inverness, adopting motions that were ignored or overruled by the NEC in London. This cut both ways, in that the Scottish Conference always voted to Ban the Bomb and its decision was ignored by either the Labour Conference or the NEC. But, and this little anoraky nugget of information is barely remembered, it was on the orders of the London leadership of the Labour Party, just before the General Election of 1974, that a conference was hastily arranged and held in a hall in Dalintober Street in Glasgow so that a decision taken by the executive of the Scottish section could be overturned.

This duly happened and the Scottish bit of the Labour Party was bounced into supporting the Scottish Assembly proposed by the Labour Party. Then, as now, the SNP was breathing down Labour's neck and it was reckoned by politics watchers that if the Scottish executive, with its anti-devolutionist majority, hadn't been over-ruled from London, the Nationalists would have won even more seats at the election. But 17 years of voting Labour overwhelmingly in Scotland only to be out-voted by Thatcherite converts in England Scottified Labour activists in Scotland.

Also, the economic and social divergence north and south of the Border legitimised the support for policies that reflected and dealt with the differences.

The clarity of the demarcation line between the Scottish section and the Labour Party became blurred as the ideas grew on devolved responsibility for the delivery of public services. 

Thanks to the percentage of the parliamentary party elected from Scottish seats, Scottish Labour was in a strong bargaining position. Also, the new Labour leader, Tony Blair, didn't have a clue about Scottish politics, but believed he had no choice on whether to deliver devolution.

Quite naturally, as the originators of the devolutionary ideas that grew into the Constitutional Convention's Claim of Right, the people running the Labour Party in Scotland became focused on Scotland's opportunities and challenges, and on the neglect or shortage of public investment north of Berwick that required policies made in Scotland.

Somewhere between then and now, the Scottish Labour Party evolved, and its website makes it quite plain that it is responsible for producing policy for the manifesto on which MSPs are elected. The website also hints that the Scottish party's decisions can't be overturned as they once were.

But the Scottish party is still a creature of the UK party, which holds the purse-strings. Also, there's a completely new element in the balance of power between Scotland and London: a cadre of MSPs who've been loyal to their Westminster colleagues, over asylum policy, for example, and who are shocked to the core by the lack of loyalty shown to them and their leader when they're being out-punched and out-politicked by the SNP . . . partly because they can't do everything they'd like because of possible adverse fall-out in England.

Maybe Labour Party activists should take a leaf from the Lib Dems' book and become structurally independent of Labour outside Scotland . . . nobody would even think of asking Nicol Stephen if he has permission from the Westminster Lib Dem leader to pursue his policies and campaigns.

Warning won't work
Thirteen's too young. Yet the age of consent in the Netherlands is 14 and the birthrate of babies born to gym-slip mums is lower than in this country. 

There are good medical reasons for trying to minimise an immature female's number of partners. Yet why should we imagine that warnings against early sexual activity because of the risk of cervical cancer will be any more successful in persuading young women to "Just say No" than the public information campaigns against smoking?

Brought to book
Did Cherie Blair and John Prescott write their books because they wanted to expand our knowledge of how the UK system of governance works, or deepen our understanding of the competing pressures and priorities that have to be juggled by prime ministers, their deputies and their families?

No chance. 
