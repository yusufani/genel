Yesterday's proposal from the Financial Services Authority to guarantee sums of up to �500,000 under the deposit compensation scheme so as to cover life- changing events such as the sale of a house looks sensible enough, but it is for the time being also completely irrelevant.

Ever since the Northern Rock debacle, there has been an effective Government guarantee of all deposits, however large. In attempting to re-establish financial stability, the Government has pledged that no one will lose a penny as a result of the banking crisis. The Government has been careful not to make the guarantee explicit, as occurred in Ireland, but the implicit understanding that all deposits are underwritten by the taxpayer has been quite helpful in stabilising the worst-affected banks.

The question is when and how to remove it, leaving the provisions of the Financial Services Compensation Scheme, which insures only the first �50,000 of each deposit, as the only safety net. Raising the ceiling to �500,000 for sums held for a limited period of time ought to address the perceived problem of what happens when large sums received for the sale of a house or a life insurance policy are caught up in a banking failure.

But it doesn't deal with the issue of weaning the public off the understanding that everything is underwritten by the taxpayer. For the time being, the banking system is judged to be still too fragile to allow removal of the blanket cover. Normality is still some distance off. 

With this week's insolvency of the Dunfermline Building Society, the Government has agreed to make up any losses above �50,000. The limited size of the bill makes this a comparatively easy thing for the Treasury to do. 

But with much larger deposit-takers, it is not yet possible to allow failure without considerable cost to the taxpayer. Already there are bitter complaints about the costs of funding the compensation scheme. Qualifying firms are being asked to put up a total of �562m to cover losses incurred in the year to March 2009 and the general levy for next year. In the midst of a banking crisis, this is for some a cost too far.

