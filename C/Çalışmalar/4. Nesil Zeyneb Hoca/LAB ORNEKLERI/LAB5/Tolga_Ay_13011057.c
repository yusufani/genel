#include <stdio.h>
#include <stdlib.h>

struct Student{
    char name[20];
    int grade;
    struct Student* next;
};

void addStudent(struct Student** root);
void removeStudent(struct Student** root);
void displayStudents(struct Student* root);

int main(){
    struct Student* head = NULL;

    while(1){
        int sel;
        printf("\n\nMenu:");
        printf("\n1) Add student");
        printf("\n2) Remove student");
        printf("\n3) Display students");
        printf("\n4) Quit\n>> ");
        scanf("%d", &sel);
        switch(sel){
            case 1:
                addStudent(&head);
                break;
            case 2:
                removeStudent(&head);
                break;
            case 3:
                displayStudents(head);
                break;
            case 4:
                exit(0);
                break;
        }
    }
    return 0;
}

void addStudent(struct Student** root){
    struct Student* newStudent = malloc(sizeof(struct Student));
    struct Student* it = *root;

    printf("\n\nName : ");
    scanf("%s", newStudent->name);
    printf("\nGrade : ");
    scanf("%d", &newStudent->grade);
    newStudent->next = NULL;

    if(*root == NULL)
        *root = newStudent;
    else{
        while(it->next != NULL)
            it = it->next;
        it->next = newStudent;
    }
}
void removeStudent(struct Student** root){
    int grade;
    printf("\n\nGrade : ");
    scanf("%d", &grade);

    struct Student* it = *root;
    struct Student* prev;
    if(it->grade == grade){
        *root = it->next;
        free(it);
        return;
    }
    else{
        prev = it;
        it = it->next;
    }

    while(it != NULL){
        if(it->grade == grade){
            prev->next = it->next;
            free(it);
        }
        prev = it;
        it = it->next;
    }
}
void displayStudents(struct Student* root){
    int count = 0;
    while(root != NULL){
        count++;
        printf("\n%d. %s %d", count, root->name, root->grade);
        root = root->next;
    }
}
