Tom Miks... 
BA�BAKAN Tayyip Erdo�an��n ilgin� bir ki�ili�i var. Kendisini kontrol etti�i zaman 28 �ubat sonras�n�n Erdo�an��n� g�r�yoruz. Kontrol�n� kaybedince veya �ok eski y�llarda �do�ru� diye inand��� hususlar�n irdelenmesi s�z konusu olunca kendisini tutam�yor, eski Erdo�an oluveriyor. 

Biliyorsunuz Say�n Ba�bakan��n laik cumhuriyeti koruma konusunda baya�� kararl� oldu�u izlenimini veren bir�ok resmi konu�mas� var.

Ger�i �laikli�i nas�l anlad���na� ili�kin a��klamalar� aras�nda �eli�kiler oluyor. Bir bak�yorsunuz �laiklik sadece din ve vicdan �zg�rl���n�n g�vence alt�na al�nmas�d�r� g�r���n� savunuyor. Bir ba�ka konu�mada �devletin t�m dini inan�lara ayn� mesafede olmas�d�r� tezini benimsedi�i izlenimini veriyor. ���nc�s�nde -aynen bir zamanlar Erbakan��n dedi�i gibi- �Bat� �lkelerindeki laiklik ne ise onu istedi�ini� s�yl�yor. 

Bunlar aras�ndaki fark�n da�lar kadar b�y�k oldu�unu ya g�rm�yor yahut g�rmezden geliyor.

En ilginci bazen de, bir insan�n laikli�i anlamad���n� -veya kar��t oldu�unu s�ylemeye cesaret edemedi�ini- g�steren eski bir s�yleme s���n�yor:

��nsanlar laik olmaz, ancak devletler laik olabilir. O nedenle ben laik de�ilim� diyor.

(Biliyorsunuz bu s�ylemi Turgut �zal icat etmi�ti. Onun da durumu bu ba�lamda Erdo�an�dan farkl� de�ildi.)

Bunlara neden de�indi�imizi tahmin etmi�sinizdir:

Konuyu Ba�bakan��n L�bnan gezisinde kendisine refakat eden gazetecilere �Yeni Ceza Yasas��n�n ka�ak Kur�an kursu a�anlar� cezaland�rmay� (fiilen) reddeden� maddesini savunan s�zleri nedeniyle an�msad�k.

Erdo�an, �Kur�an ��reniminin belli bir ya�tan sonra zorla�aca��n�� s�yleyerek, �imdi hem Milli E�itim Bakanl����n�n hem de Diyanet ��leri Ba�kanl����n�n �asgari ya� s�n�r�n� 12 ve 15�ten a�a�� �ekme� amac�yla �al��ma yapt�klar�n� bildiriyor. Ve g�r���n� savunmak i�in demagojik a��dan �ok �arp�c� bir �rnek veriyor:

�K���k ya�taki �ocuklar�m�za Tom Miks okutmaya kimse mani olmuyor da, Kur�an okumalar�na neden mani olunuyor?�

Ba�bakan��n bu s�zlerinin ger�e�i yans�tmad���n� s�ylemek zorunday�z. ��nk� ikisinin mukayese edilebilmesi i�in �nce Tom Miks ��retmenin laik sistemle �at��abilecek bir boyutu olmas� laz�m. Oysa o yok.

Keza hi�bir �a�da� e�itim sistemi Tom Miks (veya benzeri bir ba�ka kitap) okumay� yahut okumamay� m�fredat i�ine koymaz. Oysa �teki �din ve ahlak dersi� kapsam�na girer ve laik devlet, o konuda yapmas� gereken ne ise o kadar�yla yetinir, yetinmeye de mecburdur.

Kald� ki Say�n Ba�bakan��n benimsedi�i �laiklik� tan�mlar�ndan birine g�re, devlet t�m dinlere ayn� mesafede olacaksa, o zaman �Kur�an� Kerim okutma ve ��retme� devletin g�revi de�ildir. Devlet, vatanda�lar�na sadece laik cumhuriyet i�in tehdit te�kil etmeyecek �ekilde Kur�an okumay� ��renme kolayl�klar� sa�layabilir. Bunu da �teki dinler i�in ne kadar yap�yorsa, M�sl�manl�k i�in de o kadar yapar.

Daha fazlas�n� yaparsa ne oldu�unun yan�t� tarihimizde yaz�l�d�r.

Not: �B�yle ba�a b�yle t�ra�� ba�l�kl� yaz�mda, bir bilimsel �al��mas�ndan al�nt� yapt���m eski Maliye Bakanl��� M�ste�ar� Biltekin �zdemir, ���kar ve bask� gruplar�n�n etkisi alt�nda kald���ndan� s�z etti�i siyasi iktidar�n 1999 Koalisyon H�k�metiyle ilgili oldu�unu bildirdi. A��kl�yorum. O.E. 
