Gribi �nemsemezseniz zat�rreeye d�n��ebilir 
Ani ate� y�kselmesi ve kas a�r�lar�yla ba�layan gribin ge�mesi, ortalama ��-be� g�n s�rer. 

Fakat kuru �ks�r�k, nefes darl��� ve yorgunluk gibi belirtiler iki hafta kadar devam edebilir. So�ukalg�nl��� ve gribin kesin bir tedavi y�ntemi yoktur. Re�etesiz sat�lan ila�lardan yard�m beklemek yerine, doktora gitmekte fayda vard�r. ��nk�, e�er ihmal edilirse, gribin zat�rreeye d�n��me riski hi� de az de�ildir. 

Y�ksek ate� ve dayan�lmaz a�r�larla yata�a d��medi�imiz s�rece so�ukalg�nl���, nezle, grip gibi sorunlara re�etesiz sat�lan ila�larla ��z�m bulmaya �al���yoruz. �ki-�� g�n i�inde hastal���n belirtilerinden kurtulmay� beklerken sorunlar�n devam etmesi bizi �a��rt�yor. Sa�l���m�za kavu�mak i�in gerekenleri yapmam�za ra�men hastal���n b��ak gibi kesilmeyece�ini unutmayal�m. �yile�mek zaman al�r. Bo�az a�r�lar�, burun t�kanmas� ve hap��r�klarla kendini belli eden so�ukalg�nl���n�n tamamen ge�mesi, ortalama olarak bir hafta s�rer. Baz� vak'alarda iyile�me s�reci iki haftaya kadar uzayabilir.

AKUT BRON��T R�SK�

So�ukalg�nl���na yakaland�ktan sonra belli bir s�re i�inde sa�l���n�za kavu�amayabilirsiniz. ��nk� bazen komplikasyonlar ortaya ��kabilir. �rne�in sin�zit. E�er so�ukalg�nl���n�n ikinci haftas�nda y�z�n�zde a�r�lar, burunda t�kanma ve �iddetli ba� a�r�lar�ndan �ik�yet�i olursan�z, bakterilerden kaynaklanan sin�zit ihtimali ortaya ��kar. Bu sorunun giderilmesi i�in 10-14 g�n s�recek bir antibiyotik tedavisi �nerilir.

Akut bakteryel bron�it yani nefes yollar�n�n �eperlerinde iltihaplanma, bildi�imiz so�ukalg�nl��� belirtilerini sergileyebilir. Fakat s�rekli �ks�r�k, sorunun basit bir so�ukalg�nl��� olmad���na i�arettir.

Vir�slerin neden oldu�u brorn�it bir hafta i�inde etkisini kaybeder. Ama �ks�r�rken t�slamaya benzer bir ses ��kar�yorsan�z ve de solu�unuz kesiliyorsa, pn�moni (zat�rree) ihtimalini akl�n�za getirip doktora ba� vurmal�s�n�z. E�er �ks�r�rken ye�ile �alan sar� renkte balgam ��kar�yorsan�z, bakterilerin neden oldu�u bron�ite yakaland���n�z anla��l�r. Bu durumda size antibiyotik tedavisinin uygulanmas� gerekir. Bakterileri bulup �ld�rd�kleri bilinen antibiyotikler bo�az iltihaplanmalar�na kar�� da etkilidirler.

ASTIMIN TE�H�S� ZOR

Baz� durumlarda ge�mek bilmeyen �srarc� �ks�r�k ve burun t�kan�kl��� alerji ya da saman nezlesi habercisi olabilir. 

Solunum yollar� hastal�klar� aras�nda ast�m�n te�hisi ne yaz�k ki pek de kolay olmuyor. Geceleri uyku ka��ran kuru �ks�r�k �nemsenmeli. Hele �ks�r�kle beraber g���ste s�k��ma ve durup dururken nefes almakta zorlanma gibi belirtiler varsa, hi� zaman kaybedilmeden doktora ba�vurulmal�.

