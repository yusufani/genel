Don't expect next week's Budget to cast much light on where the spending axe is going to fall to repair the now disastrous state of the public finances. There's an election looming, and whereas the honest thing for political parties to do would be to spell out precisely how they plan to address the problem, enabling voters to take an informed, self-interested approach as to who they might vote for, that's not the way politics works.


For the time being, the Government's economic policy seems to amount to muddling through, letting the public finances go to hell in a handcart and leaving it to the next lot, whoever they may be, to sort out the mess. Look after the short term, John Maynard Keynes once said, and the long term will take care of itself.

This Micawber-like attitude just will not do. No country can credibly run its public finances in the hopeful expectation that "something will turn up", or that the resumption of economic growth alone will eventually correct any shortfalls. 

In a recent paper, the Institute for Fiscal Studies concluded that in order to get the public finances back to where they were supposed to be by 2015/16, the Government would have to impose a fiscal tightening over and above that already proposed of �39bn annually, equal to virtually the entire schools budget or �1,250 in extra taxes for every family in the land.

"Annual income twenty pounds, annual expenditure nineteen pounds nineteen and six, result happiness. Annual income twenty pounds, annual expenditure twenty pounds ought and six, result misery." So says the Micawber Principle. The Government is in danger of assigning the country as a whole to the same Dickensian misery of the debtors' jail. Certainly it doesn't seem to have a clue as to how to get us out again.

To be credible, the Budget must contain, at least in outline, some sort of medium-term plan for restoring health to the public finances. As I say, this has to go beyond pencilling in a few overly optimistic forecasts on future economic growth. To win credibility, the plans have to include real cuts in public spending over a period of years, and or real increases in the tax burden. 

According to the IFS, to plug the �39bn gap, the Government will in the absence of any further tax-raising measures need to reduce growth in public spending by 1.1 per cent a year. This would amount to a five-year real freeze in public spending. Could a Labour Government get away with such a draconian approach to the public sector? Even the Conservatives would blanch at the idea. Certainly no postwar government has ever achieved such a sustained squeeze.

Public spending is like the mythical Hydra - cut off one of its heads and another two grow back in its place. Its growth seems quite unstoppable, whatever governments do to curtail it, and this is not a government known for its cost-cutting zeal when it comes to public spending. 

There are peripheral spending budgets that can quite easily be chopped, but they tend to be small beer, and, as with infrastructure spending, just the sort of thing you don't want to cut in a downturn. But making significant inroads into the big spending departments is much more difficult. 

The demands of an increasingly health-conscious, ageing population mean that National Health Service spending will keep rising in real terms whatever the Government does to rein it in. Despite hair-shirted intentions, health spending has always grown in real terms right through the most intense of public spending squeezes. Even the Thatcher government, with its mandate to roll back the frontiers of the state, failed to dent the inexorable growth of health spending. There is no reason to believe future governments capable of breaking this pattern.

So if significant spending cuts are not an option, what of taxes rises? There are already quite a few of these in the pipeline, including the new 45 per cent rate for higher-income earners. To go further at a time when the whole purpose of policy is to restore confidence and demand would seem like folly. Demand would be further damaged by big tax hikes.

If front-line taxes rises are likely to prove counter-productive, what are the alternatives? You'd think that Gordon Brown had plain run out of road when it comes to stealth taxes. Such is his penchant for them that he must surely already have used them all up. Unfortunately, there is still a bit of low-hanging fruit left to pluck, most notably the higher-rate tax relief for pensions savings.

The very thought of it provokes a shudder and a strong sense of d�j� vu among pensions providers, for it was pensions which were the subject of Mr Brown's first stealth tax � the abolition of tax credits on dividends. This has been worth a cumulative �5bn a year to the Treasury ever since, or around �60bn in total. 

The gap it has filled in the public finances is equalled by the one left in Britain's once universally admired system of privately funded occupational pension schemes. These were probably going the way of the flesh regardless, but the Government's action in removing the tax break has certainly hastened them on their way, with virtually all final salary pension schemes now closed to new members, and crippling legacy costs for sponsoring companies.

In any case, with the public finances in such appalling shape, the insurance industry is more than usually alert to the possibility that the Government will be back for a second raid on the pensions piggy-bank. Certainly, the idea must be superficially attractive to a government as desperate as this one. For starters, the relief is worth a very considerable sum of money. According to the Pensions Policy Institute, the total cost of the relief for 2004/5 was an astonishing �37.7bn, or 2.9 per cent of GDP.

This sum includes �8.3bn of lost national insurance which would have been paid had employer pensions contributions been paid as salary together with some �15bn of pension contributions from employers, which presumably would be left largely untouched by any attack on higher- rate tax relief. I say presumably, but these are desperate times.

However, even netting these sums off leaves around �8bn of employee tax reliefs of which rather more than a half relates to higher-rate taxpayers. Call the sum that the Government could potentially save a nice round �5bn (eerily similar to the dividend credit raid), and you'd be in the right ball-park. Such a tax hit could quite easily be presented as "progressive" if accompanied by a more generous rate of basic tax relief available to all pension savers. Higher- income earners would lose out, but low earners would get extra relief. 

Better still, it is one of the few effective tax increases that could be achieved without any substantial short-term impact on demand, since this is money that would otherwise be saved rather than spent.

Every year, the insurance industry works itself up into a terrible lather over the possibility that the Government might attack this previously sacrosanct form of tax relief. To date, these fears have proved unwarranted. The industry has been crying wolf. Yet you can see why this time around they might seem more than justified.

Is the Government about to hit the button? It's all a question of what politically ministers think they might get away with. Not everyone who uses the higher-rate relief can be described as "well-off". With the higher-rate tax band kicking in at �36,000, a considerable cohort of middle-income earners would find themselves hit by the move.

It would also send out a powerfully negative message on pensions at a time when the Government is trying to encourage people to save for their old age so that they won't become a burden on the state. The Government could cynically choose to take the view that higher-income earners will save anyway; the problem of absent saving is rather one largely confined to low earners. All the same, it's difficult to impossible to remove an established tax relief without political consequences.

Kenneth Clarke, the last Tory Chancellor and now the shadow Business Secretary, once said that he gave up on that chronic obsession of chancellors � tax reform � after realising that whatever he did there would be winners and losers, and that inevitably he would be resented by the losers while gaining no credit whatsoever among the winners.

Politically, removing the higher- rate tax relief would have a very similar effect. It's dangerous stuff ahead of an election. Regrettably, it is going to be very hard to resist for any incoming government charged with cleaning up the present mess. Whatever the new government does is going to be unpopular. Despite the fact that removing the higher-rate relief would further discourage saving, might well further swell already burgeoning pension fund deficits, and would be a body blow to the personal pensions business of the insurance industry, it may well look like one of the least bad options.

