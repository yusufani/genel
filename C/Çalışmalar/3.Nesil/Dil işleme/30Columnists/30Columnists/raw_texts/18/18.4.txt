HBOS was under mounting pressure on Monday as its shares fell further towards its rights issue price on the day Royal Bank of Scotland announced the successful completion of its own record-breaking �12bn cash call.

Shares in the Halifax Bank of Scotland group were the biggest losers in the FTSE 100 after falling 7% to 307p - closer to the 275p at which the bank is asking shareholders to subscribe to its �4bn rights issue. They fell as low as 300p at one point.

Ever since Bradford & Bingley was forced to take unprecedented steps to reprice its cash call last week, HBOS has faced questions about its fundraising which relies on two million private investors to take up their entitlement to the shares. The investment banks who have underwritten the issue could be left with a larger than usual "rump" if retail investors - more than 25% of the shareholder base - shun the new shares.

HBOS insisted that the share issue was proceeding "according to plan" as it became clear that the authorities had been drawing up a contingency plans for the high street banks to support B&B if its rights issue had failed. 

RBS shares fell nearly 5% to 233.75p even though 95.11% of shareholders gave their backing to the cash call. Investors are now calling on the bank to beef up its board, accelerate the integration of Dutch bank ABN Amro, sell off its insurance arm and avoid further credit crunch writedowns.

Chairman Sir Tom McKillop and chief executive Sir Fred Goodwin are under intense pressure to bolster the performance of the bank, which has promised to appoint three new non-executive directors, including one from the UK who will be the next senior independent director and likely chairman. A shortlist has been drawn up and shareholders are keen for an appointment to be made.

After a charm offensive by the RBS board, reluctant shareholders agreed to buy the new shares at 200p. The remaining 5% were placed by the underwriters Merrill Lynch, Goldman Sachs and UBS later on Monday to raise a total of �12.29bn - the extra funds helping to cover fees of �246m. 

RBS shareholders are already turning their attention to the Edinburgh-based bank's trading update on Wednesday. "They've got to get a full price for the disposals, deliver faultlessly and ahead of schedule on ABN and make no more write-downs," said one investor.

Analysts at Citi noted that the trading update would be a "key catalyst, given the opportunity to not only disclose on current trading but to also provide clarity on the progress of the ABN integration by disclosing for the first time pro-forma half-year numbers".

The statement will be scrutinised as RBS assured the City last week that its trading was on track in an attempt to calm nerves after the furore caused by the shock profit warning that forced B&B to reprice its own cash call. The price was cut from 82p a share to 55p and B&B also sold a 23% stake to private equity firm TPG.
