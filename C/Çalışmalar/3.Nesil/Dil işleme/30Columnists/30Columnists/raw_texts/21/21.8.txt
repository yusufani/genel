Is that more green shoots poking their way through the frozen tundra of Britain's still snowbound housing market? That's what the latest data seems to imply, with mortgage advances slightly up again last month, and, according to the Royal Institution of Chartered Surveyors, new inquiries higher for the fifth consecutive month. More significantly still, says the Rics, the sales-to-stock ratio is up for the fourth month in a row.

All this is mildly encouraging and would certainly suggest that the fall in the market is beginning to slow. Yet any hope of a rapid and sustained bounce back is for the birds. 

Even in a recession, you will always find areas of the market where demand is still high and supply is short. Some house prices haven't fallen at all during this downturn. Unfortunately, these tend to include virtually all the really desirable family properties that you've any chance of ever aspiring to. On the other hand, if you want a new-build apartment in central Leeds, you may be able to pick it up for half the price of two years ago.

Housing has a utility value that makes it behave differently from other asset prices, particularly in Britain, where property ownership is something close to a national religion. It's hard to shake this faith, especially when most other assets seem to be doing even worse. Even with falling prices, a house might seem a rather safer home for your money than a bank.

All the same, with the ranks of the unemployed expected to swell by more than a million over the next year, repossessions rising by the week, and mortgage finance on anything less than a 25 per cent deposit still a rarity, it's hard to see the market as a whole recovering any time soon. Perhaps the best that can be hoped for is that things may soon bottom out, but with possibly years of nil or negligible growth in prices to come.

The most reliable gauge of house prices is relative to average earnings. On this measure, they've already dropped by more than 25 per cent since their peak of September 2007, but have quite a bit further yet to go before they reach their long-run average. We'll see.

