A stuffy head, a sore throat and sneezing fits that could leave you with a migraine: according to some surveys, about 20 percent of Americans report experiencing symptoms like these at the same time every year. That would suggest they are due to an allergy, not a cold. But how to tell the difference? 


Symptoms of seasonal allergies and colds overlap, but studies suggest there are ways to tell them apart. 

The first is the onset of symptoms. Colds move more slowly, taking a day or longer to set in and gradually worsening -- with symptoms like loss of appetite and headache -- before subsiding after about a week and disappearing within 10 days. But allergies begin immediately. The sneezing is sudden and overwhelming, and the congestion, typically centered behind the nose, is immediate. Allergy symptoms also disappear quickly -- almost as soon as the offending allergen, like pollen, is no longer around. 

Then there are hallmark symptoms of each. Allergies virtually always cause itchiness, in the eyes, the nose, the throat, while a cold generally does not. Telltale signs of a cold are a fever, aches and colored mucus. 

If confusion persists, consult your family tree: studies show that having a parent with allergies greatly increases your risk, particularly if that parent is your mother. 

