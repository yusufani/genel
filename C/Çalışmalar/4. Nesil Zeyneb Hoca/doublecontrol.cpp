/**
 * @author Yusuf Anı
 * @email yusufani8@gmail.com
 * @create date 2018-11-19 17:06:29
 * @modify date 2018-11-19 17:06:29
 * @desc [Checking float control statment]
*/
#include <stdio.h>#include<stdio.h>
int main()
{
 float a=0.3;
 double b=0.3;
 printf("%20.18f %20.18f",a,b);
 if(a==0.3)
  printf("Hello World!");
 else
  printf("Stack Overflow");
 return 0;
}