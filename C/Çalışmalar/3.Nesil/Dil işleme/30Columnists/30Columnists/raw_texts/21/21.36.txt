Most chief executives outstay their welcome. Mark Tucker, chief executive of Prudential, seems to be doing the reverse. He's leaving with almost indecent haste after little more than four years at the helm. Yet despite the arrival of a new chairman earlier this year and rumours of strat-egic differences � Mr Tucker is said to have been hot to trot over the acq-uisition of AIG's Asian assets while the new chairman may not have been quite so keen � I think we have to accept his insistence that he's going of his own accord having achieved much of what he set out to do at the Pru.

Mr Tucker may have been chief executive for only four years, but excepting a brief sojourn as finance director of HBOS (bet he's relieved he didn't stick around there, given that it might even have made him chief executive had he done so), he's been with the Pru virtually man and boy, having been the key figure behind the insurer's successful Asian expansion.

Mr Tucker also leaves on a relative high note, if that's possible in the current climate, with profits still robust, a dividend increase and a free capital ratio that puts other leading British insurers in the shade. What's more, most of his strategic calls seem to have turned out right. He's sold Egg and the Taiwan agency business, he's dealt with the capital and operational issues, and he's slimmed down the once-dominant UK operation.

His decision not to attempt reattribution of the inherited estate also seems to have been the right one. With market conditions dramatically changed, Aviva, which went the other way, is struggling to afford the payments it promised policyholders.

Mr Tucker reckons he's got at least one more big job left in him before he hangs up his boots. There are lots of top jobs going in banking, I suggest, including possibly Lloyds Banking Group, now the not-so-proud owner of the aforementioned HBOS. You are joking, he responds.

The new man from the Pru, Tidjane Thiam, seems positively exotic by comparison, and not just because he is set to become the first black FTSE 100 CEO. He's also a former government minister of his native Ivory Coast, where he was forced to flee a military coup. He seems to be inheriting a relatively ship-shape vessel. Mr Thiam's star-studded career to date suggests that he aims to keep it that way.

