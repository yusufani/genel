You never thought this day would come�your little bundle of joy now wants to lift weights.  Before you panic, read my column on the safety of strength training for kids. You�ll learn that kids who possess the necessary body awareness skills to master proper form are ready to take on the challenge of strength training. You�ll also learn the many benefits of youth strength training- from increased muscular endurance to improved self-image.

Proceed with Caution
The first step in determining whether your child is ready to lift weights is to assess his or her maturity level.  Kids have to understand that the goal is to lift a reasonable amount of weight correctly, not strain to lift the most weight possible. If your child likes to entertain his friends by hurling a heavy weight overhead, he�s not ready. The second step is to make sure your child has mastered proper form.  Using the correct body alignment while lifting weights prevents injury and delivers the best results.  For more information on this concept, click here.  

Start at Home
As you know, kids go through phases. One day they�re crazy about in-line skating, then a week after you�ve bought all the gear, they�re on to something new. Before shelling out money for a gym membership, find out if your child is serious about strength training by setting up a small home gym. (Suggestions for equipment are listed below.)  If the home gym gets a lot of use, you can feel better about setting your teen loose in a gym environment. Although no teen wants mom or dad hovering over them, a home gym enables you to observe them periodically to determine their proficiency in weight training. Depending on your parent/teen relationship, you could even be workout partners.

Get Equipped
Anything your child needs for a basic weight training room can be purchased at a sporting goods store. You may be able to find a few of these items second-hand at an on-line auction or garage sale. Here�s a suggested checklist:

Upper body muscles

Elastic tubing, sometimes called an Exertube, is a great go-to tool for multiple strength training exercises.  It�s inexpensive, about $10.00 per band, and available in various levels of resistance.  Every time I use an Exertube in class, the kids are intrigued. 
Dumbells with a weight rack are another option.  They�re more expensive and take up more space, but appropriate for experienced teens.  I recommend starting with sets of 5lb., 8lb., and 10lb. weights. 

Lower body muscles

A Body Bar, or something similar, works well to add weight to lunges or squats.  They�re soft-coated, easy to grip, and available in various weights. 
Some teens, especially boys, will insist on using a barbell with weight plates rather than a Body Bar.  Since this is what they will eventually use in the gym, it�s a good idea they learn how to use it correctly.  But I recommend an adult acts as a �spotter� when the barbell and weight plates are in use.

Upper and lower body muscles

A medicine ball is a fun tool that adds variation to a strength training workout.  There are a number of sites on the web where you can find medicine ball exercises that work all the major muscle groups.  I�ll list some as well in an upcoming column on strength training.

Abdominal work

All you really need for abdominal work like crunches and reverse curls is a mat.  Find a foldable one for easy storage.

Total Body

Probably the most important item on the teen weight room list isn�t a weight at all.  A full-length mirror is a must-have for checking proper form.  Don�t be surprised if you see a little flexing and posing in front of the mirror too.

Check back here in the coming weeks for columns on strength training exercises and gym etiquette for teens.

