Fatih-Hakan ve R�za hoca 

 
 

T�rk futbolunda "Baba" lakab�n� her yan� ile hak eden bir lider insand� G�nd�z K�l��... Rahmetli sa� olsayd�, �oooktan "Mil Tak�m'a al�nmamas�n� mesele yapan Hakan ��k�r" gibi "HAKAN'�n yerine Milli Tak�m'a alman Trabzonlu Fatih Tekke" onun "Baz� resimler, baz� isimler" k�sesine konu olmu�lard�. Yine ayn� k�seden �ok sevdi�ini yak�nen bildi�im R�za �al�mbay'a da iki �ift laf ederdi.

Altyap� fark�
�nanyorum ki, benim ula�t���m "Fatih Tekke kaynaklar�na" baba da ula��r, dinleyece�i adam� iyi sezdi�inden, Giray Bulak hocan�n anlatt�klar�n� can kula�� ile dinlerdi. Sevgili Bulak hocam, ge�enlerde kar��la�t���m�z bir �d�l t�reni gecesinde Fatih Tekke'nin �zelliklerinden bahsederken �unlar� anlatm��t�: "Onun altyap�dayken ne oldu�unu g�rmenizi isterdim. Bazen gen� tak�mlar aras�nda ma� yapt�r�rd�k, Fatih'in yer ald��� tak�m rakip tak�m� 15-3, 12-3 falan yenerdi. Fatih'i d��ar� al�rd�k, ayn� iki tak�m ma�a devam ederdi, sonu� 2-2'ye ya da 3-2'ye d�nerdi. Fatih'i tekrar sahaya sokard�k, onun oynad��� taraf fark� a�maya ba�lard�. Fatih Tekke daha altyap�dayken bile, taraflar�n dengesini kendisinin yer ald��� tak�m lehine farkl� sonu�lara ula�t�racak �zelliklere sahip m�thi� bir �ocuktu. Bir tek eksik yan� vard�, kafas� biraz kar���kt�. Bu y�zden �ok zaman kaybetti. Ama ya�land�k�a kafas� da oturmaya ba�lay�nca, bug�nk� Fatih ortaya ��kt�..."

Hakan ��k�r'�n futbol ya�am� ise meziyetleri ile yer ald��� altyap�da do�up geli�memi�ti. Rahmetli Tamer Kaptan'�n �al��t�rd��� Sakaryaspor, maddi s�k�nt�lar y�z�nden transfer yapamad���ndan, ba�kan Ayd�n Zengin bir g�n hocaya ��yle demi�ti: "Basketbol oynayan gen� bir �ocu�umuz var. Futbolu da �ok seviyor. �stersen onu antrenmanlara al, dene..."

Tamer hoca da onu antrenmanlara alm��, yokluktan bir "Hakan ��k�r" ger�e�i do�maya ba�lam��t�. Elbette �ok �al��m��, sahada "kargalar� korkutmak i�in tarlaya konulan korkuluklardan, kalecileri korkutan duruma" gelebilmek i�in atletik yap�s�n�, v�cudunun estetik yap�s�n�... Ama en �ok da sahaya s��mayan y�re�ini m�kemmel kullanm��t�.

Ama bug�n art�k 33 ya��n �zerine ��km��t�. Y�re�i �ok istemesine ra�men onu eskisi gibi sahan�n her yerine ta��n�yordu. Futbolunun altyap�s� olmad���ndan da kendisinden beklenen bir�ok �eyi art�k yapam�yordu. Baba G�nd�z sa� olsayd� onu incitmemeye �zen g�stererek, k�sesinden kendisine s�yle seslenirdi: "Milli Tak�m'� b�rak, sen G.Saray'� ta��maya �al��!"

R�za'n�n formas�
Be�ikta�'�n bug�n ya�ad��� h�sranda elbette R�za �al�mbay'�n g�nah� asla yoktu. Ondan, dalgal� denizde su almaya ba�layan tekneyi kurtarmas�n� istiyorlard�. Zaman da vermiyor "hemen... hemen..." diyorlard�.

Rahmetli baba �ok sevdi�i R�za'y� futbolculuk g�nlerinde ��yle tarif ederdi: "Makyajs�z dupduru bir futbol g�zelli�i. �ocuk sahada tek ba��na tak�m gibi duruyor..."

Ya�asa, onun R�za hoca hakk�ndaki d���nceleri asla de�i�mez, Del Bosque diye bir "garabetin" bozdu�u Be�ikta�'� onun d�zeltece�ine inan�rd�. Ama yine de �u laf�na mutlaka takar ve tak�l�rd�: "Ben futbolcuya forma vermem, duvara asar�m. �al��arak hak eden onu al�r, giyer..." M�sade edersen "BABA"n�n yerine ben soruyorum be hocam: "Sergen, duvara ast���n formay� �al��arak hak etti�i i�in mi al�p giyiyor?" 

 
