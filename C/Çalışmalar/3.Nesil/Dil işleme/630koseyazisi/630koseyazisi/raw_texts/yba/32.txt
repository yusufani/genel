Atat�rk'�n K�br�s vasiyeti  
     

  
KIBRIS T�rklerini tek ve vazge�ilmez g�vence kaynaklar� olan Atat�rk T�rkiyesi'nden so�utmak ve koparmak isteyenlerin propagandas� �u temay� i�eriyor:

��T�rkiye, K�br�s T�rkleri i�in de�il, kendi g�venli�i, esenli�i ve gelece�i i�in m�cadele veriyor. K�br�s'ta T�rklerin yok olmas�, T�rkiye'nin umurunda de�il, Tek bir K�br�sl� T�rk kalmasa bile T�rkiye'nin K�br�s'a ilgisi s�recektir...��

�lkelerini ve miras b�rakt��� �a�da� d���nce sistemini hi�bir zorlamaya gereksinim duymadan g�n�lden ve toplumca �z�msedi�imiz b�y�k Atat�rk'�n K�br�s'a bak�� a��s�n� irdelemeden �nce varolu� m�cadelemizin ilkesini bir kez daha an�msayal�m:

K�br�s T�rkl���n�n T�rkiyesiz var olabilmesi kesinlikle olanaks�zd�r. Bizi bug�nlere getiren yurtseverli�imizin ve milliyet�ili�imizin temelinde, gerekti�inde Anavatan ve y�ce ulusal ��karlar u�runa kendimizi feda edebilme bilinci vard�r. Bu temel bilin�ten yoksun olsayd�k, bug�ne kadar varl���m�z� s�rd�remezdik. T�rk halk�n� 'cemaat' d�zeyinden devlet kurumla�mas�na ta��yan TMC'ciler kutsal g�reve ba�lad�klar�nda, kendilerini T�rk ulusuna adad�klar�na ili�kin ant i�erlerdi.

Anadolu T�rkl���n�n K�br�s'a olan ilgisini her zaman s�cak tutabilmek i�in m�cadeleci K�br�s T�rk�n�n, adan�n T�rkiye a��s�ndan ta��d��� ya�amsal ve stratejik �nemi �n plana ��karmas� da, bu bilincin ak�lc� g�stergesidir. Bu ba�lamda, Anadolu ile y�r�t�len kader dayan��mas� hep ba�ar�l� oldu. K�br�s'�n Yunanistan'a ilhak�n� �nleyen ve halk�m�z� uluslararas� siyasal haklarla donatan dayan��man�n bundan sonra da s�rd�r�lece�ine hi� kimsenin ku�kusu olmas�n..

Ege Denizi'nde, Yunanistan'a el �abuklu�uyla ve alt�n tepsi i�inde verilen adalarla ku�at�lm�� bulunan T�rkiye'nin tek a��k olan k�y� kap�s� g�neydedir.

Ege'den ba�layan Yunan ku�atmas�n� k�racak ve T�rkiye'nin tek a��k k�y� kap�s�n� g�venceye alacak stratejik ada K�br�s't�r. T�rkiyesiz K�br�s T�rkl���n�n var olabilece�ini d���nmek ise sa�mal�kt�r.

K�br�s T�rk�, b�ylesine stratejik bir platformda varl���n� ulusal davaya ve Anavatan'a adamaktan asla eziklik duymaz; onur duyar... T�rkiye de varl���n� K�br�s'a adayan bir duyarl�k i�indedir.

B�ylesine onurlu bir ilke birlikteli�inin sava��mc�s� olmak, Atat�rk��l���n de gere�idir.

Neden mi?

��nk� Atat�rk, vasiyet nitelikli vurgulamas�nda, K�br�s'�n Anadolu'nun g�venli�i, esenli�i ve ekonomisi a��s�ndan ta��d��� ya�amsal �neme dikkatleri ��yle �ekmi�tir:

��Efendiler, K�br�s d��man elinde bulundu�u s�rece ikmal yollar�m�z t�kan�r. K�br�s'a dikkat ediniz. Bu ada bizim i�in �ok �nemlidir...��

Onun i�in diyoruz ki, Atat�rk�� oldu�unu �ne s�ren hi� kimse Atat�rk'�n K�br�s'la ilgili vasiyetini �i�neyen yakla��mlara itibar edemez. Ederse, Atat�rk��l���n yan�ndan ge�emez.

K�br�s'la ilgili vasiyetini, kritik g�nlerde bir kez daha an�msatan ebedi �����m�z Mustafa Kemal Atat�rk'e sayg�, minnet ve ��kran...

Bug�nlerin ortam�nda da rehberli�ini s�rd�r�yor.

Ahmet TOLGAY-Cumhuriyet Meclisi �zel Kalem M�d�r�-LEFKO�A



T�rkler, NBA'y� futbol ma�� izler gibi izliyor


AMER�KA'da ya�ayan bir T�rk'�n, Hido'lu NBA'dan izlenimini aktar�rken ��Amerikal�lar, basket ma�lar�n� sinema izler gibi izliyor. Ancak biz T�rkler farkl�yd�k. Bir anda salonda bir u�ultu koptu; 2 bin T�rk �Hido buraya yumruk havaya' diye tempo tuttu�� demesine k�zan Mahmut Ekenel (Missouri-ABD) bak�n nas�l kar��l�k veriyor:

��Bu arkada��m�z zahmet edip o haftan�n Fox Sport kanal�n�n NBA �zetlerini izlemi� olsayd�, Amerikal�lar�n biz T�rklerle nas�l dalga ge�ti�ini g�r�rd�. Haberin ba�l��� ��yleydi: ��T�rkler NBA ma��n� soccer (futbol) ma�� gibi izlediler.��


T�rk ne demek?


BBG'den k�sa sa�l� olan gen� bayan arkada�, taksisine binen san�r�m �� gen� ��rencinin onunla yapt��� muhabbeti di�erlerine anlat�yordu; �zellikle onlar�n yorumlar�n�, d���ncelerini BBG'ye kat�lanlar hakk�nda... O ara konuyu detayland�rmak i�in ��Adlar� de�i�ik yabanc�yd�lar��, bir ara d���nd�kten sonra ��San�r�m Ermeniydiler�� dedi. Ger�i k�t� bir niyet yok ama yanl�� bir bak�� a��s� var. B�yle �ok insan oldu�unu d���n�yorum.

Hala ��renemiyoruz. Adlar� farkl� olan, inan�lar� de�i�ik olan ya da hi�bir inanc� olmayan, T�rkiye pasaportu ta��yan, bu �lkenin yurtta�� olan herkes en az bu arkada� kadar T�rk't�r, hatta daha fazla bile T�rk olabilir. Onlar yabanc� de�ildir. Turist hi� de�ildir. Keza bir�ok turist (�zellikle bir�ok iyi e�itimli Alman ba�ta olmak �zere) bizim tarihimizi, �lkemizi yani bize ait olan bir�ok �eyi milyonlarca T�rk oldu�unu s�yleyen insandan iyi bilirler. Adlar� Ay�e, Mehmet de�il diye veya ba�ka farkl�l�klar� var diye binlerce y�ld�r bu topraklarda ya�ayan ve bizden daha biz olan bu insanlara l�tfen 'yabanc�lar' demeyelim. San�r�m buna en fazla Almanya'da hem adlar�, hem inan�lar�, hem ya�am tarzlar� ayr� olan, orada sadece 40 y�ll�k bir tarihi olan ve 'yabanc�' diye bir kenara itildi�ini hisseden ve bu tav�rdan kurtulma m�cadelesi veren gurbet�i T�rkler �z�l�rler.

G.SARA�-�STANBUL


G�N�N S�Z�


��T�rkiye'de hi�bir okulumuzda �evre dersi yok. �evre hareketinin kurumsalla�mas� e�itimle olur. ��rencilerimiz, daha ye�ilin ekolojisini bilmiyor. Ye�ili, g�ze ho� gelen bir renk olarak tan�ml�yor.��

(Prof. Dr. G�ven G�K Mu�la �niversitesi ��retim �yesi)


MESAJ 


BAYRAM'da Metro firmas� m��terilerine sayg�l� davranmad�. Antalya'dan Ankara'ya d�nmek �zere bilet bulamad�k; daha sonra 00.30'da bir otob�s bulundu�unu s�ylediler. Eski bir otob�s ve korsan �al���yordu. Tang�r tungur Ankara'ya geldik. Metro ayn� saatte Ankara'ya tam 8 korsan otob�s kald�rd�. B�yle g�nleri f�rsat bilerek korsan otob�slerle yolcu ta��yan, haks�z kazan� elde eden ve yasalara meydan okuyan Metro ve di�er otob�s firmalar� i�in neden �nlem al�nmaz, kontrol yap�lmaz?

B.�. / ANKARA

ORD. Prof. H�fz� Veldet Velidedeo�lu �l�m�n�n 10. y�ld�n�m�nde yar�n T.Z. Tunaya K�lt�r Merkezi'nde (T�nel, Beyo�lu) an�lacak. Prof. Necla Arad'�n konu�mas�ndan sonra Meri� Velidedeo�lu'nun haz�rlad��� Ali D��enkalkar ve �zlem Karaman'�n sunacaklar� '88 Y�ll�k Ya�amdan An�lar' adl� saydam g�sterinin ard�ndan 'AB �er�evesinde Ulus-Devlet' konferans�n�n konu�mac�lar�; Prof. Erol Manisal� ve ��kran Soner.

EM�N�N� Tiyatro Caddesi'nde (Azak Yoku�u) iki ayd�r patlayan la��mla ilgilenen yok. Buras� oteller caddesi, turist kayn�yor. Kokudan duram�yoruz. L�tfen bir �are.

Hayrettin B�NG�L Esnaf-EM�N�N� 
