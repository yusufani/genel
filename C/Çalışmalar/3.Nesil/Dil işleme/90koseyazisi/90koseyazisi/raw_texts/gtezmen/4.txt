Hangi �ks�r�k, hangi hastal���n habercisi 
Gecenin bir vaktinde k���k yavrunuzun �ks�rmeye ba�lad���n� duyunca kahrolursunuz. 

Minicik bedenin �iddetli �ks�r�k n�betleriyle sars�lmas� size ac� verir. �ks�r�klerinin kesilmesi i�in ne yapaca��n�z� �a��r�rs�n�z. Uzmanlar, �ks�rmekle v�cudun hava yollar�n� s�k�nt� veren s�m�ks� maddelerden kurtuldu�unu belirtiyorlar. Ancak �ocu�unuz �ks�r�yor diye de bo�una sevinmeyin. �zellikle k�� aylar�nda �ok s�k g�r�len �ks�r�k n�betleri �ocu�un sa�l���na ili�kin �nemli ipu�lar� verir.

�KS�RMEN�N de�i�ik t�rleri vard�r ve bunlar�n her biri �ocu�un sa�l��� konusunda �nemli ipu�lar� verir. �rne�in, �ocuk gece yar�s� madeni bir ses ��kararak kuru kuru �ks�rmeye ba�lad�. Yabanc�s� oldu�unuz bu t�r �ks�r�k, onun k�� aylar�nda s�k g�r�len bir vir�s enfeksiyonu ge�irdi�ine i�arettir. �ks�r�k n�betleri g�nd�z azal�r ama gece �iddetlenir. �ocu�un ate�i de y�kselmi� olabilir. K�� vir�s�, g�rtlak ve nefes borusunda �i�me ve daralmalara neden olur. Daha �ok bebeklerde ve �� ya��ndan k���k �ocuklarda g�r�l�r. �� ya��ndan sonra �ocuklar�n nefes borular� geni�ledi�i i�in daralma ve geni�leme hareketleri soluk al�p vermeyi fazla etkilemez.

HAP�IRMAYLA GELEN

T�p dilinde ���slak �ks�r�k�� olarak adland�r�lan bu �ks�r�k t�r�nde hap��rma, burun ak�nt�lar� ve g�zlerde sulanma g�r�l�r. Bu t�r �ks�r�k, bildi�imiz so�ukalg�nl���n�n habercisidir. Burunda, sin�slerde, bo�azda ve akci�erlerin geni� hava borular�nda vir�s enfeksiyonu ba�lam��t�r. �ks�r�k n�betleri genellikle so�ukalg�nl��� tedavisi tamamlan�ncaya kadar s�rer. B�yle durumlarda �ocu�un burun yollar�n�n m�mk�n oldu�u kadar temiz tutulmas� gerekir. �ocukta sin�zit ya da ast�m, alerji, hatta zat�rree gibi sorunlar ara�t�r�lmal�.

ISLIK G�B� �KS�R�K

�ocu�unuz g�n i�inde fazla hareket etti�i zamanlar, �i�ek tozlar�, so�uk hava, toz ya da dumana maruz kald�ktan sonra, s�rekli olarak �sl�k �alar gibi seslerle �ks�r�yorsa ve soluklar� h�zlanm��sa, onun ast�ma yakaland��� d���n�lebilir. Ast�m, akci�erlerdeki k���k hava borucuklar�n�n �i�ip daralmas�, s�m�ks� maddeyle dolup soluk almay� g��le�tirmesi �eklinde kendini g�sterir. 

GEN�ZDEN �KS�R�K

�ocuk gece ve g�nd�z ayr�m� olmadan ve s�k s�k genizden gelen �ks�r�k n�betleriyle sars�l�yorsa, huzursuzsa, �steliks�k s�k s�k bo�az�n�n ka��nmas�ndan yak�n�yorsa, grip ba�lang�c� s�z konusu olabilir. Ona bol bol s�v� verip nemli bir ortamda bulunmas�n� sa�lamal�s�n�z.

BALGAMLI �KS�R�K

�ocukta �ncelikle so�ukalg�nl��� belirtileri g�r�l�r, s�k s�k hap��r�r ve burnu akar. Bu belirtiler ba�lad�ktan bir hafta sonra da �ks�r�k ba�lar. �ocuk �ks�r�rken balgam ��kar�r, solu�unu d��ar� verirken de garip sesler ��kar�r. Akci�erlerdeki ��bron�iyolitis�� ad�yla bilinen minik hava yollar�nda bir vir�s enfeksiyonu bu belirtilerin nedeni olabilir. K��a do�ru bu vakalarda �o�alma g�r�l�r ve ilkbahar sonlar�nda hasta say�s� azal�r. Bron�iyolitis'i bildi�imiz bron�it ile kar��t�rmamal�y�z.

Derleyen: Azize BERG�N

SORULAR SORUNLAR

Sar� nokta hastal���

Bir s�re �nce k��enizde bahsi ge�en ��Sar� Nokta�� hastal��� ile ilgili olarak ek bilgiler rica ederiz.

Oya TANDO�AN/ MERS�N

Ac�badem G�z Merkezi'nden G�z Hastal�klar� Uzman� Do�. Dr. Yusuf Durlu, ya�a ba�l� ��sar� nokta�� hastal���n�n tedavisinde kullan�lan ve ABD'de 2000 y�l�nda onaylanan ��fotodinamik tedavi�� sayesinde bu hastal���n ilerlemesinin �n�ne ge�ilmeye �al��t���n� belirtiyor. Sar� nokta hastal���n�n ���slak�� tipinin tedavisinde argon lazer ve fotodinamik lazer kullan�l�yor. Argon lazer �s� etkisiyle sar� nokta dokusunu tahrip etti�i i�in her ne kadar se�ilmi� vakalarda h�l� kullan�lsa da, b�y�k oranda yerini fotodinamik lazere b�rakt�. Fotodinamik tedavide esas ama� hastal��� durdurabilmek ve daha k�t�ye gitmesini �nlemek. Fotodinamik tedavi, �zel bile�imde bir ilac�n koldaki toplardamardan verilmesi ve sar� noktaya lazerin uygulanmas�n� kapsamakta. Yan etkisi �ok az olan bu tedavi 3 ayda bir tekrarlan�yor ve hasta 2 y�l s�reyle izleniyor. ��Kuru�� tipin koruyucu tedavisinde ise doktor kontrol�nde y�ksek doz A, C, E vitaminleri ve �inko veriliyor. 
