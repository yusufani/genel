We live in a world addicted to short-cuts, or is that only me?

Nah, it�s not just me, everyone wants an easy path to everything, especially health, which is a large part of the reason that there�s so much focus on the putative benefits of taking high doses of supplements: anti-oxidants of all sorts, vitamins, minerals, and these days especially, fish oils.

The bottom line, however, is that, with a few possible exceptions (e.g. vitamin D and folic acid in special cases and for specific conditions) there is very little (actually, none) evidence that you can ever get the same benefit to overall health from one or more supplements that you get when you ingest the real McCoy: veggies, fruits, grains, and especially fish.

Fish oils are not fish. Period.

The latest indication that we aim wrong when we try to short-cut fish is a small study presented at the recent annual meeting of the American Stroke Association that took 102 people who�d had a stroke, randomly assigned half to a good dose of fish oils and half to placebo, and at the end of 16 weeks, the researchers claim they could detect absolutely no difference between the 2 groups in anything they tried to measure: good cholesterol, bad cholesterol, clotting factors, inflammatory proteins, all of which are generally positively affected by eating a diet higher in fish intake.

Common sense bottom line, folks: you wanna stay healthy (or get healthier): eat more fish.

It�s good for ya, it�s tasty, and it�s way, way better for the environment than eating beef.

